## Contribuer au projet

Vous pouvez aider au projet et devenir facilement un de ses contributeurs.  

Vous pouvez également rejoindre la communauté à partir de l'application discord "La Fonderie" qui dispose d'un salon qui est dédié au système PF2.

Vous pouvez rejoindre la communauté Pathfinder 2 à partir de l'application discord "Pathfinder-fr" qui dispose également de salons consacrés au chantier du wiki PF2, de salons permettant de discuter du jeu en lui-même et un salon plus particulièrement consacré spécifiquement aux tables virtuelles.

Contribuer est à la portée de chacun. Nous avons constamment besoin de relecteurs et de traducteurs et même d'utilisateurs capables de signaler des erreurs ou des problèmes d'affichage.  

Dans les données présentes, vous disposez également d'un dictionnaire VO/VF et d'un dictionnaire VF/VO. Il montre tout ce qui est traduit et ce qui ne l'est pas encore. Il est généré automatiquement à chaque mise à jour.

Si vous constatez qu'un fichier dont vous avez besoin n'est pas encore traduit, il suffit de cliquer dessus pour l'éditer. Le dépôt du projet dispose d'un éditeur intégré et il suffit donc de vous créer un compte pour pouvoir contribuer. Le fichier sera alors signalé à ceux qui contribuent au système qui pourront alors intégrer votre traduction.

Si vous éditez un fichier, lorsqu'il s'ouvre, il comprend généralement les champs suivants : 

* **Name:** C'est le nom du fichier en anglais et le nom qui est affiché en anglais dans l'interface de Foundry pour cette capacité, ce don, ce sort,... **Vous n'y touchez jamais**. Si vous constatez une typo, n'hésitez pas à la signaler sur le salon discord. Elle sera remontée aux anglophones qui procéderont aux changements.

* **Nom:** Ici vous devrez inscrire le nom que vous aurez traduit s'il n'est pas déjà. S'il existe déjà en version française, vous n'y touchez pas car il faut d'abord vérifier s'il n'apparaît pas également ailleurs dans les données pour être certain de les modifier toutes.

* **État:** L'état sert à générer les tableaux qui sont nécessaires pour mesurer la progression de la traduction et voir où les interventions sont nécessaires. 
** L'indicateur _officielle_ montre que le fichier est à jour et correspond à la traduction officielle réaliséee par l'éditeur francophone (Black Book Editions). Le contenu peut cependant avoir déjà été l'objet d'une de nos interventions pour prendre en compte un erratum ultérieur que BBE n'a pas pu prendre en compte dans le livre. En cas de discordance, **fiez vous au contenu anglophone**. De même, un retraitement est souvent opéré pour supprimer les références aux pages, par exemple ou parfois pour formater la page.
** L'état _libre_ montre que le texte proposé est issu d'une traduction libre faite par un fan. Cela peut-être une traduction officielle qui a été modifiée, si elle est erronée ou trompeuse ou si elle a subi un erratum appliqué ou d'une traduction faite par un fan qui a été réalisée avant la traduction du texte par BBE. Le projet est en avance d'environ un an et demi sur la gamme générale et contient beaucoup plus de données, BBE ne pouvant pas traduire tous les suppléments.
** L'état _aucune_ indique que le fichier n'a encore jamais été traduit jusqu'à présent. Si vous constatez malgré tout la présence d'un nom en français, cela signifie qu'un contributeur est tombée dessus et a eu besoin de traduire le nom de cet objet pour faire la traduction d'autre chose.

** Si c'est vous qui faites la traduction, vous remplacez donc aucune par **libre**.
** si vous y placez la traduction officielle effectuée par BBE en français, vous remplacez alors l'état par **officielle**. 

Si l'état indique **changé**, cela signifie que le fichier anglophone a connu des modifications depuis sa dernière extraction. Cela peut être des changements de structure, de balises, l'insertion de formules d'automatisation, l'introduction d'effets codés dans le système de Foundry pour faciliter le jeu sur la plateforme. 

Dans ce cas, en dessous d'état, devrait figurer un autre champ intitulé **État d'origine:** Il indique si la traduction précédent provient d'une traduction libre ou d'une traduction officielle ou s'il n'en avait aucune. Dans le cas où vous éditez ce fichier, il faut donc supprimer changé pour remettre l'état initial en supprimant la ligne État d'origine après avoir fait les modifications. Pour intervenir sur un fichier changé, il vous faut donc vous assurer de bien repérer les éventuelles modifications survenues dans la vo entre les deux extractions par le biais des outils d'historique de git. 

Si vous ne les utilisez pas, il faut alors tout contrôler, y compris les liens. Il vaut mieux être certain de tout repérer avant de modifier l'état pour le remettre à son état antérieur car l'indicateur changé sert à attirer l'attention de ceux qui maintiennent le système.

* **PrereqEN:** Ce champ n'existe que pour les dons à ce jour et correspond aux prérequis quand ils existent. Vous n'y touchez pas carle système va chercher lors de l'extraction les données utiles automatiquement.

* **PrereqFR:** S'il n'y a rien et que le champ Prereq est rempli, vous devrez alors remplir les prérequis après les avoir traduits. S'il y en a plusieurs, vous devrez les séparer avec une barre verticale (qui s'obtient en appuyant sur AltGr+6).

* ------ **Description (en)** ------
suivie d'un texte en anglais. elle comprend une mise en forme avec des balises de code. Elle sert de modèle mais il ne faut jamais la modifier car c'est l'extraction automatique qui la génère et qui comparera ensuite les deux versions.

* ------ **Description (fr)** ------
S'il n'existe aucune traduction, ce champ est vide et vous devrez le remplir complètement à partir de la traduction. Pour cela, il faut remettre les balises html présentes, traduire le texte de la vo.

Il existe d'autres champs extraits fonctionnant sur le même principe. L'anglais se trouve au dessus et le français en dessous. Il faut seulement remplir le français et ne jamais toucher à l'anglais.

Vous traduisez le texte en vous aidant du Dictionnaire pour les mots clés qui ont un sens technique. Le dictionnaire comprend également tous les mots qui peuvent faire l'objet d'un lien et d'une référence. 

Vous pouvez facilement regarder un fichier qui est déjà traduit pour voir comment il est constitué.

Au besoin, vous pouvez vous faire aider par différents membres de la communauté francophone sur les salons discord La Fonderie ou Pathfinder-fr.

* **Quelques conventions et observations** :

** Une majuscule est utilisée uniquement sur le premier mot du nom de la capacité que vous traduisez et sur les noms propres. Ainsi : _Blocage au bouclier_ pour **S**hield **B**lock en vo

** La première lettre des mots qui correspondent à un terme technique du jeu sont fréquemment en majuscule. Ainsi : _vous faites une **F**rappe contre une créature **O**bservée._ Cela permet au lecteur de savoir que vous visez un terme technique du jeu.


** On supprime absolument toutes les références aux pages des livres puisque nous naviguons sur internet et qu'il est facile de créer des liens, et ce, même si la version anglaise a oublié de le faire. Quand il est indiqué des sorts "dans ce livre", on remplace par des sorts "du livre du joueur". 

** En vf, dans une description, on remet autant que possible les listes créées dans l'ordre alphabétique après traduction ou par niveau puis par ordre alphabétique si c'est classé par niveau, et ce, chaque fois que c'est possible. En cas de doute, ne pas hésiter à demander un avis avant de le faire.

** Dans les textes descriptifs : 1/jour devient **Une fois par jour**. 1 min. devient **1 minute**. On a de la place, ce qui n'est pas le cas dans les livres physiques. L'anglais prenant moins de place, dans les livres, les traducteurs ont souvent besoin de réduire au maximum pour respecter la pagination. Ce n'est pas notre cas.

** Il existe des balises pour créer des liens que vous repérerez facilement car elles prennent la forme suivante `@Compendium[pf2e.feats-srd.muMOxZyduEFv8UT6]{Nom en anglais}`. Quand vous croisez ce genre de choses, vous ne remplacez que ce qui figure entre les accolades {nom affiché}. Cela s'appelle une étiquette et c'est ce qui s'affiche sur votre écran.

Vous pouvez sans souci mettre le contenu entre les accolades au féminin, au pluriel ou même el conjuguer. Les liens entre les fichiers se font ainsi par renvoi à l'ID propre à chaque fichier dans les compendium. L'ID est indiqué entre les [crochets]. Cet ID ne doit par contre jamais être modifié.

** il existe des balises qui permettent, sous Foundry, de lancer les dés à partir du fichier. Elles ont une structure très particulière `@Damage[1d4[piercing]]`. Il faut simplement créer l'étiquette en ajoutant des accolades et en traduisant `@Damage[1d4[piercing]]{1d4 dégâts perforants}`.

De même, il existe d'autres balises vers des effets qui ont un intérêt pour ceux qui utilisent Foundry. On traduit le nom de l'effet quand on en trouve un entre `{texte à traduire}`. Ce qui est entre les `{` est à traduire `}` et sera affiché par le système. 

** D'autres structures appelées inline rolls sont plus complexes à modifier pour effectuer des tests. Demandez conseil sur le discord avant de les toucher.

Pour obtenir de l'aide un petit MP, un tag à rectulo dans l'un ou l'autre des discord mentionnés et vous devriez avoir rapidement une réponse en journée (à l'heure française). Plusieurs autres contributeurs peuvent aussi vous répondre si c'est posté dans les salons discord. Il existe un discord à part pour les traducteurs qui rejoignent le projet.

Fait le 13 janvier 2024
