# État de la traduction (ac-support-benefits)

 * **changé**: 63
 * **libre**: 19


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[2Jaz98WiQMNnqKCD.htm](ac-support-benefits/2Jaz98WiQMNnqKCD.htm)|Monitor Lizard Support Benefit|Avantage de soutien du varan|changé|
|[3jZOVJsYwBQ7GPDi.htm](ac-support-benefits/3jZOVJsYwBQ7GPDi.htm)|Vampiric Animal Support Benefit|Avantage de soutien de l'animal vampirique|changé|
|[4kkcfQdHAb4YTVNw.htm](ac-support-benefits/4kkcfQdHAb4YTVNw.htm)|Ghost Support Benefit|Avantage de soutien de fantôme|changé|
|[5HnWXmOp8qNjfhdU.htm](ac-support-benefits/5HnWXmOp8qNjfhdU.htm)|Wolf Support Benefit|Avantage de soutien du loup|changé|
|[60lThr6OM9zZtqK2.htm](ac-support-benefits/60lThr6OM9zZtqK2.htm)|Elk Support Benefit|Avantage de soutien de l'élan|changé|
|[7rhcqwMDt2xqnI1N.htm](ac-support-benefits/7rhcqwMDt2xqnI1N.htm)|Bird Support Benefit|Avantage de soutien de l'oiseau de proie|changé|
|[8re1d4lM0Dn4sLto.htm](ac-support-benefits/8re1d4lM0Dn4sLto.htm)|Boar Support Benefit|Avantage de soutien du sanglier|changé|
|[9Y5QKMvy0KsliE29.htm](ac-support-benefits/9Y5QKMvy0KsliE29.htm)|Moth Support Benefit|Avantage de soutien du papillon de nuit|changé|
|[Aj7JWFXIbF3qmvSX.htm](ac-support-benefits/Aj7JWFXIbF3qmvSX.htm)|Bat Support Benefit|Avantage de soutien de la chauve-souris|changé|
|[AvDlo1mgxXd7ZA8W.htm](ac-support-benefits/AvDlo1mgxXd7ZA8W.htm)|Bear Support Benefit|Avantage de soutien de l'ours|changé|
|[bEkXle5FLNxnp3IE.htm](ac-support-benefits/bEkXle5FLNxnp3IE.htm)|Undead Bird of Prey Support Benefit|Avantage de soutien : Oiseau de proie squelettique|changé|
|[BFvS1wnFxgUqgX8m.htm](ac-support-benefits/BFvS1wnFxgUqgX8m.htm)|Water Elemental Support Benefit|Avantage de soutien de l'élémentaire d'eau|changé|
|[bHaJZSm7SakAsk00.htm](ac-support-benefits/bHaJZSm7SakAsk00.htm)|Skeletal Servant Support Benefit|Avantage de soutien de serviteur squelettique|changé|
|[C41lCXjerYjMyOTI.htm](ac-support-benefits/C41lCXjerYjMyOTI.htm)|Riding Drake Support Benefit|Avantage de soutien du drake de monte|changé|
|[cJBAKsVacLTIiPcz.htm](ac-support-benefits/cJBAKsVacLTIiPcz.htm)|Hippogriff Support Benefit|Avantage de soutien de l'hippogriffe|changé|
|[CKMQC3zu1XoGjeNd.htm](ac-support-benefits/CKMQC3zu1XoGjeNd.htm)|Skeletal Mount Support Benefit|Avantage de soutien de monture squelettique|changé|
|[D8uiA6qJN2NfxDjx.htm](ac-support-benefits/D8uiA6qJN2NfxDjx.htm)|Mongoose Support Benefit|Avantage de soutien de la mangouste|changé|
|[Fl1VEpYhmScEEon8.htm](ac-support-benefits/Fl1VEpYhmScEEon8.htm)|Capybara Support Benefit|Avantage de soutien du capybara|changé|
|[g3Wol5lNhQdxqs7t.htm](ac-support-benefits/g3Wol5lNhQdxqs7t.htm)|Draft Lizard Support Benefit|Avantage de soutien du lézard de trait|changé|
|[h65iGxPkfrT8DdPv.htm](ac-support-benefits/h65iGxPkfrT8DdPv.htm)|Shadow Hound Support Benefit|Avantage de soutien du molosse d'ombre|changé|
|[HCQXxqMtH81jE1W5.htm](ac-support-benefits/HCQXxqMtH81jE1W5.htm)|Cat Support Benefit|Avantage de soutien du félin|changé|
|[imNJpD4afn4UrLpu.htm](ac-support-benefits/imNJpD4afn4UrLpu.htm)|Undead Hand Support Benefit|Avantage de soutien de la main morte-vivante|changé|
|[IUXX1lUd9kMzV4bq.htm](ac-support-benefits/IUXX1lUd9kMzV4bq.htm)|Water Wraith Support Benefit|Avantage de soutien de l'âme-en-peine aquatique|changé|
|[kecawjDhBFz5QA4f.htm](ac-support-benefits/kecawjDhBFz5QA4f.htm)|Rhinoceros Support Benefit|Avantage de soutien du rhinocéros|changé|
|[KomM6mTwJAzvqAgi.htm](ac-support-benefits/KomM6mTwJAzvqAgi.htm)|Wood Elemental Support Benefit|Avantage de soutien d'élémentaire de bois|changé|
|[kZvomyJobGrDs1MU.htm](ac-support-benefits/kZvomyJobGrDs1MU.htm)|Legchair Support Benefit|Avantage de soutien de la chaise sur pattes|changé|
|[LcAJTLzLDcIrQ7KQ.htm](ac-support-benefits/LcAJTLzLDcIrQ7KQ.htm)|Giraffe Support Benefit|Avantage de support de la girafe|changé|
|[lzaNwsSxjVPH1nzn.htm](ac-support-benefits/lzaNwsSxjVPH1nzn.htm)|Camel Support benefit|Avantage de soutien du chameau|changé|
|[mFTzDCqNL5EdwlGm.htm](ac-support-benefits/mFTzDCqNL5EdwlGm.htm)|Beetle Support Benefit|Avantage de soutien du scarabée|changé|
|[N9fYaUuIgD6RQ7CY.htm](ac-support-benefits/N9fYaUuIgD6RQ7CY.htm)|Zombie Mount Support Benefit|Avantage de soutien : Monture zombie|changé|
|[nDUwRm0TOtc2EX0O.htm](ac-support-benefits/nDUwRm0TOtc2EX0O.htm)|Cave Gecko Support Benefit|Avantage de soutien du gecko des cavernes|changé|
|[NKgFIZQYkL3bb6xn.htm](ac-support-benefits/NKgFIZQYkL3bb6xn.htm)|Riding Tarantula Support Benefit|Avantage de soutien de la tarentule de selle|changé|
|[NSdcXgEHVHdfJlkb.htm](ac-support-benefits/NSdcXgEHVHdfJlkb.htm)|Fire Elemental Support Benefit|Avantage de soutien de l'élémentaire de feu|changé|
|[oLwBrrW33fIjSHLr.htm](ac-support-benefits/oLwBrrW33fIjSHLr.htm)|Terror Bird Support Benefit|Avantage de soutien de l'oiseau de terreur|changé|
|[QEZUnvbdrgQJAUs0.htm](ac-support-benefits/QEZUnvbdrgQJAUs0.htm)|Vulture Support Benefit|Avantage de soutien du vautour|changé|
|[QiQvx7jtfrkGnf13.htm](ac-support-benefits/QiQvx7jtfrkGnf13.htm)|Pangolin Support Benefit|Avantage de soutien du pangolin|changé|
|[Qm0iwIYyy2gbnYXe.htm](ac-support-benefits/Qm0iwIYyy2gbnYXe.htm)|Ape Support Benefit|Avantage de soutien du singe|changé|
|[R0e3jHpB7nTzIUPm.htm](ac-support-benefits/R0e3jHpB7nTzIUPm.htm)|Roc Support Benefit|Avantage de soutien du roc|changé|
|[r7QN2lUVWXlcynMW.htm](ac-support-benefits/r7QN2lUVWXlcynMW.htm)|Elephant Support Benefit|Avantage de soutien de l'éléphant|changé|
|[ShKTegi69Hizn0vB.htm](ac-support-benefits/ShKTegi69Hizn0vB.htm)|Griffon Support Benefit|Avantage de soutien du griffon|changé|
|[Si13B4VFANZN8mi1.htm](ac-support-benefits/Si13B4VFANZN8mi1.htm)|Skeletal Constrictor Support Benefit|Avantage de soutien : Serpent constricteur squelettique|changé|
|[SZD1hU2ageIDw4To.htm](ac-support-benefits/SZD1hU2ageIDw4To.htm)|Metal Elemental Support Benefit|Avantage de soutien d'élémentaire de métal|changé|
|[t1KMf7rDzJJ2vWhe.htm](ac-support-benefits/t1KMf7rDzJJ2vWhe.htm)|Hippocampus Support Benefit|Avantage de soutien de l'hippocampe|changé|
|[TDMhLC16cQOYBbmA.htm](ac-support-benefits/TDMhLC16cQOYBbmA.htm)|Triceratops Support Benefit|Avantage de soutien du tricératops|changé|
|[TFrCa4aCUKtwhuqn.htm](ac-support-benefits/TFrCa4aCUKtwhuqn.htm)|Tikar Urchinpad Support Benefit|Avantage de soutien des radeaux-méduses|changé|
|[u9jaNQk6QF2Tm8oe.htm](ac-support-benefits/u9jaNQk6QF2Tm8oe.htm)|Hermit Krait Support Benefit|Avantage de soutien du Serpent-pagure|changé|
|[ufAZJqCN7ZOBhMSJ.htm](ac-support-benefits/ufAZJqCN7ZOBhMSJ.htm)|Shotalashu Support Benefit|Avantage de soutien du shotalashu|changé|
|[uZLtEOimbFnEDUKh.htm](ac-support-benefits/uZLtEOimbFnEDUKh.htm)|Ulgrem-Lurann Support Benefit|Avantage de soutien de l'ulgrem-Lurann|changé|
|[V2kspaYOoLbHV5vb.htm](ac-support-benefits/V2kspaYOoLbHV5vb.htm)|Flying Squirrel Support Benefit|Avantage de soutien de l'écureuil volant|changé|
|[V3XczB8r8fQfJIhe.htm](ac-support-benefits/V3XczB8r8fQfJIhe.htm)|Giant Wasp Support Benefit|Avantage de soutien de la guêpe géante|changé|
|[vvuB2kEzRYSs4IpH.htm](ac-support-benefits/vvuB2kEzRYSs4IpH.htm)|Mole Support Benefit|Avantage de soutien de la taupe|changé|
|[wf20kx6pPGBWwUn2.htm](ac-support-benefits/wf20kx6pPGBWwUn2.htm)|Horse Support Benefit|Avantage de soutien du cheval|changé|
|[wlh1poSMLK9wk2Nw.htm](ac-support-benefits/wlh1poSMLK9wk2Nw.htm)|Hyena Support Benefit|Avantage de soutien de la hyène|changé|
|[xpEAKxiQUlUXkqV4.htm](ac-support-benefits/xpEAKxiQUlUXkqV4.htm)|Sundaflora Support Benefit|Avantage de soutient de Sundaflora|changé|
|[XrxIRaD1A5tpKm1t.htm](ac-support-benefits/XrxIRaD1A5tpKm1t.htm)|Tyrannosaurus Support Benefit|Avantage de soutien du tyrannosaure|changé|
|[xtXdDC5KrUqh7eCu.htm](ac-support-benefits/xtXdDC5KrUqh7eCu.htm)|Scorpion Support Benefit|Avantage de soutien du scorpion|changé|
|[XVOVZjvTwaoalpWC.htm](ac-support-benefits/XVOVZjvTwaoalpWC.htm)|Umbrella Mushroom Support Benefit|Avantage de soutien du champignon parapluie|changé|
|[XWRd0x7AvdXoeJi3.htm](ac-support-benefits/XWRd0x7AvdXoeJi3.htm)|Salamander Support Benefit|Avantage de soutien de la salamandre|changé|
|[yAYlM3aIRYnqx6j8.htm](ac-support-benefits/yAYlM3aIRYnqx6j8.htm)|Zombie Support Benefit|Avantage de soutien du zombie|changé|
|[yfjeiYMC27YGgoGW.htm](ac-support-benefits/yfjeiYMC27YGgoGW.htm)|Crocodile Support Benefit|Avantage de soutien du crocodile|changé|
|[yIB4qKV5sqcXlOmB.htm](ac-support-benefits/yIB4qKV5sqcXlOmB.htm)|Antelope Support Benefit|Avantage de soutien de l'antilope|changé|
|[YsLLV4J7L6fgzTXf.htm](ac-support-benefits/YsLLV4J7L6fgzTXf.htm)|Shark Support Benefit|Avantage de soutien du squale|changé|
|[zE01vpjowhoKygig.htm](ac-support-benefits/zE01vpjowhoKygig.htm)|Zombie Carrion Bird Support Benefit|Avantage de soutien : Oiseau charognard zombie|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[3ZYwTYI4ORCz4xQg.htm](ac-support-benefits/3ZYwTYI4ORCz4xQg.htm)|Wyvern Support Benefit|Avantage de soutien de la vouivre|libre|
|[4RXXEQVh8wXmlBce.htm](ac-support-benefits/4RXXEQVh8wXmlBce.htm)|Yzobu Support Benefit|Avantage de soutien de l'Yzobu|libre|
|[5y5rpOAEPs2fP0l4.htm](ac-support-benefits/5y5rpOAEPs2fP0l4.htm)|Chetamog Support Benefit|Avantage de soutien du chetamog|libre|
|[8b1G95eWa3DaTvw8.htm](ac-support-benefits/8b1G95eWa3DaTvw8.htm)|Cave Pterosaur Support Benefit|Avantage de soutien du ptérosaure des grottes|libre|
|[aMFtyZ3QNQkduwqR.htm](ac-support-benefits/aMFtyZ3QNQkduwqR.htm)|Air Elemental Support Benefit|Avantage de soutien de l'élémentaire d'air|libre|
|[bO92dKyk8V4aM0NP.htm](ac-support-benefits/bO92dKyk8V4aM0NP.htm)|Arboreal Sapling Support Benefit|Avantage de soutien du jeune arboréen|libre|
|[Du0wsxf7TitL6wSb.htm](ac-support-benefits/Du0wsxf7TitL6wSb.htm)|Dromaeosaur Support Benefit|Avantage de soutien du droméosaure|libre|
|[EkyB80qlJ6VbC7bi.htm](ac-support-benefits/EkyB80qlJ6VbC7bi.htm)|Orca Support Benefit|Avantage de soutien de l'orque|libre|
|[Ep4MMKK1rtTJHa8d.htm](ac-support-benefits/Ep4MMKK1rtTJHa8d.htm)|Durian Crab Support Benefit|Avantage de soutien du crabe durian|libre|
|[hC0YEa4Z8I4XxV3r.htm](ac-support-benefits/hC0YEa4Z8I4XxV3r.htm)|Oozeform Chair Support Benefit|Avantage de soutien du fauteuil de forme de vase|libre|
|[hjvjnSzrslebg0dk.htm](ac-support-benefits/hjvjnSzrslebg0dk.htm)|Rootball Chair Support Benefit|Avantage de soutien du Fauteuil motte de racines|libre|
|[LscuCDZJ5oTscjmJ.htm](ac-support-benefits/LscuCDZJ5oTscjmJ.htm)|Giant Frog Support Benefit|Avantage de support de la grenouille géante|libre|
|[nQwTbiyTHahsvmBi.htm](ac-support-benefits/nQwTbiyTHahsvmBi.htm)|Badger Support Benefit|Avantage de soutien du blaireau|libre|
|[oawpSPdSj1hSkSYk.htm](ac-support-benefits/oawpSPdSj1hSkSYk.htm)|Kangaroo Support Benefit|Avantage de soutien du kangourou|libre|
|[trujzLuDqjQ7nJEO.htm](ac-support-benefits/trujzLuDqjQ7nJEO.htm)|Snake Support Benefit|Avantage de soutien du serpent|libre|
|[ugEUIh47YrNUIsnd.htm](ac-support-benefits/ugEUIh47YrNUIsnd.htm)|Goat Support Benefit|Avantage de soutien de la chèvre|libre|
|[umNfDRmjldi67XCU.htm](ac-support-benefits/umNfDRmjldi67XCU.htm)|Giant Eel Support Benefit|Avantage de soutien de l'anguille géante|libre|
|[vGAy81FNblv833jk.htm](ac-support-benefits/vGAy81FNblv833jk.htm)|Augdunar Support Benefit|Avantage de soutien de l'augdunar|libre|
|[XteDiKFR58AkGXlP.htm](ac-support-benefits/XteDiKFR58AkGXlP.htm)|Earth Elemental Support Benefit|Avantage de soutien d'élémentaire de terre|libre|
