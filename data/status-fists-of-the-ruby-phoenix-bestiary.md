# État de la traduction (fists-of-the-ruby-phoenix-bestiary)

 * **libre**: 140
 * **aucune**: 1


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[YHbBs2Jq6ukngOra.htm](fists-of-the-ruby-phoenix-bestiary/YHbBs2Jq6ukngOra.htm)|Sanzuwu|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0N4ugHFs5GYllcMA.htm](fists-of-the-ruby-phoenix-bestiary/0N4ugHFs5GYllcMA.htm)|Shino Hakusa (Level 16)|Shino Hakusa (Niveau 16)|libre|
|[1y2FBsV4g80KNac1.htm](fists-of-the-ruby-phoenix-bestiary/1y2FBsV4g80KNac1.htm)|Takatorra (Level 13)|Takatorra (Niveau 13)|libre|
|[2ryxyUYlf8lY6o1D.htm](fists-of-the-ruby-phoenix-bestiary/2ryxyUYlf8lY6o1D.htm)|Golarion's Finest (Mingyu)|Les Champions de Golarion (Mingyu)|libre|
|[2ZnZ0d61CQHMHtHN.htm](fists-of-the-ruby-phoenix-bestiary/2ZnZ0d61CQHMHtHN.htm)|Arms of Balance (Ranya Shibhatesh)|Bras de l'équilibre (Ranya Shibhatesh)|libre|
|[4RaeyGxw5EgagDZE.htm](fists-of-the-ruby-phoenix-bestiary/4RaeyGxw5EgagDZE.htm)|Surjit Hamelan|Surjit Hamelan|libre|
|[526cj9mVkwQ9gDn6.htm](fists-of-the-ruby-phoenix-bestiary/526cj9mVkwQ9gDn6.htm)|Tino (Oni Form)|Tino (Forme Oni)|libre|
|[56U7JoNKbci67vRE.htm](fists-of-the-ruby-phoenix-bestiary/56U7JoNKbci67vRE.htm)|Rivka (Kujiba)|Rivka (Kujiba)|libre|
|[5cMbN10QKGS06Zhy.htm](fists-of-the-ruby-phoenix-bestiary/5cMbN10QKGS06Zhy.htm)|Arms of Balance (Jivati Rovat)|Bras de l'équilibre (Jivati Rovat)|libre|
|[65LJVPu9DFkF0YNX.htm](fists-of-the-ruby-phoenix-bestiary/65LJVPu9DFkF0YNX.htm)|Arms of Balance (Usvani)|Bras de l'équilibre (Usvani)|libre|
|[6Eh9w3FY129KXm7k.htm](fists-of-the-ruby-phoenix-bestiary/6Eh9w3FY129KXm7k.htm)|Kas Xi Rai|Kas Xi Rai|libre|
|[6wRicVhIx07i94s3.htm](fists-of-the-ruby-phoenix-bestiary/6wRicVhIx07i94s3.htm)|Taiga Yai|Taïga Yaï|libre|
|[6z4xzi3yZuGr1Iv7.htm](fists-of-the-ruby-phoenix-bestiary/6z4xzi3yZuGr1Iv7.htm)|Spirit Turtle|Esprit Tortue|libre|
|[7MiWOVxjocxNr0dh.htm](fists-of-the-ruby-phoenix-bestiary/7MiWOVxjocxNr0dh.htm)|Laruhao|Laruhao|libre|
|[7pCof1aDBZ1XlsXO.htm](fists-of-the-ruby-phoenix-bestiary/7pCof1aDBZ1XlsXO.htm)|Tino Tung (Level 13)|Tino Tung (Niveau 13)|libre|
|[7qyR096MhufccSiM.htm](fists-of-the-ruby-phoenix-bestiary/7qyR096MhufccSiM.htm)|Yabin the Just (Level 13)|Yabin le juste (niveau 13)|libre|
|[80tgHHqfxA5lUfjS.htm](fists-of-the-ruby-phoenix-bestiary/80tgHHqfxA5lUfjS.htm)|Bul-Gae|Bul-Gae|libre|
|[8jNxwdzrSMFyoHJ5.htm](fists-of-the-ruby-phoenix-bestiary/8jNxwdzrSMFyoHJ5.htm)|Swatting Tail|Queue écrasante|libre|
|[9ksOukuecb43zqOd.htm](fists-of-the-ruby-phoenix-bestiary/9ksOukuecb43zqOd.htm)|Razu|Razu|libre|
|[9WcpNMJJqF3cLTqT.htm](fists-of-the-ruby-phoenix-bestiary/9WcpNMJJqF3cLTqT.htm)|Ran-to (Level 16)|Ran-to (Niveau 16)|libre|
|[AeWPn9gJMgDg7WTo.htm](fists-of-the-ruby-phoenix-bestiary/AeWPn9gJMgDg7WTo.htm)|Lophiithu|Lophiithu|libre|
|[aiiUNGDPljyMuFu2.htm](fists-of-the-ruby-phoenix-bestiary/aiiUNGDPljyMuFu2.htm)|Syu Tak-nwa (Level 16)|Syu Tak-nwa (Niveau 16)|libre|
|[AxVNn9nyobosLEAq.htm](fists-of-the-ruby-phoenix-bestiary/AxVNn9nyobosLEAq.htm)|Agile Warrior|Guerrier agile|libre|
|[AYQEkPFyTuGlxNg3.htm](fists-of-the-ruby-phoenix-bestiary/AYQEkPFyTuGlxNg3.htm)|Mage of Many Styles|Mage de styles variés|libre|
|[BcRxQjtyMNgiEpvb.htm](fists-of-the-ruby-phoenix-bestiary/BcRxQjtyMNgiEpvb.htm)|Floating Flamethrower|Lance-flammes flottant|libre|
|[blm15iXMR3Lbwdom.htm](fists-of-the-ruby-phoenix-bestiary/blm15iXMR3Lbwdom.htm)|Mafika Ayuwari|Mafika Ayuwari|libre|
|[cg1kQPO3FBSCFDVt.htm](fists-of-the-ruby-phoenix-bestiary/cg1kQPO3FBSCFDVt.htm)|Shadow Yai|Yaï des ombres|libre|
|[CKS4OsoyI2QI8RUn.htm](fists-of-the-ruby-phoenix-bestiary/CKS4OsoyI2QI8RUn.htm)|Golarion's Finest (Han)|Les Champions de Golarion (Han)|libre|
|[d7fBEPfzXSeCZZXE.htm](fists-of-the-ruby-phoenix-bestiary/d7fBEPfzXSeCZZXE.htm)|Canopy Elder|Aîné de la canopée|libre|
|[dTcsY3v9lg4BRrsj.htm](fists-of-the-ruby-phoenix-bestiary/dTcsY3v9lg4BRrsj.htm)|Anugobu Apprentice|Apprenti anugobu|libre|
|[DW4UFHXzExWwvEuH.htm](fists-of-the-ruby-phoenix-bestiary/DW4UFHXzExWwvEuH.htm)|Dread Roc|Roc effroyable|libre|
|[EDeiF3PNtXUeBU6P.htm](fists-of-the-ruby-phoenix-bestiary/EDeiF3PNtXUeBU6P.htm)|Air Rift|Faille de l'air|libre|
|[EItI5u34FIIuZM9W.htm](fists-of-the-ruby-phoenix-bestiary/EItI5u34FIIuZM9W.htm)|Angoyang|Angoyang|libre|
|[eVKjtiQtDaJpk9Ra.htm](fists-of-the-ruby-phoenix-bestiary/eVKjtiQtDaJpk9Ra.htm)|Sixth Pillar Student|Étudiant du Sixième pilier|libre|
|[ew2WOdIU5njPMzok.htm](fists-of-the-ruby-phoenix-bestiary/ew2WOdIU5njPMzok.htm)|Watchtower Shadow|Ombre vigie|libre|
|[EYr9GBleLFezHqBk.htm](fists-of-the-ruby-phoenix-bestiary/EYr9GBleLFezHqBk.htm)|Rivka (Yorak)|Rivka (Yorak)|libre|
|[Ezm1FlUk4JXeqPlC.htm](fists-of-the-ruby-phoenix-bestiary/Ezm1FlUk4JXeqPlC.htm)|Rai Sho Disciple|Disciple Rai sho|libre|
|[f0nafAQA1tSXLqwL.htm](fists-of-the-ruby-phoenix-bestiary/f0nafAQA1tSXLqwL.htm)|Abbot Tsujon|Abbé Tsujon|libre|
|[fOdNBYgjTveLzpbd.htm](fists-of-the-ruby-phoenix-bestiary/fOdNBYgjTveLzpbd.htm)|Agile Warrior (Nightmares)|Guerrier agile (Cauchemars)|libre|
|[FWfKJ7e8UgbBXTkU.htm](fists-of-the-ruby-phoenix-bestiary/FWfKJ7e8UgbBXTkU.htm)|Weapon Master (Under the Pale Sun Dervishes)|Maître d'armes (sous les Derviches du Soleil pâle)|libre|
|[FwIJpQn1FIpGgvIY.htm](fists-of-the-ruby-phoenix-bestiary/FwIJpQn1FIpGgvIY.htm)|Artus Rodrivan|Artus Rodrivan|libre|
|[Fwjqlg5gsZwx1FuC.htm](fists-of-the-ruby-phoenix-bestiary/Fwjqlg5gsZwx1FuC.htm)|Quaking Footfall|Secouer la terre sous ses pas|libre|
|[fxukt1qqVaQG9KcY.htm](fists-of-the-ruby-phoenix-bestiary/fxukt1qqVaQG9KcY.htm)|Syu Tak-nwa (Level 20)|Syu Tak-nwa (Niveau 20)|libre|
|[g1ms3xQfS6Ws7KGD.htm](fists-of-the-ruby-phoenix-bestiary/g1ms3xQfS6Ws7KGD.htm)|Sthira|Sthira|libre|
|[gGDWBzon2T2mrSqf.htm](fists-of-the-ruby-phoenix-bestiary/gGDWBzon2T2mrSqf.htm)|Hummingbird|Colibri|libre|
|[ghAlcSfpO03JLvwG.htm](fists-of-the-ruby-phoenix-bestiary/ghAlcSfpO03JLvwG.htm)|Ji-yook (Level 13)|Ji-yook (Niveau 13)|libre|
|[gSlkZ86P3QrbM874.htm](fists-of-the-ruby-phoenix-bestiary/gSlkZ86P3QrbM874.htm)|Weapon Master|Maître d'armes|libre|
|[H5koEzKL8qtfX4fK.htm](fists-of-the-ruby-phoenix-bestiary/H5koEzKL8qtfX4fK.htm)|Urnak Lostwind|Urnak Ventperdu|libre|
|[haUIlixea4iwy0GZ.htm](fists-of-the-ruby-phoenix-bestiary/haUIlixea4iwy0GZ.htm)|Ki Adept (Ahmoza Twins)|Adepte du Qi (jumeaux Ahmoza)|libre|
|[hCHAr6Mp0AxSxj3h.htm](fists-of-the-ruby-phoenix-bestiary/hCHAr6Mp0AxSxj3h.htm)|Wronged Monk's Wrath|Couroux du moine lésé|libre|
|[HGtyyMucLkYlqZ8i.htm](fists-of-the-ruby-phoenix-bestiary/HGtyyMucLkYlqZ8i.htm)|Jin-hae|Jin-hae|libre|
|[hkGMvgL9IOZcAKyc.htm](fists-of-the-ruby-phoenix-bestiary/hkGMvgL9IOZcAKyc.htm)|Hana's Hundreds|Les Cent d'Hana|libre|
|[HqN4nUnl75foBKLZ.htm](fists-of-the-ruby-phoenix-bestiary/HqN4nUnl75foBKLZ.htm)|Nai Yan Fei|Nai Yan Fei|libre|
|[htshGim6Q1y75TPt.htm](fists-of-the-ruby-phoenix-bestiary/htshGim6Q1y75TPt.htm)|Desecrated Guardian|Gardien profané|libre|
|[i3dC41mOjfoBxQTk.htm](fists-of-the-ruby-phoenix-bestiary/i3dC41mOjfoBxQTk.htm)|Juspix Rammel|Juspix Rammel|libre|
|[IGEht7ysUSZndtgp.htm](fists-of-the-ruby-phoenix-bestiary/IGEht7ysUSZndtgp.htm)|Old Man Statue|Statue de vieillard|libre|
|[IlDWyCnxA6gS2UZm.htm](fists-of-the-ruby-phoenix-bestiary/IlDWyCnxA6gS2UZm.htm)|Syndara the Sculptor|Syndara le sculpteur|libre|
|[iQ1wsuosURrA6tUI.htm](fists-of-the-ruby-phoenix-bestiary/iQ1wsuosURrA6tUI.htm)|Portal Eater|Mangeportail|libre|
|[J7s3MTb3eZP6u3LT.htm](fists-of-the-ruby-phoenix-bestiary/J7s3MTb3eZP6u3LT.htm)|Takatorra (Level 9)|Takatorra (Niveau 9)|libre|
|[JFa9WD0nXOTgFGha.htm](fists-of-the-ruby-phoenix-bestiary/JFa9WD0nXOTgFGha.htm)|Golarion's Finest (Numoriz)|Les Champions de Golarion (Numoriz)|libre|
|[jnN7fYYndv3oobJJ.htm](fists-of-the-ruby-phoenix-bestiary/jnN7fYYndv3oobJJ.htm)|Butterfly Blade Warrior|Guerrier Lame Papillon|libre|
|[k5UjbiYr87NVK7qp.htm](fists-of-the-ruby-phoenix-bestiary/k5UjbiYr87NVK7qp.htm)|Rivka (Cimurlian)|Rivka (Cimurlian)|libre|
|[ka41qAFCgGFBXUPJ.htm](fists-of-the-ruby-phoenix-bestiary/ka41qAFCgGFBXUPJ.htm)|Watchtower Wraith|Âme-en-peine vigie|libre|
|[KfpYMKJ1ka9volP6.htm](fists-of-the-ruby-phoenix-bestiary/KfpYMKJ1ka9volP6.htm)|Grandfather Mantis|Grand-père Mante|libre|
|[khAJHftnWe21eTm2.htm](fists-of-the-ruby-phoenix-bestiary/khAJHftnWe21eTm2.htm)|Shino Hakusa (Level 14)|Shino Hakusa (Niveau 14)|libre|
|[KHPOm5KHtbMNzcx7.htm](fists-of-the-ruby-phoenix-bestiary/KHPOm5KHtbMNzcx7.htm)|Sigbin|Sigbin|libre|
|[koRKlywSbwttifEq.htm](fists-of-the-ruby-phoenix-bestiary/koRKlywSbwttifEq.htm)|Amihan|Amihan|libre|
|[l2oapyRhfCFRsCHX.htm](fists-of-the-ruby-phoenix-bestiary/l2oapyRhfCFRsCHX.htm)|Umbasi|Umbasi|libre|
|[L2OWZ5Afvnmaif83.htm](fists-of-the-ruby-phoenix-bestiary/L2OWZ5Afvnmaif83.htm)|Tamikan|Tamikan|libre|
|[lemXhFkFAUbZGtL6.htm](fists-of-the-ruby-phoenix-bestiary/lemXhFkFAUbZGtL6.htm)|Kun|Kun|libre|
|[Lk4LPjGsidzuD6Vy.htm](fists-of-the-ruby-phoenix-bestiary/Lk4LPjGsidzuD6Vy.htm)|Huldrin Skolsdottir|Huldrin Skolsdottir|libre|
|[lqHn5wSzxvdllGgH.htm](fists-of-the-ruby-phoenix-bestiary/lqHn5wSzxvdllGgH.htm)|Flying Mountain Kaminari|Kaminari de la montagne volante|libre|
|[Lsa0Q8aU8aPWjFYv.htm](fists-of-the-ruby-phoenix-bestiary/Lsa0Q8aU8aPWjFYv.htm)|Kannitri|Kannitri|libre|
|[MdkbAYfKgIvtTxBO.htm](fists-of-the-ruby-phoenix-bestiary/MdkbAYfKgIvtTxBO.htm)|Rivka (Mogaru)|Rivka (Mogaru)|libre|
|[mdOASi6f7CTVH1ys.htm](fists-of-the-ruby-phoenix-bestiary/mdOASi6f7CTVH1ys.htm)|Dimensional Darkside Mirror|Miroir dimensionel aux sombres reflets|libre|
|[mwovQpf278O1vdCR.htm](fists-of-the-ruby-phoenix-bestiary/mwovQpf278O1vdCR.htm)|Watchtower Poltergeist|Poltergeist vigie|libre|
|[N9Qp7x6n6ighaXOY.htm](fists-of-the-ruby-phoenix-bestiary/N9Qp7x6n6ighaXOY.htm)|Golarion's Finest (Jun)|Les Champions de Golarion (Jun)|libre|
|[nejHZ9ccKcDXZWQM.htm](fists-of-the-ruby-phoenix-bestiary/nejHZ9ccKcDXZWQM.htm)|Gomwai|Gomwai|libre|
|[o8UMCrKWf89xNvSE.htm](fists-of-the-ruby-phoenix-bestiary/o8UMCrKWf89xNvSE.htm)|Ki Adept|Adepte du Qi|libre|
|[ofYZYIZTkakVj2R0.htm](fists-of-the-ruby-phoenix-bestiary/ofYZYIZTkakVj2R0.htm)|Ji-yook (Gumiho Form)|Ji-yook (Forme Gumiho)|libre|
|[PhFDdGFXaaiLe4k4.htm](fists-of-the-ruby-phoenix-bestiary/PhFDdGFXaaiLe4k4.htm)|Joon-seo|Joon-seo|libre|
|[pi3njPvz8AsOrVGZ.htm](fists-of-the-ruby-phoenix-bestiary/pi3njPvz8AsOrVGZ.htm)|Golarion's Finest (Rajna)|Les Champions de Golarion (Rajna)|libre|
|[PiO3DnxtaT3W4728.htm](fists-of-the-ruby-phoenix-bestiary/PiO3DnxtaT3W4728.htm)|Peng|Peng|libre|
|[PooDdJo4wmkBvETW.htm](fists-of-the-ruby-phoenix-bestiary/PooDdJo4wmkBvETW.htm)|Freezing Floor Tiles|Tuiles de sol glaciales|libre|
|[PsipHKpczW9FT2Jk.htm](fists-of-the-ruby-phoenix-bestiary/PsipHKpczW9FT2Jk.htm)|Planar Terra-cotta Soldier|Soldat en terre cuite planaire|libre|
|[Q6gjfL2rha7tRS0B.htm](fists-of-the-ruby-phoenix-bestiary/Q6gjfL2rha7tRS0B.htm)|Ji-yook (Level 9)|Ji-yook (Niveau 9)|libre|
|[QhFUhlZl1bvUrtPs.htm](fists-of-the-ruby-phoenix-bestiary/QhFUhlZl1bvUrtPs.htm)|Rai Sho Postulant|Postulant Rai sho|libre|
|[qpbFSs0Z68z58T4m.htm](fists-of-the-ruby-phoenix-bestiary/qpbFSs0Z68z58T4m.htm)|Tino Tung (Level 9)|Tino Tung (Niveau 9)|libre|
|[qV1bsHLfgI8xh0Nr.htm](fists-of-the-ruby-phoenix-bestiary/qV1bsHLfgI8xh0Nr.htm)|Lantondo|Lantondo|libre|
|[qW8stNdPqQwibYQz.htm](fists-of-the-ruby-phoenix-bestiary/qW8stNdPqQwibYQz.htm)|Ran-to (Level 20)|Ran-to (Niveau 20)|libre|
|[rAsJXFWr1IJatEUm.htm](fists-of-the-ruby-phoenix-bestiary/rAsJXFWr1IJatEUm.htm)|Mammoth Turtle|Tortue mamouth|libre|
|[RCbSoJ0Kg6ovGnRG.htm](fists-of-the-ruby-phoenix-bestiary/RCbSoJ0Kg6ovGnRG.htm)|Hantu Belian|Hantu Bélian|libre|
|[RKkIczqYUJkij0Oh.htm](fists-of-the-ruby-phoenix-bestiary/RKkIczqYUJkij0Oh.htm)|Blue Viper (Level 14)|Vipère bleue (Niveau 14)|libre|
|[RVaEIyjBl44etuvL.htm](fists-of-the-ruby-phoenix-bestiary/RVaEIyjBl44etuvL.htm)|Shino Hakusa (Level 20)|Shino Hakusa (Niveau 20)|libre|
|[se8SrwtuaamXIEo5.htm](fists-of-the-ruby-phoenix-bestiary/se8SrwtuaamXIEo5.htm)|Blue Viper (Level 20)|Vipère bleue (Niveau 20)|libre|
|[SF6OlCfNiHvtJQjw.htm](fists-of-the-ruby-phoenix-bestiary/SF6OlCfNiHvtJQjw.htm)|Blue Viper (Level 16)|Vipère bleue (Niveau 16)|libre|
|[snrGRRYXD8mUG4Y1.htm](fists-of-the-ruby-phoenix-bestiary/snrGRRYXD8mUG4Y1.htm)|Golarion's Finest (Krankkiss)|Les Champions de Golarion (Krankkiss)|libre|
|[sVXMz65SI2rla17T.htm](fists-of-the-ruby-phoenix-bestiary/sVXMz65SI2rla17T.htm)|Sand Whirlwind|Tourbillon de sable|libre|
|[Sx7Ob64O0e9aIhpO.htm](fists-of-the-ruby-phoenix-bestiary/Sx7Ob64O0e9aIhpO.htm)|Syu Tak-nwa (Level 14)|Syu Tak-nwa (Niveau 14)|libre|
|[sXtwJyM7sWWDQDOU.htm](fists-of-the-ruby-phoenix-bestiary/sXtwJyM7sWWDQDOU.htm)|Muckish Creep|Limon rampant|libre|
|[T5KMiSE8W1R8ChbV.htm](fists-of-the-ruby-phoenix-bestiary/T5KMiSE8W1R8ChbV.htm)|Grave Spinosaurus|Spinosaure des tombes|libre|
|[TbImjEpxYO5tsJIA.htm](fists-of-the-ruby-phoenix-bestiary/TbImjEpxYO5tsJIA.htm)|Gumiho|Gumiho|libre|
|[tBQgipDUVhamBNrZ.htm](fists-of-the-ruby-phoenix-bestiary/tBQgipDUVhamBNrZ.htm)|Dromornis|Dromornis|libre|
|[TEHxsglxWi1i3jSN.htm](fists-of-the-ruby-phoenix-bestiary/TEHxsglxWi1i3jSN.htm)|Elder Cauthooj|Cauthooj ancien|libre|
|[tfBczpCPNVnRE8pJ.htm](fists-of-the-ruby-phoenix-bestiary/tfBczpCPNVnRE8pJ.htm)|Yoh Souran|Yoh Souran|libre|
|[TjmD89wG7qQ0tRXj.htm](fists-of-the-ruby-phoenix-bestiary/TjmD89wG7qQ0tRXj.htm)|Speakers to the Wind (Gnoll Cascade Bearer)|Orateurs du Vent (Tisseur de flots)|libre|
|[TqKFvTmJnhdclCgt.htm](fists-of-the-ruby-phoenix-bestiary/TqKFvTmJnhdclCgt.htm)|Ghost Monk|Moine fantôme|libre|
|[TTEcqxuMqzIdkX6q.htm](fists-of-the-ruby-phoenix-bestiary/TTEcqxuMqzIdkX6q.htm)|Troff Frostknuckles|Troff Poings-de-givre|libre|
|[tu64qeuTiIGHtvgR.htm](fists-of-the-ruby-phoenix-bestiary/tu64qeuTiIGHtvgR.htm)|Golarion's Finest (Brartork)|Les Champions de Golarion (Brartork)|libre|
|[Twt8z6nTPgYQ0mTv.htm](fists-of-the-ruby-phoenix-bestiary/Twt8z6nTPgYQ0mTv.htm)|Berberoka|Berbéroka|libre|
|[u3vdt2J02rw8RyRq.htm](fists-of-the-ruby-phoenix-bestiary/u3vdt2J02rw8RyRq.htm)|Yabin the Just (Level 9)|Yabin le Juste (Niveau 9)|libre|
|[UbXvVA3ZdSN6pj1j.htm](fists-of-the-ruby-phoenix-bestiary/UbXvVA3ZdSN6pj1j.htm)|Cloudsplitter|Fend-Nuage|libre|
|[uvBZm8b7Vtzn1U2d.htm](fists-of-the-ruby-phoenix-bestiary/uvBZm8b7Vtzn1U2d.htm)|Spinel Leviathan Syndara|Léviathan Syndara Spinelle|libre|
|[v85RucvwE8lGq7oY.htm](fists-of-the-ruby-phoenix-bestiary/v85RucvwE8lGq7oY.htm)|Manananggal|Manananggal|libre|
|[VEnxFwTAhyO03Yse.htm](fists-of-the-ruby-phoenix-bestiary/VEnxFwTAhyO03Yse.htm)|Inmyeonjo|Inmyeonjo|libre|
|[vf59nmtYaF0se9Ui.htm](fists-of-the-ruby-phoenix-bestiary/vf59nmtYaF0se9Ui.htm)|Dancing Night Parade|Parade nocturne dansante|libre|
|[VGJF0jWWnA4ljfZE.htm](fists-of-the-ruby-phoenix-bestiary/VGJF0jWWnA4ljfZE.htm)|Melodic Squall|Rafale mélodique|libre|
|[vgMFPC9SoC3eyNSs.htm](fists-of-the-ruby-phoenix-bestiary/vgMFPC9SoC3eyNSs.htm)|Maalya|Maalya|libre|
|[vjE1lUpklp4slNbd.htm](fists-of-the-ruby-phoenix-bestiary/vjE1lUpklp4slNbd.htm)|Planar Terra-cotta Squadron|Escadrille de soldats en terre cuite planaire|libre|
|[vjONecsFtSFpjNaN.htm](fists-of-the-ruby-phoenix-bestiary/vjONecsFtSFpjNaN.htm)|Hantu Denai|Hanty Denai|libre|
|[wfp1cV5xCdpElkoU.htm](fists-of-the-ruby-phoenix-bestiary/wfp1cV5xCdpElkoU.htm)|Yabin (White Serpent Form)|Yabi (Forme de serpent blanc)|libre|
|[wFswIWjySnPqVNVT.htm](fists-of-the-ruby-phoenix-bestiary/wFswIWjySnPqVNVT.htm)|Golarion's Finest (Paunnima)|Les Champions de Golarion (Paunnima)|libre|
|[WqkV7xd2KOmNQd67.htm](fists-of-the-ruby-phoenix-bestiary/WqkV7xd2KOmNQd67.htm)|Broken Rebus Attack|Attaque du rébus brisé|libre|
|[xBSLIoVsZ6I4LOZc.htm](fists-of-the-ruby-phoenix-bestiary/xBSLIoVsZ6I4LOZc.htm)|Arms of Balance (Pravan Majinapti)|Bras de l'équilibre (Pravan Majinapti)|libre|
|[XiO3tJlYmuZwOBaA.htm](fists-of-the-ruby-phoenix-bestiary/XiO3tJlYmuZwOBaA.htm)|Koto Zekora|Koto Zékora|libre|
|[xiQIw7iFjOizo3Fm.htm](fists-of-the-ruby-phoenix-bestiary/xiQIw7iFjOizo3Fm.htm)|Akila Stormheel|Akila Stormheel|libre|
|[XN2uksGMgndh1jSP.htm](fists-of-the-ruby-phoenix-bestiary/XN2uksGMgndh1jSP.htm)|Ran-to (Level 14)|Ran-to (Niveau 14)|libre|
|[Y1Px08o90xG5dWTq.htm](fists-of-the-ruby-phoenix-bestiary/Y1Px08o90xG5dWTq.htm)|Takatorra (Daitengu Form)|Takatorra (Forme Daitengu)|libre|
|[ybPcnDqULaojoU8a.htm](fists-of-the-ruby-phoenix-bestiary/ybPcnDqULaojoU8a.htm)|Drake Courser|Drake courvite|libre|
|[YCfpTifMs2minJrF.htm](fists-of-the-ruby-phoenix-bestiary/YCfpTifMs2minJrF.htm)|Mogaru's Breath|Souffle de Mogaru|libre|
|[Yg0lmfxyBSggOkUp.htm](fists-of-the-ruby-phoenix-bestiary/Yg0lmfxyBSggOkUp.htm)|Orochi|Orochi|libre|
|[YHSV9DfaqoaSggLi.htm](fists-of-the-ruby-phoenix-bestiary/YHSV9DfaqoaSggLi.htm)|Archery Specialist|Maître archer|libre|
|[yNqeqEULKA536K4r.htm](fists-of-the-ruby-phoenix-bestiary/yNqeqEULKA536K4r.htm)|Anugobu Wondercrafter|Créateur de merveilles anugobu|libre|
|[YVP3pM7jxY9Gyouy.htm](fists-of-the-ruby-phoenix-bestiary/YVP3pM7jxY9Gyouy.htm)|Caustic Monitor|Varan caustique|libre|
|[zFye7hJzzwgajovA.htm](fists-of-the-ruby-phoenix-bestiary/zFye7hJzzwgajovA.htm)|Halspin the Stung|Halspin le Piqué|libre|
|[zj3s5CUsTjRRiROd.htm](fists-of-the-ruby-phoenix-bestiary/zj3s5CUsTjRRiROd.htm)|Collapsing Structure|Effondrement de structure|libre|
|[zmE4irZj6a2WpB6f.htm](fists-of-the-ruby-phoenix-bestiary/zmE4irZj6a2WpB6f.htm)|Yarrika Mulandez|Yarrika Mulandez|libre|
|[zNOSSDaaCozimqaS.htm](fists-of-the-ruby-phoenix-bestiary/zNOSSDaaCozimqaS.htm)|Jaiban|Jaïban|libre|
|[ZoBq423Q9PMa3rDV.htm](fists-of-the-ruby-phoenix-bestiary/ZoBq423Q9PMa3rDV.htm)|Master Xun|Maître Xun|libre|
|[Zp6lrCF3JwpdfKyK.htm](fists-of-the-ruby-phoenix-bestiary/Zp6lrCF3JwpdfKyK.htm)|Tyrannosaurus Imperator|Tyrannosaure impérial|libre|
|[zpXQHbe037haBVmD.htm](fists-of-the-ruby-phoenix-bestiary/zpXQHbe037haBVmD.htm)|Rivka (Igroon)|Rivka (Igroon)|libre|
