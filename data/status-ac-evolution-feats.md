# État de la traduction (ac-evolution-feats)

 * **libre**: 34


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[00eOhkScQi1K5GZk.htm](ac-evolution-feats/00eOhkScQi1K5GZk.htm)|Resilient Shell (CC)|Carapace résistante (CC)|libre|
|[4iJgmh1XmMGdzSF8.htm](ac-evolution-feats/4iJgmh1XmMGdzSF8.htm)|Vibration Sense (CC)|Perception des vibrations (CC)|libre|
|[4mkzupImKqVs2JkW.htm](ac-evolution-feats/4mkzupImKqVs2JkW.htm)|Advanced Weaponry (CC)|Armement avancé (CC)|libre|
|[Bf1jBWTdKVppsLBC.htm](ac-evolution-feats/Bf1jBWTdKVppsLBC.htm)|Steed Form (CC)|Forme de monture (CC)|libre|
|[BRad9X7kjcTcj1mf.htm](ac-evolution-feats/BRad9X7kjcTcj1mf.htm)|Summoner Dedication Adjustment (CC)|Ajustement du Dévouement de conjurateur (CC)|libre|
|[CaZ36nx2kGFRr0ER.htm](ac-evolution-feats/CaZ36nx2kGFRr0ER.htm)|Burrowing Form (CC)|Forme fouisseuse (CC)|libre|
|[DzQ1melMWQPbbcyu.htm](ac-evolution-feats/DzQ1melMWQPbbcyu.htm)|Eidolon's Opportunity (CC)|Opportunité de l'eidolon (CC)|libre|
|[eF3CKuQwqXJXIQPL.htm](ac-evolution-feats/eF3CKuQwqXJXIQPL.htm)|Ever-Vigilant Senses (CC)|Sens toujours en éveil (CC)|libre|
|[eh6Kd7ozEyeRgd3l.htm](ac-evolution-feats/eh6Kd7ozEyeRgd3l.htm)|Ranged Combatant (CC)|Combattant à distance (CC)|libre|
|[en0ikSriHVja8kGM.htm](ac-evolution-feats/en0ikSriHVja8kGM.htm)|Magical Adept (CC)|Adepte magique (CC)|libre|
|[ezmJ2kq6N647wd13.htm](ac-evolution-feats/ezmJ2kq6N647wd13.htm)|Glider Form (CC)|Forme de planeur (CC)|libre|
|[iYt3zEgqDnVhnHVf.htm](ac-evolution-feats/iYt3zEgqDnVhnHVf.htm)|Blood Frenzy (CC)|Frénésie du sang (CC)|libre|
|[Ln8r5xj2kAs81uu1.htm](ac-evolution-feats/Ln8r5xj2kAs81uu1.htm)|Defend Summoner (CC)|Défendre le conjurateur (CC)|libre|
|[Ls6CaGfZZAlmY8F9.htm](ac-evolution-feats/Ls6CaGfZZAlmY8F9.htm)|Hulking Size (CC)|Taille considérable (CC)|libre|
|[m50Xh9RRYj2NxnX2.htm](ac-evolution-feats/m50Xh9RRYj2NxnX2.htm)|Phase Out (CC)|Déphasage (CC)|libre|
|[mz0Y50ZaV7hysmmJ.htm](ac-evolution-feats/mz0Y50ZaV7hysmmJ.htm)|Constricting Hold (CC)|Prise de constriction (CC)|libre|
|[OGTIinVaETSz7kdW.htm](ac-evolution-feats/OGTIinVaETSz7kdW.htm)|Eidolon's Wrath (CC)|Courroux de l'eidolon (CC)|libre|
|[OqWpydcRNgVlmm2k.htm](ac-evolution-feats/OqWpydcRNgVlmm2k.htm)|Bloodletting Claws (CC)|Griffes hémorragiques (CC)|libre|
|[prfy7faUtWFNeIXH.htm](ac-evolution-feats/prfy7faUtWFNeIXH.htm)|Dual Energy Heart (CC)|Coeur énergétique dual (CC)|libre|
|[RBvfnbDcvNDqAMgP.htm](ac-evolution-feats/RBvfnbDcvNDqAMgP.htm)|Alacritous Action (CC)|Action empressée (CC)|libre|
|[Rd1ESvt7UHwFUD6r.htm](ac-evolution-feats/Rd1ESvt7UHwFUD6r.htm)|Expanded Senses (CC)|Sens étendus (CC)|libre|
|[rhWFwue5eYVr5Omr.htm](ac-evolution-feats/rhWFwue5eYVr5Omr.htm)|Shrink Down (CC)|Rétrécir (CC)|libre|
|[Ryed1oD5pMqREk0j.htm](ac-evolution-feats/Ryed1oD5pMqREk0j.htm)|Amphibious Form (CC)|Forme amphibie (CC)|libre|
|[sbzX5d0kDO9IXinm.htm](ac-evolution-feats/sbzX5d0kDO9IXinm.htm)|Grasping Limbs (CC)|Membres agrippants (CC)|libre|
|[TtoKz5zWdEtjydlk.htm](ac-evolution-feats/TtoKz5zWdEtjydlk.htm)|Weighty Impact (CC)|Impact pesant (CC)|libre|
|[V4rqSvDeIlZCdbVQ.htm](ac-evolution-feats/V4rqSvDeIlZCdbVQ.htm)|Magical Understudy (CC)|Doublure magique (CC)|libre|
|[val0CdVJxuHkpgFG.htm](ac-evolution-feats/val0CdVJxuHkpgFG.htm)|Energy Heart (CC)|Coeur énergétique (CC)|libre|
|[VUaI1q3MqoJqo5o5.htm](ac-evolution-feats/VUaI1q3MqoJqo5o5.htm)|Towering Size (CC)|Taille imposante (CC)|libre|
|[VzVKCWACGqnKaVbv.htm](ac-evolution-feats/VzVKCWACGqnKaVbv.htm)|Trample (CC)|Piétiner (CC)|libre|
|[wifAadJvu5dKvMVm.htm](ac-evolution-feats/wifAadJvu5dKvMVm.htm)|Spell-Repelling Form (CC)|Forme repousse-sort (CC)|libre|
|[wOtKDptwbz9hU9ZR.htm](ac-evolution-feats/wOtKDptwbz9hU9ZR.htm)|Energy Resistance (CC)|Résistance à l'énergie (CC)|libre|
|[X4XOW6G0Z22QniKa.htm](ac-evolution-feats/X4XOW6G0Z22QniKa.htm)|Pushing Attack (CC)|Attaque bousculante (CC)|libre|
|[yCSsQaTFINIbr9Gh.htm](ac-evolution-feats/yCSsQaTFINIbr9Gh.htm)|Magical Master (CC)|Maître magique (CC)|libre|
|[Zn7DZ9xfADjtOwst.htm](ac-evolution-feats/Zn7DZ9xfADjtOwst.htm)|Miniaturize (CC)|Miniaturiser (CC)|libre|
