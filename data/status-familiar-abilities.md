# État de la traduction (familiar-abilities)

 * **libre**: 101
 * **officielle**: 10
 * **changé**: 1


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[SKIS1xexaOvrecdV.htm](familiar-abilities/SKIS1xexaOvrecdV.htm)|Verdant Burst|Explosion verdoyante|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0R18Nd0CotPo9KD7.htm](familiar-abilities/0R18Nd0CotPo9KD7.htm)|Lightning Needles|Aiguilles de foudre|libre|
|[0rq3ufpxB9iHlIkd.htm](familiar-abilities/0rq3ufpxB9iHlIkd.htm)|Versatile Form|Forme polyvalente|libre|
|[0Xrkk46IM43iI1Fv.htm](familiar-abilities/0Xrkk46IM43iI1Fv.htm)|Darkvision|Vision dans le noir|officielle|
|[1fRkeUkPJFiBJ5u6.htm](familiar-abilities/1fRkeUkPJFiBJ5u6.htm)|Mass-Produced|Produit en masse|libre|
|[2fiQzoEKu6YUnrU9.htm](familiar-abilities/2fiQzoEKu6YUnrU9.htm)|Independent|Indépendant|libre|
|[2GMAPffXVayRFsLM.htm](familiar-abilities/2GMAPffXVayRFsLM.htm)|Seal-Bearer|Porteur de sceau|libre|
|[3y6GGXQgyJ4Hq4Yt.htm](familiar-abilities/3y6GGXQgyJ4Hq4Yt.htm)|Radiant|Radieux|libre|
|[43xB5UnexISlfRa5.htm](familiar-abilities/43xB5UnexISlfRa5.htm)|Purify Air|Purificateur d'air|libre|
|[5019hvUZcNe9dLEO.htm](familiar-abilities/5019hvUZcNe9dLEO.htm)|Elemental Familiar (Fire)|Familier élémentaire (feu)|libre|
|[57b8u8s3fV0UCrgJ.htm](familiar-abilities/57b8u8s3fV0UCrgJ.htm)|Plant Form|Forme de plante|libre|
|[5eqcXtPsYunphMOZ.htm](familiar-abilities/5eqcXtPsYunphMOZ.htm)|Construct|Créature artificielle|libre|
|[6cwswBPHm1GWdx5j.htm](familiar-abilities/6cwswBPHm1GWdx5j.htm)|Flatten|Applati|libre|
|[6ZmgkMHUGGb1mDhr.htm](familiar-abilities/6ZmgkMHUGGb1mDhr.htm)|Familiar of Balanced Luck|Familier de la chance équilibrée|libre|
|[7QosmRHlyLLhU1hX.htm](familiar-abilities/7QosmRHlyLLhU1hX.htm)|Lab Assistant|Assistant de laboratoire|officielle|
|[7ZxPS0UU7pf7wjp0.htm](familiar-abilities/7ZxPS0UU7pf7wjp0.htm)|Ambassador|Ambassadeur|libre|
|[8Z1UkLEWkFWIjOF8.htm](familiar-abilities/8Z1UkLEWkFWIjOF8.htm)|Poison Reservoir|Réservoir de poison|libre|
|[92lgSEPFIDLvKOCF.htm](familiar-abilities/92lgSEPFIDLvKOCF.htm)|Accompanist|Accompagnateur|libre|
|[9PsptrEoCC4QdM23.htm](familiar-abilities/9PsptrEoCC4QdM23.htm)|Valet|Valet|libre|
|[9xTjRt8LkZPB42Vi.htm](familiar-abilities/9xTjRt8LkZPB42Vi.htm)|Familiar of Freezing Rime|Familier de glace verglacée|libre|
|[9ypX5ZMor6RMiFKv.htm](familiar-abilities/9ypX5ZMor6RMiFKv.htm)|Familiar of Flowing Script|Familier d'inscriptions fluides|libre|
|[A0C86V3MUECpX5jd.htm](familiar-abilities/A0C86V3MUECpX5jd.htm)|Amphibious|Amphibie|libre|
|[aEKA3YWekLhEhuV8.htm](familiar-abilities/aEKA3YWekLhEhuV8.htm)|Threat Display|Démonstration menaçante|libre|
|[Amzezp93MZckBYRZ.htm](familiar-abilities/Amzezp93MZckBYRZ.htm)|Wavesense|Perception des ondes|libre|
|[asOhEdyF8CWFbR96.htm](familiar-abilities/asOhEdyF8CWFbR96.htm)|Spellcasting|Incantateur|libre|
|[bhpa7DVdMsaNfodz.htm](familiar-abilities/bhpa7DVdMsaNfodz.htm)|Familiar of Paired Perplexity|Familier de perplexité dédoublée|libre|
|[BXssJhTJjKrfojwG.htm](familiar-abilities/BXssJhTJjKrfojwG.htm)|Fast Movement|Déplacement rapide|libre|
|[C16JgmeJJG249WXz.htm](familiar-abilities/C16JgmeJJG249WXz.htm)|Mask Freeze|Masque figé|officielle|
|[ch6nFcRJO1fm8B4D.htm](familiar-abilities/ch6nFcRJO1fm8B4D.htm)|Elemental|Élémentaire|libre|
|[cT5octWchU4gjrhP.htm](familiar-abilities/cT5octWchU4gjrhP.htm)|Manual Dexterity|Dextérité manuelle|libre|
|[cy7bdQqVANipyljS.htm](familiar-abilities/cy7bdQqVANipyljS.htm)|Erudite|Érudit|libre|
|[D0ltNUJnN7UjJpA1.htm](familiar-abilities/D0ltNUJnN7UjJpA1.htm)|Innate Surge|Déferlante innée|officielle|
|[deC1yIM2S5szGdzT.htm](familiar-abilities/deC1yIM2S5szGdzT.htm)|Lifelink|Lien vital|libre|
|[dpjf1CyMEILpJWyp.htm](familiar-abilities/dpjf1CyMEILpJWyp.htm)|Darkeater|Mangeur noir|libre|
|[dWTfO5WbLkD5mw2H.htm](familiar-abilities/dWTfO5WbLkD5mw2H.htm)|Climber|Grimpeur|officielle|
|[embGS7NjgydDw8KO.htm](familiar-abilities/embGS7NjgydDw8KO.htm)|Dragon|Dragon|libre|
|[FcQQLMAJMgOLjnSv.htm](familiar-abilities/FcQQLMAJMgOLjnSv.htm)|Resistance|Résistance|libre|
|[FeEts6s2ABVEi0H6.htm](familiar-abilities/FeEts6s2ABVEi0H6.htm)|Familiar of Stalking Night|Familier de la nuit de la traque|libre|
|[fhyQ6uTSvfErTzyE.htm](familiar-abilities/fhyQ6uTSvfErTzyE.htm)|Jet|Propulseur|libre|
|[FlRUb8U13Crj3NaA.htm](familiar-abilities/FlRUb8U13Crj3NaA.htm)|Scent|Odorat|libre|
|[fmsVItn94FeY5Q3X.htm](familiar-abilities/fmsVItn94FeY5Q3X.htm)|Snoop|Fouineur|libre|
|[Fouq5RMNS8P1CDzx.htm](familiar-abilities/Fouq5RMNS8P1CDzx.htm)|Familiar of Obscuring Snowfall|Familier de chute de neige réfléchissante|libre|
|[gPceRQqO847lvSnb.htm](familiar-abilities/gPceRQqO847lvSnb.htm)|Share Senses|Partage des sens|officielle|
|[gVPtUSEPZYGRIRyQ.htm](familiar-abilities/gVPtUSEPZYGRIRyQ.htm)|Kindling|Petit-bois|libre|
|[hMrxiUPHXKpKu1Ha.htm](familiar-abilities/hMrxiUPHXKpKu1Ha.htm)|Major Resistance|Résistance majeure|libre|
|[HNoR0D2sKQaDNh5Z.htm](familiar-abilities/HNoR0D2sKQaDNh5Z.htm)|Familiar of Bolstering Aid|Familier de l'apport d'appoint|libre|
|[iBZzOaRNodGtfMVJ.htm](familiar-abilities/iBZzOaRNodGtfMVJ.htm)|Absorb Familiar|Familier absorbé|libre|
|[j1qZiH50Bl2SJ8vT.htm](familiar-abilities/j1qZiH50Bl2SJ8vT.htm)|Shadow Step|Pas d'ombre|libre|
|[J5Wyh1EPmk848ivR.htm](familiar-abilities/J5Wyh1EPmk848ivR.htm)|Familiar of Parasitic Might|Familier de puissance parasitaire|libre|
|[j9vOSbF9kLibhSIf.htm](familiar-abilities/j9vOSbF9kLibhSIf.htm)|Second Opinion|Seconde opinion|libre|
|[jdlefpPcSCIe27vO.htm](familiar-abilities/jdlefpPcSCIe27vO.htm)|Familiar Focus|Focalisation du familier|officielle|
|[jevzf9JbJJibpqaI.htm](familiar-abilities/jevzf9JbJJibpqaI.htm)|Skilled|Compétent|libre|
|[jew52aXGdNyR4N5B.htm](familiar-abilities/jew52aXGdNyR4N5B.htm)|Fungus|Champignon|libre|
|[jfCmKk6pXcINVROk.htm](familiar-abilities/jfCmKk6pXcINVROk.htm)|Echolocation|Écholocalisation|libre|
|[jPvjeX1CsUZQSszu.htm](familiar-abilities/jPvjeX1CsUZQSszu.htm)|Synchronize Spirit|Esprit synchronisé|libre|
|[JRP2bdkdCdj2JDrq.htm](familiar-abilities/JRP2bdkdCdj2JDrq.htm)|Master's Form|Forme du maître|libre|
|[Le8UWr5BU8rV3iBf.htm](familiar-abilities/Le8UWr5BU8rV3iBf.htm)|Tough|Robuste|libre|
|[lLWkoGiNLNIEm5Sp.htm](familiar-abilities/lLWkoGiNLNIEm5Sp.htm)|Plant|Plante|libre|
|[lpyJAl5B4j2DC7mc.htm](familiar-abilities/lpyJAl5B4j2DC7mc.htm)|Gills|Branchies|libre|
|[LrDnat1DsGJoAiKv.htm](familiar-abilities/LrDnat1DsGJoAiKv.htm)|Tremorsense|Perception des vibrations|libre|
|[LUBS9csNNgRTui4p.htm](familiar-abilities/LUBS9csNNgRTui4p.htm)|Grasping Tendrils|Vrilles agrippantes|libre|
|[m8wjlYnTAAfddWyA.htm](familiar-abilities/m8wjlYnTAAfddWyA.htm)|Dazzling Show|Show éblouissant|libre|
|[MGcVYSWZvaoEAISV.htm](familiar-abilities/MGcVYSWZvaoEAISV.htm)|Soul Bond|Lien de l'âme|libre|
|[mIl4YjbssQ5AERWS.htm](familiar-abilities/mIl4YjbssQ5AERWS.htm)|Elemental Familiar (Water)|Familier élémentaire (eau)|libre|
|[mK3mAUWiRLZZYNdz.htm](familiar-abilities/mK3mAUWiRLZZYNdz.htm)|Damage Avoidance|Évitement des dégâts|libre|
|[mKmBgQmPmiLOlEvw.htm](familiar-abilities/mKmBgQmPmiLOlEvw.htm)|Augury|Augure|libre|
|[ModT5b0jvnaitzJH.htm](familiar-abilities/ModT5b0jvnaitzJH.htm)|Extra Alchemy|Alchimie supplémentaire|libre|
|[Ms8EVO2mNbr4Zp1i.htm](familiar-abilities/Ms8EVO2mNbr4Zp1i.htm)|Item Delivery|Livreur d'objet|libre|
|[mu9OL8v4rmr9311e.htm](familiar-abilities/mu9OL8v4rmr9311e.htm)|Familiar of Ongoing Misery|Familier de misère persistante|libre|
|[nLrXleY20sESisoW.htm](familiar-abilities/nLrXleY20sESisoW.htm)|Vina Song|Chanson vina|libre|
|[nrPl3Dz7fbnmas7T.htm](familiar-abilities/nrPl3Dz7fbnmas7T.htm)|Spirit Touch|Contact spirituel|libre|
|[o0fxDDUu2ZSWYDTr.htm](familiar-abilities/o0fxDDUu2ZSWYDTr.htm)|Soul Sight|Vision de l'âme|libre|
|[O0XJIvMGK36yg9TJ.htm](familiar-abilities/O0XJIvMGK36yg9TJ.htm)|Path of the Tempest|Chemin de la tempête|libre|
|[O5TIjqXAuta8iVSz.htm](familiar-abilities/O5TIjqXAuta8iVSz.htm)|Focused Rejuvenation|Récupération concentrée|libre|
|[onye3PN84lmca11D.htm](familiar-abilities/onye3PN84lmca11D.htm)|Familiar of Keen Senses|Familier des sens acérés|libre|
|[oOpppkBhLcfaKZN4.htm](familiar-abilities/oOpppkBhLcfaKZN4.htm)|Stunning Flare|Lueur éblouissante|libre|
|[ou91pzf9TlOnIjYn.htm](familiar-abilities/ou91pzf9TlOnIjYn.htm)|Luminous|Lumineux|libre|
|[pX73zPGaazVJ0lKO.htm](familiar-abilities/pX73zPGaazVJ0lKO.htm)|Play Dead|Faire le mort|libre|
|[Q42nrq9LwDdbeZM5.htm](familiar-abilities/Q42nrq9LwDdbeZM5.htm)|Restorative Familiar|Familier curatif|libre|
|[q9IXWngAOCpsXxsO.htm](familiar-abilities/q9IXWngAOCpsXxsO.htm)|Levitator|Léviteur|libre|
|[qTxH8mSOvc4PMzrP.htm](familiar-abilities/qTxH8mSOvc4PMzrP.htm)|Kinspeech|Parole des semblables|officielle|
|[ReIgBsaM95BTvHpN.htm](familiar-abilities/ReIgBsaM95BTvHpN.htm)|Medic|Médecin|libre|
|[REJfFezELjc5Gzsy.htm](familiar-abilities/REJfFezELjc5Gzsy.htm)|Recall Familiar|Rappel de familier|libre|
|[RIx6b1GvTEBh4ExW.htm](familiar-abilities/RIx6b1GvTEBh4ExW.htm)|Elemental Familiar (Air)|Familier élémentaire (air)|libre|
|[rs4Awf4k1e0Mj797.htm](familiar-abilities/rs4Awf4k1e0Mj797.htm)|Cantrip Connection|Lien avec les tours de magie|libre|
|[Rv1KTRioaZOWyQI6.htm](familiar-abilities/Rv1KTRioaZOWyQI6.htm)|Alchemical Gut|Entrailles alchimiques|libre|
|[SxWYVgqNMsq0OijU.htm](familiar-abilities/SxWYVgqNMsq0OijU.htm)|Fast Movement: Climb|Déplacement rapide : Escalade|libre|
|[t6nzeTpsTJDwAUFZ.htm](familiar-abilities/t6nzeTpsTJDwAUFZ.htm)|Familiar of Swarm's Heart|Familier du coeur de la nuée|libre|
|[tEWAHPPZULvPgHnT.htm](familiar-abilities/tEWAHPPZULvPgHnT.htm)|Tattoo Transformation|Transformation en tatouage|libre|
|[u4Sp4K4WOi461xo1.htm](familiar-abilities/u4Sp4K4WOi461xo1.htm)|Pot of Tea|Théïère|libre|
|[uFsWy6Ii1WxA0u0w.htm](familiar-abilities/uFsWy6Ii1WxA0u0w.htm)|Familiar of Nimble Flight|Familier du vol agile|libre|
|[uUrsZ4WvhjKjFjnt.htm](familiar-abilities/uUrsZ4WvhjKjFjnt.htm)|Toolbearer|Porte outils|libre|
|[uy15sDBuYNK48N3v.htm](familiar-abilities/uy15sDBuYNK48N3v.htm)|Burrower|Fouisseur|officielle|
|[v7zE3tKQb9G6PaYU.htm](familiar-abilities/v7zE3tKQb9G6PaYU.htm)|Partner in Crime|Associés dans le crime|libre|
|[VHQUZcjUxfC3GcJ9.htm](familiar-abilities/VHQUZcjUxfC3GcJ9.htm)|Fast Movement: Fly|Déplacement rapide : Vol|libre|
|[vpw2ReYdcyQBpdqn.htm](familiar-abilities/vpw2ReYdcyQBpdqn.htm)|Fast Movement: Swim|Déplacement rapide : Nage|libre|
|[wcFeaZ0TmoaLtpIQ.htm](familiar-abilities/wcFeaZ0TmoaLtpIQ.htm)|Animated|Animé|libre|
|[wOgvBymJOVQDSm1Q.htm](familiar-abilities/wOgvBymJOVQDSm1Q.htm)|Spell Delivery|Transfert de sort|libre|
|[Xanjwv4YU0CBnsMw.htm](familiar-abilities/Xanjwv4YU0CBnsMw.htm)|Spell Battery|Accumulateur de sort|libre|
|[XCqYnlVbLGqEGPeX.htm](familiar-abilities/XCqYnlVbLGqEGPeX.htm)|Touch Telepathy|Télépathie de contact|libre|
|[xH7lvZkYYAAuxR62.htm](familiar-abilities/xH7lvZkYYAAuxR62.htm)|Familiar of Overwhelming Tides|Familier des marées irrésistibles|libre|
|[xXMPZXcwQgKRAu38.htm](familiar-abilities/xXMPZXcwQgKRAu38.htm)|Familiar of Enticing Negotiation|Familier de la négociation alléchante|libre|
|[XYb2udoNGaE0M11Y.htm](familiar-abilities/XYb2udoNGaE0M11Y.htm)|Elemental Familiar (Wood)|Familier élémentaire (bois)|libre|
|[yiIE9iWx6OLd61Qs.htm](familiar-abilities/yiIE9iWx6OLd61Qs.htm)|Shadow Projection|Projection d'ombre|libre|
|[ZCze1FY3Rmnvd6Dx.htm](familiar-abilities/ZCze1FY3Rmnvd6Dx.htm)|Familiar of Restored Spirit|Familier d'esprit récupéré|libre|
|[ZHSzNt3NxkXbj1mI.htm](familiar-abilities/ZHSzNt3NxkXbj1mI.htm)|Flier|Volant|libre|
|[ZJdMyCjRvUTtg90u.htm](familiar-abilities/ZJdMyCjRvUTtg90u.htm)|Elemental Familiar (Metal)|Familier élémentaire (métal)|libre|
|[zKTL9y9et0oTHEYS.htm](familiar-abilities/zKTL9y9et0oTHEYS.htm)|Greater Resistance|Résistance supérieure|libre|
|[ZtKb89o1PPhwJ3Lx.htm](familiar-abilities/ZtKb89o1PPhwJ3Lx.htm)|Extra Vial|Fioles supplémentaires|libre|
|[ZV4J5YTZGrqh6cvY.htm](familiar-abilities/ZV4J5YTZGrqh6cvY.htm)|Elemental Familiar (Earth)|Familier élémentaire (terre)|libre|
|[zwWgHjcU0iZ6gUDY.htm](familiar-abilities/zwWgHjcU0iZ6gUDY.htm)|Lightning Armillary|Armillaire foudroyante|libre|
|[zyMRLQnFCQVpltiR.htm](familiar-abilities/zyMRLQnFCQVpltiR.htm)|Speech|Parole|officielle|
