# État de la traduction (other-effects)

 * **libre**: 48
 * **aucune**: 2
 * **changé**: 1


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[EpvyTaklBQAOr1eT.htm](other-effects/EpvyTaklBQAOr1eT.htm)|Effect: Disarm (Bonus)|
|[PuDS0DEq0CnaSIFV.htm](other-effects/PuDS0DEq0CnaSIFV.htm)|Effect: Disarm (Success)|

## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[TPbr1kErAAJKBi3V.htm](other-effects/TPbr1kErAAJKBi3V.htm)|Effect: Aquatic Combat|Effet : Combat aquatique|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0r8MznJe5UugViee.htm](other-effects/0r8MznJe5UugViee.htm)|Effect: -15-foot status penalty to your land Speed|Effet : Pénalité de -4,50 mètres à votre Vitesse au sol|libre|
|[1FrAlPeNseApVrX3.htm](other-effects/1FrAlPeNseApVrX3.htm)|Effect: Treat Poison|Effet : Soigner un empoisonnement|libre|
|[5gX3PZztMecmWxX9.htm](other-effects/5gX3PZztMecmWxX9.htm)|Effect: -2 circumstance penalty to attack rolls|Effet : pénalité de circonstances de -2 aux jets d'attaque|libre|
|[63ia9YXlCIX7PXyp.htm](other-effects/63ia9YXlCIX7PXyp.htm)|Effect: Called Foe|Effet : Ennemi désigné|libre|
|[9c93NfZpENofiGUp.htm](other-effects/9c93NfZpENofiGUp.htm)|Effect: Mounted|Effet : Monté|libre|
|[A81HpA9cBUgGRsqx.htm](other-effects/A81HpA9cBUgGRsqx.htm)|Effect: Catch your Breath|Effet : Reprends ton souffle|libre|
|[AHMUpMbaVkZ5A1KX.htm](other-effects/AHMUpMbaVkZ5A1KX.htm)|Effect: Aid|Effet : Aider|libre|
|[B09MScCT7fhtBW5S.htm](other-effects/B09MScCT7fhtBW5S.htm)|Effect: Rage and Fury|Effet : Rage et furie|libre|
|[BjkhK1nFUw3vNKg2.htm](other-effects/BjkhK1nFUw3vNKg2.htm)|Effect: -2 circumstance penalty to spell attack rolls and spell DC's|Effet : pénalité de circonstances de - 2 aux jets d'attaques des sorts et aux DD des sorts|libre|
|[Bjwlrq0mF64nZJjK.htm](other-effects/Bjwlrq0mF64nZJjK.htm)|Effect: Desperate Swing|Effet : Balancement désespéré|libre|
|[c6cwOc4Zri6QiqsR.htm](other-effects/c6cwOc4Zri6QiqsR.htm)|Effect: -2 circumstance penalty to checks and saving throws until healed|Effet : pénalité de circonstances de -2 aux tests et aux jets de sauvegarde jusqu'aux soins|libre|
|[Ck0MQP9og75AWix7.htm](other-effects/Ck0MQP9og75AWix7.htm)|Effect: -2 circumstance penalty to attack rolls until healed|Effet : pénalité de circonstances de -2 aux jets d'attaque jusqu'aux soins|libre|
|[datPIlarC3SudOSd.htm](other-effects/datPIlarC3SudOSd.htm)|Effect: Endure the Onslaught|Effet : Endurer l'assaut|libre|
|[EBE2Oid75GC4n5vG.htm](other-effects/EBE2Oid75GC4n5vG.htm)|Effect: Class Might|Effet : Puissance de classe|libre|
|[EMqGwUi3VMhCjTlF.htm](other-effects/EMqGwUi3VMhCjTlF.htm)|Effect: Scout|Effet : Éclaireur|libre|
|[fqk0ESqJACXqz21k.htm](other-effects/fqk0ESqJACXqz21k.htm)|Effect: -1 circumstance penalty to attack rolls until you score a critical hit|Effet : pénalité de circonstances de -1 aux jets d'attaque jusqu'à obtention d'un coup critique|libre|
|[HgWSyf7jULisWIlG.htm](other-effects/HgWSyf7jULisWIlG.htm)|Effect: Impossible Shot|Effet : Tir impossible|libre|
|[HLBhINtyRcHk6d7h.htm](other-effects/HLBhINtyRcHk6d7h.htm)|Effect: +2 circumstance bonus to attack rolls|Effet : bonus de circonstances de +2 aux jets d'attaque|libre|
|[HTwG0F0LDu5ihBUm.htm](other-effects/HTwG0F0LDu5ihBUm.htm)|Effect: -5-foot status penalty to your land Speed|Effet : pénalité de statut de -1,50 m à votre Vitesse au sol|libre|
|[i5WccD9u62QPRZI8.htm](other-effects/i5WccD9u62QPRZI8.htm)|Effect: -2 circumstance penalty to attack rolls made with this attack until healed|Effet : pénalité de circonstances de -2 aux jets d'attaque avec cette attaque jusqu'aux soins|libre|
|[I9lfZUiCwMiGogVi.htm](other-effects/I9lfZUiCwMiGogVi.htm)|Effect: Cover|Effet : Abri|libre|
|[IUvO6CatanKAgmNr.htm](other-effects/IUvO6CatanKAgmNr.htm)|Effect: +1 circumstance bonus to attack rolls for 3 rounds|Effet : bonus de circonstances de +1 aux jets d'attaque pendant 3 rounds|libre|
|[J3beTsJS9DBkJuVJ.htm](other-effects/J3beTsJS9DBkJuVJ.htm)|Effect: Aura of Protection|Effet : Aura de Protection|libre|
|[l9bMRAoKnFP8vb3D.htm](other-effects/l9bMRAoKnFP8vb3D.htm)|Effect: Strike True|Effet : Frappe ultime|libre|
|[lTwF7aCtQ6napqYu.htm](other-effects/lTwF7aCtQ6napqYu.htm)|Effect: +2 status bonus to attack rolls|Effet : bonus de statut de +2 aux jets d'attaque|libre|
|[NU5wAbJLZO6T86eB.htm](other-effects/NU5wAbJLZO6T86eB.htm)|Effect: -10-foot circumstance penalty to your land Speed|Effet - pénalité de circonstances de -3m à votre Vitesse|libre|
|[nwSZL6EgG8KarNOT.htm](other-effects/nwSZL6EgG8KarNOT.htm)|Effect: Spark of Courage|Effet : Étincelle de courage|libre|
|[NyUWMdcABrlVsSBy.htm](other-effects/NyUWMdcABrlVsSBy.htm)|Effect: Critical Moment|Effet : Instant critique|libre|
|[PaS2kwv4vueI4PC7.htm](other-effects/PaS2kwv4vueI4PC7.htm)|Effect: +2 status bonus to saving throws against spells for 1 min|Effet : bonus de statut de +2 aux jets de sauvegarde contre les sorts|libre|
|[pbKvPM4cnmGTkDiZ.htm](other-effects/pbKvPM4cnmGTkDiZ.htm)|Effect: +4 circumstance bonus to AC against your ranged attacks|Effet : bonus de circonstances de +4 à la CA contre vos attaques à distance|libre|
|[QGTMOcJ1Dq5bk0GG.htm](other-effects/QGTMOcJ1Dq5bk0GG.htm)|Effect: +2 circumstance bonus to AC|Effet : bonus de circonstances de +2 à la CA|libre|
|[QjvNcjLMaDJ0ig0D.htm](other-effects/QjvNcjLMaDJ0ig0D.htm)|Effect: Fluid Motion|Effet : Mouvement fluide|libre|
|[qKS1TilScX0SmesO.htm](other-effects/qKS1TilScX0SmesO.htm)|Effect: Ancestral Might|Effet : Puissance ancestrale|libre|
|[RcMZr0wSjrir1AnS.htm](other-effects/RcMZr0wSjrir1AnS.htm)|Effect: -5-foot circumstance penalty to your land Speed|Effet : pénalité de circonstances de -1,50 m à votre Vitesse|libre|
|[ScZGg91ri19H4IIc.htm](other-effects/ScZGg91ri19H4IIc.htm)|Effect: -10-foot circumstance penalty to all Speeds|Effet : pénalité de circonstances de -3 mètresà toutes les Vitesses|libre|
|[sq8lhVfn0pl5WJ4t.htm](other-effects/sq8lhVfn0pl5WJ4t.htm)|Effect: +2 status bonus to AC and all saving throws|Effet : bonus de statut de +2 à la CA et à tous les jets de sauvegarde|libre|
|[TGGdM7XnjQkkijK7.htm](other-effects/TGGdM7XnjQkkijK7.htm)|Effect: Treat Disease|Effet : Soigner une maladie|libre|
|[uwYGEeqvt8pwjhXa.htm](other-effects/uwYGEeqvt8pwjhXa.htm)|Effect: Dazzled until end of your next turn|Effet : Ébloui jusqu'à la fin de votre prochain tour|libre|
|[UZabiyynJWuUblMk.htm](other-effects/UZabiyynJWuUblMk.htm)|Effect: -2 circumstance penalty to attack rolls with this weapon|Effet : pénalité de circonstances de -2 aux jets d'attaque avec cette arme|libre|
|[VCSpuc3Tf3XWMkd3.htm](other-effects/VCSpuc3Tf3XWMkd3.htm)|Effect: Follow The Expert|Effet : Suivre l'expert|libre|
|[w0kwwWC7QTmytbVt.htm](other-effects/w0kwwWC7QTmytbVt.htm)|Effect: Daring Attempt|Effet : Tentative audacieuse|libre|
|[W2OF7VeLHqc7p3DO.htm](other-effects/W2OF7VeLHqc7p3DO.htm)|Effect: Deafened until end of your next turn|Effet : Sourd jusqu'à la fin de votre prochain tour|libre|
|[wHWWHkjDXmJl4Ia6.htm](other-effects/wHWWHkjDXmJl4Ia6.htm)|Effect: Adverse Subsist Situation|Effet : Situation de survie défavorable|libre|
|[WqgtMWgM9nUjwQiI.htm](other-effects/WqgtMWgM9nUjwQiI.htm)|Effect: Resistance 5 to all damage|Effet : contreà tous les dégâts|libre|
|[wVGSlZEGxsg1s8AZ.htm](other-effects/wVGSlZEGxsg1s8AZ.htm)|Effect: -2 circumstance penalty to ranged attacks|Effet : pénalité de circonstances de -2 aux attaques à distance|libre|
|[y1GwyXv7iOf8DhBg.htm](other-effects/y1GwyXv7iOf8DhBg.htm)|Effect: Off-Guard until end of your next turn|Effet : Pris au dépourvu jusqu'à la fin de votre prochain tour|libre|
|[zDV1wo2ytNTbyTB0.htm](other-effects/zDV1wo2ytNTbyTB0.htm)|Effect: -10-foot status penalty to your land Speed|Effet : pénalité de statut de -3 mètresà votre Vitesse|libre|
|[ZXUOdZqUW22OX3ge.htm](other-effects/ZXUOdZqUW22OX3ge.htm)|Effect: Avert Gaze|Effet : Détourner le regard|libre|
