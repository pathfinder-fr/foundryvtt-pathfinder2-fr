# État de la traduction (journals/pages-GMScreen)

 * **libre**: 54
 * **aucune**: 1


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[EdDpecb4EKATiYJH.htm](journals/pages-GMScreen/EdDpecb4EKATiYJH.htm)|Mythic Characters|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[09Jpj4cCKYClyAE7.htm](journals/pages-GMScreen/09Jpj4cCKYClyAE7.htm)|Duels|Duels|libre|
|[14gM2CIUCwZvxr3W.htm](journals/pages-GMScreen/14gM2CIUCwZvxr3W.htm)|Infiltration|Infiltration|libre|
|[4hahgIx6iZfRMpT8.htm](journals/pages-GMScreen/4hahgIx6iZfRMpT8.htm)|Simple DCs|DD simples|libre|
|[4UWkvpoPam213lQH.htm](journals/pages-GMScreen/4UWkvpoPam213lQH.htm)|Running the Game|Jouer la partie|libre|
|[5BusDmtal9AuGPL9.htm](journals/pages-GMScreen/5BusDmtal9AuGPL9.htm)|GM Screen|Écran du MJ|libre|
|[5rIVjk40flfrxU77.htm](journals/pages-GMScreen/5rIVjk40flfrxU77.htm)|Subsystems and Variant Rules|Sous-systèmes et variantes de règles|libre|
|[7sclmGmVRrNOjlNF.htm](journals/pages-GMScreen/7sclmGmVRrNOjlNF.htm)|Conditions|États|libre|
|[7zHsWW9q6YqiMIwy.htm](journals/pages-GMScreen/7zHsWW9q6YqiMIwy.htm)|DC Adjustments|Ajustements des DD|libre|
|[8gcp880pEWZ9VPnF.htm](journals/pages-GMScreen/8gcp880pEWZ9VPnF.htm)|Summon Trait|Trait convocation|libre|
|[8jKbVJi0VzlwbS3Q.htm](journals/pages-GMScreen/8jKbVJi0VzlwbS3Q.htm)|Traits|Traits|libre|
|[9e26SSQNYcyizEQ2.htm](journals/pages-GMScreen/9e26SSQNYcyizEQ2.htm)|Bonuses and Penalties|Bonus et Pénalités|libre|
|[a7RGk2IiPaC3bLkf.htm](journals/pages-GMScreen/a7RGk2IiPaC3bLkf.htm)|Player Screen|Écran du joueur|libre|
|[ARVOEu4oeL3BKEOH.htm](journals/pages-GMScreen/ARVOEu4oeL3BKEOH.htm)|Influence|Influence|libre|
|[CfYcefv7nOP0H5H6.htm](journals/pages-GMScreen/CfYcefv7nOP0H5H6.htm)|Size and Reach|taille et allonge|libre|
|[Dae8LHdXZuBv06Jk.htm](journals/pages-GMScreen/Dae8LHdXZuBv06Jk.htm)|Treasure|Trésor|libre|
|[DUOb8QrlC5t0adkL.htm](journals/pages-GMScreen/DUOb8QrlC5t0adkL.htm)|Travel Speed|Vitesse de voyage|libre|
|[E4Q7EiSG18sIvFwg.htm](journals/pages-GMScreen/E4Q7EiSG18sIvFwg.htm)|Counteracting|Contre|libre|
|[EBWqgmHdFUYBTD0a.htm](journals/pages-GMScreen/EBWqgmHdFUYBTD0a.htm)|Victory Points|Points de victoire|libre|
|[EgFKC3mPAUuccald.htm](journals/pages-GMScreen/EgFKC3mPAUuccald.htm)|Proficiency without Level|Maîtrise sans niveau|libre|
|[ga3hoHFR4l1dqEev.htm](journals/pages-GMScreen/ga3hoHFR4l1dqEev.htm)|Treat Wounds|Soigner les blessures|libre|
|[gGbJqPCTf6iJsJr6.htm](journals/pages-GMScreen/gGbJqPCTf6iJsJr6.htm)|Hexploration|Hexploration|libre|
|[GMRQwFnChvxIjIiP.htm](journals/pages-GMScreen/GMRQwFnChvxIjIiP.htm)|Reputation|Réputation|libre|
|[gP7twT1asY7rknRA.htm](journals/pages-GMScreen/gP7twT1asY7rknRA.htm)|Gradual Ability Boosts|Primes d'attributs graduels|libre|
|[GvJVd2Yj3Lk2zM6r.htm](journals/pages-GMScreen/GvJVd2Yj3Lk2zM6r.htm)|Automatic Bonus Progression|Progression du bonus automatique|libre|
|[HM9LqMsbqeRGhKo5.htm](journals/pages-GMScreen/HM9LqMsbqeRGhKo5.htm)|Structures and Force Open DCs|Structures et DD pour Ouvrir de force|libre|
|[HSg7h1jm5oASbXao.htm](journals/pages-GMScreen/HSg7h1jm5oASbXao.htm)|Chases|Poursuites|libre|
|[I5V4dZlNV1R6KOQM.htm](journals/pages-GMScreen/I5V4dZlNV1R6KOQM.htm)|Earn Income Tasks|Tâches pour gagner de l'argent|libre|
|[iufSTIPL6F63UTCv.htm](journals/pages-GMScreen/iufSTIPL6F63UTCv.htm)|Hero Points|Points d'héroïsme|libre|
|[jcRaFZEXoq7gtfBu.htm](journals/pages-GMScreen/jcRaFZEXoq7gtfBu.htm)|Monster Adjustments|Ajustements de monstre|libre|
|[KCtjphOgwTWcfBks.htm](journals/pages-GMScreen/KCtjphOgwTWcfBks.htm)|Bulk and Encumbered|Encombrement et surchargé|libre|
|[KdoLyU91c0oS2hTw.htm](journals/pages-GMScreen/KdoLyU91c0oS2hTw.htm)|Death, Dying and Unconscious|Mort, mourant et inconscient|libre|
|[l4hGoem1vtSEruPv.htm](journals/pages-GMScreen/l4hGoem1vtSEruPv.htm)|Playing the Game|Règles du jeu|libre|
|[lfqjXVxMhxKrzIJD.htm](journals/pages-GMScreen/lfqjXVxMhxKrzIJD.htm)|Cost of Living|Coût de la vie|libre|
|[LfRm7SNxoGyzBfOI.htm](journals/pages-GMScreen/LfRm7SNxoGyzBfOI.htm)|Free Archetype|Archétype gratuit|libre|
|[LkqoPLbd36O1aC7V.htm](journals/pages-GMScreen/LkqoPLbd36O1aC7V.htm)|Leadership|Commandement|libre|
|[lq9onil5mhl9UGQT.htm](journals/pages-GMScreen/lq9onil5mhl9UGQT.htm)|Detecting and Stealth with Other Senses|Détecter et Discrétion avec d'autres sens|libre|
|[nhYDtKvrdKi55HU2.htm](journals/pages-GMScreen/nhYDtKvrdKi55HU2.htm)|Skill Actions|Actions de compétence|libre|
|[O357iNXwHotbecK1.htm](journals/pages-GMScreen/O357iNXwHotbecK1.htm)|Resting|Repos|libre|
|[oEyx39yADr2OjfTz.htm](journals/pages-GMScreen/oEyx39yADr2OjfTz.htm)|Creature Identification|Identification de créature|libre|
|[oKTxJegLqJezOVEe.htm](journals/pages-GMScreen/oKTxJegLqJezOVEe.htm)|Environmental Damage|Dégâts environnementaux|libre|
|[or9dAZfXyd01A8dS.htm](journals/pages-GMScreen/or9dAZfXyd01A8dS.htm)|Downtime Events|Évènements d'intermède|libre|
|[pBS3DUjlzVuFgapv.htm](journals/pages-GMScreen/pBS3DUjlzVuFgapv.htm)|DCs by Level|DD par niveau|libre|
|[pW3Q267nqiRu4h38.htm](journals/pages-GMScreen/pW3Q267nqiRu4h38.htm)|Light|Lumière|libre|
|[q2chuQ4Am5puc9Cc.htm](journals/pages-GMScreen/q2chuQ4Am5puc9Cc.htm)|Senses|Sens|libre|
|[qbFZJybp6aFvj1VR.htm](journals/pages-GMScreen/qbFZJybp6aFvj1VR.htm)|Exploration Activities|Activités d'exploration|libre|
|[qGZwh7wge4Z5iR0a.htm](journals/pages-GMScreen/qGZwh7wge4Z5iR0a.htm)|Detecting Creatures|Détection des créatures|libre|
|[QnqlEgRJdEsrdM9q.htm](journals/pages-GMScreen/QnqlEgRJdEsrdM9q.htm)|Falling|Chuter|libre|
|[sXpFBQMoDx0PpUew.htm](journals/pages-GMScreen/sXpFBQMoDx0PpUew.htm)|Specific Skill DCs|DD de compétence spécifiques|libre|
|[WZgFEMDSP9TvYc4u.htm](journals/pages-GMScreen/WZgFEMDSP9TvYc4u.htm)|Encounter Budget|Budget de rencontre|libre|
|[XujUKJKdFfXSelmZ.htm](journals/pages-GMScreen/XujUKJKdFfXSelmZ.htm)|Stamina|Endurance|libre|
|[z68vgV2XWhi1KYEP.htm](journals/pages-GMScreen/z68vgV2XWhi1KYEP.htm)|Terrain and Cover|Terrain et abri|libre|
|[ZcddTU02joqYuuoV.htm](journals/pages-GMScreen/ZcddTU02joqYuuoV.htm)|XP Awards|Récompenses en PX|libre|
|[zkQJzslkiNQuUuKd.htm](journals/pages-GMScreen/zkQJzslkiNQuUuKd.htm)|Research|Recherche|libre|
|[ZrEmfrgDlmlhdvQg.htm](journals/pages-GMScreen/ZrEmfrgDlmlhdvQg.htm)|Basic Actions|Actions basiques|libre|
