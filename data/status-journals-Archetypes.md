# État de la traduction (journals/pages-Archetypes)

 * **changé**: 153
 * **libre**: 26
 * **aucune**: 38


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[8gUHChhAFihZP5M4.htm](journals/pages-Archetypes/8gUHChhAFihZP5M4.htm)|Broken Chain (Uncommon)|
|[AJQCccOtbPq1dIWY.htm](journals/pages-Archetypes/AJQCccOtbPq1dIWY.htm)|Godling (Rare)|
|[Aka7A3c28QqBeRxU.htm](journals/pages-Archetypes/Aka7A3c28QqBeRxU.htm)|Vindicator (Class Archetype)|
|[bi4WaRQzrOwuEILM.htm](journals/pages-Archetypes/bi4WaRQzrOwuEILM.htm)|Familiar Sage|
|[bT2NuSZ1h02PFhpJ.htm](journals/pages-Archetypes/bT2NuSZ1h02PFhpJ.htm)|Spirit Warrior|
|[ceqrGkjnFSG5edH3.htm](journals/pages-Archetypes/ceqrGkjnFSG5edH3.htm)|Battle Harbinger (Class Archetype)|
|[CLx4mTCUxUlPT7Zh.htm](journals/pages-Archetypes/CLx4mTCUxUlPT7Zh.htm)|Ostilli Host|
|[cSNtXyfkQ7g1KbL3.htm](journals/pages-Archetypes/cSNtXyfkQ7g1KbL3.htm)|Apocalypse Rider (Uncommon)|
|[CyU3RtrSs4Ssp0DR.htm](journals/pages-Archetypes/CyU3RtrSs4Ssp0DR.htm)|Ursine Avenger Hood|
|[DoPWEGMjKIE2wwRr.htm](journals/pages-Archetypes/DoPWEGMjKIE2wwRr.htm)|Archfiend (Uncommon)|
|[fLtbqWjffjM40o1U.htm](journals/pages-Archetypes/fLtbqWjffjM40o1U.htm)|Wildspell (Uncommon)|
|[HgQMFkvMik43f47C.htm](journals/pages-Archetypes/HgQMFkvMik43f47C.htm)|Avenger (Class Archetype)|
|[hK0lWWhPqiplWuRl.htm](journals/pages-Archetypes/hK0lWWhPqiplWuRl.htm)|Thlipit Contestant|
|[IBJrhr3Sj6CSIBWo.htm](journals/pages-Archetypes/IBJrhr3Sj6CSIBWo.htm)|Exemplar|
|[If0rK2DhKh2CrYKR.htm](journals/pages-Archetypes/If0rK2DhKh2CrYKR.htm)|Swarmkeeper|
|[Jfe0vnhKyyxOGMdl.htm](journals/pages-Archetypes/Jfe0vnhKyyxOGMdl.htm)|Razmiran Priest|
|[jnx6oWfmhuKetO2j.htm](journals/pages-Archetypes/jnx6oWfmhuKetO2j.htm)|Palatine Detective (Class Archetype)|
|[kWN0nFzOq3VvONC5.htm](journals/pages-Archetypes/kWN0nFzOq3VvONC5.htm)|Wandering Chef|
|[l4isOHQIdkrWLch0.htm](journals/pages-Archetypes/l4isOHQIdkrWLch0.htm)|Wild Mimic|
|[lCmFR1N4lCMOYXcE.htm](journals/pages-Archetypes/lCmFR1N4lCMOYXcE.htm)|Five-Breath Vanguard|
|[mozFKLZrgEO3F7sE.htm](journals/pages-Archetypes/mozFKLZrgEO3F7sE.htm)|Rivethun Emissary|
|[MsJGLtT0GScL8cI8.htm](journals/pages-Archetypes/MsJGLtT0GScL8cI8.htm)|Eternal Legend (Uncommon)|
|[n787ifCmIxuhtcMG.htm](journals/pages-Archetypes/n787ifCmIxuhtcMG.htm)|Mortal Herald|
|[qCtMGydkSVksIFOd.htm](journals/pages-Archetypes/qCtMGydkSVksIFOd.htm)|Prophesied Monarch (Uncommon)|
|[RIOLY9LaX3qOnujl.htm](journals/pages-Archetypes/RIOLY9LaX3qOnujl.htm)|Cultivator|
|[RK6AbtT26DHbHlLj.htm](journals/pages-Archetypes/RK6AbtT26DHbHlLj.htm)|Fan Dancer|
|[RkKrtZMb8Ifrrs2l.htm](journals/pages-Archetypes/RkKrtZMb8Ifrrs2l.htm)|Clawdancer|
|[ssd83GMzktmP3ZEC.htm](journals/pages-Archetypes/ssd83GMzktmP3ZEC.htm)|Shieldmarshal|
|[Svy85xtvHZaSsGaI.htm](journals/pages-Archetypes/Svy85xtvHZaSsGaI.htm)|Rivethun Invoker|
|[uewu2vWQluT8Jyf1.htm](journals/pages-Archetypes/uewu2vWQluT8Jyf1.htm)|Winged Warrior|
|[V87pMN1WuIHaoYpQ.htm](journals/pages-Archetypes/V87pMN1WuIHaoYpQ.htm)|Bloodrager (Class Archetype)|
|[wC2H1yHQg1zu18Bh.htm](journals/pages-Archetypes/wC2H1yHQg1zu18Bh.htm)|Stonebound|
|[WVIZIfaDTNe9YjVo.htm](journals/pages-Archetypes/WVIZIfaDTNe9YjVo.htm)|Gelid Shard|
|[xMsfhTWwWeRbxDzn.htm](journals/pages-Archetypes/xMsfhTWwWeRbxDzn.htm)|Ascended Celestial (Uncommon)|
|[YbGa7Rdxh2iQxPfG.htm](journals/pages-Archetypes/YbGa7Rdxh2iQxPfG.htm)|Stone Brawler|
|[YPPTeVkczEiT8P0Y.htm](journals/pages-Archetypes/YPPTeVkczEiT8P0Y.htm)|Beast Lord (Uncommon)|
|[Zct5tHOoQGwwx7Ia.htm](journals/pages-Archetypes/Zct5tHOoQGwwx7Ia.htm)|Werecreature|
|[ZNz2aC4wQ7KC6Ixe.htm](journals/pages-Archetypes/ZNz2aC4wQ7KC6Ixe.htm)|Animist|

## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[09VwtSsYldMkVzU1.htm](journals/pages-Archetypes/09VwtSsYldMkVzU1.htm)|Runelord (Class Archetype)|Seigneur des runes (Archétype de classe)|changé|
|[0UrqPv7XLDDRwZ13.htm](journals/pages-Archetypes/0UrqPv7XLDDRwZ13.htm)|Pactbinder|Pactiseur|changé|
|[0ZXiLwvBG20MzkO6.htm](journals/pages-Archetypes/0ZXiLwvBG20MzkO6.htm)|Scrollmaster|Maître des parchemins|changé|
|[1F84oh5hoG86mZIw.htm](journals/pages-Archetypes/1F84oh5hoG86mZIw.htm)|Marshal|Capitaine|changé|
|[1kRGIb5LPPKypQpN.htm](journals/pages-Archetypes/1kRGIb5LPPKypQpN.htm)|Kineticist|Kinétiste|changé|
|[2CbiV0VtDKZfQNte.htm](journals/pages-Archetypes/2CbiV0VtDKZfQNte.htm)|Viking|Viking|changé|
|[3B1PqqSZYQj9YXac.htm](journals/pages-Archetypes/3B1PqqSZYQj9YXac.htm)|Hallowed Necromancer|Nécromant sacré|changé|
|[4gKrDFB1GlILn9la.htm](journals/pages-Archetypes/4gKrDFB1GlILn9la.htm)|Scroll Trickster|Usurpateur de parchemins|changé|
|[4VMzG36Pm1oivS6Y.htm](journals/pages-Archetypes/4VMzG36Pm1oivS6Y.htm)|Beastmaster|Maître des bêtes|changé|
|[4ZPZqbe0Ai8ZIfcp.htm](journals/pages-Archetypes/4ZPZqbe0Ai8ZIfcp.htm)|Golden League Xun|Xun de la Ligue dorée|changé|
|[57rElfZ9sWKFQ8Ep.htm](journals/pages-Archetypes/57rElfZ9sWKFQ8Ep.htm)|Knight Vigilant|Chevalier vigilant|changé|
|[5nd8ON2kFGOsCdwQ.htm](journals/pages-Archetypes/5nd8ON2kFGOsCdwQ.htm)|Bullet Dancer|Danseur de balle|changé|
|[5RH1CkZQHYpsOium.htm](journals/pages-Archetypes/5RH1CkZQHYpsOium.htm)|Edgewatch Detective|Officier de la garde du précipice|changé|
|[5v7k1XWQxaP0DoGX.htm](journals/pages-Archetypes/5v7k1XWQxaP0DoGX.htm)|Monk|Moine|changé|
|[6A9zBmTyt8cn6ioa.htm](journals/pages-Archetypes/6A9zBmTyt8cn6ioa.htm)|Eldritch Archer|Archer mystique|changé|
|[6DgCLFEGEHvqmy7Y.htm](journals/pages-Archetypes/6DgCLFEGEHvqmy7Y.htm)|Clockwork Reanimator|Réanimateur nécromécanicien|changé|
|[6KciIDJV6ZdJcAVa.htm](journals/pages-Archetypes/6KciIDJV6ZdJcAVa.htm)|Sterling Dynamo|Dynamo sterling|changé|
|[8RYKz1WDPMJBmMNt.htm](journals/pages-Archetypes/8RYKz1WDPMJBmMNt.htm)|Game Hunter|Chasseur de gros gibier|changé|
|[9g14DjBZNj27goTt.htm](journals/pages-Archetypes/9g14DjBZNj27goTt.htm)|Soul Warden|Gardien de l'âme|changé|
|[A5HwvubNii1d2UND.htm](journals/pages-Archetypes/A5HwvubNii1d2UND.htm)|Sniping Duo|Duo de tireur d'élite|changé|
|[aAm6jY2k5qBuWETd.htm](journals/pages-Archetypes/aAm6jY2k5qBuWETd.htm)|Inventor|Inventeur|changé|
|[ackdHdIAmSb0AiVL.htm](journals/pages-Archetypes/ackdHdIAmSb0AiVL.htm)|Loremaster|Maître savant|changé|
|[aIpIUbupTjw2863C.htm](journals/pages-Archetypes/aIpIUbupTjw2863C.htm)|Ghost|Fantôme|changé|
|[ANj1KgIvIvNDmBSR.htm](journals/pages-Archetypes/ANj1KgIvIvNDmBSR.htm)|Verduran Shadow|Ombre de verduran|changé|
|[ARYrFIOsT3JxpjDY.htm](journals/pages-Archetypes/ARYrFIOsT3JxpjDY.htm)|Hellknight Armiger|Écuyer des chevaliers infernaux|changé|
|[b5VoSJNzoNuqbtvD.htm](journals/pages-Archetypes/b5VoSJNzoNuqbtvD.htm)|Summoner|Conjurateur|changé|
|[b7oePIMEMdFFS5TH.htm](journals/pages-Archetypes/b7oePIMEMdFFS5TH.htm)|Captivator|Enjôleur|changé|
|[bKoV037XO3qiakGw.htm](journals/pages-Archetypes/bKoV037XO3qiakGw.htm)|Ghoul|Goule|changé|
|[bkTCYlTFNifrM3sh.htm](journals/pages-Archetypes/bkTCYlTFNifrM3sh.htm)|Artillerist|Artilleur|changé|
|[bL5cccuvnP5h7Bnn.htm](journals/pages-Archetypes/bL5cccuvnP5h7Bnn.htm)|Animal Trainer|Dompteur|changé|
|[bQeLZt29NnHEn6fh.htm](journals/pages-Archetypes/bQeLZt29NnHEn6fh.htm)|Juggler|Jongleur|changé|
|[BRxlFHXu8tN14TDI.htm](journals/pages-Archetypes/BRxlFHXu8tN14TDI.htm)|Staff Acrobat|Funambule|changé|
|[BwPUY2X7TSmlJj5c.htm](journals/pages-Archetypes/BwPUY2X7TSmlJj5c.htm)|Duelist|Duelliste|changé|
|[C3P0TycdFCN02p9u.htm](journals/pages-Archetypes/C3P0TycdFCN02p9u.htm)|Bastion|Bastion|changé|
|[CB9YE4P7A2Wty1IX.htm](journals/pages-Archetypes/CB9YE4P7A2Wty1IX.htm)|Red Mantis Assassin|Assassin des mantes rouges|changé|
|[cBBwTnJrVmvaRMYq.htm](journals/pages-Archetypes/cBBwTnJrVmvaRMYq.htm)|Aldori Duelist|Duelliste Aldori|changé|
|[cdnkS3A8OpOnRjKu.htm](journals/pages-Archetypes/cdnkS3A8OpOnRjKu.htm)|Bellflower Tiller|Laboureur de la Campanule|changé|
|[CMgYob7Cy4meoQKg.htm](journals/pages-Archetypes/CMgYob7Cy4meoQKg.htm)|Ranger|Rôdeur|changé|
|[COTDddn4psjIoWry.htm](journals/pages-Archetypes/COTDddn4psjIoWry.htm)|Lion Blade|Lame du lion|changé|
|[CSVoyUvynmM5LzPW.htm](journals/pages-Archetypes/CSVoyUvynmM5LzPW.htm)|Gunslinger|Franc tireur|changé|
|[DAVBjDSysgXgtVQu.htm](journals/pages-Archetypes/DAVBjDSysgXgtVQu.htm)|Gray Gardener|Jardinier gris|changé|
|[DJiYP5tFBrBMD0We.htm](journals/pages-Archetypes/DJiYP5tFBrBMD0We.htm)|Sorcerer|Ensorceleur|changé|
|[drgCQcXZbIJU0Zhw.htm](journals/pages-Archetypes/drgCQcXZbIJU0Zhw.htm)|Wrestler|Lutteur|changé|
|[DXtjeVaWNB8zSjpA.htm](journals/pages-Archetypes/DXtjeVaWNB8zSjpA.htm)|Champion|Champion|changé|
|[DztQF2FexWvnzcaE.htm](journals/pages-Archetypes/DztQF2FexWvnzcaE.htm)|Spell Trickster|Mystificateur de sort|changé|
|[eLc6TSgWykjtSeuF.htm](journals/pages-Archetypes/eLc6TSgWykjtSeuF.htm)|Magaambyan Attendant|Gardien du Magaambya|changé|
|[eNTStXeuABNPSLjw.htm](journals/pages-Archetypes/eNTStXeuABNPSLjw.htm)|Witch|Sorcier|changé|
|[ePfibeHcnRcjO6lC.htm](journals/pages-Archetypes/ePfibeHcnRcjO6lC.htm)|Reanimator|Réanimateur|changé|
|[EWiUv3UiR3RHSGlA.htm](journals/pages-Archetypes/EWiUv3UiR3RHSGlA.htm)|Cathartic Mage|Mage cathartique|changé|
|[EwlYs1OzaMj9BB5I.htm](journals/pages-Archetypes/EwlYs1OzaMj9BB5I.htm)|Student of Perfection|Étudiant de la perfection|changé|
|[F5gBiVGbS8YSgPSI.htm](journals/pages-Archetypes/F5gBiVGbS8YSgPSI.htm)|Sixth Pillar|Sixième pilier|changé|
|[f7tFLR7aFonXTLQa.htm](journals/pages-Archetypes/f7tFLR7aFonXTLQa.htm)|Undead Master|Maître des morts-vivants|changé|
|[F7W15pGOeSeNJD0C.htm](journals/pages-Archetypes/F7W15pGOeSeNJD0C.htm)|Firebrand Braggart|Agitateur vantard|changé|
|[Fea8ZereQhNolDoP.htm](journals/pages-Archetypes/Fea8ZereQhNolDoP.htm)|Bounty Hunter|Chasseur de primes|changé|
|[FJGPBhYmD7xTFsrW.htm](journals/pages-Archetypes/FJGPBhYmD7xTFsrW.htm)|Spellshot (Class Archetype)|Sortiléro (Archétype de classe)|changé|
|[fTxZjmNLckRwZ4Po.htm](journals/pages-Archetypes/fTxZjmNLckRwZ4Po.htm)|Pathfinder Agent|Agent des Éclaireurs|changé|
|[G38usCLuTDmFYw7V.htm](journals/pages-Archetypes/G38usCLuTDmFYw7V.htm)|Zombie|Zombie|changé|
|[G3AtnAkIKNHrahmd.htm](journals/pages-Archetypes/G3AtnAkIKNHrahmd.htm)|Vampire|Vampire|changé|
|[G9Fzy5ZK4KtAmcFb.htm](journals/pages-Archetypes/G9Fzy5ZK4KtAmcFb.htm)|Snarecrafter|Fabricant de pièges artisanaux|changé|
|[gbBf6x89m4SEFpsL.htm](journals/pages-Archetypes/gbBf6x89m4SEFpsL.htm)|Druid|Druide|changé|
|[gfhnRp2TEy9JCfHI.htm](journals/pages-Archetypes/gfhnRp2TEy9JCfHI.htm)|Gladiator|Gladiateur|changé|
|[h1zdUIWX1k5PKTnc.htm](journals/pages-Archetypes/h1zdUIWX1k5PKTnc.htm)|Celebrity|Célébrité|changé|
|[HDTW0vr8QL20M5ND.htm](journals/pages-Archetypes/HDTW0vr8QL20M5ND.htm)|Weapon Improviser|Improvisateur d'armes|changé|
|[HvbDEgCsLbzuMRiR.htm](journals/pages-Archetypes/HvbDEgCsLbzuMRiR.htm)|Poisoner|Empoisonneur|changé|
|[HZHzpATOyhTjUDrR.htm](journals/pages-Archetypes/HZHzpATOyhTjUDrR.htm)|Alter Ego|Imposteur|changé|
|[I6FbkKYncDuu7eWq.htm](journals/pages-Archetypes/I6FbkKYncDuu7eWq.htm)|Geomancer|Géomancien|changé|
|[iH3Vrt35DyUbhxZj.htm](journals/pages-Archetypes/iH3Vrt35DyUbhxZj.htm)|Mammoth Lord|Seigneur des mammouths|changé|
|[inxtk4rYj2UZaytg.htm](journals/pages-Archetypes/inxtk4rYj2UZaytg.htm)|Fighter|Guerrier|changé|
|[J0AfK0jJgS5WxhCE.htm](journals/pages-Archetypes/J0AfK0jJgS5WxhCE.htm)|Scions of Domora|Scion de Domora|changé|
|[j5uZbCwoHOOEO1bC.htm](journals/pages-Archetypes/j5uZbCwoHOOEO1bC.htm)|Runescarred|Scarifié de runes|changé|
|[jFhTp57zO3ej6HDt.htm](journals/pages-Archetypes/jFhTp57zO3ej6HDt.htm)|Corpse Tender|Éleveur de cadavres|changé|
|[jTiXRtNNvMOnsg98.htm](journals/pages-Archetypes/jTiXRtNNvMOnsg98.htm)|Beast Gunner|Bestioléro|changé|
|[JzUDumJ3Dlyame4z.htm](journals/pages-Archetypes/JzUDumJ3Dlyame4z.htm)|Drow Shootist|Tireur drow|changé|
|[k9Ebp52kt0ZLHtMl.htm](journals/pages-Archetypes/k9Ebp52kt0ZLHtMl.htm)|Swashbuckler|Bretteur|changé|
|[K9Krytj8OtUvQxoc.htm](journals/pages-Archetypes/K9Krytj8OtUvQxoc.htm)|Thaumaturge|Thaumaturge|changé|
|[KORSADviZaSccs2W.htm](journals/pages-Archetypes/KORSADviZaSccs2W.htm)|Zephyr Guard|Garde zéphyr|changé|
|[KRUWPDzuwG0LC26d.htm](journals/pages-Archetypes/KRUWPDzuwG0LC26d.htm)|Curse Maelstrom|Maelström maudit|changé|
|[l76UxUsfrsPnP9U9.htm](journals/pages-Archetypes/l76UxUsfrsPnP9U9.htm)|Worm Caller|Invocateur de vers|changé|
|[lc3iBCamQ8jvr9dp.htm](journals/pages-Archetypes/lc3iBCamQ8jvr9dp.htm)|Trapsmith|Fabricant de pièges|changé|
|[LgkfmZnFP3TWtkuy.htm](journals/pages-Archetypes/LgkfmZnFP3TWtkuy.htm)|Living Vessel|Réceptacle vivant|changé|
|[LL4YD16kPqcGibKG.htm](journals/pages-Archetypes/LL4YD16kPqcGibKG.htm)|Ghost Hunter|Chasseur de fantômes|changé|
|[LWgcWM8B85HHVCtZ.htm](journals/pages-Archetypes/LWgcWM8B85HHVCtZ.htm)|Soulforger|Mentallurgiste|changé|
|[M5QOocGyP4zkMo9m.htm](journals/pages-Archetypes/M5QOocGyP4zkMo9m.htm)|Horizon Walker|Arpenteur d'horizon|changé|
|[Mk7ECwe1a971WyWl.htm](journals/pages-Archetypes/Mk7ECwe1a971WyWl.htm)|Demolitionist|Démolisseur|changé|
|[MKU4d2hIpLuXGN2J.htm](journals/pages-Archetypes/MKU4d2hIpLuXGN2J.htm)|Shadowdancer|Danseur de l'ombre|changé|
|[mmB3EkkdCpLke7Lk.htm](journals/pages-Archetypes/mmB3EkkdCpLke7Lk.htm)|Investigator|Enquêteur|changé|
|[mtc5frNxUHORGscz.htm](journals/pages-Archetypes/mtc5frNxUHORGscz.htm)|Linguist|Linguiste|changé|
|[mV0K9sxD3TWnZDcy.htm](journals/pages-Archetypes/mV0K9sxD3TWnZDcy.htm)|Undead Slayer|Tueur de mort-vivant|changé|
|[mXywYJJCM9IVItZz.htm](journals/pages-Archetypes/mXywYJJCM9IVItZz.htm)|Mind Smith|Façonneur spirituel|changé|
|[N01RLMQPaSeoHEuL.htm](journals/pages-Archetypes/N01RLMQPaSeoHEuL.htm)|Martial Artist|Artiste martial|changé|
|[NaG4fy33coUdSFtH.htm](journals/pages-Archetypes/NaG4fy33coUdSFtH.htm)|Jalmeri Heavenseeker|Chercheur de paradis du Jalmeray|changé|
|[ngVnNmi1Qke3lTy0.htm](journals/pages-Archetypes/ngVnNmi1Qke3lTy0.htm)|Oracle|Oracle|changé|
|[nmcEiM2gjqjGWp2c.htm](journals/pages-Archetypes/nmcEiM2gjqjGWp2c.htm)|Cavalier|Cavalier|changé|
|[nPcma8UOqWo7xw0P.htm](journals/pages-Archetypes/nPcma8UOqWo7xw0P.htm)|Trick Driver|Conducteur astucieux|changé|
|[nVfhX1aisz6jY8qf.htm](journals/pages-Archetypes/nVfhX1aisz6jY8qf.htm)|Unexpected Sharpshooter|Tireur d'élite inattendu|changé|
|[o71hqcfzhCKXcSml.htm](journals/pages-Archetypes/o71hqcfzhCKXcSml.htm)|Archer|Archer|changé|
|[O7fKfFxCx3e1YasX.htm](journals/pages-Archetypes/O7fKfFxCx3e1YasX.htm)|Spellmaster|Maître des sorts|changé|
|[O9P1YgtiCgHlPNp5.htm](journals/pages-Archetypes/O9P1YgtiCgHlPNp5.htm)|Pirate|Pirate|changé|
|[oBlKgVYRup5ORqx1.htm](journals/pages-Archetypes/oBlKgVYRup5ORqx1.htm)|Bard|Barde|changé|
|[OBTmo8rD2x8kFzeH.htm](journals/pages-Archetypes/OBTmo8rD2x8kFzeH.htm)|Sleepwalker|Somnambule|changé|
|[OcioDiLTdgzvT8VX.htm](journals/pages-Archetypes/OcioDiLTdgzvT8VX.htm)|Knight Reclaimant|Chevalier reconquérant|changé|
|[oMIA3aFKvpuV9f2H.htm](journals/pages-Archetypes/oMIA3aFKvpuV9f2H.htm)|Elementalist (Class Archetype)|Élémentaliste (Archétype de classe)|changé|
|[OtBPlgtudM4PlTWD.htm](journals/pages-Archetypes/OtBPlgtudM4PlTWD.htm)|Harrower|Liseur de Tourment|changé|
|[pDmUITVao0FDMnVf.htm](journals/pages-Archetypes/pDmUITVao0FDMnVf.htm)|Halcyon Speaker|Orateur syncrétique|changé|
|[pg9fG1ikcgJZRWlk.htm](journals/pages-Archetypes/pg9fG1ikcgJZRWlk.htm)|Lastwall Sentry|Sentinelle de Dernier-Rempart|changé|
|[Ph7yWRR376aYh12T.htm](journals/pages-Archetypes/Ph7yWRR376aYh12T.htm)|Ritualist|Ritualiste|changé|
|[PpoPRKuwanrnhd0Y.htm](journals/pages-Archetypes/PpoPRKuwanrnhd0Y.htm)|Living Monolith|Monolithe vivant|changé|
|[PVwU1LMWcSgD7Q0m.htm](journals/pages-Archetypes/PVwU1LMWcSgD7Q0m.htm)|Oatia Skysage|Sagecéleste d'Oatie|changé|
|[Q72YDLPh0urLfWPm.htm](journals/pages-Archetypes/Q72YDLPh0urLfWPm.htm)|Vehicle Mechanic|Mécanicien de véhicule|changé|
|[qlQSYjCwnoCzfto2.htm](journals/pages-Archetypes/qlQSYjCwnoCzfto2.htm)|Golem Grafter|Greffeur de golem|changé|
|[r43lPEEL7WHOZjHL.htm](journals/pages-Archetypes/r43lPEEL7WHOZjHL.htm)|Lich|Liche|changé|
|[r79jab1XgOdcgvKJ.htm](journals/pages-Archetypes/r79jab1XgOdcgvKJ.htm)|Magus|Magus|changé|
|[rhDvoOHAhAlABiae.htm](journals/pages-Archetypes/rhDvoOHAhAlABiae.htm)|Acrobat|Acrobate|changé|
|[rlKxNuq1obXe3m5J.htm](journals/pages-Archetypes/rlKxNuq1obXe3m5J.htm)|Scrounger|Bricoleur|changé|
|[RoF5NOFBefXAPftS.htm](journals/pages-Archetypes/RoF5NOFBefXAPftS.htm)|Mauler|Cogneur|changé|
|[rtAFOZOjWHC2zRNO.htm](journals/pages-Archetypes/rtAFOZOjWHC2zRNO.htm)|Vigilante|Justicier|changé|
|[RxDsPgPCCxEdjcVQ.htm](journals/pages-Archetypes/RxDsPgPCCxEdjcVQ.htm)|Hellknight Signifer|Signifer|changé|
|[rxyJUbQosVfgRjLr.htm](journals/pages-Archetypes/rxyJUbQosVfgRjLr.htm)|Crystal Keeper|Gardien des cristaux|changé|
|[S4BZW9c5n6CctDxl.htm](journals/pages-Archetypes/S4BZW9c5n6CctDxl.htm)|Firework Technician|Pyrotechnicien|changé|
|[s9kLzXOCl2GNT6TY.htm](journals/pages-Archetypes/s9kLzXOCl2GNT6TY.htm)|Dandy|Dandy|changé|
|[sAFWD8D0RKH4m25n.htm](journals/pages-Archetypes/sAFWD8D0RKH4m25n.htm)|Dual-Weapon Warrior|Combattant à deux armes|changé|
|[SO9d7RSf1zqmTlmW.htm](journals/pages-Archetypes/SO9d7RSf1zqmTlmW.htm)|Dragon Disciple|Disciple draconique|changé|
|[SUDV5hFZ9WocxWqv.htm](journals/pages-Archetypes/SUDV5hFZ9WocxWqv.htm)|Blessed One|Élu divin|changé|
|[sY25SoDaHBPIG5Jw.htm](journals/pages-Archetypes/sY25SoDaHBPIG5Jw.htm)|Nantambu Chime-Ringer|Sonneur de carillon de Nantambu|changé|
|[tJQ5f8C8m5gpZsF1.htm](journals/pages-Archetypes/tJQ5f8C8m5gpZsF1.htm)|Oozemorph|Vasemorphe|changé|
|[TnD2hTWTyjGKlw9b.htm](journals/pages-Archetypes/TnD2hTWTyjGKlw9b.htm)|Sentinel|Sentinelle|changé|
|[ttkG4geKXTao5pku.htm](journals/pages-Archetypes/ttkG4geKXTao5pku.htm)|Alkenstar Agent|Agent d'Alkenastre|changé|
|[TwSQ0VPXZL0mfPez.htm](journals/pages-Archetypes/TwSQ0VPXZL0mfPez.htm)|Bounded Spellcasting Archetypes|Archétypes d'incantation limitée|changé|
|[uA6XIArPfGSwBvZi.htm](journals/pages-Archetypes/uA6XIArPfGSwBvZi.htm)|Psychic Duelist|Duelliste psychique|changé|
|[uGsGn5sgR7wn0TQD.htm](journals/pages-Archetypes/uGsGn5sgR7wn0TQD.htm)|Turpin Rowe Lumberjack|Bûcheron de Turpin Rowe|changé|
|[ukLpSqTkmRLvZj0z.htm](journals/pages-Archetypes/ukLpSqTkmRLvZj0z.htm)|Stalwart Defender|Défenseur fidèle|changé|
|[Ux0sa5SUBu616i5k.htm](journals/pages-Archetypes/Ux0sa5SUBu616i5k.htm)|Alchemist|Alchimiste|changé|
|[uYCTmos3yZtbD9qs.htm](journals/pages-Archetypes/uYCTmos3yZtbD9qs.htm)|Barbarian|Barbare|changé|
|[vKuJVojicBmNsL1C.htm](journals/pages-Archetypes/vKuJVojicBmNsL1C.htm)|Butterfly Blade|Lame papillon|changé|
|[vS9iMDapZN9uW33Q.htm](journals/pages-Archetypes/vS9iMDapZN9uW33Q.htm)|Bright Lion|Lion radieux|changé|
|[wHs6IGCMaXsgFNop.htm](journals/pages-Archetypes/wHs6IGCMaXsgFNop.htm)|Shadowcaster|Incantateur de l'ombre|changé|
|[wMUAm1PJ1HjJ8fFU.htm](journals/pages-Archetypes/wMUAm1PJ1HjJ8fFU.htm)|Pistol Phenom|Phénomène du pistolet|changé|
|[WvleaXPfigawBvtN.htm](journals/pages-Archetypes/WvleaXPfigawBvtN.htm)|Exorcist|Exorciste|changé|
|[xgk1Qc6nzGNJ7LSz.htm](journals/pages-Archetypes/xgk1Qc6nzGNJ7LSz.htm)|Chronoskimmer|Écumeur du temps|changé|
|[XLug8rxIe1KPX6Nf.htm](journals/pages-Archetypes/XLug8rxIe1KPX6Nf.htm)|Provocator|Provocator|changé|
|[xMdIENo4w9bEzZuK.htm](journals/pages-Archetypes/xMdIENo4w9bEzZuK.htm)|Ghost Eater|Mangeur de fantômes|changé|
|[XszmLWy174esRrlg.htm](journals/pages-Archetypes/XszmLWy174esRrlg.htm)|Wizard|Magicien|changé|
|[XWkyCVISmVtJ0ZY3.htm](journals/pages-Archetypes/XWkyCVISmVtJ0ZY3.htm)|Medic|Médecin|changé|
|[Y4DiXz5I8krCh1fC.htm](journals/pages-Archetypes/Y4DiXz5I8krCh1fC.htm)|Swordmaster|Maître épéiste|changé|
|[ydELQFQq0lGLTvrA.htm](journals/pages-Archetypes/ydELQFQq0lGLTvrA.htm)|Time Mage|Mage du temps|changé|
|[ygzv72IJNmjh0SPB.htm](journals/pages-Archetypes/ygzv72IJNmjh0SPB.htm)|Psychic|Psychiste|changé|
|[YODf1eNWi9jnR93y.htm](journals/pages-Archetypes/YODf1eNWi9jnR93y.htm)|Pactbound Initiate|Initié lié par un Pacte|changé|
|[yOEiNjHLQ6XuOruq.htm](journals/pages-Archetypes/yOEiNjHLQ6XuOruq.htm)|Overwatch|Observateur|changé|
|[YQXGtBHVoiSYUjtl.htm](journals/pages-Archetypes/YQXGtBHVoiSYUjtl.htm)|Twilight Speaker|Orateur du crépuscule|changé|
|[yWtwNUkGyj79Q04W.htm](journals/pages-Archetypes/yWtwNUkGyj79Q04W.htm)|Hellknight|Chevalier infernal|changé|
|[ZewC2i5YdZPsWO8X.htm](journals/pages-Archetypes/ZewC2i5YdZPsWO8X.htm)|Mummy|Momie|changé|
|[zj9z2BwVX6TLHdx3.htm](journals/pages-Archetypes/zj9z2BwVX6TLHdx3.htm)|Wellspring Mage (Class Archetype)|Mage de la source (Archétype de classe)|changé|
|[zxY04QNfB90gjLoh.htm](journals/pages-Archetypes/zxY04QNfB90gjLoh.htm)|Magic Warrior|Guerrier magique|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0e2o62hyCxbIkVoC.htm](journals/pages-Archetypes/0e2o62hyCxbIkVoC.htm)|Assassin|Assassin|libre|
|[0pPtVXH6Duitakbp.htm](journals/pages-Archetypes/0pPtVXH6Duitakbp.htm)|Folklorist|Folkloriste|libre|
|[1rp1IJXiPTAtJ18n.htm](journals/pages-Archetypes/1rp1IJXiPTAtJ18n.htm)|Rivethun Involutionist|Involutioniste du Rivethun|libre|
|[49Y56hH8BKD3XyrW.htm](journals/pages-Archetypes/49Y56hH8BKD3XyrW.htm)|Class Archetypes|Archétypes de classe|libre|
|[4goI7CCdYrtao62w.htm](journals/pages-Archetypes/4goI7CCdYrtao62w.htm)|Seneschal (Class Archetype)|Sénéchal (Archétype de classe)|libre|
|[4YtHynO8i1a33zo9.htm](journals/pages-Archetypes/4YtHynO8i1a33zo9.htm)|Flexible Spellcaster (Class Archetype)|Incantateur flexible (Archétype de classe)|libre|
|[61SHt571HJFGZ9Bl.htm](journals/pages-Archetypes/61SHt571HJFGZ9Bl.htm)|Warrior of Legend (Class Archetype)|Combattant de légende (Archétype de classe)|libre|
|[7sbqk36AqxqUNh60.htm](journals/pages-Archetypes/7sbqk36AqxqUNh60.htm)|Starlit Sentinel|Sentinelle stellaire|libre|
|[8GbRXKWIfyTREPWM.htm](journals/pages-Archetypes/8GbRXKWIfyTREPWM.htm)|Mythic Destinies|Destinées Mythiques|libre|
|[bApp2BZEMuYQCTDM.htm](journals/pages-Archetypes/bApp2BZEMuYQCTDM.htm)|Scout|Éclaireur|libre|
|[cI5dxNYp3jRuWBk9.htm](journals/pages-Archetypes/cI5dxNYp3jRuWBk9.htm)|Tattooed Historian|Historien tatoué|libre|
|[DOc3Pf8wmVxanTIv.htm](journals/pages-Archetypes/DOc3Pf8wmVxanTIv.htm)|Spellcasting Archetypes|Archétypes d'incantation|libre|
|[fPqSekmEm5byReOk.htm](journals/pages-Archetypes/fPqSekmEm5byReOk.htm)|Talisman Dabbler|Amateur de talismans|libre|
|[gqnVXGEyVWylXmPY.htm](journals/pages-Archetypes/gqnVXGEyVWylXmPY.htm)|Temporary Items|Objets temporaires|libre|
|[hw8OdrhBwmWIJQby.htm](journals/pages-Archetypes/hw8OdrhBwmWIJQby.htm)|Archetype Artifacts|Artefacts d'archétypes|libre|
|[JBPXisA4IdXI9gVn.htm](journals/pages-Archetypes/JBPXisA4IdXI9gVn.htm)|Eldritch Researcher|Chercheur mystique|libre|
|[Jkl5KOZih7MoMqAJ.htm](journals/pages-Archetypes/Jkl5KOZih7MoMqAJ.htm)|Alchemical Archetypes|Archétypes alchimiques|libre|
|[kVC4kgYKbhqPsaDt.htm](journals/pages-Archetypes/kVC4kgYKbhqPsaDt.htm)|Rogue|Roublard|libre|
|[miKSovZfKZoqxGg1.htm](journals/pages-Archetypes/miKSovZfKZoqxGg1.htm)|Undead Archetypes|Archétypes de morts-vivants|libre|
|[O79hOcsaQyj3aQC5.htm](journals/pages-Archetypes/O79hOcsaQyj3aQC5.htm)|Archaeologist|Archéologue|libre|
|[OXXF5bZFVMXS9FZb.htm](journals/pages-Archetypes/OXXF5bZFVMXS9FZb.htm)|Multiclass Archetypes|Archétypes multiclasse|libre|
|[qPVOjyJxsof1qL0N.htm](journals/pages-Archetypes/qPVOjyJxsof1qL0N.htm)|Rules|Règles|libre|
|[Rrjz5tMJtyVEQnh8.htm](journals/pages-Archetypes/Rrjz5tMJtyVEQnh8.htm)|Herbalist|Herboriste|libre|
|[StZFWusJiaCqWwi7.htm](journals/pages-Archetypes/StZFWusJiaCqWwi7.htm)|Archetypes|Archétypes|libre|
|[yaR8E2drX6pFUFCK.htm](journals/pages-Archetypes/yaR8E2drX6pFUFCK.htm)|Familiar Master|Maître familier|libre|
|[ZJHhPFjLnizAaUM1.htm](journals/pages-Archetypes/ZJHhPFjLnizAaUM1.htm)|Cleric|Prêtre|libre|
