# État de la traduction (ac-feats)

 * **libre**: 16
 * **officielle**: 2
 * **changé**: 8


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[mJhZ1znkGreEMjTn.htm](ac-feats/mJhZ1znkGreEMjTn.htm)|Unseen Animal Companion|Compagnon animal jamais vu|changé|
|[OUoQGBy9Gk3DSWWi.htm](ac-feats/OUoQGBy9Gk3DSWWi.htm)|Bully|Spécialisé - Brute|changé|
|[Pa0h9IoePPvnEvUn.htm](ac-feats/Pa0h9IoePPvnEvUn.htm)|Daredevil|Spécialisé - Casse-cou|changé|
|[SiFB8P1upTvdhLpx.htm](ac-feats/SiFB8P1upTvdhLpx.htm)|Steadfast Strider|Spécialisé - Monture inébranlable (Touché par un shaitan)|changé|
|[umbj0foct0eZ7Jcj.htm](ac-feats/umbj0foct0eZ7Jcj.htm)|Tracker|Spécialisation - Pisteur|changé|
|[wDiFVxRBktQVj7uI.htm](ac-feats/wDiFVxRBktQVj7uI.htm)|Racer|Spécialisation - Coureur|changé|
|[wRzmLu4JR4A421So.htm](ac-feats/wRzmLu4JR4A421So.htm)|Ambusher|Spécialisé - Piègeur|changé|
|[wZXL6VgEK88BCF0C.htm](ac-feats/wZXL6VgEK88BCF0C.htm)|Wrecker|Spécialiation - Démolisseur|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0rlvJoRCrpYuBl9Y.htm](ac-feats/0rlvJoRCrpYuBl9Y.htm)|Ferocious Charge|Charge féroce|libre|
|[6Te6ocFcRFj50ycj.htm](ac-feats/6Te6ocFcRFj50ycj.htm)|Genie-Touched Companion|compagnon touché par un génie|libre|
|[89eT819I4MYWndcu.htm](ac-feats/89eT819I4MYWndcu.htm)|Nimble Animal Companion|Compagnon animal agile|officielle|
|[alc3ET2e0J4TSbfD.htm](ac-feats/alc3ET2e0J4TSbfD.htm)|Specialized Animal Companion|Compagnon animal spécialisé|libre|
|[by5W9auq2eWT3vZp.htm](ac-feats/by5W9auq2eWT3vZp.htm)|Sinking Jaws|Mâchoires plongeantes|libre|
|[c04kGEFIMMwsKCEP.htm](ac-feats/c04kGEFIMMwsKCEP.htm)|Wind Chaser|Spécialisé - Chasse-vent (touché par un djinn)|libre|
|[eoQnmkqWXaLwNQhG.htm](ac-feats/eoQnmkqWXaLwNQhG.htm)|Deep Diver|Spécialisé - Plongeur profond Vitesse de base (Touché par une maride)|libre|
|[fhgEJVsBNMKu75s5.htm](ac-feats/fhgEJVsBNMKu75s5.htm)|Running Kick|Ruade au passage|libre|
|[h262IUT47dLqZLFZ.htm](ac-feats/h262IUT47dLqZLFZ.htm)|Savage Animal Companion|Compagnon animal sauvage|officielle|
|[K3c8ztEUX27RHeZe.htm](ac-feats/K3c8ztEUX27RHeZe.htm)|Mature Animal Companion|Compagnon animal adulte|libre|
|[lm4QlSITABQKhx5K.htm](ac-feats/lm4QlSITABQKhx5K.htm)|Shade|Ombre|libre|
|[p83NzvyOMMa5Dgdh.htm](ac-feats/p83NzvyOMMa5Dgdh.htm)|Billowing Wings|Ailes déployées|libre|
|[PG5ndHwkPEyjJ70k.htm](ac-feats/PG5ndHwkPEyjJ70k.htm)|Vicious Rend|Éventration vicieuse|libre|
|[Sb8e7jIDTGghYvEk.htm](ac-feats/Sb8e7jIDTGghYvEk.htm)|Wildfire Scorcher|Brûleur de feu sauvage|libre|
|[u9dXZwKANlsC9MDM.htm](ac-feats/u9dXZwKANlsC9MDM.htm)|Indomitable Animal Companion|Compagnon animal indomptable|libre|
|[V8qaydggB3odZM3w.htm](ac-feats/V8qaydggB3odZM3w.htm)|Heightened Instincts|Instincts exacerbés|libre|
|[WCnx9f3Ghtpk9DD0.htm](ac-feats/WCnx9f3Ghtpk9DD0.htm)|Sweeping Tail|Queue fouettante|libre|
|[wqoI2x7clxJXGw8A.htm](ac-feats/wqoI2x7clxJXGw8A.htm)|Nature's Precision|Précision de la nature|libre|
