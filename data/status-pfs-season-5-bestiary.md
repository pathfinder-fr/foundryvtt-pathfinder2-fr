# État de la traduction (pfs-season-5-bestiary)

 * **libre**: 231


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[05Vy7mpQdzAs55rh.htm](pfs-season-5-bestiary/05Vy7mpQdzAs55rh.htm)|Resolved Jamimpi|Jamipi résolu|libre|
|[0BWqVxX1osDP2C8n.htm](pfs-season-5-bestiary/0BWqVxX1osDP2C8n.htm)|Calcite Seep|Suintement de calcite|libre|
|[0dfF8IyWHPop4IR6.htm](pfs-season-5-bestiary/0dfF8IyWHPop4IR6.htm)|Feral Hermit Rat|Rat-pagure sauvage|libre|
|[0dQiQc0E38o6Yvmd.htm](pfs-season-5-bestiary/0dQiQc0E38o6Yvmd.htm)|Sezelrian Warg|Warg de Sézelrian|libre|
|[0dwXcuyO0cO7WsKL.htm](pfs-season-5-bestiary/0dwXcuyO0cO7WsKL.htm)|Very Hexed Chest|Coffre très ensorcelé|libre|
|[10uyFFDhy90H2reO.htm](pfs-season-5-bestiary/10uyFFDhy90H2reO.htm)|Dull Pukwudgie|Pukwudgie insipide|libre|
|[1kqDiAAQGH26eAC7.htm](pfs-season-5-bestiary/1kqDiAAQGH26eAC7.htm)|The Warden (3-4)|Le gardien (3-4)|libre|
|[1oeRVKhIUGwH1E0q.htm](pfs-season-5-bestiary/1oeRVKhIUGwH1E0q.htm)|Consonite Clusters|Cascade de consonites|libre|
|[1sNZB3csh38LOZMm.htm](pfs-season-5-bestiary/1sNZB3csh38LOZMm.htm)|Consonite Needles|Aiguilles consonite|libre|
|[1WBoIEFSt54GDpNp.htm](pfs-season-5-bestiary/1WBoIEFSt54GDpNp.htm)|Robber Novice|Voleur novice|libre|
|[1ytpVvgPTh6J0zxF.htm](pfs-season-5-bestiary/1ytpVvgPTh6J0zxF.htm)|Assassins' Guild Rogue|Voleur de la Guilde des assassins|libre|
|[30pCH1Bsl16UM1s4.htm](pfs-season-5-bestiary/30pCH1Bsl16UM1s4.htm)|Locust Swarm|Nuée de sauterelles|libre|
|[3P1TyTsT3zrNX70k.htm](pfs-season-5-bestiary/3P1TyTsT3zrNX70k.htm)|Possessed Daeodons (7-8)|Daéodons possédés (7-8)|libre|
|[3w7qrvJsy55a8ct9.htm](pfs-season-5-bestiary/3w7qrvJsy55a8ct9.htm)|Spry Blodeuwedd|Blodeuwedd alerte|libre|
|[4JnqGDbDbDziIVgI.htm](pfs-season-5-bestiary/4JnqGDbDbDziIVgI.htm)|Alystair Caskwater (3-4)|Alystair Tonneleau (3-4)|libre|
|[4sspCrQyEVJW5wh7.htm](pfs-season-5-bestiary/4sspCrQyEVJW5wh7.htm)|Angry Foolhardy Mercenary|Mercenaire téméraire faché|libre|
|[5b0fdcM8fB8DZMz7.htm](pfs-season-5-bestiary/5b0fdcM8fB8DZMz7.htm)|Salted Ghost Pirate Captain|Capitaine pirate fantôme de saumure|libre|
|[5XCozjNbo5j47UEQ.htm](pfs-season-5-bestiary/5XCozjNbo5j47UEQ.htm)|Sharp Pukwudgie|Pukwudgie acerbe|libre|
|[6H1jqrmwgQhOECTD.htm](pfs-season-5-bestiary/6H1jqrmwgQhOECTD.htm)|Pampla|Pampla|libre|
|[6QsYuNHAfPXsT6K3.htm](pfs-season-5-bestiary/6QsYuNHAfPXsT6K3.htm)|Studied Orcish Demonologist|Orc érudit en démonologie|libre|
|[6SDKAujYM3nP4iDC.htm](pfs-season-5-bestiary/6SDKAujYM3nP4iDC.htm)|Asper Hajeri (9–10)|Asper Hajeri (9-10)|libre|
|[6Y4PxbzhsTlctbhB.htm](pfs-season-5-bestiary/6Y4PxbzhsTlctbhB.htm)|Weak Giant Mouse|Souris géante affaibli|libre|
|[71DbpnNs2qzRCziB.htm](pfs-season-5-bestiary/71DbpnNs2qzRCziB.htm)|Tok Loyalist|Loyaliste à Tok|libre|
|[76EVzDaB5gJi73tZ.htm](pfs-season-5-bestiary/76EVzDaB5gJi73tZ.htm)|Bandit Leader|Meneur bandit|libre|
|[76sfB4T9gkJjAJOS.htm](pfs-season-5-bestiary/76sfB4T9gkJjAJOS.htm)|Commodore Sticky Fingers|Commodore Doigts-collant|libre|
|[7KvVT7fGXol8j8Px.htm](pfs-season-5-bestiary/7KvVT7fGXol8j8Px.htm)|Tough Robber|Voleur coriace|libre|
|[7OV2a2XHzcKJukU9.htm](pfs-season-5-bestiary/7OV2a2XHzcKJukU9.htm)|Shooting Sphere|Sphère d'étoiles filantes|libre|
|[7Ox34TV7Py1pZr9o.htm](pfs-season-5-bestiary/7Ox34TV7Py1pZr9o.htm)|The Warden (1-2)|Le gardien (1-2)|libre|
|[7Ve2GNNqbx0q4j23.htm](pfs-season-5-bestiary/7Ve2GNNqbx0q4j23.htm)|Arrogant Entodemonologist|Entodémonologiste arrogant|libre|
|[85IQFVma3lRsLjNm.htm](pfs-season-5-bestiary/85IQFVma3lRsLjNm.htm)|Blue-Scarf Heavy|Écharpe-Bleue lourde|libre|
|[8IJilQMumSAPhsqP.htm](pfs-season-5-bestiary/8IJilQMumSAPhsqP.htm)|The Upset Warden (1-2)|Le gardien faché (1-2)|libre|
|[8KnRiKYMkDNYb1mL.htm](pfs-season-5-bestiary/8KnRiKYMkDNYb1mL.htm)|Ghessa (5-6)|Ghessa (5-6)|libre|
|[8lrRsd8fGzm9ts5R.htm](pfs-season-5-bestiary/8lrRsd8fGzm9ts5R.htm)|Tough Assassins' Guild Rogue|Voleur coriace de la Guilde des assassins|libre|
|[8PWnKKHdqNP1oByv.htm](pfs-season-5-bestiary/8PWnKKHdqNP1oByv.htm)|Empowered Shadow of Sezruth|Ombre de Sezruth puissant|libre|
|[8wE5OWsgOQ9ZURzu.htm](pfs-season-5-bestiary/8wE5OWsgOQ9ZURzu.htm)|Hoarding Bat Swarm|Nuée de chauve-souris cueilleuses|libre|
|[8Zj3XDNa07fjkWAq.htm](pfs-season-5-bestiary/8Zj3XDNa07fjkWAq.htm)|Arisen Shadow Pixie|Pixie de l'ombre ressuscitée|libre|
|[99jIe6Kvy9kEDdde.htm](pfs-season-5-bestiary/99jIe6Kvy9kEDdde.htm)|Germinated Mandragora|Mandragore éclose|libre|
|[9i6k29a2Y62VMoQa.htm](pfs-season-5-bestiary/9i6k29a2Y62VMoQa.htm)|Husk Shield|Bouclier de mue|libre|
|[9X5xKHclQlkkmNTI.htm](pfs-season-5-bestiary/9X5xKHclQlkkmNTI.htm)|Sharktopus Eidolon|Eidolon Octorequin|libre|
|[a91e870yzNM7jD93.htm](pfs-season-5-bestiary/a91e870yzNM7jD93.htm)|Toxic Sewer Gator|Alligator d'égout toxique|libre|
|[ACCnvTL2K5L9CAPg.htm](pfs-season-5-bestiary/ACCnvTL2K5L9CAPg.htm)|Barnacle Barnaby|Barnabé la bernacle|libre|
|[aIr87HsiKf9lmxi4.htm](pfs-season-5-bestiary/aIr87HsiKf9lmxi4.htm)|Vulkariki Eidolon|Eidolon Vulkariki|libre|
|[alX5zVLIXBQPzKdJ.htm](pfs-season-5-bestiary/alX5zVLIXBQPzKdJ.htm)|287'S Powerful Ghost|Fantôme puissant de 287|libre|
|[axWiudjK1tVIL2Yr.htm](pfs-season-5-bestiary/axWiudjK1tVIL2Yr.htm)|Burning Sun Orc|Orc du Soleil brûlant|libre|
|[b7jGiIJ1JDrwb4FC.htm](pfs-season-5-bestiary/b7jGiIJ1JDrwb4FC.htm)|Sewer Gator|Alligator d'égout|libre|
|[bdI2vXouliAWxlUx.htm](pfs-season-5-bestiary/bdI2vXouliAWxlUx.htm)|Fey Table Hound|Chien-table féerique|libre|
|[bPUdYAQ88KS0HTzF.htm](pfs-season-5-bestiary/bPUdYAQ88KS0HTzF.htm)|Rope Net Trap|Piège filet de cordes|libre|
|[bvhvwHYwL3VOEt8R.htm](pfs-season-5-bestiary/bvhvwHYwL3VOEt8R.htm)|Ghostknife Infiltrator|Infiltrateur Fantôme-couteau|libre|
|[Bxiaf72ErU7KNgpB.htm](pfs-season-5-bestiary/Bxiaf72ErU7KNgpB.htm)|Exhausted Raja Rakshasa|Rakshasa Raja exténué|libre|
|[byTExdYHVbLJKmKA.htm](pfs-season-5-bestiary/byTExdYHVbLJKmKA.htm)|Asper Hajeri|Asper Hajeri|libre|
|[c93xz68jVyaTiAI5.htm](pfs-season-5-bestiary/c93xz68jVyaTiAI5.htm)|Weak Cosmic Amoeba|Amibe cosmique affaiblie|libre|
|[cieLyRvsiTFKfvu8.htm](pfs-season-5-bestiary/cieLyRvsiTFKfvu8.htm)|Tok's Trap (3-4)|Piège de Tok (3-4)|libre|
|[CpDjDjrACYoosgWA.htm](pfs-season-5-bestiary/CpDjDjrACYoosgWA.htm)|Spicebomb (3-4)|Bombre d'épices (3-4)|libre|
|[cpnCL3eaHIfWyh8I.htm](pfs-season-5-bestiary/cpnCL3eaHIfWyh8I.htm)|Possessed Daeodons|Daéodons possédés|libre|
|[CqixjbeBxscOlzt6.htm](pfs-season-5-bestiary/CqixjbeBxscOlzt6.htm)|Tok's Trap|Piège de Tok|libre|
|[CQLmN7oFQ6N934hh.htm](pfs-season-5-bestiary/CQLmN7oFQ6N934hh.htm)|Arcadian Lurker In Light|Rôdeur de lumière Arcadien|libre|
|[CsTQHukdMTfu9WOZ.htm](pfs-season-5-bestiary/CsTQHukdMTfu9WOZ.htm)|Metal Rascal|Garnement de métal|libre|
|[CZ6Z15ofj8zlHcol.htm](pfs-season-5-bestiary/CZ6Z15ofj8zlHcol.htm)|Hoarding Bat|Chauve-souris cueilleuse|libre|
|[d8C8aBd2ZrG3i0o5.htm](pfs-season-5-bestiary/d8C8aBd2ZrG3i0o5.htm)|Topiary Monster|Monstre topiaire|libre|
|[D965w0VaPj6RMQZl.htm](pfs-season-5-bestiary/D965w0VaPj6RMQZl.htm)|Bloodied Kithangian|Kitangien en sang|libre|
|[DgsmfCNneGUi1sGW.htm](pfs-season-5-bestiary/DgsmfCNneGUi1sGW.htm)|Springloaded Statues|Statues à ressort|libre|
|[dtEPbSLwgcGpzMb5.htm](pfs-season-5-bestiary/dtEPbSLwgcGpzMb5.htm)|Experienced Bandit Leader|Meneur bandit professionnel|libre|
|[dXSti6gNOpoEYoIF.htm](pfs-season-5-bestiary/dXSti6gNOpoEYoIF.htm)|Weak Sewer Ooze (PFS Q18)|Vase des égouts affaibli  (PFS Q18)|libre|
|[DyCslG0OmFNRq7XP.htm](pfs-season-5-bestiary/DyCslG0OmFNRq7XP.htm)|Azarketi Ruffian|Voyou Azarketi|libre|
|[e9j6zxkEUzoPlaWG.htm](pfs-season-5-bestiary/e9j6zxkEUzoPlaWG.htm)|Unruly Mushrooms|Champignons turbulents|libre|
|[EEYXxfc4SFh0JGec.htm](pfs-season-5-bestiary/EEYXxfc4SFh0JGec.htm)|Blazing Skull|Crâne embrasé|libre|
|[EgAoF7pXBsHxthA0.htm](pfs-season-5-bestiary/EgAoF7pXBsHxthA0.htm)|Shadow Draugr|Draugr des ombres|libre|
|[ek6ZStljRFLc6VoU.htm](pfs-season-5-bestiary/ek6ZStljRFLc6VoU.htm)|Captain Shenga Heartrender|Capitaine Shenga Heartrender|libre|
|[emjBymM6qUAuRWQ1.htm](pfs-season-5-bestiary/emjBymM6qUAuRWQ1.htm)|Crumbling Floor|Effondrement du sol|libre|
|[ep7eL4vrN4F6YCdd.htm](pfs-season-5-bestiary/ep7eL4vrN4F6YCdd.htm)|Elite Arisen Umbral Pixie|Pixie ombrale ressuscitée (élite)|libre|
|[epcofI9DCm4edkMa.htm](pfs-season-5-bestiary/epcofI9DCm4edkMa.htm)|Empowered Deskari Effigy|Puissante effigie de Deskari|libre|
|[epmml7kA0akrIGPG.htm](pfs-season-5-bestiary/epmml7kA0akrIGPG.htm)|Little Brother Zibini (3-4)|Petit frère Zibini (3-4)|libre|
|[EvxkVVrCtJIczTML.htm](pfs-season-5-bestiary/EvxkVVrCtJIczTML.htm)|Wanikkawi (7-8)|Wanikkawi (7-8)|libre|
|[eWYkgj6FV8D2IUhT.htm](pfs-season-5-bestiary/eWYkgj6FV8D2IUhT.htm)|Void Zombie Adventurer|Zombie du Vide aventurier|libre|
|[EXCvJfoQBmlKGWeR.htm](pfs-season-5-bestiary/EXCvJfoQBmlKGWeR.htm)|Calcite Flow|Coulée de calcite|libre|
|[fekKhpo6eACOsktu.htm](pfs-season-5-bestiary/fekKhpo6eACOsktu.htm)|Bejeweled Rat Swarm|Nuée de rats parés de bijoux|libre|
|[FnDZckxPSmYbURSM.htm](pfs-season-5-bestiary/FnDZckxPSmYbURSM.htm)|Treasure Avalanche|Avalanche de trésors|libre|
|[fObtdW9jnZh6gKPk.htm](pfs-season-5-bestiary/fObtdW9jnZh6gKPk.htm)|Tough Titanic Flytrap|Attrape-mouche titanesque coriace|libre|
|[fOQBZNOeaFAVstc7.htm](pfs-season-5-bestiary/fOQBZNOeaFAVstc7.htm)|Corrosive Slime|Mucosité corrosive|libre|
|[FQI8cKxD4A6bG8sJ.htm](pfs-season-5-bestiary/FQI8cKxD4A6bG8sJ.htm)|Two-Headed Goat Eidolon|Eidolon chèvre bicéphale|libre|
|[FU2kzh3jmBopL5t9.htm](pfs-season-5-bestiary/FU2kzh3jmBopL5t9.htm)|Elite Commodore Sticky Fingers|Commodore Doigts-collant d'élite|libre|
|[fY2KwMmfdKUtkCvx.htm](pfs-season-5-bestiary/fY2KwMmfdKUtkCvx.htm)|Experienced Goldmonger|Amasseur de trésor chevronné|libre|
|[g8vmUHZ62eNlvhDu.htm](pfs-season-5-bestiary/g8vmUHZ62eNlvhDu.htm)|Lively Commodore Sticky Fingers|Commodore Doigts-collant animé|libre|
|[gEbWlDgtGTmKEmfT.htm](pfs-season-5-bestiary/gEbWlDgtGTmKEmfT.htm)|Burning Sun Warrior|Guerrier du Soleil brûlant|libre|
|[gHzc6zocHNWzMsm4.htm](pfs-season-5-bestiary/gHzc6zocHNWzMsm4.htm)|Enraged Firmagor|Firmagor enragé|libre|
|[GJXz9twOPUjoCBsY.htm](pfs-season-5-bestiary/GJXz9twOPUjoCBsY.htm)|Carcaras|Caracas|libre|
|[H5lFXWXFl7vKrugU.htm](pfs-season-5-bestiary/H5lFXWXFl7vKrugU.htm)|Krosovahn Mendesil|Krosovahn Mendesil|libre|
|[heClAqkkmCgPvm1K.htm](pfs-season-5-bestiary/heClAqkkmCgPvm1K.htm)|Hyacinth Lamprey|Lamproie jacinthe|libre|
|[heoQC3T9m6CoHyrj.htm](pfs-season-5-bestiary/heoQC3T9m6CoHyrj.htm)|Entodemonologist|Entodémonologiste|libre|
|[Hfh8jRPyXBw4B4ic.htm](pfs-season-5-bestiary/Hfh8jRPyXBw4B4ic.htm)|Elite Mutant Cryptid Snapdrake|Craquedrake mutant cryptide (élite)|libre|
|[HkTnWXF1xNQHTPoK.htm](pfs-season-5-bestiary/HkTnWXF1xNQHTPoK.htm)|The Librarian|Le libraire|libre|
|[HKYoyKZp8aSJ9dn7.htm](pfs-season-5-bestiary/HKYoyKZp8aSJ9dn7.htm)|Uirch of the Burning Suns|Uirch du Soleil brûlant|libre|
|[Hvrz7fywW7hmpYcJ.htm](pfs-season-5-bestiary/Hvrz7fywW7hmpYcJ.htm)|Trained Vulkariki Eidolon|Eidolon Vulkariki qualifié|libre|
|[HwedYc7ovLW11wdu.htm](pfs-season-5-bestiary/HwedYc7ovLW11wdu.htm)|Blowing Off Steam|Évacuer la vapeur|libre|
|[Hysy3I3yiYsUZHxY.htm](pfs-season-5-bestiary/Hysy3I3yiYsUZHxY.htm)|Weak Cacophonous Hound Echo|Chien Écho cacophonique affaibli|libre|
|[I3moTdccfUwGK3oJ.htm](pfs-season-5-bestiary/I3moTdccfUwGK3oJ.htm)|Experienced Robbers|Cambrioleur chevronné|libre|
|[iGNLv0ysfXGw6zm8.htm](pfs-season-5-bestiary/iGNLv0ysfXGw6zm8.htm)|Titanic Flytrap|Attrape-mouche titanesque|libre|
|[IxxXcBWC9DeOmMxL.htm](pfs-season-5-bestiary/IxxXcBWC9DeOmMxL.htm)|Trapmaster Tok|Tok le Maître des pièges|libre|
|[jB6tZ23QDSO4iCoK.htm](pfs-season-5-bestiary/jB6tZ23QDSO4iCoK.htm)|Trapmaster Tok (3-4)|Tok le Maître des pièges (3-4)|libre|
|[JGtNz1mLf5rsvJWC.htm](pfs-season-5-bestiary/JGtNz1mLf5rsvJWC.htm)|Hound Echo|Chien Écho|libre|
|[jHtXyQDDZLdytpqk.htm](pfs-season-5-bestiary/jHtXyQDDZLdytpqk.htm)|Weak "Enlightened" Div Worshipper|Adorateur éclairé de Div affaibli|libre|
|[jJd5gZmxg7py26eU.htm](pfs-season-5-bestiary/jJd5gZmxg7py26eU.htm)|Ghostknife Gardener|Jardinier Fantôme-couteau|libre|
|[JKduEdHCeBVIYcdd.htm](pfs-season-5-bestiary/JKduEdHCeBVIYcdd.htm)|Weak Hound Echo|Chien Écho affaibli|libre|
|[JnKbAKFzBMiSvCPP.htm](pfs-season-5-bestiary/JnKbAKFzBMiSvCPP.htm)|Tanuki Trickster|Arnaqueur Tanuki|libre|
|[JtB2dCxf7ap44IkQ.htm](pfs-season-5-bestiary/JtB2dCxf7ap44IkQ.htm)|Omertius, The Gorger|Omertius, Le Gorgé|libre|
|[JVp0JmEzex6bzMyt.htm](pfs-season-5-bestiary/JVp0JmEzex6bzMyt.htm)|Weary Raja Rakshasa|Raja Rakshasa fatigué|libre|
|[k59XSuksLe81FTRe.htm](pfs-season-5-bestiary/k59XSuksLe81FTRe.htm)|Draugr Captain|Capitaine Draugr|libre|
|[KACwlTYwcD9mjWKT.htm](pfs-season-5-bestiary/KACwlTYwcD9mjWKT.htm)|Ravenous Locust Swarm|Nuée de sauterelles affamées|libre|
|[KCBvB2B1OzeJy2cq.htm](pfs-season-5-bestiary/KCBvB2B1OzeJy2cq.htm)|Ashen Brimorak|Brimorak de cendres|libre|
|[kLL35GP9ZczW1tKV.htm](pfs-season-5-bestiary/kLL35GP9ZczW1tKV.htm)|Tisbah|Tisbah|libre|
|[kN7vPgG2s8zNuptz.htm](pfs-season-5-bestiary/kN7vPgG2s8zNuptz.htm)|Orcish Demonologist|Démonologiste orc|libre|
|[kpphDMUvWHsTSFZk.htm](pfs-season-5-bestiary/kpphDMUvWHsTSFZk.htm)|Azlanti Sorcerer|Sorcier Azlant|libre|
|[kRkz56GsYjnowfSR.htm](pfs-season-5-bestiary/kRkz56GsYjnowfSR.htm)|Weak Giant-er Mouse|Souris plus géante affaiblie|libre|
|[KWGNfh21lLKQpmIS.htm](pfs-season-5-bestiary/KWGNfh21lLKQpmIS.htm)|Conference Z's Victims (9-10)|Victimes de la conférence Z (9-10)|libre|
|[l4fXYWBhDNM5FAyT.htm](pfs-season-5-bestiary/l4fXYWBhDNM5FAyT.htm)|Mercenary Squad|Escouade de mercenaires|libre|
|[L55H4HofGvCM02au.htm](pfs-season-5-bestiary/L55H4HofGvCM02au.htm)|Alystair Caskwater (1-2)|Alystair Tonneleau (1-2)|libre|
|[LBzI86ngk7wYf7p6.htm](pfs-season-5-bestiary/LBzI86ngk7wYf7p6.htm)|Weak Div Worshipper|Adorateur de Div affaibli|libre|
|[LWNwaiSnwU0poWgn.htm](pfs-season-5-bestiary/LWNwaiSnwU0poWgn.htm)|Shenga Heartrender|Shenga Heartrender|libre|
|[lXIoXBXIXPvrBu8D.htm](pfs-season-5-bestiary/lXIoXBXIXPvrBu8D.htm)|Springloaded Statues (9–10)|Statues à ressort (9-10)|libre|
|[M05AoaYPBCgYJKDW.htm](pfs-season-5-bestiary/M05AoaYPBCgYJKDW.htm)|Hardened Azarketi Ruffian|Voyou endurci Azarketi|libre|
|[M1POW9BD03uSF6pA.htm](pfs-season-5-bestiary/M1POW9BD03uSF6pA.htm)|Elite Foolhardy Mercenary|Mercenaire d'élite imprudent|libre|
|[meKJ9hL6pDye46XR.htm](pfs-season-5-bestiary/meKJ9hL6pDye46XR.htm)|Raised Draugr|Draugr relevé|libre|
|[mfyZrC0GcyUcGZJK.htm](pfs-season-5-bestiary/mfyZrC0GcyUcGZJK.htm)|Goldmonger|Amasseur de trésors|libre|
|[Mmsmoh7tpsnLZWrI.htm](pfs-season-5-bestiary/Mmsmoh7tpsnLZWrI.htm)|Cosmic Amoeba|Amibe cosmique|libre|
|[mU6Ut1p451lLZslX.htm](pfs-season-5-bestiary/mU6Ut1p451lLZslX.htm)|Ghessa|Ghessa|libre|
|[mVuDXoscClbpR587.htm](pfs-season-5-bestiary/mVuDXoscClbpR587.htm)|Collapsing Debris|Effondrement de débris|libre|
|[MvXeEZ76Z83dZQcm.htm](pfs-season-5-bestiary/MvXeEZ76Z83dZQcm.htm)|Trained Sharktopus Eidolon|Eidolon Octorequin qualifié|libre|
|[MZcunXY0d99zLI1v.htm](pfs-season-5-bestiary/MZcunXY0d99zLI1v.htm)|Void Zombie Explorer|Zombie du Vide explorateur|libre|
|[nMMRUST4ow0kcbTy.htm](pfs-season-5-bestiary/nMMRUST4ow0kcbTy.htm)|Vengeful Alystair Caskwater (1-2)|Alystair Tonneleau vengeur (1-2)|libre|
|[NoatZIUfJETSq20A.htm](pfs-season-5-bestiary/NoatZIUfJETSq20A.htm)|Little Brother Zibini|Petit frère Zibini|libre|
|[NsGS0bvUPAA64hv5.htm](pfs-season-5-bestiary/NsGS0bvUPAA64hv5.htm)|Cultist of Sezelrian|Cultiste de Sézelrian|libre|
|[Nwga1GE1BrsiHVlT.htm](pfs-season-5-bestiary/Nwga1GE1BrsiHVlT.htm)|Busoborn Akata|Akata né buso|libre|
|[Nx58wKSqEkeTcvb9.htm](pfs-season-5-bestiary/Nx58wKSqEkeTcvb9.htm)|Pit of Lives Lost (1-2)|Fosse des Vies perdues (1-2)|libre|
|[OCMrR1GIiGTadIQc.htm](pfs-season-5-bestiary/OCMrR1GIiGTadIQc.htm)|Time-Worn Arjol Parkit|Arjol Parkit épuisé par le temps|libre|
|[ODorXH6Is4csa82y.htm](pfs-season-5-bestiary/ODorXH6Is4csa82y.htm)|Full Head of Steam|Vapeur en poupe|libre|
|[ojgI1Ze5pDqrGH5t.htm](pfs-season-5-bestiary/ojgI1Ze5pDqrGH5t.htm)|Experienced Cultist of Sezelrian|Cultiste de Sézelrien professionnel|libre|
|[OkAH5MvTWnwr5JoD.htm](pfs-season-5-bestiary/OkAH5MvTWnwr5JoD.htm)|Augmented Azlanti Elemental Nexus (5-6)|Nexus élementaire Azlant augmenté (5-6)|libre|
|[OkrConIF3jupAuuD.htm](pfs-season-5-bestiary/OkrConIF3jupAuuD.htm)|Dangerous Collapsing Debris|Effondrement dangereux de débris|libre|
|[ON18h9sYItx5mcXB.htm](pfs-season-5-bestiary/ON18h9sYItx5mcXB.htm)|Shining Shooting Sphere|Sphère brillante d'étoiles filantes|libre|
|[oNgo28jVdWiHUSY2.htm](pfs-season-5-bestiary/oNgo28jVdWiHUSY2.htm)|Busoborn Void Zombie|Zombie du vide né Buso|libre|
|[oU6tQs6uHhf2vuN1.htm](pfs-season-5-bestiary/oU6tQs6uHhf2vuN1.htm)|Blessed Uirch of the Burning Suns|Uirch béni du Soleil brûlant|libre|
|[oUnyuHa0VTB2Zhdk.htm](pfs-season-5-bestiary/oUnyuHa0VTB2Zhdk.htm)|The Inscrutable Librarian|Le libraire indéchiffrable|libre|
|[P0R1Nsk9BOjQTaGE.htm](pfs-season-5-bestiary/P0R1Nsk9BOjQTaGE.htm)|Enraged Shurrizih|Shurrizih enragé|libre|
|[plkilBa4KMyjkRav.htm](pfs-season-5-bestiary/plkilBa4KMyjkRav.htm)|Azlanti Elemental Nexus (5-6)|Nexus élementaire Azlant (5-6)|libre|
|[pRksqD8qJwQ7gb7P.htm](pfs-season-5-bestiary/pRksqD8qJwQ7gb7P.htm)|Flaming Sphere Rune (5-6)|Rune de flamme flottante (5-6)|libre|
|[PS82Nly4bzXQQovZ.htm](pfs-season-5-bestiary/PS82Nly4bzXQQovZ.htm)|Shurrizih|Shurrizih|libre|
|[PuyA0edGhV0eBduX.htm](pfs-season-5-bestiary/PuyA0edGhV0eBduX.htm)|Tanuki Mastermind|Maître penseur Tanuki|libre|
|[PYdVfyDCOZeX2n9n.htm](pfs-season-5-bestiary/PYdVfyDCOZeX2n9n.htm)|Shadowborn Stalker|Pisteur né des ombres|libre|
|[Q2hwsj2uhZwm2kUM.htm](pfs-season-5-bestiary/Q2hwsj2uhZwm2kUM.htm)|Hardwood Scamp|Salopin Bois-solide|libre|
|[Q2Pg6YJidDTOE7zA.htm](pfs-season-5-bestiary/Q2Pg6YJidDTOE7zA.htm)|Shrewd Busoborn Akata|Akata né Buso sournois|libre|
|[q9VY2Whn0rV7zJVK.htm](pfs-season-5-bestiary/q9VY2Whn0rV7zJVK.htm)|Sprite (PFS Q18)|Esprit follet (PFS Q18)|libre|
|[qaG6YsOmqFuZG4dX.htm](pfs-season-5-bestiary/qaG6YsOmqFuZG4dX.htm)|Giant Mouse|Souris géante|libre|
|[QAPjFsQXNVr3ci6R.htm](pfs-season-5-bestiary/QAPjFsQXNVr3ci6R.htm)|Deskari Effigy|Statuette de Deskari|libre|
|[qO1pjZGoRZfjALiO.htm](pfs-season-5-bestiary/qO1pjZGoRZfjALiO.htm)|Tough Ghostknife Gardener|Jardinier Fantôme-couteau coriace|libre|
|[QoTiAPdSnxN6jfzF.htm](pfs-season-5-bestiary/QoTiAPdSnxN6jfzF.htm)|Mutant Cryptid Pine Pangolin|Pangolin des pins cryptide mutant|libre|
|[qqBaCxN4Epxe1rnF.htm](pfs-season-5-bestiary/qqBaCxN4Epxe1rnF.htm)|Weak Shadow Draugr|Draugr des ombres affaibli|libre|
|[qvdMGFjaJe08JQW5.htm](pfs-season-5-bestiary/qvdMGFjaJe08JQW5.htm)|Div Worshipper|Adorateur de Div|libre|
|[R110GkevjeNDEfNb.htm](pfs-season-5-bestiary/R110GkevjeNDEfNb.htm)|Steadied Dohv-Dranna|Dovh-Dranna stabilisé|libre|
|[R1S3vlOlNEEqW8kO.htm](pfs-season-5-bestiary/R1S3vlOlNEEqW8kO.htm)|Sronwa|Sronwa|libre|
|[R7Fu8hiigjklcDG6.htm](pfs-season-5-bestiary/R7Fu8hiigjklcDG6.htm)|Helpful Wisp Swarm|Nuée de fredons serviables|libre|
|[rf9RczwrrbuiXFb5.htm](pfs-season-5-bestiary/rf9RczwrrbuiXFb5.htm)|Delicate Crumbling Floor|Effondrement du sol délicat|libre|
|[RghG76jeaTeQCT6j.htm](pfs-season-5-bestiary/RghG76jeaTeQCT6j.htm)|Sronwa (5-6)|Sronwa (5-6)|libre|
|[RJlJeqv7wniWDGwj.htm](pfs-season-5-bestiary/RJlJeqv7wniWDGwj.htm)|Hexed Chest|Coffre ensorcelé|libre|
|[RPhiMYpPORMIIDJi.htm](pfs-season-5-bestiary/RPhiMYpPORMIIDJi.htm)|Foolhardy Mercenary|Mercenaire imprudent|libre|
|[Rq1k5VZAedG0BGn3.htm](pfs-season-5-bestiary/Rq1k5VZAedG0BGn3.htm)|Assassins' Guild Grenadier|Grenadier de la guilde des assassins|libre|
|[S0LJTh5aCM7de0zE.htm](pfs-season-5-bestiary/S0LJTh5aCM7de0zE.htm)|Tough Ghostknife Infiltrator|Infiltrateur Fantôme-couteau coriace|libre|
|[s5vDDQbslEKjRFXX.htm](pfs-season-5-bestiary/s5vDDQbslEKjRFXX.htm)|Blood Shadow Mirror|Miroir de l'ombre en sang|libre|
|[sAUzCiBg92IFZxJt.htm](pfs-season-5-bestiary/sAUzCiBg92IFZxJt.htm)|Extremely Helpful Wisp Swarm|Nuée de fredons très serviables|libre|
|[sEUngbUxAOr8YwHh.htm](pfs-season-5-bestiary/sEUngbUxAOr8YwHh.htm)|Azlanti Novice Sorcerer|Sorcier novice Azlant|libre|
|[SFfNRoOqDlefQN4q.htm](pfs-season-5-bestiary/SFfNRoOqDlefQN4q.htm)|Hardened Locust Knight|Chevalier sauterelle endurci|libre|
|[shh9wJMiV7Z5ugZI.htm](pfs-season-5-bestiary/shh9wJMiV7Z5ugZI.htm)|Tanuki Prankster|Farceur Tanuki|libre|
|[SJ39wba6n3d5bvIh.htm](pfs-season-5-bestiary/SJ39wba6n3d5bvIh.htm)|Vengeful Alystair Caskwater (3-4)|Alystair Tonneleau vengeur (3-4)|libre|
|[sJDwY8h0z324Rmw7.htm](pfs-season-5-bestiary/sJDwY8h0z324Rmw7.htm)|Blue-Scarf Tough|Écharpe-Bleue robuste|libre|
|[sojCHh1TezXP7JhA.htm](pfs-season-5-bestiary/sojCHh1TezXP7JhA.htm)|Shadow of Sezruth|Ombre de Sezruth|libre|
|[TfhlACV07vdjfTCl.htm](pfs-season-5-bestiary/TfhlACV07vdjfTCl.htm)|Topiary Beast|Bête topiaire|libre|
|[tITdboNp18nGRhBT.htm](pfs-season-5-bestiary/tITdboNp18nGRhBT.htm)|Pit of Lives Lost (3-4)|Fosse des vies perdues (3-4)|libre|
|[TRRlNkWX1EJnLNZi.htm](pfs-season-5-bestiary/TRRlNkWX1EJnLNZi.htm)|Firmagor|Firmagor|libre|
|[tVEpi32axSPlmIjL.htm](pfs-season-5-bestiary/tVEpi32axSPlmIjL.htm)|Three-Headed Goat Eidolon|Eidolon chèvre Tricéphale|libre|
|[TWsApxv2Bx0n7M3i.htm](pfs-season-5-bestiary/TWsApxv2Bx0n7M3i.htm)|Krosovahn Mendesil (7-8)|Krosovahn Mendesil (7-8)|libre|
|[u74d5wQNBJLVHB7m.htm](pfs-season-5-bestiary/u74d5wQNBJLVHB7m.htm)|"Enlightened" Div Worshipper|Adorateur éclairé de Div|libre|
|[uDmXkhH56GdGtfQo.htm](pfs-season-5-bestiary/uDmXkhH56GdGtfQo.htm)|Azlanti Cutpurse|Vide-gousset Azlant|libre|
|[UJrYlFe8jydcY42U.htm](pfs-season-5-bestiary/UJrYlFe8jydcY42U.htm)|Beastmaster Cultist|Maître des bêtes cultiste|libre|
|[ULA3xqlpl5HE5fal.htm](pfs-season-5-bestiary/ULA3xqlpl5HE5fal.htm)|Paper Locust Tornado|Tornade de sauterelles en papier|libre|
|[uMwEuYTO92PZKhJJ.htm](pfs-season-5-bestiary/uMwEuYTO92PZKhJJ.htm)|Conference Z's Victims|Victimes de la conférence Z|libre|
|[umxjQH7AV65Eftfc.htm](pfs-season-5-bestiary/umxjQH7AV65Eftfc.htm)|Arisen Umbral Pixie|Pixie de l'ombre ressuscitée|libre|
|[ut5ji1ZwtUDUfR2B.htm](pfs-season-5-bestiary/ut5ji1ZwtUDUfR2B.htm)|Hidden Current|Courant voilaire|libre|
|[UuOiurvJyyaFZjUB.htm](pfs-season-5-bestiary/UuOiurvJyyaFZjUB.htm)|Weak Leaf Leshy (Q19)|Léchi des feuilles affaibli (Q19)|libre|
|[uxnlSeL2HruddbcF.htm](pfs-season-5-bestiary/uxnlSeL2HruddbcF.htm)|Elite Hound Of Tindalos|Chien de Tindalos d'élite|libre|
|[v3FPb5MgggIdxAOT.htm](pfs-season-5-bestiary/v3FPb5MgggIdxAOT.htm)|Paper Locust Swarm|Nuée de sauterelles en papier|libre|
|[V5gA4pv3ewFxfY6L.htm](pfs-season-5-bestiary/V5gA4pv3ewFxfY6L.htm)|Tisbah (3-4)|Tisbah (3-4)|libre|
|[V5pKh6jxtdXLl1wE.htm](pfs-season-5-bestiary/V5pKh6jxtdXLl1wE.htm)|Locust Knight|Chevalier sauterelle|libre|
|[V9o9SxLYw9s973Uh.htm](pfs-season-5-bestiary/V9o9SxLYw9s973Uh.htm)|Tough Bandit|Bandit coriace|libre|
|[vK1DxX5eTu1QKhbm.htm](pfs-season-5-bestiary/vK1DxX5eTu1QKhbm.htm)|Elite Trained Vulkariki Eidolon|Eidolon vulkariki formé élite|libre|
|[VNOQk9lAIaCT5QNW.htm](pfs-season-5-bestiary/VNOQk9lAIaCT5QNW.htm)|Shadowborn Slayer|Tueur né des ombres|libre|
|[vPHu4aZ0sfW1v7ky.htm](pfs-season-5-bestiary/vPHu4aZ0sfW1v7ky.htm)|Augmented Azlanti Elemental Nexus|Nexus élementaire Azlant augmenté|libre|
|[vsWgbVO6zJIjiqAs.htm](pfs-season-5-bestiary/vsWgbVO6zJIjiqAs.htm)|Elite Sewer Ooze (PFS Q18)|Vase des égouts élite (PFS Q18)|libre|
|[vT3oWDIHL3ncRVPx.htm](pfs-season-5-bestiary/vT3oWDIHL3ncRVPx.htm)|Tok Loyalist (3-4)|Loyaliste à Tok (3-4)|libre|
|[vu8hri1UKziND6Fo.htm](pfs-season-5-bestiary/vu8hri1UKziND6Fo.htm)|Giant Hermit Rat|Rat-pagure géant|libre|
|[vvZktgY1C8Eabs3w.htm](pfs-season-5-bestiary/vvZktgY1C8Eabs3w.htm)|Cacophonous Hound Echo|Chien Écho cacophonique|libre|
|[vXTBGridjDSxFRlU.htm](pfs-season-5-bestiary/vXTBGridjDSxFRlU.htm)|Jamimpi|Jamimpi|libre|
|[vzVej5D1DNHNQwoZ.htm](pfs-season-5-bestiary/vzVej5D1DNHNQwoZ.htm)|Well-Fed Dandasuka|Dandasuka bien nourri|libre|
|[WcLXNcwcA1k9QtbU.htm](pfs-season-5-bestiary/WcLXNcwcA1k9QtbU.htm)|Elite Lively Commodore Sticky Fingers|Commodore Doigts-collant animé d'élite|libre|
|[WoITmnqltzfUlvry.htm](pfs-season-5-bestiary/WoITmnqltzfUlvry.htm)|Angry Mercenary Squad|Escouade de mercenaires en colère|libre|
|[WZJCsTVMRd2msiwS.htm](pfs-season-5-bestiary/WZJCsTVMRd2msiwS.htm)|Spicebomb|Bombe d'épices|libre|
|[x2X7EKnHqACk878g.htm](pfs-season-5-bestiary/x2X7EKnHqACk878g.htm)|Flaming Sphere Rune|Rune de Flamme flottante|libre|
|[X5NAhZIYG5XudA3m.htm](pfs-season-5-bestiary/X5NAhZIYG5XudA3m.htm)|Azlanti Elemental Nexus|Nexus élémentaire Azlant|libre|
|[XboRToJXP1ij2iVB.htm](pfs-season-5-bestiary/XboRToJXP1ij2iVB.htm)|Treasure Avalanche (3-4)|Avalanche de trésors (3-4)|libre|
|[xdShljXDpy0zfYZZ.htm](pfs-season-5-bestiary/xdShljXDpy0zfYZZ.htm)|Younger Sister Vivatu|Soeur cadette Vivatu|libre|
|[XNHZFmrAQTcSYgKY.htm](pfs-season-5-bestiary/XNHZFmrAQTcSYgKY.htm)|Trained Beastmaster Cultist|Maîtresse des bêtes cultiste qualifié|libre|
|[xtIoQWKv3xBd2gqZ.htm](pfs-season-5-bestiary/xtIoQWKv3xBd2gqZ.htm)|Baby Brine Shark|Bébé requin de saumure|libre|
|[yavU0fOuDKQ42ELm.htm](pfs-season-5-bestiary/yavU0fOuDKQ42ELm.htm)|Elite Grig (PFS Q18)|Grig d'élite (PFS Q18)|libre|
|[yDcmp9DpXU1gLrop.htm](pfs-season-5-bestiary/yDcmp9DpXU1gLrop.htm)|Azlanti Thief|Voleur Azlant|libre|
|[yDoN1wYdKb3Wlp22.htm](pfs-season-5-bestiary/yDoN1wYdKb3Wlp22.htm)|Ulfen Ghost Pirate Captain|Capitaine Pirate fantôme ulfe|libre|
|[YDr59BA0SzwFwvTF.htm](pfs-season-5-bestiary/YDr59BA0SzwFwvTF.htm)|Riotous Mushrooms|Champignons tapageurs|libre|
|[yH9ZTfutmRfnDeRh.htm](pfs-season-5-bestiary/yH9ZTfutmRfnDeRh.htm)|Bandit|Bandit|libre|
|[YhfGETC1rs90Zj99.htm](pfs-season-5-bestiary/YhfGETC1rs90Zj99.htm)|Greater Blood Shadow Mirror|Miroir de l'ombre en sang supérieur|libre|
|[yiwRRjZ2RgyLt2gv.htm](pfs-season-5-bestiary/yiwRRjZ2RgyLt2gv.htm)|Wanikkawi|Wanikkawi|libre|
|[YKy7KS3pxT9tuRWp.htm](pfs-season-5-bestiary/YKy7KS3pxT9tuRWp.htm)|Barnacle Barnaby (5-6)|Barnabé la bernacle (5-6)|libre|
|[YmQ71Om9pSUBgrqn.htm](pfs-season-5-bestiary/YmQ71Om9pSUBgrqn.htm)|287's Ghost|Fantôme de 287|libre|
|[yq9uaibTxRdywsWo.htm](pfs-season-5-bestiary/yq9uaibTxRdywsWo.htm)|Arjol Parkit|Arjol Parkit|libre|
|[yrQTOX05SXnpvEYL.htm](pfs-season-5-bestiary/yrQTOX05SXnpvEYL.htm)|Giant-er Mouse|Souris encore plus géante|libre|
|[ytb75tCpHFXxryFZ.htm](pfs-season-5-bestiary/ytb75tCpHFXxryFZ.htm)|Elite Arisen Shadow Pixie|Pixie de l'ombre ressuscitée (élite)|libre|
|[ytfKFUktZrRur3nO.htm](pfs-season-5-bestiary/ytfKFUktZrRur3nO.htm)|Deskari Cultist|Cultiste Deskari|libre|
|[YtmA0Qpc4NrAKIfv.htm](pfs-season-5-bestiary/YtmA0Qpc4NrAKIfv.htm)|Empowered Daeodon|Daeodon puissant|libre|
|[YVUARKHWKUs8gWlr.htm](pfs-season-5-bestiary/YVUARKHWKUs8gWlr.htm)|Omertius, The Engorged|Omertius, L'Engorgé|libre|
|[Z7Rhe3GtUobh0RZ0.htm](pfs-season-5-bestiary/Z7Rhe3GtUobh0RZ0.htm)|Deskari Fantatic|Fanatique Deskari|libre|
|[Z9ofpt7xRr9o47Ya.htm](pfs-season-5-bestiary/Z9ofpt7xRr9o47Ya.htm)|The Upset Warden (3-4)|Le gardien furieux (3-4)|libre|
|[zbh0oprNBaxTbsqh.htm](pfs-season-5-bestiary/zbh0oprNBaxTbsqh.htm)|Elite Sprite (PFS Q18)|Esprit follet élite (PFS Q18)|libre|
|[Zff7C8x4VzB7ZOiZ.htm](pfs-season-5-bestiary/Zff7C8x4VzB7ZOiZ.htm)|Hermit Rat|Rat-pagure|libre|
|[zIi94PzeveWBlxTy.htm](pfs-season-5-bestiary/zIi94PzeveWBlxTy.htm)|Orcish Throat Slitters|Égorgeur orc|libre|
|[ziIkenoVDHSAyUC2.htm](pfs-season-5-bestiary/ziIkenoVDHSAyUC2.htm)|Acrid Corrosive Slime|Mucosité corrosive caustique|libre|
