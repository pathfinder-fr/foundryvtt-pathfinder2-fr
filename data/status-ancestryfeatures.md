# État de la traduction (ancestryfeatures)

 * **libre**: 50
 * **officielle**: 1


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[feat-00-8sxtjVsk9HBY5yAv.htm](ancestryfeatures/feat-00-8sxtjVsk9HBY5yAv.htm)|Glowing Horn|Corne luisante|libre|
|[feat-00-AUlPRySCqE6o6LHH.htm](ancestryfeatures/feat-00-AUlPRySCqE6o6LHH.htm)|Eyes in Back|Yeux dans le dos|libre|
|[feat-00-AzGJN1wwLFaLJIeo.htm](ancestryfeatures/feat-00-AzGJN1wwLFaLJIeo.htm)|Aquatic Adaptation|Adaptation aquatique|libre|
|[feat-00-BgHrucbZ9TH92RDv.htm](ancestryfeatures/feat-00-BgHrucbZ9TH92RDv.htm)|Sunlight Healing|Guérison par le soleil|libre|
|[feat-00-BHPDeqQHqi7ukCUW.htm](ancestryfeatures/feat-00-BHPDeqQHqi7ukCUW.htm)|Constructed (Poppet)|Construit (poupée)|libre|
|[feat-00-bHUrm79wJCcI7L3A.htm](ancestryfeatures/feat-00-bHUrm79wJCcI7L3A.htm)|Sunlight|Lumière du soleil|libre|
|[feat-00-d5TYEND07kwWRBIq.htm](ancestryfeatures/feat-00-d5TYEND07kwWRBIq.htm)|Aquatic Grace|Grace aquatique|libre|
|[feat-00-dCp517IUFJk8JvQc.htm](ancestryfeatures/feat-00-dCp517IUFJk8JvQc.htm)|Fangs|Crocs|libre|
|[feat-00-dkZ8RxdQFJrdxwQo.htm](ancestryfeatures/feat-00-dkZ8RxdQFJrdxwQo.htm)|Revulsion|Répulsion|libre|
|[feat-00-dtNsRAhCRfteA1ev.htm](ancestryfeatures/feat-00-dtNsRAhCRfteA1ev.htm)|Blunt Snout|Museau court|libre|
|[feat-00-DWNRIgt3f92fSTBM.htm](ancestryfeatures/feat-00-DWNRIgt3f92fSTBM.htm)|Hydration (Merfolk)|Hydratation (homme-poisson)|libre|
|[feat-00-E28a45fUC2OkXZXY.htm](ancestryfeatures/feat-00-E28a45fUC2OkXZXY.htm)|Constructed Body|Corps artificiel|libre|
|[feat-00-egpiSWBrNBb1Fmig.htm](ancestryfeatures/feat-00-egpiSWBrNBb1Fmig.htm)|Draconic Exemplar|Modèle draconique|libre|
|[feat-00-Eyuqu6eIaoGCjnMv.htm](ancestryfeatures/feat-00-Eyuqu6eIaoGCjnMv.htm)|Clan Dagger|Dague de clan|libre|
|[feat-00-gZvgt5n5OYO3av2V.htm](ancestryfeatures/feat-00-gZvgt5n5OYO3av2V.htm)|Natural Climber|Grimpeur naturel|libre|
|[feat-00-IXyXCMBldrU5G60e.htm](ancestryfeatures/feat-00-IXyXCMBldrU5G60e.htm)|Innate Venom|Venin inné|libre|
|[feat-00-jatezR4bENwhC6GL.htm](ancestryfeatures/feat-00-jatezR4bENwhC6GL.htm)|Bite|Morsure|libre|
|[feat-00-jS8GamSTl8yMLL4F.htm](ancestryfeatures/feat-00-jS8GamSTl8yMLL4F.htm)|Fangs (Nagaji)|Crochets (nagaji)|libre|
|[feat-00-kMgyOI4kBIEtFvhb.htm](ancestryfeatures/feat-00-kMgyOI4kBIEtFvhb.htm)|Swim|Nage|libre|
|[feat-00-mEDTJi7d1bTEiwUD.htm](ancestryfeatures/feat-00-mEDTJi7d1bTEiwUD.htm)|Unusual Anatomy|Anatomie inhabituelle|libre|
|[feat-00-mnhmhOKWLiOD0lev.htm](ancestryfeatures/feat-00-mnhmhOKWLiOD0lev.htm)|Constructed|Construit|libre|
|[feat-00-N0zhJ0whkDJPlftl.htm](ancestryfeatures/feat-00-N0zhJ0whkDJPlftl.htm)|Photosynthesis|Photosynthèse|libre|
|[feat-00-NfkxFWUeG6g41e8w.htm](ancestryfeatures/feat-00-NfkxFWUeG6g41e8w.htm)|Claws|Griffes|libre|
|[feat-00-oCIO7UJqbpTkI62l.htm](ancestryfeatures/feat-00-oCIO7UJqbpTkI62l.htm)|Wings|Ailes|libre|
|[feat-00-PtvzTm2gjdCKao4I.htm](ancestryfeatures/feat-00-PtvzTm2gjdCKao4I.htm)|Prehensile Tail|Queue préhensile|libre|
|[feat-00-qJD3PJdoSXFrZEwr.htm](ancestryfeatures/feat-00-qJD3PJdoSXFrZEwr.htm)|Sharp Beak|Bec aiguisé|libre|
|[feat-00-qKh6MxgE0cwde6mC.htm](ancestryfeatures/feat-00-qKh6MxgE0cwde6mC.htm)|Flammable|Inflammable|libre|
|[feat-00-QyBfftocP1i43Qrp.htm](ancestryfeatures/feat-00-QyBfftocP1i43Qrp.htm)|Empathic Sense|Sens empathique|libre|
|[feat-00-RYrY7o0i6s7KW9io.htm](ancestryfeatures/feat-00-RYrY7o0i6s7KW9io.htm)|Automaton Core|Noyau automate|libre|
|[feat-00-SAbzItAI4uwbdnQk.htm](ancestryfeatures/feat-00-SAbzItAI4uwbdnQk.htm)|Basic Undead Benefits|Avantages de mort-vivant basiques|libre|
|[feat-00-sL1hHxrHdMNIZVAd.htm](ancestryfeatures/feat-00-sL1hHxrHdMNIZVAd.htm)|Land on Your Feet|Retomber sur vos pattes|libre|
|[feat-00-Sm3tKetM6kddTio3.htm](ancestryfeatures/feat-00-Sm3tKetM6kddTio3.htm)|Plant Nourishment|Alimentation des plantes|libre|
|[feat-00-sW4ObB8wyLPPYvGj.htm](ancestryfeatures/feat-00-sW4ObB8wyLPPYvGj.htm)|Sharp Teeth|Dents aiguisées|libre|
|[feat-00-TRw4oBZBFZG96jKO.htm](ancestryfeatures/feat-00-TRw4oBZBFZG96jKO.htm)|Magical Strikes|Frappes magiques|libre|
|[feat-00-uSAYmU7PO2QoOWhB.htm](ancestryfeatures/feat-00-uSAYmU7PO2QoOWhB.htm)|Emotionally Unaware|Émotionnellement non préparé|libre|
|[feat-00-vPhPgzpRjYDMT9Kq.htm](ancestryfeatures/feat-00-vPhPgzpRjYDMT9Kq.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[feat-00-vt67b8uoNEbskcBv.htm](ancestryfeatures/feat-00-vt67b8uoNEbskcBv.htm)|Hydration|Hydratation|libre|
|[feat-00-y1EmCv2cEb5hXBwx.htm](ancestryfeatures/feat-00-y1EmCv2cEb5hXBwx.htm)|Keen Eyes|Yeux perçants|officielle|
|[feat-00-YGk41WV42aTM7CQV.htm](ancestryfeatures/feat-00-YGk41WV42aTM7CQV.htm)|Advanced Undead Benefits|Avantages avancés des morts-vivants|libre|
|[feat-01-0qHN69NF7JBWKO8v.htm](ancestryfeatures/feat-01-0qHN69NF7JBWKO8v.htm)|Awakened Mind|Esprit éveillé|libre|
|[feat-01-D6AjSg0JOYtn7Coa.htm](ancestryfeatures/feat-01-D6AjSg0JOYtn7Coa.htm)|Wanderer's Soul|Âme vagabonde|libre|
|[feat-01-hUpYTsbLcBg4UcVp.htm](ancestryfeatures/feat-01-hUpYTsbLcBg4UcVp.htm)|Horns (Minotaur)|Cornes (Minotaure)|libre|
|[feat-01-HYefFkddD9lOhFM8.htm](ancestryfeatures/feat-01-HYefFkddD9lOhFM8.htm)|Head Gem|Gemme de tête|libre|
|[feat-01-ItQDbIiqpb50Zbtm.htm](ancestryfeatures/feat-01-ItQDbIiqpb50Zbtm.htm)|Awakened Form|Forme éveillée|libre|
|[feat-01-knaVRiZ2QNteat1P.htm](ancestryfeatures/feat-01-knaVRiZ2QNteat1P.htm)|Change Shape|Changement de forme|libre|
|[feat-01-LhgVj9GhPsXGfwAM.htm](ancestryfeatures/feat-01-LhgVj9GhPsXGfwAM.htm)|Horns (Sarangay)|Cornes (Sarangay)|libre|
|[feat-01-oj532l4YXqzedBBn.htm](ancestryfeatures/feat-01-oj532l4YXqzedBBn.htm)|Animal Attack|Attaque animale|libre|
|[feat-01-oQ5478uONrGAWRgF.htm](ancestryfeatures/feat-01-oQ5478uONrGAWRgF.htm)|Magiphage|Magiphage|libre|
|[feat-01-QLSvmOid6gAdzBYi.htm](ancestryfeatures/feat-01-QLSvmOid6gAdzBYi.htm)|Cryptomnesia|Cryptomnésie|libre|
|[feat-01-S5XriNcxiVvqZEu0.htm](ancestryfeatures/feat-01-S5XriNcxiVvqZEu0.htm)|Robust|Robuste|libre|
|[feat-01-VX267tc1uwocRu39.htm](ancestryfeatures/feat-01-VX267tc1uwocRu39.htm)|Mount|Monture|libre|
