# État de la traduction (pfs-season-2-bestiary)

 * **libre**: 254


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0ATZwmS2yxbheza4.htm](pfs-season-2-bestiary/0ATZwmS2yxbheza4.htm)|Scorched Earth Orc (Sharpshooter) (7-8)|Orc de la Terre brûlée (Tireur d'élite) (7-8)|libre|
|[0OnpbW2SdOZXdRgw.htm](pfs-season-2-bestiary/0OnpbW2SdOZXdRgw.htm)|Glorzia|Glorzia|libre|
|[0RPGHzGalRHqEEHx.htm](pfs-season-2-bestiary/0RPGHzGalRHqEEHx.htm)|Reinforced Animated Guardian|Gardien animé renforcé|libre|
|[16NjM1igmVpNcleL.htm](pfs-season-2-bestiary/16NjM1igmVpNcleL.htm)|Tewakam Nekotek (3-4)|Téwakam Nékotek (3-4)|libre|
|[1Hr2rWZf52nWnJ4t.htm](pfs-season-2-bestiary/1Hr2rWZf52nWnJ4t.htm)|Tough Leaded Skeleton|Squelette de plomb coriace|libre|
|[1Mn1KhwyL6gU4TRl.htm](pfs-season-2-bestiary/1Mn1KhwyL6gU4TRl.htm)|Hand Of Urxehl (7-8)|Main d'Urxehl (7-8)|libre|
|[1TYxfz5Q4iVQUOa5.htm](pfs-season-2-bestiary/1TYxfz5Q4iVQUOa5.htm)|Bloody Vortex (5-6)|Vortex meurtrier (5-6)|libre|
|[1xVm60zTlUsH8hnR.htm](pfs-season-2-bestiary/1xVm60zTlUsH8hnR.htm)|Barrow Quasit|Quasit de tumulus|libre|
|[1ZIaQe0SNDmoVUoh.htm](pfs-season-2-bestiary/1ZIaQe0SNDmoVUoh.htm)|Brittle Ravener Husk|Vestige de dévoreur fragile|libre|
|[2LcIG75RQoujanC9.htm](pfs-season-2-bestiary/2LcIG75RQoujanC9.htm)|Wishbound Belker (5-6)|Belker whishbound (5-6)|libre|
|[3KB7j8D0eMdn8Sol.htm](pfs-season-2-bestiary/3KB7j8D0eMdn8Sol.htm)|Empowered Blood (1-2)|Renforcement de sang (1-2)|libre|
|[3oL6mD7SYNxbghTd.htm](pfs-season-2-bestiary/3oL6mD7SYNxbghTd.htm)|Kayajima Boar|Sanglier Kayajima|libre|
|[3ShJKXoXXENIToA8.htm](pfs-season-2-bestiary/3ShJKXoXXENIToA8.htm)|Frostbitten Trollhound|Limier de troll glacial|libre|
|[3VXbICsrBiOGnxiz.htm](pfs-season-2-bestiary/3VXbICsrBiOGnxiz.htm)|Ogthup (7-8)|Ogthup (7-8)|libre|
|[4BaZvzpSYftJEkV8.htm](pfs-season-2-bestiary/4BaZvzpSYftJEkV8.htm)|Abyssal Fungi|Champignon chthonien (Abyssal)|libre|
|[4DPuCgAisPyphiMN.htm](pfs-season-2-bestiary/4DPuCgAisPyphiMN.htm)|Teflar|Teflar|libre|
|[4ER4ulKtEiqgxWEg.htm](pfs-season-2-bestiary/4ER4ulKtEiqgxWEg.htm)|Fire-Eye Cyclops Zombie (3-4)|Zombie cyclope Oeil-de-feu (3-4)|libre|
|[4NVVOVTT1jIQfAjR.htm](pfs-season-2-bestiary/4NVVOVTT1jIQfAjR.htm)|Burned-Out Efreet|Éfrit exténué|libre|
|[51GRDiQOAA9CP3h3.htm](pfs-season-2-bestiary/51GRDiQOAA9CP3h3.htm)|Mephit Swarm (5-6)|Nuée de méphites (5-6)|libre|
|[576v0bDKtGE5dwnQ.htm](pfs-season-2-bestiary/576v0bDKtGE5dwnQ.htm)|Clouded Quartz (5-6)|Quartz nuageux (5-6)|libre|
|[5CosDEozyZDtngtD.htm](pfs-season-2-bestiary/5CosDEozyZDtngtD.htm)|Fire-Eye Cyclops Zombie (1-2)|Zombie cyclope Oeil-de-feu (1-2)|libre|
|[5NCKrDA869F82zPC.htm](pfs-season-2-bestiary/5NCKrDA869F82zPC.htm)|Leaded Skeleton|Squelette de plomb|libre|
|[5qyOQeszMplJXw8s.htm](pfs-season-2-bestiary/5qyOQeszMplJXw8s.htm)|Flooding Monolith (3-4)|Monolithe d'inondation (3-4)|libre|
|[5Vye56emf5vTmHiQ.htm](pfs-season-2-bestiary/5Vye56emf5vTmHiQ.htm)|Collapsing Cabinet (3-4)|Effondrement d'armoire (3-4)|libre|
|[61jBzgd1NYLw4NC0.htm](pfs-season-2-bestiary/61jBzgd1NYLw4NC0.htm)|Vermlek Centaur (3-4)|Centaure vermlek (3-4)|libre|
|[64x0nZk3QlENAgUU.htm](pfs-season-2-bestiary/64x0nZk3QlENAgUU.htm)|Abyssal Firestorm Surge (7-8)|Déferlement de feu chthonien (Abyssal) (7-8)|libre|
|[6aIh6EXxkqkBciQA.htm](pfs-season-2-bestiary/6aIh6EXxkqkBciQA.htm)|Blighted Leaf Leshy|Léchi de feuilles dégradée|libre|
|[6kRWSFIjPusy4d58.htm](pfs-season-2-bestiary/6kRWSFIjPusy4d58.htm)|The Scholar of Sorts (1-2)|Une sorte d'érudit (1-2)|libre|
|[6PKCOGCNmsMQHc7A.htm](pfs-season-2-bestiary/6PKCOGCNmsMQHc7A.htm)|Flaming Eye (1-2)|Oeil enflammé (1-2)|libre|
|[70vDhFdWWU6olNlb.htm](pfs-season-2-bestiary/70vDhFdWWU6olNlb.htm)|Archis Peers (1-2)|Archis Peers (1-2)|libre|
|[7nqeVP1X5v3UMWxZ.htm](pfs-season-2-bestiary/7nqeVP1X5v3UMWxZ.htm)|Scorched Earth Orc (Scout) (5-6)|Orc de la Terre brûlée (Éclaireur) (5-6)|libre|
|[7R69eW0f3BuHe7TS.htm](pfs-season-2-bestiary/7R69eW0f3BuHe7TS.htm)|Spark Troll (1-2)|Troll étincelle (1-2)|libre|
|[7rSEHlUPv6DLkAy1.htm](pfs-season-2-bestiary/7rSEHlUPv6DLkAy1.htm)|Wishbound Belker (7-8)|Belker wishbound (7-8)|libre|
|[7w0gBYxl6kS9WYgl.htm](pfs-season-2-bestiary/7w0gBYxl6kS9WYgl.htm)|Strak (5-6)|Strak (5-6)|libre|
|[83k9JooGu3wbCJKa.htm](pfs-season-2-bestiary/83k9JooGu3wbCJKa.htm)|Razortoothed Shark|Requin aux dents rasoirs|libre|
|[8Eywy3sqfECiULGl.htm](pfs-season-2-bestiary/8Eywy3sqfECiULGl.htm)|Kareida|Kareida|libre|
|[8FBm4IjFYlolzDXu.htm](pfs-season-2-bestiary/8FBm4IjFYlolzDXu.htm)|Lion (PFS 2-13)|Lion (PFS 2-13)|libre|
|[8OAeFPKcZGpFDTsv.htm](pfs-season-2-bestiary/8OAeFPKcZGpFDTsv.htm)|Mother Forsythe (5-6)|Mère Forsythe (5-6)|libre|
|[8PH89qtfwyFvggMq.htm](pfs-season-2-bestiary/8PH89qtfwyFvggMq.htm)|Ember Fox (PFS 2-14)|Renard de braise (PFS 2-14)|libre|
|[8WXX6nSrxpry1QYN.htm](pfs-season-2-bestiary/8WXX6nSrxpry1QYN.htm)|Chesjilawa Jadwiga Karina (7-8)|Chesjilawa Jadwiga Karina (7-8)|libre|
|[8ZukoQpVoMcshX62.htm](pfs-season-2-bestiary/8ZukoQpVoMcshX62.htm)|Semyon (7-8)|Semyon (7-8)|libre|
|[9a72g0wO4E9F46UX.htm](pfs-season-2-bestiary/9a72g0wO4E9F46UX.htm)|Pressurized Bottle (1-2)|Bouteille sous pression (1-2)|libre|
|[9mKcQFcY4Y1g5kJt.htm](pfs-season-2-bestiary/9mKcQFcY4Y1g5kJt.htm)|Ancient Ooze Pit|Ancienne fosse d'une vase|libre|
|[9X3qkp1qsii5LFQP.htm](pfs-season-2-bestiary/9X3qkp1qsii5LFQP.htm)|Famished Mimic|Mimique affamée|libre|
|[a4U7IlJCJSxVwl8Q.htm](pfs-season-2-bestiary/a4U7IlJCJSxVwl8Q.htm)|Mushroom Ring (1-2)|Cercle de champignons (1-2)|libre|
|[A6zDTtZ6fLBLPmCP.htm](pfs-season-2-bestiary/A6zDTtZ6fLBLPmCP.htm)|Greeleep (3-4)|Greeleep (3-4)|libre|
|[aBxy7czPiVOSIlTl.htm](pfs-season-2-bestiary/aBxy7czPiVOSIlTl.htm)|Russian Soldier (5-6)|Soldat russe (5-6)|libre|
|[ad58hkodIp5UjPa3.htm](pfs-season-2-bestiary/ad58hkodIp5UjPa3.htm)|Mutated Trollhound|Limier de troll mutant|libre|
|[AkKxh2OXfWEb5OHt.htm](pfs-season-2-bestiary/AkKxh2OXfWEb5OHt.htm)|Urxehl's Firestorm (7-8)|Tempête de feu d'Urxehl  (7-8)|libre|
|[AMZBCYKz639sHMPZ.htm](pfs-season-2-bestiary/AMZBCYKz639sHMPZ.htm)|Firestorm Brimorak|Brimorak Tempête de feu|libre|
|[aqfexx59fbDiR7RH.htm](pfs-season-2-bestiary/aqfexx59fbDiR7RH.htm)|Valgomorus (3-4)|Valgomorus (3-4)|libre|
|[aQqV0cBCyPuMoNAs.htm](pfs-season-2-bestiary/aQqV0cBCyPuMoNAs.htm)|Solvatar Caryg|Solvatar Caryg|libre|
|[aQsmJxRJIM0nzw5e.htm](pfs-season-2-bestiary/aQsmJxRJIM0nzw5e.htm)|Corvius Vayn (5-6)|Corvius Vayn (5-6)|libre|
|[B73Oo8pLdSGKqqOx.htm](pfs-season-2-bestiary/B73Oo8pLdSGKqqOx.htm)|Bloody Vortex (3-4)|Vortex meurtrier (3-4)|libre|
|[bARneaE3aBOFH1ty.htm](pfs-season-2-bestiary/bARneaE3aBOFH1ty.htm)|Explosive Monolith (5-6)|Monolithe explosif (5-6)|libre|
|[BeenWzuutHRhsoBc.htm](pfs-season-2-bestiary/BeenWzuutHRhsoBc.htm)|Sliding Statue (1-2)|Statue coulissante (1-2)|libre|
|[BjJ5KAf1f3SeAesY.htm](pfs-season-2-bestiary/BjJ5KAf1f3SeAesY.htm)|Weakened Abyssal Fungi|Champignon chthonien (Abyssal) affaibli|libre|
|[BPchAmEdLQtWhe9r.htm](pfs-season-2-bestiary/BPchAmEdLQtWhe9r.htm)|Animated Guardian Warrior|Gardien animé guerrier|libre|
|[BSbqNkNSs5IMu6RO.htm](pfs-season-2-bestiary/BSbqNkNSs5IMu6RO.htm)|Usij Acolyte|Acolyte Usij|libre|
|[BXnmDgVkhD97fI1o.htm](pfs-season-2-bestiary/BXnmDgVkhD97fI1o.htm)|Anatomical Model (1-2)|Modèle anatomique (1-2)|libre|
|[bzV9QF6CfIGX8aNR.htm](pfs-season-2-bestiary/bzV9QF6CfIGX8aNR.htm)|Aspis Veteran Guard|Garde vétéran de l'Aspis|libre|
|[C0UQiPJLDm7vb0ZV.htm](pfs-season-2-bestiary/C0UQiPJLDm7vb0ZV.htm)|Master Sergeant Morgroar (5-6)|Sergeant Maître Morgroar (5-6)|libre|
|[c4KECFBOIXqAagwm.htm](pfs-season-2-bestiary/c4KECFBOIXqAagwm.htm)|Mishka The Bear (7-8)|L'ours Mishka (7-8)|libre|
|[c4t6aDgo7pdMI7yN.htm](pfs-season-2-bestiary/c4t6aDgo7pdMI7yN.htm)|Mercenary Wizard (1-2)|Mage mercenaire (1-2)|libre|
|[c7ppBFpsmLFP1YD5.htm](pfs-season-2-bestiary/c7ppBFpsmLFP1YD5.htm)|Corrupted Wildfire (5-6)|Feu sauvage corrompu (5-6)|libre|
|[c7uwPrgkjqSKRXQG.htm](pfs-season-2-bestiary/c7uwPrgkjqSKRXQG.htm)|Qxal, The Thorned Monarch|Qxal, le Monarque Épineux|libre|
|[ckvfamrYKeUYX7Cl.htm](pfs-season-2-bestiary/ckvfamrYKeUYX7Cl.htm)|Starved Sea Drake|Drake des mers affamé|libre|
|[Cl2RNfFj3iuc1xp3.htm](pfs-season-2-bestiary/Cl2RNfFj3iuc1xp3.htm)|Chained Troll|Troll enchaîné|libre|
|[CmMK9sadwbxfBaYg.htm](pfs-season-2-bestiary/CmMK9sadwbxfBaYg.htm)|Yeth Warbeast|Chien (de Yeth) hurleur de combat|libre|
|[cRrHxzUAZvbhgcDr.htm](pfs-season-2-bestiary/cRrHxzUAZvbhgcDr.htm)|Scorched Earth Orc (Scout) (7-8)|Orc de la Terre brûlée (Éclaireur) (7-8)|libre|
|[cWenvBwMHSnN0sHV.htm](pfs-season-2-bestiary/cWenvBwMHSnN0sHV.htm)|Explosive Monolith (3-4)|Monolithe explosif (3-4)|libre|
|[cx796RgvywxF8P1A.htm](pfs-season-2-bestiary/cx796RgvywxF8P1A.htm)|Flaming Moss Monster|Monstre bryacé enflammé|libre|
|[CXSThQwaRg1eEurb.htm](pfs-season-2-bestiary/CXSThQwaRg1eEurb.htm)|Master Sergeant Morgroar (3-4)|Sergeant Maître Morgroar (3-4)|libre|
|[D0eC5CwcBAsSoAJC.htm](pfs-season-2-bestiary/D0eC5CwcBAsSoAJC.htm)|Fence (PFS 2-00)|Receleur (PFS 2-00)|libre|
|[D4B3dR8y5h9QIeqi.htm](pfs-season-2-bestiary/D4B3dR8y5h9QIeqi.htm)|Disciple of Urxehl (7-8)|Disciple d'Urxehl (7-8)|libre|
|[DlIJbeLe4iNKMOQd.htm](pfs-season-2-bestiary/DlIJbeLe4iNKMOQd.htm)|Run-Down Efreet|Éfrit délabré|libre|
|[dnClDBmInobiLLDY.htm](pfs-season-2-bestiary/dnClDBmInobiLLDY.htm)|Stranded Melody (7-8)|Mélodie échouée (7-8)|libre|
|[dqPqUX69bPiVdMNG.htm](pfs-season-2-bestiary/dqPqUX69bPiVdMNG.htm)|Lesser Bruorsivi Mandragora|Mandragore Bruorsivi inférieur|libre|
|[DVqAA7JxUykHG2oT.htm](pfs-season-2-bestiary/DVqAA7JxUykHG2oT.htm)|Empowered Blood (3-4)|Renforcement de sang (3-4)|libre|
|[dXvAp2TjUxpE16nS.htm](pfs-season-2-bestiary/dXvAp2TjUxpE16nS.htm)|Zombie Grindylow|Strangulot zombie|libre|
|[DyADskqXgDZlRWz3.htm](pfs-season-2-bestiary/DyADskqXgDZlRWz3.htm)|Urxehl's Firestorm (9-10)|Tempête de feu d'Urxehl (9-10)|libre|
|[DYEJU8PPBg6yWGJe.htm](pfs-season-2-bestiary/DYEJU8PPBg6yWGJe.htm)|Mercenary Trapper (3-4)|Trappeur mercenaire (3-4)|libre|
|[E0ShG9xKRN88vUdP.htm](pfs-season-2-bestiary/E0ShG9xKRN88vUdP.htm)|Wounded Dragon Turtle (5-6)|Tortue dragon blessé (5-6)|libre|
|[E0V3aKLWuXyWrY8C.htm](pfs-season-2-bestiary/E0V3aKLWuXyWrY8C.htm)|Demoniac Spirit (7-8)|Esprit démoniaque (7-8)|libre|
|[E3KaNlIYwSD4fpfL.htm](pfs-season-2-bestiary/E3KaNlIYwSD4fpfL.htm)|Dragon's Death (1-2)|Mort du dragon (1-2)|libre|
|[E8mFpCpTrhiBfYcp.htm](pfs-season-2-bestiary/E8mFpCpTrhiBfYcp.htm)|Smoldering Moss Monster|Monstre bryacé incandescent|libre|
|[Ek6C2abooQizETsX.htm](pfs-season-2-bestiary/Ek6C2abooQizETsX.htm)|Draugr Raider|Pilleur Draugr|libre|
|[EK8DKarizcVrLG5E.htm](pfs-season-2-bestiary/EK8DKarizcVrLG5E.htm)|Demoniac Spirit (9-10)|Esprit démoniaque (9-10)|libre|
|[encctJrtSpWpAQaV.htm](pfs-season-2-bestiary/encctJrtSpWpAQaV.htm)|Abyssal Firestorm Surge (9-10)|Déferlement de feu chthonien (Abyssal) (9-10)|libre|
|[ErhCUif0jmnDKogH.htm](pfs-season-2-bestiary/ErhCUif0jmnDKogH.htm)|Collapsing Cabinet (1-2)|Effondrement de l'armoire (1-2)|libre|
|[EVWVIc2zNT18kZ2T.htm](pfs-season-2-bestiary/EVWVIc2zNT18kZ2T.htm)|Weak Lion|Lion affaibli|libre|
|[F22a7CKj2leYfxyo.htm](pfs-season-2-bestiary/F22a7CKj2leYfxyo.htm)|Hobgoblin Skeleton|Squelette hobgobelin|libre|
|[f4tnLJMEjq9IUMNZ.htm](pfs-season-2-bestiary/f4tnLJMEjq9IUMNZ.htm)|Urkhas (3-4)|Urkhas (3-4)|libre|
|[FCs8aeSiwqnSyTMF.htm](pfs-season-2-bestiary/FCs8aeSiwqnSyTMF.htm)|Wind Malevolent (3-4)|Vent Malveillant (3-4)|libre|
|[fF28drhtacyoHU96.htm](pfs-season-2-bestiary/fF28drhtacyoHU96.htm)|Skeletal Reveler|Fêtard squelettique|libre|
|[fHDBl7jIleuzmOM2.htm](pfs-season-2-bestiary/fHDBl7jIleuzmOM2.htm)|Scorched Earth Orc (Sharpshooter) (5-6)|Orc de la Terre brûlée (Tireur d'élite) (5-6)|libre|
|[fNX8O2z4d3peG5zJ.htm](pfs-season-2-bestiary/fNX8O2z4d3peG5zJ.htm)|Foretold Ruin (3-4)|Destruction annoncée (3-4)|libre|
|[fpKKNDabU5XPov30.htm](pfs-season-2-bestiary/fpKKNDabU5XPov30.htm)|Ash Skeleton (1-2)|Squelette de cendres (1-2)|libre|
|[fSeiGTAcD0mVUz5w.htm](pfs-season-2-bestiary/fSeiGTAcD0mVUz5w.htm)|Corrupted Wildfire (3-4)|Feu sauvage corrompu (3-4)|libre|
|[gAoKDxmIriTC7xVn.htm](pfs-season-2-bestiary/gAoKDxmIriTC7xVn.htm)|Churning Lava (1-2)|Lave bouillonnante (1-2)|libre|
|[ggYXvNjK12pVwJ0P.htm](pfs-season-2-bestiary/ggYXvNjK12pVwJ0P.htm)|Goblin Skeleton|Squelette gobelin|libre|
|[GIJOVHi9G7tcoMds.htm](pfs-season-2-bestiary/GIJOVHi9G7tcoMds.htm)|Lesser Skeletal Cyclops|Cyclope squelettique inférieur|libre|
|[gKAUuRJoKEoYo3Ub.htm](pfs-season-2-bestiary/gKAUuRJoKEoYo3Ub.htm)|Flames of Verakivhan (5-6)|Flammes de Vérakivhan (5-6)|libre|
|[GL9QRBCuNKjuvZSE.htm](pfs-season-2-bestiary/GL9QRBCuNKjuvZSE.htm)|Clouded Quartz (7-8)|Quartz nuageux (7-8)|libre|
|[GopDDbkjVhXu4cwt.htm](pfs-season-2-bestiary/GopDDbkjVhXu4cwt.htm)|Plague Zombie (PFS 2-15)|Zombie pestifiérée (PFS 2-15)|libre|
|[gOPvMgZIQXidd1n8.htm](pfs-season-2-bestiary/gOPvMgZIQXidd1n8.htm)|Demonplague Zombie|Zombie démon-pestiféré|libre|
|[GPEyzpKkfrZIIF9L.htm](pfs-season-2-bestiary/GPEyzpKkfrZIIF9L.htm)|Desiccated Giant Crawling Hand|Main rampante géante déssechée|libre|
|[H8kJUgCxKicEMUFz.htm](pfs-season-2-bestiary/H8kJUgCxKicEMUFz.htm)|Mushroom Ring (5-6)|Cercle de champignons (5-6)|libre|
|[hCZIkwJQQTMwdjfy.htm](pfs-season-2-bestiary/hCZIkwJQQTMwdjfy.htm)|Tough Small Opossum|Petit opossum costaud|libre|
|[hSUo6Y5Cc26Eckd7.htm](pfs-season-2-bestiary/hSUo6Y5Cc26Eckd7.htm)|Onyx Alliance Agent (5-6)|Agent de l'Alliance Onyx (5-6)|libre|
|[i3mgIwXZeJxwFWNI.htm](pfs-season-2-bestiary/i3mgIwXZeJxwFWNI.htm)|Planar Nixie|Nixie planaire|libre|
|[ib1c9CIOEXxnEfQG.htm](pfs-season-2-bestiary/ib1c9CIOEXxnEfQG.htm)|Hand Of Urxehl (9-10)|Main d'Urxehl (9-10)|libre|
|[iEQyYzFBvgyD5yBy.htm](pfs-season-2-bestiary/iEQyYzFBvgyD5yBy.htm)|Wind Malevolent (5-6)|Vent Malveillant (5-6)|libre|
|[IF1DWtelPtUF5aKh.htm](pfs-season-2-bestiary/IF1DWtelPtUF5aKh.htm)|Scorched Cavern Troll|Troll des cavernes brûlé|libre|
|[IHh8MUVqP315OCZ4.htm](pfs-season-2-bestiary/IHh8MUVqP315OCZ4.htm)|Blighted Fungus Leshy|Léchi de feuille dégradé|libre|
|[iqGqGi066OvLdgKb.htm](pfs-season-2-bestiary/iqGqGi066OvLdgKb.htm)|The Saboteur (3-4)|Le saboteur (3-4)|libre|
|[iQprqoS2RXWPMAMQ.htm](pfs-season-2-bestiary/iQprqoS2RXWPMAMQ.htm)|Twigjack (PFS 2-08)|Homme-brindille (PFS 2-08)|libre|
|[iryp7aFx7CUaIksJ.htm](pfs-season-2-bestiary/iryp7aFx7CUaIksJ.htm)|Daughter of Cocytus Tough|Fille de Cocytus la Dure|libre|
|[iuHyixlsXUA4KJ9Q.htm](pfs-season-2-bestiary/iuHyixlsXUA4KJ9Q.htm)|Tough Skeletal Reveler|Fêtard squelette coriace|libre|
|[IxDPownROvUhSU21.htm](pfs-season-2-bestiary/IxDPownROvUhSU21.htm)|Ash Skeleton (3-4)|Squelette de cendres (3-4)|libre|
|[j7K9SefeHJ16qHMl.htm](pfs-season-2-bestiary/j7K9SefeHJ16qHMl.htm)|Anatomical Model (3-4)|Modèle anatomique (3-4)|libre|
|[jCyYRx0Kkaf0TNfF.htm](pfs-season-2-bestiary/jCyYRx0Kkaf0TNfF.htm)|Electric Lock Rune|Rune de verrou électrique|libre|
|[jFfxjbvNtfWyHHCK.htm](pfs-season-2-bestiary/jFfxjbvNtfWyHHCK.htm)|Bloodthirsty Razortoothed Shark|Requin aux dents rasoirs assoiffé de sang|libre|
|[Jie7zk3ct2AE92Om.htm](pfs-season-2-bestiary/Jie7zk3ct2AE92Om.htm)|Ssalarn|Ssalarn|libre|
|[jjx1CL2NpFG78tOZ.htm](pfs-season-2-bestiary/jjx1CL2NpFG78tOZ.htm)|Small Opossum|Petit opossum|libre|
|[JxJ3vbV7piLxZrJ9.htm](pfs-season-2-bestiary/JxJ3vbV7piLxZrJ9.htm)|The Scholar of Sorts (3-4)|Une sorte d'érudit (3-4)|libre|
|[jZ5M9JxemkGVpfLy.htm](pfs-season-2-bestiary/jZ5M9JxemkGVpfLy.htm)|Awakened Giant Frilled Lizard|Lézard à collerette géant éveillé|libre|
|[KJAk6ALQk9Qkt53l.htm](pfs-season-2-bestiary/KJAk6ALQk9Qkt53l.htm)|Kobold Warrior (PFS 2-11)|Guerrier kobold (PFS 2-11)|libre|
|[Kklhv0OWqkb6RbIF.htm](pfs-season-2-bestiary/Kklhv0OWqkb6RbIF.htm)|Cobbleswarm (B5)|Nuée de teignes-pavés (B5)|libre|
|[kRCNfSMqwHY9sYcg.htm](pfs-season-2-bestiary/kRCNfSMqwHY9sYcg.htm)|Semyon (5-6)|Semyon (5-6)|libre|
|[KvFN6OFxOnTYjdJq.htm](pfs-season-2-bestiary/KvFN6OFxOnTYjdJq.htm)|Perizia (5-6)|Périzia (5-6)|libre|
|[KVvRtfyqewytq40f.htm](pfs-season-2-bestiary/KVvRtfyqewytq40f.htm)|Hunting Spider (PFS 2-18)|Araignée chasseresse (PFS 2-18)|libre|
|[LdZgV7b5BhCF0Ye6.htm](pfs-season-2-bestiary/LdZgV7b5BhCF0Ye6.htm)|Collapsing Ramp (7-8)|Effondrement de passerelle (7-8)|libre|
|[ljs6Ueki3nmInJ9v.htm](pfs-season-2-bestiary/ljs6Ueki3nmInJ9v.htm)|Archis Peers (3-4)|Archis Peers (3-4)|libre|
|[LxPHOPYyrNhDNFod.htm](pfs-season-2-bestiary/LxPHOPYyrNhDNFod.htm)|Chained Drained Troll|Troll enchaîné affaibli|libre|
|[LZiiTvbxfgJ3QfIQ.htm](pfs-season-2-bestiary/LZiiTvbxfgJ3QfIQ.htm)|Ice Slick|Couche de glace dérapante|libre|
|[M1mbQnk40lu1lxdr.htm](pfs-season-2-bestiary/M1mbQnk40lu1lxdr.htm)|Young Ahuizotl|Jeune Ahuizotl|libre|
|[M9AglEGo8jMH9CnU.htm](pfs-season-2-bestiary/M9AglEGo8jMH9CnU.htm)|Churning Lava (3-4)|Lave bouillonnante (3-4)|libre|
|[mFfhFprmaQr9rYC1.htm](pfs-season-2-bestiary/mFfhFprmaQr9rYC1.htm)|Drandle Dreng (7-8)|Drandle Dreng (7-8)|libre|
|[mfvNh0UBzR1vXHRW.htm](pfs-season-2-bestiary/mfvNh0UBzR1vXHRW.htm)|Hands of Slow Death|Mains de la mort lente|libre|
|[mot8pWHD6YEj08Js.htm](pfs-season-2-bestiary/mot8pWHD6YEj08Js.htm)|Sea Devil Impaler|Diable des mers empaleur|libre|
|[mREQ0kbYYSYsYb4e.htm](pfs-season-2-bestiary/mREQ0kbYYSYsYb4e.htm)|Weak Mist Stalker|Rôdeur de brume affaibli|libre|
|[nA6U7TiEPyEbcio6.htm](pfs-season-2-bestiary/nA6U7TiEPyEbcio6.htm)|Mushroom Ring (3-4)|Cercle de champignons (3-4)|libre|
|[NAUPuR7aWSQ6mBUs.htm](pfs-season-2-bestiary/NAUPuR7aWSQ6mBUs.htm)|Usij Cultist|Cultiste Usij|libre|
|[NAX7nXWzSkrjjsrq.htm](pfs-season-2-bestiary/NAX7nXWzSkrjjsrq.htm)|Awakened Megalania|Mégalania éveillé|libre|
|[NFKQObG0b0lVsvgS.htm](pfs-season-2-bestiary/NFKQObG0b0lVsvgS.htm)|Unstable Ice|Glace instable|libre|
|[NGNU5Rq90TmNhwhN.htm](pfs-season-2-bestiary/NGNU5Rq90TmNhwhN.htm)|Kayajima Guardian Dogu (3-4)|Garde dogu Kayajima (3-4)|libre|
|[NioUzYo4svyO981f.htm](pfs-season-2-bestiary/NioUzYo4svyO981f.htm)|Relive the Rending (1-2)|Revivre la Déchirure (1-2)|libre|
|[NMD2wE4GLfoS41IB.htm](pfs-season-2-bestiary/NMD2wE4GLfoS41IB.htm)|Wishbound Mist Stalker (7-8)|Rôdeurs de brume wishbound (7-8)|libre|
|[nSFA4Kei2Gyf7dc6.htm](pfs-season-2-bestiary/nSFA4Kei2Gyf7dc6.htm)|Murta Kronniksdottir (5-6)|Murta Kronniksdottir (5-6)|libre|
|[nu3o0lcRZ6Bk57pA.htm](pfs-season-2-bestiary/nu3o0lcRZ6Bk57pA.htm)|Iverri (5-6)|Iverri (5-6)|libre|
|[nUweXdZmsMFLcEsM.htm](pfs-season-2-bestiary/nUweXdZmsMFLcEsM.htm)|Weak Sea Devil Scout|Éclaireur diable des mers affaibli|libre|
|[o13KISidrNovd4jp.htm](pfs-season-2-bestiary/o13KISidrNovd4jp.htm)|Cyclops Bodak|Cyclope bodak|libre|
|[O78tQX7291q1CADm.htm](pfs-season-2-bestiary/O78tQX7291q1CADm.htm)|Bruorsivi Mandragora|Mandragore Bruorsivi|libre|
|[OMEVkAFReRtjj1zb.htm](pfs-season-2-bestiary/OMEVkAFReRtjj1zb.htm)|Sticky Web Lurker Noose|Noeud coulant collant du rôdeur des toiles|libre|
|[OSNYHI3K4Ir8DVof.htm](pfs-season-2-bestiary/OSNYHI3K4Ir8DVof.htm)|Planar Cracks (7-8)|Fissures planaires (7-8)|libre|
|[P8fk8icXxlR6VVcm.htm](pfs-season-2-bestiary/P8fk8icXxlR6VVcm.htm)|Mercenary Wizard (3-4)|Mage mercenaire (3-4)|libre|
|[p9guvHB0pj7Q4tOx.htm](pfs-season-2-bestiary/p9guvHB0pj7Q4tOx.htm)|Drandlesticks (7-8)|Drandlesticks (7-8)|libre|
|[P9xoZGN9pSqNPbRK.htm](pfs-season-2-bestiary/P9xoZGN9pSqNPbRK.htm)|Mechanical Jaws (1-2)|Mâchoires mécaniques (1-2)|libre|
|[pe7Pb0vsbah7L299.htm](pfs-season-2-bestiary/pe7Pb0vsbah7L299.htm)|Flames of Verakivhan (3-4)|Flammes de Vérakivhan (3-4)|libre|
|[peEfisGfqzcCxaRf.htm](pfs-season-2-bestiary/peEfisGfqzcCxaRf.htm)|Urkhas (5-6)|Urkhas (5-6)|libre|
|[PFRXopyGSJmdYE6Y.htm](pfs-season-2-bestiary/PFRXopyGSJmdYE6Y.htm)|Kayajima Guardian Dogu (5-6)|Garde dogu Kayajima (5-6)|libre|
|[PH7M0jJ88cdGaNq9.htm](pfs-season-2-bestiary/PH7M0jJ88cdGaNq9.htm)|Dragon's Death (3-4)|Mort du dragon (3-4)|libre|
|[PIAR4xFEhkWlWg8D.htm](pfs-season-2-bestiary/PIAR4xFEhkWlWg8D.htm)|Hidden Bog|Tourbière cachée|libre|
|[pmuLQSwB0530m873.htm](pfs-season-2-bestiary/pmuLQSwB0530m873.htm)|Daughter of Cocytus Grunt|Fille de Cocytus la Subalterne|libre|
|[pNPUo3lDaw7ZoDek.htm](pfs-season-2-bestiary/pNPUo3lDaw7ZoDek.htm)|Chaotic Door (3-4)|Porte chaotique (3-4)|libre|
|[psJpwY07YDdsQsgL.htm](pfs-season-2-bestiary/psJpwY07YDdsQsgL.htm)|Planar Cracks (5-6)|Fissures planaires (5-6)|libre|
|[PUb8Ipy6NKAMDFQw.htm](pfs-season-2-bestiary/PUb8Ipy6NKAMDFQw.htm)|Skeletal Cyclops|Cyclope squelettique|libre|
|[pUcBzyZY5kY7ERNN.htm](pfs-season-2-bestiary/pUcBzyZY5kY7ERNN.htm)|Perizia (7-8)|Périzia (7-8)|libre|
|[PZ0PjHKcmcekBTSp.htm](pfs-season-2-bestiary/PZ0PjHKcmcekBTSp.htm)|Ogthup (5-6)|Ogthup (5-6)|libre|
|[q5GtWMOvluZVt8Gx.htm](pfs-season-2-bestiary/q5GtWMOvluZVt8Gx.htm)|Mercenary Trapper (1-2)|Trappeur mercenaire (1-2)|libre|
|[q5ObT0toaicC2OyP.htm](pfs-season-2-bestiary/q5ObT0toaicC2OyP.htm)|Nuglub (PFS 2-00)|Nuglub (PFS 2-00)|libre|
|[Qdx9UbT68Elqneti.htm](pfs-season-2-bestiary/Qdx9UbT68Elqneti.htm)|Debilitated Bulette|Bulette affaiblie|libre|
|[qG5e77fMVkjBlK8B.htm](pfs-season-2-bestiary/qG5e77fMVkjBlK8B.htm)|Commoner (PFS 2-13)|Roturier ((PFS 2-13))|libre|
|[QpNynyK54qEwmswI.htm](pfs-season-2-bestiary/QpNynyK54qEwmswI.htm)|Frost Troll Hunter|Chasseur Troll des glaces|libre|
|[QQ93gOZxsfTdTSn2.htm](pfs-season-2-bestiary/QQ93gOZxsfTdTSn2.htm)|Storm Bear (3-4)|Ours de la tempête (3-4)|libre|
|[qZFgHBqfjIaBviTS.htm](pfs-season-2-bestiary/qZFgHBqfjIaBviTS.htm)|Aspis Guard|Garde de l'Aspis|libre|
|[R1QGjRlquOmfH2Zx.htm](pfs-season-2-bestiary/R1QGjRlquOmfH2Zx.htm)|Exploding Skeleton Guard|Garde squelette explosif|libre|
|[R58hBfz4R5QQtFnD.htm](pfs-season-2-bestiary/R58hBfz4R5QQtFnD.htm)|Awakened Ball Python|Python boule éveillé|libre|
|[R7LYEfEApIhp6jdp.htm](pfs-season-2-bestiary/R7LYEfEApIhp6jdp.htm)|Joyful Planar Nixie|Nixie planaire joyeuse|libre|
|[rCErTabCxzPbR0ON.htm](pfs-season-2-bestiary/rCErTabCxzPbR0ON.htm)|Insidious Dragon Forest Rot|Dragon des forêts putréfié insidieux|libre|
|[rFh82u8Jb6ZaGkm3.htm](pfs-season-2-bestiary/rFh82u8Jb6ZaGkm3.htm)|Artennod Raike (5-6)|Artennod Raike (5-6)|libre|
|[rFRbYNAH3LXRXXrS.htm](pfs-season-2-bestiary/rFRbYNAH3LXRXXrS.htm)|Silaqui (3-4)|Silaqui (3-4)|libre|
|[RmcCPvmrAeSTx3BR.htm](pfs-season-2-bestiary/RmcCPvmrAeSTx3BR.htm)|Burning Sun Orc (7-8)|Orc du Soleil brûlant (7-8)|libre|
|[RO4UAvgak0jAo2We.htm](pfs-season-2-bestiary/RO4UAvgak0jAo2We.htm)|Burning Sun Orc (5-6)|Orc du Soleil brûlant (5-6)|libre|
|[SFBumlEX6jDIwAvL.htm](pfs-season-2-bestiary/SFBumlEX6jDIwAvL.htm)|Saddleback Bunyip|Bunyip Sacoche de selle|libre|
|[SgRcBE1CNLlGXbwz.htm](pfs-season-2-bestiary/SgRcBE1CNLlGXbwz.htm)|Silaqui (5-6)|Silaqui (5-6)|libre|
|[SHmxvGZAPxkttd06.htm](pfs-season-2-bestiary/SHmxvGZAPxkttd06.htm)|Wounded Dragon Turtle (3-4)|Tortue-dragon blessé (3-4)|libre|
|[sm6SKZaGvDSAAwde.htm](pfs-season-2-bestiary/sm6SKZaGvDSAAwde.htm)|Sezruth (1-2)|Sézruth (1-2)|libre|
|[sOTeRfQ6NB3CtqBX.htm](pfs-season-2-bestiary/sOTeRfQ6NB3CtqBX.htm)|Quickling (PFS 2-00)|Viflin (PFS 2-00)|libre|
|[SqAWqJEi3IVB5TbO.htm](pfs-season-2-bestiary/SqAWqJEi3IVB5TbO.htm)|Kayajima Daeodon|Daeodon Kayajima|libre|
|[sRXkrqt5mOXzceR5.htm](pfs-season-2-bestiary/sRXkrqt5mOXzceR5.htm)|Dragon Forest Rot|Dragon des forêts putréfié|libre|
|[StqA0Lpk26G4Ux1l.htm](pfs-season-2-bestiary/StqA0Lpk26G4Ux1l.htm)|The Saboteur (5-6)|Le Saboteur (5-6)|libre|
|[SX348ZSY0SyMfOXa.htm](pfs-season-2-bestiary/SX348ZSY0SyMfOXa.htm)|Flaming Eye (3-4)|Oeil enflammé (3-4)|libre|
|[SXXftJEy3DL8XXfF.htm](pfs-season-2-bestiary/SXXftJEy3DL8XXfF.htm)|Scorched Earth Orc (Clipper) (5-6)|Orc de la Terre brûlée (Clipper) (5-6)|libre|
|[Syfo21iDW5wnYkLS.htm](pfs-season-2-bestiary/Syfo21iDW5wnYkLS.htm)|Stranded Melody (5-6)|Mélodie échouée (5-6)|libre|
|[T2IXTYeb4Ky6l1D8.htm](pfs-season-2-bestiary/T2IXTYeb4Ky6l1D8.htm)|Relive the Rending (3-4)|Revivre la Déchirure (3-4)|libre|
|[tLCIfeC1WWuGB9zj.htm](pfs-season-2-bestiary/tLCIfeC1WWuGB9zj.htm)|Baron Utomo (3-4)|Baron Utomo (3-4)|libre|
|[TX7WPf5f7lq3AufA.htm](pfs-season-2-bestiary/TX7WPf5f7lq3AufA.htm)|Jinkin (PFS 2-00)|Jinkin (PFS 2-00)|libre|
|[TXHl1KvScSce9bQV.htm](pfs-season-2-bestiary/TXHl1KvScSce9bQV.htm)|Drandle Dreng (9-10)|Drangle Dreng (9-10)|libre|
|[tY5snIGj4jBSJcay.htm](pfs-season-2-bestiary/tY5snIGj4jBSJcay.htm)|Ogre Spider (PFS 2-18)|Araignée ogre (PFS 2-18)|libre|
|[U3RoZnfVAAKbrULX.htm](pfs-season-2-bestiary/U3RoZnfVAAKbrULX.htm)|Disciple of Urxehl (9-10)|Disciple d'Urxehl (9-10)|libre|
|[U6gghw2Ke21cRzs0.htm](pfs-season-2-bestiary/U6gghw2Ke21cRzs0.htm)|Brittle Skeletal Horse|Cheval squelettique cassant|libre|
|[UIAaR0AdABRgGGNc.htm](pfs-season-2-bestiary/UIAaR0AdABRgGGNc.htm)|Chesjilawa Jadwiga Karina (5-6)|Chesjilawa Jadwiga Karina (5-6)|libre|
|[UkcHfRJUx1OHELZm.htm](pfs-season-2-bestiary/UkcHfRJUx1OHELZm.htm)|Duergar Raider|Pilleur hryngar (duergar)|libre|
|[UNdSfmCEICYna9ki.htm](pfs-season-2-bestiary/UNdSfmCEICYna9ki.htm)|Foretold Ruin (1-2)|Destruction annoncée (1-2)|libre|
|[unfgNRDtbFivlOEJ.htm](pfs-season-2-bestiary/unfgNRDtbFivlOEJ.htm)|Weak Mudwretch|Bourbeux affaibli|libre|
|[UngnjxTXgiKzBTuB.htm](pfs-season-2-bestiary/UngnjxTXgiKzBTuB.htm)|Storasta's Last Stand (5-6)|Dernier bastion de Storasta (5-6)|libre|
|[uROqM79s9ivzHzl3.htm](pfs-season-2-bestiary/uROqM79s9ivzHzl3.htm)|Ash Archer|Archer des cendres|libre|
|[urrUBt8FFtuw3LNv.htm](pfs-season-2-bestiary/urrUBt8FFtuw3LNv.htm)|Drandlesticks (9-10)|Drandlesticks (9-10)|libre|
|[UtMuL2qXMHxt2ccB.htm](pfs-season-2-bestiary/UtMuL2qXMHxt2ccB.htm)|Mushroom Ring (7-8)|Cercle de champignons (7-8)|libre|
|[V5O0ecsMk0QSTpqJ.htm](pfs-season-2-bestiary/V5O0ecsMk0QSTpqJ.htm)|Crumbling Ravener Husk|Vestige de dévoreur effrité|libre|
|[VbRX9bK5kLIJWnXH.htm](pfs-season-2-bestiary/VbRX9bK5kLIJWnXH.htm)|Sezruth (3-4)|Sézruth (3-4)|libre|
|[VjVKGrVXJd7HRn5h.htm](pfs-season-2-bestiary/VjVKGrVXJd7HRn5h.htm)|Chaotic Door (1-2)|Porte chaotique (1-2)|libre|
|[vqgcpFLXabsGj7RQ.htm](pfs-season-2-bestiary/vqgcpFLXabsGj7RQ.htm)|Awakened Giant Chameleon|Caméléon géant éveillé|libre|
|[VWjNkgTXFPqO0Qc5.htm](pfs-season-2-bestiary/VWjNkgTXFPqO0Qc5.htm)|Pressurized Bottle (3-4)|Bouteille sous pression (3-4)|libre|
|[W9XSFbMIXk1A7aT9.htm](pfs-season-2-bestiary/W9XSFbMIXk1A7aT9.htm)|Scorched Earth Mephit|Méphite de la Terre brûlée|libre|
|[WEo5SYs2SPZkU6lu.htm](pfs-season-2-bestiary/WEo5SYs2SPZkU6lu.htm)|Animated Guardian|Gardien animé|libre|
|[WisQuxckwd6XU5MF.htm](pfs-season-2-bestiary/WisQuxckwd6XU5MF.htm)|Sliding Statue (3-4)|Statue coulissante (3-4)|libre|
|[wt2bOysQCOVDRGJu.htm](pfs-season-2-bestiary/wt2bOysQCOVDRGJu.htm)|Wishbound Mist Stalker (5-6)|Rôdeur de brume wishbound (5-6)|libre|
|[wTr7zLN8is9720ub.htm](pfs-season-2-bestiary/wTr7zLN8is9720ub.htm)|Mechanical Jaws (3-4)|Mâchoires mécaniques (3-4)|libre|
|[wYPS72mQp7VqullU.htm](pfs-season-2-bestiary/wYPS72mQp7VqullU.htm)|Artennod Raike (3-4)|Artennod Raike (3-4)|libre|
|[X0HPqlpqFHqmB9Ni.htm](pfs-season-2-bestiary/X0HPqlpqFHqmB9Ni.htm)|Valgomorus (1-2)|Valgomorus (1-2)|libre|
|[x4m5ks6Rd8fYzXPm.htm](pfs-season-2-bestiary/x4m5ks6Rd8fYzXPm.htm)|Daughter Of Cocytus Enforcer|Fille de Cocytus Exécutrice|libre|
|[X6ODple6SbEvbmmj.htm](pfs-season-2-bestiary/X6ODple6SbEvbmmj.htm)|Corvius Vayn (7-8)|Corvius Vayn (7-8)|libre|
|[X7YcO5G0hpDP46EW.htm](pfs-season-2-bestiary/X7YcO5G0hpDP46EW.htm)|Greeleep (5-6)|Greeleep (5-6)|libre|
|[X951Ow3nHOvBFaFg.htm](pfs-season-2-bestiary/X951Ow3nHOvBFaFg.htm)|Mephit Swarm (7-8)|Nuée de méphites (7-8)|libre|
|[Xgt8hV1WTSxQLv7y.htm](pfs-season-2-bestiary/Xgt8hV1WTSxQLv7y.htm)|Collapsing Ice|Effondrement de la glace|libre|
|[XKrlU3hiLBTPg95p.htm](pfs-season-2-bestiary/XKrlU3hiLBTPg95p.htm)|Pixie (PFS 2-00)|Pixie (PFS 2-00)|libre|
|[xmKHQAQ9z7XrPNp7.htm](pfs-season-2-bestiary/xmKHQAQ9z7XrPNp7.htm)|Russian Soldier (7-8)|Solat russe (7-8)|libre|
|[XoE0tsQ4twaoNtBu.htm](pfs-season-2-bestiary/XoE0tsQ4twaoNtBu.htm)|Collapsing Ramp (9-10)|Effondrement de la passerelle (9-10)|libre|
|[xRwAQ93HjGe59RvO.htm](pfs-season-2-bestiary/xRwAQ93HjGe59RvO.htm)|Ygracix|Ygracix|libre|
|[XUyHffBEQ7K2bsiP.htm](pfs-season-2-bestiary/XUyHffBEQ7K2bsiP.htm)|Flooding Monolith (5-6)|Monolithe d'inondation (5-6)|libre|
|[XVda3dqZWi21we2j.htm](pfs-season-2-bestiary/XVda3dqZWi21we2j.htm)|Ragewight (3-4)|Nécrophage enragé (3-4)|libre|
|[xW5IhEbAhQneniT2.htm](pfs-season-2-bestiary/xW5IhEbAhQneniT2.htm)|Storm Bear (1-2)|Ours de la tempête (1-2)|libre|
|[XZOpmROxLS1bnApy.htm](pfs-season-2-bestiary/XZOpmROxLS1bnApy.htm)|Mother Forsythe (3-4)|Mère Forsythe (3-4)|libre|
|[Y0qzWOi3ihBHwYWE.htm](pfs-season-2-bestiary/Y0qzWOi3ihBHwYWE.htm)|Ferocious Zombie Grindylow|Strangulot zombie féroce|libre|
|[Y2tEkaZK5j2NySJu.htm](pfs-season-2-bestiary/Y2tEkaZK5j2NySJu.htm)|Sinkhole|Doline|libre|
|[yCACZfj6PJJGCLcr.htm](pfs-season-2-bestiary/yCACZfj6PJJGCLcr.htm)|Murta Kronniksdottir (3-4)|Murta Kronniksdottir (3-4)|libre|
|[yEvWtmFxwVsNGjWh.htm](pfs-season-2-bestiary/yEvWtmFxwVsNGjWh.htm)|Unkillable Zombie Shambler|Titubeur zombie intuable|libre|
|[YpACDyWCHXtaXppg.htm](pfs-season-2-bestiary/YpACDyWCHXtaXppg.htm)|Scorched Earth Orc (Clipper) (7-8)|Orc de la Terre brûlée (Clipper) (7-8)|libre|
|[yQm21z0RyZgZWcpm.htm](pfs-season-2-bestiary/yQm21z0RyZgZWcpm.htm)|Baron Utomo (5-6)|Baron Utomo (5-6)|libre|
|[yqOuv6qNnJgCFPn1.htm](pfs-season-2-bestiary/yqOuv6qNnJgCFPn1.htm)|Spark Troll (3-4)|Troll étincelle (3-4)|libre|
|[z5YeacI4avkX3etA.htm](pfs-season-2-bestiary/z5YeacI4avkX3etA.htm)|Iverri (3-4)|Iverri (3-4)|libre|
|[Z6qNXxgTgIz4rH2C.htm](pfs-season-2-bestiary/Z6qNXxgTgIz4rH2C.htm)|Mishka The Bear (5-6)|L'ours Mishka (5-6)|libre|
|[zihTF7JohmZM8cdS.htm](pfs-season-2-bestiary/zihTF7JohmZM8cdS.htm)|Giant Worker Ant|Fourmi travailleuse géante|libre|
|[zLeePJplf25baqJa.htm](pfs-season-2-bestiary/zLeePJplf25baqJa.htm)|Charmed Crab|Crabe enchanté|libre|
|[zNN1Af6Xx5Tmglcu.htm](pfs-season-2-bestiary/zNN1Af6Xx5Tmglcu.htm)|Tewakam Nekotek (5-6)|Téwakam Nékotek (5-6)|libre|
|[zpIVn1k5nsR0sMZ8.htm](pfs-season-2-bestiary/zpIVn1k5nsR0sMZ8.htm)|Ragewight (5-6)|Nécrophage enragé (5-6)|libre|
|[ZpSLUDoO0WmxwLCG.htm](pfs-season-2-bestiary/ZpSLUDoO0WmxwLCG.htm)|Onyx Alliance Agent (7-8)|Agent de l'Alliance Onyx (7-8)|libre|
|[ZshsztmoWjDZEqGT.htm](pfs-season-2-bestiary/ZshsztmoWjDZEqGT.htm)|Elite Anatomical Model (3-4)|Modèle anatomique d'élite (3-4)|libre|
|[zSuXInzH5E8rYNEl.htm](pfs-season-2-bestiary/zSuXInzH5E8rYNEl.htm)|Vermlek Centaur (5-6)|Vermlek Centaure (5-6)|libre|
|[ZzBtsGGjEvlqpiDq.htm](pfs-season-2-bestiary/ZzBtsGGjEvlqpiDq.htm)|Strak (7-8)|Strak (7-8)|libre|
|[ZzCskCNtjDetelAi.htm](pfs-season-2-bestiary/ZzCskCNtjDetelAi.htm)|Storasta's Last Stand (3-4)|Le dernier bastion de Storasta (3-4)|libre|
