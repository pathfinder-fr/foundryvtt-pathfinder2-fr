# État de la traduction (rage-of-elements-bestiary)

 * **libre**: 80
 * **changé**: 1


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[g36FR0xlbMSvWqdS.htm](rage-of-elements-bestiary/g36FR0xlbMSvWqdS.htm)|Talos Gadgeteer|gadgetiste Talos|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[1cpG2DuXPXq3DMBx.htm](rage-of-elements-bestiary/1cpG2DuXPXq3DMBx.htm)|Jabali Shuyookh|Shuyookh Jabali|libre|
|[3ItvROg0JbYKUGRu.htm](rage-of-elements-bestiary/3ItvROg0JbYKUGRu.htm)|Brochmaw|Gueule-broch'|libre|
|[411wAxSMPPAImVz5.htm](rage-of-elements-bestiary/411wAxSMPPAImVz5.htm)|Brass Bastion|Bastion d'airain|libre|
|[5nf90vfzOVFbGEZZ.htm](rage-of-elements-bestiary/5nf90vfzOVFbGEZZ.htm)|Crystal Strider|Arpenteur de cristal|libre|
|[5Seti8VsPjgd8f6i.htm](rage-of-elements-bestiary/5Seti8VsPjgd8f6i.htm)|Metal Scamp|Crapule de métal|libre|
|[6CYo7vuxoroSqKzr.htm](rage-of-elements-bestiary/6CYo7vuxoroSqKzr.htm)|Zephyr, The West Wind|Zéphyr, le Vent d'Ouest|libre|
|[6teMPgUKnYc1YIIx.htm](rage-of-elements-bestiary/6teMPgUKnYc1YIIx.htm)|Pine Pangolin|Pangolin des pins|libre|
|[6VCNPJ3xPw4Js52V.htm](rage-of-elements-bestiary/6VCNPJ3xPw4Js52V.htm)|Blustering Gale|Coup de vent|libre|
|[7C6sSVh7jipvHKbV.htm](rage-of-elements-bestiary/7C6sSVh7jipvHKbV.htm)|Vegetable Lamb|Agneau végétal|libre|
|[7qRWMhYwrTHv58VG.htm](rage-of-elements-bestiary/7qRWMhYwrTHv58VG.htm)|Comozant Wyrd|Wyrd comozant|libre|
|[7U27iYyyGK8iGYzR.htm](rage-of-elements-bestiary/7U27iYyyGK8iGYzR.htm)|Austral, The South Wind|Austral, Le Vent du Sud|libre|
|[7Y3DAOMaLzqmRqUh.htm](rage-of-elements-bestiary/7Y3DAOMaLzqmRqUh.htm)|Saltborn Stalkers|Harceleurs Sels-nés|libre|
|[7zKCt0OyooqW9UkL.htm](rage-of-elements-bestiary/7zKCt0OyooqW9UkL.htm)|Ardande Gardener|Jardinier Ardande|libre|
|[8cJlApaLdgZCxfWq.htm](rage-of-elements-bestiary/8cJlApaLdgZCxfWq.htm)|Boreal, The North Wind|Boreal, Le Vent du Nord|libre|
|[a2kTygtqIuMCSLIr.htm](rage-of-elements-bestiary/a2kTygtqIuMCSLIr.htm)|Zuhra Shuyookh|Shuyookh Zuhra|libre|
|[AahAJjsv74rPfAG5.htm](rage-of-elements-bestiary/AahAJjsv74rPfAG5.htm)|Oregorger|Orgorgeur|libre|
|[BKHoKKSWvc7rQoyz.htm](rage-of-elements-bestiary/BKHoKKSWvc7rQoyz.htm)|Mercurial|Mercuriel|libre|
|[bzMyAFhSQ1mfTNx9.htm](rage-of-elements-bestiary/bzMyAFhSQ1mfTNx9.htm)|Nursery Crawler|Rampant pépiniériste|libre|
|[CKRqlCHgvZp9YL5s.htm](rage-of-elements-bestiary/CKRqlCHgvZp9YL5s.htm)|Living Lodestone|Pierre aimantée vivante|libre|
|[d0InnF6QVgDIG4j3.htm](rage-of-elements-bestiary/d0InnF6QVgDIG4j3.htm)|Quickiron Plasm|Plasme de fer-vif|libre|
|[D1WlR977uIedFwAr.htm](rage-of-elements-bestiary/D1WlR977uIedFwAr.htm)|Lava Otter|Loutre de lave|libre|
|[d8801ruMQkpXLcWk.htm](rage-of-elements-bestiary/d8801ruMQkpXLcWk.htm)|Moss Sloth|Paresseux moussu|libre|
|[dLFxm5kZBRzvqf1f.htm](rage-of-elements-bestiary/dLFxm5kZBRzvqf1f.htm)|Eural, The East Wind|Eural, le Vent d'Est|libre|
|[E3n4MNWQNPR2o3bG.htm](rage-of-elements-bestiary/E3n4MNWQNPR2o3bG.htm)|Pelegox Cube|Cube pelegoxe|libre|
|[E7OZ0cdycOi5Cuu1.htm](rage-of-elements-bestiary/E7OZ0cdycOi5Cuu1.htm)|Faydhaan Shuyookh|Shuyookh Faydhaan|libre|
|[EHowgLz8ohFGTGvG.htm](rage-of-elements-bestiary/EHowgLz8ohFGTGvG.htm)|Boiling Spring|Source bouillonnante|libre|
|[EjgbrWiEp37eIifZ.htm](rage-of-elements-bestiary/EjgbrWiEp37eIifZ.htm)|Wood Scamp|Crapule de bois|libre|
|[eMbeqzGyypNU1wTS.htm](rage-of-elements-bestiary/eMbeqzGyypNU1wTS.htm)|Kizidhar Shuyookh|Shuyookh Kizidhar|libre|
|[EXiLRXbweiiMbTNk.htm](rage-of-elements-bestiary/EXiLRXbweiiMbTNk.htm)|Coldmire Pond|Mare givre-mire|libre|
|[g9hnlCy2R4uspfAI.htm](rage-of-elements-bestiary/g9hnlCy2R4uspfAI.htm)|Vault Builder|Constructeur de grotte|libre|
|[GdXjhFu8Afg24fM9.htm](rage-of-elements-bestiary/GdXjhFu8Afg24fM9.htm)|Elder Outcrop|Ancien affleurement|libre|
|[Gqz1qtpdgAl13pCE.htm](rage-of-elements-bestiary/Gqz1qtpdgAl13pCE.htm)|Gennayn|Génnayin|libre|
|[h6oWKFSqpXHnOg5h.htm](rage-of-elements-bestiary/h6oWKFSqpXHnOg5h.htm)|Munsahir Gatecrasher|Munsahir briseur de porte|libre|
|[hGPjJjjqBc42VrFs.htm](rage-of-elements-bestiary/hGPjJjjqBc42VrFs.htm)|Anemos|Anémos|libre|
|[IBfXxdi8ja4095km.htm](rage-of-elements-bestiary/IBfXxdi8ja4095km.htm)|Nightwood Guardian|Gardien du bois de nuit|libre|
|[ikcWmhEdL5G0s9T4.htm](rage-of-elements-bestiary/ikcWmhEdL5G0s9T4.htm)|Painted Stag|Cerf peint|libre|
|[IPKzI7XwILuDc21S.htm](rage-of-elements-bestiary/IPKzI7XwILuDc21S.htm)|Asp of Grief|Aspic des chagrins|libre|
|[jKSirpKNicffmRpW.htm](rage-of-elements-bestiary/jKSirpKNicffmRpW.htm)|Cullitox|Cullitox|libre|
|[JR7VT7ObQRaBtUlk.htm](rage-of-elements-bestiary/JR7VT7ObQRaBtUlk.htm)|Tantriog|Tantriog|libre|
|[ki7r3AMO0lROqzcK.htm](rage-of-elements-bestiary/ki7r3AMO0lROqzcK.htm)|Gluttonous Geode|Géode gloutonne|libre|
|[Kv6zyQC3JCEWWwvf.htm](rage-of-elements-bestiary/Kv6zyQC3JCEWWwvf.htm)|Solar Crow|Corneille solaire|libre|
|[kzczIOOaDfPlFc03.htm](rage-of-elements-bestiary/kzczIOOaDfPlFc03.htm)|Lithic Locus|Locus lithique|libre|
|[lQcfACi4RQrjIl5S.htm](rage-of-elements-bestiary/lQcfACi4RQrjIl5S.htm)|Elemental Thicket|Bosquet élémentaire|libre|
|[LVLf8bfqqLkM56tj.htm](rage-of-elements-bestiary/LVLf8bfqqLkM56tj.htm)|Wood Wisp|Fredon de bois|libre|
|[m2WdV4XJ3r70mnJX.htm](rage-of-elements-bestiary/m2WdV4XJ3r70mnJX.htm)|Rakkatak|Rakkatak|libre|
|[m6lUcyekHwJxTLzo.htm](rage-of-elements-bestiary/m6lUcyekHwJxTLzo.htm)|Vault Keeper|Gardien de grotte|libre|
|[Mw0sQmrSf9hao5mF.htm](rage-of-elements-bestiary/Mw0sQmrSf9hao5mF.htm)|Zuhra|Zuhra|libre|
|[nhIxIC4O6XGUZ8Ng.htm](rage-of-elements-bestiary/nhIxIC4O6XGUZ8Ng.htm)|Nanoshard Swarm|Nuée de nanoéclats|libre|
|[NhmmvWicMmhXuKJo.htm](rage-of-elements-bestiary/NhmmvWicMmhXuKJo.htm)|Snapdrake|Craquedrake|libre|
|[NHWf9R3yN5Iil7Zh.htm](rage-of-elements-bestiary/NHWf9R3yN5Iil7Zh.htm)|Dewdrop Jelly|Gelée de rosée|libre|
|[O3Rjdf00nJOHF4PE.htm](rage-of-elements-bestiary/O3Rjdf00nJOHF4PE.htm)|Jaathoom Shuyookh|Shuyookh Jaathoom|libre|
|[oT99a9W6HcU0WsU2.htm](rage-of-elements-bestiary/oT99a9W6HcU0WsU2.htm)|Whipping Willow|Saule fouettard|libre|
|[OX25euLqeBR07Ul7.htm](rage-of-elements-bestiary/OX25euLqeBR07Ul7.htm)|Kinzaruk|Kinzaruk|libre|
|[P3UcyuiqqYPzAwwF.htm](rage-of-elements-bestiary/P3UcyuiqqYPzAwwF.htm)|Carved Beast|Bête sculptée|libre|
|[PameP0qGSvNrhGeH.htm](rage-of-elements-bestiary/PameP0qGSvNrhGeH.htm)|Avalanche Legion|Légion d'avalanches|libre|
|[qdVEmjf6e98KPzyK.htm](rage-of-elements-bestiary/qdVEmjf6e98KPzyK.htm)|Veiled Current|Courant voilaire|libre|
|[QkD7kUl6KvUvPAYA.htm](rage-of-elements-bestiary/QkD7kUl6KvUvPAYA.htm)|Kizidhar|Kizidhar|libre|
|[RFxLankyC74BEY03.htm](rage-of-elements-bestiary/RFxLankyC74BEY03.htm)|Capritellix|Capritellix|libre|
|[rmno91OxFdAJX5ap.htm](rage-of-elements-bestiary/rmno91OxFdAJX5ap.htm)|Ore Louse|Pou de minerai|libre|
|[RObxZM2z4P6ZW3hN.htm](rage-of-elements-bestiary/RObxZM2z4P6ZW3hN.htm)|Jann Shuyookh|Shuyookh jann|libre|
|[Rv14qdCi4cTEBXJ4.htm](rage-of-elements-bestiary/Rv14qdCi4cTEBXJ4.htm)|Murajau|Murajau|libre|
|[RZ83RzvCJJHHooyk.htm](rage-of-elements-bestiary/RZ83RzvCJJHHooyk.htm)|Veldenar|Veldenar|libre|
|[TlM6ePIW2yHNOMJv.htm](rage-of-elements-bestiary/TlM6ePIW2yHNOMJv.htm)|Twins of Rowan|Les jumeaux de sorbier|libre|
|[TPi6QRmfyM9BWSof.htm](rage-of-elements-bestiary/TPi6QRmfyM9BWSof.htm)|Living Magma|Magma vivant|libre|
|[Ub0UIqBOjouOePe8.htm](rage-of-elements-bestiary/Ub0UIqBOjouOePe8.htm)|Ferrous Butterfly|Papillon ferrugineux|libre|
|[uEabVqGzN4TprwHw.htm](rage-of-elements-bestiary/uEabVqGzN4TprwHw.htm)|Sootsoldiers|Fantassuies|libre|
|[vM6zlGT02raJqPTu.htm](rage-of-elements-bestiary/vM6zlGT02raJqPTu.htm)|Despairing Pall|Nuage sombre|libre|
|[Vtx0UBhy5aUu3UMO.htm](rage-of-elements-bestiary/Vtx0UBhy5aUu3UMO.htm)|Metal Wisp|Fredon de métal|libre|
|[Vv9ffFM1qiTdIEsy.htm](rage-of-elements-bestiary/Vv9ffFM1qiTdIEsy.htm)|Munsahir Trooper|Soldat Munsahir|libre|
|[WrGYN6jtEfv7k1pV.htm](rage-of-elements-bestiary/WrGYN6jtEfv7k1pV.htm)|Living Grove|Bosquet vivant|libre|
|[XL7B63sTgdDJPbVQ.htm](rage-of-elements-bestiary/XL7B63sTgdDJPbVQ.htm)|Ifrit Shuyookh|Shuyookh ifrit|libre|
|[xorqWffvwJFUFuON.htm](rage-of-elements-bestiary/xorqWffvwJFUFuON.htm)|Olobigonde|Olobigonde|libre|
|[XvFyRUv2BxgjKm77.htm](rage-of-elements-bestiary/XvFyRUv2BxgjKm77.htm)|Lomori Sprout|Pousse de lomori|libre|
|[XyU6fPkQJjS1VFTb.htm](rage-of-elements-bestiary/XyU6fPkQJjS1VFTb.htm)|Skymetal Striker|Heurtard en métal céleste|libre|
|[YKEPPmyqTvVSApo8.htm](rage-of-elements-bestiary/YKEPPmyqTvVSApo8.htm)|Harvest Regiment|Régiment de récolte|libre|
|[YTTKgBLXSIna2KNO.htm](rage-of-elements-bestiary/YTTKgBLXSIna2KNO.htm)|Abysium Horror|Atrocité d'abysium|libre|
|[ytvGscCgKbOCn0dB.htm](rage-of-elements-bestiary/ytvGscCgKbOCn0dB.htm)|Rust Scarab|Scarabée de rouille|libre|
|[YWFBlSODRkWD60Je.htm](rage-of-elements-bestiary/YWFBlSODRkWD60Je.htm)|Picture-in-Clouds|Paréidolie|libre|
|[Z5qBvEJ628DGe8zS.htm](rage-of-elements-bestiary/Z5qBvEJ628DGe8zS.htm)|Melomach|Mélomac|libre|
|[zBUFjuz3dR8bacee.htm](rage-of-elements-bestiary/zBUFjuz3dR8bacee.htm)|Sootsoldiers (The Radiant Host)|Fantassuies (l'Ost rayonnant)|libre|
