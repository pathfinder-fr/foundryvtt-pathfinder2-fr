# État de la traduction (monsters-of-myth-bestiary)

 * **libre**: 42


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0ypo8Vt3B6p9DqAt.htm](monsters-of-myth-bestiary/0ypo8Vt3B6p9DqAt.htm)|Collapsing Structure|Effondrement de structure|libre|
|[10srqxC7QZy0dq2g.htm](monsters-of-myth-bestiary/10srqxC7QZy0dq2g.htm)|Spectral Devil|Diable spectral|libre|
|[5PwT7t6eTC0qcK7E.htm](monsters-of-myth-bestiary/5PwT7t6eTC0qcK7E.htm)|Grisantian Lion|Lion grisantien|libre|
|[7a57dwK5XrV78ajM.htm](monsters-of-myth-bestiary/7a57dwK5XrV78ajM.htm)|Temteki|Temteki|libre|
|[a1vTm01GBVF4TI8f.htm](monsters-of-myth-bestiary/a1vTm01GBVF4TI8f.htm)|Taljjae (The Beast)|Taljjae (la Bête)|libre|
|[bhLbKN7MJofTabfK.htm](monsters-of-myth-bestiary/bhLbKN7MJofTabfK.htm)|Taljjae|Taljjae|libre|
|[Bi6bi9Ko5x9OJfDT.htm](monsters-of-myth-bestiary/Bi6bi9Ko5x9OJfDT.htm)|Desert's Howl|Hurlesable|libre|
|[C87bRxKDTuJXGkPG.htm](monsters-of-myth-bestiary/C87bRxKDTuJXGkPG.htm)|Tehialai-Thief-of-Ships|Téhialai-Voleuse-de-navires|libre|
|[CgNC8uWPrJbGDklJ.htm](monsters-of-myth-bestiary/CgNC8uWPrJbGDklJ.htm)|Spawn Of Kothogaz|Rejeton de Kothogaz|libre|
|[EmWSRL2aYBcHCgrW.htm](monsters-of-myth-bestiary/EmWSRL2aYBcHCgrW.htm)|Crystal Pin|Épingle en cristal|libre|
|[EXyfhhX9GLOnK1uZ.htm](monsters-of-myth-bestiary/EXyfhhX9GLOnK1uZ.htm)|Spring-Heeled Jack|Jack à ressort|libre|
|[FhgZOBWNMa5t7pYV.htm](monsters-of-myth-bestiary/FhgZOBWNMa5t7pYV.htm)|Mosquito Witch (The Legion Leech)|La Sorcière moustique (Légion sangsues)|libre|
|[G1jYZOjA3E3BqrIM.htm](monsters-of-myth-bestiary/G1jYZOjA3E3BqrIM.htm)|Melfesh Monster|Monstre Melfesh|libre|
|[hfi0qJePy2cf4Fqe.htm](monsters-of-myth-bestiary/hfi0qJePy2cf4Fqe.htm)|Taljjae (The Wanderer)|Taljjae (le Vagabond)|libre|
|[hLyLnBn7lEFstUG2.htm](monsters-of-myth-bestiary/hLyLnBn7lEFstUG2.htm)|Somnalu Oculus|Somnalu Oculus|libre|
|[IO7sPevkM0zvnWbi.htm](monsters-of-myth-bestiary/IO7sPevkM0zvnWbi.htm)|Taljjae (The Hero)|Taljjae (le Héros)|libre|
|[j3NhCEMLVm4HSsgr.htm](monsters-of-myth-bestiary/j3NhCEMLVm4HSsgr.htm)|Taljjae (The Nobleman)|Taljjae (le Noble)|libre|
|[jInkcz2UYdaCzsBU.htm](monsters-of-myth-bestiary/jInkcz2UYdaCzsBU.htm)|Ulgrem-Axaan|Ulgrem-Axaan|libre|
|[JX7k6wFmZLqNjK0k.htm](monsters-of-myth-bestiary/JX7k6wFmZLqNjK0k.htm)|Ulgrem-Lurann|Ulgrem-Lurann|libre|
|[K7hgO9SEBai2jwYj.htm](monsters-of-myth-bestiary/K7hgO9SEBai2jwYj.htm)|Krampus Celebrant|Célébrant de Krampus|libre|
|[kdkmTLlqzOYVxb50.htm](monsters-of-myth-bestiary/kdkmTLlqzOYVxb50.htm)|Krampus (The Horned Miser)|Krampus (L'avare cornu)|libre|
|[KdOwmclPVpcTSPjF.htm](monsters-of-myth-bestiary/KdOwmclPVpcTSPjF.htm)|Mosquito Witch|La Sorcière moustique|libre|
|[KeKlBbO3hfT5mzqO.htm](monsters-of-myth-bestiary/KeKlBbO3hfT5mzqO.htm)|Storm Discharge|Décharge de foudre|libre|
|[KLxdUFCvZ7HkAP1E.htm](monsters-of-myth-bestiary/KLxdUFCvZ7HkAP1E.htm)|Somnalu|Somnalu|libre|
|[LK3SgIKJqqo4Q4NQ.htm](monsters-of-myth-bestiary/LK3SgIKJqqo4Q4NQ.htm)|Cuetzmonquali|Cuetzmonquali|libre|
|[mE9MQ0hnRmlR9m94.htm](monsters-of-myth-bestiary/mE9MQ0hnRmlR9m94.htm)|Mosquito Witch (The Swarm Seer)|La Sorcière moustique (Voyante des nuées)|libre|
|[N3OJ0wxik4wEgTg7.htm](monsters-of-myth-bestiary/N3OJ0wxik4wEgTg7.htm)|Imperfect Automaton|Automate imparfait|libre|
|[ns6fmJ8469hzBOtM.htm](monsters-of-myth-bestiary/ns6fmJ8469hzBOtM.htm)|Mosquito Witch (The Hemoprophet)|Sorcière moustique (l'Hémoprophétesse)|libre|
|[O4gEaG5vzkLFl7J1.htm](monsters-of-myth-bestiary/O4gEaG5vzkLFl7J1.htm)|Grogrisant|Grogrisant|libre|
|[OF0fZDjNapydddM1.htm](monsters-of-myth-bestiary/OF0fZDjNapydddM1.htm)|Kuworsys|Kuworsys|libre|
|[OLgKekTU0LOYwoxd.htm](monsters-of-myth-bestiary/OLgKekTU0LOYwoxd.htm)|Young Linnorm|Jeune Linnorm|libre|
|[qcdNzMiO3XxPHLPJ.htm](monsters-of-myth-bestiary/qcdNzMiO3XxPHLPJ.htm)|Howling Spawn|Rejeton hurlant|libre|
|[RqdL0YzNF1dG163i.htm](monsters-of-myth-bestiary/RqdL0YzNF1dG163i.htm)|Kothogaz, Dance Of Disharmony|Kothogaz, Danse de la Dissonance|libre|
|[sXZExyaijbRnKI2S.htm](monsters-of-myth-bestiary/sXZExyaijbRnKI2S.htm)|Taljjae (The Grandmother)|Taljjae (la Grand-mère)|libre|
|[VCC4nw6EXvqwgmCp.htm](monsters-of-myth-bestiary/VCC4nw6EXvqwgmCp.htm)|Ainamuuren|Ainamuuren|libre|
|[vI7wAKNDX05DShWy.htm](monsters-of-myth-bestiary/vI7wAKNDX05DShWy.htm)|Quaking Slither|Séisme rampant|libre|
|[W0oF6Dl7G4UeTBSC.htm](monsters-of-myth-bestiary/W0oF6Dl7G4UeTBSC.htm)|Taljjae (The General)|Taljjae (le Général)|libre|
|[WdnuPiAoRFaymR8R.htm](monsters-of-myth-bestiary/WdnuPiAoRFaymR8R.htm)|Fafnheir|Fafnheir|libre|
|[XCW1WzJqwGUS1cGi.htm](monsters-of-myth-bestiary/XCW1WzJqwGUS1cGi.htm)|Ulistul|Ulistul|libre|
|[XLDh6o6vBRCDEKsl.htm](monsters-of-myth-bestiary/XLDh6o6vBRCDEKsl.htm)|Taljjae (The Hermit)|Taljjae (l'Ermite)|libre|
|[XwBFT7VO9M7M6AgQ.htm](monsters-of-myth-bestiary/XwBFT7VO9M7M6AgQ.htm)|Planar Tear|Déchirure planétaire|libre|
|[zE2ykzHZKNS3Rnc3.htm](monsters-of-myth-bestiary/zE2ykzHZKNS3Rnc3.htm)|Kallas Devil|Diablesse Kallas|libre|
