# État de la traduction (actions)

 * **officielle**: 37
 * **changé**: 27
 * **libre**: 374
 * **aucune**: 25


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[0MZ5tFUYKGMq21NV.htm](actions/0MZ5tFUYKGMq21NV.htm)|Intercession Spell|
|[24k7qCV8S95yYK3y.htm](actions/24k7qCV8S95yYK3y.htm)|Reap the Field|
|[3MiV0maH8imtnVwS.htm](actions/3MiV0maH8imtnVwS.htm)|Spell-Woven Shot|
|[4d6QSFBP4lMUYrVK.htm](actions/4d6QSFBP4lMUYrVK.htm)|Bond with Spirit|
|[8r9ePG6BnrnO6YE1.htm](actions/8r9ePG6BnrnO6YE1.htm)|Only You and I|
|[bUgSKcdHyEKbtXhl.htm](actions/bUgSKcdHyEKbtXhl.htm)|Opportunistic Accusation|
|[cfQb0mBbyrjhyGf5.htm](actions/cfQb0mBbyrjhyGf5.htm)|Plant Thirty Barbs|
|[CJnA1wEg2kA5BXfe.htm](actions/CJnA1wEg2kA5BXfe.htm)|Heavy is the Crown|
|[dypCcXb9mwVbg6xD.htm](actions/dypCcXb9mwVbg6xD.htm)|Flyby Attack|
|[GrzbczH9S3SHPiIQ.htm](actions/GrzbczH9S3SHPiIQ.htm)|Harvest Blood|
|[jjxxWxnlEqWjTUur.htm](actions/jjxxWxnlEqWjTUur.htm)|Restorative Teleportation|
|[JxuXTJIKRwmYn08D.htm](actions/JxuXTJIKRwmYn08D.htm)|Drain Realm|
|[mbOa48mQCqj86lVw.htm](actions/mbOa48mQCqj86lVw.htm)|Coiling Serpents|
|[mBqZ2IahMT9Jvo7k.htm](actions/mBqZ2IahMT9Jvo7k.htm)|Ringing Challenge|
|[mpFTlTGmPBKXMq2C.htm](actions/mpFTlTGmPBKXMq2C.htm)|Unleash Realm|
|[plLX3yDcKo2YiJ0j.htm](actions/plLX3yDcKo2YiJ0j.htm)|Speed of Arms|
|[QWAWf8NDKpo1mzLE.htm](actions/QWAWf8NDKpo1mzLE.htm)|Enter Seat Of Power|
|[SNjmjFw30KQxIP4A.htm](actions/SNjmjFw30KQxIP4A.htm)|Rewrite Fate|
|[Ssp7CCGMJhcAfeYw.htm](actions/Ssp7CCGMJhcAfeYw.htm)|Spread Realm|
|[ukt9Dr4qmj9pUQII.htm](actions/ukt9Dr4qmj9pUQII.htm)|Bear Allies' Burdens|
|[vZwa0PZLQvm3X5ME.htm](actions/vZwa0PZLQvm3X5ME.htm)|Drink of my Foes|
|[Y9XUwC8ihxZCfndG.htm](actions/Y9XUwC8ihxZCfndG.htm)|Break the Sun's Legs|
|[Yrc21SgKRWOMNo7C.htm](actions/Yrc21SgKRWOMNo7C.htm)|Hunt the Razer's Pawn|
|[ZhVJ7EUTJngrIO2Z.htm](actions/ZhVJ7EUTJngrIO2Z.htm)|Burn out of Time|
|[ZJ8KJp5mZYgzPJYN.htm](actions/ZJ8KJp5mZYgzPJYN.htm)|Amassed Assault|

## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[04VQuQih77pxX06q.htm](actions/04VQuQih77pxX06q.htm)|Fling Magic|Lancer la magie|changé|
|[3D9kGfwg4LUZBR9A.htm](actions/3D9kGfwg4LUZBR9A.htm)|Overdrive|Surrégime|changé|
|[6OZfIjiPjWTIe7Pr.htm](actions/6OZfIjiPjWTIe7Pr.htm)|Spinning Crush|Rotation écrasante|changé|
|[9MJcQEMVVrmMyGyq.htm](actions/9MJcQEMVVrmMyGyq.htm)|Living Fortification|Fortification vivante|changé|
|[9sXQf1SyppS5B6O1.htm](actions/9sXQf1SyppS5B6O1.htm)|Automaton Aim|Visée de l'automate|changé|
|[A4L90h7FIgO5EyBx.htm](actions/A4L90h7FIgO5EyBx.htm)|Siegebreaker|Briseur de siège|changé|
|[aBQ8ajvEBByv45yz.htm](actions/aBQ8ajvEBByv45yz.htm)|Cast a Spell|Lancer un sort|changé|
|[b3h3QqvwS5fnaiu1.htm](actions/b3h3QqvwS5fnaiu1.htm)|Banshee Cry|Pleur de la banshie|changé|
|[cYdz2grcOcRt4jk6.htm](actions/cYdz2grcOcRt4jk6.htm)|Disable a Device|Désamorcer un dispositif|changé|
|[djULKW76BbSoN3zb.htm](actions/djULKW76BbSoN3zb.htm)|Enter Spirit Trance|Entrer en transe spirituelle|changé|
|[DYn1igFjCGJEiP22.htm](actions/DYn1igFjCGJEiP22.htm)|Recall Ammunition|Rappeler une munition|changé|
|[EeM0Czaep7G5ZSh5.htm](actions/EeM0Czaep7G5ZSh5.htm)|Ten Paces|Dix pas|changé|
|[Fe487XZBdqEI2InL.htm](actions/Fe487XZBdqEI2InL.htm)|Dispelling Bullet|Balle de dissipation|changé|
|[izvfZ561JTdeyh6i.htm](actions/izvfZ561JTdeyh6i.htm)|Goblin Jubilee|Jubilé gobelin|changé|
|[jSC5AYEfliOPpO3H.htm](actions/jSC5AYEfliOPpO3H.htm)|Reloading Strike|Frappe rechargeante|changé|
|[mech0dhb4eKbCAu0.htm](actions/mech0dhb4eKbCAu0.htm)|Coughing Dragon|Dragon qui tousse|changé|
|[O1iircZOOCo42r9Y.htm](actions/O1iircZOOCo42r9Y.htm)|Ghost Shot|Tir fantôme|changé|
|[rI2MSXR1MQzgQUO7.htm](actions/rI2MSXR1MQzgQUO7.htm)|Grim Swagger|Sinistre assurance|changé|
|[SMF1hTWPHtmlS8Cd.htm](actions/SMF1hTWPHtmlS8Cd.htm)|Bullet Dancer Stance|Posture du danseur de balle|changé|
|[Tlrde2xh7AhesXNB.htm](actions/Tlrde2xh7AhesXNB.htm)|One Shot, One Kill|Un tir, un mort|changé|
|[TSDbyYRQwhIyY2Oq.htm](actions/TSDbyYRQwhIyY2Oq.htm)|Energy Shot|Tir énergétique|changé|
|[tw1KDRPdBAkg5DlS.htm](actions/tw1KDRPdBAkg5DlS.htm)|Clear a Path|Libérer le passage|changé|
|[VNuOwXIHafSLHvsZ.htm](actions/VNuOwXIHafSLHvsZ.htm)|Spellsling|Envoi de sort|changé|
|[vO0Y1dVjNfbDyT4S.htm](actions/vO0Y1dVjNfbDyT4S.htm)|Vital Shot|Tir vital|changé|
|[vvYsE7OiSSCXhPdr.htm](actions/vvYsE7OiSSCXhPdr.htm)|Set Explosives|Placer des explosifs|changé|
|[wt6jdjjje16Nx34f.htm](actions/wt6jdjjje16Nx34f.htm)|Jumping Jenny|Saut de Njini|changé|
|[yUjuLbBMflVum8Yn.htm](actions/yUjuLbBMflVum8Yn.htm)|Launch Fireworks|Démonstration de feux d'artifice|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[00LNVSCbwJ8pszwE.htm](actions/00LNVSCbwJ8pszwE.htm)|Mutagenic Flashback|Flashback du mutagène|officielle|
|[06frwOOuo4HJtivl.htm](actions/06frwOOuo4HJtivl.htm)|Furious Strike|Frappe furieuse|libre|
|[0LwQufKtXfRda0BR.htm](actions/0LwQufKtXfRda0BR.htm)|Dazzle Seeker|Chercheur d'éblouissement|libre|
|[0VwLAKI7QP6mib88.htm](actions/0VwLAKI7QP6mib88.htm)|Raise Slabs|Élever des blocs|libre|
|[1ejGup6ouh9t9S96.htm](actions/1ejGup6ouh9t9S96.htm)|Dancing Dodge|Esquive dansante|libre|
|[1If9lLVoZdO8woVg.htm](actions/1If9lLVoZdO8woVg.htm)|Consume Flesh|Dévorer la chair|libre|
|[1kGNdIIhuglAjIp9.htm](actions/1kGNdIIhuglAjIp9.htm)|Treat Wounds|Soigner les blessures|libre|
|[1LDOfV8jEka09eXr.htm](actions/1LDOfV8jEka09eXr.htm)|Deceptive Sidestep|Pas de côté trompeur|libre|
|[1OagaWtBpVXExToo.htm](actions/1OagaWtBpVXExToo.htm)|Recall Knowledge|Se souvenir|libre|
|[1xRFPTFtWtGJ9ELw.htm](actions/1xRFPTFtWtGJ9ELw.htm)|Sense Motive|Deviner les intentions|officielle|
|[21WIfSu7Xd7uKqV8.htm](actions/21WIfSu7Xd7uKqV8.htm)|Tumble Through|Déplacement acrobatique|libre|
|[2424g94rpoiN1IPh.htm](actions/2424g94rpoiN1IPh.htm)|Catharsis|Catharsis|libre|
|[24PSsn1SKpUwYA1X.htm](actions/24PSsn1SKpUwYA1X.htm)|Retraining|Réapprentissage|libre|
|[25WDi1cVUrW92sUj.htm](actions/25WDi1cVUrW92sUj.htm)|Clue In|Partager les indices|libre|
|[2EE4aF4SZpYf0R6H.htm](actions/2EE4aF4SZpYf0R6H.htm)|Pick a Lock|Crocheter une serrure|officielle|
|[2HJ4yuEFY1Cast4h.htm](actions/2HJ4yuEFY1Cast4h.htm)|High Jump|Sauter en hauteur|libre|
|[2KV87ponbWXgGIhZ.htm](actions/2KV87ponbWXgGIhZ.htm)|Act Together|Agir ensemble|libre|
|[2r4BY90F9PLFai3c.htm](actions/2r4BY90F9PLFai3c.htm)|Once Bitten|Une fois mordu|libre|
|[2u915NdUyQan6uKF.htm](actions/2u915NdUyQan6uKF.htm)|Demoralize|Démoraliser|libre|
|[34E7k2YRcsOU5uyl.htm](actions/34E7k2YRcsOU5uyl.htm)|Change Shape|Changement de forme|libre|
|[3cuTA58ObXhuFX2r.htm](actions/3cuTA58ObXhuFX2r.htm)|Bend Time|Courber le temps|libre|
|[3f5DMFu8fPiqHpRg.htm](actions/3f5DMFu8fPiqHpRg.htm)|Sustain|Maintenir|libre|
|[3sgY9afTTBhZXE8V.htm](actions/3sgY9afTTBhZXE8V.htm)|Devour Ambient Magic|Dévorer la magie ambiante|libre|
|[3yoajuKjwHZ9ApUY.htm](actions/3yoajuKjwHZ9ApUY.htm)|Grab an Edge|Se raccrocher in extremis|libre|
|[3ZzoI9MTtJFd1Kjl.htm](actions/3ZzoI9MTtJFd1Kjl.htm)|Share Senses|Partager les sens|libre|
|[49F65W6VwuXhmv8K.htm](actions/49F65W6VwuXhmv8K.htm)|Drink Blood|Boire le sang|libre|
|[49y9Ec4bDii8pcD3.htm](actions/49y9Ec4bDii8pcD3.htm)|Subsist|Subsister|libre|
|[4IxCKbGaEM9nUhld.htm](actions/4IxCKbGaEM9nUhld.htm)|Finish The Job|Finir le boulot|libre|
|[5bCZGpp9yFHXDz1j.htm](actions/5bCZGpp9yFHXDz1j.htm)|Entity's Resurgence|Résurgence de l'entité|libre|
|[5g0gCXJnUVipYtW0.htm](actions/5g0gCXJnUVipYtW0.htm)|Turn Aside Ambient Magic|Mettre à l'écart la magie ambiante|libre|
|[5HZ4cYDowUw2oUbZ.htm](actions/5HZ4cYDowUw2oUbZ.htm)|Tenacious Stance|Posture tenace|libre|
|[5LW1k5DUkzbbYuYL.htm](actions/5LW1k5DUkzbbYuYL.htm)|Daydream Trance|Transe du rêve éveillé|libre|
|[5p2AMqM9bOVnhwPT.htm](actions/5p2AMqM9bOVnhwPT.htm)|Recenter|Recentrer|libre|
|[5tOovJGK2TQIBHUZ.htm](actions/5tOovJGK2TQIBHUZ.htm)|Grow|Faire pousser|libre|
|[5V49l0K460CcvBOO.htm](actions/5V49l0K460CcvBOO.htm)|Defend Life|Protéger la vie|libre|
|[6iGoVnEJuBHJqgpi.htm](actions/6iGoVnEJuBHJqgpi.htm)|Death Drone|Bourdonnement de la mort|libre|
|[6lbr0Jnv0zMB5uGb.htm](actions/6lbr0Jnv0zMB5uGb.htm)|Elemental Blast|Déflagration élémentaire|libre|
|[6Oe9dg3Lu10slyeC.htm](actions/6Oe9dg3Lu10slyeC.htm)|Influence|Influence|libre|
|[6SAdjml3OZw0BZnn.htm](actions/6SAdjml3OZw0BZnn.htm)|Thundering Roar|Rugissement tonitruant|libre|
|[6xWX8Ey5ShAyj1HO.htm](actions/6xWX8Ey5ShAyj1HO.htm)|Flowing Spirit Strike|Frappe de l'esprit gracieux|libre|
|[74iat04PtfG8gn2Q.htm](actions/74iat04PtfG8gn2Q.htm)|Mighty Rage|Rage formidable|officielle|
|[7blmbDrQFNfdT731.htm](actions/7blmbDrQFNfdT731.htm)|Shove|Pousser|libre|
|[7dq6bpYvLArJ5odx.htm](actions/7dq6bpYvLArJ5odx.htm)|Forge Documents|Falsifier des documents|libre|
|[7GeguyqyD1TjoC4r.htm](actions/7GeguyqyD1TjoC4r.htm)|Unleash Psyche|Déchaîner la psyché|libre|
|[7pdG8l9POMK76Lf2.htm](actions/7pdG8l9POMK76Lf2.htm)|Warding Sign|Signe de protection|libre|
|[7qjfYsLNTr17Aftf.htm](actions/7qjfYsLNTr17Aftf.htm)|Energy Emanation|Émanation énergétique|libre|
|[8aIe57gXJ94mPpW4.htm](actions/8aIe57gXJ94mPpW4.htm)|Anadi Venom|Venin anadi|libre|
|[8b63qGzqSUENQgOT.htm](actions/8b63qGzqSUENQgOT.htm)|Armor Up!|Retour d'armure|libre|
|[8kGpUUUSX1sB2OqQ.htm](actions/8kGpUUUSX1sB2OqQ.htm)|Call Companion|Appel du compagnon|libre|
|[8w6esW689NNbbq3i.htm](actions/8w6esW689NNbbq3i.htm)|Call on Ancient Blood|Appel du sang ancien|libre|
|[97jWk4XbIrGQK4Cp.htm](actions/97jWk4XbIrGQK4Cp.htm)|Transform Ammunition|Transformer une munition|libre|
|[9cGJBXSJW6EpgibP.htm](actions/9cGJBXSJW6EpgibP.htm)|Shift Immanence|Transfert d'immanence|libre|
|[9gDMkIfDifh61yLz.htm](actions/9gDMkIfDifh61yLz.htm)|Stop|S'arrêter|libre|
|[9kIvFxA5V9rvNWiH.htm](actions/9kIvFxA5V9rvNWiH.htm)|Thoughtful Reload|Rechargement réfléchi|libre|
|[9Nmz5114UB87n7Aj.htm](actions/9Nmz5114UB87n7Aj.htm)|Affix a Fulu|Fixer un fulu|libre|
|[9X80J5RN21Uoaeiw.htm](actions/9X80J5RN21Uoaeiw.htm)|Binding Vow|Respecter sa parole|libre|
|[a47Npi5XHXo47y49.htm](actions/a47Npi5XHXo47y49.htm)|Blizzard Evasion|Évasion du blizzard|libre|
|[A72nHGUtNXgY5Ey9.htm](actions/A72nHGUtNXgY5Ey9.htm)|Delay|Retarder|libre|
|[a9PzINjFTO5GvAJN.htm](actions/a9PzINjFTO5GvAJN.htm)|Quick-Tempered|Impulsif|libre|
|[Ah5g9pDwWF9b9VW9.htm](actions/Ah5g9pDwWF9b9VW9.htm)|Rage|Rage|libre|
|[aHE92WSosMDewe33.htm](actions/aHE92WSosMDewe33.htm)|Take a Taste|Goûte un morceau|libre|
|[AJeLwbQBt1YH3S6v.htm](actions/AJeLwbQBt1YH3S6v.htm)|Fade Into Daydreams|Se fondre en un rêve éveillé|libre|
|[AjLSHZSWQ90exdLo.htm](actions/AjLSHZSWQ90exdLo.htm)|Dismiss|Révoquer|libre|
|[AJstokjdG6iDjVjE.htm](actions/AJstokjdG6iDjVjE.htm)|Impersonate|Se déguiser|libre|
|[akmQzZoNhyfCKFpL.htm](actions/akmQzZoNhyfCKFpL.htm)|Airy Step|Pas aérien|libre|
|[ako5zZwC6o26cfCQ.htm](actions/ako5zZwC6o26cfCQ.htm)|Lantern Beam|Faisceau de lanterne|libre|
|[AOWwwXVDWwliFlWj.htm](actions/AOWwwXVDWwliFlWj.htm)|Blood Feast|Festin sanglant|libre|
|[Aq2mXT2hLlstFL5C.htm](actions/Aq2mXT2hLlstFL5C.htm)|Invoke Celestial Privilege|Invoquer le privilège céleste|libre|
|[aVGNaNgG6cYNRp5A.htm](actions/aVGNaNgG6cYNRp5A.htm)|Lantern Strobe|Lanterne stroboscopique|libre|
|[AZt0m0EHRmtDCd5s.htm](actions/AZt0m0EHRmtDCd5s.htm)|Marathon Dash|Élan marathonien|libre|
|[b6CanpXyUKJgxEwq.htm](actions/b6CanpXyUKJgxEwq.htm)|Salt Wound|Saler une blessure|libre|
|[B7wDxyjX66JkiCOV.htm](actions/B7wDxyjX66JkiCOV.htm)|Venom Draw|Tirer du venin|libre|
|[Ba9EuLV1I2b1ue9P.htm](actions/Ba9EuLV1I2b1ue9P.htm)|Map the Area|Cartographier la zone|libre|
|[bCDMuu3tE4d6KHrJ.htm](actions/bCDMuu3tE4d6KHrJ.htm)|Pistolero's Retort|Réplique du pistoléro|libre|
|[BCp3tn2HATUOUEdQ.htm](actions/BCp3tn2HATUOUEdQ.htm)|Gossip|Rumeur|libre|
|[Bcxarzksqt9ezrs6.htm](actions/Bcxarzksqt9ezrs6.htm)|Stride|Marcher rapidement|officielle|
|[bG91dbtbgOnw7Ofx.htm](actions/bG91dbtbgOnw7Ofx.htm)|Board|Monter à bord|libre|
|[BKMxC0ZvdYfdhx7C.htm](actions/BKMxC0ZvdYfdhx7C.htm)|Objection|Objection|libre|
|[BKnN9la3WNrRgZ6n.htm](actions/BKnN9la3WNrRgZ6n.htm)|Conduct Energy|Conduire l'énergie|libre|
|[BlAOM2X92SI6HMtJ.htm](actions/BlAOM2X92SI6HMtJ.htm)|Seek|Chercher|officielle|
|[bp0Up04x3dzGK5bB.htm](actions/bp0Up04x3dzGK5bB.htm)|Debilitating Strike|Frappe incapacitante|libre|
|[BRR0HS8yRhgBfipz.htm](actions/BRR0HS8yRhgBfipz.htm)|A Quick Glimpse Beyond|Un rapide aperçu de l'au-delà|libre|
|[bT3skovyLUtP22ME.htm](actions/bT3skovyLUtP22ME.htm)|Repair|Réparer|officielle|
|[c40APnn4a7bWhtcZ.htm](actions/c40APnn4a7bWhtcZ.htm)|A Challenge for Heroes|Une épreuve digne de héros|libre|
|[c8TGiZ48ygoSPofx.htm](actions/c8TGiZ48ygoSPofx.htm)|Swim|Nager|libre|
|[cCCOjU9ihfrn0spp.htm](actions/cCCOjU9ihfrn0spp.htm)|Arrow Splits Arrow|Tirer au centre de la flèche|libre|
|[CCMemaB1RoVa0aOu.htm](actions/CCMemaB1RoVa0aOu.htm)|Bestial Clarity|Clarté bestiale|libre|
|[cczvLO5r7QuHaXwx.htm](actions/cczvLO5r7QuHaXwx.htm)|Dampening Harmonics|Atténuation des harmoniques|libre|
|[CIqiFw9rqYnuzggq.htm](actions/CIqiFw9rqYnuzggq.htm)|Practical Research|Recherche appliquée|libre|
|[CLX9WEYXdntl2wrt.htm](actions/CLX9WEYXdntl2wrt.htm)|Reactive Falsehood|Fausseté réactive|libre|
|[CPTolKAF55p7D7Sn.htm](actions/CPTolKAF55p7D7Sn.htm)|Mirror-Trickery|Tromperie-miroir|libre|
|[cS9nfDRGD83bNU1p.htm](actions/cS9nfDRGD83bNU1p.htm)|Fly|Voler|libre|
|[Cu7P7NB3XH67mi1N.htm](actions/Cu7P7NB3XH67mi1N.htm)|Crash Against Me|S'écraser contre moi|libre|
|[cwBsPU24715fUCub.htm](actions/cwBsPU24715fUCub.htm)|Ultimatum of Liberation|Ultimatun de libération|libre|
|[cx0juTYewwBmrYWv.htm](actions/cx0juTYewwBmrYWv.htm)|Beast's Charge|Charge de la bête|libre|
|[cx5tBTy6weK6YSw9.htm](actions/cx5tBTy6weK6YSw9.htm)|Thermal Eruption|Éruption thermique|libre|
|[cyl7y8GcGjd8ENC7.htm](actions/cyl7y8GcGjd8ENC7.htm)|Swift Choreography|Chorégraphie rapide|libre|
|[cYtYKa1gDEl7y2N0.htm](actions/cYtYKa1gDEl7y2N0.htm)|Defend|Défendre|officielle|
|[D2PNfIw7U6Ks0VY4.htm](actions/D2PNfIw7U6Ks0VY4.htm)|Steel Your Resolve|Puiser dans votre résolution|libre|
|[d5I6018Mci2SWokk.htm](actions/d5I6018Mci2SWokk.htm)|Leap|Bondir|libre|
|[D91jQs0wleU5ml4K.htm](actions/D91jQs0wleU5ml4K.htm)|Drink from the Chalice|Boire au calice|libre|
|[d9gbpiQjChYDYA2L.htm](actions/d9gbpiQjChYDYA2L.htm)|Decipher Writing|Déchiffrer un texte|libre|
|[DCb62iCBrJXy0Ik6.htm](actions/DCb62iCBrJXy0Ik6.htm)|Request|Solliciter|libre|
|[dCuvfq3r2K9wXY9g.htm](actions/dCuvfq3r2K9wXY9g.htm)|Basic Finisher|Aboutissement basique|libre|
|[DFRLID6lIRw7CzOT.htm](actions/DFRLID6lIRw7CzOT.htm)|Spring the Trap|Enclencher le piège|libre|
|[dLgAMt3TbkmLkUqE.htm](actions/dLgAMt3TbkmLkUqE.htm)|Ready|Préparer|officielle|
|[dnaPJfA0CDLNrWcW.htm](actions/dnaPJfA0CDLNrWcW.htm)|Implement's Interruption|Interruption de l'implément|libre|
|[DS9sDOWkXrz2xmHi.htm](actions/DS9sDOWkXrz2xmHi.htm)|Eldritch Shot|Tir mystique|libre|
|[Dt6B1slsBy8ipJu9.htm](actions/Dt6B1slsBy8ipJu9.htm)|Disarm|Désarmer|libre|
|[dwA0wfUaRf4aT0eB.htm](actions/dwA0wfUaRf4aT0eB.htm)|Spit Ambient Magic|Cracher la magie ambiante|libre|
|[DXIZ4DHGxhZiWNWb.htm](actions/DXIZ4DHGxhZiWNWb.htm)|Long-Term Rest|Repos long|officielle|
|[e2ePMDa7ixbLRryj.htm](actions/e2ePMDa7ixbLRryj.htm)|Encouraging Words|Paroles encourageantes|libre|
|[E48YTUyreo1kc9GM.htm](actions/E48YTUyreo1kc9GM.htm)|Swarm Forth|Essaimage|libre|
|[e5JscXqvBUC867oo.htm](actions/e5JscXqvBUC867oo.htm)|Find Fault|Trouver le défaut|libre|
|[EA5vuSgJfiHH7plD.htm](actions/EA5vuSgJfiHH7plD.htm)|Track|Pister|libre|
|[EAP98XaChJEbgKcK.htm](actions/EAP98XaChJEbgKcK.htm)|Retributive Strike|Frappe punitive|libre|
|[eBgO5gp5kKhGtmk9.htm](actions/eBgO5gp5kKhGtmk9.htm)|Water Transfer|Transfert par l'eau|libre|
|[EcDcowQ8vS6cEfXd.htm](actions/EcDcowQ8vS6cEfXd.htm)|Scout Location|Reconnaître l'endroit|libre|
|[EEDElIyin4z60PXx.htm](actions/EEDElIyin4z60PXx.htm)|Perform|Se produire|officielle|
|[EfjoIuDmtUn4yiow.htm](actions/EfjoIuDmtUn4yiow.htm)|Opportune Riposte|Riposte opportune|libre|
|[egXlMyYLBthoQSgh.htm](actions/egXlMyYLBthoQSgh.htm)|Liar's Hidden Blade|Lame cachée du larron|libre|
|[EHa0owz6mccnmSBf.htm](actions/EHa0owz6mccnmSBf.htm)|Final Surge|Accélération finale|libre|
|[EhLvRWFKhZ3HtrZO.htm](actions/EhLvRWFKhZ3HtrZO.htm)|Settle Emotions|Maîtriser ses émotions|libre|
|[Ei4T4aQx0a5EdBhm.htm](actions/Ei4T4aQx0a5EdBhm.htm)|Fracture Mountains|Fendre les montagnes|libre|
|[enQieRrITuEQZxx2.htm](actions/enQieRrITuEQZxx2.htm)|Selfish Shield|Bouclier égoïste|libre|
|[ePfdZYZRmycMpSWU.htm](actions/ePfdZYZRmycMpSWU.htm)|Avoid Dire Fate|Éviter un terrible destin|libre|
|[eReSHVEPCsdkSL4G.htm](actions/eReSHVEPCsdkSL4G.htm)|Identify Magic|Identifier la magie|officielle|
|[ESMIHOOahLQoqxW1.htm](actions/ESMIHOOahLQoqxW1.htm)|Call Gun|Appel du pistolet|libre|
|[EuoCFSHTzi0VOfuf.htm](actions/EuoCFSHTzi0VOfuf.htm)|Harrow the Fiend|Tourmenter le fiélon|libre|
|[ev8OHpBO3xq3Zt08.htm](actions/ev8OHpBO3xq3Zt08.htm)|Tail Toxin|Queue à toxine|libre|
|[EwgTZBWsc8qKaViP.htm](actions/EwgTZBWsc8qKaViP.htm)|Investigate|Enquêter|officielle|
|[ewwCglB7XOPLUz72.htm](actions/ewwCglB7XOPLUz72.htm)|Lie|Mentir|officielle|
|[exuTl8tvVlewxVGJ.htm](actions/exuTl8tvVlewxVGJ.htm)|Skirt the Underworld|Se faufiler à l'orée du monde des morts|libre|
|[F0JgJR2rXKOg9k1z.htm](actions/F0JgJR2rXKOg9k1z.htm)|Upstage|Surpasser|libre|
|[Fha8jFmfkOPxAsrZ.htm](actions/Fha8jFmfkOPxAsrZ.htm)|Calculate Threats|Calculer les menaces|libre|
|[fJImDBQfqfjKJOhk.htm](actions/fJImDBQfqfjKJOhk.htm)|Sense Direction|S'orienter|libre|
|[FkfWKq9jhhPzKAbb.htm](actions/FkfWKq9jhhPzKAbb.htm)|Rampaging Ferocity|Férocité déchaînée|libre|
|[fL7FhTBcKxMzLBAs.htm](actions/fL7FhTBcKxMzLBAs.htm)|Empty Vessel|Réceptacle vide|libre|
|[fodJ3zuwQsYnBbtk.htm](actions/fodJ3zuwQsYnBbtk.htm)|Exploit Vulnerability|Exploiter la vulnérabilité|libre|
|[fsR3X4c3p19W8RB2.htm](actions/fsR3X4c3p19W8RB2.htm)|Name Drop|Noms connus|libre|
|[ftG89SjTSa9DYDOD.htm](actions/ftG89SjTSa9DYDOD.htm)|Create Forgery|Contrefaire|libre|
|[FzZAYGib08aEq5P2.htm](actions/FzZAYGib08aEq5P2.htm)|Accidental Shot|Tir accidentel|libre|
|[g8QrV39TmZfkbXgE.htm](actions/g8QrV39TmZfkbXgE.htm)|Channel Elements|Canaliser les éléments|libre|
|[gBhWEy3ToxQeCLQm.htm](actions/gBhWEy3ToxQeCLQm.htm)|Covered Reload|Rechargement à couvert|libre|
|[ge56Lu1xXVFYUnLP.htm](actions/ge56Lu1xXVFYUnLP.htm)|Trip|Croc-en-jambe|libre|
|[GKIKJNInDev72qai.htm](actions/GKIKJNInDev72qai.htm)|Drape Ambient Magic|Se draper de magie ambiante|libre|
|[GkmbTGfg8KcgynOA.htm](actions/GkmbTGfg8KcgynOA.htm)|Create a Diversion|Faire diversion|officielle|
|[GmqHs4NEhAjqyAze.htm](actions/GmqHs4NEhAjqyAze.htm)|Spiritual Aid|Aide spirituelle|libre|
|[gohBPHf4dxi4PkBk.htm](actions/gohBPHf4dxi4PkBk.htm)|Timely Dodge|Esquive opportune|libre|
|[GPd3hmyUSbcSBj39.htm](actions/GPd3hmyUSbcSBj39.htm)|Stellar Misfortune|Infortune stellaire|libre|
|[Gu0QsFvWoR9EP5SE.htm](actions/Gu0QsFvWoR9EP5SE.htm)|Swirl Crimson Shroud|Faire tournoyer le voile pourpre|libre|
|[gU93T2UqxLsX9gyF.htm](actions/gU93T2UqxLsX9gyF.htm)|Fated Not to Die|Destiné à ne pas mourir|libre|
|[gxtq81VAhpmNvEgA.htm](actions/gxtq81VAhpmNvEgA.htm)|Tap Ley Line|Puiser dans les lignes telluriques|libre|
|[h4Tzdhqfryp5m2fO.htm](actions/h4Tzdhqfryp5m2fO.htm)|Harvest Heartsliver|Collecter un fragment de coeur|libre|
|[H6v1VgowHaKHnVlG.htm](actions/H6v1VgowHaKHnVlG.htm)|Burrow|Creuser|libre|
|[HAmGozJwLAal5v82.htm](actions/HAmGozJwLAal5v82.htm)|Intensify Vulnerability|Intensifier la vulnérabilité|libre|
|[HbejhIywqIufrmVM.htm](actions/HbejhIywqIufrmVM.htm)|Arcane Cascade|Cascade arcanique|libre|
|[HCl3pzVefiv9ZKQW.htm](actions/HCl3pzVefiv9ZKQW.htm)|Aid|Aider|libre|
|[hFRHPBj6wjAayNtW.htm](actions/hFRHPBj6wjAayNtW.htm)|Jinx|Porter la poisse|libre|
|[HFv62jIK98S2xw4x.htm](actions/HFv62jIK98S2xw4x.htm)|Influence Rumor|Influencer une rumeur|libre|
|[hi56uHG1aAb84Zzu.htm](actions/hi56uHG1aAb84Zzu.htm)|Fight with Fear|Se débattre avec la peur|libre|
|[hPZQ5vA9QHEPtjFW.htm](actions/hPZQ5vA9QHEPtjFW.htm)|Spin Tale|Broder un conte|libre|
|[HYNhdaPtF1QmQbR3.htm](actions/HYNhdaPtF1QmQbR3.htm)|Drop Prone|Se jeter à terre|officielle|
|[I08t3hnpMZSRX5Ug.htm](actions/I08t3hnpMZSRX5Ug.htm)|Rejoin in Flight|Réunir en vol|libre|
|[I75R9NSfsVrit6cU.htm](actions/I75R9NSfsVrit6cU.htm)|Cram|Bachoter|libre|
|[I9k9qe4gOT8UVK4e.htm](actions/I9k9qe4gOT8UVK4e.htm)|Mist Blending|Se fondre dans la brume|libre|
|[IBEbFlMmDbGsrN4G.htm](actions/IBEbFlMmDbGsrN4G.htm)|Raise the Walls|Lever de boucliers|libre|
|[iBf9uGn5LOHkWpZ6.htm](actions/iBf9uGn5LOHkWpZ6.htm)|Craft Disharmonic Instrument|Fabriquer un instrument dysharmonique|libre|
|[IE2nThCmoyhQA0Jn.htm](actions/IE2nThCmoyhQA0Jn.htm)|Avoid Notice|Échapper aux regards|officielle|
|[IEBBHnI7wWRt44KG.htm](actions/IEBBHnI7wWRt44KG.htm)|One Moment till Glory|Un instant avant la gloire|libre|
|[iEYZ5PAim5Cvbila.htm](actions/iEYZ5PAim5Cvbila.htm)|Secure Disguises|Se procurer des déguisements|libre|
|[IfWF56KiSFQbNmHj.htm](actions/IfWF56KiSFQbNmHj.htm)|Manifest Realm|Manifester le royaume|libre|
|[IGPbMkKjnlFW1w1a.htm](actions/IGPbMkKjnlFW1w1a.htm)|Bribe Contact|Corrompre un contact|libre|
|[iJLzVonevhsi2uPs.htm](actions/iJLzVonevhsi2uPs.htm)|Visions of Sin|Visions du péché|libre|
|[ijZ0DDFpMkWqaShd.htm](actions/ijZ0DDFpMkWqaShd.htm)|Palm an Object|Escamoter un objet|libre|
|[IR95FFdO7MtC8C0E.htm](actions/IR95FFdO7MtC8C0E.htm)|Talon Stance|Posture de serre|libre|
|[ITWUi1r8Z7EtChkB.htm](actions/ITWUi1r8Z7EtChkB.htm)|Flash of Grandeur|Éclair de grandeur|libre|
|[iTx0vXAhiS4lKwEi.htm](actions/iTx0vXAhiS4lKwEi.htm)|Psychic Defense|Défense psychique|libre|
|[Iuq8CeNqv3a0oWfQ.htm](actions/Iuq8CeNqv3a0oWfQ.htm)|Life Block|Bloquer la vie|libre|
|[IV8sgoLO6ShD3DCJ.htm](actions/IV8sgoLO6ShD3DCJ.htm)|Expel Maelstrom|Expulser le maelström|libre|
|[IX1VlVCL5sFTptEE.htm](actions/IX1VlVCL5sFTptEE.htm)|Liberating Step|Pas libérateur|officielle|
|[JAka1VWMPGOm5JpR.htm](actions/JAka1VWMPGOm5JpR.htm)|Verdant Rest|Repos verdoyant|libre|
|[jbmXxq56swDYw8hy.htm](actions/jbmXxq56swDYw8hy.htm)|Final Spite|Ultime méchanceté|libre|
|[jD8RW5PdLI6snlsO.htm](actions/jD8RW5PdLI6snlsO.htm)|Mark the Center|Marquer le centre|libre|
|[JdpUaQeNwoVr2VHL.htm](actions/JdpUaQeNwoVr2VHL.htm)|No Scar but This|Nulle autre cicatrice|libre|
|[jftNJjBNxp2cleoi.htm](actions/jftNJjBNxp2cleoi.htm)|Expeditious Inspection|Inspection expéditive|libre|
|[jl1euufxNpAhVt26.htm](actions/jl1euufxNpAhVt26.htm)|Disarming Interception|Interception désarmante|libre|
|[JLPY5hl4qiJ1zLi1.htm](actions/JLPY5hl4qiJ1zLi1.htm)|Discover|Découvrir|libre|
|[JpcegMRqdizrkG0m.htm](actions/JpcegMRqdizrkG0m.htm)|Slayer's Identification|Identification du tueur|libre|
|[JPHWzD2soqjffeSU.htm](actions/JPHWzD2soqjffeSU.htm)|Drain Life|Drain de vie|libre|
|[jsEBrm8SPcsokJLw.htm](actions/jsEBrm8SPcsokJLw.htm)|Tap the Past|Exploiter le passé|libre|
|[JtEzSceixS0WA8wn.htm](actions/JtEzSceixS0WA8wn.htm)|Heaven Rains an Ending|Pluie céleste ultime|libre|
|[jTfJzj1yfmPLsxmj.htm](actions/jTfJzj1yfmPLsxmj.htm)|Captivating Charm|Charme captivant|libre|
|[jtuWJa6Iyyd3gkVv.htm](actions/jtuWJa6Iyyd3gkVv.htm)|Gallop|Galop|libre|
|[JuqmIAnkL9hVGai8.htm](actions/JuqmIAnkL9hVGai8.htm)|Hustle|S'empresser|officielle|
|[JUvAvruz7yRQXfz2.htm](actions/JUvAvruz7yRQXfz2.htm)|Long Jump|Sauter en longueur|libre|
|[JYi4MnsdFu618hPm.htm](actions/JYi4MnsdFu618hPm.htm)|Hunt Prey|Chasser une proie|officielle|
|[jZQoAHmGJvi53NRR.htm](actions/jZQoAHmGJvi53NRR.htm)|Psychometric Assessment|Évaluation psychométrique|libre|
|[K3hd3w20M707t6Iq.htm](actions/K3hd3w20M707t6Iq.htm)|Feral Swing|Crochet brutal|libre|
|[k5S2a2a6o2GS5l01.htm](actions/k5S2a2a6o2GS5l01.htm)|Stonestrike Stance|Posture de frappe de roche|libre|
|[k5TASjIxghvGCy7g.htm](actions/k5TASjIxghvGCy7g.htm)|Call to Axis|Appel à Axis|libre|
|[k6ML9SPMxaGJyjqO.htm](actions/k6ML9SPMxaGJyjqO.htm)|Bite and Sting|Mordre et piquer|libre|
|[K878asDgf1EF0B9S.htm](actions/K878asDgf1EF0B9S.htm)|Confident Finisher|Aboutissement assuré|libre|
|[KAVf7AmRnbCAHrkT.htm](actions/KAVf7AmRnbCAHrkT.htm)|Reactive Strike|Frappe réactive|libre|
|[kAwE9YqU6GH1cD7W.htm](actions/kAwE9YqU6GH1cD7W.htm)|A Moment Unending|Un moment suspendu|libre|
|[kbUymGTjbesOKsV6.htm](actions/kbUymGTjbesOKsV6.htm)|Primal Roar|Rugissement primal|libre|
|[KC6o1cvbr45xnMei.htm](actions/KC6o1cvbr45xnMei.htm)|Conjure Bullet|Conjurer une balle|libre|
|[KInVpdfrxg0AqjRt.htm](actions/KInVpdfrxg0AqjRt.htm)|Tactical Retreat|Retraite tactique|libre|
|[KjoCEEmPGTeFE4hh.htm](actions/KjoCEEmPGTeFE4hh.htm)|Treat Poison|Soigner un empoisonnement|libre|
|[kKKHwVUnroKuAnOt.htm](actions/kKKHwVUnroKuAnOt.htm)|Toxic Skin|Peau toxique|libre|
|[kMcV8e5EZUxa6evt.htm](actions/kMcV8e5EZUxa6evt.htm)|Squeeze|Se faufiler|libre|
|[KMKuJ0onVS72t9Fv.htm](actions/KMKuJ0onVS72t9Fv.htm)|Manifest Soulforged Armament|Manifester l'arsenal mentallurgique|libre|
|[KofrLYBif2QKyiqe.htm](actions/KofrLYBif2QKyiqe.htm)|Unwavering Resilience|Résilience inébranlable|libre|
|[kRxWINkrHUPSHHYq.htm](actions/kRxWINkrHUPSHHYq.htm)|Recall the Teachings|Rappeler les enseignements|libre|
|[kUME7DMhhHH6eIiH.htm](actions/kUME7DMhhHH6eIiH.htm)|That's My Number|C'est mon numéro|libre|
|[kV3XM0YJeS2KCSOb.htm](actions/kV3XM0YJeS2KCSOb.htm)|Scout|Reconnaître|libre|
|[KvVdGkUi0iJwapvD.htm](actions/KvVdGkUi0iJwapvD.htm)|Fortify Camp|Fortifier le camp|libre|
|[kWrks0NxMQH39JHD.htm](actions/kWrks0NxMQH39JHD.htm)|Trench Digging|Creuser une tranchée|libre|
|[KyKXp599tK9BgodC.htm](actions/KyKXp599tK9BgodC.htm)|Dutiful Retaliation|Représailles consciencieuses|libre|
|[lAvEK4yNL3Y7pVmr.htm](actions/lAvEK4yNL3Y7pVmr.htm)|Assume a Role|Endosser un rôle|libre|
|[lgpJoFrkzfkjAzeV.htm](actions/lgpJoFrkzfkjAzeV.htm)|Inscribe Shadow Pamor|Incruster l'ombre|libre|
|[lH3mlvs1rCyXw6iY.htm](actions/lH3mlvs1rCyXw6iY.htm)|Recall Under Pressure|Souvenir sous pression|libre|
|[lID4rJHAVZB6tavf.htm](actions/lID4rJHAVZB6tavf.htm)|Run Over|Foncer dessus|libre|
|[lOE4yjUnETTdaf2T.htm](actions/lOE4yjUnETTdaf2T.htm)|Reposition|Repositionner|libre|
|[low5ORd87QeA9Oug.htm](actions/low5ORd87QeA9Oug.htm)|Extract Element|Extraction d'élément|libre|
|[lvqPQDdWT2DDO0k2.htm](actions/lvqPQDdWT2DDO0k2.htm)|Invest an Item|Investir un objet|officielle|
|[LWrR6UiGm3eCAALJ.htm](actions/LWrR6UiGm3eCAALJ.htm)|Collect Spirit Remnant|Collecter une volute spirituelle|libre|
|[m0f2B7G9eaaTmhFL.htm](actions/m0f2B7G9eaaTmhFL.htm)|Devise a Stratagem|Concevoir un stratagème|libre|
|[M4ALaFXoQDJGxSsI.htm](actions/M4ALaFXoQDJGxSsI.htm)|Starlit Transformation|Transformation stellaire|libre|
|[M76ycLAqHoAgbcej.htm](actions/M76ycLAqHoAgbcej.htm)|Balance|Garder l'équilibre|officielle|
|[M8RCbthRhB4bxO9t.htm](actions/M8RCbthRhB4bxO9t.htm)|Iron Command|Ordre de fer|libre|
|[Ma93dpT4K7JbP9gu.htm](actions/Ma93dpT4K7JbP9gu.htm)|Prove Peace|Prouver la paix|libre|
|[Mh4Vdg6gu8g8RAjh.htm](actions/Mh4Vdg6gu8g8RAjh.htm)|Mirror's Reflection|Reflet du miroir|libre|
|[MHLuKy4nQO2Z4Am1.htm](actions/MHLuKy4nQO2Z4Am1.htm)|Administer First Aid|Prodiguer les premiers soins|libre|
|[mk6rzaAzsBBRGJnh.htm](actions/mk6rzaAzsBBRGJnh.htm)|Call Upon the Brightness|Faire appel à l'illumination|libre|
|[MLchOIG6uLvzK3r0.htm](actions/MLchOIG6uLvzK3r0.htm)|Gain Contact|Obtenir un contact|libre|
|[MLOkyKi1Y3N6y56Q.htm](actions/MLOkyKi1Y3N6y56Q.htm)|Raise Neck|Dresser le cou|libre|
|[MrTULCUsE9naPxXg.htm](actions/MrTULCUsE9naPxXg.htm)|Race the Skies|Course des cieux|libre|
|[mVscmsZWWcVACdU5.htm](actions/mVscmsZWWcVACdU5.htm)|Soaring Flight|Voler haut|libre|
|[MY6z2b4GPhAD2Eoa.htm](actions/MY6z2b4GPhAD2Eoa.htm)|Share Life|Partager la vie|libre|
|[n5vwBnLSlIXL9ptp.htm](actions/n5vwBnLSlIXL9ptp.htm)|Manifest Eidolon|Manifester l'eidolon|libre|
|[N6U02s9qJKQIvmQd.htm](actions/N6U02s9qJKQIvmQd.htm)|Wish for Luck|Souhait de bonne fortune|libre|
|[naKVqd8POxcnGclz.htm](actions/naKVqd8POxcnGclz.htm)|Explode|Exploser|libre|
|[nbfNETdpee8CVM17.htm](actions/nbfNETdpee8CVM17.htm)|Flurry of Blows|Déluge de coups|officielle|
|[ncdryKskPwHMgHFh.htm](actions/ncdryKskPwHMgHFh.htm)|Wicked Thorns|Terribles épines|libre|
|[NdMSHDtV9G7MbdP2.htm](actions/NdMSHDtV9G7MbdP2.htm)|Survive the Wilds|Survie en milieu sauvage|libre|
|[nSTMF6kYIt6rXhJx.htm](actions/nSTMF6kYIt6rXhJx.htm)|Seething Frenzy|Frénésie effrénée|libre|
|[NT1rtY5DFJGsR5QR.htm](actions/NT1rtY5DFJGsR5QR.htm)|Create Familiar Fulu|Créer un familier fulu|libre|
|[nTBrvt2b9wngyr0i.htm](actions/nTBrvt2b9wngyr0i.htm)|Base Kinesis|Kinésie de base|libre|
|[NYR2IdfUiHevdt1V.htm](actions/NYR2IdfUiHevdt1V.htm)|Break Them Down|Les abattre|libre|
|[oAGawsNqr6FfMrSG.htm](actions/oAGawsNqr6FfMrSG.htm)|Pacifying Infusion|Perfusion pacificatrice|libre|
|[oaJeGdGPmHiNT5mP.htm](actions/oaJeGdGPmHiNT5mP.htm)|Strike, Breathe, Rend|Frappe, Souffle, Éventre|libre|
|[oanRfXGLnBm6mMVg.htm](actions/oanRfXGLnBm6mMVg.htm)|Dragon Breath|Souffle de dragon|libre|
|[oAWNluJaMlaGysXA.htm](actions/oAWNluJaMlaGysXA.htm)|Barbed Quills|Piquants barbelés|libre|
|[ObFY26oKlreyVIUm.htm](actions/ObFY26oKlreyVIUm.htm)|Fleeting Arc through Heaven and Earth|Un arc fugace traverse les cieux et la terre|libre|
|[OdIUybJ3ddfL7wzj.htm](actions/OdIUybJ3ddfL7wzj.htm)|Stand|Se relever|officielle|
|[Oh6H1fuXC6jK4Hcp.htm](actions/Oh6H1fuXC6jK4Hcp.htm)|Cycle Elemental Stance|Posture du cycle élémentaire|libre|
|[OizxuPb44g3eHPFh.htm](actions/OizxuPb44g3eHPFh.htm)|Borrow an Arcane Spell|Emprunter un sort arcanique|libre|
|[OJ9cIvPukPT0rppR.htm](actions/OJ9cIvPukPT0rppR.htm)|Wyrm's Breath|Souffle du Ver|libre|
|[On5CQjX4euWqToly.htm](actions/On5CQjX4euWqToly.htm)|Resist Elf Magic|Résister à la magie elfique|libre|
|[OQaFzDtVEOMWizJJ.htm](actions/OQaFzDtVEOMWizJJ.htm)|Repeat a Spell|Répéter un sort|officielle|
|[OqKYkzbc7NEWmJWB.htm](actions/OqKYkzbc7NEWmJWB.htm)|Rearrange Bones|Réarranger les os|libre|
|[Or6RLXeoZkN8CLdi.htm](actions/Or6RLXeoZkN8CLdi.htm)|Amulet's Abeyance|Évitement de l'amulette|libre|
|[orjJjLdm4XNAcFi8.htm](actions/orjJjLdm4XNAcFi8.htm)|Mark for Death|Marquer pour mort|libre|
|[OSefkMgojBLqmRDh.htm](actions/OSefkMgojBLqmRDh.htm)|Refocus|Refocaliser|libre|
|[OX4fy22hQgUHDr0q.htm](actions/OX4fy22hQgUHDr0q.htm)|Make an Impression|Faire bonne impression|libre|
|[PaQL7cyry64nx9CG.htm](actions/PaQL7cyry64nx9CG.htm)|Enlightenment in Adversity|Illumination dans l'adversité|libre|
|[PdEJEiPUP0FDaBki.htm](actions/PdEJEiPUP0FDaBki.htm)|Feed the Masses|Nourrir les masses|libre|
|[PE1jm9lBveujj2md.htm](actions/PE1jm9lBveujj2md.htm)|Cobble Together|Bricoler ensemble|libre|
|[plBGdZhqq5JBl1D8.htm](actions/plBGdZhqq5JBl1D8.htm)|Gather Information|Recueillir des informations|libre|
|[PM5jvValFkbFH3TV.htm](actions/PM5jvValFkbFH3TV.htm)|Mount|Se mettre en selle|libre|
|[PMbdMWc2QroouFGD.htm](actions/PMbdMWc2QroouFGD.htm)|Grapple|Saisir|libre|
|[pn557VVMaNJyq0jX.htm](actions/pn557VVMaNJyq0jX.htm)|True Shapeshift|Métamorphose ultime|libre|
|[PpfkUdNMckFE22WI.htm](actions/PpfkUdNMckFE22WI.htm)|Activate Resonant Reflection|Activer un reflet retentissant|libre|
|[pprgrYQ1QnIDGZiy.htm](actions/pprgrYQ1QnIDGZiy.htm)|Climb|Escalader|libre|
|[PTEiTXsObvtke43x.htm](actions/PTEiTXsObvtke43x.htm)|Stitching Strike|Frappe effilochée|libre|
|[pTHMZLqWDJ3lkan9.htm](actions/pTHMZLqWDJ3lkan9.htm)|Smoke Blending|Se fondre dans la fumée|libre|
|[pvQ5rY2zrtPI614F.htm](actions/pvQ5rY2zrtPI614F.htm)|Interact|Interagir|libre|
|[PWNi3h0F9EsttE2p.htm](actions/PWNi3h0F9EsttE2p.htm)|Administer Ambient Magic|Administrer la magie ambiante|libre|
|[Q4kdWVOf2ztIBFg1.htm](actions/Q4kdWVOf2ztIBFg1.htm)|Identify Alchemy|Identifier l'alchimie|libre|
|[Q5iIYCFdqJFM31GW.htm](actions/Q5iIYCFdqJFM31GW.htm)|Learn a Spell|Apprendre un sort|libre|
|[Q8H3lBgiUyGvZYTM.htm](actions/Q8H3lBgiUyGvZYTM.htm)|Telekinetic Assault|Assaut télékinésique|libre|
|[q9nbyIF0PEBqMtYe.htm](actions/q9nbyIF0PEBqMtYe.htm)|Command an Animal|Diriger un animal|libre|
|[qc0VsZ0UesnurUUB.htm](actions/qc0VsZ0UesnurUUB.htm)|Take a Breather|Reprendre son souffle|libre|
|[QDW9H8XLIjuW2fE4.htm](actions/QDW9H8XLIjuW2fE4.htm)|Spellstrike|Frappe de sort|libre|
|[Qf1ylAbdVi1rkc8M.htm](actions/Qf1ylAbdVi1rkc8M.htm)|Maneuver in Flight|Manoeuvrer en vol|officielle|
|[QGeA2sPtDdryTpWw.htm](actions/QGeA2sPtDdryTpWw.htm)|Overawe Crowd|Foule en grand émoi|libre|
|[QHFMeJGzFWj2QczA.htm](actions/QHFMeJGzFWj2QczA.htm)|Quick Tincture|Préparation rapide|libre|
|[QIrJJ1pl4H6DctaQ.htm](actions/QIrJJ1pl4H6DctaQ.htm)|Reconnoiter|Partir en reconnaissance|libre|
|[Qj36ramjkoKE8kzJ.htm](actions/Qj36ramjkoKE8kzJ.htm)|Decompose|Décomposer|libre|
|[qm7xptMSozAinnPS.htm](actions/qm7xptMSozAinnPS.htm)|Arrest a Fall|Arrêter une chute|officielle|
|[QNAVeNKtHA0EUw4X.htm](actions/QNAVeNKtHA0EUw4X.htm)|Feint|Feinter|officielle|
|[qVNVSmsgpKFGk9hV.htm](actions/qVNVSmsgpKFGk9hV.htm)|Conceal an Object|Dissimuler un objet|officielle|
|[QyzlsLrqM0EEwd7j.htm](actions/QyzlsLrqM0EEwd7j.htm)|Earn Income|Gagner de l'argent|libre|
|[r5Uth6yvCoE4tr9z.htm](actions/r5Uth6yvCoE4tr9z.htm)|Destructive Vengeance|Vengeance destructrice|libre|
|[RADNqsvAt4gP9FOX.htm](actions/RADNqsvAt4gP9FOX.htm)|Raconteur's Reload|Rechargement du baratineur|libre|
|[RDXXE7wMrSPCLv5k.htm](actions/RDXXE7wMrSPCLv5k.htm)|Steal|Voler|libre|
|[ri6ZVR7nhFOvZfVa.htm](actions/ri6ZVR7nhFOvZfVa.htm)|Pitch-Perfect Projection|Projection de tonalité parfaite|libre|
|[rlGVQku78DGoOl9V.htm](actions/rlGVQku78DGoOl9V.htm)|Nudging Whisper|Encouragement murmuré|libre|
|[Rlp7ND33yYfxiEWi.htm](actions/Rlp7ND33yYfxiEWi.htm)|Master Strike|Frappe de maître|libre|
|[rmnhzmJqfzEVnz1h.htm](actions/rmnhzmJqfzEVnz1h.htm)|Drain Familiar|Drain de familier|libre|
|[rmwa3OyhTZ2i2AHl.htm](actions/rmwa3OyhTZ2i2AHl.htm)|Craft|Fabriquer|libre|
|[Rp7gcG6Mxasurr8o.htm](actions/Rp7gcG6Mxasurr8o.htm)|Toxic Touch|Contact toxique|libre|
|[rqT4LMH7qbfyScBi.htm](actions/rqT4LMH7qbfyScBi.htm)|Reclaim Destiny|Reconquérir sa destinée|libre|
|[rSpCV0leurp2Bg2d.htm](actions/rSpCV0leurp2Bg2d.htm)|Instinctive Obfuscation|Obscurcissement instinctif|libre|
|[rU3CE5niG8vHc5x4.htm](actions/rU3CE5niG8vHc5x4.htm)|Shed the Mortal Skin|Muer la peau mortelle|libre|
|[RxIPBjodIRm7Pd6W.htm](actions/RxIPBjodIRm7Pd6W.htm)|Lucky Break|Pause chanceuse|libre|
|[s2RrhZx1f1X4YnYV.htm](actions/s2RrhZx1f1X4YnYV.htm)|Divert Lightning|Détourner la foudre|libre|
|[s4s40hJr5uRLOqix.htm](actions/s4s40hJr5uRLOqix.htm)|Change Tradition Focus|Se focaliser sur une autre tradition|libre|
|[s4V7JWSMF9JPJAeX.htm](actions/s4V7JWSMF9JPJAeX.htm)|Envenom|Envenimer|libre|
|[S9PZFOVe7zhORkUc.htm](actions/S9PZFOVe7zhORkUc.htm)|Absorb into the Aegis|Absorber dans l'égide|libre|
|[SB7cMECVtE06kByk.htm](actions/SB7cMECVtE06kByk.htm)|Cover Tracks|Dissimuler des traces|libre|
|[scCEnY6IhU59uekX.htm](actions/scCEnY6IhU59uekX.htm)|Bless Ally|Bénir un allié|libre|
|[SjmKHgI7a5Z9JzBx.htm](actions/SjmKHgI7a5Z9JzBx.htm)|Force Open|Ouvrir de force|libre|
|[SkZAQRkLLkmBQNB9.htm](actions/SkZAQRkLLkmBQNB9.htm)|Escape|S'échapper|officielle|
|[sL1J8cFwpy1lI359.htm](actions/sL1J8cFwpy1lI359.htm)|Study|Étudier|libre|
|[sn2hIy1iIJX9Vpgj.htm](actions/sn2hIy1iIJX9Vpgj.htm)|Point Out|Signaler|officielle|
|[SN3sVsU7rD2eBfkf.htm](actions/SN3sVsU7rD2eBfkf.htm)|Repel Ambient Magic|Repousser la magie ambiante|libre|
|[SowCo0BXngrpatgn.htm](actions/SowCo0BXngrpatgn.htm)|Royal Grace|Grâce royale|libre|
|[SPtzUNatWJvTK61y.htm](actions/SPtzUNatWJvTK61y.htm)|Spirit's Mercy|Pitié de l'esprit|libre|
|[t5nBkpjroaq7QBGK.htm](actions/t5nBkpjroaq7QBGK.htm)|Research|Rechercher|libre|
|[TC7OcDa7JlWbqMaN.htm](actions/TC7OcDa7JlWbqMaN.htm)|Treat Disease|Soigner une maladie|libre|
|[tfa4Sh7wcxCEqL29.htm](actions/tfa4Sh7wcxCEqL29.htm)|Follow the Expert|Suivre l'expert|libre|
|[tHCqgwjtQtzNqVvd.htm](actions/tHCqgwjtQtzNqVvd.htm)|Coerce|Contraindre|libre|
|[tHHpBREXDaafK3TF.htm](actions/tHHpBREXDaafK3TF.htm)|Spasm of the Berserker|Trépidation du berserker|libre|
|[TiNDYUGlMmxzxBYU.htm](actions/TiNDYUGlMmxzxBYU.htm)|Search|Fouiller|officielle|
|[Tj055UcNm6UEgtCg.htm](actions/Tj055UcNm6UEgtCg.htm)|Crawl|Ramper|libre|
|[TMBXArwICQRJdwT6.htm](actions/TMBXArwICQRJdwT6.htm)|Fey's Fortune|Chance féerique|libre|
|[tNrBIYct9l1lrW1I.htm](actions/tNrBIYct9l1lrW1I.htm)|Field of Roots|Champ de racines|libre|
|[travkW5KLTnIUt9Y.htm](actions/travkW5KLTnIUt9Y.htm)|Inured to Death|Insensible à la mort|libre|
|[tu5viJZT4zFE1sYn.htm](actions/tu5viJZT4zFE1sYn.htm)|Whirlwind Maul|Masse tourbillon|libre|
|[tuZnRWHixLArvaIf.htm](actions/tuZnRWHixLArvaIf.htm)|Glimpse of Redemption|Lueur de rédemption|officielle|
|[TXqTIwNGULs3j6CH.htm](actions/TXqTIwNGULs3j6CH.htm)|Bullying Press|Pression agressive|libre|
|[tzuYnmYCvA3zrj6w.htm](actions/tzuYnmYCvA3zrj6w.htm)|Giant-Felling Comet|Comète tombeuse de géant|libre|
|[UAaQk93a30nx0nYY.htm](actions/UAaQk93a30nx0nYY.htm)|Affix a Talisman|Fixer un talisman|libre|
|[ublVm5gmCIm3eRdQ.htm](actions/ublVm5gmCIm3eRdQ.htm)|Ring Bell|Sonner la cloche|libre|
|[UEkGL7uAGYDPFNfK.htm](actions/UEkGL7uAGYDPFNfK.htm)|Fire in the Hole|Mise à feu !|libre|
|[UHpkTuCtyaPqiCAB.htm](actions/UHpkTuCtyaPqiCAB.htm)|Step|Faire un pas|libre|
|[Ul4I0ER6pj3U5eAk.htm](actions/Ul4I0ER6pj3U5eAk.htm)|Invigorating Fear|Peur revigorante|libre|
|[uMFB3uw8WTWL0LZD.htm](actions/uMFB3uw8WTWL0LZD.htm)|Draconic Frenzy|Frénésie draconique|libre|
|[uOlyklPCUWLtCaYI.htm](actions/uOlyklPCUWLtCaYI.htm)|Chassis Deflection|Déflexion du châssis|libre|
|[Uq2qy9aGNQ5JcPI1.htm](actions/Uq2qy9aGNQ5JcPI1.htm)|Into the Fray|Dans le feu de l'action|libre|
|[UqUDWO0TuSzEAKpw.htm](actions/UqUDWO0TuSzEAKpw.htm)|Sustain an Effect|Maintenir un effet|libre|
|[ur4AfegC9RqRgy3e.htm](actions/ur4AfegC9RqRgy3e.htm)|Mystic Aegis|Égide mystique|libre|
|[uruoG0PuuA0CqEG7.htm](actions/uruoG0PuuA0CqEG7.htm)|Shed Spirit|Mue spirituelle|libre|
|[uS3qDAgOkZ7b8ERL.htm](actions/uS3qDAgOkZ7b8ERL.htm)|Drive|Diriger|libre|
|[ust1jJSCZQUhBZIz.htm](actions/ust1jJSCZQUhBZIz.htm)|Take Cover|Mise à l'abri|officielle|
|[UWdRX1VelipCzrCg.htm](actions/UWdRX1VelipCzrCg.htm)|Avert Gaze|Détourner le regard|officielle|
|[uWTQxEOj2pl45Kns.htm](actions/uWTQxEOj2pl45Kns.htm)|Sense Weakness|Percevoir les faiblesses|libre|
|[UyMkWfVqdabLTgkH.htm](actions/UyMkWfVqdabLTgkH.htm)|Wind Them Up|Les agacer|libre|
|[V4HToYPQlkw8AW50.htm](actions/V4HToYPQlkw8AW50.htm)|Embrace of Destiny|Étreinte de la destinée|libre|
|[v82XtjAVN4ffgVVR.htm](actions/v82XtjAVN4ffgVVR.htm)|Drain Bonded Item|Drain d'objet lié|libre|
|[VAcxOCFQLb3Bap7K.htm](actions/VAcxOCFQLb3Bap7K.htm)|Accept Echo|Accepter l'écho|libre|
|[VBwNo2Wbfd4n4Y3g.htm](actions/VBwNo2Wbfd4n4Y3g.htm)|Patron's Presence|Présence du patron|libre|
|[VCUz5EnBUJF07j1a.htm](actions/VCUz5EnBUJF07j1a.htm)|Sever Conduit|Couper le conduit|libre|
|[vdnuczo4ktS7ow7N.htm](actions/vdnuczo4ktS7ow7N.htm)|Prophecy's Pawn|Jouet de la prophétie|libre|
|[vFaNE7s7vFs9BxJW.htm](actions/vFaNE7s7vFs9BxJW.htm)|Set Free|Délivrer|libre|
|[VjxZFuUXrCU94MWR.htm](actions/VjxZFuUXrCU94MWR.htm)|Strike|Frapper|libre|
|[VMozDqMMuK5kpoX4.htm](actions/VMozDqMMuK5kpoX4.htm)|Sneak|Être furtif|officielle|
|[VOEWhPQfN3lvHivK.htm](actions/VOEWhPQfN3lvHivK.htm)|Foresight|Prémonition|libre|
|[VPl4pifKcq7ecK87.htm](actions/VPl4pifKcq7ecK87.htm)|Stridulating Song|Chanson stridulante|libre|
|[VQ5OxaDKE0lCj8Mr.htm](actions/VQ5OxaDKE0lCj8Mr.htm)|Shadow Step|Pas d'ombre|libre|
|[Vr3w3t028ZKDg0mO.htm](actions/Vr3w3t028ZKDg0mO.htm)|Breath Weapon|Arme de souffle|libre|
|[vZltxwRNvF5khf9a.htm](actions/vZltxwRNvF5khf9a.htm)|Boarding Assault|Assaut d'abordage|libre|
|[W4M8L9WepGLamlHs.htm](actions/W4M8L9WepGLamlHs.htm)|Threatening Approach|Approche menaçante|libre|
|[wfUIsKBxCBfXi1TF.htm](actions/wfUIsKBxCBfXi1TF.htm)|Erupting Spurs|Éruption d'éperons|libre|
|[wGTHH6v25CDavO6g.htm](actions/wGTHH6v25CDavO6g.htm)|Shake It Off|Secoue-toi|libre|
|[WKEFnxUH0CkM8bWq.htm](actions/WKEFnxUH0CkM8bWq.htm)|Impromptu Investment|Investiture impromptue|libre|
|[wKPtG0pzARZLGHZT.htm](actions/wKPtG0pzARZLGHZT.htm)|I Defy You!|Je vous défie !|libre|
|[wnZhAUiYcs2mv8sZ.htm](actions/wnZhAUiYcs2mv8sZ.htm)|Primal Howl|Hurlement primordial|libre|
|[WpCs3QmPVn8SRbXy.htm](actions/WpCs3QmPVn8SRbXy.htm)|Touch and Go|Toucher et aller|libre|
|[wQYmDStjdjn0I26t.htm](actions/wQYmDStjdjn0I26t.htm)|Release|Relâcher|libre|
|[x1qSEkzHAviQ5jry.htm](actions/x1qSEkzHAviQ5jry.htm)|Lay Down Arms|Les bras m'en tombent|libre|
|[Xc5MLIbT5OPhnFIJ.htm](actions/Xc5MLIbT5OPhnFIJ.htm)|Indomitable Act|Acte indomptable|libre|
|[xccOiNL2W1EtfUYl.htm](actions/xccOiNL2W1EtfUYl.htm)|Pointed Question|Question imparable|libre|
|[XeZwXzR1KBlJF770.htm](actions/XeZwXzR1KBlJF770.htm)|Resist Magic|Résister à la magie|libre|
|[XFdTDDAPO7U0r4Et.htm](actions/XFdTDDAPO7U0r4Et.htm)|Sever Four Dragonfly Wings|Sectionner quatre ailes de libellules|libre|
|[Xg57qG1rOfSSobke.htm](actions/Xg57qG1rOfSSobke.htm)|Breath Weapon (Riding Drake)|Arme de souffle (Drake de monte)|libre|
|[xGqOIheAOV12RGU4.htm](actions/xGqOIheAOV12RGU4.htm)|Dueling Counter|Contre de duel|libre|
|[XH133DE3daobeav1.htm](actions/XH133DE3daobeav1.htm)|Screaming Skull|Crâne hurlant|libre|
|[XiiR6WwfTcwIUvHG.htm](actions/XiiR6WwfTcwIUvHG.htm)|Plummeting Roll|Roulade en chute libre|libre|
|[Xj3LvT3I78cVwzer.htm](actions/Xj3LvT3I78cVwzer.htm)|Shatter Glass|Éclats de verre|libre|
|[xJEkXFJgEfEida27.htm](actions/xJEkXFJgEfEida27.htm)|Rally|Rallier|libre|
|[xjGwis0uaC2305pm.htm](actions/xjGwis0uaC2305pm.htm)|Raise a Shield|Lever un bouclier|libre|
|[XkrN7gxdRXTYYBkX.htm](actions/XkrN7gxdRXTYYBkX.htm)|Restore the Mind|Restaurer l'esprit|libre|
|[XMcnh4cSI32tljXa.htm](actions/XMcnh4cSI32tljXa.htm)|Hide|Se cacher|libre|
|[XotRIv6tKRtuAGAF.htm](actions/XotRIv6tKRtuAGAF.htm)|Blazing Conflagration|Conflagration embrasée|libre|
|[xpsD4DsYHKXCB4ac.htm](actions/xpsD4DsYHKXCB4ac.htm)|Anchor|S'ancrer|libre|
|[xTK2zsWFyxSJvYbX.htm](actions/xTK2zsWFyxSJvYbX.htm)|Pursue a Lead|Suivre une piste|libre|
|[Y6ee2ZvIE2fzzvAY.htm](actions/Y6ee2ZvIE2fzzvAY.htm)|Blinding of the Needle|Aveuglement de l'aiguille|libre|
|[Yb0C1uLzeHrVLl7a.htm](actions/Yb0C1uLzeHrVLl7a.htm)|Detect Magic|Détection de la magie|officielle|
|[Yfl6q6Pi42FttDRE.htm](actions/Yfl6q6Pi42FttDRE.htm)|Glimpse Vulnerability|Entrevoir une vulnérabilité|libre|
|[yh9O9BQjwWrAIiuf.htm](actions/yh9O9BQjwWrAIiuf.htm)|Take Control|Prendre le contrôle|libre|
|[YjrM5G8Up0wv6x0u.htm](actions/YjrM5G8Up0wv6x0u.htm)|Chaotic Destiny|Destinée chaotique|libre|
|[yM20ZnvmNxu9qMDV.htm](actions/yM20ZnvmNxu9qMDV.htm)|Compose Missive|Composer une missive|libre|
|[YMhmebfXAoOFXeSB.htm](actions/YMhmebfXAoOFXeSB.htm)|Drifter's Wake|Sillage du vagabond|libre|
|[yOk8s1pftOe5I9Sg.htm](actions/yOk8s1pftOe5I9Sg.htm)|Shielding Wave|Vague protectrice|libre|
|[yOtu5X3qWfjuX8Vy.htm](actions/yOtu5X3qWfjuX8Vy.htm)|Learn Name|Apprendre un nom|libre|
|[YpuEmI1fJBZD3kMc.htm](actions/YpuEmI1fJBZD3kMc.htm)|Tendril Strike|Frappe étendue|libre|
|[Ypwa9HIw7uXdt25D.htm](actions/Ypwa9HIw7uXdt25D.htm)|Topple the Pillar of Heaven|Renverser le pilier des cieux|libre|
|[yXk0G8l0leaqHh1U.htm](actions/yXk0G8l0leaqHh1U.htm)|Host Spirit|Accueillir un esprit|libre|
|[yzNJgwzV9XqEhKc6.htm](actions/yzNJgwzV9XqEhKc6.htm)|Quick Alchemy|Alchimie rapide|libre|
|[Z8aa6afUterlfh5i.htm](actions/Z8aa6afUterlfh5i.htm)|Travel|Voyager|libre|
|[ZC4SuXmL79N9VVIW.htm](actions/ZC4SuXmL79N9VVIW.htm)|Patron's Claim|Réclamation du patron|libre|
|[ZhKXvnw7ND2hQ2pp.htm](actions/ZhKXvnw7ND2hQ2pp.htm)|Cleanse Soul Path|Purifier le chemin spirituel|libre|
|[ZJcc7KGOEsYvN6SE.htm](actions/ZJcc7KGOEsYvN6SE.htm)|Overload Vision|Vision surchargée|libre|
|[ZL5o9c4jV5fRwTGR.htm](actions/ZL5o9c4jV5fRwTGR.htm)|Break Free|Se libérer|libre|
|[zs4ZMcH9oFSCgkBx.htm](actions/zs4ZMcH9oFSCgkBx.htm)|Claw Stance|Posture de griffe|libre|
|[ZUAaBIAntZhFQixm.htm](actions/ZUAaBIAntZhFQixm.htm)|Deconstruct|Démanteler|libre|
|[zUWj4zmBNWOTzeFJ.htm](actions/zUWj4zmBNWOTzeFJ.htm)|Overwhelming Combination|Impressionnante combinaison|libre|
|[ZXvJGxMJc0HRpcQh.htm](actions/ZXvJGxMJc0HRpcQh.htm)|Inspired Stratagem|Stratagème inspiré|libre|
