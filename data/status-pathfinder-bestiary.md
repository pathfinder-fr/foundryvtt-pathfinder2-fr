# État de la traduction (pathfinder-bestiary)

 * **officielle**: 156
 * **libre**: 22


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[024PqcF8yMRBrPuq.htm](pathfinder-bestiary/024PqcF8yMRBrPuq.htm)|White Dragon (Adult)|Dragon Blanc (Adulte, Chromatique)|officielle|
|[0FGz2eXm0SB04sJW.htm](pathfinder-bestiary/0FGz2eXm0SB04sJW.htm)|Green Dragon (Ancient)|Dragon vert (Vénérable, Chromatique)|officielle|
|[0HjVFx8qIKDCfblg.htm](pathfinder-bestiary/0HjVFx8qIKDCfblg.htm)|Lantern Archon|Archon de lumière|libre|
|[0MOWKI97bgGDf5Xi.htm](pathfinder-bestiary/0MOWKI97bgGDf5Xi.htm)|Dark Naga|Naga ténébreux|libre|
|[2T2cN4j4EzfDAIEe.htm](pathfinder-bestiary/2T2cN4j4EzfDAIEe.htm)|Red Dragon (Ancient, Spellcaster)|Dragon rouge (Vénérable, Incantateur)|libre|
|[31kXa1P0LUl38jYG.htm](pathfinder-bestiary/31kXa1P0LUl38jYG.htm)|Zaramuun|Zaramuun|libre|
|[3nOBhH8j9I7ps6fC.htm](pathfinder-bestiary/3nOBhH8j9I7ps6fC.htm)|Green Hag|Guenaude verte|libre|
|[3OPUMuBQThM5EXjz.htm](pathfinder-bestiary/3OPUMuBQThM5EXjz.htm)|White Dragon (Young)|Dragon blanc (Jeune, Chromatique)|officielle|
|[3W2PMV6IbVdouOB1.htm](pathfinder-bestiary/3W2PMV6IbVdouOB1.htm)|Copper Dragon (Young)|Dragon de cuivre (Jeune, Métallique)|officielle|
|[4B47SUq57pcl3U9u.htm](pathfinder-bestiary/4B47SUq57pcl3U9u.htm)|Legion Archon|Archon Légionnaire|libre|
|[4C3Sh2lO9oWi78Zo.htm](pathfinder-bestiary/4C3Sh2lO9oWi78Zo.htm)|Blue Dragon (Ancient, Spellcaster)|Dragon bleu (Vénérable, Incantateur)|libre|
|[4cPw8hZwW6uvyzvh.htm](pathfinder-bestiary/4cPw8hZwW6uvyzvh.htm)|Vrock|Vrock (Démon)|officielle|
|[4npwV3fuBm3sBCPG.htm](pathfinder-bestiary/4npwV3fuBm3sBCPG.htm)|Xorn|Xorn (Élémentaire, terre)|officielle|
|[5XtCOknYJuEl4SfD.htm](pathfinder-bestiary/5XtCOknYJuEl4SfD.htm)|Green Dragon (Young, Spellcaster)|Dragon vert (Jeune, Incantateur)|officielle|
|[6UczvqBDlGNXcIlW.htm](pathfinder-bestiary/6UczvqBDlGNXcIlW.htm)|Tidal Master|Maître des marées (Élémentaire, eau)|officielle|
|[6XlGTt3RveX49YbC.htm](pathfinder-bestiary/6XlGTt3RveX49YbC.htm)|Shambler|Grand tertre|officielle|
|[7EpLGBoddL8Vzlha.htm](pathfinder-bestiary/7EpLGBoddL8Vzlha.htm)|Soulbound Doll (Chaotic Evil)|Poupée des âmes (Chaotique mauvais)|libre|
|[7ptWlh4t6oHvYOcj.htm](pathfinder-bestiary/7ptWlh4t6oHvYOcj.htm)|Soulbound Doll (Neutral Evil)|Poupée des âmes (Neutre mauvais)|officielle|
|[8JvzSTwQWtOsxRfL.htm](pathfinder-bestiary/8JvzSTwQWtOsxRfL.htm)|Quetzalcoatlus|Quetzalcoatl (Ptérosaure)|officielle|
|[93hZtLl9pRRfqI05.htm](pathfinder-bestiary/93hZtLl9pRRfqI05.htm)|Kobold Dragon Mage (Blue)|Mage draconique kobold (Bleu)|libre|
|[9H11QHnoKz8hHKSM.htm](pathfinder-bestiary/9H11QHnoKz8hHKSM.htm)|Red Dragon (Ancient)|Dragon rouge (Vénérable, Chromatique)|officielle|
|[9jF564DF6ylEovna.htm](pathfinder-bestiary/9jF564DF6ylEovna.htm)|Quasit|Quasit (Démon)|officielle|
|[9vNYtJZiseCEf4wt.htm](pathfinder-bestiary/9vNYtJZiseCEf4wt.htm)|Balor|Balor (Démon)|libre|
|[9xdl2PpfMEfJWhSO.htm](pathfinder-bestiary/9xdl2PpfMEfJWhSO.htm)|Gold Dragon (Young, Spellcaster)|Dragon d'or (Jeune, Incantateur)|officielle|
|[aAiuQvMGPN9QXwKY.htm](pathfinder-bestiary/aAiuQvMGPN9QXwKY.htm)|Gold Dragon (Adult)|Dragon d'or (Adulte, Métallique)|officielle|
|[aD76W2uEQhFFUrom.htm](pathfinder-bestiary/aD76W2uEQhFFUrom.htm)|Chuul|Chuul|officielle|
|[AdQVjlOWB6rmBRVp.htm](pathfinder-bestiary/AdQVjlOWB6rmBRVp.htm)|Doppelganger|Doppelganger|officielle|
|[aq2H1lRALUNMEGRG.htm](pathfinder-bestiary/aq2H1lRALUNMEGRG.htm)|Guardian Naga|Naga gardien|libre|
|[aqW5sp9HPgZgpXxs.htm](pathfinder-bestiary/aqW5sp9HPgZgpXxs.htm)|Hill Giant|Géant des collines|officielle|
|[AQwxOoYBwDUAwvfh.htm](pathfinder-bestiary/AQwxOoYBwDUAwvfh.htm)|Black Dragon (Adult, Spellcaster)|Dragon noir (Adulte, Incantateur)|libre|
|[ar0tszs8NAgQamHX.htm](pathfinder-bestiary/ar0tszs8NAgQamHX.htm)|Blue Dragon (Young)|Dragon bleu (Jeune, Chromatique)|officielle|
|[ASc5sT2hU84tp1fx.htm](pathfinder-bestiary/ASc5sT2hU84tp1fx.htm)|Simurgh|Simurgh|officielle|
|[B0QyZEjAXUG5TsJU.htm](pathfinder-bestiary/B0QyZEjAXUG5TsJU.htm)|Storm Giant|Géant des tempêtes|officielle|
|[B7eLG2k7qUo8HU6O.htm](pathfinder-bestiary/B7eLG2k7qUo8HU6O.htm)|Drider|Drider (Distordu)|libre|
|[B9KJUdZre51J3E3e.htm](pathfinder-bestiary/B9KJUdZre51J3E3e.htm)|Copper Dragon (Ancient)|Dragon de cuivre (Vénérable, Métallique)|libre|
|[bkaDwBD3mIBgvULs.htm](pathfinder-bestiary/bkaDwBD3mIBgvULs.htm)|Cloaker|Manteleur|officielle|
|[Br1AtKUHe3nbzjnY.htm](pathfinder-bestiary/Br1AtKUHe3nbzjnY.htm)|Mimic|Mimique|officielle|
|[BRo0snV2sH6TFuh6.htm](pathfinder-bestiary/BRo0snV2sH6TFuh6.htm)|Glabrezu|Glabrezu (Démon)|libre|
|[bSjF2lCgchtp2ocS.htm](pathfinder-bestiary/bSjF2lCgchtp2ocS.htm)|Brass Dragon (Ancient)|Dragon d'airain (Vénérable, Métallique)|officielle|
|[BzBFDaNj51PP97RZ.htm](pathfinder-bestiary/BzBFDaNj51PP97RZ.htm)|Bronze Dragon (Ancient)|Dragon de bronze (Vénérable, Métallique)|officielle|
|[c0zqasudrwZU3fdw.htm](pathfinder-bestiary/c0zqasudrwZU3fdw.htm)|Efreeti|Éfrit (Génie)|officielle|
|[CHIh3vMssFixUlw8.htm](pathfinder-bestiary/CHIh3vMssFixUlw8.htm)|Salamander|Salamandre (Élémentaire, feu)|officielle|
|[CJuHwIRCAgTB1SEl.htm](pathfinder-bestiary/CJuHwIRCAgTB1SEl.htm)|Kobold Dragon Mage (Red)|Mage draconique kobold (Rouge)|officielle|
|[csfVqep68nYGHExr.htm](pathfinder-bestiary/csfVqep68nYGHExr.htm)|White Dragon (Young, Spellcaster)|Dragon blanc (Jeune, Incantateur)|officielle|
|[csRH8Fx0r6iMWlFc.htm](pathfinder-bestiary/csRH8Fx0r6iMWlFc.htm)|Ether Spider|Araignée de l'éther|officielle|
|[cuET2PHGcE7eL7DJ.htm](pathfinder-bestiary/cuET2PHGcE7eL7DJ.htm)|Marid|Maride (Génie)|officielle|
|[cZsaAKlEYWZUO1CV.htm](pathfinder-bestiary/cZsaAKlEYWZUO1CV.htm)|Terotricus|Terotricus|libre|
|[CzxQpB3p0d9hwPeR.htm](pathfinder-bestiary/CzxQpB3p0d9hwPeR.htm)|Bulette|Bulette|officielle|
|[dEAneTvoPuQXZoLR.htm](pathfinder-bestiary/dEAneTvoPuQXZoLR.htm)|Gibbering Mouther|Babélien|officielle|
|[DlRe4c2XlBSpwmms.htm](pathfinder-bestiary/DlRe4c2XlBSpwmms.htm)|Greater Barghest|Barghest supérieur|officielle|
|[DolNTN9s2p79N8Cy.htm](pathfinder-bestiary/DolNTN9s2p79N8Cy.htm)|Gold Dragon (Young)|Dragon d'or (Jeune, Métallique)|officielle|
|[Dp6dq9Jd0TanufdU.htm](pathfinder-bestiary/Dp6dq9Jd0TanufdU.htm)|Blue Dragon (Adult, Spellcaster)|Dragon bleu (Adulte, Incantateur)|officielle|
|[dTz1SQvJIUsB9S2w.htm](pathfinder-bestiary/dTz1SQvJIUsB9S2w.htm)|Drow Fighter|Guerrier Drow|officielle|
|[dYbfZnB1ngiqPm4O.htm](pathfinder-bestiary/dYbfZnB1ngiqPm4O.htm)|Silver Dragon (Young, Spellcaster)|Dragon d'argent (Jeune, Incantateur)|officielle|
|[ETNlci6VnSthx5V6.htm](pathfinder-bestiary/ETNlci6VnSthx5V6.htm)|Erinys|Érinye (Diable)|officielle|
|[F99r7jcWX5YZB8xw.htm](pathfinder-bestiary/F99r7jcWX5YZB8xw.htm)|Brass Dragon (Adult, Spellcaster)|Dragon d'airain (Adulte, Incantateur)|officielle|
|[FaBHkmFGuEIqIYM1.htm](pathfinder-bestiary/FaBHkmFGuEIqIYM1.htm)|Drow Priestess|Prêtresse Drow|officielle|
|[fcE9PgnC82e9haL5.htm](pathfinder-bestiary/fcE9PgnC82e9haL5.htm)|Brass Dragon (Young, Spellcaster)|Dragon d'airain (Jeune, Incantateur)|officielle|
|[fd3Bf10lloqMg2jW.htm](pathfinder-bestiary/fd3Bf10lloqMg2jW.htm)|Blue Dragon (Young, Spellcaster)|Dragon bleu (Jeune, Incantateur)|officielle|
|[FIoRPHaHdYUPVKdT.htm](pathfinder-bestiary/FIoRPHaHdYUPVKdT.htm)|Barbazu|Barbazu (Diable)|officielle|
|[fsdCVADRbLxpBVT8.htm](pathfinder-bestiary/fsdCVADRbLxpBVT8.htm)|Bronze Dragon (Young, Spellcaster)|Dragon de bronze (Jeune, Incantateur)|officielle|
|[FwH05kDUlC8CwWTU.htm](pathfinder-bestiary/FwH05kDUlC8CwWTU.htm)|Troll King|Roi Troll|officielle|
|[Fzn4jHusVeyytgfx.htm](pathfinder-bestiary/Fzn4jHusVeyytgfx.htm)|Copper Dragon (Adult)|Dragon de cuivre (Adulte, Métallique)|officielle|
|[gDMPUL0UiOHrUUd3.htm](pathfinder-bestiary/gDMPUL0UiOHrUUd3.htm)|Aasimar Redeemer|Rédempteur Aasimar (Héritier des plans)|officielle|
|[gvCCATlH9mPGWbsp.htm](pathfinder-bestiary/gvCCATlH9mPGWbsp.htm)|Troll|Troll|officielle|
|[GyigGu36XLPV72nW.htm](pathfinder-bestiary/GyigGu36XLPV72nW.htm)|Djinni|Djinn (Génie)|officielle|
|[gztjujplfVCBVXv0.htm](pathfinder-bestiary/gztjujplfVCBVXv0.htm)|Lantern Archon (Gestalt)|Archon lumineux (Gestalt)|libre|
|[HFbZ580RDOG6Rxz2.htm](pathfinder-bestiary/HFbZ580RDOG6Rxz2.htm)|Sea Devil Baron|Baron diable des mers|officielle|
|[hNR4xZRsxUkGPI1v.htm](pathfinder-bestiary/hNR4xZRsxUkGPI1v.htm)|Dandasuka|Dandasuka (Rakshasas)|officielle|
|[hOXpmWmXU8N3n4Bw.htm](pathfinder-bestiary/hOXpmWmXU8N3n4Bw.htm)|Raja Rakshasa|Rakshasa raja|officielle|
|[HpD0BTfid3hnUEWj.htm](pathfinder-bestiary/HpD0BTfid3hnUEWj.htm)|Bloodseeker|Cherchesang|officielle|
|[i3N3udPyTGVPLpoq.htm](pathfinder-bestiary/i3N3udPyTGVPLpoq.htm)|Blue Dragon (Adult)|Dragon bleu (Adulte, Chromatique)|officielle|
|[iF5NeHC6ReqXLFmY.htm](pathfinder-bestiary/iF5NeHC6ReqXLFmY.htm)|Soulbound Doll (Neutral Good)|Poupée des âmes (Neutre bon)|officielle|
|[iII08V0HlvWGWSmu.htm](pathfinder-bestiary/iII08V0HlvWGWSmu.htm)|Horned Archon|Archon cornu|officielle|
|[IMasNR02C74jy3cT.htm](pathfinder-bestiary/IMasNR02C74jy3cT.htm)|Bronze Dragon (Adult)|Dragon de bronze (Adulte, Métallique)|officielle|
|[ImueS9YFhV6sxqBP.htm](pathfinder-bestiary/ImueS9YFhV6sxqBP.htm)|Marilith|Marilith (Démon)|officielle|
|[irrXrWxJ0LYSUCQB.htm](pathfinder-bestiary/irrXrWxJ0LYSUCQB.htm)|Black Dragon (Adult)|Dragon noir (Adulte, Chromatique)|officielle|
|[Iw2QccAkRc5Vnfzj.htm](pathfinder-bestiary/Iw2QccAkRc5Vnfzj.htm)|Ghaele|Ghaéle (Azata)|officielle|
|[j0gCguJx3ILB1j9o.htm](pathfinder-bestiary/j0gCguJx3ILB1j9o.htm)|Soulbound Doll (Chaotic Good)|Poupée des âmes (Chaotique bon)|officielle|
|[j1mm6bb3hZ56jSrK.htm](pathfinder-bestiary/j1mm6bb3hZ56jSrK.htm)|Grig|Grig (Esprit follet)|officielle|
|[j7NNPfZwD19BwSEZ.htm](pathfinder-bestiary/j7NNPfZwD19BwSEZ.htm)|Phantasmal Minion|Sbire fantasmagorique|officielle|
|[Jgh0WGVetNXi5jlM.htm](pathfinder-bestiary/Jgh0WGVetNXi5jlM.htm)|Quatoid|Quatoïde (Élémentaire, eau)|officielle|
|[JhQGNMlKARMx1n2D.htm](pathfinder-bestiary/JhQGNMlKARMx1n2D.htm)|Red Dragon (Adult)|Dragon rouge (Adulte, Chromatique)|officielle|
|[JiThbhDfjUoPaTP1.htm](pathfinder-bestiary/JiThbhDfjUoPaTP1.htm)|Baomal|Baomal|officielle|
|[JKG3m8pFISF48Sfr.htm](pathfinder-bestiary/JKG3m8pFISF48Sfr.htm)|Green Dragon (Adult, Spellcaster)|Dragon vert (Adulte, Incantateur)|officielle|
|[jlEFaUJjAK4MTsLc.htm](pathfinder-bestiary/jlEFaUJjAK4MTsLc.htm)|Silver Dragon (Ancient, Spellcaster)|Dragon d'argent (Vénérable, Incantateur)|officielle|
|[JnOgG1xfWleFGNt9.htm](pathfinder-bestiary/JnOgG1xfWleFGNt9.htm)|Brass Dragon (Adult)|Dragon d'airain (Adulte, Métallique)|officielle|
|[k9fM0vufbdsDPQul.htm](pathfinder-bestiary/k9fM0vufbdsDPQul.htm)|Fire Mephit|Méphite du feu (Élémentaire)|officielle|
|[kbZ3t5HhHRJJ4AMn.htm](pathfinder-bestiary/kbZ3t5HhHRJJ4AMn.htm)|Brass Dragon (Ancient, Spellcaster)|Dragon d'airain (Vénérable, Incantateur)|officielle|
|[kCRfBZqCugMQmdpd.htm](pathfinder-bestiary/kCRfBZqCugMQmdpd.htm)|Kobold Dragon Mage (White)|Mage draconique kobold (blanc)|officielle|
|[KDRlxdIUADWHI6Vr.htm](pathfinder-bestiary/KDRlxdIUADWHI6Vr.htm)|Air Mephit|Méphite d'air (Élémentaire)|officielle|
|[keCgklXcy4HZgQIL.htm](pathfinder-bestiary/keCgklXcy4HZgQIL.htm)|Adamantine Golem|Golem d'adamantium|officielle|
|[KFf6pJmFmzj7HH9i.htm](pathfinder-bestiary/KFf6pJmFmzj7HH9i.htm)|Water Mephit|Méphite de l'eau (Élémentaire)|officielle|
|[kNmRn3WfiWLsuwoe.htm](pathfinder-bestiary/kNmRn3WfiWLsuwoe.htm)|Gelugon|Gélugon (Diable)|officielle|
|[kPBSSOuR8n8vZHXB.htm](pathfinder-bestiary/kPBSSOuR8n8vZHXB.htm)|Otyugh|Otyugh|officielle|
|[kR1wGS61ubIpH8A6.htm](pathfinder-bestiary/kR1wGS61ubIpH8A6.htm)|Black Dragon (Young, Spellcaster)|Dragon noir (Jeune, Incantateur)|officielle|
|[l05LjJTXvFS4tYTE.htm](pathfinder-bestiary/l05LjJTXvFS4tYTE.htm)|Centaur|Centaure|officielle|
|[LHHgGSs0ELCR4CYK.htm](pathfinder-bestiary/LHHgGSs0ELCR4CYK.htm)|Ghoul|Goule|officielle|
|[LUtSo30fQWj7mrDn.htm](pathfinder-bestiary/LUtSo30fQWj7mrDn.htm)|Storm Lord|Seigneur des tempêtes (Élémentaire, air)|officielle|
|[Lvdykf6wfqzZBlZd.htm](pathfinder-bestiary/Lvdykf6wfqzZBlZd.htm)|Black Dragon (Ancient)|Dragon noir (Vénérable, Chromatique)|officielle|
|[mEmWRqTRxLUZQYSh.htm](pathfinder-bestiary/mEmWRqTRxLUZQYSh.htm)|Rust Monster|Oxydeur|officielle|
|[MhEfUXkSfLtgpmfE.htm](pathfinder-bestiary/MhEfUXkSfLtgpmfE.htm)|White Dragon (Adult, Spellcaster)|Dragon blanc (Adulte, Incantateur)|officielle|
|[MLmwdUIW4o7ZSUEG.htm](pathfinder-bestiary/MLmwdUIW4o7ZSUEG.htm)|Bronze Dragon (Ancient, Spellcaster)|Dragon de bronze (Vénérable, Incantateur)|officielle|
|[mXkMSlj3LldzKxB9.htm](pathfinder-bestiary/mXkMSlj3LldzKxB9.htm)|White Dragon (Ancient)|Dragon blanc (Vénérable, Chromatique)|officielle|
|[n1Mv0Q1MirfjBmfI.htm](pathfinder-bestiary/n1Mv0Q1MirfjBmfI.htm)|Uthul|Uthul|officielle|
|[NgCKGq28Qvt6ZOSr.htm](pathfinder-bestiary/NgCKGq28Qvt6ZOSr.htm)|Gold Dragon (Adult, Spellcaster)|Dragon d'or (Adulte, Incantateur)|officielle|
|[NTXm3ee7WZJ92Sww.htm](pathfinder-bestiary/NTXm3ee7WZJ92Sww.htm)|Wemmuth|Wemmuth|officielle|
|[NW1Ax1QGE9W4DmiN.htm](pathfinder-bestiary/NW1Ax1QGE9W4DmiN.htm)|Ceustodaemon|Ceustodaémon (Daémon)|officielle|
|[nWiibOE9wVSlTdkx.htm](pathfinder-bestiary/nWiibOE9wVSlTdkx.htm)|Gold Dragon (Ancient)|Dragon d'or (Vénérable, Métallique)|officielle|
|[oj0vzfjflUc3xMBo.htm](pathfinder-bestiary/oj0vzfjflUc3xMBo.htm)|Bronze Dragon (Adult, Spellcaster)|Dragon de bronze (Adulte, Incantateur)|officielle|
|[oLdrCmarnzd62kA8.htm](pathfinder-bestiary/oLdrCmarnzd62kA8.htm)|Red Dragon (Young, Spellcaster)|Dragon rouge (Jeune, Incantateur)|officielle|
|[OqCiTKXZu7EWmAI0.htm](pathfinder-bestiary/OqCiTKXZu7EWmAI0.htm)|White Dragon (Ancient, Spellcaster)|Dragon blanc (Vénérable, Incantateur)|officielle|
|[ouOt6d8xUFk8NhJ9.htm](pathfinder-bestiary/ouOt6d8xUFk8NhJ9.htm)|Copper Dragon (Adult, Spellcaster)|Dragon de cuivre (Adulte, Incantateur)|officielle|
|[p2ECLKUzZ0GhFg22.htm](pathfinder-bestiary/p2ECLKUzZ0GhFg22.htm)|Green Dragon (Ancient, Spellcaster)|Dragon vert (Vénérable, Incantateur)|officielle|
|[pFmaszqtsA2yt7dv.htm](pathfinder-bestiary/pFmaszqtsA2yt7dv.htm)|Red Dragon (Adult, Spellcaster)|Dragon rouge (Adulte, Incantateur)|officielle|
|[PgBsMyjMITjQnCs8.htm](pathfinder-bestiary/PgBsMyjMITjQnCs8.htm)|Kolyarut|Kolyarut (Aéon)|libre|
|[Pl3J94dvVY159G64.htm](pathfinder-bestiary/Pl3J94dvVY159G64.htm)|Soulbound Doll (True Neutral)|Poupée des âmes (Neutre strict)|officielle|
|[pL4sS2HZtGAryKnN.htm](pathfinder-bestiary/pL4sS2HZtGAryKnN.htm)|Naunet|Naunet (Protéen)|officielle|
|[pTkz08ak9YlKRsOY.htm](pathfinder-bestiary/pTkz08ak9YlKRsOY.htm)|Hive Mother|Mère de la ruche ankhrav|officielle|
|[PXru127aJljwKhSy.htm](pathfinder-bestiary/PXru127aJljwKhSy.htm)|Sea Devil Scout|Éclaireur diable des mers|libre|
|[pYNMxg5z2nRFmkMM.htm](pathfinder-bestiary/pYNMxg5z2nRFmkMM.htm)|Black Dragon (Ancient, Spellcaster)|Dragon noir (Vénérable, Incantateur)|officielle|
|[pzG7Od1aHj0DkTzS.htm](pathfinder-bestiary/pzG7Od1aHj0DkTzS.htm)|Copper Dragon (Young, Spellcaster)|Dragon de cuivre (Jeune, Incantateur)|officielle|
|[Q3EaaLLx5kDXb5vQ.htm](pathfinder-bestiary/Q3EaaLLx5kDXb5vQ.htm)|Shoggoth|Shoggoth|officielle|
|[QaldZV2p9RpMXzzn.htm](pathfinder-bestiary/QaldZV2p9RpMXzzn.htm)|Kobold Dragon Mage (Green)|Mage draconique kobold (Vert)|officielle|
|[qGvK4f8Clc6dOwLU.htm](pathfinder-bestiary/qGvK4f8Clc6dOwLU.htm)|Copper Dragon (Ancient, Spellcaster)|Dragon de cuivre (Vénérable, Incantateur)|officielle|
|[qkWkshBHyTbLP07b.htm](pathfinder-bestiary/qkWkshBHyTbLP07b.htm)|Shield Archon|Archon bouclier|officielle|
|[qpRJOzx3bJ7rolHp.htm](pathfinder-bestiary/qpRJOzx3bJ7rolHp.htm)|Manticore|Manticore|officielle|
|[R9eoGwQ2tudxUKxS.htm](pathfinder-bestiary/R9eoGwQ2tudxUKxS.htm)|Kobold Dragon Mage (Black)|Mage draconique kobold (Noir)|officielle|
|[RAjbVzWcfrPBbgkK.htm](pathfinder-bestiary/RAjbVzWcfrPBbgkK.htm)|Soulbound Doll (Lawful Good)|Poupée des âmes (Loyal bon)|officielle|
|[RDFvGocLW0OuHmlC.htm](pathfinder-bestiary/RDFvGocLW0OuHmlC.htm)|Wendigo|Wendigo|officielle|
|[RizAUQHtPy0YRUyt.htm](pathfinder-bestiary/RizAUQHtPy0YRUyt.htm)|Soulbound Doll (Lawful Evil)|Poupée des âmes (Loyal mauvais)|officielle|
|[rJgUpdXBCMBrsJFf.htm](pathfinder-bestiary/rJgUpdXBCMBrsJFf.htm)|Soulbound Doll (Lawful Neutral)|Poupée des âmes (Loyal neutre)|officielle|
|[S35DifoycBwkxaGq.htm](pathfinder-bestiary/S35DifoycBwkxaGq.htm)|Demilich|Demi-liche|libre|
|[S8iX8sPaYFFYDoUq.htm](pathfinder-bestiary/S8iX8sPaYFFYDoUq.htm)|Bunyip|Bunyip|officielle|
|[saEPHCN5lDiylb5H.htm](pathfinder-bestiary/saEPHCN5lDiylb5H.htm)|Black Pudding|Pouding noir (Vase)|officielle|
|[skwkJmgz7mqIcStF.htm](pathfinder-bestiary/skwkJmgz7mqIcStF.htm)|Ettin|Ettin|officielle|
|[skz8n8N9GEQY2c90.htm](pathfinder-bestiary/skz8n8N9GEQY2c90.htm)|Clay Golem|Golem d'argile|officielle|
|[sNFBOw4z9KagWUHe.htm](pathfinder-bestiary/sNFBOw4z9KagWUHe.htm)|Flesh Golem|Golem de chair|officielle|
|[sUGKrcx7kbHkWnwG.htm](pathfinder-bestiary/sUGKrcx7kbHkWnwG.htm)|Red Dragon (Young)|Dragon rouge (Jeune, Chromatique)|officielle|
|[SUpy8sleRwi2Egsq.htm](pathfinder-bestiary/SUpy8sleRwi2Egsq.htm)|Drakauthix|Drakauthix|officielle|
|[SWOZ0e2IQifxzlgL.htm](pathfinder-bestiary/SWOZ0e2IQifxzlgL.htm)|Silver Dragon (Ancient)|Dragon d'argent (Vénérable, Métallique)|officielle|
|[t4bHhZBBDUyVzEmf.htm](pathfinder-bestiary/t4bHhZBBDUyVzEmf.htm)|Iron Golem|Golem de fer|officielle|
|[t6Xh4fHdHi2GP87z.htm](pathfinder-bestiary/t6Xh4fHdHi2GP87z.htm)|Annis Hag|Guenaude annis|officielle|
|[tl1aGbwuPPUDKHbN.htm](pathfinder-bestiary/tl1aGbwuPPUDKHbN.htm)|Silver Dragon (Adult, Spellcaster)|Dragon d'argent (Adulte, Incantateur)|officielle|
|[tMxtnGthVV01wNQb.htm](pathfinder-bestiary/tMxtnGthVV01wNQb.htm)|Janni|Jann (Génie)|officielle|
|[Tpuqwt6Af29EMtqX.htm](pathfinder-bestiary/Tpuqwt6Af29EMtqX.htm)|Alchemical Golem|Golem alchimique|officielle|
|[tuIqEvsct8EO33xs.htm](pathfinder-bestiary/tuIqEvsct8EO33xs.htm)|Alghollthu Master|Maître alghollthu|officielle|
|[TZcDdN5o7s4alZNE.htm](pathfinder-bestiary/TZcDdN5o7s4alZNE.htm)|Gelatinous Cube|Cube gélatineux (Vase)|officielle|
|[uA0cv4OT5mQnym0V.htm](pathfinder-bestiary/uA0cv4OT5mQnym0V.htm)|Brass Dragon (Young)|Dragon d'airain (Jeune, Métallique)|officielle|
|[Upf2fOZ6QgGG3seI.htm](pathfinder-bestiary/Upf2fOZ6QgGG3seI.htm)|Stone Golem|Golem de pierre|officielle|
|[uPxh3VpA80zZdWfx.htm](pathfinder-bestiary/uPxh3VpA80zZdWfx.htm)|Blue Dragon (Ancient)|Dragon bleu (Vénérable, Chromatique)|officielle|
|[uQBOJfjtzEXvpLQz.htm](pathfinder-bestiary/uQBOJfjtzEXvpLQz.htm)|Ochre Jelly|Gelée ocre (Vase)|officielle|
|[uQOTCiHslczvrNpt.htm](pathfinder-bestiary/uQOTCiHslczvrNpt.htm)|Bronze Dragon (Young)|Dragon de bronze (Jeune, Métallique)|officielle|
|[UrviURGu5o9LkhxN.htm](pathfinder-bestiary/UrviURGu5o9LkhxN.htm)|Sea Devil Brute|Brute diable des mers|officielle|
|[v0GZ6zDWhD1Y3xs9.htm](pathfinder-bestiary/v0GZ6zDWhD1Y3xs9.htm)|Black Dragon (Young)|Dragon noir (Jeune, Chromatique)|officielle|
|[v9eQFqibX6EYsmuX.htm](pathfinder-bestiary/v9eQFqibX6EYsmuX.htm)|Silver Dragon (Adult)|Dragon d'argent (Adulte, Métallique)|officielle|
|[VBXjZ1jYxdKf64B7.htm](pathfinder-bestiary/VBXjZ1jYxdKf64B7.htm)|Drow Rogue|Roublard drow|officielle|
|[VC7rtYR4hLxwg7WZ.htm](pathfinder-bestiary/VC7rtYR4hLxwg7WZ.htm)|Ghast|Blême|officielle|
|[VEnjCj4geJBzTgBJ.htm](pathfinder-bestiary/VEnjCj4geJBzTgBJ.htm)|Gold Dragon (Ancient, Spellcaster)|Dragon d'or (Vénérable, Incantateur)|officielle|
|[VUJrPHKOjYkIQnWn.htm](pathfinder-bestiary/VUJrPHKOjYkIQnWn.htm)|Mu Spore|Spore de mu|officielle|
|[WQy7HBUcgDLsfVJd.htm](pathfinder-bestiary/WQy7HBUcgDLsfVJd.htm)|Night Hag|Guenaude de la nuit|libre|
|[X03lxGz9ELv8rG2w.htm](pathfinder-bestiary/X03lxGz9ELv8rG2w.htm)|Soulbound Doll (Chaotic Neutral)|Poupée d'âme (Chaotique neutre)|officielle|
|[X03vq2RWi2jiA6Ri.htm](pathfinder-bestiary/X03vq2RWi2jiA6Ri.htm)|Owlbear|Hibours|officielle|
|[x23aXeWTo026pMui.htm](pathfinder-bestiary/x23aXeWTo026pMui.htm)|Earth Mephit|Méphite de la terre (élémentaire)|officielle|
|[x26dyZZZRvZpzK2X.htm](pathfinder-bestiary/x26dyZZZRvZpzK2X.htm)|Quelaunt|Quelaunt|officielle|
|[x4mlZseBP5bWcy4H.htm](pathfinder-bestiary/x4mlZseBP5bWcy4H.htm)|Barghest|Barghest|officielle|
|[xqnl2eFzy5H2NZoQ.htm](pathfinder-bestiary/xqnl2eFzy5H2NZoQ.htm)|Shaitan|Shaitan (Génie)|libre|
|[xZQIChzzIYbNIEoT.htm](pathfinder-bestiary/xZQIChzzIYbNIEoT.htm)|Green Dragon (Young)|Dragon vert (Jeune, Chromatique)|officielle|
|[YPLZkAlOuaj3F3nZ.htm](pathfinder-bestiary/YPLZkAlOuaj3F3nZ.htm)|Silver Dragon (Young)|Dragon d'argent (Jeune, Métallique)|officielle|
|[yq1BgQi0BsAMbU69.htm](pathfinder-bestiary/yq1BgQi0BsAMbU69.htm)|Kapoacinth|Kapoacinth|officielle|
|[Z7xWkQKCHGyd02B1.htm](pathfinder-bestiary/Z7xWkQKCHGyd02B1.htm)|Giant Mantis|Mante géante|officielle|
|[Zbvh6ChdD0TWv257.htm](pathfinder-bestiary/Zbvh6ChdD0TWv257.htm)|Remorhaz|Rémorhaz|officielle|
|[ZMr28tFTA5NUcBTi.htm](pathfinder-bestiary/ZMr28tFTA5NUcBTi.htm)|Web Lurker|Rôdeur des toiles|officielle|
|[zq18QX6CBJNeUIgG.htm](pathfinder-bestiary/zq18QX6CBJNeUIgG.htm)|Gug|Gug|officielle|
|[ZSQ4M8qeAv4dkLq1.htm](pathfinder-bestiary/ZSQ4M8qeAv4dkLq1.htm)|Skum|Skum (Alghollthu)|officielle|
|[zUvgAbgeQH5t6DWs.htm](pathfinder-bestiary/zUvgAbgeQH5t6DWs.htm)|Choral|Choeur (Ange)|officielle|
|[ZXbr1ke1vF0ZFKY3.htm](pathfinder-bestiary/ZXbr1ke1vF0ZFKY3.htm)|Roper|Enlaceur|officielle|
|[zXi6UhZ0mXWpKk4A.htm](pathfinder-bestiary/zXi6UhZ0mXWpKk4A.htm)|Green Dragon (Adult)|Dragon vert (Adulte, Chromatique)|officielle|
