# État de la traduction (pfs-season-3-bestiary)

 * **libre**: 278


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[06XCiuS7ZDEM5Xpf.htm](pfs-season-3-bestiary/06XCiuS7ZDEM5Xpf.htm)|Filth Fire (PFS 3-01)|Feu d'immondices (PFS 3-01)|libre|
|[0T4gSWRTGUN0aFzT.htm](pfs-season-3-bestiary/0T4gSWRTGUN0aFzT.htm)|Natron Explosion|Explosion de natron|libre|
|[1NWTUcGckuJ2qC5I.htm](pfs-season-3-bestiary/1NWTUcGckuJ2qC5I.htm)|Overloading Arch (5-6)|Arche surchargée (5-6)|libre|
|[1OHuJmY24AnfJHpz.htm](pfs-season-3-bestiary/1OHuJmY24AnfJHpz.htm)|Bergworm|Ver de glace|libre|
|[1PE6CVTY6BBH9k7t.htm](pfs-season-3-bestiary/1PE6CVTY6BBH9k7t.htm)|Elite Hadrosaurid (PFS 3-99)|Hadrosaure élite (PFS 3-99)|libre|
|[2jiOLCpHekITyQTQ.htm](pfs-season-3-bestiary/2jiOLCpHekITyQTQ.htm)|Temporal Echoes (5-6)|Échos du temps (5-6)|libre|
|[2N2Nj4W9kWn2fU6P.htm](pfs-season-3-bestiary/2N2Nj4W9kWn2fU6P.htm)|Animated Knickknack (1-2)|Babiole animée (1-2)|libre|
|[2wV0Yv8uQpmRs2E5.htm](pfs-season-3-bestiary/2wV0Yv8uQpmRs2E5.htm)|Smoldering Nightmare|Destrier incandescent|libre|
|[39ShWwOPdJq2X5o2.htm](pfs-season-3-bestiary/39ShWwOPdJq2X5o2.htm)|Aydrian Thrune (3-4)|Aydrian Thrune (3-4)|libre|
|[3bijukcKD8MfoHA9.htm](pfs-season-3-bestiary/3bijukcKD8MfoHA9.htm)|Forged Bones|Os forgé|libre|
|[3eowuKJaQ99K8Ofp.htm](pfs-season-3-bestiary/3eowuKJaQ99K8Ofp.htm)|Ghast (Heretical)|Blême (hérétique)|libre|
|[3r3etd3bUOn58TH3.htm](pfs-season-3-bestiary/3r3etd3bUOn58TH3.htm)|Advanced Eltha Embercall (9-10)/Eltha Embercall (11-12)|Eltha Embercall (9-10)/Eltha Embercall (11-12) amélioré|libre|
|[3TvWKI01CMwxex51.htm](pfs-season-3-bestiary/3TvWKI01CMwxex51.htm)|Cloud of Reverie|Nuage de rêverie|libre|
|[43L2RtbBprItSjPY.htm](pfs-season-3-bestiary/43L2RtbBprItSjPY.htm)|Pontrius Tilasti (3-4)|Pontrius Tilasti (3-4)|libre|
|[4f96tNhBeuyTR5jO.htm](pfs-season-3-bestiary/4f96tNhBeuyTR5jO.htm)|Cleansing Torrent|Torrent purificateur|libre|
|[4fhQXAfVGQ0aX8LA.htm](pfs-season-3-bestiary/4fhQXAfVGQ0aX8LA.htm)|Manipulator's Metronome (1-2)|Métronome manipulateur (1-2)|libre|
|[4paPYhdN4HHT3Orz.htm](pfs-season-3-bestiary/4paPYhdN4HHT3Orz.htm)|Flaming Animated Armor (Holy Hand) (1-2)|Armure animée enflammée (Main Sainte) (1-2)|libre|
|[4S77ftUx1zp7JscD.htm](pfs-season-3-bestiary/4S77ftUx1zp7JscD.htm)|Bloody Skeletal Champion|Champion squelette ensanglanté|libre|
|[4XlfENtBfF9GB2fy.htm](pfs-season-3-bestiary/4XlfENtBfF9GB2fy.htm)|Ninth Army Burglar|Cambrioleur de la neuvième armée|libre|
|[538nxlPKC93GXzZh.htm](pfs-season-3-bestiary/538nxlPKC93GXzZh.htm)|Mud Pot|Creuset de boue|libre|
|[55uIx8vScaf8JeTY.htm](pfs-season-3-bestiary/55uIx8vScaf8JeTY.htm)|Junior (3-4)|Junior (3-4)|libre|
|[5vtvRmXRvQAvG39l.htm](pfs-season-3-bestiary/5vtvRmXRvQAvG39l.htm)|Weeping Carved Mourner|Bête scupltée en pleurs|libre|
|[5YBQ8KPyEaKnR5eX.htm](pfs-season-3-bestiary/5YBQ8KPyEaKnR5eX.htm)|Bloodletting Scythe Blade|Lames de faux de saignement|libre|
|[6aGKyGMSHD4fqReA.htm](pfs-season-3-bestiary/6aGKyGMSHD4fqReA.htm)|Weretiger (PFS 3-13)|Tigre-garou (PFS 3-13)|libre|
|[83lyaQloNlS16sJu.htm](pfs-season-3-bestiary/83lyaQloNlS16sJu.htm)|Mature Arabuk|Arabuk adulte|libre|
|[8CEKF7wq9qZzmveH.htm](pfs-season-3-bestiary/8CEKF7wq9qZzmveH.htm)|Sparkles (9-10)|Sparkles (9-10)|libre|
|[8p1RAJIqtx9JhHqO.htm](pfs-season-3-bestiary/8p1RAJIqtx9JhHqO.htm)|Grieving Carved Mourner|Bête sculptée en deuil|libre|
|[8sCLuGyghlIvMoBo.htm](pfs-season-3-bestiary/8sCLuGyghlIvMoBo.htm)|Languid Purple Worm|Ver violet alangui|libre|
|[8Sea3unOcDH6dWJI.htm](pfs-season-3-bestiary/8Sea3unOcDH6dWJI.htm)|Bergworm Tyrant|Ver de glace tyrannique|libre|
|[8tDDIvcinSurN8BF.htm](pfs-season-3-bestiary/8tDDIvcinSurN8BF.htm)|Marcien Blakros (9-10)|Marcien Blakros (9-10)|libre|
|[92qhrLmutMVqwQlj.htm](pfs-season-3-bestiary/92qhrLmutMVqwQlj.htm)|Trip Wire|Fil d'acier|libre|
|[93b4CDDzR9xmHss4.htm](pfs-season-3-bestiary/93b4CDDzR9xmHss4.htm)|Muesello's Summoning Rune|Rune de convocation de Muesello|libre|
|[986YKe07u2538RcF.htm](pfs-season-3-bestiary/986YKe07u2538RcF.htm)|Fasiel Ibn Sazadin (3-4)|Fasiel Ibn Sazadin (3-4)|libre|
|[98iFvMVq4MvrwvK3.htm](pfs-season-3-bestiary/98iFvMVq4MvrwvK3.htm)|Eternal Hall (1-2)|Couloir d'éternité (1-2)|libre|
|[9auIrnhXBCHnO5Ww.htm](pfs-season-3-bestiary/9auIrnhXBCHnO5Ww.htm)|Shrouded Bloodseeker|Cherchesang voilé|libre|
|[9FB0dIfsZSk6bdKt.htm](pfs-season-3-bestiary/9FB0dIfsZSk6bdKt.htm)|Animated Cookware|Batterie de cuisine animée|libre|
|[9FgyBdbHxwMYdGAt.htm](pfs-season-3-bestiary/9FgyBdbHxwMYdGAt.htm)|Pup of Tindalos|Chiot de Tindalos|libre|
|[9RI0biNpRaZBU1de.htm](pfs-season-3-bestiary/9RI0biNpRaZBU1de.htm)|Flaming Animated Armor (Ashgrip) (1-2)|Armure animée enflammée (Ashgrip) (1-2)|libre|
|[AaCmgrEQ4sPakXca.htm](pfs-season-3-bestiary/AaCmgrEQ4sPakXca.htm)|Caliclotherax (11-12)|Caliclothérax (11-12)|libre|
|[AffE7EuKLFijoJzG.htm](pfs-season-3-bestiary/AffE7EuKLFijoJzG.htm)|Ninth Army Operative|Agent de la Neuvième armée|libre|
|[aICR8DJ80nD1rOM0.htm](pfs-season-3-bestiary/aICR8DJ80nD1rOM0.htm)|Living Landslide (PFS 3-11)|Éboulement vivant (PFS 3-11)|libre|
|[AIO9Hug8KBesUfxI.htm](pfs-season-3-bestiary/AIO9Hug8KBesUfxI.htm)|Bloody Beetle Exoskeleton|Exosquelette de timarque crache-sang|libre|
|[ALH2skuaXvEy6NHW.htm](pfs-season-3-bestiary/ALH2skuaXvEy6NHW.htm)|Revinus's Guards (3-4)|Garde de Révinus (3-4)|libre|
|[ATBFj4El0uLXDrjb.htm](pfs-season-3-bestiary/ATBFj4El0uLXDrjb.htm)|Cinder Rat (Leadroar)|Rat des braises (Leadroar)|libre|
|[AuU3BGEz05Q180yI.htm](pfs-season-3-bestiary/AuU3BGEz05Q180yI.htm)|Tascio Raetullus (3-4)|Tascio Raétullus (3-4)|libre|
|[ayfX6eOrfR8695ev.htm](pfs-season-3-bestiary/ayfX6eOrfR8695ev.htm)|Cooking Catastrophe|Catastrophe en cuisine|libre|
|[aZl2962bnq1j6Hpy.htm](pfs-season-3-bestiary/aZl2962bnq1j6Hpy.htm)|Vengeful Ember Fox|Renard des braises vengeur|libre|
|[B57HMAtx8pj3kt8u.htm](pfs-season-3-bestiary/B57HMAtx8pj3kt8u.htm)|Fipp the Prophet|Fipp le prophète|libre|
|[b5CAFC9BgfOCm7NM.htm](pfs-season-3-bestiary/b5CAFC9BgfOCm7NM.htm)|Muesello's Best Summoning Rune|Rune de convocation améliorée de Muesello|libre|
|[BcSac9voqpoDjMTy.htm](pfs-season-3-bestiary/BcSac9voqpoDjMTy.htm)|Thundering Wihsaak (Heretical)|Wihsaak tonitruant (Hérétique)|libre|
|[bgwjxU9MmKJtcxx5.htm](pfs-season-3-bestiary/bgwjxU9MmKJtcxx5.htm)|Old Drakauthix (PFS 3-15)|Vieux Drakauthix (PFS 3-15)|libre|
|[bn4ghzDcq3XncBwS.htm](pfs-season-3-bestiary/bn4ghzDcq3XncBwS.htm)|Cleansing Cascade|Cascade purificatrice|libre|
|[Bpprw07yfbxqLdBp.htm](pfs-season-3-bestiary/Bpprw07yfbxqLdBp.htm)|Poltergeist (Holy Hand)|Poltergeist (Main Sainte)|libre|
|[BQOgvcGbGxFSQQ0T.htm](pfs-season-3-bestiary/BQOgvcGbGxFSQQ0T.htm)|Leopard (PFS 3-13)|Léopard (PFS 3-13)|libre|
|[BuLgk7s7lPfn1HUS.htm](pfs-season-3-bestiary/BuLgk7s7lPfn1HUS.htm)|Weakened Cobbleswarm|Nuée de teignes-pavé affaiblies|libre|
|[bW7iJLqP4AOvkMeu.htm](pfs-season-3-bestiary/bW7iJLqP4AOvkMeu.htm)|Veteran Onyx Alliance Enforcer|Exécuteur vétéran de l'Alliance Onyx|libre|
|[by3YpQPT2mWc0kFE.htm](pfs-season-3-bestiary/by3YpQPT2mWc0kFE.htm)|Tadrun (3-4)|Tadrun (3-4)|libre|
|[byhh6wysf9MdM7LM.htm](pfs-season-3-bestiary/byhh6wysf9MdM7LM.htm)|Ninth Army Guard|Garde de la Neuvième armée|libre|
|[Ce5z95Z1twvMCsw9.htm](pfs-season-3-bestiary/Ce5z95Z1twvMCsw9.htm)|Arabuk Yearling|Arabuk d'un an|libre|
|[ChuQ39tv0plbsfVJ.htm](pfs-season-3-bestiary/ChuQ39tv0plbsfVJ.htm)|Smilglen Daet|Smilglen Daet|libre|
|[CIjBUkWnth0CE30k.htm](pfs-season-3-bestiary/CIjBUkWnth0CE30k.htm)|Cog the Ruffian|Cog le voyou|libre|
|[cm09iVg6JGpCu3Rh.htm](pfs-season-3-bestiary/cm09iVg6JGpCu3Rh.htm)|Fire Sentry|Sentinelle de feu|libre|
|[CmHz3bQDj6FxEecW.htm](pfs-season-3-bestiary/CmHz3bQDj6FxEecW.htm)|Deadly Fetchling Sneak|Fetchelin furtif mortel|libre|
|[cYovLvzfbqkTF9uF.htm](pfs-season-3-bestiary/cYovLvzfbqkTF9uF.htm)|Cobbled Bruiser|Teigne-pavé cogneur|libre|
|[d0kPtsi2DH3V9WJu.htm](pfs-season-3-bestiary/d0kPtsi2DH3V9WJu.htm)|Shadow (PFS 3-17)|Ombre (PFS 3-17)|libre|
|[dCljcVK6RAc0Y8Z7.htm](pfs-season-3-bestiary/dCljcVK6RAc0Y8Z7.htm)|Wasp Swarm (PFS 3-13)|Nuée de guêpes (PFS 3-13)|libre|
|[DEfeDmBh7lR7c9W8.htm](pfs-season-3-bestiary/DEfeDmBh7lR7c9W8.htm)|Animated Full Plate (PFS 3-99)|Harnois animé (PFS 3-99)|libre|
|[df7a1D5Zbv1yZIYl.htm](pfs-season-3-bestiary/df7a1D5Zbv1yZIYl.htm)|Spinning Juice Fountain|Fontaine de jus tournoyant|libre|
|[dgwmJ8A8sROXAGgf.htm](pfs-season-3-bestiary/dgwmJ8A8sROXAGgf.htm)|Hellbound Jailer (5-6)|Geôlier damné (5-6)|libre|
|[dj61OwkpKzv9ctsN.htm](pfs-season-3-bestiary/dj61OwkpKzv9ctsN.htm)|Nuglub (PFS 3-18)|Nuglub (PFS 3-18)|libre|
|[DOG6jdFk7JmBDEBg.htm](pfs-season-3-bestiary/DOG6jdFk7JmBDEBg.htm)|Fast Shambler Troop|Troupe de titubeurs rapide|libre|
|[DTBa5iNcHztFu7oo.htm](pfs-season-3-bestiary/DTBa5iNcHztFu7oo.htm)|Deep Shadow Guardian|Gardien des ombres profondes|libre|
|[dYcDARPuNmLlGNuG.htm](pfs-season-3-bestiary/dYcDARPuNmLlGNuG.htm)|Flaming Animated Armor (Firecutter) (1-2)|Armure animée enflammée (Firecutter) (1-2)|libre|
|[DZwKXXrlzmk5Hqyx.htm](pfs-season-3-bestiary/DZwKXXrlzmk5Hqyx.htm)|Cairn Wight (PFS 3-09)|Nécrophage des tertres (PFS3-09)|libre|
|[e9kWyI8plxVuMOe3.htm](pfs-season-3-bestiary/e9kWyI8plxVuMOe3.htm)|Deep Mud|Boue profonde|libre|
|[EB00f6ADElWInuix.htm](pfs-season-3-bestiary/EB00f6ADElWInuix.htm)|Shobhad Hunter (7-8)|Chasseur Shobhad (7-8)|libre|
|[EiCWpQOwOlGnvWB8.htm](pfs-season-3-bestiary/EiCWpQOwOlGnvWB8.htm)|Explosive Rat|Rat explosif|libre|
|[eiWWhTeR8QTLKptH.htm](pfs-season-3-bestiary/eiWWhTeR8QTLKptH.htm)|Lesser Animate Dream|Rêve animé inférieur|libre|
|[El0QINB4nVKeiI5A.htm](pfs-season-3-bestiary/El0QINB4nVKeiI5A.htm)|Giant Amoeba (PFS 3-98)|Amibe géante (PFS 3-98)|libre|
|[eVVAitAkOGCPzhXJ.htm](pfs-season-3-bestiary/eVVAitAkOGCPzhXJ.htm)|Aydrian Thrune (5-6)|Aydrian Thrune (5-6)|libre|
|[ExDYM8AqQVVDKEKM.htm](pfs-season-3-bestiary/ExDYM8AqQVVDKEKM.htm)|Anguished Tombstone Door|Porte tombale torturée|libre|
|[Eyf5gaIlI4X2HCos.htm](pfs-season-3-bestiary/Eyf5gaIlI4X2HCos.htm)|Crushing Spirits (5-6)|Esprits accablants (5-6)|libre|
|[FfhpuXIxjhWGVnv2.htm](pfs-season-3-bestiary/FfhpuXIxjhWGVnv2.htm)|Ninth Army Soldier|Soldat de la Neuvième armée|libre|
|[ffzfTw9KfaJopkCx.htm](pfs-season-3-bestiary/ffzfTw9KfaJopkCx.htm)|Peryton (Heretical)|Péryton (Hérétique)|libre|
|[FHsq4Yw458ogI2RQ.htm](pfs-season-3-bestiary/FHsq4Yw458ogI2RQ.htm)|Angry Void Boils|Ébulition du vide contrarié|libre|
|[fiArvThMW2QanZGk.htm](pfs-season-3-bestiary/fiArvThMW2QanZGk.htm)|Raseri Kanton Skeleton|Squelette de Raséri Kanton|libre|
|[fiwIeiExpZ1CQKAj.htm](pfs-season-3-bestiary/fiwIeiExpZ1CQKAj.htm)|Glurorchaes (7-8)|Glurorchaes (7-8)|libre|
|[FjRztl9Aro21lXj6.htm](pfs-season-3-bestiary/FjRztl9Aro21lXj6.htm)|Lacerating Trip Wire|Fil d'acier lacérant|libre|
|[FmZ8bz1ItIzxuu1D.htm](pfs-season-3-bestiary/FmZ8bz1ItIzxuu1D.htm)|Iron Ring Bosun|Bosco du cercle de fer|libre|
|[fRIyhOmNon1GtQeT.htm](pfs-season-3-bestiary/fRIyhOmNon1GtQeT.htm)|Mutated Brown Mold|Moisissure brune mutante|libre|
|[FyQji78S25WiZMct.htm](pfs-season-3-bestiary/FyQji78S25WiZMct.htm)|Khisisi (7-8)|Khisisi (7-8)|libre|
|[g3UYRJQCwnjdvCm9.htm](pfs-season-3-bestiary/g3UYRJQCwnjdvCm9.htm)|Tyrannical Interrogator|Interrogateur tyrannique|libre|
|[g4hfpuGdtTDq7KZw.htm](pfs-season-3-bestiary/g4hfpuGdtTDq7KZw.htm)|Tascio Raetullus (5-6)|Tascio Raétullus (5-6)|libre|
|[gehsgt6uN6sUsmDW.htm](pfs-season-3-bestiary/gehsgt6uN6sUsmDW.htm)|Revinus (3-4)|Révinus (3-4)|libre|
|[GKF2zLJ0VLy1IBML.htm](pfs-season-3-bestiary/GKF2zLJ0VLy1IBML.htm)|Aspis Oppressor|Oppresseur de l'Aspis|libre|
|[gMWqywO74q6evs8U.htm](pfs-season-3-bestiary/gMWqywO74q6evs8U.htm)|Tilting Floor|Sol pivotant|libre|
|[GODf6SocqJJOktWY.htm](pfs-season-3-bestiary/GODf6SocqJJOktWY.htm)|Unstable Column (1-2)|Colonne instable (1-2)|libre|
|[gR4cLkgcor3Lp35F.htm](pfs-season-3-bestiary/gR4cLkgcor3Lp35F.htm)|Bugbear Tormentor (PFS 3-13)|Gobelours tourmenteur (PFS 3-13)|libre|
|[Gt2NqeZldkUf4vHI.htm](pfs-season-3-bestiary/Gt2NqeZldkUf4vHI.htm)|Aspis Enforcer|Exécuteur de l'Aspis|libre|
|[gtcISL1zMhQ0mv5g.htm](pfs-season-3-bestiary/gtcISL1zMhQ0mv5g.htm)|Arisept (7-8)|Arisept (7-8)|libre|
|[gTJsV3w2gknQJXiH.htm](pfs-season-3-bestiary/gTJsV3w2gknQJXiH.htm)|Smiglen Daet|Smilglen Daet|libre|
|[gwy6v9P8E8hIlP19.htm](pfs-season-3-bestiary/gwy6v9P8E8hIlP19.htm)|Zebub (PFS)|Zébub (PFS)|libre|
|[gzMCaAr2Bd2gbhrF.htm](pfs-season-3-bestiary/gzMCaAr2Bd2gbhrF.htm)|Thundering Wihsaak (Arctic)|Wihsaak tonitruant (Arctique)|libre|
|[h2vnwOGmGFcKVGYq.htm](pfs-season-3-bestiary/h2vnwOGmGFcKVGYq.htm)|Iron Ring Pirate|Pirate du Cercle de fer|libre|
|[h65eZJq6Iie69zLD.htm](pfs-season-3-bestiary/h65eZJq6Iie69zLD.htm)|Bloody Barber Ambusher|Piègeur des Barbiers sanglants|libre|
|[HB1bcZ5gU5tjGoIF.htm](pfs-season-3-bestiary/HB1bcZ5gU5tjGoIF.htm)|Sliding Floor|Sol glissant|libre|
|[hEh3j8Fz8lbjuiRF.htm](pfs-season-3-bestiary/hEh3j8Fz8lbjuiRF.htm)|Aspis Recruit|Stagiaire de l'Aspis|libre|
|[Hez7S4Mhvi816fHi.htm](pfs-season-3-bestiary/Hez7S4Mhvi816fHi.htm)|Unstable Skeletal Champion|Champion squelette instable|libre|
|[hGu2JDWOAHHMLYeH.htm](pfs-season-3-bestiary/hGu2JDWOAHHMLYeH.htm)|Thundering Wihsaak (Rifle Mutation)|Wihsaak tonitruant (Mutation fusil)|libre|
|[hINxfZgkvp21ePQ2.htm](pfs-season-3-bestiary/hINxfZgkvp21ePQ2.htm)|Eternal Hall (3-4)|Couloir d'éternité (3-4)|libre|
|[HIPqiHMAfN2mmAlg.htm](pfs-season-3-bestiary/HIPqiHMAfN2mmAlg.htm)|Cobbled Brutalizer|Teigne-pavé brutal|libre|
|[hjdO3H4Kh3iZbRAD.htm](pfs-season-3-bestiary/hjdO3H4Kh3iZbRAD.htm)|Firespark Trap (3-4)|Piège étincelles (3-4)|libre|
|[HOQG0wl3ZtqS2j4v.htm](pfs-season-3-bestiary/HOQG0wl3ZtqS2j4v.htm)|Weak Hell Hound (PFS 3-07)|Molosse infernal affaibli (PFS 3-07)|libre|
|[hqFuN3UINx7ChbqS.htm](pfs-season-3-bestiary/hqFuN3UINx7ChbqS.htm)|Aspis Striker|Agresseur de l'Aspis|libre|
|[hRVg3dgSjOrLxgLs.htm](pfs-season-3-bestiary/hRVg3dgSjOrLxgLs.htm)|Severing Trip Wire|Fil d'acier handicappant|libre|
|[HtrdSWlJzDf7dMOO.htm](pfs-season-3-bestiary/HtrdSWlJzDf7dMOO.htm)|Hellbound Robber|Voleur damné|libre|
|[HvrXHDTyttxDYvpL.htm](pfs-season-3-bestiary/HvrXHDTyttxDYvpL.htm)|Thomil Bolyrius (5-6)|Thomil Bolyrius (5-6)|libre|
|[hYqXZ2glAj0mVfCx.htm](pfs-season-3-bestiary/hYqXZ2glAj0mVfCx.htm)|Temporal Echoes (1-2)|Échos du temps (1-2)|libre|
|[i9cCVnI5OlMe8d2u.htm](pfs-season-3-bestiary/i9cCVnI5OlMe8d2u.htm)|Displaced Robot (1-2)|Robot déplacé (1-2)|libre|
|[iev4EOtchd0hp9sh.htm](pfs-season-3-bestiary/iev4EOtchd0hp9sh.htm)|Reckless Akitonian Scientist|Savant fou Akitonian|libre|
|[IHuTNi8FQt54OpPj.htm](pfs-season-3-bestiary/IHuTNi8FQt54OpPj.htm)|Rampaging Roast (Medium Rare)|Rôti déchaîné (Saignant)|libre|
|[iibGqYRM92mGVV8h.htm](pfs-season-3-bestiary/iibGqYRM92mGVV8h.htm)|Cog the Torchbearer|Cog Porteur de flambeau|libre|
|[iL7qWBOdmhRwjcVG.htm](pfs-season-3-bestiary/iL7qWBOdmhRwjcVG.htm)|Panicked Bat Swarm|Nuée de chauves-souris paniquée|libre|
|[IP4wercLl66JDCaG.htm](pfs-season-3-bestiary/IP4wercLl66JDCaG.htm)|Bloody Barber Leader|Chef des Barbiers sanglants|libre|
|[ips7faBeWNAqpByM.htm](pfs-season-3-bestiary/ips7faBeWNAqpByM.htm)|Iron Ring Deckhand|Débardeur de l'Anneau de fer|libre|
|[ISBNLUY6Ilu0Jl28.htm](pfs-season-3-bestiary/ISBNLUY6Ilu0Jl28.htm)|Eloko (PFS 3-03)|Éloko (PFS 3-03)|libre|
|[j78lsOcAHxTNQQRE.htm](pfs-season-3-bestiary/j78lsOcAHxTNQQRE.htm)|Tombstone Door|Porte tombale|libre|
|[jaMihaRbFNvKHVUc.htm](pfs-season-3-bestiary/jaMihaRbFNvKHVUc.htm)|Grimstalker (PFS 3-13)|Rôdeur sombre (PFS 3-13)|libre|
|[jItk6GNZ7lmT5zVY.htm](pfs-season-3-bestiary/jItk6GNZ7lmT5zVY.htm)|Hellbound Jailer (3-4)|Geôlier damné (3-4)|libre|
|[jmPtnZwZq7YugkFe.htm](pfs-season-3-bestiary/jmPtnZwZq7YugkFe.htm)|Precarious Sliding Floor|Sol glissant précaire|libre|
|[JmQiEhCnr9WcG6Dl.htm](pfs-season-3-bestiary/JmQiEhCnr9WcG6Dl.htm)|Temporal Echoes (7-8)|Échos du temps (7-8)|libre|
|[jMxFlkAbepA47sHb.htm](pfs-season-3-bestiary/jMxFlkAbepA47sHb.htm)|Scalding Mephit|Méphite bouillant|libre|
|[JteHqFsY5wFDnrcF.htm](pfs-season-3-bestiary/JteHqFsY5wFDnrcF.htm)|Volatile Tidal Controls|Contrôle des marée volatile|libre|
|[JXWHVfaSm3eHIb5v.htm](pfs-season-3-bestiary/JXWHVfaSm3eHIb5v.htm)|Onyx Alliance Commander|Commandant de l'Alliance Onyx|libre|
|[JZtLQye4QlfFjrYT.htm](pfs-season-3-bestiary/JZtLQye4QlfFjrYT.htm)|Wasp Swarm (Heretical)|Nuée de guêpes (Hérétique)|libre|
|[kBYuJs4je7XBPisk.htm](pfs-season-3-bestiary/kBYuJs4je7XBPisk.htm)|Foot Pincer|Pince-pied|libre|
|[KSKDACF4qh3HJTDo.htm](pfs-season-3-bestiary/KSKDACF4qh3HJTDo.htm)|Vaggas the Fanatic|Vaggas le fanatique|libre|
|[kTsUEbM3JkhT5VZM.htm](pfs-season-3-bestiary/kTsUEbM3JkhT5VZM.htm)|Sandswept Statue|Statue balayée par le vent|libre|
|[l1EWODWiDlDLQx0L.htm](pfs-season-3-bestiary/l1EWODWiDlDLQx0L.htm)|Kobold Scout (PFS 3-99)|Éclaireur kobold (PFS 3-99)|libre|
|[L1gzOT7o2xxJoqUD.htm](pfs-season-3-bestiary/L1gzOT7o2xxJoqUD.htm)|Timewarped Amalgam|Amalgame distordu par le temps|libre|
|[L2BPCPQ4fRMviF2d.htm](pfs-season-3-bestiary/L2BPCPQ4fRMviF2d.htm)|Shadow Double Runes (3-4)|Double rune de l'ombre (3-4)|libre|
|[LG2pZMbLXZKtcWZh.htm](pfs-season-3-bestiary/LG2pZMbLXZKtcWZh.htm)|Arisept (9-10)|Arisept (9-10)|libre|
|[LHEI3E4THLsz0rr0.htm](pfs-season-3-bestiary/LHEI3E4THLsz0rr0.htm)|Enduring Zombie Hulk|Mastodonte zombie tenace|libre|
|[LiKplPXYeqLVJ462.htm](pfs-season-3-bestiary/LiKplPXYeqLVJ462.htm)|Insolent's Retort|Riposte aux insolents|libre|
|[Lmo4SH5wpywZL1YS.htm](pfs-season-3-bestiary/Lmo4SH5wpywZL1YS.htm)|Displaced Robot (5-6)|Robot déplacé (5-6)|libre|
|[LqEHXPTgyVUltvS2.htm](pfs-season-3-bestiary/LqEHXPTgyVUltvS2.htm)|Skulk (PFS 3-13)|Skulk (PFS 3-13)|libre|
|[m1bVKRXsSXnBNka5.htm](pfs-season-3-bestiary/m1bVKRXsSXnBNka5.htm)|Elite Hieracosphinx (PFS 3-14)|Hiérocasphinx d'élite (PFS 3-14)|libre|
|[MK2YstG6JpFwI9sB.htm](pfs-season-3-bestiary/MK2YstG6JpFwI9sB.htm)|Duergar Crusher|Écraseur hryngar|libre|
|[mQmcKIgTUn3xMClx.htm](pfs-season-3-bestiary/mQmcKIgTUn3xMClx.htm)|Revinus's Guards (1-2)|Gardes de Révinus (1-2)|libre|
|[MQMfOX5h2OgJrhm6.htm](pfs-season-3-bestiary/MQMfOX5h2OgJrhm6.htm)|Uncle Jeb (3-4)|Oncle Jeb (3-4)|libre|
|[mSkooYG0IgmBAktL.htm](pfs-season-3-bestiary/mSkooYG0IgmBAktL.htm)|Flensing Trap (5-6)|Piège ébarbeur (5-6)|libre|
|[mYpAGYbt6cGiTU8p.htm](pfs-season-3-bestiary/mYpAGYbt6cGiTU8p.htm)|Junior (1-2)|Junior (1-2)|libre|
|[mZ2bmxg4JcVh1p2k.htm](pfs-season-3-bestiary/mZ2bmxg4JcVh1p2k.htm)|Rampaging Roast (Well Done)|Rôti déchaîné (À point)|libre|
|[n0Gq9VPAOReN4IJB.htm](pfs-season-3-bestiary/n0Gq9VPAOReN4IJB.htm)|Overloading Arch (1-2)|Arche surchargée (1-2)|libre|
|[nABHOLMNGoOFzsfE.htm](pfs-season-3-bestiary/nABHOLMNGoOFzsfE.htm)|Junk Launcher|Lance-détritus|libre|
|[njJCv6ItwIzwO3Xj.htm](pfs-season-3-bestiary/njJCv6ItwIzwO3Xj.htm)|Ninth Army Fence|Receleur de la Neuvième armée|libre|
|[nPuiF2GEyJ2WMG4v.htm](pfs-season-3-bestiary/nPuiF2GEyJ2WMG4v.htm)|Grimple (PFS 3-18)|Tristefripe (PFS 3-18)|libre|
|[nS4auasfEXEU0rtx.htm](pfs-season-3-bestiary/nS4auasfEXEU0rtx.htm)|Onyx Alliance Scout|Éclaireur de l'Alliance Onyx|libre|
|[oJiVHS6pZlJumvj5.htm](pfs-season-3-bestiary/oJiVHS6pZlJumvj5.htm)|Shadow Spears (7-8)|Lances d'ombres (7-8)|libre|
|[Ol8JpWfZKdJ28K5L.htm](pfs-season-3-bestiary/Ol8JpWfZKdJ28K5L.htm)|Sorrowful Tombstone Door|Porte tombale attristée|libre|
|[olP1l0gi9qYFZtt2.htm](pfs-season-3-bestiary/olP1l0gi9qYFZtt2.htm)|Lightweight Animated Armor|Armure légère animée|libre|
|[OOHNMq2uOfnp8QCg.htm](pfs-season-3-bestiary/OOHNMq2uOfnp8QCg.htm)|Fasiel Ibn Sazadin (1-2)|Fasiel Ibn Sazadin (1-2)|libre|
|[oOl4UOYKpGeS3Ep7.htm](pfs-season-3-bestiary/oOl4UOYKpGeS3Ep7.htm)|Onyx Alliance Enforcer|Exécuteur de l'Alliance Onyx|libre|
|[oqxqAesRlacxYKiY.htm](pfs-season-3-bestiary/oqxqAesRlacxYKiY.htm)|Temporal Echoes (3-4)|Échos du temps (3-4)|libre|
|[oTftuPSL5iB6fhWU.htm](pfs-season-3-bestiary/oTftuPSL5iB6fhWU.htm)|Poisoned Dart Trap (1-2)|Flèchettes empoisonnées (1-2)|libre|
|[ovNZDN5d2yYY8tqP.htm](pfs-season-3-bestiary/ovNZDN5d2yYY8tqP.htm)|Falling Tree|Chute d'arbre|libre|
|[OVsp8tvdybZbbszm.htm](pfs-season-3-bestiary/OVsp8tvdybZbbszm.htm)|Onyx Alliance Officer|Officier de l'Alliance Onyx|libre|
|[p0RvcmcEe8zfoMkT.htm](pfs-season-3-bestiary/p0RvcmcEe8zfoMkT.htm)|Hellbound Rogue|Roublard damné|libre|
|[P0Y86CC9ULe0D4x7.htm](pfs-season-3-bestiary/P0Y86CC9ULe0D4x7.htm)|Debased Relic|Relique dégradée|libre|
|[P51YAmzcq1jONhJR.htm](pfs-season-3-bestiary/P51YAmzcq1jONhJR.htm)|Wailing Ghost|Fantôme hurlant|libre|
|[pBRkhSlkyNznhzP9.htm](pfs-season-3-bestiary/pBRkhSlkyNznhzP9.htm)|Poisoned Dart Trap (3-4)|Flèchettes empoisonnées (3-4)|libre|
|[PcKhzFKtzNvtBfdI.htm](pfs-season-3-bestiary/PcKhzFKtzNvtBfdI.htm)|Manipulator's Metronome (3-4)|Métronome manipulateur (3-4)|libre|
|[pcSXAn8KbvPDWCWU.htm](pfs-season-3-bestiary/pcSXAn8KbvPDWCWU.htm)|Fast-Spinning Juice Fountain|Fontaine de jus tournoyant rapidement|libre|
|[phgXSggQiGRNn4Yr.htm](pfs-season-3-bestiary/phgXSggQiGRNn4Yr.htm)|Ninth Army Bodyguard|Garde du corps de la Neuvième armée|libre|
|[PhnmHglnmoiIb3Al.htm](pfs-season-3-bestiary/PhnmHglnmoiIb3Al.htm)|Flaming Animated Armor (Leadroar) (1-2)|Armure enflammée animée (Leadroar) (1-2)|libre|
|[plYMMhlfO6OVrtEo.htm](pfs-season-3-bestiary/plYMMhlfO6OVrtEo.htm)|Eloise's Ghost (1-2)|Fantôme d'Éloïse (1-2)|libre|
|[pnZ9aFB0mdYdU7Zj.htm](pfs-season-3-bestiary/pnZ9aFB0mdYdU7Zj.htm)|Shadow Double Runes (1-2)|Double rune de l'ombre (1-2)|libre|
|[POFH4BcrEF9wxwp2.htm](pfs-season-3-bestiary/POFH4BcrEF9wxwp2.htm)|Young Arabuk|Jeune arabuk|libre|
|[PqawYWEK0iqvcBZj.htm](pfs-season-3-bestiary/PqawYWEK0iqvcBZj.htm)|Marcien Blakros (7-8) (Shadow Double)|Marcien Blakros (7-8)(Double d'ombre)|libre|
|[psBBFRgVi0NwKSWX.htm](pfs-season-3-bestiary/psBBFRgVi0NwKSWX.htm)|Muesello's Greater Summoning Rune|Rune de convocation supérieure de Muesello|libre|
|[psFZ9UdpJR4rRpEy.htm](pfs-season-3-bestiary/psFZ9UdpJR4rRpEy.htm)|Kobold Warrior (PFS 3-99)|Guerrier Kobold (PFS 3-99)|libre|
|[PXXLxoPdGwtiVvtS.htm](pfs-season-3-bestiary/PXXLxoPdGwtiVvtS.htm)|Eternal Hall (5-6)|Couloir d'éternité (5-6)|libre|
|[pYR3Ovkd9eKQQ5qE.htm](pfs-season-3-bestiary/pYR3Ovkd9eKQQ5qE.htm)|Eltha Embercall (11-12, 28+ CP)|Eltha Embercall (11-12, 28+ CP)|libre|
|[Q2a9EA1zQB8acuiF.htm](pfs-season-3-bestiary/Q2a9EA1zQB8acuiF.htm)|Shobhad Hunter (5-6)|Chasseur shobhad (5-6)|libre|
|[q6J7DcU3am7wKIRD.htm](pfs-season-3-bestiary/q6J7DcU3am7wKIRD.htm)|Aspis Warden|Gardien de l'Aspis|libre|
|[qd6yO0nBdmoz8Fl2.htm](pfs-season-3-bestiary/qd6yO0nBdmoz8Fl2.htm)|Cole Farsen, Advisor|Cole Farsen, Conseiller|libre|
|[qDpKFbhWgSWEEA00.htm](pfs-season-3-bestiary/qDpKFbhWgSWEEA00.htm)|Marcien Blakros (7-8)|Marcien Blakros (7-8)|libre|
|[qFzhrDwmnBhk4FyL.htm](pfs-season-3-bestiary/qFzhrDwmnBhk4FyL.htm)|Skeleton Warrior|Guerrier squelette|libre|
|[QgyodFPH6wehKmmM.htm](pfs-season-3-bestiary/QgyodFPH6wehKmmM.htm)|Carved Mourner|Bête sculptée|libre|
|[QHDVWljImF5sRhX2.htm](pfs-season-3-bestiary/QHDVWljImF5sRhX2.htm)|Drakauthix (PFS 3-15)|Drakauthix (PFS 3-15)|libre|
|[qnu2pwoPe5Hh73vB.htm](pfs-season-3-bestiary/qnu2pwoPe5Hh73vB.htm)|Uncle Jeb (1-2)|Oncle Jeb (1-2)|libre|
|[qPUP8Qtp7HXmXiBi.htm](pfs-season-3-bestiary/qPUP8Qtp7HXmXiBi.htm)|Displaced Robot (3-4)|Robot déplacé (3-4)|libre|
|[qRX3Gax6xC3yIqyt.htm](pfs-season-3-bestiary/qRX3Gax6xC3yIqyt.htm)|Ninth Army War Mage|Mage de guerre de la Neuvième armée|libre|
|[qulQzbgDOLz969yg.htm](pfs-season-3-bestiary/qulQzbgDOLz969yg.htm)|Void Boils|Bouillons du vide|libre|
|[QvrnfL8u3azh9lBA.htm](pfs-season-3-bestiary/QvrnfL8u3azh9lBA.htm)|Fetchling Sneak|Fetchelin furtif|libre|
|[R7d4HsytkpganuFa.htm](pfs-season-3-bestiary/R7d4HsytkpganuFa.htm)|Rock Launcher|Lance-rochers|libre|
|[RCCGR5wlfGNPkkBy.htm](pfs-season-3-bestiary/RCCGR5wlfGNPkkBy.htm)|Enraged Shrouded Bloodseeker|Cherchesang voilé enragé|libre|
|[rhohGomgIOjLv2by.htm](pfs-season-3-bestiary/rhohGomgIOjLv2by.htm)|Firespark Trap (1-2)|Piège étincelles (1-2)|libre|
|[riCZJcenSSIQckJi.htm](pfs-season-3-bestiary/riCZJcenSSIQckJi.htm)|Faulty Tidal Controls|Contrôle des marées défectueux|libre|
|[RLBOKSaMrOCA4fdZ.htm](pfs-season-3-bestiary/RLBOKSaMrOCA4fdZ.htm)|Ninth Army Ruffian|Voyou de la Neuvième armée|libre|
|[RmgJ2iZ20AKxZZjQ.htm](pfs-season-3-bestiary/RmgJ2iZ20AKxZZjQ.htm)|Animated Knickknack (3-4)|Babiole animée (3-4)|libre|
|[RTHM0lrmzvUskBMy.htm](pfs-season-3-bestiary/RTHM0lrmzvUskBMy.htm)|Arabuk|Arabuk|libre|
|[rwRPi6GwXhF34W55.htm](pfs-season-3-bestiary/rwRPi6GwXhF34W55.htm)|Branwaen|Branwaen|libre|
|[RxB7Hj5E3kGcE9Fi.htm](pfs-season-3-bestiary/RxB7Hj5E3kGcE9Fi.htm)|Ruthless Fetchling Sneak|Fetchelin furtif impitoyable|libre|
|[rXNQ9zxsf9rN7zUQ.htm](pfs-season-3-bestiary/rXNQ9zxsf9rN7zUQ.htm)|Enhanced Brown Mold|Moisissure brune améliorée|libre|
|[RZ8sWKkogng0IU8M.htm](pfs-season-3-bestiary/RZ8sWKkogng0IU8M.htm)|Vaggas the Bosun|Vaggas le bosco|libre|
|[S6xhwxwQDP3tEXiS.htm](pfs-season-3-bestiary/S6xhwxwQDP3tEXiS.htm)|Flensing Trap (7-8)|Piège ébarbeur (7-8)|libre|
|[S9v8OCktGEL0k6sC.htm](pfs-season-3-bestiary/S9v8OCktGEL0k6sC.htm)|Cinder Rat (Ashgrip)|Rat des braises (Ashgrip)|libre|
|[SBMZYJ1wYptIuWRA.htm](pfs-season-3-bestiary/SBMZYJ1wYptIuWRA.htm)|Unstable Column (3-4)|Colonne instable (3-4)|libre|
|[sfStaEmGGvXWftsn.htm](pfs-season-3-bestiary/sfStaEmGGvXWftsn.htm)|Horrid Nightmare|Destrier noir horrifiant|libre|
|[sHl25TefznyV8xC8.htm](pfs-season-3-bestiary/sHl25TefznyV8xC8.htm)|Unstable Skeleton Guard|Garde squelette instable|libre|
|[SJoWqX2P5LhJsbYA.htm](pfs-season-3-bestiary/SJoWqX2P5LhJsbYA.htm)|Thundering Wihsaak|Wihsaak tonitruant|libre|
|[SkV5zYJgu7hdpZE0.htm](pfs-season-3-bestiary/SkV5zYJgu7hdpZE0.htm)|Sparkles (11-12)|Sparkles (11-12)|libre|
|[SPt3t3ROjGqkwpCU.htm](pfs-season-3-bestiary/SPt3t3ROjGqkwpCU.htm)|Insolent's Castigation|Châtiment des insolents|libre|
|[SyFPYtqvE9jAZ6LR.htm](pfs-season-3-bestiary/SyFPYtqvE9jAZ6LR.htm)|Aspis Defender|Défenseur de l'Aspis|libre|
|[SzcyJHMmYgLwtXlQ.htm](pfs-season-3-bestiary/SzcyJHMmYgLwtXlQ.htm)|Blessed Crossbow Trap|Arbalète bénite|libre|
|[T8rc4C98gf87869G.htm](pfs-season-3-bestiary/T8rc4C98gf87869G.htm)|Strong Fire Sentry|Sentinelle de feu puissant|libre|
|[TIR0DiANNlcQGwMS.htm](pfs-season-3-bestiary/TIR0DiANNlcQGwMS.htm)|Overloading Arch (3-4)|Arche surchargée (3-4)|libre|
|[TmMK56lcxYBbhXqt.htm](pfs-season-3-bestiary/TmMK56lcxYBbhXqt.htm)|Pernicious Powder Keg|Tonnelet de poudre pernicieux|libre|
|[TrZ0ixbWfscr0k93.htm](pfs-season-3-bestiary/TrZ0ixbWfscr0k93.htm)|Raging Debris Storm|Tempête de débris rageuse|libre|
|[Tt9DtLxj1N2TBT9w.htm](pfs-season-3-bestiary/Tt9DtLxj1N2TBT9w.htm)|Kobold Dragon Mage (PFS 3-99)|Mage draconique Kobold (PFS 3-99)|libre|
|[tU0JjT2Hxbi8bPJW.htm](pfs-season-3-bestiary/tU0JjT2Hxbi8bPJW.htm)|Eternal Hall (7-8)|Couloir d'éternité (7-8)|libre|
|[tVsrWl9kfVIGG5OW.htm](pfs-season-3-bestiary/tVsrWl9kfVIGG5OW.htm)|Thick Pelagastr Tail|Queue de Pélagastr amélioré|libre|
|[U41mRr07sJywSFYZ.htm](pfs-season-3-bestiary/U41mRr07sJywSFYZ.htm)|Werebat (PFS 3-13)|Chauve-souris garou (PFS 3-13)|libre|
|[UBV2LL1j8vP9cW43.htm](pfs-season-3-bestiary/UBV2LL1j8vP9cW43.htm)|Cooking Disaster|Désastre en cuisine|libre|
|[uD1GCmruAUJ3zfdj.htm](pfs-season-3-bestiary/uD1GCmruAUJ3zfdj.htm)|Natron Eruption|Éruption de natron|libre|
|[uip49rfJG1KuUlyn.htm](pfs-season-3-bestiary/uip49rfJG1KuUlyn.htm)|Rockslide Trap|Glissement de terrain|libre|
|[uJe5OEPDuvIG6eg6.htm](pfs-season-3-bestiary/uJe5OEPDuvIG6eg6.htm)|Ghast (Arctic)|Blême (Arctique)|libre|
|[umiKZDfhNO3mXvEb.htm](pfs-season-3-bestiary/umiKZDfhNO3mXvEb.htm)|Alchemy-Gorged Giant Leech|Sangsue géante gorgée d'alchimie|libre|
|[uOgiYc5VGhSxkLFa.htm](pfs-season-3-bestiary/uOgiYc5VGhSxkLFa.htm)|Pontrius Tilasti (1-2)|Pontrius Tilasti (1-2)|libre|
|[Upa2rtDx5daLyx2z.htm](pfs-season-3-bestiary/Upa2rtDx5daLyx2z.htm)|Eloise's Last Gasp (3-4)|Le dernier soupir d'Éloise (3-4)|libre|
|[uzFHHePtuxBv6K9u.htm](pfs-season-3-bestiary/uzFHHePtuxBv6K9u.htm)|Revinus (1-2)|Révinus (1-2)|libre|
|[vkrKdprKwRpuXHS3.htm](pfs-season-3-bestiary/vkrKdprKwRpuXHS3.htm)|Caustic Mud Pot|Creuset de boue caustique|libre|
|[VMeVz1AsEkq6QWDE.htm](pfs-season-3-bestiary/VMeVz1AsEkq6QWDE.htm)|Muesello's Elite Summoning Rune|Rune de convocation d'élite de Muesello|libre|
|[vQjnfmvUzZwaUoBd.htm](pfs-season-3-bestiary/vQjnfmvUzZwaUoBd.htm)|Pelagastr Tail|Queue de Pélastre|libre|
|[vuxdXTrDMv5YQ1Fx.htm](pfs-season-3-bestiary/vuxdXTrDMv5YQ1Fx.htm)|Displaced Robot (7-8)|Robot déplacé (7-8)|libre|
|[vwV33OVSDH6fsdPI.htm](pfs-season-3-bestiary/vwV33OVSDH6fsdPI.htm)|Mummy Guardian (PFS 3-09)|Momie gardienne (PFS 3-09)|libre|
|[vwWjjQwPR9XItpjY.htm](pfs-season-3-bestiary/vwWjjQwPR9XItpjY.htm)|Giant Rat (PFS 3-98)|Rats géants (PFS 3-98)|libre|
|[w295JTcaHaTQuplt.htm](pfs-season-3-bestiary/w295JTcaHaTQuplt.htm)|Malfunctioning Tidal Controls|Contôle des marées défectueux|libre|
|[WaTrewLfMf8WH0O9.htm](pfs-season-3-bestiary/WaTrewLfMf8WH0O9.htm)|Ninth Army Mage|Mage de la Neuvième armée|libre|
|[WB4DMTHCIEBOiitp.htm](pfs-season-3-bestiary/WB4DMTHCIEBOiitp.htm)|Shadow Guardian|Gardien des ombres|libre|
|[WGi99nXZYSv7U92M.htm](pfs-season-3-bestiary/WGi99nXZYSv7U92M.htm)|Tadrun (1-2)|Tadrun (1-2)|libre|
|[wLM38nmdIITxkr4k.htm](pfs-season-3-bestiary/wLM38nmdIITxkr4k.htm)|Elite Green Hag (PFS 3-99)|Guenaude verte d'élite (PFS 3-99)|libre|
|[WpdOwEpoWwxrBUpF.htm](pfs-season-3-bestiary/WpdOwEpoWwxrBUpF.htm)|Peryton (Rifle Mutation)|Péryton (Mutation fusil)|libre|
|[wqJDAf89wSAdG0qy.htm](pfs-season-3-bestiary/wqJDAf89wSAdG0qy.htm)|Peryton (Arctic)|Péryton (Arctique)|libre|
|[WQUsCm35NNZUlJYy.htm](pfs-season-3-bestiary/WQUsCm35NNZUlJYy.htm)|Faulty Rockslide Trap|Glissement de terrain défectueux|libre|
|[WRVcJ6z2q2oDxTO7.htm](pfs-season-3-bestiary/WRVcJ6z2q2oDxTO7.htm)|Crushing Spirits (7-8)|Esprits accablants (7-8)|libre|
|[wun5VzDIMSZCVLuI.htm](pfs-season-3-bestiary/wun5VzDIMSZCVLuI.htm)|Waking Nightmare|Cauchemar éveillé|libre|
|[WxN19w3aO5MpVY6Z.htm](pfs-season-3-bestiary/WxN19w3aO5MpVY6Z.htm)|Cole Farsen, Gang Leader|Cole Farsen, Chef de gang|libre|
|[x6uXLDuFimMuvz3w.htm](pfs-season-3-bestiary/x6uXLDuFimMuvz3w.htm)|Half-Formed Abrikandilu|Abrikandilu à moité formé|libre|
|[XhxucXUyWWLyty9H.htm](pfs-season-3-bestiary/XhxucXUyWWLyty9H.htm)|Overloading Arch (7-8)|Arche surchargée (7-8)|libre|
|[xiBVzPUwh1QI2lIs.htm](pfs-season-3-bestiary/xiBVzPUwh1QI2lIs.htm)|Eltha Embercall (9-10)|Altha Embercall (9-10)|libre|
|[xkXMKw8GhdI5FNar.htm](pfs-season-3-bestiary/xkXMKw8GhdI5FNar.htm)|Monastic Bodyguard (3-4)|Protecteur monastique (3-4)|libre|
|[Xpf9HQDxoafOwfoB.htm](pfs-season-3-bestiary/Xpf9HQDxoafOwfoB.htm)|Thomil Bolyrius (3-4)|Thomil Bolyrius (3-4)|libre|
|[xShj7KlFATzm397y.htm](pfs-season-3-bestiary/xShj7KlFATzm397y.htm)|Wasp Swarm (Arctic)|Nuée de guêpes (Arctique)|libre|
|[xVmDwoBxjKyCihYz.htm](pfs-season-3-bestiary/xVmDwoBxjKyCihYz.htm)|Marcien Blakros (9-10) (Shadow Double)|Marcien Blakros (9-10)(Double d'ombre)|libre|
|[XxbdbTlnY0P3RU3h.htm](pfs-season-3-bestiary/XxbdbTlnY0P3RU3h.htm)|Aspis Usurper|Usurpateur de l'Aspis|libre|
|[XYBA4EoyegNciY2P.htm](pfs-season-3-bestiary/XYBA4EoyegNciY2P.htm)|Elite Hell Hound (PFS 3-07)|Molosse infernal élite (PFS 3-7)|libre|
|[xYByANF8t6zhxt4p.htm](pfs-season-3-bestiary/xYByANF8t6zhxt4p.htm)|Veteran Onyx Alliance Scout|Éclaireur vétéran de l'Alliance Onyx|libre|
|[XyrToL9Wyl4MoAf1.htm](pfs-season-3-bestiary/XyrToL9Wyl4MoAf1.htm)|Caliclotherax (9-10)|Caliclothérax (9-10)|libre|
|[Y5ZcwtbKxA1BImQq.htm](pfs-season-3-bestiary/Y5ZcwtbKxA1BImQq.htm)|Umbral Cu Sith|Cu sith ombral|libre|
|[yb3tVvtIkblQoEoH.htm](pfs-season-3-bestiary/yb3tVvtIkblQoEoH.htm)|Unstable Column (5-6)|Colonne instable (5-6)|libre|
|[yfh8wibKauDIh6Dj.htm](pfs-season-3-bestiary/yfh8wibKauDIh6Dj.htm)|Sulfuric Slime|Muscosité sulfurique|libre|
|[yhQALO3emgx8Z5N9.htm](pfs-season-3-bestiary/yhQALO3emgx8Z5N9.htm)|Waking Terror|Terreur éveillée|libre|
|[YMT0Hl0LO9QKrb3X.htm](pfs-season-3-bestiary/YMT0Hl0LO9QKrb3X.htm)|Aspis Agent|Agent de l'Aspis|libre|
|[YsF8TteK6XsThooV.htm](pfs-season-3-bestiary/YsF8TteK6XsThooV.htm)|Shanrigol Mound|Monticule de shanrigol|libre|
|[yw5hz4q8vn7S7aUF.htm](pfs-season-3-bestiary/yw5hz4q8vn7S7aUF.htm)|Umbral Drake|Drake ombral|libre|
|[YwkPGpBPttW2G7Xn.htm](pfs-season-3-bestiary/YwkPGpBPttW2G7Xn.htm)|Bloody Skeleton Guard|Garde squelette ensanglanté|libre|
|[Z5zTWbeZ0N1XedgS.htm](pfs-season-3-bestiary/Z5zTWbeZ0N1XedgS.htm)|Strong Electric Latch Rune|Rune électrique renforcée de loquet|libre|
|[zC29Os27IYf65qNp.htm](pfs-season-3-bestiary/zC29Os27IYf65qNp.htm)|Shadow Spears (9-10)|Lances d'ombres (9-10)|libre|
|[zDNR0eYpfEvGLOGo.htm](pfs-season-3-bestiary/zDNR0eYpfEvGLOGo.htm)|Debris Storm|Tempête de débris|libre|
|[zhtkM2GPvDNw3bY1.htm](pfs-season-3-bestiary/zhtkM2GPvDNw3bY1.htm)|Eloise's Ghost (3-4)|Fantôme d'Éloïse (3-4)|libre|
|[zIMgoLwuJVMYxqeF.htm](pfs-season-3-bestiary/zIMgoLwuJVMYxqeF.htm)|Languid Isqulug|Isqulug léthargique|libre|
|[zMVQXCDhP7yeuQg3.htm](pfs-season-3-bestiary/zMVQXCDhP7yeuQg3.htm)|Aspis Bulwark|Protecteur de l'Aspis|libre|
|[ZSCBm3JFU6tFtqvL.htm](pfs-season-3-bestiary/ZSCBm3JFU6tFtqvL.htm)|Eloise's Last Gasp (1-2)|Le dernier soupir d'Éloise (1-2)|libre|
|[zUbfyAyNN4aYLQKA.htm](pfs-season-3-bestiary/zUbfyAyNN4aYLQKA.htm)|Khisisi (5-6)|Khisisi (5-6)|libre|
|[zzbBBSmjvCzJ6lyt.htm](pfs-season-3-bestiary/zzbBBSmjvCzJ6lyt.htm)|Monastic Bodyguard (5-6)|Protecteur monastique (5-6)|libre|
|[ZzH4EqJtWIhaNWSv.htm](pfs-season-3-bestiary/ZzH4EqJtWIhaNWSv.htm)|Animated Blade (PFS 3-99)|Lame animée (PFS 3-99)|libre|
