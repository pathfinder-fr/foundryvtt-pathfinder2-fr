# État de la traduction (ac-advanced-maneuvers)

 * **libre**: 81
 * **changé**: 1


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[tsOaXTRWQvsKTyaL.htm](ac-advanced-maneuvers/tsOaXTRWQvsKTyaL.htm)|Gallop|Galop|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0WtqP7Wei3AxHh5M.htm](ac-advanced-maneuvers/0WtqP7Wei3AxHh5M.htm)|Frightening Display|Posture effrayante|libre|
|[5gK05d4qc7frX6zr.htm](ac-advanced-maneuvers/5gK05d4qc7frX6zr.htm)|Diving Skewer|Piqûre en plongée|libre|
|[5ux66ubWlevY7z2K.htm](ac-advanced-maneuvers/5ux66ubWlevY7z2K.htm)|Darting Stab|Dard éclair|libre|
|[5x3uxG6DYwSIrN30.htm](ac-advanced-maneuvers/5x3uxG6DYwSIrN30.htm)|Wrapping Tendrils|Tentacules enveloppants|libre|
|[6LvtdtVVwtbzqUXG.htm](ac-advanced-maneuvers/6LvtdtVVwtbzqUXG.htm)|Screaming Skull|Crâne hurlant|libre|
|[7C9iFWBpEOJs4Uo2.htm](ac-advanced-maneuvers/7C9iFWBpEOJs4Uo2.htm)|Pterosaur Swoop|Piqué du ptérosaure|libre|
|[7H8wqKtsAaSgyLyp.htm](ac-advanced-maneuvers/7H8wqKtsAaSgyLyp.htm)|Grab and Sting|Saisir et piquer|libre|
|[8JdETVTpbZAgtBA3.htm](ac-advanced-maneuvers/8JdETVTpbZAgtBA3.htm)|Fire Breath|Souffle de feu|libre|
|[9IqqECMTmfbaBY0C.htm](ac-advanced-maneuvers/9IqqECMTmfbaBY0C.htm)|Long Stomp|Longue cavalcade|libre|
|[9o7NwxGqohpa9M7r.htm](ac-advanced-maneuvers/9o7NwxGqohpa9M7r.htm)|Pick at the Meat|Piquer la viande|libre|
|[aDuPDWgutiDIoWjW.htm](ac-advanced-maneuvers/aDuPDWgutiDIoWjW.htm)|Gulp Blood|Gorgée de sang|libre|
|[AH3kNbdGLYaRjVAr.htm](ac-advanced-maneuvers/AH3kNbdGLYaRjVAr.htm)|Bony Constriction|Constriction osseuse|libre|
|[AJ7hsZCvwLIzalYA.htm](ac-advanced-maneuvers/AJ7hsZCvwLIzalYA.htm)|Scurry|S'empresser|libre|
|[ClUiCoBn3lHBBK5c.htm](ac-advanced-maneuvers/ClUiCoBn3lHBBK5c.htm)|Darting Attack|Attaque foudroyante|libre|
|[cpMkN79PdNci3nGp.htm](ac-advanced-maneuvers/cpMkN79PdNci3nGp.htm)|Distracting Spray|Aspersion distrayante|libre|
|[cVrW2GGLVpydj8h5.htm](ac-advanced-maneuvers/cVrW2GGLVpydj8h5.htm)|Blood Feast|Festin sanglant|libre|
|[Di4yucWH3akIJXOr.htm](ac-advanced-maneuvers/Di4yucWH3akIJXOr.htm)|Crab Cannon|Cannon crabe|libre|
|[eFZtA8LHdw4mWJVt.htm](ac-advanced-maneuvers/eFZtA8LHdw4mWJVt.htm)|Hair Barrage|Déluge velu|libre|
|[ePNzRvGvBqzIujJr.htm](ac-advanced-maneuvers/ePNzRvGvBqzIujJr.htm)|Circling Flyby|Vol circulaire|libre|
|[EvuMVR9ut9wIHOtq.htm](ac-advanced-maneuvers/EvuMVR9ut9wIHOtq.htm)|Bay|Plainte|libre|
|[EwCqdvU8WOw2SSxm.htm](ac-advanced-maneuvers/EwCqdvU8WOw2SSxm.htm)|Boar Charge|Charge du sanglier|libre|
|[F98ajoIakyOPEuwj.htm](ac-advanced-maneuvers/F98ajoIakyOPEuwj.htm)|Shred|Déchiqueter|libre|
|[g4JEtCnKXyY4LJpm.htm](ac-advanced-maneuvers/g4JEtCnKXyY4LJpm.htm)|Feast on the Fallen|Festin sur le déchu|libre|
|[gFZCyLnqeWrK6u0P.htm](ac-advanced-maneuvers/gFZCyLnqeWrK6u0P.htm)|Antler Catapult|Ramure catapulte|libre|
|[gvOo0KSSHkqfW3j5.htm](ac-advanced-maneuvers/gvOo0KSSHkqfW3j5.htm)|Grabbing Branches|Branches accrocheuses|libre|
|[gzefJocgdWbXWWPi.htm](ac-advanced-maneuvers/gzefJocgdWbXWWPi.htm)|Extend Pseudopod|Étendre un pseudopode|libre|
|[H1ElYt6KovGYGzLD.htm](ac-advanced-maneuvers/H1ElYt6KovGYGzLD.htm)|Lumbering Knockdown|Renversement boutoir|libre|
|[h1Rldre8WVjUR5XO.htm](ac-advanced-maneuvers/h1Rldre8WVjUR5XO.htm)|Disgusting Gallop|Galop immonde|libre|
|[hjYmAzurrzcGyds4.htm](ac-advanced-maneuvers/hjYmAzurrzcGyds4.htm)|Spring Kick|Ruade bondissante|libre|
|[HLD4JW6SUn5Oy8C9.htm](ac-advanced-maneuvers/HLD4JW6SUn5Oy8C9.htm)|Badger Rage|Rage du blaireau|libre|
|[HY6rzDpBJ4L5OmgI.htm](ac-advanced-maneuvers/HY6rzDpBJ4L5OmgI.htm)|Breach|Saut piqué|libre|
|[HzGLDKNJJBs7fsYN.htm](ac-advanced-maneuvers/HzGLDKNJJBs7fsYN.htm)|Snatch|Arracher du sol|libre|
|[i8RWZ5XkR6DYbOCp.htm](ac-advanced-maneuvers/i8RWZ5XkR6DYbOCp.htm)|Telepathic Pounce|Assaut télépathique|libre|
|[IJYikMs3TzYGpdHm.htm](ac-advanced-maneuvers/IJYikMs3TzYGpdHm.htm)|Weighted Kick|Ruade lestée|libre|
|[IOD6OoDYcncviidf.htm](ac-advanced-maneuvers/IOD6OoDYcncviidf.htm)|Swimming Snap|Claquement aquatique|libre|
|[IoWvKP6WDijlkrin.htm](ac-advanced-maneuvers/IoWvKP6WDijlkrin.htm)|Knock Aside|Pousser de côté|libre|
|[JMHoCFb886K6dT1n.htm](ac-advanced-maneuvers/JMHoCFb886K6dT1n.htm)|Rhinoceros Charge|Charge du rhinocéros|libre|
|[kKW68gi1FNJlNSOp.htm](ac-advanced-maneuvers/kKW68gi1FNJlNSOp.htm)|Constrict|Constriction|libre|
|[l3aasm0to5RvUkU0.htm](ac-advanced-maneuvers/l3aasm0to5RvUkU0.htm)|Poisonous Sweep|Balayage empoisonné|libre|
|[LaBfTYUsvoI3nscv.htm](ac-advanced-maneuvers/LaBfTYUsvoI3nscv.htm)|Take a Taste|Goûte un morceau|libre|
|[LBPGvjR3zSau0QC4.htm](ac-advanced-maneuvers/LBPGvjR3zSau0QC4.htm)|Liberating Bite|Morsure libératrice|libre|
|[LCilgiiIZp408R3k.htm](ac-advanced-maneuvers/LCilgiiIZp408R3k.htm)|Flamethrower|Lance-flammes|libre|
|[lrchn6ROZcuKCg3C.htm](ac-advanced-maneuvers/lrchn6ROZcuKCg3C.htm)|Careful Withdraw|Retrait précautionneux|libre|
|[MNiXOHI1Ezh0D24r.htm](ac-advanced-maneuvers/MNiXOHI1Ezh0D24r.htm)|Tail Swipe|Coup de queue|libre|
|[MQv67R7KBIrJkshN.htm](ac-advanced-maneuvers/MQv67R7KBIrJkshN.htm)|Grip Throat|Gorge serrée|libre|
|[NMaMTiDB40L5O41S.htm](ac-advanced-maneuvers/NMaMTiDB40L5O41S.htm)|Telekinetic Assault|Assaut télékinésique|libre|
|[nyJV4tsR316EmsYF.htm](ac-advanced-maneuvers/nyJV4tsR316EmsYF.htm)|Raging Stampede|Charge déchaînée|libre|
|[oCgdnQP5bMDD02YC.htm](ac-advanced-maneuvers/oCgdnQP5bMDD02YC.htm)|Bear Hug|Étreinte de l'ours|libre|
|[ODGf6brAHHKrSAPo.htm](ac-advanced-maneuvers/ODGf6brAHHKrSAPo.htm)|Wing Thrash|Balayage d'aile|libre|
|[oisXYTTYE0TABboA.htm](ac-advanced-maneuvers/oisXYTTYE0TABboA.htm)|Gnaw|Ronger|libre|
|[ow3S5zhS51Nj8tsI.htm](ac-advanced-maneuvers/ow3S5zhS51Nj8tsI.htm)|Unnerving Screech|Cri strident|libre|
|[oYEXImMSzg0eDqzR.htm](ac-advanced-maneuvers/oYEXImMSzg0eDqzR.htm)|Grabbing Trunk|Trompe préhensile|libre|
|[oyNZpuQ6EU3prvo2.htm](ac-advanced-maneuvers/oyNZpuQ6EU3prvo2.htm)|Spiked Bunker|Bunker pointu|libre|
|[OYzEhVH6zMgf64Tg.htm](ac-advanced-maneuvers/OYzEhVH6zMgf64Tg.htm)|Death from Above|Piqué furieux|libre|
|[PAXJgysWqv3T0tTD.htm](ac-advanced-maneuvers/PAXJgysWqv3T0tTD.htm)|Sand Stride|Marche sur le sable|libre|
|[pMPtswKNokDNyYR2.htm](ac-advanced-maneuvers/pMPtswKNokDNyYR2.htm)|Defensive Curl|Posture défensive|libre|
|[PZAHlaN4aG6NkSjj.htm](ac-advanced-maneuvers/PZAHlaN4aG6NkSjj.htm)|Burrowing Ambush|Embuscade souterraine|libre|
|[q0Vh2V6KlzdMi1Pv.htm](ac-advanced-maneuvers/q0Vh2V6KlzdMi1Pv.htm)|Tongue Pull|Traction de la langue|libre|
|[R2RNXJH06UhSMmzp.htm](ac-advanced-maneuvers/R2RNXJH06UhSMmzp.htm)|Sudden Retreat|Retraite soudaine|libre|
|[SkVl3yomdYZoS7Hx.htm](ac-advanced-maneuvers/SkVl3yomdYZoS7Hx.htm)|Aerial Retreat|Retraite aérienne|libre|
|[TCEQtO5DawzVAIyT.htm](ac-advanced-maneuvers/TCEQtO5DawzVAIyT.htm)|Tearing Clutch|Serrage déchirant|libre|
|[tLUBgSE9Mm6ic6sY.htm](ac-advanced-maneuvers/tLUBgSE9Mm6ic6sY.htm)|Throw Rock|Lancer de rocher|libre|
|[ttJ1CanrAuehoSV8.htm](ac-advanced-maneuvers/ttJ1CanrAuehoSV8.htm)|Death Roll|Roulade mortelle|libre|
|[U0J8J5ukmqnOskE8.htm](ac-advanced-maneuvers/U0J8J5ukmqnOskE8.htm)|Cat Pounce|Bond de félin|libre|
|[uhv8V9STpaCXdkCf.htm](ac-advanced-maneuvers/uhv8V9STpaCXdkCf.htm)|Flower Dance|Danse florale|libre|
|[Upr7u0KhPAIMOqXv.htm](ac-advanced-maneuvers/Upr7u0KhPAIMOqXv.htm)|Snatch and Zap|Saisir et électrocuter|libre|
|[uR3j6TAJFeOVUHpY.htm](ac-advanced-maneuvers/uR3j6TAJFeOVUHpY.htm)|Lurching Rush|Embardée ruante|libre|
|[Uv49tQCwUUjaMCj5.htm](ac-advanced-maneuvers/Uv49tQCwUUjaMCj5.htm)|Bouncing Slam|Coup rebondissant|libre|
|[VSsW37hmpmXNOUwd.htm](ac-advanced-maneuvers/VSsW37hmpmXNOUwd.htm)|Float|Flottement|libre|
|[W6zkL0y4MqvlR0yW.htm](ac-advanced-maneuvers/W6zkL0y4MqvlR0yW.htm)|Rolling Knockdown|Roulade renversante|libre|
|[WdTzlCO2i4loeYkQ.htm](ac-advanced-maneuvers/WdTzlCO2i4loeYkQ.htm)|Ultrasonic Scream|Cri ultrasonique|libre|
|[WocN44CJGHYxitzM.htm](ac-advanced-maneuvers/WocN44CJGHYxitzM.htm)|Flying Strafe|Frappes en vol|libre|
|[wQewsJLOsfucMXyB.htm](ac-advanced-maneuvers/wQewsJLOsfucMXyB.htm)|Defensive Curl (Hermit Krait)|Enroulement défensif (Serpent-pagure)|libre|
|[X1UE76uHDTcXf9ZW.htm](ac-advanced-maneuvers/X1UE76uHDTcXf9ZW.htm)|Tongue Grab|Langue gluante|libre|
|[x4clojDNIvYB5V7X.htm](ac-advanced-maneuvers/x4clojDNIvYB5V7X.htm)|Drench|Tremper|libre|
|[X4VjMfVVAjZjwcrT.htm](ac-advanced-maneuvers/X4VjMfVVAjZjwcrT.htm)|Flyby Attack|Attaque en vol|libre|
|[Xd4dnyHWexQRVyXn.htm](ac-advanced-maneuvers/Xd4dnyHWexQRVyXn.htm)|Seedpod Spring|Éclosion printanière|libre|
|[xssFjTXqGlsaddvl.htm](ac-advanced-maneuvers/xssFjTXqGlsaddvl.htm)|Takedown|Renversement|libre|
|[yCbm5XbAzPc6U82H.htm](ac-advanced-maneuvers/yCbm5XbAzPc6U82H.htm)|Bounding Retreat|Retraite bondissante|libre|
|[yXw82Zd5nkcCUfRQ.htm](ac-advanced-maneuvers/yXw82Zd5nkcCUfRQ.htm)|Overwhelm|Prise de mâchoire|libre|
|[zSlsnbvsGKdCTzL1.htm](ac-advanced-maneuvers/zSlsnbvsGKdCTzL1.htm)|Hustle|S'empresser|libre|
