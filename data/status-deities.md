# État de la traduction (deities)

 * **libre**: 452
 * **officielle**: 1


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[01liRoMjNR5XgorX.htm](deities/01liRoMjNR5XgorX.htm)|Mrtyu|Mrtyu|libre|
|[0BH54ZVR8NcrGlMu.htm](deities/0BH54ZVR8NcrGlMu.htm)|Findeladlara|Findeladlara|libre|
|[0cimyk6ZbdvGOmwC.htm](deities/0cimyk6ZbdvGOmwC.htm)|Nulgreth|Nulgreth|libre|
|[0EXQHfNN11AfDqNm.htm](deities/0EXQHfNN11AfDqNm.htm)|Phi Deva|Phi Deva|libre|
|[0nQItOlce5ua6Eaf.htm](deities/0nQItOlce5ua6Eaf.htm)|Sekhmet|Sekhmet|libre|
|[0R2BjVb7d9tYCIbk.htm](deities/0R2BjVb7d9tYCIbk.htm)|Tlehar|Tléhar|libre|
|[0uKqA42aPdHEqESI.htm](deities/0uKqA42aPdHEqESI.htm)|Guardians of the Sacred Self|Gardiens du Moi Sacré|libre|
|[0UunUljPt3uZGuMj.htm](deities/0UunUljPt3uZGuMj.htm)|Pavnuri|Pavnuri|libre|
|[0xCttKr5oqjirGCe.htm](deities/0xCttKr5oqjirGCe.htm)|Aakriti|Aakriti|libre|
|[14GLsVobnZTEmJrs.htm](deities/14GLsVobnZTEmJrs.htm)|Kelizandri|Kélizandre|libre|
|[19qKU9wPd0JctskB.htm](deities/19qKU9wPd0JctskB.htm)|Hataam|Hataam|libre|
|[1CqGgDeEuaVOgFFE.htm](deities/1CqGgDeEuaVOgFFE.htm)|Zelishkar|Zélishkar|libre|
|[1fmTXGRPa8fLXZKL.htm](deities/1fmTXGRPa8fLXZKL.htm)|Balumbdar|Balumbdar|libre|
|[1LiO5bDQIlJmXk35.htm](deities/1LiO5bDQIlJmXk35.htm)|Lady Razor|Dame Couperet (Déchu)|libre|
|[1nQVLOyWv5kC0bCU.htm](deities/1nQVLOyWv5kC0bCU.htm)|Ravithra|Ravithra|libre|
|[1NxkZf6ndijKeufX.htm](deities/1NxkZf6ndijKeufX.htm)|Sairazul|Saïrazul|libre|
|[1TDsbM52mkWBxlur.htm](deities/1TDsbM52mkWBxlur.htm)|Granduncle Taproot|Grand'oncle Primeracine (Homme-vert)|libre|
|[1xG0PDxn0rhmJhMS.htm](deities/1xG0PDxn0rhmJhMS.htm)|Yelayne|Yélayne|libre|
|[1xHnoMlCyCV70TnT.htm](deities/1xHnoMlCyCV70TnT.htm)|Saloc|Saloc|libre|
|[22n3N47sqbqbDxbp.htm](deities/22n3N47sqbqbDxbp.htm)|Urgathoa|Urgathoa|libre|
|[29jHeZ1jA23AnZrV.htm](deities/29jHeZ1jA23AnZrV.htm)|Elion|Élion|libre|
|[2fVbLwfQjyHSoSVy.htm](deities/2fVbLwfQjyHSoSVy.htm)|Titivilus|Titivilus|libre|
|[2mZmXeokndl2ocWK.htm](deities/2mZmXeokndl2ocWK.htm)|Nyuo-Ogh|Nyuo-Ogh|libre|
|[2uY2CDR2DyBqxbVU.htm](deities/2uY2CDR2DyBqxbVU.htm)|Stone's Blood|Sang de la pierre|libre|
|[2XM5clKBu67zJEwW.htm](deities/2XM5clKBu67zJEwW.htm)|Aleth|Aleth|libre|
|[30uY6NkhYmPI3Oii.htm](deities/30uY6NkhYmPI3Oii.htm)|Bergelmir|Bergelmir|libre|
|[39z55kowQFOUH2v0.htm](deities/39z55kowQFOUH2v0.htm)|Droskar|Droskar|libre|
|[3c15mKInre8c2iRo.htm](deities/3c15mKInre8c2iRo.htm)|Hadregash|Hadregash|libre|
|[3ffVxwgtvR1imHm8.htm](deities/3ffVxwgtvR1imHm8.htm)|Gendowyn|Gendowyn|libre|
|[3INt1X4ijBYnZYbU.htm](deities/3INt1X4ijBYnZYbU.htm)|Shyka|Shyka|libre|
|[3lgzcg84J0fsVqjh.htm](deities/3lgzcg84J0fsVqjh.htm)|Esoteric Order of the Palatine Eye|Ordre ésotérique de l'Oeil palatin|libre|
|[3nxIpMGeCvcBIVov.htm](deities/3nxIpMGeCvcBIVov.htm)|Nyarlathotep (The Crawling Chaos)|Nyarlathotep (Le Chaos Rampant)|libre|
|[3vzvggkPuLkTF6So.htm](deities/3vzvggkPuLkTF6So.htm)|Sovyrian Conclave|Conclave Sovyrien|libre|
|[44fcXgp3fnkoyb4R.htm](deities/44fcXgp3fnkoyb4R.htm)|Rowdrosh|Rowdrosh|libre|
|[4bEPoujxhYdUyUOg.htm](deities/4bEPoujxhYdUyUOg.htm)|Ydersius|Ydersius|libre|
|[4GO4iT585ynLJnc8.htm](deities/4GO4iT585ynLJnc8.htm)|Chaldira|Chaldira|libre|
|[4HJKRGYIVKeUdOwf.htm](deities/4HJKRGYIVKeUdOwf.htm)|Nin|Nin|libre|
|[4I1aVKED08cwcC4H.htm](deities/4I1aVKED08cwcC4H.htm)|Laws of Mortality|Lois de la mortalité|libre|
|[4Rjj2SA7xSxT1ftC.htm](deities/4Rjj2SA7xSxT1ftC.htm)|Aegirran|Aegirran|libre|
|[51kMMWaT6CqpGlGk.htm](deities/51kMMWaT6CqpGlGk.htm)|Achaekek|Achaékek|libre|
|[58gSanY90BSz9Uwa.htm](deities/58gSanY90BSz9Uwa.htm)|Shawnari|Shawnari|libre|
|[5dhBYY6sWOJotbpU.htm](deities/5dhBYY6sWOJotbpU.htm)|Smiad|Smiad|libre|
|[5QjycohRhMIejeN7.htm](deities/5QjycohRhMIejeN7.htm)|Jezelda|Jézelda|libre|
|[5YUNikBpJZ596MRD.htm](deities/5YUNikBpJZ596MRD.htm)|Anubis|Anubis|libre|
|[5ZIvg8CkqRMepVcX.htm](deities/5ZIvg8CkqRMepVcX.htm)|Korada|Korada|libre|
|[65o189p2eUPbqaDK.htm](deities/65o189p2eUPbqaDK.htm)|Narriseminek|Narriseminek|libre|
|[6gxC5awANDFs1kLM.htm](deities/6gxC5awANDFs1kLM.htm)|Grask Uldeth|Grask Uldeth|libre|
|[6j37TAv5BdOmrsUV.htm](deities/6j37TAv5BdOmrsUV.htm)|Yamasoth|Yamasoth|libre|
|[6O4UGWcYERDEugzk.htm](deities/6O4UGWcYERDEugzk.htm)|Fumeiyoshi|Fumeiyoshi|libre|
|[6YX5wtiLvDhwk1iZ.htm](deities/6YX5wtiLvDhwk1iZ.htm)|Oaur-Ooung|Oaur-Ooung|libre|
|[7Gs5SnHEd4IciJIw.htm](deities/7Gs5SnHEd4IciJIw.htm)|Rhan-Tegoth|Rhan-Tégoth|libre|
|[7l8fDCLz8fo4qEib.htm](deities/7l8fDCLz8fo4qEib.htm)|Rokoga Gin|Rokoga Gin|libre|
|[7L9KIqixgseSCY3X.htm](deities/7L9KIqixgseSCY3X.htm)|Ymeri|Yméri|libre|
|[7zyxg8TlXCDzHGMf.htm](deities/7zyxg8TlXCDzHGMf.htm)|The Last Breath|Le Dernier Souffle|libre|
|[81EbN765Clkvwyqi.htm](deities/81EbN765Clkvwyqi.htm)|Tresmalvos|Tresmalvos|libre|
|[83jKKHQPoqVylmrF.htm](deities/83jKKHQPoqVylmrF.htm)|Yaezhing|Yaezhing|libre|
|[88vRw2ZVPax4hhga.htm](deities/88vRw2ZVPax4hhga.htm)|Gorum|Gorum|libre|
|[8DXel4AvpyUy6oJ0.htm](deities/8DXel4AvpyUy6oJ0.htm)|Eritrice|Éritrice|libre|
|[8hsVGRyq0dKMwwXT.htm](deities/8hsVGRyq0dKMwwXT.htm)|Obari|Obari|libre|
|[8Oy4qHylOOp79JxD.htm](deities/8Oy4qHylOOp79JxD.htm)|Nhimbaloth|Nhimbaloth|libre|
|[8roijlQ03l9rWFFe.htm](deities/8roijlQ03l9rWFFe.htm)|Zevgavizeb|Zevgavizeb|libre|
|[8wOAwLMgB1HFLo9n.htm](deities/8wOAwLMgB1HFLo9n.htm)|Winlas|Winlas|libre|
|[8xrpo05lZvTQaAkJ.htm](deities/8xrpo05lZvTQaAkJ.htm)|Apep|Apep|libre|
|[8xYnD5FuLI4RSJN8.htm](deities/8xYnD5FuLI4RSJN8.htm)|Faith in the Fallen|Foi envers les Tombés|libre|
|[8ZSywyo8YQPduo92.htm](deities/8ZSywyo8YQPduo92.htm)|Zura|Zura|libre|
|[97Qe42wJN5EVUS14.htm](deities/97Qe42wJN5EVUS14.htm)|Sturovenen|Sturovenen|libre|
|[9EKyWmjLUu42evxC.htm](deities/9EKyWmjLUu42evxC.htm)|Nethys|Néthys|libre|
|[9eltBjLngkR9x1Xm.htm](deities/9eltBjLngkR9x1Xm.htm)|Trelmarixian|Trelmarixian|libre|
|[9FWBJcADLEeP8Krf.htm](deities/9FWBJcADLEeP8Krf.htm)|Valmallos|Valmallos|libre|
|[9LwncUwHbG1f6qIt.htm](deities/9LwncUwHbG1f6qIt.htm)|Marishi|Marishi|libre|
|[9MhgyxTfsffWzDbU.htm](deities/9MhgyxTfsffWzDbU.htm)|Ssila'meshnik|Ssila'meshnik|libre|
|[9n2CI0B3EA4y3BuU.htm](deities/9n2CI0B3EA4y3BuU.htm)|Alseta|Alséta|libre|
|[9vcKBv8h5vugb8lL.htm](deities/9vcKBv8h5vugb8lL.htm)|Fandarra|Fandarra|libre|
|[9WD0hbUT0IbnuTiM.htm](deities/9WD0hbUT0IbnuTiM.htm)|Baalzebul|Baalzébul|libre|
|[9Z1vOSaLsEiAs2yN.htm](deities/9Z1vOSaLsEiAs2yN.htm)|Thalaphyrr|Thalaphyrr (Déchu)|libre|
|[A37XqON6QRPzQhp7.htm](deities/A37XqON6QRPzQhp7.htm)|Hshurha|Hshurha|libre|
|[aChBtIDfA59QQ7SN.htm](deities/aChBtIDfA59QQ7SN.htm)|Ayrzul|Ayrzul|libre|
|[Adk14fcM9UHCQWb1.htm](deities/Adk14fcM9UHCQWb1.htm)|Corosbel|Corosbel|libre|
|[AEj745gVsRehu2EL.htm](deities/AEj745gVsRehu2EL.htm)|Etaris|Étaris|libre|
|[Af4TSJjutVi9Vdwi.htm](deities/Af4TSJjutVi9Vdwi.htm)|Mahathallah|Mahathallah|libre|
|[AHX89nhuVhmg8k6w.htm](deities/AHX89nhuVhmg8k6w.htm)|Dagon|Dagon|libre|
|[AIcDyoff265z9285.htm](deities/AIcDyoff265z9285.htm)|Kofusachi|Kofusachi|libre|
|[aievwuZalZ2abs2q.htm](deities/aievwuZalZ2abs2q.htm)|Thoth|Thoth|libre|
|[aipkJQxP4GBsTaGq.htm](deities/aipkJQxP4GBsTaGq.htm)|Lamashtu|Lamashtu|libre|
|[aj4ChTlDcZCmrtJD.htm](deities/aj4ChTlDcZCmrtJD.htm)|Baphomet|Baphomet|libre|
|[AJ7Wgpj5Ext9wbtj.htm](deities/AJ7Wgpj5Ext9wbtj.htm)|Keepers of the Hearth|Gardiens du foyer|libre|
|[akcmQbvNWfhEfEhf.htm](deities/akcmQbvNWfhEfEhf.htm)|Arqueros|Arqueros|libre|
|[aLCfEgt48SRUHnMC.htm](deities/aLCfEgt48SRUHnMC.htm)|Jaidi|Jaïdi|libre|
|[AlL9GtlTldbqNoVm.htm](deities/AlL9GtlTldbqNoVm.htm)|Calistria|Calistria|libre|
|[AMFkYKUNQ3kCD0ob.htm](deities/AMFkYKUNQ3kCD0ob.htm)|Doloras|Doloras|libre|
|[ArmPZQrl0SFAKvrY.htm](deities/ArmPZQrl0SFAKvrY.htm)|Ma'at|Maât|libre|
|[aTYy1WHwAZiuei4h.htm](deities/aTYy1WHwAZiuei4h.htm)|Tsukiyo|Tsukiyo|libre|
|[auHS1P7mXJjO2QlY.htm](deities/auHS1P7mXJjO2QlY.htm)|Genzaeri|Genzaéri|libre|
|[aUkAP1fVqOjtRzkc.htm](deities/aUkAP1fVqOjtRzkc.htm)|Blooms of the Spreading Weald|Fleurs de la Forêt Étendue|libre|
|[avyBK4NbMllCvRj8.htm](deities/avyBK4NbMllCvRj8.htm)|Waves of the Boundless Sea|Vagues de la Mer Infinie|libre|
|[aVyxfqY9GBjTqQul.htm](deities/aVyxfqY9GBjTqQul.htm)|Eiseth|Eiseth|libre|
|[b7H379HnCOv9UZtd.htm](deities/b7H379HnCOv9UZtd.htm)|Orcus|Orcus|libre|
|[baMgw2Jw17dlmMzj.htm](deities/baMgw2Jw17dlmMzj.htm)|Reymenda|Reymenda|libre|
|[bc2B53BGQ7OOwZg6.htm](deities/bc2B53BGQ7OOwZg6.htm)|Bharnarol|Bharnarol|libre|
|[bF77ydtptI54STl9.htm](deities/bF77ydtptI54STl9.htm)|Jaidz|Jaidz|libre|
|[Bhxji0kxIbMKqdkw.htm](deities/Bhxji0kxIbMKqdkw.htm)|Rovagug|Rovagug|libre|
|[bIl8GkwYltjsAMcm.htm](deities/bIl8GkwYltjsAMcm.htm)|Minderhal|Minderhal|libre|
|[BjOTP1lUHouAQzt4.htm](deities/BjOTP1lUHouAQzt4.htm)|Children of the Night|Enfants de la Nuit|libre|
|[BlHzQ693N9UdLVrR.htm](deities/BlHzQ693N9UdLVrR.htm)|Aonaurious|Aonaurious|libre|
|[bLI1JMpI2GXQT5AC.htm](deities/bLI1JMpI2GXQT5AC.htm)|Abraxas|Abraxas|libre|
|[BlLaiS1cWDhmXcBf.htm](deities/BlLaiS1cWDhmXcBf.htm)|Rull|Rull|libre|
|[BLNXYqvzmLgjMq2p.htm](deities/BLNXYqvzmLgjMq2p.htm)|Sun Wukong|Sun Wukong|libre|
|[BNycwu3I21dTh4D9.htm](deities/BNycwu3I21dTh4D9.htm)|Sarenrae|Sarenrae|libre|
|[bOnXaO1xTvkvCBoN.htm](deities/bOnXaO1xTvkvCBoN.htm)|Treasures of the Eternal Delve|Trésors de l'Excavation Éternelle|libre|
|[boSRWIodYDIPlZ4W.htm](deities/boSRWIodYDIPlZ4W.htm)|Vermilion Mother|Mère Écarlate|libre|
|[BrbXGyzCeORNyJT5.htm](deities/BrbXGyzCeORNyJT5.htm)|Sifkesh|Sifkesh|libre|
|[BRqiW7ZvzhFDuiIw.htm](deities/BRqiW7ZvzhFDuiIw.htm)|Areshkagal|Areshkagal|libre|
|[BVF5L71JblkYUW5E.htm](deities/BVF5L71JblkYUW5E.htm)|Neith|Neith|libre|
|[Byw9omVSNwYKcauw.htm](deities/Byw9omVSNwYKcauw.htm)|Camazotz|Camazotz|libre|
|[BYx7QoQhCBUGlWpl.htm](deities/BYx7QoQhCBUGlWpl.htm)|Dolok Darkfur|Dolok Noirpelage|libre|
|[Bzfc5mIUfMxOnQWu.htm](deities/Bzfc5mIUfMxOnQWu.htm)|Dahak|Dahak|libre|
|[c9NFjRR8gr6CWpjl.htm](deities/c9NFjRR8gr6CWpjl.htm)|Black Butterfly|Papillon noir|libre|
|[CDkr6eXR1lgu7g6A.htm](deities/CDkr6eXR1lgu7g6A.htm)|Andoletta|Andoletta|libre|
|[CeBP002LcMfqgIRl.htm](deities/CeBP002LcMfqgIRl.htm)|Picoperi|Picopéri|libre|
|[CewJyduCTUyW7x2u.htm](deities/CewJyduCTUyW7x2u.htm)|Lissala (The Order of Virtue)|Lissala (L'Ordre de la Vertu)|libre|
|[CezU0sI58mzkvAz3.htm](deities/CezU0sI58mzkvAz3.htm)|Zursvaater|Zursvaater|libre|
|[cHAbE86pto5bvIWo.htm](deities/cHAbE86pto5bvIWo.htm)|Angazhan|Angazhan|libre|
|[ChiKlaNDNYgGqtF7.htm](deities/ChiKlaNDNYgGqtF7.htm)|Atrogine|Atrogine|libre|
|[cICflsnWTdwTtxx0.htm](deities/cICflsnWTdwTtxx0.htm)|The Pale Horse|Le Cheval Pâle|libre|
|[Cme0TBL9QeusAUKE.htm](deities/Cme0TBL9QeusAUKE.htm)|Mestama|Mestama|libre|
|[CnDEoo6WruQCXo1o.htm](deities/CnDEoo6WruQCXo1o.htm)|Lubaiko|Lubaiko|libre|
|[CnEpvcfRgtAp1V31.htm](deities/CnEpvcfRgtAp1V31.htm)|Immaculate Growth|Croissance Immaculée|libre|
|[cSakUTwOtxe6Y0rH.htm](deities/cSakUTwOtxe6Y0rH.htm)|Baekho|Baékho|libre|
|[ctTBqyrSRpfL33jE.htm](deities/ctTBqyrSRpfL33jE.htm)|Seramaydiel|Séramaydiel|libre|
|[CzHjFLx4hpIEIoWt.htm](deities/CzHjFLx4hpIEIoWt.htm)|Dispater|Dispater|libre|
|[d47XTSODGAKANu3d.htm](deities/d47XTSODGAKANu3d.htm)|Vineshvakhi|Vineshvakhi|libre|
|[d56paSkcwvll2OhR.htm](deities/d56paSkcwvll2OhR.htm)|Abadar|Abadar|libre|
|[D8lFcjLj5sd25BRk.htm](deities/D8lFcjLj5sd25BRk.htm)|Shapes of the Fading Luster|Formes de l'Éclat Fané|libre|
|[dcbf3pBSNa2q1Kkq.htm](deities/dcbf3pBSNa2q1Kkq.htm)|Dhalavei|Dhalavei|libre|
|[DEKKbg6riAdL2ARf.htm](deities/DEKKbg6riAdL2ARf.htm)|Hei Feng|Hei Feng|libre|
|[DFxbPmQLOqcmbPTz.htm](deities/DFxbPmQLOqcmbPTz.htm)|Tanagaar|Tanagaar|libre|
|[DikXombRpmC8P1Lc.htm](deities/DikXombRpmC8P1Lc.htm)|Xiquiripat|Xiquiripat|libre|
|[DJ7LDFTtns6YFodT.htm](deities/DJ7LDFTtns6YFodT.htm)|Ashava|Ashava|libre|
|[dkKnv37c8RBPblgV.htm](deities/dkKnv37c8RBPblgV.htm)|Ah Pook|Ah Pook|libre|
|[Dm8qlzpUj7g82QFv.htm](deities/Dm8qlzpUj7g82QFv.htm)|Erecura|Érécura|libre|
|[DN0urKdPPqhXFzTq.htm](deities/DN0urKdPPqhXFzTq.htm)|Zibik|Zibik|libre|
|[DnBpqbMjRe2TwVZF.htm](deities/DnBpqbMjRe2TwVZF.htm)|Uirch|Uirch|libre|
|[dQb1y00PCsWIFcX4.htm](deities/dQb1y00PCsWIFcX4.htm)|Olheon|Olhéon|libre|
|[DWIGqLu8iQnZYS26.htm](deities/DWIGqLu8iQnZYS26.htm)|Vapula|Vapula|libre|
|[E1dUhoSEf2XfklU2.htm](deities/E1dUhoSEf2XfklU2.htm)|Mother Vulture|Mère Vautour|libre|
|[e6IGAMPvaGqwZPkI.htm](deities/e6IGAMPvaGqwZPkI.htm)|Phlegyas|Phlégyas|libre|
|[eayZ9moMzAGotO01.htm](deities/eayZ9moMzAGotO01.htm)|Rivethun|Rivethun|libre|
|[EbiJVJN86Q5SfDOt.htm](deities/EbiJVJN86Q5SfDOt.htm)|Nephthys|Néphthys|libre|
|[EDmAlxCXJHv8rZs2.htm](deities/EDmAlxCXJHv8rZs2.htm)|Iggeret|Iggeret|libre|
|[EE8sNM4JraOPN669.htm](deities/EE8sNM4JraOPN669.htm)|Cernunnos|Cernunnos|libre|
|[eErEiVmBqltYXFJ6.htm](deities/eErEiVmBqltYXFJ6.htm)|Iomedae|Iomédae|libre|
|[EfHlurXdqkdRXaqv.htm](deities/EfHlurXdqkdRXaqv.htm)|Set|Seth|libre|
|[EFyDjlH7XjGq7Erd.htm](deities/EFyDjlH7XjGq7Erd.htm)|Isis|Isis|libre|
|[ENzMYuzBZGIujSMx.htm](deities/ENzMYuzBZGIujSMx.htm)|Treerazer|Fléau des arbres|libre|
|[eTzCA17qD0Tabq3i.htm](deities/eTzCA17qD0Tabq3i.htm)|Dammar|Dammar|libre|
|[EUOAV02rHz96jMPh.htm](deities/EUOAV02rHz96jMPh.htm)|Whispering Way|Voie du murmure|libre|
|[eyGEoggFlTEUQmvJ.htm](deities/eyGEoggFlTEUQmvJ.htm)|Bes|Bès|libre|
|[F7Y83d8keV9CAhP2.htm](deities/F7Y83d8keV9CAhP2.htm)|Vale|Vale|libre|
|[f890Kbsk5dxrmU5h.htm](deities/f890Kbsk5dxrmU5h.htm)|Dramindyr|Dramindyr|libre|
|[f8fQIrsOdKVucT1G.htm](deities/f8fQIrsOdKVucT1G.htm)|Telvrys|Telvrys (homme-vert)|libre|
|[FeMWwhaVbaoqu3q6.htm](deities/FeMWwhaVbaoqu3q6.htm)|Reshmit of the Heavy Voice|Reshmit Voix forte (Déchu)|libre|
|[fFoqdiTV2YuITZSd.htm](deities/fFoqdiTV2YuITZSd.htm)|Valani|Valani|libre|
|[FHZ2DZEJzjrPfyjH.htm](deities/FHZ2DZEJzjrPfyjH.htm)|Mhar|Mhar|libre|
|[FktAD5GCv60OdSAB.htm](deities/FktAD5GCv60OdSAB.htm)|Myr|Myr|libre|
|[FREFauCaKxzGNuxE.htm](deities/FREFauCaKxzGNuxE.htm)|Xoveron|Xovéron|libre|
|[fTJLgTfEMZreJ19r.htm](deities/fTJLgTfEMZreJ19r.htm)|Milani|Milani|libre|
|[fTs56epghXwclPcX.htm](deities/fTs56epghXwclPcX.htm)|Stag Mother of the Forest of Stones|Mère Élaphe de la Forêt de pierres|libre|
|[fuPTR1laXO6EionS.htm](deities/fuPTR1laXO6EionS.htm)|Grandmother Spider|Grand-mère araignée|libre|
|[fZ5caXxpdV1MErsa.htm](deities/fZ5caXxpdV1MErsa.htm)|Cihua Coatl|Cihua Coatl|libre|
|[g1AVEGi8QibhS007.htm](deities/g1AVEGi8QibhS007.htm)|Walkena|Walkéna|libre|
|[g1VxllMjs2YRIJAm.htm](deities/g1VxllMjs2YRIJAm.htm)|Moloch|Moloch|libre|
|[G5g4Xa0KjKA2T65I.htm](deities/G5g4Xa0KjKA2T65I.htm)|The Spirit Wall|Frontière de la Force|libre|
|[GBdxyS79vTozcv3P.htm](deities/GBdxyS79vTozcv3P.htm)|The Laborer's Bastion|Le Bastion du travailleur|libre|
|[GEBaoeiv5CB9h2Bn.htm](deities/GEBaoeiv5CB9h2Bn.htm)|Ragathiel|Ragathiel|libre|
|[GFDsfRLFINnaHDTw.htm](deities/GFDsfRLFINnaHDTw.htm)|Kagia|Kagia|libre|
|[giAsBYiL2omr0x2L.htm](deities/giAsBYiL2omr0x2L.htm)|Qi Zhong|Qi Zhong|libre|
|[gpS3hv7LP2IRwEHT.htm](deities/gpS3hv7LP2IRwEHT.htm)|Sorrow's Sword|Épée du chagrin|libre|
|[gsFaxWHbZmc1moY6.htm](deities/gsFaxWHbZmc1moY6.htm)|Nalinivati|Nalinivati|libre|
|[GUmYLD7PGmE2TOx6.htm](deities/GUmYLD7PGmE2TOx6.htm)|Wadjet|Wadjet|libre|
|[gutbzYTZvRTnq4on.htm](deities/gutbzYTZvRTnq4on.htm)|Adanye|Adanye|libre|
|[GuUn4gElGNAT3Rbc.htm](deities/GuUn4gElGNAT3Rbc.htm)|Wulgren|Wulgren|libre|
|[gUyP9Ir8FTaZeV9D.htm](deities/gUyP9Ir8FTaZeV9D.htm)|Orgesh|Orgesh|libre|
|[GX6PF9ifR47Xyjg9.htm](deities/GX6PF9ifR47Xyjg9.htm)|Norgorber|Norgorber|libre|
|[gXHoxuuiAD54LnWV.htm](deities/gXHoxuuiAD54LnWV.htm)|Cyth-V'sug|Cyth-V'sug|libre|
|[gxwmzxUWBOhdgFSN.htm](deities/gxwmzxUWBOhdgFSN.htm)|Shelyn|Shélyn|libre|
|[Gy78DCwfY4ltBGI6.htm](deities/Gy78DCwfY4ltBGI6.htm)|Husk|Dépouille (Déchu)|libre|
|[GYmwlpH8zou0Mkap.htm](deities/GYmwlpH8zou0Mkap.htm)|Ristrentho|Ristrentho|libre|
|[H1VrZ1QX0d2aPXT8.htm](deities/H1VrZ1QX0d2aPXT8.htm)|Dammerich|Dammerich|libre|
|[H3qCK0Qq1bK0uM9o.htm](deities/H3qCK0Qq1bK0uM9o.htm)|Light of the Everlasting Flame|Lumière de la Flamme Éternelle|libre|
|[hAoVzkeFjTwh5Ykl.htm](deities/hAoVzkeFjTwh5Ykl.htm)|Shumunue|Shumunue|libre|
|[HbuynQRCjGKtbhcC.htm](deities/HbuynQRCjGKtbhcC.htm)|The Lost Prince|Le prince perdu|libre|
|[HCBsEO4bxbAxSWEN.htm](deities/HCBsEO4bxbAxSWEN.htm)|Yrmidar|Yrmidar|libre|
|[HD7g8T8ioy1EH3M8.htm](deities/HD7g8T8ioy1EH3M8.htm)|Bolka|Bolka|libre|
|[hDNLCRnDjYqmPYv6.htm](deities/hDNLCRnDjYqmPYv6.htm)|Embaral|Embaral|libre|
|[hhV2g6aU8jKLEca1.htm](deities/hhV2g6aU8jKLEca1.htm)|Narakaas|Narakaas|libre|
|[hLq1IF3mT8zOfUaE.htm](deities/hLq1IF3mT8zOfUaE.htm)|Kaldemash|Kaldemash|libre|
|[HR563VdVbW8aUdqM.htm](deities/HR563VdVbW8aUdqM.htm)|The Curtain Call|Le rappel|libre|
|[hT2prXTvejvNoCPB.htm](deities/hT2prXTvejvNoCPB.htm)|Ananshea|Ananshéa|libre|
|[HtRxo4J3elFteIqu.htm](deities/HtRxo4J3elFteIqu.htm)|Arazni|Arazni|libre|
|[hV4OjuhTOFG7jUB4.htm](deities/hV4OjuhTOFG7jUB4.htm)|Dalenydra|Dalenydra|libre|
|[hv7poWIKVN1AT51p.htm](deities/hv7poWIKVN1AT51p.htm)|Razmir|Razmir|libre|
|[hWTTS0e7J9hJfsp3.htm](deities/hWTTS0e7J9hJfsp3.htm)|Matravash|Matravash|libre|
|[i79OCvsFlDGqYjTk.htm](deities/i79OCvsFlDGqYjTk.htm)|Il'Surrish|Il'Surrish|libre|
|[IAGkKOJ0Pu0Bous3.htm](deities/IAGkKOJ0Pu0Bous3.htm)|Onos|Onos|libre|
|[iB7v8EkkQzCmdZ5X.htm](deities/iB7v8EkkQzCmdZ5X.htm)|Vonymos|Vonymos|libre|
|[idAu7puhPp3kFTnl.htm](deities/idAu7puhPp3kFTnl.htm)|Lorthact|Culte de Lorthact|libre|
|[iEt6idGIEPXY2HzV.htm](deities/iEt6idGIEPXY2HzV.htm)|Ydajisk|Ydajisk|libre|
|[iFF8oXwt6yePcwko.htm](deities/iFF8oXwt6yePcwko.htm)|Amaznen|Amaznen|libre|
|[Ig2ZSxvJ0K7F2Bb7.htm](deities/Ig2ZSxvJ0K7F2Bb7.htm)|Isph-Aun-Vuln|Isph-Aun-Vuln|libre|
|[Ij1KRM8ufkEjGnR3.htm](deities/Ij1KRM8ufkEjGnR3.htm)|Ranginori|Ranginori|libre|
|[iJyhAFmHOLZBvWnJ.htm](deities/iJyhAFmHOLZBvWnJ.htm)|Sivanah|Sivanah|libre|
|[IK2AL3bL6htrrnnC.htm](deities/IK2AL3bL6htrrnnC.htm)|Skode|Skode|libre|
|[iKgERiIc8rt3fatf.htm](deities/iKgERiIc8rt3fatf.htm)|Nyarlathotep (Haunter in the Dark)|Nyarlathotep (Celui qui hante les ténèbres)|libre|
|[iMRXc519Uepf8yH3.htm](deities/iMRXc519Uepf8yH3.htm)|Magdh|Magdh|libre|
|[ItYptsfjV8XvfhOV.htm](deities/ItYptsfjV8XvfhOV.htm)|Lahkgya|Lahkgya|libre|
|[Iwgr8NVomfEuX4WR.htm](deities/Iwgr8NVomfEuX4WR.htm)|The Devourer|Le Dévoreur|libre|
|[IXLzDSMGndFb5rGY.htm](deities/IXLzDSMGndFb5rGY.htm)|Falayna|Falayna|libre|
|[iXvyp9rGnqvFfy4q.htm](deities/iXvyp9rGnqvFfy4q.htm)|Kazutal|Kazutal|libre|
|[IY6fWfuIwSSIBb0Y.htm](deities/IY6fWfuIwSSIBb0Y.htm)|Atropos|Atropos|libre|
|[IZcdF1G8ZsnMotaM.htm](deities/IZcdF1G8ZsnMotaM.htm)|Pazuzu|Pazuzu|libre|
|[J33L1nYGD06FcxYF.htm](deities/J33L1nYGD06FcxYF.htm)|Vulot|Vulot|libre|
|[JcIf4uwKLxEWdplI.htm](deities/JcIf4uwKLxEWdplI.htm)|Kostchtchie|Kostchtchie|libre|
|[jCryzaFDLRL192Vv.htm](deities/jCryzaFDLRL192Vv.htm)|Venkelvore|Venkelvore|libre|
|[JDbrj8aKkLzdn8BU.htm](deities/JDbrj8aKkLzdn8BU.htm)|Atheists and Free Agents|Athéistes et agents libres|libre|
|[JeGR8YCpbmQAhxhF.htm](deities/JeGR8YCpbmQAhxhF.htm)|Weight of the World|Le Poids du Monde|libre|
|[JEZ6ZZc2aCzWyu8P.htm](deities/JEZ6ZZc2aCzWyu8P.htm)|Haborym|Haborym|libre|
|[JFyAEoCo32fl3b74.htm](deities/JFyAEoCo32fl3b74.htm)|Aminara|Aminara (homme-vert)|libre|
|[JgDqFUB1NtQkGmzp.htm](deities/JgDqFUB1NtQkGmzp.htm)|Otolmens|Otolmens|libre|
|[jGENfggMNVR9ObAb.htm](deities/jGENfggMNVR9ObAb.htm)|Zipacna|Zipacna|libre|
|[JgqH3BhuEuA4Zyqs.htm](deities/JgqH3BhuEuA4Zyqs.htm)|Desna|Desna|libre|
|[jL1qiVOZBKXETtVD.htm](deities/jL1qiVOZBKXETtVD.htm)|Nocticula|Nocticula|libre|
|[JMRGy8Fb1ZYeSbcK.htm](deities/JMRGy8Fb1ZYeSbcK.htm)|Vudravati|Vudravati|libre|
|[JndhOIHnQoMwnsw3.htm](deities/JndhOIHnQoMwnsw3.htm)|The Godclaw|Le dieu griffu|libre|
|[jrPmEfubRDzMKbxl.htm](deities/jrPmEfubRDzMKbxl.htm)|Arundhat|Arundhat|libre|
|[JXAg05NcjHZbDqxl.htm](deities/JXAg05NcjHZbDqxl.htm)|Aesocar|Aésocar|libre|
|[JxigqtJO2juuBh8V.htm](deities/JxigqtJO2juuBh8V.htm)|Radiant Prism|Le Prisme Radieux|libre|
|[JzaazIXXvAW02Cip.htm](deities/JzaazIXXvAW02Cip.htm)|The Lady of the North Star|La Dame de l'étoile polaire|libre|
|[K1tzUClsozKcdpIe.htm](deities/K1tzUClsozKcdpIe.htm)|Halcamora|Halcamora|libre|
|[K4s8a6gI1OmuWhMk.htm](deities/K4s8a6gI1OmuWhMk.htm)|Emmeton Galardaria|Emmeton Galardaria|libre|
|[KB6O6dvwJj7wuwO7.htm](deities/KB6O6dvwJj7wuwO7.htm)|Urban Prosperity|Prospérité urbaine|libre|
|[kBuCFUp4KYJEnMLk.htm](deities/kBuCFUp4KYJEnMLk.htm)|Zjar-Tovan|Zjar-Tovan|libre|
|[KDFujU3Yug6AwCOz.htm](deities/KDFujU3Yug6AwCOz.htm)|Arshea|Arshéa|libre|
|[KIbuRCeRdfOwxUXX.htm](deities/KIbuRCeRdfOwxUXX.htm)|Vavaalrav|Vavaalrav|libre|
|[KIEgpcmHYtFu1dpg.htm](deities/KIEgpcmHYtFu1dpg.htm)|Imot|Imot|libre|
|[kiTcZxmKxGPCwDpg.htm](deities/kiTcZxmKxGPCwDpg.htm)|Kalekot|Kalekot|libre|
|[kKdvt2m8MdsJ5aFb.htm](deities/kKdvt2m8MdsJ5aFb.htm)|Charg|Charg|libre|
|[KPrtgoIcgiBSKzS3.htm](deities/KPrtgoIcgiBSKzS3.htm)|The Freeing Flame|La flamme libératrice|libre|
|[kqO3mcoq1EVRSYxF.htm](deities/kqO3mcoq1EVRSYxF.htm)|Good Neighbors|Les Bons Voisins|libre|
|[KT7HKL7vbZEwbzX4.htm](deities/KT7HKL7vbZEwbzX4.htm)|Green Faith|La Verte religion|libre|
|[kvMjvTyMNODBA1N2.htm](deities/kvMjvTyMNODBA1N2.htm)|Yhidothrus|Yhidothrus|libre|
|[KWVdoAm3b01M8Lcp.htm](deities/KWVdoAm3b01M8Lcp.htm)|Mephistopheles|Méphistophélès|libre|
|[lDYntNcMZ8AKdlIU.htm](deities/lDYntNcMZ8AKdlIU.htm)|Zarongel|Zarongel|libre|
|[LGOkWwN6AZhLiZpl.htm](deities/LGOkWwN6AZhLiZpl.htm)|Touch of the Sun|Caresse du soleil|libre|
|[LIqW0cgGWFQln1Lk.htm](deities/LIqW0cgGWFQln1Lk.htm)|Scal|Scal|libre|
|[LJr99kbbzzgh4IZm.htm](deities/LJr99kbbzzgh4IZm.htm)|Izuyaku|Izuyaku|libre|
|[LLMTMgARSTlO7S9U.htm](deities/LLMTMgARSTlO7S9U.htm)|Zyphus|Zyphus|libre|
|[lPU0bl88a7cWWuyu.htm](deities/lPU0bl88a7cWWuyu.htm)|Zugero|Zugéro|libre|
|[LqJkd54Vtq9yMduS.htm](deities/LqJkd54Vtq9yMduS.htm)|Cong|Cong|libre|
|[lSciGZGcbU9PJxQy.htm](deities/lSciGZGcbU9PJxQy.htm)|Irez|Irèz|libre|
|[lUFMwdv1UOyzFhIM.htm](deities/lUFMwdv1UOyzFhIM.htm)|Likha|Likha|libre|
|[lvCryIEcfy7XIbAc.htm](deities/lvCryIEcfy7XIbAc.htm)|Ulon|Ulon|libre|
|[lZ74P8kUA1ti808w.htm](deities/lZ74P8kUA1ti808w.htm)|Prophecies of Kalistrade|Prohéties de Kalistrade|libre|
|[M3eb94Nd6uXti2IK.htm](deities/M3eb94Nd6uXti2IK.htm)|Daikitsu|Daikitsu|libre|
|[m6UYxDnwGkOo8IY5.htm](deities/m6UYxDnwGkOo8IY5.htm)|Thisamet|Thisamet|libre|
|[MbJ93Nw39h2cBYi5.htm](deities/MbJ93Nw39h2cBYi5.htm)|Lao Shu Po|Lao Shu Po|libre|
|[McUqN13BGLTpYWCg.htm](deities/McUqN13BGLTpYWCg.htm)|Green Man|Homme vert|libre|
|[mebdNVKeKuTmDDHj.htm](deities/mebdNVKeKuTmDDHj.htm)|Xhamen-Dor|Xhamen-dor|libre|
|[mIoXpTRhcCs4PTdN.htm](deities/mIoXpTRhcCs4PTdN.htm)|Pahti Coatl|Pahti Coatl|libre|
|[mm1j6UbHqFCI8fwB.htm](deities/mm1j6UbHqFCI8fwB.htm)|Erastil|Érastil|libre|
|[Mum7kJFrNOpVrxxP.htm](deities/Mum7kJFrNOpVrxxP.htm)|Laivatiniel|Laivatiniel|libre|
|[Mv6uTJLh3VGEoGLH.htm](deities/Mv6uTJLh3VGEoGLH.htm)|Nivi Rhombodazzle|Nivi Rhomboéblouissante|libre|
|[mwqdXCX19j7ammfh.htm](deities/mwqdXCX19j7ammfh.htm)|Thuskchoon|Thuskchoon|libre|
|[N8a76dkGfUvxvSW0.htm](deities/N8a76dkGfUvxvSW0.htm)|Trudd|Trudd|libre|
|[nawnLEk0rr7BgF1K.htm](deities/nawnLEk0rr7BgF1K.htm)|Teshallas|Teshallas|libre|
|[neoMBrfYuNsmQagi.htm](deities/neoMBrfYuNsmQagi.htm)|Aroden|Aroden|libre|
|[NkKdZor1FAePE5z9.htm](deities/NkKdZor1FAePE5z9.htm)|Velgaas|Velgaas|libre|
|[nsiDGdgbAzkTQzOt.htm](deities/nsiDGdgbAzkTQzOt.htm)|Magrim|Magrim|libre|
|[nuqIimigyt986WLE.htm](deities/nuqIimigyt986WLE.htm)|The Lantern King|Le Roi Lanterne|libre|
|[nuvfUC4TcOvFbKxx.htm](deities/nuvfUC4TcOvFbKxx.htm)|Osiris|Osiris|libre|
|[NXmqXck4jUfSY2ns.htm](deities/NXmqXck4jUfSY2ns.htm)|Nyarlathotep (The Veiled Voice)|Nyarlathotep (La Voix voilée)|libre|
|[o8ayCv3LAHWlu0vQ.htm](deities/o8ayCv3LAHWlu0vQ.htm)|Apsu|Apsu|libre|
|[o9tK1T28LNEVzpFV.htm](deities/o9tK1T28LNEVzpFV.htm)|Ragdya|Ragdya|libre|
|[Oci540vcXkJKR0gW.htm](deities/Oci540vcXkJKR0gW.htm)|The Enlightened Scholar's Path|La Voie de l'érudit éclairé|libre|
|[oduHa5yyTisFsIRD.htm](deities/oduHa5yyTisFsIRD.htm)|God Calling|Appel-dieu|libre|
|[oiEo6tDP3gICHtfZ.htm](deities/oiEo6tDP3gICHtfZ.htm)|Barbatos|Barbatos|libre|
|[oJPwMa9xG8oMnC14.htm](deities/oJPwMa9xG8oMnC14.htm)|Apollyon|Apollyon|libre|
|[OkEJdnT0HyjLjrg9.htm](deities/OkEJdnT0HyjLjrg9.htm)|Khepri|Khépri|libre|
|[oKuNQPNRHGong4jo.htm](deities/oKuNQPNRHGong4jo.htm)|Bifrons|Bifrons|libre|
|[OLHByk9LF3QG2x41.htm](deities/OLHByk9LF3QG2x41.htm)|Xsistaid|Xsistaid|libre|
|[OlVCyRgOaPJji4qt.htm](deities/OlVCyRgOaPJji4qt.htm)|Chohar|Chohar|libre|
|[omUWo1TQ6PDm5oeW.htm](deities/omUWo1TQ6PDm5oeW.htm)|Mugura and Nrithu|Mugura et Nrithu|libre|
|[Ons4JwstcXOlkaRF.htm](deities/Ons4JwstcXOlkaRF.htm)|The Perplexing Jest|La Plaisanterie Perplexe|libre|
|[OoPq9MtnAcrHBumx.htm](deities/OoPq9MtnAcrHBumx.htm)|Telastmar|Télastmar|libre|
|[oRvuLq3o2FxXizt9.htm](deities/oRvuLq3o2FxXizt9.htm)|Ghlaunder|Ghlaunder|libre|
|[OS9vZh5GYrNHahZq.htm](deities/OS9vZh5GYrNHahZq.htm)|Zogmugot|Zogmugot|libre|
|[oV5cnlPJQS8JfMIg.htm](deities/oV5cnlPJQS8JfMIg.htm)|Cosmic Caravan|Caravane cosmique|libre|
|[ox0AaGLIyzCUrre2.htm](deities/ox0AaGLIyzCUrre2.htm)|Srikalis, Sritaming, and Sribaril|Srikalis, Sritaming et Sribaril|libre|
|[oxORi6rZmqZNdJyJ.htm](deities/oxORi6rZmqZNdJyJ.htm)|Yig|Yig|libre|
|[oyp2E685VsQFKMXi.htm](deities/oyp2E685VsQFKMXi.htm)|Dranngvit|Dranngvit|libre|
|[p4TbCVOwHB4ccmT4.htm](deities/p4TbCVOwHB4ccmT4.htm)|The Endless Road|La route sans fin|officielle|
|[p4tkVSULWGHhYPGU.htm](deities/p4tkVSULWGHhYPGU.htm)|Shivaska|Shivaska|libre|
|[pafYqAGj47TmGBib.htm](deities/pafYqAGj47TmGBib.htm)|Verilorn|Vérilorn|libre|
|[pCtJONJlhAugUXaG.htm](deities/pCtJONJlhAugUXaG.htm)|Ahriman|Ahriman|libre|
|[pEm6tEGOhUbnwX4p.htm](deities/pEm6tEGOhUbnwX4p.htm)|Inna|Inna|libre|
|[PFp4Sdp2TDDKD62l.htm](deities/PFp4Sdp2TDDKD62l.htm)|Thamir|Thamir|libre|
|[PIMd4yRGj8XQgGbW.htm](deities/PIMd4yRGj8XQgGbW.htm)|The Divine Dare|Le Défi divin|libre|
|[pm8H2DbtrHM01KNV.htm](deities/pm8H2DbtrHM01KNV.htm)|Naderi|Nadéri|libre|
|[Pq9PT7dqRrQ3nnNx.htm](deities/Pq9PT7dqRrQ3nnNx.htm)|Talons of the Godclaw|Serres du Dieu Griffu|libre|
|[pqrmviqboqjeS4z8.htm](deities/pqrmviqboqjeS4z8.htm)|Eyes That Watch|Yeux veilleurs (Déchu)|libre|
|[psUfw455soFiUNlF.htm](deities/psUfw455soFiUNlF.htm)|Norns|Les Nornes|libre|
|[pVQzTL5elNTxvffu.htm](deities/pVQzTL5elNTxvffu.htm)|The Green Mother|La Mère verte|libre|
|[PYQnJe4ZXPzEnukx.htm](deities/PYQnJe4ZXPzEnukx.htm)|Nyarlathotep (The Faceless Sphinx)|Nyarlathotep (Le Sphinx sans Visage)|libre|
|[q4SyvGotD0LprYar.htm](deities/q4SyvGotD0LprYar.htm)|Cixyron|Cixyron|libre|
|[q5Rov9T1KTA0nJnm.htm](deities/q5Rov9T1KTA0nJnm.htm)|Vorasha|Vorasha|libre|
|[q8DsNpcH49PxObcd.htm](deities/q8DsNpcH49PxObcd.htm)|Breath of the Endless Sky|Souffle du Ciel Infini|libre|
|[QArqJ387HeYgGZxG.htm](deities/QArqJ387HeYgGZxG.htm)|Gravelady's Guard|La garde de la Dame des tombes|libre|
|[qfbZbNGQiiJ9hciP.htm](deities/qfbZbNGQiiJ9hciP.htm)|Dachzerul|Dachzérul|libre|
|[QibpLccM5bLUMopj.htm](deities/QibpLccM5bLUMopj.htm)|Lymnieris|Lymniéris|libre|
|[QlDnoOwf0PppymTn.htm](deities/QlDnoOwf0PppymTn.htm)|Hanspur|Hanspur|libre|
|[qmAMERMn2FvvvQwc.htm](deities/qmAMERMn2FvvvQwc.htm)|Furcas|Furcas|libre|
|[qMwLAXcjZ16qkMml.htm](deities/qMwLAXcjZ16qkMml.htm)|Malthus|Malthus|libre|
|[qqqrbq4uCrelQvgH.htm](deities/qqqrbq4uCrelQvgH.htm)|Irori|Irori|libre|
|[Qr8BqULcaBAiIKUY.htm](deities/Qr8BqULcaBAiIKUY.htm)|Urazra|Urazra|libre|
|[QRkcFciCOmFoxF1B.htm](deities/QRkcFciCOmFoxF1B.htm)|Asmodeus|Asmodeus|libre|
|[QTvR7ZqUrNMG56Nr.htm](deities/QTvR7ZqUrNMG56Nr.htm)|The Readied Strike|Le Coup Préparé|libre|
|[QZD0u1jxwz0kj8uI.htm](deities/QZD0u1jxwz0kj8uI.htm)|Pharasma|Pharasma|libre|
|[R0DvoLO8WbYIYhb3.htm](deities/R0DvoLO8WbYIYhb3.htm)|Atreia|Atréia|libre|
|[r5vooG7NfvA9prJK.htm](deities/r5vooG7NfvA9prJK.htm)|Kurgess|Kurgess|libre|
|[r6SFB1PzKsagDXff.htm](deities/r6SFB1PzKsagDXff.htm)|Lady Nanbyo|Dame Nanbyo|libre|
|[ra7mnjAZcEuJxI37.htm](deities/ra7mnjAZcEuJxI37.htm)|Shei|Sheï|libre|
|[RbslST0RZPXj6Eby.htm](deities/RbslST0RZPXj6Eby.htm)|Ashukharma|Ashukharma|libre|
|[rdA9jbvUZdT1Zr8Q.htm](deities/rdA9jbvUZdT1Zr8Q.htm)|Mahja Firehair|Mahja Cheveux de feu|libre|
|[rDTXcBJ8LyqA2TkL.htm](deities/rDTXcBJ8LyqA2TkL.htm)|Szuriel|Szuriel|libre|
|[remyY1BwhEVmBwya.htm](deities/remyY1BwhEVmBwya.htm)|Belial|Bélial|libre|
|[rji8c4S3dZJIy4VU.htm](deities/rji8c4S3dZJIy4VU.htm)|Wheels of Innovation|Rouages de l'Innovation|libre|
|[RlOuyadtreI7pht1.htm](deities/RlOuyadtreI7pht1.htm)|The Resplendent Court|La Cour Resplendissante|libre|
|[RnS5P7zX0HRPsDnt.htm](deities/RnS5P7zX0HRPsDnt.htm)|Ferrumnestra|Ferrumnestra|libre|
|[RoBL67dQIQgIahtY.htm](deities/RoBL67dQIQgIahtY.htm)|Ptah|Ptah|libre|
|[rRuxBgC58PxrLzRv.htm](deities/rRuxBgC58PxrLzRv.htm)|Horus|Horus|libre|
|[RUzMtIQal0drPwQY.htm](deities/RUzMtIQal0drPwQY.htm)|Gogunta|Gogunta|libre|
|[rXtm3Jbs7BBrxjk4.htm](deities/rXtm3Jbs7BBrxjk4.htm)|Anogetz|Anogetz|libre|
|[Rz7C08TWmj87WNR6.htm](deities/Rz7C08TWmj87WNR6.htm)|Shadow Cabinet|Conseil de l'Ombre|libre|
|[RzTd9PmgnfHNlFVf.htm](deities/RzTd9PmgnfHNlFVf.htm)|Ng|Ng|libre|
|[s1PgIQ8Oi35oS2Et.htm](deities/s1PgIQ8Oi35oS2Et.htm)|Shoanti Animism|Animisme shoanti|libre|
|[s81N9hxffmRuy3ik.htm](deities/s81N9hxffmRuy3ik.htm)|Kerkamoth|Kerkamoth|libre|
|[s9opv94lGDRKnw6W.htm](deities/s9opv94lGDRKnw6W.htm)|Shizuru|Shizuru|libre|
|[SAuFz101iP6VVwvw.htm](deities/SAuFz101iP6VVwvw.htm)|Nurgal|Nurgal|libre|
|[SCGdJmm7Wzle9jFP.htm](deities/SCGdJmm7Wzle9jFP.htm)|Zohls|Zohls|libre|
|[SCMlQDcoU1QcF3AS.htm](deities/SCMlQDcoU1QcF3AS.htm)|Lysianassa|Lysianassa|libre|
|[SeD3n4VMk5EQJ2CI.htm](deities/SeD3n4VMk5EQJ2CI.htm)|Ceyannan|Ceyannan|libre|
|[sezTnLBBCYv3ZrZN.htm](deities/sezTnLBBCYv3ZrZN.htm)|Jukha|Jukha|libre|
|[sILda7I8Ak20Ioj9.htm](deities/sILda7I8Ak20Ioj9.htm)|Azathoth|Azathoth|libre|
|[smGfeOk8Jgcz0Qkv.htm](deities/smGfeOk8Jgcz0Qkv.htm)|Immonhiel|Immonhiel|libre|
|[sNjPI1iaS8Z43YJf.htm](deities/sNjPI1iaS8Z43YJf.htm)|Mammon|Mammon|libre|
|[soiVh53y9RAeSLEk.htm](deities/soiVh53y9RAeSLEk.htm)|Casandalee|Casandalee|libre|
|[SUaGdvQXxSAQAw03.htm](deities/SUaGdvQXxSAQAw03.htm)|Yog-Sothoth|Yog-Sothoth|libre|
|[swwwP7eVmlNuWTb7.htm](deities/swwwP7eVmlNuWTb7.htm)|Gozreh|Gozreh|libre|
|[sZrdzh0ANQnZGXJb.htm](deities/sZrdzh0ANQnZGXJb.htm)|Torag|Torag|libre|
|[t6Vn590vTLDlVUdo.htm](deities/t6Vn590vTLDlVUdo.htm)|Liisglan|Liisglan|libre|
|[tb0oADxOoMhyrYRA.htm](deities/tb0oADxOoMhyrYRA.htm)|Yamatsumi|Yamatsumi|libre|
|[tcvobm8O84qv0aO5.htm](deities/tcvobm8O84qv0aO5.htm)|Gruhastha|Gruhastha|libre|
|[tcXrtRYCOr2EmgGB.htm](deities/tcXrtRYCOr2EmgGB.htm)|Charon|Charon|libre|
|[tk7gRxXsqvBwQrUF.htm](deities/tk7gRxXsqvBwQrUF.htm)|Grundinnar|Grundinnar|libre|
|[Tn8rVCvsjrPyzKWC.htm](deities/Tn8rVCvsjrPyzKWC.htm)|Nameless|Sans-Nom|libre|
|[tnAmR9WxWoEehDZw.htm](deities/tnAmR9WxWoEehDZw.htm)|Cthulhu|Cthulhu|libre|
|[TpAMy8tb5LrRgvqN.htm](deities/TpAMy8tb5LrRgvqN.htm)|Hathor|Hathor|libre|
|[Tqk5xJpDKI2MXO1R.htm](deities/Tqk5xJpDKI2MXO1R.htm)|Bokrug|Bokrug|libre|
|[TvxsVRztDlGcWpcI.htm](deities/TvxsVRztDlGcWpcI.htm)|Sobek|Sobek|libre|
|[TW7Q6O928YWNME9r.htm](deities/TW7Q6O928YWNME9r.htm)|Kitumu|Kitumu|libre|
|[u0H9wcgttBI9Ef1j.htm](deities/u0H9wcgttBI9Ef1j.htm)|Kelinahat|Kélinahat|libre|
|[u2tnG7fKkZ2KHMcT.htm](deities/u2tnG7fKkZ2KHMcT.htm)|Suyuddha|Suyuddha|libre|
|[u3YfcJh1C4w6n75j.htm](deities/u3YfcJh1C4w6n75j.htm)|Raumya|Raumya|libre|
|[u5z1YYlpuBGdTzS9.htm](deities/u5z1YYlpuBGdTzS9.htm)|Keltheald|Keltheald|libre|
|[ub0PJ5QkrdoGzzb2.htm](deities/ub0PJ5QkrdoGzzb2.htm)|Luhar|Luhar|libre|
|[Up2Wv097dRjoClSw.htm](deities/Up2Wv097dRjoClSw.htm)|Varg|Varg|libre|
|[upRQcwjjaksc1g7h.htm](deities/upRQcwjjaksc1g7h.htm)|Enkaar|Enkaar, le Prisonnier malformé (Déchu)|libre|
|[uTRswfDyjndlQZJ1.htm](deities/uTRswfDyjndlQZJ1.htm)|Neshen|Neshen|libre|
|[UUcAqU3KeJSMNxGF.htm](deities/UUcAqU3KeJSMNxGF.htm)|Barzahk|Barzahk|libre|
|[uXkMGx0OD896ZQlT.htm](deities/uXkMGx0OD896ZQlT.htm)|Alglenweis|Alglenweis|libre|
|[uydTiyN5AzhBnQY8.htm](deities/uydTiyN5AzhBnQY8.htm)|Ardad Lili|Ardad Lili|libre|
|[uYpxTi4byHc5w78R.htm](deities/uYpxTi4byHc5w78R.htm)|Yuelral|Yuelral|libre|
|[Uz7DDxMvWjSXUrmX.htm](deities/Uz7DDxMvWjSXUrmX.htm)|Sithhud|Sithud|libre|
|[v03ImN8VkF3cz7MI.htm](deities/v03ImN8VkF3cz7MI.htm)|Uskyeria|Uskyéria|libre|
|[v1fsLP6nTkiy5ghB.htm](deities/v1fsLP6nTkiy5ghB.htm)|Mazludeh|Mazludeh|libre|
|[V5PbbkZUP9N7kl6h.htm](deities/V5PbbkZUP9N7kl6h.htm)|Kabriri|Kabriri|libre|
|[v67fHklTZ6LoU54q.htm](deities/v67fHklTZ6LoU54q.htm)|Cayden Cailean|Cayden Cailéan|libre|
|[vEMCpm7iidycRT5D.htm](deities/vEMCpm7iidycRT5D.htm)|Brigh|Brigh|libre|
|[VEyowuhC4FsBnG8c.htm](deities/VEyowuhC4FsBnG8c.htm)|Folgrit|Folgrit|libre|
|[VG51y4FbTLdeGHZB.htm](deities/VG51y4FbTLdeGHZB.htm)|The Path of the Heavens|Le chemin des cieux|libre|
|[vjG96N3HYdK7xvh6.htm](deities/vjG96N3HYdK7xvh6.htm)|Teki Stronggut|Téki Stronggut|libre|
|[Vk1FM54rJx1RHtUA.htm](deities/Vk1FM54rJx1RHtUA.htm)|Seafarers' Hope|L'espoir des marins|libre|
|[VlgaeJlpl2ubDv4a.htm](deities/VlgaeJlpl2ubDv4a.htm)|Nergal|Nergal|libre|
|[vnmoAGURp8oWCY6R.htm](deities/vnmoAGURp8oWCY6R.htm)|Soralyon|Soralyon|libre|
|[VRPVppdQ4W0oXeB1.htm](deities/VRPVppdQ4W0oXeB1.htm)|Alocer|Alocer|libre|
|[vSUz97Y29l1O50bC.htm](deities/vSUz97Y29l1O50bC.htm)|Count Ranalc|Comte Ranalc|libre|
|[VVUS4JWxWDd5ByJC.htm](deities/VVUS4JWxWDd5ByJC.htm)|Lurlup|Lurlup|libre|
|[VvxEvEKhCGRwrMJp.htm](deities/VvxEvEKhCGRwrMJp.htm)|Imbrex|Imbrex|libre|
|[Vzxm5FyIA40b2OSP.htm](deities/Vzxm5FyIA40b2OSP.htm)|Pulura|Pulura|libre|
|[w4fDrwL8VIqJDPfR.htm](deities/w4fDrwL8VIqJDPfR.htm)|Uvuko|Uvuko|libre|
|[W4QGkdn9V5ACRpkh.htm](deities/W4QGkdn9V5ACRpkh.htm)|Monad|La Monade|libre|
|[w66gCNiZ7dslqG8K.htm](deities/w66gCNiZ7dslqG8K.htm)|Lissala|Lissala|libre|
|[w7B4HD5XOFaLb3ZG.htm](deities/w7B4HD5XOFaLb3ZG.htm)|Hearth and Harvest|Foyer et Récolte|libre|
|[W7GPA9QiSrcbN7bX.htm](deities/W7GPA9QiSrcbN7bX.htm)|Chamiaholom|Chamiaholom|libre|
|[w8Pfl5syV0vaEuTM.htm](deities/w8Pfl5syV0vaEuTM.htm)|Ussharassim|Ussharassim|libre|
|[w9vtltygWNG4eZDR.htm](deities/w9vtltygWNG4eZDR.htm)|Jin Li|Jin Li|libre|
|[WfqLY2Fm1LGCcG5X.htm](deities/WfqLY2Fm1LGCcG5X.htm)|Sangpotshi|Sangpotshi|libre|
|[wjXNXFWBLbJaeZQR.htm](deities/wjXNXFWBLbJaeZQR.htm)|Ketephys|Kétéphys|libre|
|[wkDOeK7ENz1ra8IC.htm](deities/wkDOeK7ENz1ra8IC.htm)|Besmara|Besmara|libre|
|[WloVxo6HXJdS4YTt.htm](deities/WloVxo6HXJdS4YTt.htm)|Groetus|Groétus|libre|
|[WO9UsCY41oTtC2G0.htm](deities/WO9UsCY41oTtC2G0.htm)|Pillars of Knowledge|Piliers de la connaissance|libre|
|[woDEgBs1MznhzpJ4.htm](deities/woDEgBs1MznhzpJ4.htm)|Ra|Râ|libre|
|[WOiSJ4IWjaRsSgRO.htm](deities/WOiSJ4IWjaRsSgRO.htm)|Geryon|Géryon|libre|
|[wtXlsIbYRjwuCexX.htm](deities/wtXlsIbYRjwuCexX.htm)|Tjasse|Tjasse|libre|
|[WvoMr86bSSYxXKtI.htm](deities/WvoMr86bSSYxXKtI.htm)|Hastur|Hastur|libre|
|[wXRJe0QsebyqQ1L0.htm](deities/wXRJe0QsebyqQ1L0.htm)|Ozranvial|Ozranvial|libre|
|[wyKJRFrEJdRFtsha.htm](deities/wyKJRFrEJdRFtsha.htm)|Tolte Coatl|Tolte Coatl|libre|
|[XaaDDVS4sEJd4m99.htm](deities/XaaDDVS4sEJd4m99.htm)|Dajermube|Dajermube|libre|
|[Xb1N5YfxboJH26Jh.htm](deities/Xb1N5YfxboJH26Jh.htm)|Angradd|Angradd|libre|
|[XFqgtfxc47LPFjLZ.htm](deities/XFqgtfxc47LPFjLZ.htm)|The Pandemonia|La Pandémonia|libre|
|[XHDlP7o1jNz9851v.htm](deities/XHDlP7o1jNz9851v.htm)|Diomazul|Diomazul|libre|
|[xLbSMedLDFyb8sFw.htm](deities/xLbSMedLDFyb8sFw.htm)|Laudinmio|Laudimio|libre|
|[XP0SPRL92tAPMV0K.htm](deities/XP0SPRL92tAPMV0K.htm)|Acavna|Acavna|libre|
|[xpny4ZzlExIbGs6a.htm](deities/xpny4ZzlExIbGs6a.htm)|Haggakal|Haggakal|libre|
|[xTD0XFaguBX6QQ2L.htm](deities/xTD0XFaguBX6QQ2L.htm)|Selket|Selket|libre|
|[xu05XGotyT5st56H.htm](deities/xu05XGotyT5st56H.htm)|Lady Jingxi|Dame Jingxi|libre|
|[XxIxEaW8NG952Bc0.htm](deities/XxIxEaW8NG952Bc0.htm)|Grasping Iovett|Iovett la Prise (Déchu)|libre|
|[xyHhtBdNQGyldsKQ.htm](deities/xyHhtBdNQGyldsKQ.htm)|Lalaci|Lalaci|libre|
|[XYJupIPx0GW4s7eu.htm](deities/XYJupIPx0GW4s7eu.htm)|Ragadahn|Ragadahn|libre|
|[Y0TY7oerRZ0pLbOB.htm](deities/Y0TY7oerRZ0pLbOB.htm)|Skrymir|Skrymir|libre|
|[y3RlZ3FnbZN8wv8j.htm](deities/y3RlZ3FnbZN8wv8j.htm)|Demon Bringers|Porteurs de démons|libre|
|[Y6tbAWlcccGlUH0z.htm](deities/Y6tbAWlcccGlUH0z.htm)|Haagenti|Haagenti|libre|
|[y7aYkVd72Kh6ydWi.htm](deities/y7aYkVd72Kh6ydWi.htm)|Thremyr|Thrémyr|libre|
|[y93whGnGZjFyw5A0.htm](deities/y93whGnGZjFyw5A0.htm)|Vildeis|Vildéïs|libre|
|[YeUhlOt3c98ocwjZ.htm](deities/YeUhlOt3c98ocwjZ.htm)|The Offering Plate|Plateau d'Offrandes|libre|
|[yIfdsL1788boOOYk.htm](deities/yIfdsL1788boOOYk.htm)|Sky Keepers|Gardiens du ciel|libre|
|[yiKle2URZ43KYVKQ.htm](deities/yiKle2URZ43KYVKQ.htm)|Chamidu|Chamidu|libre|
|[ykqWA3pS0045DRs4.htm](deities/ykqWA3pS0045DRs4.htm)|Lorris|Lorris|libre|
|[YLftgCdyOyF22Gt6.htm](deities/YLftgCdyOyF22Gt6.htm)|Cormigus|Cormigus|libre|
|[YnOxUUJz1dzocG6N.htm](deities/YnOxUUJz1dzocG6N.htm)|Sicva|Sicva|libre|
|[yX5U1Uvdug9i08nU.htm](deities/yX5U1Uvdug9i08nU.htm)|Conqueror Worm|Ver conquérant|libre|
|[YXmeBdOZfWgUljW1.htm](deities/YXmeBdOZfWgUljW1.htm)|Ylimancha|Ylimancha|libre|
|[yxttKoBEx93T3dWl.htm](deities/yxttKoBEx93T3dWl.htm)|Alazhra|Alazhra|libre|
|[ZfN7jkK6boU1FuiS.htm](deities/ZfN7jkK6boU1FuiS.htm)|Zon-Kuthon|Zon-Kuthon|libre|
|[zgM2jsxw52HjALV0.htm](deities/zgM2jsxw52HjALV0.htm)|The Deliberate Journey|Le Voyage délibéré|libre|
|[zhPhYge7VJbIlJvA.htm](deities/zhPhYge7VJbIlJvA.htm)|Kols|Kols|libre|
|[zhXBavaHMCwnVpYA.htm](deities/zhXBavaHMCwnVpYA.htm)|Fortune's Fate|Destin de la Fortune|libre|
|[Zi1UARhode3FSFLr.htm](deities/Zi1UARhode3FSFLr.htm)|General Susumu|Général Susumu|libre|
|[zI3qO0KCBKxe0Ui1.htm](deities/zI3qO0KCBKxe0Ui1.htm)|Kzininn|Kzininn (homme-vert)|libre|
|[zlJGKpeLBwajP91o.htm](deities/zlJGKpeLBwajP91o.htm)|Gyronna|Gyronna|libre|
|[zQbb1kAQJkPfiDIh.htm](deities/zQbb1kAQJkPfiDIh.htm)|Chavazvug|Chavazvug|libre|
|[zRmAXj7FSlrMfDeA.htm](deities/zRmAXj7FSlrMfDeA.htm)|Bastet|Bastet|libre|
|[ZynnoQJFz12FD0Xn.htm](deities/ZynnoQJFz12FD0Xn.htm)|Wards of the Pharaoh|Glyphes du pharaon|libre|
|[zzhYFcShT9JoE2Mp.htm](deities/zzhYFcShT9JoE2Mp.htm)|Shax|Shax|libre|
