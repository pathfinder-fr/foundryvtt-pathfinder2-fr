# État de la traduction (pathfinder-monster-core)

 * **libre**: 490
 * **changé**: 2


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[fgZx6VJkVZ26GFyI.htm](pathfinder-monster-core/fgZx6VJkVZ26GFyI.htm)|Bandersnatch|Bandersnatch (Tane)|changé|
|[oKaewcA4gu3fbSVP.htm](pathfinder-monster-core/oKaewcA4gu3fbSVP.htm)|Ifrit|Ifrit (Génie, Feu)|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[00uNOPsU5VognIcB.htm](pathfinder-monster-core/00uNOPsU5VognIcB.htm)|Yamaraj|Yamaraje (Psychopompe)|libre|
|[085JMzkQqhNEWWWL.htm](pathfinder-monster-core/085JMzkQqhNEWWWL.htm)|Akhana|Akhana (Aéon)|libre|
|[0Dsy2I3mu86Czjm0.htm](pathfinder-monster-core/0Dsy2I3mu86Czjm0.htm)|Awakened Tree|Arbre éveillé (Arboréen)|libre|
|[0H54u83vZ1w3xHcD.htm](pathfinder-monster-core/0H54u83vZ1w3xHcD.htm)|Tor Linnorm|Linnorm des roches|libre|
|[0jSkyGbnMiN6kzwH.htm](pathfinder-monster-core/0jSkyGbnMiN6kzwH.htm)|Zyss Serpentfolk|Zyss (Homme-serpent)|libre|
|[0PNjXE892kU89LyV.htm](pathfinder-monster-core/0PNjXE892kU89LyV.htm)|Soulbound Doll (Calm)|Poupée des âmes (Calme)|libre|
|[0qaGLx8ads9blcfS.htm](pathfinder-monster-core/0qaGLx8ads9blcfS.htm)|Lamia Matriarch|Matriarche lamie|libre|
|[0rm0UDbXvwg4sSxQ.htm](pathfinder-monster-core/0rm0UDbXvwg4sSxQ.htm)|Arboreal Warden|Gardien arboréen (Arboréen)|libre|
|[15TyWlbDQWNjMKeL.htm](pathfinder-monster-core/15TyWlbDQWNjMKeL.htm)|Greater Hell Hound|Molosse infernal supérieur|libre|
|[1a5faH5CCtFfbQHO.htm](pathfinder-monster-core/1a5faH5CCtFfbQHO.htm)|Cauthooj|Cauthooj|libre|
|[1AZoRkLec47xrTqY.htm](pathfinder-monster-core/1AZoRkLec47xrTqY.htm)|Living Waterfall|Cascade vivante (Élémentaire, eau)|libre|
|[1Qqu3b4J4aJYEQOX.htm](pathfinder-monster-core/1Qqu3b4J4aJYEQOX.htm)|Hyaenodon|Hyaenodon|libre|
|[1uVwkGlqYzyWaDMy.htm](pathfinder-monster-core/1uVwkGlqYzyWaDMy.htm)|Viper|Vipère (Serpent)|libre|
|[1x0BdpVQLX7o3rrA.htm](pathfinder-monster-core/1x0BdpVQLX7o3rrA.htm)|Elephant|Éléphant|libre|
|[1ZRHjuGOluR4IUrs.htm](pathfinder-monster-core/1ZRHjuGOluR4IUrs.htm)|Pukwudgie|Pukwudgie (Fée)|libre|
|[2A7jnl9tpmeTBkQy.htm](pathfinder-monster-core/2A7jnl9tpmeTBkQy.htm)|Sargassum Heap|Marasme de sargasses|libre|
|[2jtRdYo9ey7osDUH.htm](pathfinder-monster-core/2jtRdYo9ey7osDUH.htm)|Ofalth|Ofalth|libre|
|[2k0JX3RFTVRf0KS2.htm](pathfinder-monster-core/2k0JX3RFTVRf0KS2.htm)|Wererat|Rat-garou|libre|
|[2KUe0AMwqswKivRo.htm](pathfinder-monster-core/2KUe0AMwqswKivRo.htm)|Ghoul Soldier|Soldat goule|libre|
|[2mg30nJR6P3HJDSd.htm](pathfinder-monster-core/2mg30nJR6P3HJDSd.htm)|Vampire Bat Swarm|Nuée de chauves-souris vampires|libre|
|[2p7MEk4SdXfLbzxO.htm](pathfinder-monster-core/2p7MEk4SdXfLbzxO.htm)|Scarecrow|Épouvantail|libre|
|[2QFhhm6cGqqaVlyK.htm](pathfinder-monster-core/2QFhhm6cGqqaVlyK.htm)|Omen Dragon (Ancient, Spellcaster)|Dragon des présages (Vénérable, incantateur)|libre|
|[2rMLYkUR47ZCQMUg.htm](pathfinder-monster-core/2rMLYkUR47ZCQMUg.htm)|Crocodile|Crocodile|libre|
|[2SixuEUfKpEyfOEY.htm](pathfinder-monster-core/2SixuEUfKpEyfOEY.htm)|Frost Drake|Drake du froid|libre|
|[3IPLvfez8DzwwNqm.htm](pathfinder-monster-core/3IPLvfez8DzwwNqm.htm)|Terotricus|Térotricus|libre|
|[3nUt7cW8fqE5IpyE.htm](pathfinder-monster-core/3nUt7cW8fqE5IpyE.htm)|Envyspawn|Rejeton de l'envie|libre|
|[3QyqHIuAXE3YVLUh.htm](pathfinder-monster-core/3QyqHIuAXE3YVLUh.htm)|Phistophilus|Phistophilus (Diable)|libre|
|[3t1z62LREP4nnIDr.htm](pathfinder-monster-core/3t1z62LREP4nnIDr.htm)|Duskwalker Ghost Hunter|Chasseur de fantômes crépusculaire|libre|
|[3vkQLclK0z4LhAh7.htm](pathfinder-monster-core/3vkQLclK0z4LhAh7.htm)|Shemhazian|Shemhazian (Démon de la mutilation)|libre|
|[3wzvIQuxIaJU3JVO.htm](pathfinder-monster-core/3wzvIQuxIaJU3JVO.htm)|Empyreal Dragon (Young, Spellcaster)|Dragon empyréen (Jeune, incantateur)|libre|
|[3zqVFz4FfRYv5Sgy.htm](pathfinder-monster-core/3zqVFz4FfRYv5Sgy.htm)|Hadrosaurid|Hadrosaure (Dinosaure)|libre|
|[4Ejgj6p1LAu1RAN3.htm](pathfinder-monster-core/4Ejgj6p1LAu1RAN3.htm)|Living Landslide|Éboulement vivant (Élémentaire, terre)|libre|
|[4GXL5FLmN7IxPzVC.htm](pathfinder-monster-core/4GXL5FLmN7IxPzVC.htm)|Gigantopithecus|Gigantopithécus|libre|
|[4ISm82EYQeOndynw.htm](pathfinder-monster-core/4ISm82EYQeOndynw.htm)|Giant Tarantula|Tarentule géante|libre|
|[4MoqBCDQA6FR1sPw.htm](pathfinder-monster-core/4MoqBCDQA6FR1sPw.htm)|Dryad|Dryade (Nymphe)|libre|
|[4n0nhynklrs2iHUJ.htm](pathfinder-monster-core/4n0nhynklrs2iHUJ.htm)|Earth Scamp|Salopin de terre|libre|
|[59eY2OOFIuuj4f72.htm](pathfinder-monster-core/59eY2OOFIuuj4f72.htm)|Soulbound Doll (Timid)|Poupée des âmes (Timide)|libre|
|[5iqkL9Me5164H7NY.htm](pathfinder-monster-core/5iqkL9Me5164H7NY.htm)|Wyvern|Vouivre|libre|
|[5KstOkXabrOlZaKR.htm](pathfinder-monster-core/5KstOkXabrOlZaKR.htm)|Nosoi|Nosoi (Psychopompe)|libre|
|[5lbXQ6MSn4AndglU.htm](pathfinder-monster-core/5lbXQ6MSn4AndglU.htm)|Horned Dragon (Young)|Dragon cornu (Jeune)|libre|
|[5meW7DytYnF7Iq2V.htm](pathfinder-monster-core/5meW7DytYnF7Iq2V.htm)|Greater Shadow|Ombre majeure|libre|
|[5NnKgWCNbEzbSEN8.htm](pathfinder-monster-core/5NnKgWCNbEzbSEN8.htm)|Soulbound Doll (Impish)|Poupée des âmes (Malicieuse)|libre|
|[5SFDHViyTCZ47TR5.htm](pathfinder-monster-core/5SFDHViyTCZ47TR5.htm)|Scorpion Swarm|Nuée de scorpions|libre|
|[5vBG8a8dnJfmVd3Y.htm](pathfinder-monster-core/5vBG8a8dnJfmVd3Y.htm)|Xulgath Warrior|Guerrier xulgath|libre|
|[63gNd2JNUDcAjYzo.htm](pathfinder-monster-core/63gNd2JNUDcAjYzo.htm)|Umbral Gnome Scout|Éclaireur gnome ombral|libre|
|[6aaBmiOgqZ5h2IhW.htm](pathfinder-monster-core/6aaBmiOgqZ5h2IhW.htm)|Giant Wasp|Guêpe géante|libre|
|[6EgJNAJss45TQqpa.htm](pathfinder-monster-core/6EgJNAJss45TQqpa.htm)|Smilodon|Smilodon|libre|
|[6FW3MILVXOy6mMrY.htm](pathfinder-monster-core/6FW3MILVXOy6mMrY.htm)|Stone Bulwark|Rempart de pierre (Golem)|libre|
|[6IBevff4lJOFl539.htm](pathfinder-monster-core/6IBevff4lJOFl539.htm)|Empyreal Dragon (Young)|Dragon empyréen (Jeune)|libre|
|[6K4RWus85o8iqy0t.htm](pathfinder-monster-core/6K4RWus85o8iqy0t.htm)|Grizzly Bear|Grizzly|libre|
|[6KF6TQvLHHpE0uAM.htm](pathfinder-monster-core/6KF6TQvLHHpE0uAM.htm)|Lion|Lion|libre|
|[6mdq7UV9gFXAEOpJ.htm](pathfinder-monster-core/6mdq7UV9gFXAEOpJ.htm)|Great Cyclops|Grand Cyclope|libre|
|[6uiRT0sNk6TMQ309.htm](pathfinder-monster-core/6uiRT0sNk6TMQ309.htm)|Horned Dragon (Ancient, Spellcaster)|Dragon cornu (Ancien, incantateur)|libre|
|[6UrgCT8MwC8CeGbu.htm](pathfinder-monster-core/6UrgCT8MwC8CeGbu.htm)|Brimorak|Brimorak (Démon incendiaire)|libre|
|[6VOX4biqPtAE7y3g.htm](pathfinder-monster-core/6VOX4biqPtAE7y3g.htm)|Aeolaeka|Aéolaeka (Azata)|libre|
|[6WgkWAumJDId0vuJ.htm](pathfinder-monster-core/6WgkWAumJDId0vuJ.htm)|Soulbound Doll (Kind)|Poupée des âmes (Gentille)|libre|
|[6wPW2dvpt86Ou6bL.htm](pathfinder-monster-core/6wPW2dvpt86Ou6bL.htm)|Rat Swarm|Nuée de rats|libre|
|[6yTmbDaZmrkXUJ4t.htm](pathfinder-monster-core/6yTmbDaZmrkXUJ4t.htm)|Umbral Gnome Rockwarden|Gardien des roches gnome ombral|libre|
|[702acAJ1OPRBjcni.htm](pathfinder-monster-core/702acAJ1OPRBjcni.htm)|Grikkitog|Grikkitog|libre|
|[74TxKGaW7RPzTdbm.htm](pathfinder-monster-core/74TxKGaW7RPzTdbm.htm)|Vanth|Vanth (Psychopompe)|libre|
|[7ikLoTK1gQJ08YDR.htm](pathfinder-monster-core/7ikLoTK1gQJ08YDR.htm)|Conspirator Dragon (Ancient)|Dragon comploteur (Ancien)|libre|
|[7jwIapHl80UbnIWr.htm](pathfinder-monster-core/7jwIapHl80UbnIWr.htm)|Horned Dragon (Adult, Spellcaster)|Dragon cornu (Adulte, incantateur)|libre|
|[7Uf4Q9w3dCDEV30e.htm](pathfinder-monster-core/7Uf4Q9w3dCDEV30e.htm)|Omen Dragon (Young)|Dragon des présages (Jeune)|libre|
|[7v1gykqjBO1YHDfu.htm](pathfinder-monster-core/7v1gykqjBO1YHDfu.htm)|Axiomite|Axiomite (Aéon)|libre|
|[7w1w7VTnIWMBdFux.htm](pathfinder-monster-core/7w1w7VTnIWMBdFux.htm)|Xulgath Leader|Chef xulgath|libre|
|[893mp411SdYren3H.htm](pathfinder-monster-core/893mp411SdYren3H.htm)|Frost Giant|Géant du givre|libre|
|[8CYHgj4SV2LvlaUs.htm](pathfinder-monster-core/8CYHgj4SV2LvlaUs.htm)|Barghest|Barghest|libre|
|[8JbgbskxcU0EloJX.htm](pathfinder-monster-core/8JbgbskxcU0EloJX.htm)|Mirage Dragon (Young)|Dragon mirage (Jeune)|libre|
|[8w0LwQHkip8nzFo0.htm](pathfinder-monster-core/8w0LwQHkip8nzFo0.htm)|Norn|Norne|libre|
|[9Bl1ua3uLqodv47s.htm](pathfinder-monster-core/9Bl1ua3uLqodv47s.htm)|Cacodaemon|Cacodaémon|libre|
|[9cBuzDV8seJqhNKJ.htm](pathfinder-monster-core/9cBuzDV8seJqhNKJ.htm)|Bugbear Prowler|Maraudeur gobelours|libre|
|[9InGpxq5xbbHaL9f.htm](pathfinder-monster-core/9InGpxq5xbbHaL9f.htm)|Sewer Ooze|Vase des égouts|libre|
|[9qERA2jk7Yv74Hqq.htm](pathfinder-monster-core/9qERA2jk7Yv74Hqq.htm)|Cassisian|Cassisien (Ange)|libre|
|[9qlccWxGFcQmgL3h.htm](pathfinder-monster-core/9qlccWxGFcQmgL3h.htm)|Doldrums Heap|Amoncellement de doldrums|libre|
|[9rXYGu7D3umIW1sv.htm](pathfinder-monster-core/9rXYGu7D3umIW1sv.htm)|Megalodon|Mégalodon|libre|
|[9sGgANyNFCKnu06t.htm](pathfinder-monster-core/9sGgANyNFCKnu06t.htm)|Twigjack|Hommes-brindilles (Fée)|libre|
|[9SNpsnwlzZjH4DQf.htm](pathfinder-monster-core/9SNpsnwlzZjH4DQf.htm)|Choral|Choeur (Ange)|libre|
|[9VMoTqyVaKc4ZR4H.htm](pathfinder-monster-core/9VMoTqyVaKc4ZR4H.htm)|Phantom Knight|Chevalier fantôme|libre|
|[9wNjq9BirBoxyJVH.htm](pathfinder-monster-core/9wNjq9BirBoxyJVH.htm)|Homunculus|Homoncule|libre|
|[9XccneFB5DmMHig0.htm](pathfinder-monster-core/9XccneFB5DmMHig0.htm)|Gnome Bard|barde gnome|libre|
|[9YPFqikAmURwcTEO.htm](pathfinder-monster-core/9YPFqikAmURwcTEO.htm)|Caligni Skulker|Rôdeur caligni|libre|
|[a4LgD3NrgkiINvru.htm](pathfinder-monster-core/a4LgD3NrgkiINvru.htm)|Quetzalcoatlus|Quetzalcoatl (Ptérosaure)|libre|
|[A4VgQIHsqJKssQOM.htm](pathfinder-monster-core/A4VgQIHsqJKssQOM.htm)|Hunting Spider|Araignée chasseresse|libre|
|[aCTOWweRtjucLmfe.htm](pathfinder-monster-core/aCTOWweRtjucLmfe.htm)|Con Rit|Cryptoïde|libre|
|[AcUb8s5fiktYw8Fx.htm](pathfinder-monster-core/AcUb8s5fiktYw8Fx.htm)|Caligni Hunter|Chasseur caligni|libre|
|[ADjEac8U6pNoPq6J.htm](pathfinder-monster-core/ADjEac8U6pNoPq6J.htm)|Diabolic Dragon (Young)|Dragon diabolique (Jeune)|libre|
|[aeOzCBpwnUVcpqxI.htm](pathfinder-monster-core/aeOzCBpwnUVcpqxI.htm)|Pugwampi|Pugwampi (Gremlin)|libre|
|[AFWmiIBJ7ypgydQD.htm](pathfinder-monster-core/AFWmiIBJ7ypgydQD.htm)|Dire Wolf|Loup sanguinaire|libre|
|[aj4ZJUULa7VoPYWy.htm](pathfinder-monster-core/aj4ZJUULa7VoPYWy.htm)|Satyr|Satyre|libre|
|[AJ5LuNMVPLCydryP.htm](pathfinder-monster-core/AJ5LuNMVPLCydryP.htm)|Giant Viper|Vipère géante (Serpent)|libre|
|[alPZcKVrHTcMdtIU.htm](pathfinder-monster-core/alPZcKVrHTcMdtIU.htm)|Yeti|Yéti|libre|
|[Ao8hNYvOeo2HbJ5p.htm](pathfinder-monster-core/Ao8hNYvOeo2HbJ5p.htm)|Quai Dau To|Quai Dau To|libre|
|[aWiTcxAySoYjUP6T.htm](pathfinder-monster-core/aWiTcxAySoYjUP6T.htm)|Plague Zombie|Zombie pestiféré|libre|
|[awrILpXoGooQKEui.htm](pathfinder-monster-core/awrILpXoGooQKEui.htm)|Omen Dragon (Adult)|Dragon des présages (Adulte)|libre|
|[aYDbWuOj66nve8r4.htm](pathfinder-monster-core/aYDbWuOj66nve8r4.htm)|Jungle Drake|Drake de la jungle|libre|
|[AZIG0COCaDBronJa.htm](pathfinder-monster-core/AZIG0COCaDBronJa.htm)|Cave Bear|Ours des cavernes|libre|
|[aZNYR8Zn18ljILfO.htm](pathfinder-monster-core/aZNYR8Zn18ljILfO.htm)|Pipefox|Serpent-renard|libre|
|[bb5Z4z4EHb5LbCLK.htm](pathfinder-monster-core/bb5Z4z4EHb5LbCLK.htm)|Leukodaemon|Leukodaémon|libre|
|[BCPpwj9uCiS7bF9C.htm](pathfinder-monster-core/BCPpwj9uCiS7bF9C.htm)|Pitborn Adept|Adepte né-de-la-fosse (Héritier des plans, Néphilim)|libre|
|[bdarrRaLjNmlYvNa.htm](pathfinder-monster-core/bdarrRaLjNmlYvNa.htm)|Soulbound Doll (Cruel)|Poupée des âmes (Cruelle)|libre|
|[Bgfu9IQfK1DyaXmt.htm](pathfinder-monster-core/Bgfu9IQfK1DyaXmt.htm)|Adamantine Dragon (Adult, Spellcaster)|Dragon adamantin (Adulte, Incantateur)|libre|
|[bgKwwvO0uDGD7XsG.htm](pathfinder-monster-core/bgKwwvO0uDGD7XsG.htm)|Kraken|Kraken|libre|
|[bGp2t0UteEYu3BGe.htm](pathfinder-monster-core/bGp2t0UteEYu3BGe.htm)|Pteranodon|Ptéranodon (Ptérosaure)|libre|
|[BIZfjoz8DZt75EDn.htm](pathfinder-monster-core/BIZfjoz8DZt75EDn.htm)|Kobold Warrior|Guerrier kobold|libre|
|[Bkr0soTDhQq1qjWx.htm](pathfinder-monster-core/Bkr0soTDhQq1qjWx.htm)|Hydra|Hydre|libre|
|[BlTEvlCUKPOfBYMR.htm](pathfinder-monster-core/BlTEvlCUKPOfBYMR.htm)|Nessari|Nessari (Diable)|libre|
|[BMpBXSq8T20WdUDl.htm](pathfinder-monster-core/BMpBXSq8T20WdUDl.htm)|War Pony|Poney de bataille|libre|
|[BN5Lb6IsQ9Wyu3rL.htm](pathfinder-monster-core/BN5Lb6IsQ9Wyu3rL.htm)|Wolf|Loup|libre|
|[BoXCGLACP9vuIZkZ.htm](pathfinder-monster-core/BoXCGLACP9vuIZkZ.htm)|Fungus Leshy|Léchi fongique|libre|
|[BPznJcLvfkpfeQ2q.htm](pathfinder-monster-core/BPznJcLvfkpfeQ2q.htm)|Guthallath|Guthallath|libre|
|[bsrQp0pLgvjJr6mC.htm](pathfinder-monster-core/bsrQp0pLgvjJr6mC.htm)|Revenant|Revenant|libre|
|[bveW59P2mTiiFVIt.htm](pathfinder-monster-core/bveW59P2mTiiFVIt.htm)|Dhampir Wizard|Mage dhampir|libre|
|[bVn0EUj4xrOWjtna.htm](pathfinder-monster-core/bVn0EUj4xrOWjtna.htm)|Chimera|Chimère|libre|
|[BvYVgvlTcRbim4Xb.htm](pathfinder-monster-core/BvYVgvlTcRbim4Xb.htm)|Fortune Dragon (Young, Spellcaster)|Dragon fortuné (Jeune, Incantateur)|libre|
|[BWm17BRQYGMLqtNe.htm](pathfinder-monster-core/BWm17BRQYGMLqtNe.htm)|Giant Scorpion|Scorpion géant|libre|
|[BxOlYmZiwLRpxGWp.htm](pathfinder-monster-core/BxOlYmZiwLRpxGWp.htm)|Dero Stalker|Traqueur déro|libre|
|[byShcvERXQVHNaoL.htm](pathfinder-monster-core/byShcvERXQVHNaoL.htm)|Venedaemon|Vénédaémon|libre|
|[C1gYuDSwTkTIkAcC.htm](pathfinder-monster-core/C1gYuDSwTkTIkAcC.htm)|Ratfolk Grenadier|Grenadier homme-rat|libre|
|[C1MfXltrIJRnFaZg.htm](pathfinder-monster-core/C1MfXltrIJRnFaZg.htm)|Tomb Jelly|Vase des tombes|libre|
|[C5o3vQKGHhGEsAuV.htm](pathfinder-monster-core/C5o3vQKGHhGEsAuV.htm)|Conspirator Dragon (Ancient, Spellcaster)|Dragon comploteur (Ancien, incantateur)|libre|
|[c5RzWCW3KsMqvO4h.htm](pathfinder-monster-core/c5RzWCW3KsMqvO4h.htm)|Empyreal Dragon (Adult, Spellcaster)|Dragon empyréen (Adulte, incantateur)|libre|
|[C9a1JvKRo43I1nx3.htm](pathfinder-monster-core/C9a1JvKRo43I1nx3.htm)|Aiuvarin Elementalist|Élementaliste aiuvarin|libre|
|[CAEdqhfZDfMLmEyS.htm](pathfinder-monster-core/CAEdqhfZDfMLmEyS.htm)|Faydhaan|Faydhaan (Génie, Eau)|libre|
|[ccAeM0tNgf5aVokj.htm](pathfinder-monster-core/ccAeM0tNgf5aVokj.htm)|Slothspawn|Rejeton de la paresse|libre|
|[Ccjg08wVRO43YlTf.htm](pathfinder-monster-core/Ccjg08wVRO43YlTf.htm)|Jann|Jann (Génie)|libre|
|[CFlx1tkRxKC9qAC7.htm](pathfinder-monster-core/CFlx1tkRxKC9qAC7.htm)|Animated Armor|Armure animée|libre|
|[CHVMOXznjUDcb3XP.htm](pathfinder-monster-core/CHVMOXznjUDcb3XP.htm)|Hippocampus|Hippocampe|libre|
|[ciLdEf6sld8h2a2j.htm](pathfinder-monster-core/ciLdEf6sld8h2a2j.htm)|Voidworm|Ver du néant (Protéen)|libre|
|[CIQgNHkiUtBQR2NJ.htm](pathfinder-monster-core/CIQgNHkiUtBQR2NJ.htm)|Snow Oni|Oni du froid|libre|
|[ClAXGpqu4ZsYuNle.htm](pathfinder-monster-core/ClAXGpqu4ZsYuNle.htm)|Tengu Sneak|Fureteur tengu|libre|
|[CLf9k9A9ApTAkZeL.htm](pathfinder-monster-core/CLf9k9A9ApTAkZeL.htm)|Riding Horse|Cheval de selle|libre|
|[clzHAfegb9Rn6DOe.htm](pathfinder-monster-core/clzHAfegb9Rn6DOe.htm)|Shadow Spawn|Rejeton d'ombre|libre|
|[cPRdGSSOmIIvAZVy.htm](pathfinder-monster-core/cPRdGSSOmIIvAZVy.htm)|Soulbound Doll (Jolly)|Poupée des âmes (Joviale)|libre|
|[CPzquDOPnlHyIoVF.htm](pathfinder-monster-core/CPzquDOPnlHyIoVF.htm)|Diabolic Dragon (Adult)|Dragon diabolique (Jeune)|libre|
|[Cq9Cg5dZ3I3cWXXR.htm](pathfinder-monster-core/Cq9Cg5dZ3I3cWXXR.htm)|Mirage Dragon (Adult)|Dragon mirage (Adulte)|libre|
|[Cqi8jadYpUTajIC6.htm](pathfinder-monster-core/Cqi8jadYpUTajIC6.htm)|Naiad|Naïade (Nymphe)|libre|
|[cRBVKMNukkRgELMs.htm](pathfinder-monster-core/cRBVKMNukkRgELMs.htm)|Azuretzi|Azuretzi (Protéen)|libre|
|[cvfIkEF6xmWn2soN.htm](pathfinder-monster-core/cvfIkEF6xmWn2soN.htm)|Gargoyle|Gargouille|libre|
|[CXoAvRxBt3bJ5IZQ.htm](pathfinder-monster-core/CXoAvRxBt3bJ5IZQ.htm)|Adamantine Dragon (Young)|Dragon adamantin (Jeune)|libre|
|[CYt04IKRQeiC9Ly9.htm](pathfinder-monster-core/CYt04IKRQeiC9Ly9.htm)|Ankylosaurus|Ankylosaure (Dinosaure)|libre|
|[CYyxzpymkIZxX4Zo.htm](pathfinder-monster-core/CYyxzpymkIZxX4Zo.htm)|Smaranava|Smaranava (Naga)|libre|
|[D2Pe38jYbeBxUaxU.htm](pathfinder-monster-core/D2Pe38jYbeBxUaxU.htm)|Nilith|Nilith|libre|
|[dbElCgziDp8ysxqR.htm](pathfinder-monster-core/dbElCgziDp8ysxqR.htm)|Fortune Dragon (Ancient)|Dragon fortuné (Vénérable)|libre|
|[DBTbqI9QQRtlJwWh.htm](pathfinder-monster-core/DBTbqI9QQRtlJwWh.htm)|Wight|Nécrophage|libre|
|[DEZNEMDhWXvv7BrT.htm](pathfinder-monster-core/DEZNEMDhWXvv7BrT.htm)|Irnakurse|Irnakurse (Distordu)|libre|
|[dfshMY9yEEXOUoMo.htm](pathfinder-monster-core/dfshMY9yEEXOUoMo.htm)|Quatoid|Quatoïde (Élémentaire, eau)|libre|
|[dFzTXkrpoOOdbzuW.htm](pathfinder-monster-core/dFzTXkrpoOOdbzuW.htm)|Vampire Servitor|Vampire, Serviteur|libre|
|[diU0V2M3LiMDMsS0.htm](pathfinder-monster-core/diU0V2M3LiMDMsS0.htm)|Witchwarg|Sorcewarg|libre|
|[DQbHmsmYuodcQ2KC.htm](pathfinder-monster-core/DQbHmsmYuodcQ2KC.htm)|Conspirator Dragon (Young)|Dragon comploteur (Jeune)|libre|
|[drcSWbCIWc7P4lKO.htm](pathfinder-monster-core/drcSWbCIWc7P4lKO.htm)|Army Ant Swarm|Nuée de fourmis soldats|libre|
|[dUI8N2AXDGcf3qRD.htm](pathfinder-monster-core/dUI8N2AXDGcf3qRD.htm)|Pachycephalosaurus|Pachycéphalosaure (Dinosaure)|libre|
|[dY1wqHKN6PV4oJ07.htm](pathfinder-monster-core/dY1wqHKN6PV4oJ07.htm)|Nuckelavee|Nuckelavee (Fée)|libre|
|[DZoKH4IXR4zeNxoa.htm](pathfinder-monster-core/DZoKH4IXR4zeNxoa.htm)|Horned Dragon (Ancient)|Dragon cornu (Ancien)|libre|
|[E2XL2egIA3QaSDBM.htm](pathfinder-monster-core/E2XL2egIA3QaSDBM.htm)|Flash Beetle|Scarabée-flash (Coléoptère)|libre|
|[ECe2DkOgSSqXHBqv.htm](pathfinder-monster-core/ECe2DkOgSSqXHBqv.htm)|Riding Dog|Chien de selle|libre|
|[ED9YZQYMGHuN5l8E.htm](pathfinder-monster-core/ED9YZQYMGHuN5l8E.htm)|Fortune Dragon (Adult, Spellcaster)|Dragon fortuné (Adulte, Incantateur)|libre|
|[EDYKUdYmilw3rgJg.htm](pathfinder-monster-core/EDYKUdYmilw3rgJg.htm)|Zombie Hulk|Mastodonte zombie|libre|
|[eFGPKbhix65FSG9u.htm](pathfinder-monster-core/eFGPKbhix65FSG9u.htm)|Hippogriff|Hippogriffe|libre|
|[EH84J9FedbK3ax50.htm](pathfinder-monster-core/EH84J9FedbK3ax50.htm)|Boggard Warrior|Guerrier bourbiérin|libre|
|[Ehtm5k9iBYTvSUcZ.htm](pathfinder-monster-core/Ehtm5k9iBYTvSUcZ.htm)|Pixie|Pixie (Esprit follet)|libre|
|[eI6rRuxIPSZcm9OC.htm](pathfinder-monster-core/eI6rRuxIPSZcm9OC.htm)|Hryngar Taskmaster|Prévôt hryngar|libre|
|[eke1AhiUjNPpyRhG.htm](pathfinder-monster-core/eke1AhiUjNPpyRhG.htm)|Gluttonyspawn|Rejeton de la gourmandise|libre|
|[eP4a0FAbus3tNTbc.htm](pathfinder-monster-core/eP4a0FAbus3tNTbc.htm)|Goliath Spider|Araignée goliath|libre|
|[ePJcp2ADOjJYYQ5Q.htm](pathfinder-monster-core/ePJcp2ADOjJYYQ5Q.htm)|Shining Child|Enfant lumineux|libre|
|[eqHTr6Wj1JadHSa1.htm](pathfinder-monster-core/eqHTr6Wj1JadHSa1.htm)|Noxious Needler|Inoculeur nocif (Golem)|libre|
|[EWYXtfIQYhUbWK4R.htm](pathfinder-monster-core/EWYXtfIQYhUbWK4R.htm)|Living Tar|Goudron vivant|libre|
|[ExVmw8bSvUd2wYkI.htm](pathfinder-monster-core/ExVmw8bSvUd2wYkI.htm)|War Horse|Cheval de bataille|libre|
|[EYiIQh526d0HLiDu.htm](pathfinder-monster-core/EYiIQh526d0HLiDu.htm)|Sea Hag|Guenaude marine|libre|
|[f15mNNhOT3aq66VQ.htm](pathfinder-monster-core/f15mNNhOT3aq66VQ.htm)|Krooth|Krooth|libre|
|[F2Xhn4rqPAj55w3O.htm](pathfinder-monster-core/F2Xhn4rqPAj55w3O.htm)|Boggard Swampseer|Devin des marais bourbiérin|libre|
|[F3ungAqpGjFotwUK.htm](pathfinder-monster-core/F3ungAqpGjFotwUK.htm)|Riding Pony|Poney de selle|libre|
|[f9fjSDdcBKkEPoIi.htm](pathfinder-monster-core/f9fjSDdcBKkEPoIi.htm)|Gourd Leshy|Léchi calebasse|libre|
|[FFnsYr2aIfDGnUVS.htm](pathfinder-monster-core/FFnsYr2aIfDGnUVS.htm)|Giant Frilled Lizard|Lézard à collerette géant|libre|
|[FH58AcRBZIfrHKvv.htm](pathfinder-monster-core/FH58AcRBZIfrHKvv.htm)|Skeletal Champion|Champion squelette|libre|
|[fLLKuOXwPq1Iq0U4.htm](pathfinder-monster-core/fLLKuOXwPq1Iq0U4.htm)|Goblin Warrior|Guerrier gobelin|libre|
|[Fn5ZKPwiaXm242Np.htm](pathfinder-monster-core/Fn5ZKPwiaXm242Np.htm)|Water Scamp|Salopin d'eau|libre|
|[fRjJkktWp7s8NBN7.htm](pathfinder-monster-core/fRjJkktWp7s8NBN7.htm)|Ugothol|Ugothol (Alghollthu)|libre|
|[fWAjkhQ0y50Eh2BT.htm](pathfinder-monster-core/fWAjkhQ0y50Eh2BT.htm)|Redcap|Bonnet rouge|libre|
|[FWg5qHuwoa0JfEMR.htm](pathfinder-monster-core/FWg5qHuwoa0JfEMR.htm)|Rekhep|Rekhep (Archon)|libre|
|[FWLTPQZzrVsokXHb.htm](pathfinder-monster-core/FWLTPQZzrVsokXHb.htm)|Halfling Street Watcher|Observateur urbain halfelin|libre|
|[G5OY8UMEOrbycZZc.htm](pathfinder-monster-core/G5OY8UMEOrbycZZc.htm)|Quelaunt|Quelaunt|libre|
|[gCmfPEfqS60BiuVP.htm](pathfinder-monster-core/gCmfPEfqS60BiuVP.htm)|Electric Eel|Anguille électrique|libre|
|[GdqPoQ2WWnuzEzbu.htm](pathfinder-monster-core/GdqPoQ2WWnuzEzbu.htm)|Fortune Dragon (Ancient, Spellcaster)|Dragon fortuné (Vénérable, Incantateur)|libre|
|[gGdIV6uUHzX23vz6.htm](pathfinder-monster-core/gGdIV6uUHzX23vz6.htm)|Lizardfolk Stargazer|Astrobservateur homme-lézard (Iruxi)|libre|
|[GJShyw6HgV25ywqU.htm](pathfinder-monster-core/GJShyw6HgV25ywqU.htm)|Umbral Gnome Warrior|Guerrier gnome ombral|libre|
|[GmKNb1UIjAkTMDOt.htm](pathfinder-monster-core/GmKNb1UIjAkTMDOt.htm)|Giylea|Giyléa (Archon)|libre|
|[GNwLVbfFx8EPz7xO.htm](pathfinder-monster-core/GNwLVbfFx8EPz7xO.htm)|Dullahan|Dullahan|libre|
|[gRy6Ci6zb1s4Nvy5.htm](pathfinder-monster-core/gRy6Ci6zb1s4Nvy5.htm)|Dryad Queen|Dryade souveraine (Hamadryade, nymphe)|libre|
|[Gu0cJHGwPd547OtC.htm](pathfinder-monster-core/Gu0cJHGwPd547OtC.htm)|Wraith|Âme-en-peine|libre|
|[GUjtk2h6Yj0NKifF.htm](pathfinder-monster-core/GUjtk2h6Yj0NKifF.htm)|Charnel Creation|Création de chair (Golem)|libre|
|[gWgMg7cARqOe82O6.htm](pathfinder-monster-core/gWgMg7cARqOe82O6.htm)|Griffon|Griffon|libre|
|[h3JksV9Idr9eZLkE.htm](pathfinder-monster-core/h3JksV9Idr9eZLkE.htm)|Werebear|Ours-garou|libre|
|[h4cMwW2K34KheWtD.htm](pathfinder-monster-core/h4cMwW2K34KheWtD.htm)|Pridespawn|Rejeton de l'orgueil|libre|
|[h7je8nkscJQ2Ac8j.htm](pathfinder-monster-core/h7je8nkscJQ2Ac8j.htm)|Wasp Swarm|Essaim de guêpes|libre|
|[H7z7VHzlHlEFev1r.htm](pathfinder-monster-core/H7z7VHzlHlEFev1r.htm)|Brine Shark|Requin de saumure (Élémentaire, eau)|libre|
|[H8lGFF3PKUv2yRL2.htm](pathfinder-monster-core/H8lGFF3PKUv2yRL2.htm)|Naiad Queen|Naïade souveraine (Nymphe)|libre|
|[H8Xq9hZP5bEEI3Hf.htm](pathfinder-monster-core/H8Xq9hZP5bEEI3Hf.htm)|Tabellia|Tabellia (Ange)|libre|
|[H9CHNiW18cRFocNO.htm](pathfinder-monster-core/H9CHNiW18cRFocNO.htm)|Globster|Globster (Vase)|libre|
|[heMHnYShW7TCJCJb.htm](pathfinder-monster-core/heMHnYShW7TCJCJb.htm)|Mirage Dragon (Ancient, Spellcaster)|Dragon mirage (Vénérable, Incantateur)|libre|
|[Hg7nCvltRBQOiijQ.htm](pathfinder-monster-core/Hg7nCvltRBQOiijQ.htm)|Werewolf|Loup-garou|libre|
|[hGYIynV60GGfg8Du.htm](pathfinder-monster-core/hGYIynV60GGfg8Du.htm)|Gancanagh|Gancanagh (Azata)|libre|
|[HKXa9E91WLy6dAZA.htm](pathfinder-monster-core/HKXa9E91WLy6dAZA.htm)|Phade|Phade (Élémentaire, air)|libre|
|[Hl1Nnnda8KSf0Obp.htm](pathfinder-monster-core/Hl1Nnnda8KSf0Obp.htm)|Chupacabra|Chupacabra|libre|
|[HLM55InRGOAUkqoH.htm](pathfinder-monster-core/HLM55InRGOAUkqoH.htm)|Pleroma|Pléroma (Aéon)|libre|
|[hmcXFrf8xwrYQIg6.htm](pathfinder-monster-core/hmcXFrf8xwrYQIg6.htm)|Kanya|Kanya (Azata)|libre|
|[HMfH0CI1CQCXB9Xr.htm](pathfinder-monster-core/HMfH0CI1CQCXB9Xr.htm)|Pegasus|Pégase|libre|
|[hSSe2FxlXKvjKsEw.htm](pathfinder-monster-core/hSSe2FxlXKvjKsEw.htm)|Astradaemon|Astradaémon|libre|
|[htK4dElL6YvFCLkz.htm](pathfinder-monster-core/htK4dElL6YvFCLkz.htm)|Forest Troll|Troll des forêts|libre|
|[httXfBPGseF9csXa.htm](pathfinder-monster-core/httXfBPGseF9csXa.htm)|Bugbear Tormentor|Tourmenteur gobelours|libre|
|[hvVFocY8r72XEaqQ.htm](pathfinder-monster-core/hvVFocY8r72XEaqQ.htm)|Omox|Omox (Démon de fange)|libre|
|[hXl3HV5uuCD1KJjU.htm](pathfinder-monster-core/hXl3HV5uuCD1KJjU.htm)|Diabolic Dragon (Adult, Spellcaster)|Dragon diabolique (Adulte, Incantateur)|libre|
|[hybmXlTxljL9L1dW.htm](pathfinder-monster-core/hybmXlTxljL9L1dW.htm)|Adamantine Dragon (Adult)|Dragon adamantin (Adulte)|libre|
|[I00S3LWnDJfCn4zv.htm](pathfinder-monster-core/I00S3LWnDJfCn4zv.htm)|Giant Animated Statue|Statue géante animée|libre|
|[i6gxQwKAPhOQZPgs.htm](pathfinder-monster-core/i6gxQwKAPhOQZPgs.htm)|Empyreal Dragon (Adult)|Dragon empyréen (Adulte)|libre|
|[IGzoFGlVbkit8hnH.htm](pathfinder-monster-core/IGzoFGlVbkit8hnH.htm)|Mukradi|Mukradi|libre|
|[iIJPJcDT8wlJ8z5M.htm](pathfinder-monster-core/iIJPJcDT8wlJ8z5M.htm)|Giant Rat|Rat géant|libre|
|[IjFlp5eVVTEg902W.htm](pathfinder-monster-core/IjFlp5eVVTEg902W.htm)|Dwarf Warrior|Guerrier nain|libre|
|[IKmiLv4ZNiLD10zv.htm](pathfinder-monster-core/IKmiLv4ZNiLD10zv.htm)|Omen Dragon (Young, Spellcaster)|Dragon des présages (Jeune, incantateur)|libre|
|[iLkQt8A99nQWUI8k.htm](pathfinder-monster-core/iLkQt8A99nQWUI8k.htm)|Ghoul Stalker|Traqueur goule|libre|
|[iNwaxIYuD0OTDNjJ.htm](pathfinder-monster-core/iNwaxIYuD0OTDNjJ.htm)|Lyrakien|Lyrakien (Azata)|libre|
|[ioi5L2CAM6q9aWKJ.htm](pathfinder-monster-core/ioi5L2CAM6q9aWKJ.htm)|Horned Dragon (Adult)|Dragon cornu (Adulte)|libre|
|[irl1wnfk4b83JWkY.htm](pathfinder-monster-core/irl1wnfk4b83JWkY.htm)|Desert Drake|Drake du désert|libre|
|[IrssIkWkW6fsbHJL.htm](pathfinder-monster-core/IrssIkWkW6fsbHJL.htm)|Dybbuk|Dybbouk|libre|
|[IuVoyT5lkzcWJ6sK.htm](pathfinder-monster-core/IuVoyT5lkzcWJ6sK.htm)|Clay Effigy|Effigie d'argile (Golem)|libre|
|[Iwk2qT4lVyrvoz3B.htm](pathfinder-monster-core/Iwk2qT4lVyrvoz3B.htm)|Gogiteth|Gogiteth|libre|
|[IxezJU9rIKKyd3LY.htm](pathfinder-monster-core/IxezJU9rIKKyd3LY.htm)|Merfolk Wavecaller|Sorcevague homme-poisson|libre|
|[iy5XBb2u4BOVxjtz.htm](pathfinder-monster-core/iy5XBb2u4BOVxjtz.htm)|Vrolikai|Vrolikai (Démon de mort)|libre|
|[IyhbcdTVmkV4pSju.htm](pathfinder-monster-core/IyhbcdTVmkV4pSju.htm)|Boar|Sanglier|libre|
|[JFmFuoOzCwc3hA1H.htm](pathfinder-monster-core/JFmFuoOzCwc3hA1H.htm)|Kobold Cavern Mage|Mage des cavernes kobold|libre|
|[JGwKk83oX4gTGlqe.htm](pathfinder-monster-core/JGwKk83oX4gTGlqe.htm)|Tiger|Tigre|libre|
|[jh3XmHoFtcGYkdJm.htm](pathfinder-monster-core/jh3XmHoFtcGYkdJm.htm)|Magma Worm|Ver de magma|libre|
|[jkp7THTZMc0ivN8Y.htm](pathfinder-monster-core/jkp7THTZMc0ivN8Y.htm)|Mammoth|Mammouth|libre|
|[jR4E2I2tmQ6sd5DR.htm](pathfinder-monster-core/jR4E2I2tmQ6sd5DR.htm)|Dezullon|Dézulone|libre|
|[JRBUBgJymEeEE4hm.htm](pathfinder-monster-core/JRBUBgJymEeEE4hm.htm)|Crag Linnorm|Linnorm des falaises|libre|
|[JSKZryHwf0Ggq8KR.htm](pathfinder-monster-core/JSKZryHwf0Ggq8KR.htm)|Shadow Giant|Géant de l'ombre|libre|
|[jTszZSs0K6vOqidM.htm](pathfinder-monster-core/jTszZSs0K6vOqidM.htm)|Azarketi Tide Tamer|Dompteur aquatique azarketi|libre|
|[JwOlSrHk1pkAKMRn.htm](pathfinder-monster-core/JwOlSrHk1pkAKMRn.htm)|Hobgoblin Archer|Arbalétrier hobgobelin|libre|
|[k0HUy6WdbZUNfG8X.htm](pathfinder-monster-core/k0HUy6WdbZUNfG8X.htm)|Tarn Linnorm|Linnorm des lacs|libre|
|[K0mtYv1v0J1utap1.htm](pathfinder-monster-core/K0mtYv1v0J1utap1.htm)|Winged Chupacabra|Chupacabra ailé|libre|
|[k4fqHhiax2GPrkbh.htm](pathfinder-monster-core/k4fqHhiax2GPrkbh.htm)|Lustspawn|Rejeton de la luxure|libre|
|[k5p4mRDT26DrDXPA.htm](pathfinder-monster-core/k5p4mRDT26DrDXPA.htm)|Giant Octopus|Pieuvre géante|libre|
|[k89Kw65a4Ip2ZSGA.htm](pathfinder-monster-core/k89Kw65a4Ip2ZSGA.htm)|Manticore (Scorpion Tail)|Manticore (Queue de scorpion)|libre|
|[ka7bXO7HIfBxk8Gy.htm](pathfinder-monster-core/ka7bXO7HIfBxk8Gy.htm)|Giant Flytrap|Attrape-mouche géant|libre|
|[kB7FNn3vosp6cqQg.htm](pathfinder-monster-core/kB7FNn3vosp6cqQg.htm)|Leopard|Léopard|libre|
|[KCVKMVYRuq6huXGz.htm](pathfinder-monster-core/KCVKMVYRuq6huXGz.htm)|Giant Mantis|Mante géante|libre|
|[KDbJ402jFuvn5frX.htm](pathfinder-monster-core/KDbJ402jFuvn5frX.htm)|Hryngar Bombardier|Bombardier hryngar|libre|
|[KdUMMd6ol83JpP62.htm](pathfinder-monster-core/KdUMMd6ol83JpP62.htm)|Adamantine Dragon (Ancient)|Dragon adamantin (Vénérable)|libre|
|[KdW5UeZSqeTZZlo5.htm](pathfinder-monster-core/KdW5UeZSqeTZZlo5.htm)|Aolaz|Aloaz|libre|
|[KgJq51AeYrENo3Db.htm](pathfinder-monster-core/KgJq51AeYrENo3Db.htm)|Will-o'-Wisp|Feu follet|libre|
|[KH1GkazaI59zftst.htm](pathfinder-monster-core/KH1GkazaI59zftst.htm)|Crawling Hand|Main rampante|libre|
|[KHTYbQgR5hnFZdGL.htm](pathfinder-monster-core/KHTYbQgR5hnFZdGL.htm)|Guard Dog|Chien de garde|libre|
|[kkAllKGsVCZVGFpf.htm](pathfinder-monster-core/kkAllKGsVCZVGFpf.htm)|Mitflit|Mitflit (Gremlin)|libre|
|[kKFfigxrJ2vbJazp.htm](pathfinder-monster-core/kKFfigxrJ2vbJazp.htm)|Grim Reaper|Faucheuse|libre|
|[kLhBdqKOMHDGjdFz.htm](pathfinder-monster-core/kLhBdqKOMHDGjdFz.htm)|Dragon Turtle|Tortue-dragon|libre|
|[kngoPtu4Cr3BtgW8.htm](pathfinder-monster-core/kngoPtu4Cr3BtgW8.htm)|Cuckoo Hag|Guenaude coucou|libre|
|[kohQQtOfhwxbzWZB.htm](pathfinder-monster-core/kohQQtOfhwxbzWZB.htm)|Ort|Ort (Diable)|libre|
|[ksJuhQGIyTejZYWS.htm](pathfinder-monster-core/ksJuhQGIyTejZYWS.htm)|Vescavor Swarm|Nuée de vescavores|libre|
|[Ky5eNRvN71O0tY9l.htm](pathfinder-monster-core/Ky5eNRvN71O0tY9l.htm)|Goblin Pyro|Pyromane gobelin|libre|
|[L39Fr3dewrvIK2LE.htm](pathfinder-monster-core/L39Fr3dewrvIK2LE.htm)|Halfling Troublemaker|Trublion halfelin|libre|
|[lFDYJOIp2knQ0IRY.htm](pathfinder-monster-core/lFDYJOIp2knQ0IRY.htm)|Jinkin|Jinkin (Gremlin)|libre|
|[lfUUnFazGLAtqRsP.htm](pathfinder-monster-core/lfUUnFazGLAtqRsP.htm)|Iron Hag|Guenaude de fer|libre|
|[LiURMjxzav8SBKSq.htm](pathfinder-monster-core/LiURMjxzav8SBKSq.htm)|Rhinoceros|Rhinocéros|libre|
|[lK4Xfb58mUNygoae.htm](pathfinder-monster-core/lK4Xfb58mUNygoae.htm)|Larval Ofalth|Ofalth larvaire|libre|
|[llSpQyOTaylpqgnW.htm](pathfinder-monster-core/llSpQyOTaylpqgnW.htm)|Warsworn|Guerrlier|libre|
|[LN7MXD38Zs2bDoW6.htm](pathfinder-monster-core/LN7MXD38Zs2bDoW6.htm)|Ghost Mage|Mage fantôme|libre|
|[loYbh3SealeYmxai.htm](pathfinder-monster-core/loYbh3SealeYmxai.htm)|Woolly Rhinoceros|Rhinocéros laineux|libre|
|[LRboVCCVODz00d5B.htm](pathfinder-monster-core/LRboVCCVODz00d5B.htm)|Vicharamuni|Vicharamuni (Naga)|libre|
|[lUBkzsSqMfQBczU1.htm](pathfinder-monster-core/lUBkzsSqMfQBczU1.htm)|Sprite|Esprit follet|libre|
|[lYoAwofYGbhWL75Q.htm](pathfinder-monster-core/lYoAwofYGbhWL75Q.htm)|Unicorn|Licorne|libre|
|[mathcxCcrQmn9Jj8.htm](pathfinder-monster-core/mathcxCcrQmn9Jj8.htm)|Giant Gecko|Gecko géant|libre|
|[MCgSMT680ic6kr5k.htm](pathfinder-monster-core/MCgSMT680ic6kr5k.htm)|Graveknight|Chevalier sépulcre|libre|
|[md1fhwGMwDv4NNwO.htm](pathfinder-monster-core/md1fhwGMwDv4NNwO.htm)|Succubus|Succube (Démon de la luxure)|libre|
|[mEjCLVRt7iDiNZL6.htm](pathfinder-monster-core/mEjCLVRt7iDiNZL6.htm)|Thulgant|Thulgant (Qlippoth)|libre|
|[meVRbBCY7LwIZq3t.htm](pathfinder-monster-core/meVRbBCY7LwIZq3t.htm)|Soulbound Doll (Careful)|Poupée des âmes (Minutieuse)|libre|
|[mEZUTqNIgu0ASApu.htm](pathfinder-monster-core/mEZUTqNIgu0ASApu.htm)|Giant Ant|Fourmis géantes|libre|
|[MkupNnMKqDBElhhp.htm](pathfinder-monster-core/MkupNnMKqDBElhhp.htm)|Giant Stag Beetle|Lucane géant (Coléoptère)|libre|
|[mOr6eIUhNuggjCKH.htm](pathfinder-monster-core/mOr6eIUhNuggjCKH.htm)|Vescavor Queen|Reine Vescavore|libre|
|[mpkGWfPHmwAYPYJH.htm](pathfinder-monster-core/mpkGWfPHmwAYPYJH.htm)|Xoarian|Xoarien (Sombre domaine)|libre|
|[mQJL411e9Iz8dJoh.htm](pathfinder-monster-core/mQJL411e9Iz8dJoh.htm)|Phoenix|Phénix|libre|
|[mRtBMD6vmFEdlgzA.htm](pathfinder-monster-core/mRtBMD6vmFEdlgzA.htm)|Empyreal Dragon (Ancient)|Dragon empyréen (Ancien)|libre|
|[MrzlaE7k1PEsd3iQ.htm](pathfinder-monster-core/MrzlaE7k1PEsd3iQ.htm)|Cockatrice|Cockatrice|libre|
|[MSm1im7lZA5i82rz.htm](pathfinder-monster-core/MSm1im7lZA5i82rz.htm)|Air Scamp|Salopin de l'air|libre|
|[MX2cLbODuo4gECPJ.htm](pathfinder-monster-core/MX2cLbODuo4gECPJ.htm)|Firewyrm|Wyrm de feu (Élémentaire, feu)|libre|
|[mx2Xpra7bRJP0GuX.htm](pathfinder-monster-core/mx2Xpra7bRJP0GuX.htm)|Elemental Hurricane|Ouragan élémentaire (Élémentaire, air)|libre|
|[mX47c0W9rizbmMBM.htm](pathfinder-monster-core/mX47c0W9rizbmMBM.htm)|Basilisk|Basilic|libre|
|[MXSKccQqbQqQ77Ii.htm](pathfinder-monster-core/MXSKccQqbQqQ77Ii.htm)|Aapoph Granitescale|Aapoph Écaille-de-granit (Homme-serpent)|libre|
|[mz7ZO0g4begL6EGH.htm](pathfinder-monster-core/mz7ZO0g4begL6EGH.htm)|Diabolic Dragon (Ancient)|Dragon diabolique (Ancien)|libre|
|[N98ug9jQHqeFoK1N.htm](pathfinder-monster-core/N98ug9jQHqeFoK1N.htm)|Ghost Commoner|Roturier fantôme|libre|
|[NAKHn3jzuaZTerir.htm](pathfinder-monster-core/NAKHn3jzuaZTerir.htm)|Sedacthy Speaker|Orateur sédacthie|libre|
|[NcYbJS5PWBGdNDqh.htm](pathfinder-monster-core/NcYbJS5PWBGdNDqh.htm)|Spider Swarm|Nuée d'araignées|libre|
|[nEB0UekUmP5L8Tj8.htm](pathfinder-monster-core/nEB0UekUmP5L8Tj8.htm)|Dwarf Stonecaster|Pétromage nain|libre|
|[nFhAJSpbJz1w71EU.htm](pathfinder-monster-core/nFhAJSpbJz1w71EU.htm)|Raktavarna|Raktavarna|libre|
|[nlnPjvP35lPLUYQp.htm](pathfinder-monster-core/nlnPjvP35lPLUYQp.htm)|Alce|Alce|libre|
|[nMC9d7ORMz3cdaHa.htm](pathfinder-monster-core/nMC9d7ORMz3cdaHa.htm)|River Drake|Drake des rivières|libre|
|[Np5Z7RMQzvSNnH0h.htm](pathfinder-monster-core/Np5Z7RMQzvSNnH0h.htm)|Ice Linnorm|Linnorm des glaces|libre|
|[NqbaJG3tPG0NGJud.htm](pathfinder-monster-core/NqbaJG3tPG0NGJud.htm)|Iron Warden|Gardien de fer (Golem)|libre|
|[NRBgcu0LkXXp8mtp.htm](pathfinder-monster-core/NRBgcu0LkXXp8mtp.htm)|Giant Centipede|Mille-pattes géant|libre|
|[NW68bxCLC6oDHxL9.htm](pathfinder-monster-core/NW68bxCLC6oDHxL9.htm)|Hobgoblin Soldier|Soldat hobgobelin|libre|
|[NxLQHUMs57TktiZa.htm](pathfinder-monster-core/NxLQHUMs57TktiZa.htm)|Vilderavn|Vilderavn|libre|
|[oaxKg1yQDmK2PWXG.htm](pathfinder-monster-core/oaxKg1yQDmK2PWXG.htm)|Arbiter|Arbitre (Aéon)|libre|
|[OCZR5r4EBUrt97Ai.htm](pathfinder-monster-core/OCZR5r4EBUrt97Ai.htm)|Jaathoom|Jaathoom (Génie, Air)|libre|
|[oNR29GreK0AxLucN.htm](pathfinder-monster-core/oNR29GreK0AxLucN.htm)|Herexen|Hérexen|libre|
|[oopxVowT2jnUQJiS.htm](pathfinder-monster-core/oopxVowT2jnUQJiS.htm)|Ogre Boss|Boss ogre|libre|
|[OoUrk7aHE5wq9nLs.htm](pathfinder-monster-core/OoUrk7aHE5wq9nLs.htm)|Pusk|Peusk (Démon de la paresse)|libre|
|[Oq31fcKwH0EE9R89.htm](pathfinder-monster-core/Oq31fcKwH0EE9R89.htm)|Elemental Avalanche|Avalanche élémentaire (Élémentaire, terre)|libre|
|[osscRiyHUXzfv0yz.htm](pathfinder-monster-core/osscRiyHUXzfv0yz.htm)|Empyreal Dragon (Ancient, Spellcaster)|Dragon empyréen (Ancien, incantateur)|libre|
|[oyfheSs1ta4xvtEg.htm](pathfinder-monster-core/oyfheSs1ta4xvtEg.htm)|Giant Monitor Lizard|Varan géant|libre|
|[oZVeE7D70bHOrs1d.htm](pathfinder-monster-core/oZVeE7D70bHOrs1d.htm)|Cyclops|Cyclope|libre|
|[P8pNcpNeXQcj6lBB.htm](pathfinder-monster-core/P8pNcpNeXQcj6lBB.htm)|Treerazer|Fléau des arbres|libre|
|[PcBcC9HZBrM4gWu3.htm](pathfinder-monster-core/PcBcC9HZBrM4gWu3.htm)|Raja-Krodha|Raja-Krodha|libre|
|[PcHQDmPTztw32PhL.htm](pathfinder-monster-core/PcHQDmPTztw32PhL.htm)|Kobold Scout|Éclaireur Kobold|libre|
|[pH2yNe16EnoJ8R0i.htm](pathfinder-monster-core/pH2yNe16EnoJ8R0i.htm)|Gimmerling|Gimmerling|libre|
|[pkhNqTDUttoAzcKn.htm](pathfinder-monster-core/pkhNqTDUttoAzcKn.htm)|Greedspawn|Rejeton de l'avarice|libre|
|[PlkRv9NMKq9TShYf.htm](pathfinder-monster-core/PlkRv9NMKq9TShYf.htm)|Gorilla|Gorille|libre|
|[PLZk6zY5iwccPTPS.htm](pathfinder-monster-core/PLZk6zY5iwccPTPS.htm)|Orc Commander|Commandant orc|libre|
|[pPQyoQHTxrE2U7px.htm](pathfinder-monster-core/pPQyoQHTxrE2U7px.htm)|Vampire Count|Vampire, Comte|libre|
|[ptaN3MY80GiE6PHW.htm](pathfinder-monster-core/ptaN3MY80GiE6PHW.htm)|Adamantine Dragon (Ancient, Spellcaster)|Dragon adamantin (Vénérable, Incantateur)|libre|
|[PUPnGG406PanzIvL.htm](pathfinder-monster-core/PUPnGG406PanzIvL.htm)|Sea Serpent|Serpent de mer|libre|
|[pVbggIyXxCo8pPue.htm](pathfinder-monster-core/pVbggIyXxCo8pPue.htm)|Living Wildfire|Furole vivant (Élémentaire, feu)|libre|
|[PvYl5kItb7xoE8Is.htm](pathfinder-monster-core/PvYl5kItb7xoE8Is.htm)|Catfolk Pouncer|Félide musard (amurrun)|libre|
|[pyTr1VOPrPYH8UNg.htm](pathfinder-monster-core/pyTr1VOPrPYH8UNg.htm)|Megaprimatus|Mégaprimatus|libre|
|[pzQhR0mc8HEhXLOZ.htm](pathfinder-monster-core/pzQhR0mc8HEhXLOZ.htm)|Athamaru Hunter|Chasseur athamaru|libre|
|[Q1qjdG3i8TZuEOq6.htm](pathfinder-monster-core/Q1qjdG3i8TZuEOq6.htm)|Centaur Herbalist|Centaure herboriste|libre|
|[Q1wybC7rSc4MIF9g.htm](pathfinder-monster-core/Q1wybC7rSc4MIF9g.htm)|Balisse|Balisse (Ange)|libre|
|[q95mjOkL678a1Wnt.htm](pathfinder-monster-core/q95mjOkL678a1Wnt.htm)|Tooth Fairy|Fée des dents|libre|
|[QBu0tRV8nNIFKbLS.htm](pathfinder-monster-core/QBu0tRV8nNIFKbLS.htm)|Omen Dragon (Ancient)|Dragon des présages (Ancien)|libre|
|[QCPpQya5TEUuIxQn.htm](pathfinder-monster-core/QCPpQya5TEUuIxQn.htm)|Medusa|Méduse|libre|
|[qDTbixJDMopR0ixh.htm](pathfinder-monster-core/qDTbixJDMopR0ixh.htm)|Soulbound Homunculus|Homoncule des âmes|libre|
|[QIXc18xHrEWDmtKW.htm](pathfinder-monster-core/QIXc18xHrEWDmtKW.htm)|Fey Dragonet|Fée dragon|libre|
|[QJgC1lWvwekWJjkD.htm](pathfinder-monster-core/QJgC1lWvwekWJjkD.htm)|Qarna|Qarna (Archon)|libre|
|[qlxVPpwVFw5qIVQM.htm](pathfinder-monster-core/qlxVPpwVFw5qIVQM.htm)|Flame Drake|Drake des flammes|libre|
|[qOAuCiPgsjpjDgA1.htm](pathfinder-monster-core/qOAuCiPgsjpjDgA1.htm)|Kholo Hunter|Chasseur kholo|libre|
|[qRUezfNovWdrZtdt.htm](pathfinder-monster-core/qRUezfNovWdrZtdt.htm)|Sedacthy Scout|Éclaireur sédacthie|libre|
|[qRUqoezeEnQ2KdyT.htm](pathfinder-monster-core/qRUqoezeEnQ2KdyT.htm)|Nightmare|Destrier noir|libre|
|[qtJ36jlcRQw5sBnr.htm](pathfinder-monster-core/qtJ36jlcRQw5sBnr.htm)|Stegosaurus|Stégosaure (Dinosaure)|libre|
|[qtwhQEehDrMoWusV.htm](pathfinder-monster-core/qtwhQEehDrMoWusV.htm)|Fortune Dragon (Young)|Dragon fortuné (Jeune)|libre|
|[qvagIV8gqF0g3m3l.htm](pathfinder-monster-core/qvagIV8gqF0g3m3l.htm)|Vordine|Vordine (Diable)|libre|
|[qyCcr32PVcnNm4Wr.htm](pathfinder-monster-core/qyCcr32PVcnNm4Wr.htm)|Dero Magister|Magister dero|libre|
|[qYw2ToefDK5Vrwgu.htm](pathfinder-monster-core/qYw2ToefDK5Vrwgu.htm)|Ankhrav Hive Mother|Mère de la ruche ankhrav|libre|
|[r3EXyntNIazKf2gP.htm](pathfinder-monster-core/r3EXyntNIazKf2gP.htm)|String Slime|Vasecorde|libre|
|[R427CMT90S7fv7MY.htm](pathfinder-monster-core/R427CMT90S7fv7MY.htm)|Elemental Tsunami|Tsunami élémentaire (Élémentaire, eau)|libre|
|[rATBpWa62XUPnGZb.htm](pathfinder-monster-core/rATBpWa62XUPnGZb.htm)|Soulbound Doll (Rash)|Poupée des âmes (Téméraire)|libre|
|[rdgs2gxTWxkyanD6.htm](pathfinder-monster-core/rdgs2gxTWxkyanD6.htm)|Sphinx|Sphinx|libre|
|[Re7B1kEJMFlgpaSc.htm](pathfinder-monster-core/Re7B1kEJMFlgpaSc.htm)|Aesra|Aésra (Archon)|libre|
|[rEDhI2oI4TqTICN2.htm](pathfinder-monster-core/rEDhI2oI4TqTICN2.htm)|Fire Giant|Géant du feu|libre|
|[RJFpwZIGbuOuCtXr.htm](pathfinder-monster-core/RJFpwZIGbuOuCtXr.htm)|Elananx|Élananxe|libre|
|[rmJItn5jMW7Af0Iy.htm](pathfinder-monster-core/rmJItn5jMW7Af0Iy.htm)|Tooth Fairy Swarm|Nuée de fées des dents|libre|
|[Ro49rEZwBLkkoEKE.htm](pathfinder-monster-core/Ro49rEZwBLkkoEKE.htm)|Mummy Pharaoh|Momie pharaon|libre|
|[RqX0vfUjlycKjGyp.htm](pathfinder-monster-core/RqX0vfUjlycKjGyp.htm)|Changeling Exile|Exilé changelin|libre|
|[Rr1u6WvZEdPw1s6v.htm](pathfinder-monster-core/Rr1u6WvZEdPw1s6v.htm)|Reefclaw|Pince des récifs|libre|
|[RTviEfjYnsXa0wkT.htm](pathfinder-monster-core/RTviEfjYnsXa0wkT.htm)|Hell Hound|Molosse infernal|libre|
|[S1XgBHtXIOV3JjLy.htm](pathfinder-monster-core/S1XgBHtXIOV3JjLy.htm)|Xulgath Skulker|Vadrouilleur Xulgath|libre|
|[s2TkernjfKVEhlJY.htm](pathfinder-monster-core/s2TkernjfKVEhlJY.htm)|Boggard Scout|Éclaireur bourbiérin|libre|
|[s492qJ4psEb4FXuy.htm](pathfinder-monster-core/s492qJ4psEb4FXuy.htm)|Gylou|Gylou (Diable)|libre|
|[S67oZ1mvVNz9FTUE.htm](pathfinder-monster-core/S67oZ1mvVNz9FTUE.htm)|Roc|Roc|libre|
|[sA2dFdRUwiapo69Z.htm](pathfinder-monster-core/sA2dFdRUwiapo69Z.htm)|Goblin Commando|Commando gobelin|libre|
|[sAfjpjAS56jtrUbi.htm](pathfinder-monster-core/sAfjpjAS56jtrUbi.htm)|Zephyr Hawk|Faucon zéphyr (Élémentaire, air)|libre|
|[sC4B1pjGrKFXhjOQ.htm](pathfinder-monster-core/sC4B1pjGrKFXhjOQ.htm)|Grindylow|Strangulot|libre|
|[sd6BsHgVrw4FgR70.htm](pathfinder-monster-core/sd6BsHgVrw4FgR70.htm)|Elf Ranger|Rôdeur elfe|libre|
|[sEgjgitJmwYYa4mV.htm](pathfinder-monster-core/sEgjgitJmwYYa4mV.htm)|Skeletal Giant|Géant squelettique|libre|
|[SipdgBCL7XuuEjn6.htm](pathfinder-monster-core/SipdgBCL7XuuEjn6.htm)|Kholo Sergeant|Sergent kholo|libre|
|[sK514S9XUfkNXcy8.htm](pathfinder-monster-core/sK514S9XUfkNXcy8.htm)|Mirage Dragon (Adult, Spellcaster)|Dragon mirage (Adulte, Incantateur)|libre|
|[sldauWtSyK4JEiRl.htm](pathfinder-monster-core/sldauWtSyK4JEiRl.htm)|Centipede Swarm|Nuée de mille-pattes|libre|
|[smItqlbr0iuDJ8nL.htm](pathfinder-monster-core/smItqlbr0iuDJ8nL.htm)|Lich|Liche|libre|
|[SMLMW81mKN5VlcVV.htm](pathfinder-monster-core/SMLMW81mKN5VlcVV.htm)|Bone Prophet|Prophète des ossements (Homme-serpent)|libre|
|[sPFQOP6asxC8Ga0O.htm](pathfinder-monster-core/sPFQOP6asxC8Ga0O.htm)|Seraptis|Séraptis (Démon du suicide)|libre|
|[sUqSb1cvpotAFf0K.htm](pathfinder-monster-core/sUqSb1cvpotAFf0K.htm)|Diabolic Dragon (Young, Spellcaster)|Dragon diabolique (Jeune, Incantateur)|libre|
|[SZCf0IZkf36plwVd.htm](pathfinder-monster-core/SZCf0IZkf36plwVd.htm)|Caligni Dancer|Danseur caligni|libre|
|[T0OAOkmk4xz0wvjJ.htm](pathfinder-monster-core/T0OAOkmk4xz0wvjJ.htm)|Conspirator Dragon (Adult, Spellcaster)|Dragon comploteur (Adulte, incantateur)|libre|
|[tashopA1s2fAbSXA.htm](pathfinder-monster-core/tashopA1s2fAbSXA.htm)|Slurk|Slurk|libre|
|[tbqcZG0lOoUVhNfn.htm](pathfinder-monster-core/tbqcZG0lOoUVhNfn.htm)|Mirage Dragon (Young, Spellcaster)|Dragon mirage (Jeune, Incantateur)|libre|
|[TdhUDXvkKBctIpH8.htm](pathfinder-monster-core/TdhUDXvkKBctIpH8.htm)|Fortune Dragon (Adult)|Dragon fortuné (Adulte)|libre|
|[TElwkEGZy1zgwoVg.htm](pathfinder-monster-core/TElwkEGZy1zgwoVg.htm)|Sprigjack|Homme-brin|libre|
|[TGYELuImcTcuX0aH.htm](pathfinder-monster-core/TGYELuImcTcuX0aH.htm)|Conspirator Dragon (Adult)|Dragon comploteur (Adulte)|libre|
|[TlDmc2ZKeIAJuD5v.htm](pathfinder-monster-core/TlDmc2ZKeIAJuD5v.htm)|Giant Crawling Hand|Main rampante géante|libre|
|[ToteDLIM7jCyHwDH.htm](pathfinder-monster-core/ToteDLIM7jCyHwDH.htm)|Stone Mauler|Broyeur de pierre (Élémentaire, terre)|libre|
|[tpNP1UooPPHMyZye.htm](pathfinder-monster-core/tpNP1UooPPHMyZye.htm)|Tyrannosaurus|Tyrannosaure (Dinosaure)|libre|
|[trchDxbDR2TiPMxT.htm](pathfinder-monster-core/trchDxbDR2TiPMxT.htm)|Skeleton Guard|Garde squelette|libre|
|[tuuSayyL0A5R6hZh.htm](pathfinder-monster-core/tuuSayyL0A5R6hZh.htm)|Marsh Giant|Géant des marais|libre|
|[tUWchW8dXavTFeBy.htm](pathfinder-monster-core/tUWchW8dXavTFeBy.htm)|Hyena|Hyène|libre|
|[TwFcW5O5J1SdsYv3.htm](pathfinder-monster-core/TwFcW5O5J1SdsYv3.htm)|Gosreg|Gosreg (Sombre domaine)|libre|
|[Twvzy1yRo6m6dM8D.htm](pathfinder-monster-core/Twvzy1yRo6m6dM8D.htm)|Ogre Warrior|Guerrier ogre|libre|
|[TZt3H39oxVdZRKs9.htm](pathfinder-monster-core/TZt3H39oxVdZRKs9.htm)|Vampire Mastermind|Vampire, Maître|libre|
|[uaKlWSKuyZmt88ol.htm](pathfinder-monster-core/uaKlWSKuyZmt88ol.htm)|Soulbound Doll (Brave)|Poupée des âmes (Courageuse)|libre|
|[uBNm3R9wbLTPrM9i.htm](pathfinder-monster-core/uBNm3R9wbLTPrM9i.htm)|Harpy|Harpie|libre|
|[uc61UL57ywxLy0q0.htm](pathfinder-monster-core/uc61UL57ywxLy0q0.htm)|Mirage Dragon (Ancient)|Dragon mirage (Vénérable)|libre|
|[uco1YijAEotYjdnF.htm](pathfinder-monster-core/uco1YijAEotYjdnF.htm)|Orca|Orque épaulard|libre|
|[UMlGhyoHMvhVW6kv.htm](pathfinder-monster-core/UMlGhyoHMvhVW6kv.htm)|Sarglagon|Sarglagon (Diable)|libre|
|[uNNOQFvuMq8ZsQkn.htm](pathfinder-monster-core/uNNOQFvuMq8ZsQkn.htm)|Great White Shark|Grand requin blanc|libre|
|[UQRvCkhkS1ChOzLE.htm](pathfinder-monster-core/UQRvCkhkS1ChOzLE.htm)|Gongorinan|Gongorinien (Qlippoth)|libre|
|[UuAvrw7KUEjOM6uN.htm](pathfinder-monster-core/UuAvrw7KUEjOM6uN.htm)|Bogwid|Embourbé|libre|
|[uUP9MQscB0EFPptr.htm](pathfinder-monster-core/uUP9MQscB0EFPptr.htm)|Dero Strangler|Étrangleur dero|libre|
|[UuQHd4v6gG8ONdCt.htm](pathfinder-monster-core/UuQHd4v6gG8ONdCt.htm)|Draxie|Draxie (Esprit follet)|libre|
|[UwvAf8riRdMaitA4.htm](pathfinder-monster-core/UwvAf8riRdMaitA4.htm)|Fire Scamp|Salopin du feu|libre|
|[uzH85ifDz5GU525p.htm](pathfinder-monster-core/uzH85ifDz5GU525p.htm)|Compsognathus|Compsognathus (Dinosaure)|libre|
|[V1Kr5aiPaTM0mDFu.htm](pathfinder-monster-core/V1Kr5aiPaTM0mDFu.htm)|Ankhrav|Ankhrav|libre|
|[v1UK3IwCB8wCbL3L.htm](pathfinder-monster-core/v1UK3IwCB8wCbL3L.htm)|Leaf Leshy|Léchi de feuilles|libre|
|[v3jXIOHyQqXvImbc.htm](pathfinder-monster-core/v3jXIOHyQqXvImbc.htm)|Soulbound Doll (Sassy)|Poupée des âmes (Insolente)|libre|
|[v4KP0HYaygoFOIlo.htm](pathfinder-monster-core/v4KP0HYaygoFOIlo.htm)|Daeodon|Daeodon|libre|
|[V4rVnbjJbcOIdC4Z.htm](pathfinder-monster-core/V4rVnbjJbcOIdC4Z.htm)|Skeletal Hulk|Mastodonte squelettique|libre|
|[v7LH85fl189pXMsR.htm](pathfinder-monster-core/v7LH85fl189pXMsR.htm)|Benthic Worm|Ver des profondeurs|libre|
|[V8w4iOwUMPqYnqVE.htm](pathfinder-monster-core/V8w4iOwUMPqYnqVE.htm)|Lizardfolk Scout|Éclaireur homme-lézard (Iruxi)|libre|
|[V90OYOMyyPLPJuod.htm](pathfinder-monster-core/V90OYOMyyPLPJuod.htm)|Orc Veteran|Vétéran orc|libre|
|[VbN33np2jBfaxhAz.htm](pathfinder-monster-core/VbN33np2jBfaxhAz.htm)|Coarti|Coarti (Diable)|libre|
|[VCYF0NAfPKwTHkK1.htm](pathfinder-monster-core/VCYF0NAfPKwTHkK1.htm)|Grothlut|Grothlut (Distordu)|libre|
|[VDm2XhfNwhgiJOxD.htm](pathfinder-monster-core/VDm2XhfNwhgiJOxD.htm)|Vidileth|Vidileth (Alghollthu)|libre|
|[VGPtiSeeT7CYgWrv.htm](pathfinder-monster-core/VGPtiSeeT7CYgWrv.htm)|Island Oni|Oni des îles|libre|
|[vijBriZmbUJjbJNH.htm](pathfinder-monster-core/vijBriZmbUJjbJNH.htm)|Cave Worm|Ver des cavernes|libre|
|[vN2alMciNlKpBpKN.htm](pathfinder-monster-core/vN2alMciNlKpBpKN.htm)|Minotaur Hunter|Chasseur minotaure|libre|
|[VotlWUsFKdOrHWF6.htm](pathfinder-monster-core/VotlWUsFKdOrHWF6.htm)|Shadow|Ombre|libre|
|[VQPzDz3xnCQGFOGL.htm](pathfinder-monster-core/VQPzDz3xnCQGFOGL.htm)|Sod Hound|Molosse de tourbe|libre|
|[vqYrJ33XgoeQUUle.htm](pathfinder-monster-core/vqYrJ33XgoeQUUle.htm)|Lamia|Lamie|libre|
|[vRAdYovWcy2euwuL.htm](pathfinder-monster-core/vRAdYovWcy2euwuL.htm)|Snapping Flytrap|Attrape-mouches vorace|libre|
|[vVs0N3mcWYHsyccc.htm](pathfinder-monster-core/vVs0N3mcWYHsyccc.htm)|Zecui|Zécui|libre|
|[VzkfSgbtsGV79WZH.htm](pathfinder-monster-core/VzkfSgbtsGV79WZH.htm)|Sedacthy Marauder|Maraudeur sédacthie|libre|
|[w2jByvjeZbAclGWf.htm](pathfinder-monster-core/w2jByvjeZbAclGWf.htm)|Soulbound Doll (Gentle)|Poupée des âmes (Douce)|libre|
|[WamGnH8v0QHz8NFr.htm](pathfinder-monster-core/WamGnH8v0QHz8NFr.htm)|Giant Moray Eel|Murène géante|libre|
|[WBPEvEqIGvxeQKlp.htm](pathfinder-monster-core/WBPEvEqIGvxeQKlp.htm)|Eagle|Aigle|libre|
|[WDBGcfNArKSpN7z0.htm](pathfinder-monster-core/WDBGcfNArKSpN7z0.htm)|Jabali|Jabali (Génie, Terre)|libre|
|[wdHIc6Tg1OBMGaK5.htm](pathfinder-monster-core/wdHIc6Tg1OBMGaK5.htm)|Horned Dragon (Young, Spellcaster)|Dragon cornu (Jeune, incantateur)|libre|
|[wepiUEi2Lxl8j1BH.htm](pathfinder-monster-core/wepiUEi2Lxl8j1BH.htm)|Goblin War Chanter|Chantre de guerre gobelin|libre|
|[wfZoM7RfFzkmK7hI.htm](pathfinder-monster-core/wfZoM7RfFzkmK7hI.htm)|Paleohemoth|Paléohémoth (Golem)|libre|
|[WG4Tk2k9Lm21CEQv.htm](pathfinder-monster-core/WG4Tk2k9Lm21CEQv.htm)|Greater Nightmare|Destrier noir supérieur|libre|
|[WioQ6rOeMRuTOliY.htm](pathfinder-monster-core/WioQ6rOeMRuTOliY.htm)|Zombie Brute|Brute zombie|libre|
|[wJH5BXOWZYEyVB3y.htm](pathfinder-monster-core/wJH5BXOWZYEyVB3y.htm)|Troll Warleader|Seigneur de guerre troll|libre|
|[WlbwkGQPAenbSUKJ.htm](pathfinder-monster-core/WlbwkGQPAenbSUKJ.htm)|Dromaar Mountaineer|Montagnard dromaar|libre|
|[Wlupxz7dmKb6BYcr.htm](pathfinder-monster-core/Wlupxz7dmKb6BYcr.htm)|Caldera Oni|Oni des caldeiras|libre|
|[wNkS1ArFjS6ZsrPS.htm](pathfinder-monster-core/wNkS1ArFjS6ZsrPS.htm)|Deinonychus|Deinonychus (Dinosaure)|libre|
|[WNUvjcKRAqdguWfN.htm](pathfinder-monster-core/WNUvjcKRAqdguWfN.htm)|Poracha|Poracha|libre|
|[WoICHri7raCYv1wU.htm](pathfinder-monster-core/WoICHri7raCYv1wU.htm)|Lesser Death|Mort mineure|libre|
|[wvP8zBmI0PDO1Uq2.htm](pathfinder-monster-core/wvP8zBmI0PDO1Uq2.htm)|Giant Hippocampus|Hippocampe géant|libre|
|[wy8Ve0m3wbHMo1U1.htm](pathfinder-monster-core/wy8Ve0m3wbHMo1U1.htm)|Brontosaurus|Brontosaure (Dinosaure)|libre|
|[wycu7XN9VgiSpSfB.htm](pathfinder-monster-core/wycu7XN9VgiSpSfB.htm)|Zoaem|Zoaem (Archon)|libre|
|[WZRHV2WU0SkpbtJI.htm](pathfinder-monster-core/WZRHV2WU0SkpbtJI.htm)|Lizardfolk Defender|Défenseur homme-lézard (Iruxi)|libre|
|[X2vz6CrMaHIso0ha.htm](pathfinder-monster-core/X2vz6CrMaHIso0ha.htm)|Skeletal Horse|Cheval squelettique|libre|
|[X3QcYLr2rBcIsJrC.htm](pathfinder-monster-core/X3QcYLr2rBcIsJrC.htm)|Aapoph Serpentfolk|Aapoph (Homme-serpent)|libre|
|[x7Aa4Tvr9eBaHryF.htm](pathfinder-monster-core/x7Aa4Tvr9eBaHryF.htm)|Warg|Warg|libre|
|[X7PaA6XgvrY5ByfM.htm](pathfinder-monster-core/X7PaA6XgvrY5ByfM.htm)|Ogre Glutton|Glouton ogre|libre|
|[xC6v5Ef8mDt05QFK.htm](pathfinder-monster-core/xC6v5Ef8mDt05QFK.htm)|Deinosuchus|Deinosuchus|libre|
|[XDNJSVxOOryeuN44.htm](pathfinder-monster-core/XDNJSVxOOryeuN44.htm)|Giant Anaconda|Anaconda géant|libre|
|[XEjkJ8fQqLc02hrU.htm](pathfinder-monster-core/XEjkJ8fQqLc02hrU.htm)|Giant Eagle|Aigle géant|libre|
|[XgGeD4fz5m7nQQlN.htm](pathfinder-monster-core/XgGeD4fz5m7nQQlN.htm)|Living Whirlwind|Tourbillon vivant (Élémentaire, air)|libre|
|[xgKDQB6ZYmAutwAm.htm](pathfinder-monster-core/xgKDQB6ZYmAutwAm.htm)|Hobgoblin General|Général hobgobelin|libre|
|[XKOQ3ll9TGNso0uB.htm](pathfinder-monster-core/XKOQ3ll9TGNso0uB.htm)|Banshee|Banshie|libre|
|[xN5J9S485LxFZMkL.htm](pathfinder-monster-core/xN5J9S485LxFZMkL.htm)|Cinder Rat|Rat des braises (Élémentaire, feu)|libre|
|[xnpuGO8jEMba9wy5.htm](pathfinder-monster-core/xnpuGO8jEMba9wy5.htm)|Giant Bat|Chauve-souris géante|libre|
|[XO3qw7XPkgRq4H6I.htm](pathfinder-monster-core/XO3qw7XPkgRq4H6I.htm)|Omen Dragon (Adult, Spellcaster)|Dragon des présages (Adulte, incantateur)|libre|
|[Xo4IGzw28hivgMmM.htm](pathfinder-monster-core/Xo4IGzw28hivgMmM.htm)|Zombie Shambler|Titubeur zombie|libre|
|[xTnBWwbZKbhCfHgJ.htm](pathfinder-monster-core/xTnBWwbZKbhCfHgJ.htm)|Lawbringer Warpriest|Prêtre combatant Porteur de loi (Héritier des plans, Néphilim)|libre|
|[xz2NZqSG5YVl17dc.htm](pathfinder-monster-core/xz2NZqSG5YVl17dc.htm)|Shuln|Shuln|libre|
|[XZWUQklzWF6YFPmG.htm](pathfinder-monster-core/XZWUQklzWF6YFPmG.htm)|Mummy Guardian|Momie gardienne|libre|
|[y2w2jjs2O3gP0H5v.htm](pathfinder-monster-core/y2w2jjs2O3gP0H5v.htm)|Coil Spy|Espion anneau (Homme-serpent)|libre|
|[Yadztw8CmYuWfA7k.htm](pathfinder-monster-core/Yadztw8CmYuWfA7k.htm)|Python|Python|libre|
|[YAGc6gQ5VrvWyR37.htm](pathfinder-monster-core/YAGc6gQ5VrvWyR37.htm)|Phantom Beast|Bête fantôme|libre|
|[ybkelAOtSIA06fnj.htm](pathfinder-monster-core/ybkelAOtSIA06fnj.htm)|Animated Broom|Balai animé|libre|
|[yclRuradTmZbdKFQ.htm](pathfinder-monster-core/yclRuradTmZbdKFQ.htm)|Goblin Dog|Chien gobelin|libre|
|[YHoXWNZhIaZWdq0Y.htm](pathfinder-monster-core/YHoXWNZhIaZWdq0Y.htm)|Conspirator Dragon (Young, Spellcaster)|Dragon comploteur (Jeune, incantateur)|libre|
|[YhVYGhzNrOFQROui.htm](pathfinder-monster-core/YhVYGhzNrOFQROui.htm)|Cythnigot|Cythnigote (Qlippoth)|libre|
|[YiGZxwT2xTVYQyTu.htm](pathfinder-monster-core/YiGZxwT2xTVYQyTu.htm)|Merfolk Warrior|Combattant homme-poisson|libre|
|[yj2nhIS8ZsAJh2l5.htm](pathfinder-monster-core/yj2nhIS8ZsAJh2l5.htm)|Rhu-Chalik|Rhu-Chalik (Sombre domaine)|libre|
|[YKMvlKA1AZJlXtz9.htm](pathfinder-monster-core/YKMvlKA1AZJlXtz9.htm)|Augnagar|Augnagar (Qlippoth)|libre|
|[ylhweChE6bNwf7Q2.htm](pathfinder-monster-core/ylhweChE6bNwf7Q2.htm)|Diabolic Dragon (Ancient, Spellcaster)|Dragon diabolique (Ancien, Incantateur)|libre|
|[yogotW0edcHEPeuR.htm](pathfinder-monster-core/yogotW0edcHEPeuR.htm)|Kholo Bonekeeper|Gardien des os kholo|libre|
|[yPYQC2bfOYmqcfIB.htm](pathfinder-monster-core/yPYQC2bfOYmqcfIB.htm)|Imp|Diablotin|libre|
|[yQ2mosomuAPiLMkU.htm](pathfinder-monster-core/yQ2mosomuAPiLMkU.htm)|Animated Statue|Statue animée|libre|
|[YReM6QbqwUz3UTP7.htm](pathfinder-monster-core/YReM6QbqwUz3UTP7.htm)|Orc Scrapper|Baroudeur orc|libre|
|[YRyTBciVtCnO7J0Z.htm](pathfinder-monster-core/YRyTBciVtCnO7J0Z.htm)|Rune Giant|Géant des runes|libre|
|[yS0bUM8R6hb4fIx2.htm](pathfinder-monster-core/yS0bUM8R6hb4fIx2.htm)|Weretiger|Tigre-garou|libre|
|[Ytp0kRaG8iexmPfN.htm](pathfinder-monster-core/Ytp0kRaG8iexmPfN.htm)|Hryngar Sharpshooter|Tireur d'élite hryngar|libre|
|[YUI465JYqM65iimj.htm](pathfinder-monster-core/YUI465JYqM65iimj.htm)|Jah-Tohl|Jah-Tohl (Sombre domaine)|libre|
|[yv7hdQ3MO3pLgCF5.htm](pathfinder-monster-core/yv7hdQ3MO3pLgCF5.htm)|Wrathspawn|Rejeton de la colère|libre|
|[YV83qiV7nEtVDPEP.htm](pathfinder-monster-core/YV83qiV7nEtVDPEP.htm)|Adamantine Dragon (Young, Spellcaster)|Dragon Adamantin (Jeune, Incantateur)|libre|
|[yVS5i5U28fQyGz99.htm](pathfinder-monster-core/yVS5i5U28fQyGz99.htm)|Mountain Oni|Oni des montagnes|libre|
|[yYicb7uJH4EaBO4v.htm](pathfinder-monster-core/yYicb7uJH4EaBO4v.htm)|Elemental Inferno|Brasier élémentaire (Élémentaire, feu)|libre|
|[YZ4G7eRQ49dTVtjb.htm](pathfinder-monster-core/YZ4G7eRQ49dTVtjb.htm)|Bottlenose Dolphin|Grand dauphin|libre|
|[z1TEwL0plpK4l2uf.htm](pathfinder-monster-core/z1TEwL0plpK4l2uf.htm)|Keketar|Ké'ktar (Protéen)|libre|
|[z2l8K7woKYPkm0qz.htm](pathfinder-monster-core/z2l8K7woKYPkm0qz.htm)|Arboreal Regent|Régent arboréen (Arboréen)|libre|
|[Z5QezXy38ZHyt3O3.htm](pathfinder-monster-core/Z5QezXy38ZHyt3O3.htm)|Cloud Giant|Géant des nuages|libre|
|[z9jEyLrsoBMmh9qg.htm](pathfinder-monster-core/z9jEyLrsoBMmh9qg.htm)|Morrigna|Morrigna (Psychopompe)|libre|
|[zAxKR8XWtQm2rqh4.htm](pathfinder-monster-core/zAxKR8XWtQm2rqh4.htm)|Triceratops|Tricératops (Dinosaure)|libre|
|[zeK0ii5YaynhtQi0.htm](pathfinder-monster-core/zeK0ii5YaynhtQi0.htm)|Quetz Coatl|Quetz couatl|libre|
|[ZeUxgpNi5AXr9pX6.htm](pathfinder-monster-core/ZeUxgpNi5AXr9pX6.htm)|Manticore (Quill Tail)|Manticore (Queue de piquants)|libre|
|[zkl6planCbeCuAdS.htm](pathfinder-monster-core/zkl6planCbeCuAdS.htm)|Skulltaker|Volecrâne|libre|
|[ZlAzJNsHYz1ZtMVK.htm](pathfinder-monster-core/ZlAzJNsHYz1ZtMVK.htm)|Greater Herexen|Hérexen supérieur|libre|
|[zpd6b6UPP72ZELCj.htm](pathfinder-monster-core/zpd6b6UPP72ZELCj.htm)|Poltergeist|Poltergeist|libre|
|[zQraVA7SUjd6qGNh.htm](pathfinder-monster-core/zQraVA7SUjd6qGNh.htm)|Deadly Mantis|Mante meurtrière|libre|
|[zrzoj0L0ZwfCbHXr.htm](pathfinder-monster-core/zrzoj0L0ZwfCbHXr.htm)|Sweet Hag|Guenaude doucereuse|libre|
|[zT3fSxNatEbrkCzN.htm](pathfinder-monster-core/zT3fSxNatEbrkCzN.htm)|Stone Giant|Géant de pierre|libre|
|[ZW8ARUrNdc3zewLM.htm](pathfinder-monster-core/ZW8ARUrNdc3zewLM.htm)|Velociraptor|Vélociraptor (Dinosaure)|libre|
|[ZXiFjrQbhvboEZTL.htm](pathfinder-monster-core/ZXiFjrQbhvboEZTL.htm)|Azarketi Crab Catcher|Piégeur de crabe azarketi|libre|
