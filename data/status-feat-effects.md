# État de la traduction (feat-effects)

 * **libre**: 550
 * **aucune**: 112
 * **changé**: 4


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[0h6dhuMU0fOr1OS5.htm](feat-effects/0h6dhuMU0fOr1OS5.htm)|Effect: Invoke Offense|
|[0nkzRcjN9iOSFUf9.htm](feat-effects/0nkzRcjN9iOSFUf9.htm)|Effect: Pass Vengeful Judgment|
|[1bnJJb4hsnMOw9cQ.htm](feat-effects/1bnJJb4hsnMOw9cQ.htm)|Effect: Stymie the Gods|
|[1FIje7Y5zIbf9V0p.htm](feat-effects/1FIje7Y5zIbf9V0p.htm)|Effect: Abjure Harm|
|[21P9Xc6DslCjfq0Y.htm](feat-effects/21P9Xc6DslCjfq0Y.htm)|Effect: Pale Horse|
|[2JnREGOAqZCZPfdK.htm](feat-effects/2JnREGOAqZCZPfdK.htm)|Effect: The Proud|
|[2O1MmFvLdGZodvVc.htm](feat-effects/2O1MmFvLdGZodvVc.htm)|Effect: Enter Spirit Trance|
|[32QaXCRgU5l5lMIa.htm](feat-effects/32QaXCRgU5l5lMIa.htm)|Effect: Invoke Defense|
|[3K8fNQKjmrHU8dzi.htm](feat-effects/3K8fNQKjmrHU8dzi.htm)|Effect: Gird Champion (Hierophant)|
|[3MVVaD8hAJnGSyfm.htm](feat-effects/3MVVaD8hAJnGSyfm.htm)|Effect: Mutagenist Field Benefit|
|[3PmBgt8SkkHqhRh1.htm](feat-effects/3PmBgt8SkkHqhRh1.htm)|Effect: Destined Victory (Note)|
|[3wA2IOyZXjFssheT.htm](feat-effects/3wA2IOyZXjFssheT.htm)|Effect: Vindication Edge|
|[3zNPch2pttYW2n1j.htm](feat-effects/3zNPch2pttYW2n1j.htm)|Effect: To War!|
|[4j5J4IM9sWNe1UcT.htm](feat-effects/4j5J4IM9sWNe1UcT.htm)|Effect: Demon Hunted|
|[5dxWxTDjPPT79xpn.htm](feat-effects/5dxWxTDjPPT79xpn.htm)|Effect: Crash Against Me (Note)|
|[5rypwpGjbu4aCAHh.htm](feat-effects/5rypwpGjbu4aCAHh.htm)|Effect: Ascend|
|[6hrbTLlSyhL2W9Nf.htm](feat-effects/6hrbTLlSyhL2W9Nf.htm)|Effect: Lantern|
|[7UWKpTquSpP6Pgzh.htm](feat-effects/7UWKpTquSpP6Pgzh.htm)|Effect: Ascended Celestial Nimbus|
|[8uXXaQ3Y5FP2mwn6.htm](feat-effects/8uXXaQ3Y5FP2mwn6.htm)|Effect: Echolocation|
|[8YjmPizeglw83W9E.htm](feat-effects/8YjmPizeglw83W9E.htm)|Effect: Curse of Inevitable Rot|
|[90dTxPhFtbhK6Nv4.htm](feat-effects/90dTxPhFtbhK6Nv4.htm)|Effect: Amulet's Abeyance|
|[9DXdG20VAIGnFweU.htm](feat-effects/9DXdG20VAIGnFweU.htm)|Effect: Absolve Sins|
|[9hIzh4UvjSTyYBby.htm](feat-effects/9hIzh4UvjSTyYBby.htm)|Effect: Guiding Words|
|[9p4Ieqw6qAkhc4m3.htm](feat-effects/9p4Ieqw6qAkhc4m3.htm)|Effect: Terrifying Mien|
|[9rUzqHni8LE19Gu5.htm](feat-effects/9rUzqHni8LE19Gu5.htm)|Effect: Left-Hand Blood|
|[aLrFDxT2dhIEwcQr.htm](feat-effects/aLrFDxT2dhIEwcQr.htm)|Effect: Shining Glory|
|[AsYHSc1sNEXYELj7.htm](feat-effects/AsYHSc1sNEXYELj7.htm)|Effect: Cry of Rebellion|
|[BZPPcHtYugsNKz4Z.htm](feat-effects/BZPPcHtYugsNKz4Z.htm)|Effect: Channeler's Stance|
|[C1SVTVEwhKpqSs6B.htm](feat-effects/C1SVTVEwhKpqSs6B.htm)|Effect: Apparition's Enhancement|
|[cSH9aXM0hQlF5pgp.htm](feat-effects/cSH9aXM0hQlF5pgp.htm)|Effect: Drain Realm|
|[dA8AzlayuQ1hIeEx.htm](feat-effects/dA8AzlayuQ1hIeEx.htm)|Effect: Regional Specialty|
|[dFNh66uyI5yxVKbt.htm](feat-effects/dFNh66uyI5yxVKbt.htm)|Effect: Aegis for the Innocent|
|[dn7P5RzMZugxiWri.htm](feat-effects/dn7P5RzMZugxiWri.htm)|Effect: Raise the Walls|
|[dZxFehDfiKu3M8GT.htm](feat-effects/dZxFehDfiKu3M8GT.htm)|Effect: Creature of Myth (Energy Aegis)|
|[Eh0uLxzYfheAWDpG.htm](feat-effects/Eh0uLxzYfheAWDpG.htm)|Effect: Virulent Strike|
|[ePvCQNEIhq1iQkuq.htm](feat-effects/ePvCQNEIhq1iQkuq.htm)|Effect: Godspeed|
|[eTosCwuCMf09fYYH.htm](feat-effects/eTosCwuCMf09fYYH.htm)|Effect: Game Hunter (Failure)|
|[Eu3hA5BrHX9u9Bjc.htm](feat-effects/Eu3hA5BrHX9u9Bjc.htm)|Effect: Bloc Tactics|
|[FL24cTM3EDqWLgHA.htm](feat-effects/FL24cTM3EDqWLgHA.htm)|Effect: Dancer in the Seasons|
|[fOypEG6eeTwfM6c3.htm](feat-effects/fOypEG6eeTwfM6c3.htm)|Effect: Regalia|
|[G6ytTRTwu9kBi59S.htm](feat-effects/G6ytTRTwu9kBi59S.htm)|Effect: Wild Mimic Dedication|
|[GAKXSLDwFpE5VLyx.htm](feat-effects/GAKXSLDwFpE5VLyx.htm)|Effect: Instructive Strike|
|[gjxSQgN3MopLxM35.htm](feat-effects/gjxSQgN3MopLxM35.htm)|Effect: Drink from the Chalice|
|[gyfPLgqeIgW7fYD0.htm](feat-effects/gyfPLgqeIgW7fYD0.htm)|Effect: Declare Anathema|
|[h8BLaHUFnzCDJf5I.htm](feat-effects/h8BLaHUFnzCDJf5I.htm)|Effect: Celestial Armaments|
|[ICUcmwY504WHCs3N.htm](feat-effects/ICUcmwY504WHCs3N.htm)|Effect: Wither Away|
|[iGJ6C4AUQZcSEx1W.htm](feat-effects/iGJ6C4AUQZcSEx1W.htm)|Effect: Invigorating Surge|
|[IgLMAwovQ9roTsRN.htm](feat-effects/IgLMAwovQ9roTsRN.htm)|Effect: Familiar of Bolstering Aid|
|[ik5QuOpF0IO275Nm.htm](feat-effects/ik5QuOpF0IO275Nm.htm)|Effect: Empowered Onslaught|
|[iN7j0d5LE7OwZlAo.htm](feat-effects/iN7j0d5LE7OwZlAo.htm)|Effect: Invoke Movement|
|[iYHNKt2eailYxNXc.htm](feat-effects/iYHNKt2eailYxNXc.htm)|Effect: Lord of the Fiends|
|[je3lgJso1WmyC81w.htm](feat-effects/je3lgJso1WmyC81w.htm)|Effect: Shadow Sheath Weapon|
|[jeBDJW383w5jv1BG.htm](feat-effects/jeBDJW383w5jv1BG.htm)|Effect: Soul Vessel Mount|
|[JMkGtyRU4bTSL2sB.htm](feat-effects/JMkGtyRU4bTSL2sB.htm)|Effect: Shield the Faithful|
|[jp1oRa3wEHT7hKBX.htm](feat-effects/jp1oRa3wEHT7hKBX.htm)|Effect: Harvest Blood|
|[Jt7NnE7WeR7e9V6Q.htm](feat-effects/Jt7NnE7WeR7e9V6Q.htm)|Effect: Lost in the Crowd|
|[K2hqRFjGMX0GCIUi.htm](feat-effects/K2hqRFjGMX0GCIUi.htm)|Effect: Roaring Heart|
|[KR391Ltqp0d3EWg1.htm](feat-effects/KR391Ltqp0d3EWg1.htm)|Effect: Survive the Wilds|
|[kyX0DvZEXR4Xrbdp.htm](feat-effects/kyX0DvZEXR4Xrbdp.htm)|Effect: Channel Divine Spark|
|[la8rWwUtReElgTS6.htm](feat-effects/la8rWwUtReElgTS6.htm)|Effect: Scout (Incredible Scout)|
|[lfbDqyKUMssoxUTZ.htm](feat-effects/lfbDqyKUMssoxUTZ.htm)|Effect: Game Hunter (Critical Failure)|
|[LhHJkfS5VhfJss6p.htm](feat-effects/LhHJkfS5VhfJss6p.htm)|Effect: Fly on Shadowed Wings|
|[mNc42XjBVlxcr9k8.htm](feat-effects/mNc42XjBVlxcr9k8.htm)|Effect: Marked for Rebuke|
|[MRPaYmohQ5FZkrZj.htm](feat-effects/MRPaYmohQ5FZkrZj.htm)|Effect: Fling Magic (Adept Cold)|
|[N82KSgJMeMse7uPW.htm](feat-effects/N82KSgJMeMse7uPW.htm)|Effect: Victor's Wreath|
|[N8d1R833ojGFj1Mt.htm](feat-effects/N8d1R833ojGFj1Mt.htm)|Effect: Eye-Catching Spot|
|[nBQZdTtwNLJ70J6C.htm](feat-effects/nBQZdTtwNLJ70J6C.htm)|Effect: Oath of the Defender|
|[NDCeLFdp5eJ1VYjm.htm](feat-effects/NDCeLFdp5eJ1VYjm.htm)|Effect: Zealous Inevitability|
|[niYOLA6l44U2NMM3.htm](feat-effects/niYOLA6l44U2NMM3.htm)|Effect: Bless Ally|
|[NuCwsyydiaBQpwiL.htm](feat-effects/NuCwsyydiaBQpwiL.htm)|Effect: Healer of the World|
|[ogzkGtUR9vMt47ts.htm](feat-effects/ogzkGtUR9vMt47ts.htm)|Effect: Crash Against Me|
|[ooX6QMPFfah5Yk5t.htm](feat-effects/ooX6QMPFfah5Yk5t.htm)|Effect: Don Thy Fervor|
|[ORbgwLZpFFHsaJxm.htm](feat-effects/ORbgwLZpFFHsaJxm.htm)|Effect: Celestial Armaments (Allies)|
|[OWCoFC1Q8AflBGAY.htm](feat-effects/OWCoFC1Q8AflBGAY.htm)|Effect: Unfazed Assessment|
|[OWFMglWgeCjRhqLK.htm](feat-effects/OWFMglWgeCjRhqLK.htm)|Effect: Divine Invulnerability|
|[PD1tyMvE75VU2qVi.htm](feat-effects/PD1tyMvE75VU2qVi.htm)|Effect: Force of Nature (Silver Damage)|
|[pUXV4sks4p9Jc8Wb.htm](feat-effects/pUXV4sks4p9Jc8Wb.htm)|Effect: A Challenge for Heroes|
|[PYyJjl5EEXs0IswY.htm](feat-effects/PYyJjl5EEXs0IswY.htm)|Effect: Hunt the Razer's Pawn|
|[Q2qCKyL63QzXsdGq.htm](feat-effects/Q2qCKyL63QzXsdGq.htm)|Effect: You Can't Kill an Idea|
|[qcmJGJEKHhqOhQh4.htm](feat-effects/qcmJGJEKHhqOhQh4.htm)|Effect: Herald's Weapon|
|[QjIiY4ofmhCspUJb.htm](feat-effects/QjIiY4ofmhCspUJb.htm)|Effect: Pierce the Eye|
|[QKpx9S3JeIMeqFnw.htm](feat-effects/QKpx9S3JeIMeqFnw.htm)|Effect: Healing Sanctuary|
|[qzQo3qETndgRZKcO.htm](feat-effects/qzQo3qETndgRZKcO.htm)|Effect: Amulet's Abeyance (Adept)|
|[raZ8SIBYyFux8L3U.htm](feat-effects/raZ8SIBYyFux8L3U.htm)|Effect: Feral Swing|
|[RLA7eqF2wjUPS2mf.htm](feat-effects/RLA7eqF2wjUPS2mf.htm)|Effect: Race the Skies|
|[rOKA405aECklgq42.htm](feat-effects/rOKA405aECklgq42.htm)|Stance: Divine Presence|
|[RPfpy4WzUzdwTeo0.htm](feat-effects/RPfpy4WzUzdwTeo0.htm)|Effect: Ultimatum of Liberation|
|[sEv8H8PrMd0C3wJ7.htm](feat-effects/sEv8H8PrMd0C3wJ7.htm)|Effect: Consult the Spirits|
|[sEWwGSuXxcTl020D.htm](feat-effects/sEWwGSuXxcTl020D.htm)|Effect: Crash Against Me (Clang)|
|[sgRIIdKNXbBAv4dk.htm](feat-effects/sgRIIdKNXbBAv4dk.htm)|Stance: Immovable Object|
|[SLXhPgaeKB0W3k1D.htm](feat-effects/SLXhPgaeKB0W3k1D.htm)|Effect: The Bigger They Are|
|[Sq558879cgEc1lGC.htm](feat-effects/Sq558879cgEc1lGC.htm)|Effect: Chalice Adept Benefit (Self)|
|[SX5csPb38vTBjHOb.htm](feat-effects/SX5csPb38vTBjHOb.htm)|Effect: Mythic Allies (Circumstance Bonus)|
|[U9tJvfQDdpsf5Ipd.htm](feat-effects/U9tJvfQDdpsf5Ipd.htm)|Effect: Packed with Flavor|
|[ubHYfNIf0pAwzAum.htm](feat-effects/ubHYfNIf0pAwzAum.htm)|Effect: Mirrored Aegis|
|[UE312nw1ihsc0stp.htm](feat-effects/UE312nw1ihsc0stp.htm)|Effect: Living Epic|
|[uGNTOgns4lZ0HW08.htm](feat-effects/uGNTOgns4lZ0HW08.htm)|Effect: Hunt the Razer's Pawn (Ephemeral)|
|[UNq3f2ALPG1b67ar.htm](feat-effects/UNq3f2ALPG1b67ar.htm)|Effect: Fish From the Falls' Edge|
|[VlBozDEaEaKE1A3O.htm](feat-effects/VlBozDEaEaKE1A3O.htm)|Effect: Chosen Ward|
|[vm8Ir6qfvb3BaNUs.htm](feat-effects/vm8Ir6qfvb3BaNUs.htm)|Effect: Tome Adept Benefit|
|[VrTaP6BcXo6sizn6.htm](feat-effects/VrTaP6BcXo6sizn6.htm)|Effect: Manifest Realm|
|[vSUyph3Z6wKl2l6Q.htm](feat-effects/vSUyph3Z6wKl2l6Q.htm)|Effect: Extra Alchemy|
|[VTJ8D23sOIfApEt3.htm](feat-effects/VTJ8D23sOIfApEt3.htm)|Effect: Debilitating Bomb|
|[WdxPp1pTQTUYRTA3.htm](feat-effects/WdxPp1pTQTUYRTA3.htm)|Effect: Divine Invulnerability (Domain Spell)|
|[WqjTa5mnBnBFRxqu.htm](feat-effects/WqjTa5mnBnBFRxqu.htm)|Effect: Harbinger's Armament|
|[WUvjLX4ieyISdNbr.htm](feat-effects/WUvjLX4ieyISdNbr.htm)|Effect: One with the Spirits|
|[wxVHqB06oB9yRIhT.htm](feat-effects/wxVHqB06oB9yRIhT.htm)|Stance: Unified Stance|
|[X22o74v8rk7ySnzX.htm](feat-effects/X22o74v8rk7ySnzX.htm)|Effect: Mythic Allies|
|[xHbMPpYEPwI0tmcV.htm](feat-effects/xHbMPpYEPwI0tmcV.htm)|Effect: One Moment till Glory|
|[ZdPLjPFwp9JuVRFf.htm](feat-effects/ZdPLjPFwp9JuVRFf.htm)|Effect: Only You and I|
|[zmow6d0PswEqLcrM.htm](feat-effects/zmow6d0PswEqLcrM.htm)|Effect: Storied Companion|
|[ZNpDvK1r9B8d7aZp.htm](feat-effects/ZNpDvK1r9B8d7aZp.htm)|Effect: Emboldened with Glorious Purpose|

## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[1XlJ9xLzL19GHoOL.htm](feat-effects/1XlJ9xLzL19GHoOL.htm)|Effect: Overdrive|Effet : Surrégime|changé|
|[6ctQFQfSZ6o1uyyZ.htm](feat-effects/6ctQFQfSZ6o1uyyZ.htm)|Stance: Bullet Dancer Stance|Posture : Posture du danseur de balle|changé|
|[AKKHagjg5bL1fMG5.htm](feat-effects/AKKHagjg5bL1fMG5.htm)|Effect: Overwatch Field|Effet : Champ d'observation|changé|
|[EtFMN1ZLkL7sUk01.htm](feat-effects/EtFMN1ZLkL7sUk01.htm)|Effect: Curse of Outpouring Life|Effet : Malédiction de la vie déversée|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[06oOr07YORG2km9C.htm](feat-effects/06oOr07YORG2km9C.htm)|Effect: Meddling Futures|Effet : Ingérence d'avenir|libre|
|[09oP0FBBAhXOS4JW.htm](feat-effects/09oP0FBBAhXOS4JW.htm)|Effect: Earth Impulse Junction|Effet : Jonction d'impulsion de la terre|libre|
|[0AD7BiKjT8a6Uh92.htm](feat-effects/0AD7BiKjT8a6Uh92.htm)|Effect: Energetic Meltdown|Effet : Effondrement énergétique|libre|
|[0Ai0u9kNJrEudPnN.htm](feat-effects/0Ai0u9kNJrEudPnN.htm)|Effect: Forcible Energy|Effet : Énergie vigoureuse|libre|
|[0BNGjyW5WigX1FIs.htm](feat-effects/0BNGjyW5WigX1FIs.htm)|Effect: Boaster's Challenge|Effet : Défi du vantard|libre|
|[0JrHvdUgJBl631En.htm](feat-effects/0JrHvdUgJBl631En.htm)|Effect: Juvenile Flight|Effet : Vol juvénile|libre|
|[0JvxhEv83bAD5PGv.htm](feat-effects/0JvxhEv83bAD5PGv.htm)|Effect: Familiar of Balanced Luck|Effet : Familier de la chance équilibrée|libre|
|[0kO3M46aK64a8ru8.htm](feat-effects/0kO3M46aK64a8ru8.htm)|Effect: Pistolero's Challenge|Effet : Défi du pistoléro|libre|
|[0pq3MPLH0C9z4tj3.htm](feat-effects/0pq3MPLH0C9z4tj3.htm)|Effect: Victorious Vigor|Effet : Vigueur dans la victoire|libre|
|[0r2V1nK5pV31IUPY.htm](feat-effects/0r2V1nK5pV31IUPY.htm)|Effect: Protective Mentor Boon (Revered) (PFS)|Effet : Récompense du mentor protecteur révéré (PFS)|libre|
|[18FHJoazfEmgNkfk.htm](feat-effects/18FHJoazfEmgNkfk.htm)|Effect: Aura of Preservation|Effet : Aura de préservation|libre|
|[1aMjsd08O2v5TNG2.htm](feat-effects/1aMjsd08O2v5TNG2.htm)|Effect: Fluttering Distraction|Effet : Distraction oscillante|libre|
|[1Azz9TSWXrX4aNX4.htm](feat-effects/1Azz9TSWXrX4aNX4.htm)|Effect: Harrow-Chosen|Effet : Choisi par le Tourment|libre|
|[1bzFIIEw2K1CAUe9.htm](feat-effects/1bzFIIEw2K1CAUe9.htm)|Effect: Crystal Luminescence|Effet : Luminescence du cristal|libre|
|[1dxD3xsTzak6GNj5.htm](feat-effects/1dxD3xsTzak6GNj5.htm)|Stance: Monastic Archer Stance|Posture : Posture de l'archer monastique|libre|
|[1nCwQErK6hpkNvfw.htm](feat-effects/1nCwQErK6hpkNvfw.htm)|Effect: Dueling Parry|Effet : Parade en duel|libre|
|[1ssTlwNcRRkatmPC.htm](feat-effects/1ssTlwNcRRkatmPC.htm)|Effect: Phenom Verve|Effet : Verve du phénomène|libre|
|[1VwsQa7SyUviSoFA.htm](feat-effects/1VwsQa7SyUviSoFA.htm)|Effect: Protective Cycle|Effet : Cycle protecteur|libre|
|[1WqXbwhfT1f6OrPU.htm](feat-effects/1WqXbwhfT1f6OrPU.htm)|Effect: Thermal Nimbus|Effet : Nimbe thermique|libre|
|[263Cd5JMj8Lgc9yz.htm](feat-effects/263Cd5JMj8Lgc9yz.htm)|Effect: Radiant Circuitry|Effet : Circuiterie rayonnante|libre|
|[2a6fuugiPpICe3Li.htm](feat-effects/2a6fuugiPpICe3Li.htm)|Effect: Champion's Extra Damage|Effet : Dégâts supplémentaires du champion|libre|
|[2ca1ZuqQ7VkunAh3.htm](feat-effects/2ca1ZuqQ7VkunAh3.htm)|Effect: Accept Echo|Effet : Accepter l'écho|libre|
|[2CJwgnCsYtk4R5sO.htm](feat-effects/2CJwgnCsYtk4R5sO.htm)|Effect: Path of the Tempest|Effet : Chemin de la tempête|libre|
|[2EMak2C8x6pFwoUi.htm](feat-effects/2EMak2C8x6pFwoUi.htm)|Stance: Thermal Nimbus|Posture : Nimbe thermique|libre|
|[2gVP04ZWYbQdX3uS.htm](feat-effects/2gVP04ZWYbQdX3uS.htm)|Effect: Spiral Sworn|Effet : Spirale assermentée|libre|
|[2GWZgsvMJF9DN0DO.htm](feat-effects/2GWZgsvMJF9DN0DO.htm)|Effect: Fresh Produce|Effet : Produit frais|libre|
|[2MIn8qyPTmz4ZyO1.htm](feat-effects/2MIn8qyPTmz4ZyO1.htm)|Effect: Smite Good|Effet : Châtiment du bien|libre|
|[2Qpt0CHuOMeL48rN.htm](feat-effects/2Qpt0CHuOMeL48rN.htm)|Stance: Cobra Stance (Cobra Envenom)|Posture : Posture du cobra (Cobra envenimé)|libre|
|[2wCAEtshgSzRuYNK.htm](feat-effects/2wCAEtshgSzRuYNK.htm)|Effect: Undead Blood Magic|Effet : Magie du sang mort-vivant|libre|
|[2XEYQNZTCGpdkyR6.htm](feat-effects/2XEYQNZTCGpdkyR6.htm)|Effect: Battle Medicine Immunity|Effet : Immunité à Médecine militaire|libre|
|[3bvYym3Dz6b6xmEK.htm](feat-effects/3bvYym3Dz6b6xmEK.htm)|Effect: Hot Foot (Critical Success)|Effet : Pied chaud (Succès critique)|libre|
|[3eHMqVx30JGiJqtM.htm](feat-effects/3eHMqVx30JGiJqtM.htm)|Stance: Twinned Defense|Posture : Défense jumelée|libre|
|[3gGBZHcUFsHLJeQH.htm](feat-effects/3gGBZHcUFsHLJeQH.htm)|Effect: Elemental Blood Magic (Self)|Effet : Magie du sang élémentaire (Soi)|libre|
|[3GPh6O3PJxORytAC.htm](feat-effects/3GPh6O3PJxORytAC.htm)|Effect: Shadow Sight|Effet : Vision d'ombre|libre|
|[3WzaQKb10AYLdTsQ.htm](feat-effects/3WzaQKb10AYLdTsQ.htm)|Effect: Corpse-Killer's Defiance|Effet : Défi du tueur de cadavre (niveau égal ou supérieur)|libre|
|[3xsComVKLuLn7etk.htm](feat-effects/3xsComVKLuLn7etk.htm)|Effect: Deity's Protection|Effet : Protection de la divinité|libre|
|[4a7g78DOZqHE9pRD.htm](feat-effects/4a7g78DOZqHE9pRD.htm)|Effect: Connect the Dots|Effet : Relier les points|libre|
|[4alr9e8w9H0RCLwI.htm](feat-effects/4alr9e8w9H0RCLwI.htm)|Effect: Tiller's Drive|Effet : Conduite du Laboureur|libre|
|[4HNPQ6pnMDRe8jaN.htm](feat-effects/4HNPQ6pnMDRe8jaN.htm)|Effect: Methodical Debilitations (Cover)|Effet : Handicaps méthodiques (abri)|libre|
|[4QWayYR3JSL9bk2T.htm](feat-effects/4QWayYR3JSL9bk2T.htm)|Effect: Weapon Tampered With (Success)|Effet : Arme trafiquée (succès)|libre|
|[4UNQfMrwfWirdwoV.htm](feat-effects/4UNQfMrwfWirdwoV.htm)|Effect: Masterful Hunter's Edge, Flurry|Effet : Spécialité Maître chasseur, Déluge|libre|
|[4USRFNH06dwOlgL5.htm](feat-effects/4USRFNH06dwOlgL5.htm)|Effect: Enjoy the Show|Effet : Profitez du spectacle|libre|
|[4wJM0OA9y2gsBAh7.htm](feat-effects/4wJM0OA9y2gsBAh7.htm)|Effect: Familiar of Keen Senses|Effet : Familier des sens acérés|libre|
|[4xtHFRGI05SNe9rA.htm](feat-effects/4xtHFRGI05SNe9rA.htm)|Effect: Hungry Goblin|Effet : Gobelin affamé|libre|
|[4Zj71naHbY6O9ggP.htm](feat-effects/4Zj71naHbY6O9ggP.htm)|Effect: Bristle|Effet : Hirsute|libre|
|[5bEnBqVOgdp4gROP.htm](feat-effects/5bEnBqVOgdp4gROP.htm)|Effect: Catfolk Dance|Effet : Danse homme-félin|libre|
|[5IGz4iheaiUWm5KR.htm](feat-effects/5IGz4iheaiUWm5KR.htm)|Effect: Eye of the Arclords|Effet : Oeil des Seigneurs de l'Arc|libre|
|[5PIaLkys5ZqP2BUv.htm](feat-effects/5PIaLkys5ZqP2BUv.htm)|Effect: Primal Aegis|Effet : Égide primordiale|libre|
|[5uMMLUvJOEmfMgeV.htm](feat-effects/5uMMLUvJOEmfMgeV.htm)|Effect: Shield of Faith|Effet : Bouclier de la foi|libre|
|[5v0ndPPMfZwhiVZF.htm](feat-effects/5v0ndPPMfZwhiVZF.htm)|Effect: Predictable!|Effet : Présivisible !|libre|
|[5veOBmMYQxywTudd.htm](feat-effects/5veOBmMYQxywTudd.htm)|Effect: Goblin Song (Success)|Effet : Chant gobelin - Succès|libre|
|[5yfA4sAo0CjXIYDr.htm](feat-effects/5yfA4sAo0CjXIYDr.htm)|Effect: Blade of the Heart|Effet : Lame du coeur|libre|
|[5Z250imfYqYGU2AP.htm](feat-effects/5Z250imfYqYGU2AP.htm)|Stance: Rushing Goat Stance|Posture : Posture de la charge de bouc|libre|
|[61qS86bjxg8HCJBY.htm](feat-effects/61qS86bjxg8HCJBY.htm)|Effect: Eternal Torch|Effet : Torche éternelle|libre|
|[6ACbQIpmmemxmoBJ.htm](feat-effects/6ACbQIpmmemxmoBJ.htm)|Effect: Saoc Astrology|Effet : Astrologie saoc|libre|
|[6EDoy3OSFZ4L3Vs7.htm](feat-effects/6EDoy3OSFZ4L3Vs7.htm)|Stance: Paragon's Guard|Posture : Protection du parangon|libre|
|[6fb15XuSV4TNuVAT.htm](feat-effects/6fb15XuSV4TNuVAT.htm)|Effect: Hag Blood Magic|Effet : Magie du sang guenaude|libre|
|[6fObd480rDBkFwZ3.htm](feat-effects/6fObd480rDBkFwZ3.htm)|Effect: Curse of Living Death|Effet : Malédiction de la mort vivante|libre|
|[6hh788S8hznyD66m.htm](feat-effects/6hh788S8hznyD66m.htm)|Effect: Shattershields|Effet : Plaquesboucliers|libre|
|[6I4qWkHzxj4MAt6g.htm](feat-effects/6I4qWkHzxj4MAt6g.htm)|Effect: Familiar's Resolve|Effet : Résolution du familier|libre|
|[6IsZQpwRJQWIzdGx.htm](feat-effects/6IsZQpwRJQWIzdGx.htm)|Stance: Masquerade of Seasons Stance|Posture : Posture de la mascarade des saisons|libre|
|[6OFJhFsw78w7ogW5.htm](feat-effects/6OFJhFsw78w7ogW5.htm)|Effect: Heroic Defiance|Effet : Défi héroïque|libre|
|[6VrKQ0PhRXuteusQ.htm](feat-effects/6VrKQ0PhRXuteusQ.htm)|Effect: Giant's Stature|Effet : Stature de géant|libre|
|[6YhbQmOmbmy84W1C.htm](feat-effects/6YhbQmOmbmy84W1C.htm)|Effect: Crimson Shroud|Effet : Voile pourpre|libre|
|[72THfaqak0F4XnON.htm](feat-effects/72THfaqak0F4XnON.htm)|Effect: Didactic Strike|Effet : Frappe didactique|libre|
|[78Il8Sx0iB0TuLZv.htm](feat-effects/78Il8Sx0iB0TuLZv.htm)|Effect: Terrain Attunement (Mountain)|Effet : Harmonisations de terrain (Montagne)|libre|
|[7AzGhIOx4Df5Cicr.htm](feat-effects/7AzGhIOx4Df5Cicr.htm)|Effect: Vigorous Anthem|Effet : Hymne vigoureux|libre|
|[7BFd8A9HFrmg6vwL.htm](feat-effects/7BFd8A9HFrmg6vwL.htm)|Effect: Psychopomp Blood Magic (Self)|Effet : Magie du sang psychopompe (Soi)|libre|
|[7cG8kpQvh2oyBV8d.htm](feat-effects/7cG8kpQvh2oyBV8d.htm)|Effect: Stone Body|Effet : Corps de pierre|libre|
|[7Eh8gHNcRNs9iIR8.htm](feat-effects/7Eh8gHNcRNs9iIR8.htm)|Stance: Twisting Petal Stance|Posture : Posture du pétale tournoyant|libre|
|[7hiwUPQY5aBEkpIt.htm](feat-effects/7hiwUPQY5aBEkpIt.htm)|Effect: Blood Sovereignty|Effet : Blood Sovereignty|libre|
|[7hQnwwsixZmXzdIT.htm](feat-effects/7hQnwwsixZmXzdIT.htm)|Effect: Channel the Godmind|Effet : Canaliser l'esprit divin|libre|
|[7hRgBo0fRQBxMK7g.htm](feat-effects/7hRgBo0fRQBxMK7g.htm)|Effect: Distracting Feint|Effet : Feinte de diversion|libre|
|[7juD2XQ7wKI5pjPW.htm](feat-effects/7juD2XQ7wKI5pjPW.htm)|Effect: Spark Fist|Effet : Poing à étincelles|libre|
|[7MQLLkQACZt8cspt.htm](feat-effects/7MQLLkQACZt8cspt.htm)|Effect: Purifying Breeze|Effet : Brise purifiante|libre|
|[7ogytOgDmh4h2g5d.htm](feat-effects/7ogytOgDmh4h2g5d.htm)|Effect: Heroic Recovery|Effet : Récupération héroïque|libre|
|[7XxZ4YJ6c97D9Fc9.htm](feat-effects/7XxZ4YJ6c97D9Fc9.htm)|Effect: Oracular Warning|Effet : Avertissement oraculaire|libre|
|[85w9OI1gMd6uJRKh.htm](feat-effects/85w9OI1gMd6uJRKh.htm)|Effect: Curse Maelstrom State|Effet : État de Maelström maudit|libre|
|[8E5SCmFndGAvgkTw.htm](feat-effects/8E5SCmFndGAvgkTw.htm)|Effect: Energize Wings|Effet : Énergiser les ailes|libre|
|[8HGz3rQCHNgdAqFC.htm](feat-effects/8HGz3rQCHNgdAqFC.htm)|Effect: Current Spell|Effet : Courant de sort|libre|
|[8hmw9L2ORKz6Z6Bc.htm](feat-effects/8hmw9L2ORKz6Z6Bc.htm)|Stance: Drifting Pollen|Posture : Pollen flottant|libre|
|[8iLsH0McVDbPGMJ3.htm](feat-effects/8iLsH0McVDbPGMJ3.htm)|Effect: Oni Form|Effet : Forme d'Oni|libre|
|[8rDbWcWmQL0N5FFG.htm](feat-effects/8rDbWcWmQL0N5FFG.htm)|Effect: Azarketi Purification|Effet : Purification azarketie|libre|
|[939OHjW9y8uCmDk3.htm](feat-effects/939OHjW9y8uCmDk3.htm)|Effect: Unleash Psyche|Effet : Déchaîner la psyché|libre|
|[94MzLpLykQIWKcA1.htm](feat-effects/94MzLpLykQIWKcA1.htm)|Effect: Deteriorated|Effet : Détérioré|libre|
|[9AUcoY48H5LrVZiF.htm](feat-effects/9AUcoY48H5LrVZiF.htm)|Effect: Genie Blood Magic|Effet : Magie du sang génie|libre|
|[9dCt0asv0kt7DR4q.htm](feat-effects/9dCt0asv0kt7DR4q.htm)|Effect: Liberating Step (vs. Fiend)|Effet : Pas libérateur (Fiélon)|libre|
|[9HPxAKpP3WJmICBx.htm](feat-effects/9HPxAKpP3WJmICBx.htm)|Stance: Point Blank Stance|Posture : Tir à bout portant|libre|
|[9kNbiZPOM2wy60ao.htm](feat-effects/9kNbiZPOM2wy60ao.htm)|Effect: Ceremony of Protection|Effet : Cérémonie de protection|libre|
|[9O4q3bYlWkuC24vh.htm](feat-effects/9O4q3bYlWkuC24vh.htm)|Effect: Come and Get Me (Circumstance Bonus)|Effet : Je t'attends (Bonus de circonstances)|libre|
|[9ZrfvQxxcdzlND3y.htm](feat-effects/9ZrfvQxxcdzlND3y.htm)|Effect: Cantorian Rejuvenation|Effet : Rajeunissement cantorien|libre|
|[A06Qu3BNMQ4FGinM.htm](feat-effects/A06Qu3BNMQ4FGinM.htm)|Effect: Indomitable Spirit|Effet : Esprit irréductible|libre|
|[a4C89wVlcwV6Uxx4.htm](feat-effects/a4C89wVlcwV6Uxx4.htm)|Effect: Overdrive Ally|Effet : Allié en surrégime|libre|
|[A6i55HQ59lzsHXVQ.htm](feat-effects/A6i55HQ59lzsHXVQ.htm)|Effect: Raise Symbol|Effet : Lever le symbole|libre|
|[a7qiSYdlaIRPe57i.htm](feat-effects/a7qiSYdlaIRPe57i.htm)|Effect: Watchful Gaze|Effet : Regard vigilant|libre|
|[ACnP67mu97ZZMEz0.htm](feat-effects/ACnP67mu97ZZMEz0.htm)|Effect: Unending Subsistence|Effet : Subsistance infinie|libre|
|[aEuDaQY1GnrrnDRA.htm](feat-effects/aEuDaQY1GnrrnDRA.htm)|Effect: Aldori Parry|Effet : Parade aldorie|libre|
|[AggnVD5loQHLb7zj.htm](feat-effects/AggnVD5loQHLb7zj.htm)|Effect: Raise Menhir|Effet : Ériger un menhir|libre|
|[aKRo5TIhUtu0kyEr.htm](feat-effects/aKRo5TIhUtu0kyEr.htm)|Effect: Demonic Blood Magic|Effet : Magie du sang démoniaque|libre|
|[AlnxieIRjqNqsdVu.htm](feat-effects/AlnxieIRjqNqsdVu.htm)|Effect: Smite|Effet : Châtiment|libre|
|[AOqlEOF3Q7w9XYFp.htm](feat-effects/AOqlEOF3Q7w9XYFp.htm)|Effect: Pushing Wind|Effet : Vent porteur|libre|
|[aUpcWqaLBlmpnJgW.htm](feat-effects/aUpcWqaLBlmpnJgW.htm)|Effect: Legendary Monster Warden|Effet : Garde-monstres légendaire|libre|
|[b2kWJuCPj1rDMdwz.htm](feat-effects/b2kWJuCPj1rDMdwz.htm)|Stance: Wolf Stance|Posture : Posture du loup|libre|
|[BBGg5gpMmuBSo7Mi.htm](feat-effects/BBGg5gpMmuBSo7Mi.htm)|Effect: Deep Freeze|Effet : Congélation profonde|libre|
|[BC92TyFzRCWq8fu0.htm](feat-effects/BC92TyFzRCWq8fu0.htm)|Effect: Great Tengu Form|Effet : Forme du grand tengu|libre|
|[BCyGDKcplkJiSAKJ.htm](feat-effects/BCyGDKcplkJiSAKJ.htm)|Stance: Stumbling Stance|Posture : Posture chancelante|libre|
|[BHnunYPROBG5lxv4.htm](feat-effects/BHnunYPROBG5lxv4.htm)|Effect: Heroes' Call|Effet : Appel des héros|libre|
|[BIHU3o499fsa1bwt.htm](feat-effects/BIHU3o499fsa1bwt.htm)|Effect: Psi Strikes|Effet : Frappes psy|libre|
|[bIRIS6mnynr72RDw.htm](feat-effects/bIRIS6mnynr72RDw.htm)|Effect: Goblin Song (Critical Success)|Effet : Chant gobelin - Succès critique|libre|
|[BJc494USeyM011p3.htm](feat-effects/BJc494USeyM011p3.htm)|Effect: Replenishment of War|Effet : Récupération martiale|libre|
|[bmcvernfk6l4EbAG.htm](feat-effects/bmcvernfk6l4EbAG.htm)|Effect: Hopeful Athamaru|Effet : Athamaru pleine d'espoir|libre|
|[bmVwaN0C4e9fE2Sz.htm](feat-effects/bmVwaN0C4e9fE2Sz.htm)|Effect: Bolera's Interrogation|Effet : Interrogatoire de Boléra|libre|
|[bqbliPkslR5hkAcB.htm](feat-effects/bqbliPkslR5hkAcB.htm)|Effect: Blessed Swiftness|Effet : Célérité bénie|libre|
|[BTI4obxa5UmNltyU.htm](feat-effects/BTI4obxa5UmNltyU.htm)|Effect: Castigating Weapon|Effet : Arme punitive|libre|
|[buCKRBDZi27shB3X.htm](feat-effects/buCKRBDZi27shB3X.htm)|Stance: Sea Glass Guardians|Posture : Gardiens de la mer de verre|libre|
|[bvk5rwYSoTtz8QGf.htm](feat-effects/bvk5rwYSoTtz8QGf.htm)|Effect: Accursed Clay Fist|Effet : Poing de glaise maudit|libre|
|[bWvOkRT3alzllsiG.htm](feat-effects/bWvOkRT3alzllsiG.htm)|Effect: Kindle Inner Flames|Effet : Flammes de la chandelle intérieure|libre|
|[bxxek5BP4zqP5GzM.htm](feat-effects/bxxek5BP4zqP5GzM.htm)|Effect: Guide the Timeline|Effet : Guider le flot temporel|libre|
|[ByXEvk0gyFwUk5gU.htm](feat-effects/ByXEvk0gyFwUk5gU.htm)|Effect: Thoughtform Summoning|Effet : Invocation mentalisée|libre|
|[C6H3gF5HTdsIKpOC.htm](feat-effects/C6H3gF5HTdsIKpOC.htm)|Effect: Improved Poison Weapon|Effet : Arme empoisonnée améliorée|libre|
|[cA6ps8RKE0gysEWr.htm](feat-effects/cA6ps8RKE0gysEWr.htm)|Effect: Prayer-Touched Weapon|Effet : Arme touchée par la grâce|libre|
|[CdWAXaePkMP4hts9.htm](feat-effects/CdWAXaePkMP4hts9.htm)|Effect: Combat Premonition|Effet : Prémonition du combat|libre|
|[cGwFYusGTsJR3x3P.htm](feat-effects/cGwFYusGTsJR3x3P.htm)|Effect: Under Control|Effet : Sous contrôle|libre|
|[CgxYa0lrLUjS2ZhI.htm](feat-effects/CgxYa0lrLUjS2ZhI.htm)|Stance: Cobra Stance|Posture : Posture du cobra|libre|
|[cmCLIMgtd4TLA23p.htm](feat-effects/cmCLIMgtd4TLA23p.htm)|Stance: Steam Knight|Posture : Chevalier vapeur|libre|
|[cmwlVYHtp3a1OipU.htm](feat-effects/cmwlVYHtp3a1OipU.htm)|Effect: Siphon Torment|Effet : Tourment siphonné|libre|
|[COsdMolZraFRTdP8.htm](feat-effects/COsdMolZraFRTdP8.htm)|Effect: Prevailing Position|Effet : Position prédominante|libre|
|[CQfkyJkRHw4IHWhv.htm](feat-effects/CQfkyJkRHw4IHWhv.htm)|Stance: Sky and Heaven Stance|Posture : Posture du Ciel et du Paradis|libre|
|[cqgbTZCvqaSvtQdz.htm](feat-effects/cqgbTZCvqaSvtQdz.htm)|Effect: Encroaching Presence|Effet : Présence envahissante|libre|
|[ctiTtuRWFjAnWdYQ.htm](feat-effects/ctiTtuRWFjAnWdYQ.htm)|Effect: Corpse-Killer's Defiance (Lower Level)|Effet : Défi du tueur de cadavre (niveau inférieur)|libre|
|[cu10OjQUdY0a9gpj.htm](feat-effects/cu10OjQUdY0a9gpj.htm)|Effect: Exemplary Finisher (Wit)|Effet : Aboutissement exemplaire (esprit)|libre|
|[CUtvkuGSxq1raBIB.htm](feat-effects/CUtvkuGSxq1raBIB.htm)|Effect: Shared Clarity|Effet : Clarté partagée|libre|
|[cvIwUEempg0cfhFB.htm](feat-effects/cvIwUEempg0cfhFB.htm)|Stance: Talon Stance|Posture : Posture de serre|libre|
|[CW4zphOOpeaLJIWc.htm](feat-effects/CW4zphOOpeaLJIWc.htm)|Effect: Recall Under Pressure|Effet : Souvenir sous pression|libre|
|[CXMDSu8pKh0OJkUo.htm](feat-effects/CXMDSu8pKh0OJkUo.htm)|Effect: Emblazon Armament|Effet : Arsenal blasonné|libre|
|[CxT12G5HXpGL6xu0.htm](feat-effects/CxT12G5HXpGL6xu0.htm)|Stance: Claw Stance|Posture : Posture de griffe|libre|
|[CYmipBeliNuymENB.htm](feat-effects/CYmipBeliNuymENB.htm)|Effect: Come and Get Me (Temporary Hit Points)|Effet : Je t'attends (Points de vie temporaires)|libre|
|[cYtUBB2cO0FOUU8D.htm](feat-effects/cYtUBB2cO0FOUU8D.htm)|Effect: Hone Claws|Effet : Griffes aiguisées|libre|
|[d0TYadetRJvNb2Au.htm](feat-effects/d0TYadetRJvNb2Au.htm)|Effect: Steal Death|Effet : Voler la mort|libre|
|[D1SUxDvtDyqwdXqO.htm](feat-effects/D1SUxDvtDyqwdXqO.htm)|Effect: Celestial Yaoguai Might|Effet : Libérer la puissance yaoguai|libre|
|[D7JhDc4dcWhORY9h.htm](feat-effects/D7JhDc4dcWhORY9h.htm)|Effect: Proud Mentor|Effet : Fier Mentor|libre|
|[DawVHfoPKbPJsz4k.htm](feat-effects/DawVHfoPKbPJsz4k.htm)|Effect: Champion's Resistance|Effet : Résistance du champion|libre|
|[Dbr5hInQXH904Ca7.htm](feat-effects/Dbr5hInQXH904Ca7.htm)|Effect: Psychic Rapport|Effet : Rapport psychique|libre|
|[DjxZpQ4xJWWvYQqY.htm](feat-effects/DjxZpQ4xJWWvYQqY.htm)|Effect: Repair Module|Effet : Module de réparation|libre|
|[DrM8d4joWHAaGlbe.htm](feat-effects/DrM8d4joWHAaGlbe.htm)|Effect: Divine Health|Effet : Santé divine|libre|
|[dTymNXgTtnjqgYP9.htm](feat-effects/dTymNXgTtnjqgYP9.htm)|Effect: Emotional Surge|Effet : Surcharge émotionnelle|libre|
|[DVIxPdgUN8T5LwIp.htm](feat-effects/DVIxPdgUN8T5LwIp.htm)|Effect: Continuous Flair|Effet : Élégance continuelle|libre|
|[DvyyA11a63FBwV7x.htm](feat-effects/DvyyA11a63FBwV7x.htm)|Effect: Known Weakness|Effet : Faiblesses connues|libre|
|[DWrsDJte9sez0Ppi.htm](feat-effects/DWrsDJte9sez0Ppi.htm)|Effect: Rampaging Form|Effet : Forme destructrice|libre|
|[DX6v0lcI2uA99Yjv.htm](feat-effects/DX6v0lcI2uA99Yjv.htm)|Effect: Sixth Pillar Mastery|Effet : Maîtrise du sixième pilier|libre|
|[DyX4E7KDkzRnDxzc.htm](feat-effects/DyX4E7KDkzRnDxzc.htm)|Effect: Perfect Resistance|Effet : Résistance parfaite|libre|
|[e6mv68aarIbQ3tXL.htm](feat-effects/e6mv68aarIbQ3tXL.htm)|Effect: Undying Conviction|Effet : Conviction immortelle|libre|
|[E9GohJi4hZ4vSLNu.htm](feat-effects/E9GohJi4hZ4vSLNu.htm)|Effect: Terrain Attunement (Arctic)|Effet : Harmonisation de terrain (Arctique)|libre|
|[eaKB3rIkJyfsow5H.htm](feat-effects/eaKB3rIkJyfsow5H.htm)|Effect: Animate Net|Effet : Animation de filet|libre|
|[ebCWQB5nfK19GpY5.htm](feat-effects/ebCWQB5nfK19GpY5.htm)|Stance: Geologic Attunement|Posture : Harmonisation géologique|libre|
|[EcVitI0Wqu3uNfaK.htm](feat-effects/EcVitI0Wqu3uNfaK.htm)|Effect: Dissolution's Sight|Effet : Vision de dissolution|libre|
|[ecWVnrjsjubZ4On6.htm](feat-effects/ecWVnrjsjubZ4On6.htm)|Effect: Stoney Deflection|Effet : Déviation rocheuse|libre|
|[ed9iJxdHuft6bDFF.htm](feat-effects/ed9iJxdHuft6bDFF.htm)|Effect: Deadly Poison Weapon|Effet : Arme empoisonnée mortelle|libre|
|[EDpjey6SCdvIYqEc.htm](feat-effects/EDpjey6SCdvIYqEc.htm)|Effect: Twin Parry (Parry Trait)|Effet : Parade jumelée (Trait parade)|libre|
|[eeAlh6edygcZIz9c.htm](feat-effects/eeAlh6edygcZIz9c.htm)|Stance: Wild Winds Stance|Posture : Posture des vents violents|libre|
|[eecIuD6sGPX0FJcT.htm](feat-effects/eecIuD6sGPX0FJcT.htm)|Effect: Divine Infusion|Effet : Imprégnation divine|libre|
|[EEujvJ9UzzK7fvqG.htm](feat-effects/EEujvJ9UzzK7fvqG.htm)|Effect: Overdrive Ally (Shared Overdrive)|Effet : Allié en surrégime (Surrégime partagé)|libre|
|[Ef63SlxSKlZmHosM.htm](feat-effects/Ef63SlxSKlZmHosM.htm)|Effect: Exalted Reaction (Desecrator)|Effet : Réaction exaltée (Profanation)|libre|
|[EfMaI6AnROP4X9lN.htm](feat-effects/EfMaI6AnROP4X9lN.htm)|Effect: Mountain Stronghold|Effet : Bastion de la montagne|libre|
|[EgOcWruvNdVIpNAC.htm](feat-effects/EgOcWruvNdVIpNAC.htm)|Effect: Revitalizing Finisher|Effet : Aboutissement revitalisant|libre|
|[ELCODoiGDBEqRAtT.htm](feat-effects/ELCODoiGDBEqRAtT.htm)|Effect: Legendary Monster|Effet : Monstre légendaire|libre|
|[emSh1VxHVtTmt925.htm](feat-effects/emSh1VxHVtTmt925.htm)|Effect: Methodical Debilitations (Flanking)|Effet : Handicaps méthodiques (tenaille)|libre|
|[eMsI1lR0SuJBCYjn.htm](feat-effects/eMsI1lR0SuJBCYjn.htm)|Effect: Consume Energy (Augment Strike)|Effet : Consommer l'énergie (Augmenter la Frappe)|libre|
|[EQCnu8DGHDDNXch0.htm](feat-effects/EQCnu8DGHDDNXch0.htm)|Effect: Reanimator Dedication|Effet : Dévouement : Réanimateur|libre|
|[er5tvDNvpbcnlbHQ.htm](feat-effects/er5tvDNvpbcnlbHQ.htm)|Stance: Inspiring Marshal Stance|Posture : Posture du capitaine inspirant|libre|
|[ErLweSmVAN57QIpp.htm](feat-effects/ErLweSmVAN57QIpp.htm)|Effect: Nanite Surge|Effet : Poussée nanite|libre|
|[eS9I5kaig0i1j3z0.htm](feat-effects/eS9I5kaig0i1j3z0.htm)|Effect: Field Discovery (Chirurgeon)|Effet : Découverte de domaine (Chirurgien)|libre|
|[ESnzqtwSgahLcxg2.htm](feat-effects/ESnzqtwSgahLcxg2.htm)|Effect: Hamstringing Strike|Effet : Frappe aux ischio-jambiers|libre|
|[ETZHTCjlOUHHSE0Z.htm](feat-effects/ETZHTCjlOUHHSE0Z.htm)|Effect: Oracular Warning (Initiative)|Effet : Avertissement oraculaire (Initiative)|libre|
|[eu2HidLHaGKe4MPn.htm](feat-effects/eu2HidLHaGKe4MPn.htm)|Effect: Twin Parry|Effet : Parade jumelée}|libre|
|[EVRcdGt4awWPgXla.htm](feat-effects/EVRcdGt4awWPgXla.htm)|Effect: Arcane Propulsion|Effet : Propulsion arcanique|libre|
|[ewfHVi6NWBGM8B6f.htm](feat-effects/ewfHVi6NWBGM8B6f.htm)|Effect: Disrupting Strikes|Effet : Frappes vitalisantes|libre|
|[EWiuxy4H1mCVcmud.htm](feat-effects/EWiuxy4H1mCVcmud.htm)|Stance: Intensified Element Stance|Posture : Posture élémentaire intensifiée|libre|
|[EzgW32MCOGov9h5C.htm](feat-effects/EzgW32MCOGov9h5C.htm)|Effect: Striking Retribution|Effet : Rétribution frappante|libre|
|[FaEvoqZXfQ7xnpRo.htm](feat-effects/FaEvoqZXfQ7xnpRo.htm)|Effect: Gunpowder Gauntlet|Effet : Attraction balistique|libre|
|[fgz7lwg1xHsVW4dX.htm](feat-effects/fgz7lwg1xHsVW4dX.htm)|Effect: Exemplary Finisher (Rascal)|Effet : Aboutissement exemplaire - Fripouille|libre|
|[fh8TgCfiifVk0eqU.htm](feat-effects/fh8TgCfiifVk0eqU.htm)|Effect: Magical Mentor Boon (PFS)|Effet : Récompense de mentor magique (PFS)|libre|
|[FHBYfq3w7ddLvzrK.htm](feat-effects/FHBYfq3w7ddLvzrK.htm)|Effect: Orchard's Endurance|Effet : Endurance du verger|libre|
|[fHM9xvCBWrBqcDQY.htm](feat-effects/fHM9xvCBWrBqcDQY.htm)|Effect: Acupuncturist|Effet : Acupuncteur|libre|
|[FIgud5jqZgIjwkRE.htm](feat-effects/FIgud5jqZgIjwkRE.htm)|Effect: Maiden's Mending|Effet : Guérison de la vierge|libre|
|[fILVhS5NuCtGXfri.htm](feat-effects/fILVhS5NuCtGXfri.htm)|Effect: Wyrmblessed Blood Magic|Effet : Magie du sang Béni du ver|libre|
|[fIRJftsmd83iSoR4.htm](feat-effects/fIRJftsmd83iSoR4.htm)|Effect: Expel Maelstrom (Success)|Effet : Expulser le maelström (Succès)|libre|
|[FNTTeJHiK6iOjrSq.htm](feat-effects/FNTTeJHiK6iOjrSq.htm)|Effect: Draconic Blood Magic|Effet : Magie du sang draconique|libre|
|[fo8kb5GW0OF3HPKK.htm](feat-effects/fo8kb5GW0OF3HPKK.htm)|Stance: Kaiju Stance|Posture : Posture kaiju|libre|
|[FPuICuxBLiDaEbDX.htm](feat-effects/FPuICuxBLiDaEbDX.htm)|Effect: Aura of Life|Effet : Aura de vie|libre|
|[fqK8TsXmHr1JiM5e.htm](feat-effects/fqK8TsXmHr1JiM5e.htm)|Effect: Soothing Vials|Effet : Fioles apaisantes|libre|
|[fRlvmul3LbLo2xvR.htm](feat-effects/fRlvmul3LbLo2xvR.htm)|Effect: Living Fortification|Effet : Fortification vivante|libre|
|[fsjO5oTKttsbpaKl.htm](feat-effects/fsjO5oTKttsbpaKl.htm)|Stance: Arcane Cascade|Posture : Cascade arcanique|libre|
|[fSZH0aVKyPxatE6i.htm](feat-effects/fSZH0aVKyPxatE6i.htm)|Effect: Upstage|Effet : Surpasser|libre|
|[FyaekbWsazkJhJda.htm](feat-effects/FyaekbWsazkJhJda.htm)|Effect: Decry Thief|Effet : Fléau des voleurs|libre|
|[G4L49aMxHqO2yqxi.htm](feat-effects/G4L49aMxHqO2yqxi.htm)|Effect: Curse of Creeping Ashes|Effet : Malédiction des cendres rampantes|libre|
|[G7tYqJYFX8YBnF3r.htm](feat-effects/G7tYqJYFX8YBnF3r.htm)|Effect: Big Debut|Effet : Grande première|libre|
|[GboCEoWR636VR9Z6.htm](feat-effects/GboCEoWR636VR9Z6.htm)|Effect: Geobukseon Retaliation|Effet : Représaille de geobukseon|libre|
|[gcR66Xgi12ICOVt7.htm](feat-effects/gcR66Xgi12ICOVt7.htm)|Stance: Desert Wind|Posture : Vent du désert|libre|
|[gDlOd7z3EUkjW7DU.htm](feat-effects/gDlOd7z3EUkjW7DU.htm)|Effect: Worm Form|Effet : Forme de ver|libre|
|[GFLiTESBmoyQ3sLx.htm](feat-effects/GFLiTESBmoyQ3sLx.htm)|Effect: Exalted Reaction (Redemption)|Effet : Réaction exaltée (Rédemption)|libre|
|[GGebXpRPyONZB3eS.htm](feat-effects/GGebXpRPyONZB3eS.htm)|Stance: Everstand Stance|Posture : Posture toujours en position|libre|
|[ghZFZWUh5Z20vOlR.htm](feat-effects/ghZFZWUh5Z20vOlR.htm)|Effect: Fortify Shield|Effet : Bouclier fortifié|libre|
|[gN1LbKYQgi8Fx98V.htm](feat-effects/gN1LbKYQgi8Fx98V.htm)|Effect: Anadi Venom|Effet : Venin anadi|libre|
|[GnEzDtPBQ1lDwmZI.htm](feat-effects/GnEzDtPBQ1lDwmZI.htm)|Effect: Lava Leap|Effet : Bond de lave|libre|
|[GoSls6SKCFmSoDxT.htm](feat-effects/GoSls6SKCFmSoDxT.htm)|Effect: Bon Mot|Effet : Bon Mot|libre|
|[gtpqoCOWhF9Pjb8G.htm](feat-effects/gtpqoCOWhF9Pjb8G.htm)|Effect: Roll the Bones of Fate (Good)|Effet : Osselets du destin (Bon)|libre|
|[gvE0iQ7h6263Mkhi.htm](feat-effects/gvE0iQ7h6263Mkhi.htm)|Effect: Crimson Shroud (AC)|Effet : Voile pourpre (CA)|libre|
|[GvqB4M8LrHpzYEvl.htm](feat-effects/GvqB4M8LrHpzYEvl.htm)|Stance: Fane's Fourberie|Posture : Fourberie de Fane|libre|
|[gWwG7MNAesJgpmRW.htm](feat-effects/gWwG7MNAesJgpmRW.htm)|Effect: Cut from the Air|Effet : Découper en l'air|libre|
|[gYpy9XBPScIlY93p.htm](feat-effects/gYpy9XBPScIlY93p.htm)|Stance: Mountain Stance|Posture : Posture de la montagne|libre|
|[h45sUZFs5jhuQdCE.htm](feat-effects/h45sUZFs5jhuQdCE.htm)|Stance: Vitality-Manipulating Stance|Posture : Posture de manipulation de la vitalité|libre|
|[h6nyMp4dtPXBfCJc.htm](feat-effects/h6nyMp4dtPXBfCJc.htm)|Effect: Selfish Shield|Effet : Bouclier égoïste|libre|
|[h8kgxDMclDHj6bIr.htm](feat-effects/h8kgxDMclDHj6bIr.htm)|Effect: Sword Practice|Effet : Pratique de l'épée|libre|
|[HcNa7gv7a357CnFi.htm](feat-effects/HcNa7gv7a357CnFi.htm)|Effect: Inertial Barrier|Effet : Barrière inertielle|libre|
|[HfXGhXc9D120gvl5.htm](feat-effects/HfXGhXc9D120gvl5.htm)|Effect: Divine Wings|Effet : Ailes divines (aasimar)|libre|
|[hGj6fnFwQwczmubB.htm](feat-effects/hGj6fnFwQwczmubB.htm)|Effect: Renewing Cycle|Effet : Cycle de renouvellement|libre|
|[HKPmrxkZwHRND5Um.htm](feat-effects/HKPmrxkZwHRND5Um.htm)|Effect: Favored Terrain (Increase Swim Speed)|Effet : Environnement de prédilection (Augmenter la Vitesse de nage)|libre|
|[HohNWN5sdJW3naYe.htm](feat-effects/HohNWN5sdJW3naYe.htm)|Effect: Howling Aspect|Effet : Aspect hurlant|libre|
|[HoNojV4S2Yb6CoZR.htm](feat-effects/HoNojV4S2Yb6CoZR.htm)|Effect: Steel Yourself!|Effet : Prépare toi !|libre|
|[hqeR9faxHj0NDFFP.htm](feat-effects/hqeR9faxHj0NDFFP.htm)|Effect: Curse of Engulfing Flames|Effet : Malédiction du linceul de flammes|libre|
|[hrEG2smy3tZyGxIn.htm](feat-effects/hrEG2smy3tZyGxIn.htm)|Stance: Kindle Inner Flames|Posture : Flammes de la chandelle intérieure|libre|
|[I0g5oaSwaqZ8fFAV.htm](feat-effects/I0g5oaSwaqZ8fFAV.htm)|Effect: Curse of the Perpetual Storm|Effet : Malédiction de la tempête perpétuelle|libre|
|[iAePXmGL9bMKBTpv.htm](feat-effects/iAePXmGL9bMKBTpv.htm)|Effect: Life-Draining|Effet : Draineuse de vie|libre|
|[IAwtIy6VVel7NexY.htm](feat-effects/IAwtIy6VVel7NexY.htm)|Effect: Ghost Strike|Effet : Frappe spectrale|libre|
|[iaZ6P59YVhdnFIN8.htm](feat-effects/iaZ6P59YVhdnFIN8.htm)|Effect: Guided Skill|Effet : Compétence guidée|libre|
|[IfRkgjyh0JzGalIy.htm](feat-effects/IfRkgjyh0JzGalIy.htm)|Effect: Armor Tampered With (Success)|Effet : Armure trafiquée avec succès|libre|
|[IfsglZ7fdegwem0E.htm](feat-effects/IfsglZ7fdegwem0E.htm)|Effect: Hydraulic Deflection|Effet : Manoeuvres hydrauliques|libre|
|[IhwcqxPt1SkgXQB0.htm](feat-effects/IhwcqxPt1SkgXQB0.htm)|Stance: Stretching Reach|Posture : Extension de l'allonge|libre|
|[Im5JBInybWFbHEYS.htm](feat-effects/Im5JBInybWFbHEYS.htm)|Stance: Rain of Embers Stance|Posture : Posture de la pluie de charbons ardents|libre|
|[ImkjllInxmrdDCOq.htm](feat-effects/ImkjllInxmrdDCOq.htm)|Effect: Sanctify Armament|Effet : Arsenal sanctifié|libre|
|[iMY1ClfWdn50s0PM.htm](feat-effects/iMY1ClfWdn50s0PM.htm)|Effect: Command Attention|Effet : Attirer l'attention|libre|
|[ipaniua1HJ4qGzcr.htm](feat-effects/ipaniua1HJ4qGzcr.htm)|Effect: Energy Ward|Effet : Protection énergétique|libre|
|[IpRfT9lL3YR6MH6w.htm](feat-effects/IpRfT9lL3YR6MH6w.htm)|Effect: Favored Terrain (Increase Climb Speed)|Effet : Environnement de prédilection (Augmenter la Vitesse d'escalade)|libre|
|[iqvurepX0zyu9OlI.htm](feat-effects/iqvurepX0zyu9OlI.htm)|Effect: Masterful Hunter's Edge, Outwit|Effet : Spécialité Maître chasseur, Ruse|libre|
|[ITvyvbB234bxceRK.htm](feat-effects/ITvyvbB234bxceRK.htm)|Effect: Mutate Weapon|Effet : Arme mutante|libre|
|[ivGiUp0EC5nWT9Hb.htm](feat-effects/ivGiUp0EC5nWT9Hb.htm)|Effect: Read Shibboleths|Effet : Lire les signes distinctifs|libre|
|[iyONT1qgeRgoYHsZ.htm](feat-effects/iyONT1qgeRgoYHsZ.htm)|Effect: Liberating Step (vs. Dragon)|Effet : Pas libérateur (contre un dragon)|libre|
|[j6DbvXzg5lTgw3Bq.htm](feat-effects/j6DbvXzg5lTgw3Bq.htm)|Effect: Can't Fall Here|Effet : Tu ne peux tomber ici|libre|
|[J8WkLtuY0RpcjI8q.htm](feat-effects/J8WkLtuY0RpcjI8q.htm)|Effect: Hasty Celebration|Effet : Célébration hâtive|libre|
|[jACKRmVfr9ATsmwg.htm](feat-effects/jACKRmVfr9ATsmwg.htm)|Effect: Devrin's Cunning Stance|Effet : Posture astucieuse de Devrin|libre|
|[jDlnvm9QjA7A2weD.htm](feat-effects/jDlnvm9QjA7A2weD.htm)|Effect: Volatile Grease (Success)|Effet : Graisse volatile (Succès)|libre|
|[JefXqvhzUeBArkAP.htm](feat-effects/JefXqvhzUeBArkAP.htm)|Stance: Whirling Blade Stance|Posture : Posture de la lame tournoyante|libre|
|[JF2xCqL6t4UJZtUi.htm](feat-effects/JF2xCqL6t4UJZtUi.htm)|Effect: Blizzard Evasion|Effet : Évasion du blizzard|libre|
|[jlZjUtrfcfIWumSe.htm](feat-effects/jlZjUtrfcfIWumSe.htm)|Effect: Renewed Vigor|Effet : Regain de vigueur|libre|
|[JmAYeBSVvr5QMNis.htm](feat-effects/JmAYeBSVvr5QMNis.htm)|Effect: Starlit Transformation|Effet : Transformation stellaire|libre|
|[jMQi2kDirzGgddti.htm](feat-effects/jMQi2kDirzGgddti.htm)|Effect: Divine Rebuttal|Effet : Réfutation divine|libre|
|[jO7wMhnjT7yoAtQg.htm](feat-effects/jO7wMhnjT7yoAtQg.htm)|Effect: Root Magic|Effet : Magie des racines|libre|
|[JQUoBlZKT5N5zO5k.htm](feat-effects/JQUoBlZKT5N5zO5k.htm)|Effect: Avenge in Glory|Effet : Revanche dans la gloire|libre|
|[JUgx48XHMz4QM4Ir.htm](feat-effects/JUgx48XHMz4QM4Ir.htm)|Effect: Tactical Debilitations (No Flanking)|Effet : Handicaps tactiques (Pas de tenaille)|libre|
|[jUUjgpEEOeZeG7II.htm](feat-effects/jUUjgpEEOeZeG7II.htm)|Effect: Emblazon Energy|Effet : Énergie blasonnée|libre|
|[jw5uOE1sJ9o9bUEb.htm](feat-effects/jw5uOE1sJ9o9bUEb.htm)|Effect: Mentor of Legends|Effet : Mentor des légendes|libre|
|[JW93FDTAOUNuCgIu.htm](feat-effects/JW93FDTAOUNuCgIu.htm)|Stance: Ravel of Thorns|Posture : Enchevêtrement d'épines|libre|
|[JwDCoBIwyhOFnDGZ.htm](feat-effects/JwDCoBIwyhOFnDGZ.htm)|Effect: Augment Senses|Effet : Sens augmentés|libre|
|[jwxurN6JPQm9wXug.htm](feat-effects/jwxurN6JPQm9wXug.htm)|Effect: Defensive Recovery|Effet : Récupération défensive|libre|
|[JysvElDwGZ5ABQ6x.htm](feat-effects/JysvElDwGZ5ABQ6x.htm)|Effect: Emotional Fervor|Effet : Ferveur émotionnelle|libre|
|[jYxWTG5J171XVa5r.htm](feat-effects/jYxWTG5J171XVa5r.htm)|Effect: Restorative Strike|Effet : Frappe restauratrice|libre|
|[JzVfTeHDopKAA5P0.htm](feat-effects/JzVfTeHDopKAA5P0.htm)|Effect: Alloy Flesh and Steel|Effet : Alliage de chair et d'acier|libre|
|[K0Sv9AHgq245hSLC.htm](feat-effects/K0Sv9AHgq245hSLC.htm)|Effect: Inspired Stratagem|Effet : Stratagème inspiré|libre|
|[K0VkhTXXmyGPilVN.htm](feat-effects/K0VkhTXXmyGPilVN.htm)|Effect: Memory of Skill|Effet : Souvenir de compétence|libre|
|[K1IgNCf3Hh2EJwQ9.htm](feat-effects/K1IgNCf3Hh2EJwQ9.htm)|Effect: Divine Aegis|Effet : Égide divine|libre|
|[k1J2SaHPwZb2Y6Bp.htm](feat-effects/k1J2SaHPwZb2Y6Bp.htm)|Effect: Wings of Air|Effet : Ailes d'air|libre|
|[k29Ao7wxjPVqxuez.htm](feat-effects/k29Ao7wxjPVqxuez.htm)|Effect: Come and Get Me|Effet : Je t'attends|libre|
|[k8gB0eDuAlGRoeQj.htm](feat-effects/k8gB0eDuAlGRoeQj.htm)|Effect: Benevolent Spirit Deck|Effet : Jeu spirituel bienveillant|libre|
|[KanP6trNwPOggb7P.htm](feat-effects/KanP6trNwPOggb7P.htm)|Effect: Spirit Guide Form|Effet : Forme de guide spirituel|libre|
|[KBEJVRrie2JTHWIK.htm](feat-effects/KBEJVRrie2JTHWIK.htm)|Effect: Dread Marshal Stance|Posture : Posture du terrible Capitaine|libre|
|[kDTiRg9vVOYNnTyr.htm](feat-effects/kDTiRg9vVOYNnTyr.htm)|Stance: Powder Punch Stance|Posture : Posture du coup de poing à poudre|libre|
|[KiuBRoMFxL2Npt51.htm](feat-effects/KiuBRoMFxL2Npt51.htm)|Stance: Dueling Dance|Posture : Danse en duel|libre|
|[klZVVIetJlR99cT6.htm](feat-effects/klZVVIetJlR99cT6.htm)|Effect: Portentous Spell|Effet : Sort de mauvais augure|libre|
|[KpEtIFwjj0ZrSVbD.htm](feat-effects/KpEtIFwjj0ZrSVbD.htm)|Effect: Precious Ammunition|Effet : Munition précieuse|libre|
|[KqyxpiGpEuju3d4z.htm](feat-effects/KqyxpiGpEuju3d4z.htm)|Effect: God's Palm (Temporary Hit Points)|Effet : Paume des dieux (Points de vie temporaires)|libre|
|[KSXdpnGIaxe08v2G.htm](feat-effects/KSXdpnGIaxe08v2G.htm)|Effect: Terrain Attunement (Plains)|Effet : Harmonisation de terrain (Plaines)|libre|
|[kui8yKIVsxfJnrYe.htm](feat-effects/kui8yKIVsxfJnrYe.htm)|Effect: Walking the Cardinal Paths|Effet : Arpenter les chemins cardinaux|libre|
|[kyrvZfZfzKK1vx9b.htm](feat-effects/kyrvZfZfzKK1vx9b.htm)|Stance: Devrin's Cunning Stance|Posture : Posture astucieuse de Devrin|libre|
|[kZdVPBO58uq38KIR.htm](feat-effects/kZdVPBO58uq38KIR.htm)|Effect: Kneecap|Effet ! Rotule|libre|
|[kzEPq4aczYb6OD2h.htm](feat-effects/kzEPq4aczYb6OD2h.htm)|Effect: Inspiring Marshal Stance|Effet : Posture du Capitaine inspirant|libre|
|[kzSjzK72CQ67wfBH.htm](feat-effects/kzSjzK72CQ67wfBH.htm)|Effect: Protective Spirit Mask|Effet : Masque de l'esprit protecteur|libre|
|[L0hDj8vFk1IWh01L.htm](feat-effects/L0hDj8vFk1IWh01L.htm)|Effect: Aura of Righteousness|Effet : Aura de vertu|libre|
|[l3S9i2UWGhSO58YX.htm](feat-effects/l3S9i2UWGhSO58YX.htm)|Effect: Cat Nap|Effet : Sieste féline|libre|
|[l4QUaedYofnfXig0.htm](feat-effects/l4QUaedYofnfXig0.htm)|Stance: Multishot Stance|Posture : Posture de tirs multiples|libre|
|[l5NDN9V3DwFYPIsD.htm](feat-effects/l5NDN9V3DwFYPIsD.htm)|Effect: Portents of the Haruspex|Effet : Présages de l'haruspice|libre|
|[L9g3EMCT3imX650b.htm](feat-effects/L9g3EMCT3imX650b.htm)|Effect: Heaven's Thunder|Effet : Tonnerre du Paradis|libre|
|[LB0PTV5yqMlBmRFj.htm](feat-effects/LB0PTV5yqMlBmRFj.htm)|Effect: Legendary Monster Hunter|Effet : Chasseur de monstres légendaire|libre|
|[Lb4q2bBAgxamtix5.htm](feat-effects/Lb4q2bBAgxamtix5.htm)|Effect: Treat Wounds Immunity|Effet : Immunité à Soigner les blessures|libre|
|[lbe8XDSZB8gwyg90.htm](feat-effects/lbe8XDSZB8gwyg90.htm)|Effect: Protective Mentor Boon (Admired) (PFS)|Effet : Récompense du mentor protecteur admiré (PFS)|libre|
|[LbICHKe5jLMxhaOw.htm](feat-effects/LbICHKe5jLMxhaOw.htm)|Effect: Turn Back The Clock|Effet : Retourner l'horloge|libre|
|[lc8ez8O4eN73hNpR.htm](feat-effects/lc8ez8O4eN73hNpR.htm)|Effect: Expel Maelstrom (Failure or Critical Failure)|Effet : Expulser le maelström (Échec ou échec critique)|libre|
|[LF8xzzFsFJKxejqv.htm](feat-effects/LF8xzzFsFJKxejqv.htm)|Effect: Enforce Oath|Effet : Serment renforcé|libre|
|[lFIUuFrNey4kD4Md.htm](feat-effects/lFIUuFrNey4kD4Md.htm)|Effect: Wish for Luck|Effet : Souhait de bonne fortune|libre|
|[lge4vtZdfZOCWAch.htm](feat-effects/lge4vtZdfZOCWAch.htm)|Stance: Winter Sleet|Posture : Grésil hivernal|libre|
|[lHbTN7jk1zuER92O.htm](feat-effects/lHbTN7jk1zuER92O.htm)|Effect: Cringe|Effet : Recroquevillé|libre|
|[Ljrx4N5XACKSk1Ks.htm](feat-effects/Ljrx4N5XACKSk1Ks.htm)|Effect: Core Cannon|Effet : Noyau canon|libre|
|[LOql7Rc5anCse9Nx.htm](feat-effects/LOql7Rc5anCse9Nx.htm)|Effect: Sea Glass Guardians|Effet : Gardiens de la mer de verre|libre|
|[lPtt7PojWBwPOaYt.htm](feat-effects/lPtt7PojWBwPOaYt.htm)|Effect: Bespell Strikes|Effet : Frappes enchantées|libre|
|[Lt5iSfx8fxHSdYXz.htm](feat-effects/Lt5iSfx8fxHSdYXz.htm)|Effect: Masterful Hunter's Edge, Precision|Effet : Spécialité Maître chasseur, Précision|libre|
|[ltIvO9ZQlmqGD89Y.htm](feat-effects/ltIvO9ZQlmqGD89Y.htm)|Effect: Hunter's Edge, Outwit|Effet : Spécialité du chasseur, Ruse|libre|
|[lUTUBYnA0GKYR5m5.htm](feat-effects/lUTUBYnA0GKYR5m5.htm)|Effect: Terrain Attunement (Underground)|Effet : Harmonisation de terrain (Souterrain)|libre|
|[LVPodfYEWKtK3fUW.htm](feat-effects/LVPodfYEWKtK3fUW.htm)|Effect: Formation Training|Effet : Combat en formation|libre|
|[LxSev4GNKv26DbZw.htm](feat-effects/LxSev4GNKv26DbZw.htm)|Stance: Disarming Stance|Posture : Posture désarmante|libre|
|[lZPbv3nBRWmfbs3z.htm](feat-effects/lZPbv3nBRWmfbs3z.htm)|Effect: Strained Metabolism|Effet : Métabolisme sous tension|libre|
|[m0hi0jpIF6tNJzzo.htm](feat-effects/m0hi0jpIF6tNJzzo.htm)|Effect: Sheltering Pulse|Effet : Pulsion protectrice|libre|
|[m5xWMaDfV0PiTE6u.htm](feat-effects/m5xWMaDfV0PiTE6u.htm)|Effect: Ursine Avenger Form|Effet : Forme de vengeur ursin|libre|
|[m8QCmsy4WFQCIC7t.htm](feat-effects/m8QCmsy4WFQCIC7t.htm)|Effect: Tap the Past|Effet : Exploiter le passé|libre|
|[maBSHuVHyGwga9uC.htm](feat-effects/maBSHuVHyGwga9uC.htm)|Effect: Duelist's Challenge|Effet : Défi du duelliste|libre|
|[MaepXeSHiMoWDm7u.htm](feat-effects/MaepXeSHiMoWDm7u.htm)|Effect: Rage Temporary Hit Points Immunity|Effet : Immunité aux Points de vie temporaires de rage|libre|
|[mark4VEQoynfYNBF.htm](feat-effects/mark4VEQoynfYNBF.htm)|Stance: Graceful Poise|Posture : Aisance gracieuse|libre|
|[MbtWd4PvUHmwPtFO.htm](feat-effects/MbtWd4PvUHmwPtFO.htm)|Effect: Helpful Tinkering|Effet : Bricolages utiles|libre|
|[MFCcEWCC9PR46qPy.htm](feat-effects/MFCcEWCC9PR46qPy.htm)|Effect: Aura Junction (Metal)|Effet : Jonction d'aura - métal|libre|
|[mkIamZGtQaSsUWLk.htm](feat-effects/mkIamZGtQaSsUWLk.htm)|Effect: Control Tower|Effet : Tour de contrôle|libre|
|[mmaJYXBUxVncsOsx.htm](feat-effects/mmaJYXBUxVncsOsx.htm)|Effect: Ghosts in the Storm|Effet : fantôme dans la tempête|libre|
|[mNk0KxsZMFnDjUA0.htm](feat-effects/mNk0KxsZMFnDjUA0.htm)|Effect: Hunter's Edge, Precision|Effet : Spécialité du chasseur, Précision|libre|
|[MNkIxAishE22TqL3.htm](feat-effects/MNkIxAishE22TqL3.htm)|Effect: Aura of Despair|Effet : Aura de Désespoir|libre|
|[Mnvd3jV4EW6nAJKI.htm](feat-effects/Mnvd3jV4EW6nAJKI.htm)|Effect: Aura Junction (Air)|Effet : Aura de jonction - air|libre|
|[MqUZKmLRL3ntnCZS.htm](feat-effects/MqUZKmLRL3ntnCZS.htm)|Effect: Predictable! (Saving Throw)|Effet : Prévisible ! (jet de sauvegarde)|libre|
|[MrdT7LiOZMN8J4GK.htm](feat-effects/MrdT7LiOZMN8J4GK.htm)|Effect: Fiendish Wings|Effet : Ailes divines (tieffelin)|libre|
|[Ms6WPXRWfXb2KpG2.htm](feat-effects/Ms6WPXRWfXb2KpG2.htm)|Stance: Tenacious Stance|Posture : Posture tenace|libre|
|[MSkspeBsbXm6LQ19.htm](feat-effects/MSkspeBsbXm6LQ19.htm)|Effect: Harrow the Fiend|Effet : Tourmenter le fiélon|libre|
|[n1vhmOd7aNiuR3nk.htm](feat-effects/n1vhmOd7aNiuR3nk.htm)|Effect: Diabolic Blood Magic (Self)|Effet : Magie du sang diabolique (Soi)|libre|
|[N27voDwKbzs7EZVt.htm](feat-effects/N27voDwKbzs7EZVt.htm)|Effect: Expel Maelstrom (Misfortune)|Effet : Expulser le maelström (Malchance)|libre|
|[N2CSGvtPXloOEPrK.htm](feat-effects/N2CSGvtPXloOEPrK.htm)|Effect: Giant's Lunge|Effet : Fente de géant|libre|
|[ngwcN8u7f7CnqGXp.htm](feat-effects/ngwcN8u7f7CnqGXp.htm)|Effect: Distant Wandering|Effet : Errance à distance|libre|
|[Njb4eLx5VngDIDpo.htm](feat-effects/Njb4eLx5VngDIDpo.htm)|Stance: Shattershields|Posture : Plaquesboucliers|libre|
|[nlaxROgSSLVHZ1hx.htm](feat-effects/nlaxROgSSLVHZ1hx.htm)|Effect: Monster Warden|Effet : Garde-monstre|libre|
|[nlMZCi8xi9YSvlYR.htm](feat-effects/nlMZCi8xi9YSvlYR.htm)|Effect: Engine of Destruction|Effet : Machine de destruction|libre|
|[nMMqJQsdV37TLfTu.htm](feat-effects/nMMqJQsdV37TLfTu.htm)|Effect: Monarch Wings|Effet : Ailes de monarque|libre|
|[NMmsJyeMTawpgLVR.htm](feat-effects/NMmsJyeMTawpgLVR.htm)|Effect: Resounding Bravery|Effet : Bravoure retentissante|libre|
|[nnF7RSVlC6swbSw8.htm](feat-effects/nnF7RSVlC6swbSw8.htm)|Effect: Anoint Ally|Effet : Oindre un allié|libre|
|[no8vv18N7YmEKvOH.htm](feat-effects/no8vv18N7YmEKvOH.htm)|Effect: Strategist Stance|Effet : Posture du stratège|libre|
|[Np3OSqKxuB9rTbij.htm](feat-effects/Np3OSqKxuB9rTbij.htm)|Effect: Harden Flesh|Effet : Peau durcie|libre|
|[nV2zH7OcQeoQrrvB.htm](feat-effects/nV2zH7OcQeoQrrvB.htm)|Effect: Spirit's Mercy|Effet : Pitié de l'esprit|libre|
|[Nv70aqcQgCBpDYp8.htm](feat-effects/Nv70aqcQgCBpDYp8.htm)|Effect: Shadow Blood Magic|Effet : Magie du sang ombre|libre|
|[NviQYIVZbPCSWLqT.htm](feat-effects/NviQYIVZbPCSWLqT.htm)|Effect: Endemic Herbs|Effet : Herbes endémiques|libre|
|[nwkYZs6YwXYAJ4ps.htm](feat-effects/nwkYZs6YwXYAJ4ps.htm)|Stance: Crane Stance|Posture : Posture de la grue|libre|
|[NWOmJ6WJFheaGhho.htm](feat-effects/NWOmJ6WJFheaGhho.htm)|Stance: Mobile Shot Stance|Posture : Posture de tir mobile|libre|
|[nWrUYrSFScwURSTs.htm](feat-effects/nWrUYrSFScwURSTs.htm)|Effect: Strain Mind|Effet : Stress mental|libre|
|[O7OO1kWFEFv6vl3E.htm](feat-effects/O7OO1kWFEFv6vl3E.htm)|Effect: Mountain Strategy (Critically Hit)|Effet : Stratégie de la montagne (Touché par un coup critique)|libre|
|[o7qm13OmaYOMwgib.htm](feat-effects/o7qm13OmaYOMwgib.htm)|Effect: Weapon Tampered With (Critical Success)|Effet : Arme trafiquée avec succès critique|libre|
|[O8qithYQCv3e7DUQ.htm](feat-effects/O8qithYQCv3e7DUQ.htm)|Effect: Elementalist Dedication|Effet : Dévouement : Élémentaliste|libre|
|[OBsItGQScxHyUcmR.htm](feat-effects/OBsItGQScxHyUcmR.htm)|Effect: Unleash Yaoguai Might|Effet : Libérer la puissance yaoguai|libre|
|[OeZ0E1oUKyGPxPy0.htm](feat-effects/OeZ0E1oUKyGPxPy0.htm)|Effect: Push Back the Dead!|Effet : Repoussez les morts !|libre|
|[OhLcaJeQy4Nf5Mwo.htm](feat-effects/OhLcaJeQy4Nf5Mwo.htm)|Effect: Favored Terrain (Gain Swim Speed)|Effet : Environnement de prédilection (Obtenir une Vitesse de nage)|libre|
|[OK7zMlYy25JciBp6.htm](feat-effects/OK7zMlYy25JciBp6.htm)|Effect: Shed Tail|Effet : Autotomie caudale|libre|
|[oKJr59FYdDORxLcR.htm](feat-effects/oKJr59FYdDORxLcR.htm)|Effect: Worldly Mentor Boon (PFS)|Effet : Récompense de mentor expérimenté (PFS)|libre|
|[OKOqC1wswrh9jXqP.htm](feat-effects/OKOqC1wswrh9jXqP.htm)|Effect: Protective Mentor Boon (Liked) (PFS)|Effet : Récompense de mentor protecteur aimé (PFS)|libre|
|[olpkQDGDzmYZCvQH.htm](feat-effects/olpkQDGDzmYZCvQH.htm)|Effect: Unstable Check Failure|Effet : Test d'échec instable|libre|
|[OochfTTXnDLVXeSS.htm](feat-effects/OochfTTXnDLVXeSS.htm)|Effect: Spell Protection Array|Effet : Barrière de protection contre les sorts|libre|
|[OqZtUOemGXgi9HDM.htm](feat-effects/OqZtUOemGXgi9HDM.htm)|Effect: Signature Weapon|Effet : Arme emblématique|libre|
|[oSzUv21eN9VS9TC1.htm](feat-effects/oSzUv21eN9VS9TC1.htm)|Effect: Curse of Turbulent Moments|Effet : Malédiction des moments de turbulence|libre|
|[oXG7eX26FmePmwUF.htm](feat-effects/oXG7eX26FmePmwUF.htm)|Effect: Discordant Voice|Effet : Voix discordante|libre|
|[p0S3VHkRgMye7RSI.htm](feat-effects/p0S3VHkRgMye7RSI.htm)|Effect: Gathering Moss|Effet : Amasseur de mousse|libre|
|[P4EkIpTqPDB3gNba.htm](feat-effects/P4EkIpTqPDB3gNba.htm)|Effect: Avowed Insight|Effet : Perspicacité avérée|libre|
|[P6druSuWIVoLrXJR.htm](feat-effects/P6druSuWIVoLrXJR.htm)|Effect: Calculate Threats|Effet : Calculer les menaces|libre|
|[P80mwvCAEncR2snK.htm](feat-effects/P80mwvCAEncR2snK.htm)|Stance: Six Pillars Stance|Posture : Posture des six piliers|libre|
|[PdFisHX9ZJmKEKCv.htm](feat-effects/PdFisHX9ZJmKEKCv.htm)|Stance: Magnetic Field|Posture : Champ magnétique|libre|
|[Pe4qwrE1oU68iW4g.htm](feat-effects/Pe4qwrE1oU68iW4g.htm)|Effect: Kindling|Effet : Petit bois|libre|
|[pf9yvKNg6jZLrE30.htm](feat-effects/pf9yvKNg6jZLrE30.htm)|Stance: Tiger Stance|Posture : Posture du tigre|libre|
|[pFo9DVyaDb4LdURY.htm](feat-effects/pFo9DVyaDb4LdURY.htm)|Effect: Nanite Surge (Bonus)|Effet : Poussée nanite - bonus|libre|
|[pkcr9w5x6bKzl3om.htm](feat-effects/pkcr9w5x6bKzl3om.htm)|Stance: Jellyfish Stance|Posture : Posture de la méduse|libre|
|[pkWudzFWfKnggT1f.htm](feat-effects/pkWudzFWfKnggT1f.htm)|Effect: Terrain Attunement (Aquatic)|Effet : Harmonisation de terrain (Aquatique)|libre|
|[pLurcSPQb2gjAzoP.htm](feat-effects/pLurcSPQb2gjAzoP.htm)|Effect: Kinetic Aura|Effet : Aura kinétique|libre|
|[PMHwCrnh9W4sMu5b.htm](feat-effects/PMHwCrnh9W4sMu5b.htm)|Stance: Tangled Forest Stance|Posture : Posture de la forêt enchevêtrée|libre|
|[pQ3EjUm1lZW9t3el.htm](feat-effects/pQ3EjUm1lZW9t3el.htm)|Effect: Curse of the Hero's Burden|Effet : Malédiction du fardeau du héros|libre|
|[pQ9e5njvIOe5QzFa.htm](feat-effects/pQ9e5njvIOe5QzFa.htm)|Effect: Fleet Tempo|Effet : Tempo rapide|libre|
|[PS17dsXkTdQmOv7w.htm](feat-effects/PS17dsXkTdQmOv7w.htm)|Stance: Buckler Dance|Posture : Danse de la targe|libre|
|[pTYTanMHMwSgJ8TN.htm](feat-effects/pTYTanMHMwSgJ8TN.htm)|Effect: Defensive Instincts|Effet : Instinct défensif|libre|
|[pvkrl2F0ghYgZXnR.htm](feat-effects/pvkrl2F0ghYgZXnR.htm)|Effect: Pride in Arms|Effet : Fierté militaire|libre|
|[pwbFFD6NzDooobKo.htm](feat-effects/pwbFFD6NzDooobKo.htm)|Effect: Reflexive Shield|Effet : Bouclier instinctif|libre|
|[Px4E3Hyx2pmvSWAA.htm](feat-effects/Px4E3Hyx2pmvSWAA.htm)|Effect: Undying Ferocity|Effet : Férocité immortelle|libre|
|[PX6WdrpzEdUzKRHx.htm](feat-effects/PX6WdrpzEdUzKRHx.htm)|Effect: Enduring Debilitating Strike|Effet : Frappe incapacitante persistante|libre|
|[Q0DKJRnDuuUnLpvn.htm](feat-effects/Q0DKJRnDuuUnLpvn.htm)|Effect: Tail Toxin|Effet : Queue à toxine|libre|
|[q2kY0TzXloJ8HLNO.htm](feat-effects/q2kY0TzXloJ8HLNO.htm)|Effect: Combat Mentor Boon (PFS)|Effet : Récompense de mentor de combat (PFS)|libre|
|[Q5FUu7yhWPJlcXei.htm](feat-effects/Q5FUu7yhWPJlcXei.htm)|Effect: Dehydrated|Effet : Hydratation|libre|
|[q6UokHWSEcEYWmvh.htm](feat-effects/q6UokHWSEcEYWmvh.htm)|Stance: Whirlwind Stance|Posture : Posture tourbillonnante|libre|
|[Q8pD29NcGGuRxWwh.htm](feat-effects/Q8pD29NcGGuRxWwh.htm)|Effect: Change Shape (Werecreature)|Effet : Changement de forme (Créature-garou)|libre|
|[qaIqu9H9OdX1B5wy.htm](feat-effects/qaIqu9H9OdX1B5wy.htm)|Effect: Staggering Fire|Effet : Feu ralentissant|libre|
|[qBR3kqGCeKp3T2Be.htm](feat-effects/qBR3kqGCeKp3T2Be.htm)|Stance: Disruptive Stance|Posture : Posture perturbatrice|libre|
|[QcReJp7kgURdQCGz.htm](feat-effects/QcReJp7kgURdQCGz.htm)|Effect: Disruptive Stare|Effet : Regard perturbateur|libre|
|[QDQwHxNowRwzUx9R.htm](feat-effects/QDQwHxNowRwzUx9R.htm)|Stance: Reflective Ripple Stance|Effet : Posture de l'onde réfléchissante|libre|
|[qIOEe4kUN7FOBifb.htm](feat-effects/qIOEe4kUN7FOBifb.htm)|Effect: Hybrid Shape (Beastkin)|Effet : Forme hybride (animanthrope)|libre|
|[qIPl31SfvT993Lyz.htm](feat-effects/qIPl31SfvT993Lyz.htm)|Effect: Protective Pose|Effet : Pose protectrice|libre|
|[qM4bQfcwZ0EOS2M9.htm](feat-effects/qM4bQfcwZ0EOS2M9.htm)|Effect: Inspiring Resilience|Effet : Résistance inspirante|libre|
|[QOWjsM4GYUHw6pFA.htm](feat-effects/QOWjsM4GYUHw6pFA.htm)|Effect: Aura Junction (Fire)|Effet : Jonction d'aura - feu|libre|
|[QrJ06Sc2GiloQ6hB.htm](feat-effects/QrJ06Sc2GiloQ6hB.htm)|Stance: Assume Earth's Mantle|Posture : Endosser la croute terrestre|libre|
|[qSKVcw6brzrvfhUM.htm](feat-effects/qSKVcw6brzrvfhUM.htm)|Effect: Supercharge Prosthetic Eyes|Effet : Surcharge des yeux prothétiques|libre|
|[QUelSJzS2EIEkoD7.htm](feat-effects/QUelSJzS2EIEkoD7.htm)|Effect: Leech-Clip|Effet : Accroche-sangsue|libre|
|[qUowHpn79Dpt1hVn.htm](feat-effects/qUowHpn79Dpt1hVn.htm)|Stance: Dragon Stance|Posture : Posture du dragon|libre|
|[QVk0oRgc1pRwhG7U.htm](feat-effects/QVk0oRgc1pRwhG7U.htm)|Effect: Lightning Armillary|Effet : Armillaire foudroyante|libre|
|[qX62wJzDYtNxDbFv.htm](feat-effects/qX62wJzDYtNxDbFv.htm)|Stance: Dread Marshal Stance|Posture : Posture du terrible capitaine|libre|
|[qZ2FIUz1ZXnS0T7N.htm](feat-effects/qZ2FIUz1ZXnS0T7N.htm)|Effect: Terrain Shield|Effet : Terrain protecteur|libre|
|[r4kb2zDepFeczMsl.htm](feat-effects/r4kb2zDepFeczMsl.htm)|Effect: Bone Swarm|Effet : Nuée d'os|libre|
|[R6mx6EfLxSrQlrRa.htm](feat-effects/R6mx6EfLxSrQlrRa.htm)|Effect: Anchored|Effet : Ancré|libre|
|[raLQ458uiyd3lI2K.htm](feat-effects/raLQ458uiyd3lI2K.htm)|Effect: Guided by the Stars|Effet : Guidé par les étoiles|libre|
|[raoz523QRsj5WjcF.htm](feat-effects/raoz523QRsj5WjcF.htm)|Effect: Harsh Judgement|Effet : Jugement sévère|libre|
|[RATDyLyxXN3qmOas.htm](feat-effects/RATDyLyxXN3qmOas.htm)|Effect: Daydream Trance|Effet : Transe du rêve éveillé|libre|
|[rCsmv66TzQhte4Gp.htm](feat-effects/rCsmv66TzQhte4Gp.htm)|Effect: Wood Impulse Junction|Effet : Jonction d'impulsion du bois|libre|
|[RcxDIOa68SUGyMun.htm](feat-effects/RcxDIOa68SUGyMun.htm)|Effect: Titan's Stature|Effet : Stature de titan|libre|
|[RDKbWKphiD9ippAv.htm](feat-effects/RDKbWKphiD9ippAv.htm)|Stance: Stonestrike Stance|Posture : Posture de frappe de roche|libre|
|[ReqVLQUQy7Y91IiB.htm](feat-effects/ReqVLQUQy7Y91IiB.htm)|Effect: Adamantine Body|Effet : Corps d'adamantium|libre|
|[rflbFzV44Fd6aBLE.htm](feat-effects/rflbFzV44Fd6aBLE.htm)|Effect: Goading Feint|Effet : Feinte provocante|libre|
|[Rgt9hH3W1oh9dvku.htm](feat-effects/Rgt9hH3W1oh9dvku.htm)|Effect: Vicious Debilitations|Effet : Handicaps cruels|libre|
|[rJpkKaPRGaH0pLse.htm](feat-effects/rJpkKaPRGaH0pLse.htm)|Effect: Fey Blood Magic|Effet : Magie du sang féerique|libre|
|[RmJ5qosNpE8DUX2a.htm](feat-effects/RmJ5qosNpE8DUX2a.htm)|Effect: Silk Bracelet|Effet : Bracelet de soie|libre|
|[RoGEt7lrCdfaueB9.htm](feat-effects/RoGEt7lrCdfaueB9.htm)|Effect: Share Rage|Effet : Rage partagée|libre|
|[RozqjLocahvQWERr.htm](feat-effects/RozqjLocahvQWERr.htm)|Stance: Gorilla Stance|Posture : Posture du gorille|libre|
|[rp1YauUSULuqW8rs.htm](feat-effects/rp1YauUSULuqW8rs.htm)|Stance: Stoked Flame Stance|Posture : Posture de la flamme ravivée|libre|
|[rp6hA52dWVwtuu5F.htm](feat-effects/rp6hA52dWVwtuu5F.htm)|Effect: Harrow Omen|Effet : Présage du Tourment|libre|
|[Ru4BNABCZ0hUbX7S.htm](feat-effects/Ru4BNABCZ0hUbX7S.htm)|Effect: Marshal's Aura|Effet : Aura de capitaine|libre|
|[RU6D7pNQSBt1zSuK.htm](feat-effects/RU6D7pNQSBt1zSuK.htm)|Effect: Propulsive Leap|Effet : Bond propulsif|libre|
|[rvyeOU7TQTLnKj03.htm](feat-effects/rvyeOU7TQTLnKj03.htm)|Effect: Reckless Abandon (Goblin)|Effet : Dangereux abandon (Gobelin)|libre|
|[rwDsr5XsrYcH7oFT.htm](feat-effects/rwDsr5XsrYcH7oFT.htm)|Effect: Curse of the Sky's Call|Effet : Malédiction de l'appel des cieux|libre|
|[RXbfq6oqzVnW6xOV.htm](feat-effects/RXbfq6oqzVnW6xOV.htm)|Stance: Shooting Stars Stance|Posture : Posture des étoiles lancées|libre|
|[RxDDXK52lwyHXl7v.htm](feat-effects/RxDDXK52lwyHXl7v.htm)|Effect: Scout's Warning|Effet : Avertissement de l'éclaireur|libre|
|[RXhGjWqBqmQOlaRV.htm](feat-effects/RXhGjWqBqmQOlaRV.htm)|Effect: Phoenix Blood Magic|Effet : Magie du sang du phénix|libre|
|[RyGaB5hDRcOeb34Q.htm](feat-effects/RyGaB5hDRcOeb34Q.htm)|Effect: Emblazon Antimagic|Effet : Antimagie blasonnée|libre|
|[rYYU6UVHblaJBxFB.htm](feat-effects/rYYU6UVHblaJBxFB.htm)|Effect: Blessed Counterstrike|Effet : Contre-frappe bénie|libre|
|[rzcpTJU9MvW1x1gz.htm](feat-effects/rzcpTJU9MvW1x1gz.htm)|Effect: Armor Tampered With (Critical Success)|Effet : Armure trafiquée avec succès critique|libre|
|[Rzg3c7hfkvTlxPDp.htm](feat-effects/Rzg3c7hfkvTlxPDp.htm)|Effect: Channeling Block|Effet : Blocage canalisé|libre|
|[s1tulrmW6teTFjVd.htm](feat-effects/s1tulrmW6teTFjVd.htm)|Effect: Angelic Blood Magic|Effet : Magie du sang angélique|libre|
|[s3Te8waFP3KEb2dN.htm](feat-effects/s3Te8waFP3KEb2dN.htm)|Effect: Shield Ally|Effet : Bouclier allié|libre|
|[s5OHkf1HkLwgvZT7.htm](feat-effects/s5OHkf1HkLwgvZT7.htm)|Effect: Shining Arms|Effet : Armes éblouissantes|libre|
|[s8LMK2zCQgUT3HoY.htm](feat-effects/s8LMK2zCQgUT3HoY.htm)|Effect: Aura Junction (Water)|Effet : Jonction d'aura - air|libre|
|[Sb3ZdFs61atILypS.htm](feat-effects/Sb3ZdFs61atILypS.htm)|Effect: Ghosts in the Storm (Move)|Effet : Fantômes dans la tempête (déplacement)|libre|
|[sCxi8lOH8tWQjLh0.htm](feat-effects/sCxi8lOH8tWQjLh0.htm)|Effect: Blessed Armament|Effet : Arsenal béni|libre|
|[sDftJWPPSUeSZD3A.htm](feat-effects/sDftJWPPSUeSZD3A.htm)|Effect: Favored Terrain (Gain Climb Speed)|Effet : Environnement de prédilection (Obtenir une Vitesse d'escalade)|libre|
|[seOQfjfjrDyK6rCo.htm](feat-effects/seOQfjfjrDyK6rCo.htm)|Effect: Ongoing Selfishness|Effet : Égoïsme continu|libre|
|[sfUsodcGb4atcSyN.htm](feat-effects/sfUsodcGb4atcSyN.htm)|Effect: Desperate Wrath|Effet : Courroux désespéré|libre|
|[SiegLMJpVOGuoyWJ.htm](feat-effects/SiegLMJpVOGuoyWJ.htm)|Effect: Ghost Wrangler|Effet : Dresseur de fantôme|libre|
|[SITsBOnOG1EJsEpl.htm](feat-effects/SITsBOnOG1EJsEpl.htm)|Effect: Inventive Offensive|Effet : Offensive inventive|libre|
|[SKjVvQcRQmnDoouw.htm](feat-effects/SKjVvQcRQmnDoouw.htm)|Effect: Skillful Mentor Boon (PFS)|Effet : Récompense de mentor talentueux (PFS)|libre|
|[sPIaly8bgNxgcNvT.htm](feat-effects/sPIaly8bgNxgcNvT.htm)|Stance: Ghosts in the Storm|Posture : Fantômes dans la tempête|libre|
|[su2HbEdb4IYB7r5l.htm](feat-effects/su2HbEdb4IYB7r5l.htm)|Effect: Spell Parry|Effet : Parade de sort|libre|
|[su5qLXoweaHxt6ZP.htm](feat-effects/su5qLXoweaHxt6ZP.htm)|Effect: Aura Junction (Wood)|Effet : Jonction d'aura - bois|libre|
|[SVGW8CLKwixFlnTv.htm](feat-effects/SVGW8CLKwixFlnTv.htm)|Effect: Nymph Blood Magic|Effet : Magie du sang nymphe|libre|
|[svVczVV174KfJRDf.htm](feat-effects/svVczVV174KfJRDf.htm)|Effect: Shared Avoidance|Effet : Évitement partagé|libre|
|[SXYcrnGzWCuj8zq7.htm](feat-effects/SXYcrnGzWCuj8zq7.htm)|Effect: Poison Weapon|Effet : Arme empoisonnée|libre|
|[SZY4zxhxoeehYRdN.htm](feat-effects/SZY4zxhxoeehYRdN.htm)|Effect: Cyclonic Ascent|Effet : Ascension cyclonique|libre|
|[T7AJQbfmlA57y625.htm](feat-effects/T7AJQbfmlA57y625.htm)|Effect: Vivacious Bravado|Effet : Bravade vivifiante|libre|
|[T7GGNvfqowFrRcKW.htm](feat-effects/T7GGNvfqowFrRcKW.htm)|Effect: Aura of Determination|Effet : Aura de détermination|libre|
|[tAsFXMzNkpj964X4.htm](feat-effects/tAsFXMzNkpj964X4.htm)|Effect: Liberating Step (vs. Aberration)|Effet : Pas libérateur (Aberration)|libre|
|[tcdIBJTHuexSKJ6S.htm](feat-effects/tcdIBJTHuexSKJ6S.htm)|Effect: Mental Balm|Effet : Baume mental|libre|
|[tCMVyxQhzYCjBQtK.htm](feat-effects/tCMVyxQhzYCjBQtK.htm)|Effect: Web|Effet : Toile|libre|
|[tI2fM9pAOpCWP2V9.htm](feat-effects/tI2fM9pAOpCWP2V9.htm)|Effect: Empathetic Plea|Effet : Supplique empathique|libre|
|[Tju9kpQlwcKkyKor.htm](feat-effects/Tju9kpQlwcKkyKor.htm)|Effect: Curse of Torrential Knowledge|Effet : Malédiction de l'afflux de connaissances|libre|
|[TKuUezmnwVftoCcL.htm](feat-effects/TKuUezmnwVftoCcL.htm)|Effect: Kishin Rage|Effet : Rége du kishin|libre|
|[tl94WHJ2Hg0akK2o.htm](feat-effects/tl94WHJ2Hg0akK2o.htm)|Effect: Invigorating Breath|Effet : Souffle revigorant|libre|
|[tlft5vzk66iWCVRq.htm](feat-effects/tlft5vzk66iWCVRq.htm)|Effect: Safeguard Soul|Effet : Âme à l'abri|libre|
|[tMsVpICbBU3kL1lp.htm](feat-effects/tMsVpICbBU3kL1lp.htm)|Effect: Surgical Shock (Failure)|Effet : Choc chirurgical (échec)|libre|
|[tPKXLtDJ3bzJcXlv.htm](feat-effects/tPKXLtDJ3bzJcXlv.htm)|Stance: Ironblood Stance|Posture : Posture du sang-de-fer|libre|
|[ttIvoQUl1yG5K62o.htm](feat-effects/ttIvoQUl1yG5K62o.htm)|Effect: Watch Your Back|Effet : Fais attention|libre|
|[Tw9MjeQHL3qFY1PO.htm](feat-effects/Tw9MjeQHL3qFY1PO.htm)|Effect: Furnace Form|Effet : Forme de four|libre|
|[Twno1jQEtPlrLOvK.htm](feat-effects/Twno1jQEtPlrLOvK.htm)|Effect: Diverting Vortex|Effet : Vortex déroutant|libre|
|[twvhTLrLEfr7dz1m.htm](feat-effects/twvhTLrLEfr7dz1m.htm)|Effect: Familiar of Restored Spirit|Effet : Familier d'esprit récupéré|libre|
|[tx0S0fnfZ6Q2o80H.htm](feat-effects/tx0S0fnfZ6Q2o80H.htm)|Effect: High-Speed Regeneration Speed Boost|Effet : Régénération à haute vitesse augmentation de vitesse|libre|
|[U2Pgm6B4nmdQ2Gpd.htm](feat-effects/U2Pgm6B4nmdQ2Gpd.htm)|Effect: Divine Castigation|Effet : Punition divine|libre|
|[U6BAwYT5iC8cmBDG.htm](feat-effects/U6BAwYT5iC8cmBDG.htm)|Effect: Elemental Bulwark|Effet : Fortification élémentaire|libre|
|[U7QM4yqxcoGSysoG.htm](feat-effects/U7QM4yqxcoGSysoG.htm)|Effect: Show Off|Effet : Frimer|libre|
|[uaWIJU1m2eHgfkDp.htm](feat-effects/uaWIJU1m2eHgfkDp.htm)|Effect: Born to the Trees|Effet : Né pour les arbres|libre|
|[UBC6HbfqbfPQYlMq.htm](feat-effects/UBC6HbfqbfPQYlMq.htm)|Effect: Tidal Shield|Effet : Bouclier de la marée|libre|
|[uBJsxCzNhje8m8jj.htm](feat-effects/uBJsxCzNhje8m8jj.htm)|Effect: Panache|Effet : Panache|libre|
|[UE0yky6aW0WCF0Qg.htm](feat-effects/UE0yky6aW0WCF0Qg.htm)|Effect: Fulminating Shot|Effet : Tir fulminant|libre|
|[uFYvW3kFP9iyNfVX.htm](feat-effects/uFYvW3kFP9iyNfVX.htm)|Stance: Clinging Shadows Stance|Posture : Posture des ombres tenaces|libre|
|[uG1EljvrnC9HGwUh.htm](feat-effects/uG1EljvrnC9HGwUh.htm)|Effect: Ocean's Balm|Effet : Baume de l'océan|libre|
|[ugeStF0Rj8phBPWL.htm](feat-effects/ugeStF0Rj8phBPWL.htm)|Effect: Witch's Charge|Effet : Protégé du sorcier|libre|
|[ukdRbANZICKl29k9.htm](feat-effects/ukdRbANZICKl29k9.htm)|Effect: Uncanny Bombs|Effet : Bombes incroyables|libre|
|[UKNtAmCdczhWB4xI.htm](feat-effects/UKNtAmCdczhWB4xI.htm)|Effect: Moisture Bath|Effet : Bain hydratant|libre|
|[UMLqhBMT98flcmJi.htm](feat-effects/UMLqhBMT98flcmJi.htm)|Effect: Volatile Grease (Failure)|Effet : Graisse volatile (Échec)|libre|
|[Unfl4QQURWaX2zfd.htm](feat-effects/Unfl4QQURWaX2zfd.htm)|Stance: Ricochet Stance|Posture : Posture du ricochet|libre|
|[UookwOq46zEokGOM.htm](feat-effects/UookwOq46zEokGOM.htm)|Effect: Channeled Protection|Effet : Protection canalisée|libre|
|[UQ7vZgmfK0VSFS8A.htm](feat-effects/UQ7vZgmfK0VSFS8A.htm)|Effect: Aberrant Blood Magic|Effet : Magie du sang aberrant|libre|
|[uRwnbZ0xWrB5ZzZ9.htm](feat-effects/uRwnbZ0xWrB5ZzZ9.htm)|Effect: Turn Aside Ambient Magic|Effet : Mettre à l'écart|libre|
|[ut5SVyCSXel69nnd.htm](feat-effects/ut5SVyCSXel69nnd.htm)|Effect: Offensive Boost|Effet : Renfort offensif|libre|
|[uXCU8GgriUjuj5FV.htm](feat-effects/uXCU8GgriUjuj5FV.htm)|Effect: Hunter's Edge, Flurry|Effet : Spécialisation du chasseur, Déluge|libre|
|[UzIamWcEJTOjwfoA.htm](feat-effects/UzIamWcEJTOjwfoA.htm)|Effect: Spin Tale|Effet : Broder un conte|libre|
|[UZKIKLuwpQu47feK.htm](feat-effects/UZKIKLuwpQu47feK.htm)|Stance: Gorilla Stance (Gorilla Pound)|Posture : Posture du gorille (Martèlement du gorille)|libre|
|[v2HDcrxQF2Dncjbs.htm](feat-effects/v2HDcrxQF2Dncjbs.htm)|Effect: Flamboyant Cruelty|Effet : Cruauté flamboyante|libre|
|[V6lnFOq998B76Rr0.htm](feat-effects/V6lnFOq998B76Rr0.htm)|Effect: Curse of Ancestral Meddling|Effet : Malédiction de l'incursion ancestrale|libre|
|[v9ujShXxA76JL8kh.htm](feat-effects/v9ujShXxA76JL8kh.htm)|Effect: Fungal Rot|Effet : Pourrissement fongique|libre|
|[Ve1CRFI8ikL6dqcL.htm](feat-effects/Ve1CRFI8ikL6dqcL.htm)|Effect: Cast Down|Effet : Incantation renversante|libre|
|[VfXwZXZaVq9u4GHo.htm](feat-effects/VfXwZXZaVq9u4GHo.htm)|Stance: Spear of Doom|Posture : Lance de la condamnation|libre|
|[vguxP8ukwVTWWWaA.htm](feat-effects/vguxP8ukwVTWWWaA.htm)|Effect: Imperial Blood Magic|Effet : Magie du sang impérial|libre|
|[vhSYlQiAQMLuXqoc.htm](feat-effects/vhSYlQiAQMLuXqoc.htm)|Effect: Clue In|Effet : Partager les indices|libre|
|[vjvcccAwdkOLA1Fc.htm](feat-effects/vjvcccAwdkOLA1Fc.htm)|Stance: Peafowl Stance|Posture : Posture du paon|libre|
|[vkgmMBMd2e2BwiwJ.htm](feat-effects/vkgmMBMd2e2BwiwJ.htm)|Effect: Ancestral Influence|Effet : Influence ancestrale|libre|
|[VM3A4YSG72JCpWMg.htm](feat-effects/VM3A4YSG72JCpWMg.htm)|Effect: Multifaceted Will|Effet : Volonté aux multiples visages|libre|
|[VOOShYoB4gTopZtg.htm](feat-effects/VOOShYoB4gTopZtg.htm)|Effect: Aura of Faith|Effet : Aura de foi|libre|
|[W2tWq0gdAcnoz2MO.htm](feat-effects/W2tWq0gdAcnoz2MO.htm)|Effect: Monster Hunter|Effet : Chasseur de monstres|libre|
|[w6X7io56B2HHTOvs.htm](feat-effects/w6X7io56B2HHTOvs.htm)|Effect: Guardian's Deflection|Effet : Déviation du Gardien|libre|
|[W8CKuADdbODpBh6O.htm](feat-effects/W8CKuADdbODpBh6O.htm)|Stance: Lunging Stance|Posture : Posture de fente|libre|
|[W8HWQ47YNHWB8kj6.htm](feat-effects/W8HWQ47YNHWB8kj6.htm)|Effect: Topple Giants|Effet : Renverser les géants|libre|
|[wAvcGfatk9H3TblG.htm](feat-effects/wAvcGfatk9H3TblG.htm)|Effect: Unwavering Resilience|Effet : Résilience inébranlable|libre|
|[WEP9Oe1D9WsWSOU8.htm](feat-effects/WEP9Oe1D9WsWSOU8.htm)|Effect: Energy Ablation|Effet : Emprunt énergétique|libre|
|[wjNNHgX6ceKLbn8Q.htm](feat-effects/wjNNHgX6ceKLbn8Q.htm)|Effect: Rallying Charge|Effet : Charge de ralliement|libre|
|[wmBSuZPqiDyUNwXH.htm](feat-effects/wmBSuZPqiDyUNwXH.htm)|Effect: Dragon's Rage Wings|Effet : Ailes de rage du dragon|libre|
|[wnCKvlOecXAmtWII.htm](feat-effects/wnCKvlOecXAmtWII.htm)|Effect: Repel Ambient Magic|Effet : Repousser la magie ambiante|libre|
|[woKCbf1kXPrPjeZG.htm](feat-effects/woKCbf1kXPrPjeZG.htm)|Stance: Crowned in Tempest's Fury|Posture : Couronné par la furie de la tempête|libre|
|[wQDHpOKY3GZqvS2v.htm](feat-effects/wQDHpOKY3GZqvS2v.htm)|Effect: Seedpod|Effet : Cosse|libre|
|[WRe8qbemruWxkN8d.htm](feat-effects/WRe8qbemruWxkN8d.htm)|Effect: Rampaging Form (Frozen Winds Kitsune)|Effet : Forme destructrice (Kitsune du vent gelé)|libre|
|[WrWSieH9Acy6XuzV.htm](feat-effects/WrWSieH9Acy6XuzV.htm)|Effect: Educate Allies|Effet : Alliés instruits|libre|
|[WxE5S3KY1DR5Nbxm.htm](feat-effects/WxE5S3KY1DR5Nbxm.htm)|Effect: Living Fortification (Parry Trait)|Effet : Fortification vivante - trait parade|libre|
|[X19XgqqItqZ4tfmq.htm](feat-effects/X19XgqqItqZ4tfmq.htm)|Effect: Guardian's Embrace|Effet : Étreinte du gardien|libre|
|[X1pGyhMKrCTvHB0q.htm](feat-effects/X1pGyhMKrCTvHB0q.htm)|Effect: Favorable Winds|Effet : Vents favorables|libre|
|[x7CjbOQZUgZKW16M.htm](feat-effects/x7CjbOQZUgZKW16M.htm)|Effect: Overextending Feint|Effet : Feinte précautionneuse|libre|
|[XAdhPJqssO3v4Zvw.htm](feat-effects/XAdhPJqssO3v4Zvw.htm)|Effect: Pinpoint Poisoner|Effet : Empoisonneur opportun|libre|
|[XaZdQHF9GvaJINqH.htm](feat-effects/XaZdQHF9GvaJINqH.htm)|Effect: Elemental Assault|Effet : Assaut élémentaire|libre|
|[xdcblhm5Ie3CG9wS.htm](feat-effects/xdcblhm5Ie3CG9wS.htm)|Effect: Glimpses to Beyond|Effet : Aperçu de l'au-delà|libre|
|[xdlAursiOE9A9rK7.htm](feat-effects/xdlAursiOE9A9rK7.htm)|Effect: Boaster's Challenge (Critical Success)|Effet : Défi du vantard (Succès critique)|libre|
|[xDT10fUWp8UStSZR.htm](feat-effects/xDT10fUWp8UStSZR.htm)|Effect: Cavalier's Banner|Effet : Bannière du cavalier|libre|
|[XFc3dVzTe7KnpjuP.htm](feat-effects/XFc3dVzTe7KnpjuP.htm)|Stance: Air Shroud|Posture : Enveloppe d'air|libre|
|[XIYWFGHBlcc79YI5.htm](feat-effects/XIYWFGHBlcc79YI5.htm)|Effect: Echoes in Stone|Effet : Échos dans la roche|libre|
|[XJtlvaqAHseq1yoz.htm](feat-effects/XJtlvaqAHseq1yoz.htm)|Effect: Towering Presence|Effet : Présence imposante|libre|
|[XM1AA8z5cHm8sJXM.htm](feat-effects/XM1AA8z5cHm8sJXM.htm)|Effect: Enlightened Presence|Effet : Présence éclairée|libre|
|[xPg5wzzKNxJy18rU.htm](feat-effects/xPg5wzzKNxJy18rU.htm)|Effect: Brightness Seeker|Effet : Aspirant à l'illumination|libre|
|[xPHUZgr0XEyP1Nof.htm](feat-effects/xPHUZgr0XEyP1Nof.htm)|Effect: Benefactor's Majesty|Effet : Majesté du bienfaiteur|libre|
|[XQpTyjXFYYNexyOk.htm](feat-effects/XQpTyjXFYYNexyOk.htm)|Effect: Devise a Stratagem|Effet : Concevoir un stratagème|libre|
|[xr4bNJvtrHSqWkrF.htm](feat-effects/xr4bNJvtrHSqWkrF.htm)|Effect: Whispers of Weakness|Effet : Murmures de faiblesse|libre|
|[xtqOIXCe0Nsd0QCt.htm](feat-effects/xtqOIXCe0Nsd0QCt.htm)|Effect: Lock On|Effet : Verrouillage|libre|
|[xYCoyHRLdOXm8Ndr.htm](feat-effects/xYCoyHRLdOXm8Ndr.htm)|Effect: Shielding Wave|Effet : Vague protectrice|libre|
|[xYd4G4yPLHSWWWvT.htm](feat-effects/xYd4G4yPLHSWWWvT.htm)|Effect: Bold Defiance|Effet : Défi audacieux|libre|
|[Y96a1OedsU8PVf7z.htm](feat-effects/Y96a1OedsU8PVf7z.htm)|Effect: Starlight Armor|Effet : Armure de lumière stellaire|libre|
|[YaSxccYfE5ShFdFd.htm](feat-effects/YaSxccYfE5ShFdFd.htm)|Effect: Resurrectionist|Effet : Résurrecteur|libre|
|[YAWfkGBnDwimMtJ8.htm](feat-effects/YAWfkGBnDwimMtJ8.htm)|Effect: Pain is Temporary|Effet : La douleur est momentanée|libre|
|[ybc7tZwByenCzow8.htm](feat-effects/ybc7tZwByenCzow8.htm)|Effect: Creeping Ashes|Effet : Malédiction des cendres rampantes|libre|
|[yBTASi3FvnReAwHy.htm](feat-effects/yBTASi3FvnReAwHy.htm)|Effect: Debilitating Strike|Effet : Frappe incapacitante|libre|
|[ycalVFivloJ14yhA.htm](feat-effects/ycalVFivloJ14yhA.htm)|Effect: Resounding Finale|Effet : Final retentissant|libre|
|[YIpFCMT8AKKhgbAF.htm](feat-effects/YIpFCMT8AKKhgbAF.htm)|Effect: Premonition of Clarity|Effet : Clarté prémonitoire|libre|
|[YIShkE3JCEuwCAxl.htm](feat-effects/YIShkE3JCEuwCAxl.htm)|Effect: Metallic Skin|Effet : Peau métallique|libre|
|[yKFV294LeXzrvF8K.htm](feat-effects/yKFV294LeXzrvF8K.htm)|Effect: Infused with Belkzen's Might|Effet : Imprégné de la puissance du Belkzen|libre|
|[YkiTA74FrUUu5IvI.htm](feat-effects/YkiTA74FrUUu5IvI.htm)|Stance: Rough Terrain Stance|Posture : Posture du terrain accidenté|libre|
|[YKJhjkerCW0Jl6HP.htm](feat-effects/YKJhjkerCW0Jl6HP.htm)|Effect: Life-Giving Magic|Effet : Magie donneuse de vie|libre|
|[YMdRmEcOlM3uU9Em.htm](feat-effects/YMdRmEcOlM3uU9Em.htm)|Effect: Living for the Applause|Effet : Vivre pour les applaudissements|libre|
|[YNoBbHUu7enOSKyv.htm](feat-effects/YNoBbHUu7enOSKyv.htm)|Stance: Orchard's Endurance|Posture : Endurance du verger|libre|
|[yqCzUxrdLlk6Q9VW.htm](feat-effects/yqCzUxrdLlk6Q9VW.htm)|Effect: Envenom Fangs|Effet : Crocs empoisonnés|libre|
|[yr5ey5qC8dXH749T.htm](feat-effects/yr5ey5qC8dXH749T.htm)|Effect: Entity's Resurgence|Effet : Résurgence de l'entité|libre|
|[ytG5XJmkOnDOTjNN.htm](feat-effects/ytG5XJmkOnDOTjNN.htm)|Effect: Soaring Flight|Effet : Voler haut|libre|
|[z3uyCMBddrPK5umr.htm](feat-effects/z3uyCMBddrPK5umr.htm)|Effect: Rage|Effet : Rage|libre|
|[z6oLNlBs724PCcR6.htm](feat-effects/z6oLNlBs724PCcR6.htm)|Stance: Strategist Stance|Posture : Posture du stratège|libre|
|[zcJii1XyOne9EvMr.htm](feat-effects/zcJii1XyOne9EvMr.htm)|Effect: Assisting Shot|Effet : Tir de soutien|libre|
|[zezKegTvOArcDQ0x.htm](feat-effects/zezKegTvOArcDQ0x.htm)|Effect: Weapon Infusion|Effet : Arsenal perfusé|libre|
|[zgRWm8KRquDzd1iC.htm](feat-effects/zgRWm8KRquDzd1iC.htm)|Effect: Draw From the Land|Effet : Tiré de la terre|libre|
|[ZMFgz4GYSsFeaKKK.htm](feat-effects/ZMFgz4GYSsFeaKKK.htm)|Effect: Rugged Mentor Boon (PFS)|Effet : Récompense de mentor robuste (PFS)|libre|
|[ZnKnOPPq3cG54PlG.htm](feat-effects/ZnKnOPPq3cG54PlG.htm)|Effect: Liberating Step (vs. Undead)|Effet : Pas libérateur (morts-vivants)|libre|
|[zocU4IYIlWwRKUuE.htm](feat-effects/zocU4IYIlWwRKUuE.htm)|Effect: Energy Shot|Effet : Tir énergétique|libre|
|[ZqEOsqnGFLI2ob9m.htm](feat-effects/ZqEOsqnGFLI2ob9m.htm)|Effect: Prepare Elemental Medicine|Effet : Préparer un remède élémentaire|libre|
|[zqgOjMU9TGoGwJWc.htm](feat-effects/zqgOjMU9TGoGwJWc.htm)|Effect: Rebirth in Living Stone|Effet : Renaissance en roche vivante|libre|
|[zQHF2kkhZRAcrQvR.htm](feat-effects/zQHF2kkhZRAcrQvR.htm)|Effect: Sniping Duo Dedication|Effet : Dévouement : Duo de tieur d'élite|libre|
|[ZsO5juyylVoxUkXh.htm](feat-effects/ZsO5juyylVoxUkXh.htm)|Effect: Bone Spikes|Effet : Pointe d'os|libre|
|[zUvicEXd4OgCZ1cO.htm](feat-effects/zUvicEXd4OgCZ1cO.htm)|Effect: You're an Embarrassment|Effet : Tu es gênant !|libre|
|[zZ25N1zpXA8GNhFL.htm](feat-effects/zZ25N1zpXA8GNhFL.htm)|Effect: Divine Weapon|Effet : Arme Divine|libre|
|[zzC2qZwEKf4Ja3xD.htm](feat-effects/zzC2qZwEKf4Ja3xD.htm)|Stance: Impassable Wall Stance|Posture : Posture du mur infranchissable|libre|
|[ZZXIUvZqqIxkMfYa.htm](feat-effects/ZZXIUvZqqIxkMfYa.htm)|Effect: Precise Debilitations|Effet : Handicap précis|libre|
