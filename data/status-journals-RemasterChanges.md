# État de la traduction (journals/pages-RemasterChanges)

 * **changé**: 4
 * **libre**: 2


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[8fgkR236jbcX08qz.htm](journals/pages-RemasterChanges/8fgkR236jbcX08qz.htm)|Feats|Dons|changé|
|[a5XWVMjK5OsZciRi.htm](journals/pages-RemasterChanges/a5XWVMjK5OsZciRi.htm)|Equipment|Équipement|changé|
|[GRjgmsZRxLYcK7Ko.htm](journals/pages-RemasterChanges/GRjgmsZRxLYcK7Ko.htm)|Class Features|Capacités de classe|changé|
|[Gtvdok4YnzNR2Xx4.htm](journals/pages-RemasterChanges/Gtvdok4YnzNR2Xx4.htm)|Spells|Sorts|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[HtFFpq0n6tjZqLOO.htm](journals/pages-RemasterChanges/HtFFpq0n6tjZqLOO.htm)|Rules and Languages|Règles et langues|libre|
|[JYJd1xZwqUNRNsqG.htm](journals/pages-RemasterChanges/JYJd1xZwqUNRNsqG.htm)|Remaster Changes|Changements du remaster|libre|
