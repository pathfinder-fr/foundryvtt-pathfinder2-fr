# État de la traduction (journals/pages-HeroPointDeck)

 * **libre**: 52


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[6xnJaS3qRZGdDHtr.htm](journals/pages-HeroPointDeck/6xnJaS3qRZGdDHtr.htm)|Roll Back|Roulade arrière|libre|
|[7uERHpfJplGtYf8w.htm](journals/pages-HeroPointDeck/7uERHpfJplGtYf8w.htm)|Aura of Protection|Aura de protection|libre|
|[8NHTJ8kK9qCzOVxp.htm](journals/pages-HeroPointDeck/8NHTJ8kK9qCzOVxp.htm)|Battle Cry|Cri de guerre|libre|
|[8w220Bvj6W2Ui8Ae.htm](journals/pages-HeroPointDeck/8w220Bvj6W2Ui8Ae.htm)|Surge of Speed|Poussée de vitesse|libre|
|[a68Phr1qsqVRPMVp.htm](journals/pages-HeroPointDeck/a68Phr1qsqVRPMVp.htm)|Class Might|Puissance de classe|libre|
|[AeGcoQNaZ1BUmWvu.htm](journals/pages-HeroPointDeck/AeGcoQNaZ1BUmWvu.htm)|Last Second Sidestep|Pas de côté au dernier moment|libre|
|[bb4nv44qEdpt0pLq.htm](journals/pages-HeroPointDeck/bb4nv44qEdpt0pLq.htm)|Stoke the Magical Flame|Attiser la flamme magique|libre|
|[cQH9Syl1SQpbAi5A.htm](journals/pages-HeroPointDeck/cQH9Syl1SQpbAi5A.htm)|Rage and Fury|Rage et furie|libre|
|[d15Zjdqplmj7ZTKK.htm](journals/pages-HeroPointDeck/d15Zjdqplmj7ZTKK.htm)|Desperate Swing|Balancement désespéré|libre|
|[D6hqfmZUksRvqNFR.htm](journals/pages-HeroPointDeck/D6hqfmZUksRvqNFR.htm)|Fluid Motion|Mouvement fluide|libre|
|[dCd0YXLVx0DQwitm.htm](journals/pages-HeroPointDeck/dCd0YXLVx0DQwitm.htm)|Tuck and Roll|Se plier et rouler|libre|
|[E4aI0BTV9ATxfjGd.htm](journals/pages-HeroPointDeck/E4aI0BTV9ATxfjGd.htm)|Reckless Charge|Charge téméraire|libre|
|[EGa7oNFMSLkvkQjL.htm](journals/pages-HeroPointDeck/EGa7oNFMSLkvkQjL.htm)|Flash of Insight|Éclair de lucidité|libre|
|[eZUSi5q5NnEvCDEk.htm](journals/pages-HeroPointDeck/eZUSi5q5NnEvCDEk.htm)|Cut Through the Fog|Traverser le brouillard|libre|
|[EZZxz9jeEB0N3FPZ.htm](journals/pages-HeroPointDeck/EZZxz9jeEB0N3FPZ.htm)|Spark of Courage|Étincelle de courage|libre|
|[FgA086C40VR9mzNr.htm](journals/pages-HeroPointDeck/FgA086C40VR9mzNr.htm)|Hold the Line|Tenez la ligne|libre|
|[Fq1KEUyv0zBp5nMV.htm](journals/pages-HeroPointDeck/Fq1KEUyv0zBp5nMV.htm)|Last Ounce of Strength|Dernière once de force|libre|
|[ftdrUGjW8A4TPkMa.htm](journals/pages-HeroPointDeck/ftdrUGjW8A4TPkMa.htm)|Protect the Innocent|Protéger l'innocent|libre|
|[g5G4unm9tbEDu3pZ.htm](journals/pages-HeroPointDeck/g5G4unm9tbEDu3pZ.htm)|Hasty Block|Blocage précipité|libre|
|[gmwyfTlk0BGZLdCt.htm](journals/pages-HeroPointDeck/gmwyfTlk0BGZLdCt.htm)|Magical Reverberation|Réverbération magique|libre|
|[GqZLaGQDNe41saZ7.htm](journals/pages-HeroPointDeck/GqZLaGQDNe41saZ7.htm)|Misdirected Attack|Attaque mal dirigée|libre|
|[Gu6CHAPMigpV9awj.htm](journals/pages-HeroPointDeck/Gu6CHAPMigpV9awj.htm)|Tumble Through|Déplacement acrobatique|libre|
|[j6hsF2TWFtGMyClW.htm](journals/pages-HeroPointDeck/j6hsF2TWFtGMyClW.htm)|Shoot Through|Tir à travers|libre|
|[jHn1m1r95YgMQSvM.htm](journals/pages-HeroPointDeck/jHn1m1r95YgMQSvM.htm)|Daring Attempt|Tentative audacieuse|libre|
|[jIs5mtRaqG0aGQ8u.htm](journals/pages-HeroPointDeck/jIs5mtRaqG0aGQ8u.htm)|Make Way!|Faites place !|libre|
|[jMhD1Z6aagYAYCnb.htm](journals/pages-HeroPointDeck/jMhD1Z6aagYAYCnb.htm)|Last Stand|Dernier combat|libre|
|[k0ue81dFKH4OKxed.htm](journals/pages-HeroPointDeck/k0ue81dFKH4OKxed.htm)|Impossible Shot|Tir impossible|libre|
|[ksg8D5ssP6WGwGqi.htm](journals/pages-HeroPointDeck/ksg8D5ssP6WGwGqi.htm)|Opportune Distraction|Distraction opportune|libre|
|[LafJ1PXfnR4Ogyzh.htm](journals/pages-HeroPointDeck/LafJ1PXfnR4Ogyzh.htm)|Catch your Breath|Reprends ton souffle|libre|
|[LaNM3BfoZUG2B39v.htm](journals/pages-HeroPointDeck/LaNM3BfoZUG2B39v.htm)|Warding Sign|Signe de protection|libre|
|[lJe2uRM1wNJw4Mgr.htm](journals/pages-HeroPointDeck/lJe2uRM1wNJw4Mgr.htm)|Push Through the Pain|Surmonter la douleur|libre|
|[lLdAvaOo1rLLg5b4.htm](journals/pages-HeroPointDeck/lLdAvaOo1rLLg5b4.htm)|Pierce Resistance|Perce résistance|libre|
|[MSdKfjoNBj7PjEoo.htm](journals/pages-HeroPointDeck/MSdKfjoNBj7PjEoo.htm)|Grazing Blow|Coup égratignant|libre|
|[O2hME2ilgN9BcEA4.htm](journals/pages-HeroPointDeck/O2hME2ilgN9BcEA4.htm)|Dive Out of Danger|Se mettre hors de danger|libre|
|[oa0wVDfT3dbWwNDf.htm](journals/pages-HeroPointDeck/oa0wVDfT3dbWwNDf.htm)|Surge of Magic|Afflux de magie|libre|
|[odrMigI38k7lgurE.htm](journals/pages-HeroPointDeck/odrMigI38k7lgurE.htm)|Channel Life Force|Canalisation de force vitale|libre|
|[OnB94Y7wYE9j0ict.htm](journals/pages-HeroPointDeck/OnB94Y7wYE9j0ict.htm)|I Hope This Works|J'espère que ça fonctionne|libre|
|[ox9mFGY50NEzRR12.htm](journals/pages-HeroPointDeck/ox9mFGY50NEzRR12.htm)|Reverse Strike|Frappe à revers|libre|
|[P3mSUWRvCCwEouB0.htm](journals/pages-HeroPointDeck/P3mSUWRvCCwEouB0.htm)|Critical Moment|Instant critique|libre|
|[pS87Zh4QJnu0p8ES.htm](journals/pages-HeroPointDeck/pS87Zh4QJnu0p8ES.htm)|Strike True|Frappe ultime|libre|
|[QUhXVsUDujUTYz7F.htm](journals/pages-HeroPointDeck/QUhXVsUDujUTYz7F.htm)|Stay in the Fight|Continuer le combat|libre|
|[quxPxuMub8k6abzN.htm](journals/pages-HeroPointDeck/quxPxuMub8k6abzN.htm)|Ancestral Might|Puissance ancestrale|libre|
|[spzRl5CS4vnvcrm5.htm](journals/pages-HeroPointDeck/spzRl5CS4vnvcrm5.htm)|Called Foe|Ennemi désigné|libre|
|[Ttn2H0JHJVksChGi.htm](journals/pages-HeroPointDeck/Ttn2H0JHJVksChGi.htm)|Healing Prayer|Prière de guérison|libre|
|[TU21DvdpQG7U2Q9Y.htm](journals/pages-HeroPointDeck/TU21DvdpQG7U2Q9Y.htm)|Endure the Onslaught|Endurer l'assaut|libre|
|[tUu3x2iHtOUqqWUc.htm](journals/pages-HeroPointDeck/tUu3x2iHtOUqqWUc.htm)|Distract Foe|Distraire l'ennemi|libre|
|[VSZGUtcr5NGNPtlB.htm](journals/pages-HeroPointDeck/VSZGUtcr5NGNPtlB.htm)|Rampage|Carnage|libre|
|[XHSXMcRaMOsbjoO1.htm](journals/pages-HeroPointDeck/XHSXMcRaMOsbjoO1.htm)|Rending Swipe|Coup éventreur|libre|
|[xsdJIpd9NRLpRZT2.htm](journals/pages-HeroPointDeck/xsdJIpd9NRLpRZT2.htm)|Drain Power|Drainer la puissance|libre|
|[Y20GMjqVbTvGZ87k.htm](journals/pages-HeroPointDeck/Y20GMjqVbTvGZ87k.htm)|Shake it Off|Reprends-toi|libre|
|[zcBCxlvPCpmjt5IS.htm](journals/pages-HeroPointDeck/zcBCxlvPCpmjt5IS.htm)|Press On|Poursuivre|libre|
|[zX28AGU1k5sO2Tpd.htm](journals/pages-HeroPointDeck/zX28AGU1k5sO2Tpd.htm)|Run and Shoot|Courir et tirer|libre|
