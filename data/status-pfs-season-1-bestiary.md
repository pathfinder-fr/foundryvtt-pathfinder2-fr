# État de la traduction (pfs-season-1-bestiary)

 * **libre**: 330
 * **changé**: 1


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[Tnq5pjDMMSmPkBtB.htm](pfs-season-1-bestiary/Tnq5pjDMMSmPkBtB.htm)|Ralthiss (3-4)|Ralthiss (3-4)|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[02MIBcyPwCEmh8lA.htm](pfs-season-1-bestiary/02MIBcyPwCEmh8lA.htm)|Wounded Orc Brute|Brute orc blessé|libre|
|[06T3st7Hyx2O4E3v.htm](pfs-season-1-bestiary/06T3st7Hyx2O4E3v.htm)|Lesser Guardian Statue|Statue gardienne inférieure|libre|
|[0DudqNVGfXC39umi.htm](pfs-season-1-bestiary/0DudqNVGfXC39umi.htm)|Reefclaw Spawn|Rejeton de pince des récifs|libre|
|[0oZJXOPw0OfkaMMU.htm](pfs-season-1-bestiary/0oZJXOPw0OfkaMMU.htm)|Trapmaster Tok's Surprise (3-4)|Surprise de Tok le Maître des pièges (3-4)|libre|
|[0TEnslG0jaLrbf58.htm](pfs-season-1-bestiary/0TEnslG0jaLrbf58.htm)|Tough Giant Shrew|Musaraigne géante coriace|libre|
|[1eeMzFoSPuQAAsiY.htm](pfs-season-1-bestiary/1eeMzFoSPuQAAsiY.htm)|Paravaax (5-6)|Paravaax (5-6)|libre|
|[1JVuJXe27PLyUVwf.htm](pfs-season-1-bestiary/1JVuJXe27PLyUVwf.htm)|Farmer Drystan's Scarecrow (1-2)|Épouvantail de Drystan le fermier (1-2)|libre|
|[1lfSUWK706fFrvhT.htm](pfs-season-1-bestiary/1lfSUWK706fFrvhT.htm)|Bandit Lieutenant|Lieutenant bandit|libre|
|[1LM0AUYkZcsbtETs.htm](pfs-season-1-bestiary/1LM0AUYkZcsbtETs.htm)|Elite Boggard Swampseer (PFS 1-24, Staff)|Devin des marais bourbiérin élite (PFS 1-12, Bâton)|libre|
|[1MXO3xJS50lENkHL.htm](pfs-season-1-bestiary/1MXO3xJS50lENkHL.htm)|Meleeka Sanvara (1-2)|Meleeka Sanvara (1-2)|libre|
|[1Yb78i4YajKrsens.htm](pfs-season-1-bestiary/1Yb78i4YajKrsens.htm)|Nevashi (7-8)|Névashi (7-8)|libre|
|[20DJMDdNk0zLzaZZ.htm](pfs-season-1-bestiary/20DJMDdNk0zLzaZZ.htm)|Immature Giant Gecko|Gecko géant immature|libre|
|[24UI02EJVRSO6KZx.htm](pfs-season-1-bestiary/24UI02EJVRSO6KZx.htm)|House Cat|Chat de compagnie|libre|
|[26SFLzXVXqX7aEUD.htm](pfs-season-1-bestiary/26SFLzXVXqX7aEUD.htm)|Annis Hag (1-06)|Guenaude annis (1-06)|libre|
|[2cIhbpiQcXGnWdCP.htm](pfs-season-1-bestiary/2cIhbpiQcXGnWdCP.htm)|Lightning Platform (5-6)|Plate-forme d'éclairs (5-6)|libre|
|[2fUyhm29ugKzkIdS.htm](pfs-season-1-bestiary/2fUyhm29ugKzkIdS.htm)|Kobold Dragon Spellweaver|Tisseur de sort kobold draconique|libre|
|[2HEhlngODb1KfSwE.htm](pfs-season-1-bestiary/2HEhlngODb1KfSwE.htm)|Cairn Wight (PFS 1-20)|Nécrophage des tertres (PFS1-20)|libre|
|[32ifzPZVkaLfVkDa.htm](pfs-season-1-bestiary/32ifzPZVkaLfVkDa.htm)|Collapsing Ceiling (1-2)|Effondrement de plafond (1-2)|libre|
|[3fctnHmcLQM3KrZj.htm](pfs-season-1-bestiary/3fctnHmcLQM3KrZj.htm)|Greater Shadow Wisp|Flammerole de l'ombre supérieure|libre|
|[3MSKMI7ehLTC1bqw.htm](pfs-season-1-bestiary/3MSKMI7ehLTC1bqw.htm)|Webhekiz (5-6)|Webhekiz (5-6)|libre|
|[3wNJeQbBO1FDg5I9.htm](pfs-season-1-bestiary/3wNJeQbBO1FDg5I9.htm)|Creeping Slime|Vase insidieuse|libre|
|[4MXoQZUrmGeuySrp.htm](pfs-season-1-bestiary/4MXoQZUrmGeuySrp.htm)|Meleeka Sanvara (3-4)|Meleeka Sanvara (3-4)|libre|
|[4QEU9t2vYef8QfZQ.htm](pfs-season-1-bestiary/4QEU9t2vYef8QfZQ.htm)|Loyal Giant Fox|Renard géant loyal|libre|
|[54m8L1izUMDi02O9.htm](pfs-season-1-bestiary/54m8L1izUMDi02O9.htm)|Harsus (3-4)|Harsus (3-4)|libre|
|[5hR3mQw2sEy3Fy8g.htm](pfs-season-1-bestiary/5hR3mQw2sEy3Fy8g.htm)|Ghost Of Diggen Thrune (3-4)|Fantôme de Diggen Thrune (3-4)|libre|
|[5JcVwrLq7moBA3WE.htm](pfs-season-1-bestiary/5JcVwrLq7moBA3WE.htm)|Bijan And Zaynap Ohrlavi|Bijan et Zaynap Ohrlavi|libre|
|[5ss5AIhNdmoreHcd.htm](pfs-season-1-bestiary/5ss5AIhNdmoreHcd.htm)|Kobold Scout (PFS 1-11)|Éclaireur kobold (PFS 1-11)|libre|
|[5tDBVocD4ScJHKQz.htm](pfs-season-1-bestiary/5tDBVocD4ScJHKQz.htm)|Nevashi (5-6)|Névashi (5-6)|libre|
|[6dL5bhGVdRyID20x.htm](pfs-season-1-bestiary/6dL5bhGVdRyID20x.htm)|Mr. Chitters (7-8)|M. Chitters (7-8)|libre|
|[6rjkzfJcwGUVq8Uo.htm](pfs-season-1-bestiary/6rjkzfJcwGUVq8Uo.htm)|Storm Warrior-Priest|Prêtre guerrier de la tempête|libre|
|[6RnFVhsi5XImLuuF.htm](pfs-season-1-bestiary/6RnFVhsi5XImLuuF.htm)|Charlatan|Charlatan|libre|
|[6T2Ui881qJflski6.htm](pfs-season-1-bestiary/6T2Ui881qJflski6.htm)|Krooth Hatchlings|Nuée de bébés krooth|libre|
|[73j0kmPBzSCIOtq1.htm](pfs-season-1-bestiary/73j0kmPBzSCIOtq1.htm)|Deeply Flawed Ritual|Rituel profondément déficient|libre|
|[7467YYEHyLkJzXvU.htm](pfs-season-1-bestiary/7467YYEHyLkJzXvU.htm)|Technic Zombie (3-4)|Zombie techno (3-4)|libre|
|[7aBQjC0nswiFuRAX.htm](pfs-season-1-bestiary/7aBQjC0nswiFuRAX.htm)|Spiky Pit (3-4)|Piège hérissé de pointes (3-4)|libre|
|[7PXPcv8x3Wnc730i.htm](pfs-season-1-bestiary/7PXPcv8x3Wnc730i.htm)|Prefect (Fist) (3-4)|Préfecteur (Poing) (3-4)|libre|
|[7VCfX1QpRb2T7324.htm](pfs-season-1-bestiary/7VCfX1QpRb2T7324.htm)|Muraxi (5-6)|Muraxi (5-6)|libre|
|[81H7cwCK8TBi6dnv.htm](pfs-season-1-bestiary/81H7cwCK8TBi6dnv.htm)|Weak Zephyr Hawk (PFS 1-24)|Faucon zéphyr affaibli (PFS 1-24)|libre|
|[86JAsGyUAfSviibC.htm](pfs-season-1-bestiary/86JAsGyUAfSviibC.htm)|Ahksiva (1-2)|Ahksiva (1-2)|libre|
|[8axdoIyHw2WK9anh.htm](pfs-season-1-bestiary/8axdoIyHw2WK9anh.htm)|Ptiro Valner (3-4)|Ptiro Valner (3-4)|libre|
|[8G0V3AjaLUkTbq0H.htm](pfs-season-1-bestiary/8G0V3AjaLUkTbq0H.htm)|Mitflit Sneak|Mitflit furtif|libre|
|[8hdL1UOQZ1WPb6v7.htm](pfs-season-1-bestiary/8hdL1UOQZ1WPb6v7.htm)|Groetus's Chosen (3-4)|Élus de Groétus (3-4)|libre|
|[8ndVAfhYn9kROK3R.htm](pfs-season-1-bestiary/8ndVAfhYn9kROK3R.htm)|Witchwyrd (PFS 1-21)|Sorcewyrd (PFS 1-21)|libre|
|[8q3pSr4OLkcIDMIH.htm](pfs-season-1-bestiary/8q3pSr4OLkcIDMIH.htm)|Barralbus (3-4)|Barralbus (3-4)|libre|
|[8y0d3FLVkqbObMLM.htm](pfs-season-1-bestiary/8y0d3FLVkqbObMLM.htm)|Veteran Guard Captain|Capitaine de la garde vétéran|libre|
|[91rxlFyg6uGC16RJ.htm](pfs-season-1-bestiary/91rxlFyg6uGC16RJ.htm)|Ankle Trap (1-2)|Piège de cheville (1-2)|libre|
|[9dnyyDG4MCkL3zkn.htm](pfs-season-1-bestiary/9dnyyDG4MCkL3zkn.htm)|Zombie Riding Horse|Cheval de selle zombie|libre|
|[9f5t3nOOqhNykqhj.htm](pfs-season-1-bestiary/9f5t3nOOqhNykqhj.htm)|Angry Fox|Renard furieux|libre|
|[9KauvSo8zxsaBWRc.htm](pfs-season-1-bestiary/9KauvSo8zxsaBWRc.htm)|Animal Companion (Wolf) (3-4)|Compagnon animal (loup) (3-4)|libre|
|[9ZqTtwmViROBjYZf.htm](pfs-season-1-bestiary/9ZqTtwmViROBjYZf.htm)|Goblin Skeleton|Squelette gobelin|libre|
|[a15tV7snudRPQXHO.htm](pfs-season-1-bestiary/a15tV7snudRPQXHO.htm)|Toxic Snapjaw (5-6)|Claque-mâchoires toxique (5-6)|libre|
|[AaiDy0SMHX6beLgm.htm](pfs-season-1-bestiary/AaiDy0SMHX6beLgm.htm)|Season's Toll (5-6)|Valse des saison (5-6)|libre|
|[AbFKkDIHUwx2EnR5.htm](pfs-season-1-bestiary/AbFKkDIHUwx2EnR5.htm)|Dorobu (3-4)|Dorobu (3-4)|libre|
|[AFQU2tJl09ykBkwY.htm](pfs-season-1-bestiary/AFQU2tJl09ykBkwY.htm)|Thorned Cocoon (3-4)|Cocon épineux (3-4)|libre|
|[aGLtnb99MyPf3gp4.htm](pfs-season-1-bestiary/aGLtnb99MyPf3gp4.htm)|Murderous Bathhouse (5-6)|Salle de bains meurtrière (5-6)|libre|
|[agRs8xhRc6QTi9lC.htm](pfs-season-1-bestiary/agRs8xhRc6QTi9lC.htm)|Port Peril Bar Fight (Rivanti's Bar) (1-2)|Bagarre de bar à Port-Peril (Chez Rivanti) (1-2)|libre|
|[Aidv4CvhXsSXKtON.htm](pfs-season-1-bestiary/Aidv4CvhXsSXKtON.htm)|Automatic Fire Suppression (5-6)|Extincteurs de feu automatiques (5-6)|libre|
|[AKNY8155nYQ1WsmG.htm](pfs-season-1-bestiary/AKNY8155nYQ1WsmG.htm)|Skeletal Mount|Monture squelettique|libre|
|[anjyGCh5n1fj2kn3.htm](pfs-season-1-bestiary/anjyGCh5n1fj2kn3.htm)|Ruffian|Voyou|libre|
|[aWiBgV5B9sIO7UtN.htm](pfs-season-1-bestiary/aWiBgV5B9sIO7UtN.htm)|Human Bandit (3-4)|Bandit humain (3-4)|libre|
|[awImEAjEPe1LOY8m.htm](pfs-season-1-bestiary/awImEAjEPe1LOY8m.htm)|Ralthiss (1-2)|Ralthiss (1-2)|libre|
|[B437LbKlbmgXBeVV.htm](pfs-season-1-bestiary/B437LbKlbmgXBeVV.htm)|Port Peril Bar Fight (The Watchtower) (1-2)|Bagarre de bar à Port-Peril (La Tour de garde) (1-2)|libre|
|[b7VoFnPimmOwx8nX.htm](pfs-season-1-bestiary/b7VoFnPimmOwx8nX.htm)|Lelzeshin (Tier 3-4)|Lelzeshin (Tiers 3-4)|libre|
|[BcaaEonlKq3K7jxV.htm](pfs-season-1-bestiary/BcaaEonlKq3K7jxV.htm)|Steaming Fields (5-6)|Champs de vapeur (5-6)|libre|
|[bDKozdvVEElVQywQ.htm](pfs-season-1-bestiary/bDKozdvVEElVQywQ.htm)|Fleshforge Dreg (Acid Spit)|Résidu chairforgé (Crachat d'acide)|libre|
|[bfTmy4404MFMykxH.htm](pfs-season-1-bestiary/bfTmy4404MFMykxH.htm)|Dryad (1-16)|Dryade (1-16)|libre|
|[BjkteD4admd5peO7.htm](pfs-season-1-bestiary/BjkteD4admd5peO7.htm)|Lightning Platform (7-8)|Plate-forme d'éclairs (7-8)|libre|
|[bjugOjcHcBHgWTeZ.htm](pfs-season-1-bestiary/bjugOjcHcBHgWTeZ.htm)|Unkillable Zombie Brute (PFS 1-01)|Brute zombie intuable (PFS 1-01)|libre|
|[bM27sjs9yA3rX4DA.htm](pfs-season-1-bestiary/bM27sjs9yA3rX4DA.htm)|The Ascendant (5-6)|L'Ascendant (5-6)|libre|
|[bpCyglPCTMC8EL5w.htm](pfs-season-1-bestiary/bpCyglPCTMC8EL5w.htm)|Minor Summoning Rune|Rune de convocation mineure|libre|
|[BVgoWV3U3kHHAMg9.htm](pfs-season-1-bestiary/BVgoWV3U3kHHAMg9.htm)|Neidre Fliavazzana (3-4)|Neidre Fliavazzana (3-4)|libre|
|[BY9A7Isy3YWqUyyg.htm](pfs-season-1-bestiary/BY9A7Isy3YWqUyyg.htm)|Zombie Charger|Destrier zombie|libre|
|[c6Nj3CaiHSXpfehS.htm](pfs-season-1-bestiary/c6Nj3CaiHSXpfehS.htm)|Crumbling Floor|Effondrement du sol|libre|
|[cG7wNXEp67uWgwfB.htm](pfs-season-1-bestiary/cG7wNXEp67uWgwfB.htm)|Injured Sewer Ooze|Vase des égouts blessée|libre|
|[cnmrSCuK8FuiBA7J.htm](pfs-season-1-bestiary/cnmrSCuK8FuiBA7J.htm)|Ogre Glutton (PFS 1-05)|Glouton ogre (PFS 1-05)|libre|
|[CPDWLOHl0ODbMplt.htm](pfs-season-1-bestiary/CPDWLOHl0ODbMplt.htm)|Dorobu (1-2)|Dorobu (1-2)|libre|
|[Cq7YWQF3qqkMF4oN.htm](pfs-season-1-bestiary/Cq7YWQF3qqkMF4oN.htm)|Port Peril Bar Fight (The Watchtower) (3-4)|Bagarre de bar à Port-Peril (La tour de garde) (3-4)|libre|
|[Cr0jrWe49odtef2P.htm](pfs-season-1-bestiary/Cr0jrWe49odtef2P.htm)|Raven Swarm (1-05)|Nuée de Corbeaux (1-05)|libre|
|[CsGxHwDh1y8Ti4mX.htm](pfs-season-1-bestiary/CsGxHwDh1y8Ti4mX.htm)|Boggard Pit (5-6)|Fosse bourbiérienne (5-6)|libre|
|[cuTgbvAxDEkT4XhZ.htm](pfs-season-1-bestiary/cuTgbvAxDEkT4XhZ.htm)|Fleshforge Dreg (Shield)|Résidu chairforgé (Bouclier)|libre|
|[czYt4Yp6ZeGnzyV6.htm](pfs-season-1-bestiary/czYt4Yp6ZeGnzyV6.htm)|Animal Companion (Wolf) (1-2)|Compagnon animal (loup)(1-2)|libre|
|[D2Bc4GbmiuW29310.htm](pfs-season-1-bestiary/D2Bc4GbmiuW29310.htm)|Elite Boggard Swampseer (PFS 1-24, Animal Staff)|Devin des marais bourbiérin élite (PFS 1-24 Bâton animal)|libre|
|[D8CPosS1fj2ZRBv0.htm](pfs-season-1-bestiary/D8CPosS1fj2ZRBv0.htm)|Elite Skeletal Giant (PFS 1-18)|Géant squelettique d'élite (PFS 1-18)|libre|
|[D9Ogfbn0ooPAqa9X.htm](pfs-season-1-bestiary/D9Ogfbn0ooPAqa9X.htm)|Elderly Giant Tarantula|Vieille tarantule géante|libre|
|[da3XtaZGVIYkfpKE.htm](pfs-season-1-bestiary/da3XtaZGVIYkfpKE.htm)|Weak Spider Swarm|Nuée d'araignée affaiblie|libre|
|[dagMCzX17LqZfuqW.htm](pfs-season-1-bestiary/dagMCzX17LqZfuqW.htm)|Collapsing Floor|Effondrement du plancher|libre|
|[dbEMEKNFWOIQw4BQ.htm](pfs-season-1-bestiary/dbEMEKNFWOIQw4BQ.htm)|Mercenary|Mercenaire|libre|
|[DFu5ktENReo9lpbe.htm](pfs-season-1-bestiary/DFu5ktENReo9lpbe.htm)|Treacherous Quagmire|Bourbier perfide|libre|
|[Dhs9OY0qqZQAkbAp.htm](pfs-season-1-bestiary/Dhs9OY0qqZQAkbAp.htm)|Phoenix Rune (3-4)|Rune du phénix (3-4)|libre|
|[dOSeTKHADNhJ1bhz.htm](pfs-season-1-bestiary/dOSeTKHADNhJ1bhz.htm)|Kobold Dragon Mage (1-14)|Mage draconique kobold (1-14)|libre|
|[dSA9TMhvF4yw20c7.htm](pfs-season-1-bestiary/dSA9TMhvF4yw20c7.htm)|Kobold Warrior (PFS 1-11)|Guerrier Kobold (PFS 1-11)|libre|
|[DsEg9sT7jCcsuEDI.htm](pfs-season-1-bestiary/DsEg9sT7jCcsuEDI.htm)|Ogre|Ogre|libre|
|[dtXiSNcy7bM4UITo.htm](pfs-season-1-bestiary/dtXiSNcy7bM4UITo.htm)|Paravaax (3-4)|Paravaax (3-4)|libre|
|[E17afWSsCBGM8Gnq.htm](pfs-season-1-bestiary/E17afWSsCBGM8Gnq.htm)|Nalla, Rebel Leader (3-4)|Nalla, chef de la rébellion (3-4)|libre|
|[E2ExOJ9KFfGpw2H3.htm](pfs-season-1-bestiary/E2ExOJ9KFfGpw2H3.htm)|Chops (3-4)|Chops (3-4)|libre|
|[eBOY53WFYYksLmiH.htm](pfs-season-1-bestiary/eBOY53WFYYksLmiH.htm)|Dockhand Cultist|Débardeur cultiste|libre|
|[Eccdm7gQHoppXFU5.htm](pfs-season-1-bestiary/Eccdm7gQHoppXFU5.htm)|Experienced Mercenary|Mercenaire chevronné|libre|
|[EGwZ6MSs8AVHf3q6.htm](pfs-season-1-bestiary/EGwZ6MSs8AVHf3q6.htm)|Rebel|Rebelle|libre|
|[EKwisoYBBn9femPg.htm](pfs-season-1-bestiary/EKwisoYBBn9femPg.htm)|Osprey|Balbuzard|libre|
|[eR68t20AU5lPKALJ.htm](pfs-season-1-bestiary/eR68t20AU5lPKALJ.htm)|Voidworm Ouroboros|Ver du néant ouroboros|libre|
|[Et2jn8eZ1nQStapE.htm](pfs-season-1-bestiary/Et2jn8eZ1nQStapE.htm)|Goblin Warrior (PFS 1-01)|Guerrier gobelin (PFS 1-01)|libre|
|[eWlUE6xrNLGMcfMX.htm](pfs-season-1-bestiary/eWlUE6xrNLGMcfMX.htm)|Halfling Druid (1-2)|Druide halfelin (1-2)|libre|
|[F4LvpkwzeT7spRX9.htm](pfs-season-1-bestiary/F4LvpkwzeT7spRX9.htm)|Harsus (1-2)|Harsus (1-2)|libre|
|[F7gjEHGO2c956FEy.htm](pfs-season-1-bestiary/F7gjEHGO2c956FEy.htm)|Doctor Velshun (5-6)|Docteur Velshun (5-6)|libre|
|[f9jyVOOKEvRkvIY4.htm](pfs-season-1-bestiary/f9jyVOOKEvRkvIY4.htm)|'Zombie' Flesh Golem (3-4)|Golem de chair 'zombie' (3-4)|libre|
|[f9NDRa2grZrqnTIs.htm](pfs-season-1-bestiary/f9NDRa2grZrqnTIs.htm)|Webhekiz (3-4)|Webhekiz (3-4)|libre|
|[FcqRRAWeUzV89Z2s.htm](pfs-season-1-bestiary/FcqRRAWeUzV89Z2s.htm)|Mitflit (PFS 1-02)|Mitflit (PFS 1-02)|libre|
|[ff0j2iOsGGONwOVw.htm](pfs-season-1-bestiary/ff0j2iOsGGONwOVw.htm)|Seaborn Captain|Capitaine marinier|libre|
|[FG0Cc949yZ8opW8I.htm](pfs-season-1-bestiary/FG0Cc949yZ8opW8I.htm)|Hogweed Leshy (1-2)|Léchi berce ((1-2))|libre|
|[fG2rxzrSiMS01Xqe.htm](pfs-season-1-bestiary/fG2rxzrSiMS01Xqe.htm)|Unkillable Zombie Brute (PFS 1-13)|Brute zombie intuable (PFS 1-13)|libre|
|[fK69SijSGN3bOgSE.htm](pfs-season-1-bestiary/fK69SijSGN3bOgSE.htm)|Plum Leshy (3-4)|Léchi Prune (3-4)|libre|
|[FN8SN6gIqQa9d8Ex.htm](pfs-season-1-bestiary/FN8SN6gIqQa9d8Ex.htm)|Enraged Raven Swarm|Nuée de corbeaux enragés|libre|
|[FWNxPgsg0cfvrnY0.htm](pfs-season-1-bestiary/FWNxPgsg0cfvrnY0.htm)|Crowd with Trash|Foule avec des déchets|libre|
|[fXIJWv2uooumqZo8.htm](pfs-season-1-bestiary/fXIJWv2uooumqZo8.htm)|'Zombie' Flesh Golem (5-6)|Golem de chair 'zombi' (5-6)|libre|
|[FXwYc62zORdcfnYb.htm](pfs-season-1-bestiary/FXwYc62zORdcfnYb.htm)|Vengeant Thorn (3-4)|Vengeant Thorn (3-4)|libre|
|[gB7km6bV9dCXUQjZ.htm](pfs-season-1-bestiary/gB7km6bV9dCXUQjZ.htm)|Synthetic Khismar (7-8)|Khismar synthétique (7-8)|libre|
|[GdYMRHVctjyvOD7D.htm](pfs-season-1-bestiary/GdYMRHVctjyvOD7D.htm)|Prefect (Fist) (5-6)|Préfecteur (Poing) (5-6)|libre|
|[glxrhALybtp9WYh9.htm](pfs-season-1-bestiary/glxrhALybtp9WYh9.htm)|Rabid Squirrel Swarm|Nuée d'écureuils rabique|libre|
|[gq4YudXiHmpaTbbQ.htm](pfs-season-1-bestiary/gq4YudXiHmpaTbbQ.htm)|Malicious Spirits|Esprits malicieux|libre|
|[Gs2zYE86QGrDzCrv.htm](pfs-season-1-bestiary/Gs2zYE86QGrDzCrv.htm)|Human Bandit (PFS 1-06)|Bandit humain (PFS 1-06)|libre|
|[Gsqdd6TysPcSd64h.htm](pfs-season-1-bestiary/Gsqdd6TysPcSd64h.htm)|Weak Arboreal Warden|Gardien arboréen affaibli|libre|
|[Gxy0zuO7aqC9NBv0.htm](pfs-season-1-bestiary/Gxy0zuO7aqC9NBv0.htm)|Little Zura (3-4)|Petite Zura (3-4)|libre|
|[gZXQGOZO5XbWr9yD.htm](pfs-season-1-bestiary/gZXQGOZO5XbWr9yD.htm)|Elite Rebel|Rebelle élite|libre|
|[H3Iz5JJCXBLxR499.htm](pfs-season-1-bestiary/H3Iz5JJCXBLxR499.htm)|Ankle Trap (3-4)|Piège de cheville (3-4)|libre|
|[HbMQ2PXNYZH69HDP.htm](pfs-season-1-bestiary/HbMQ2PXNYZH69HDP.htm)|Hungry Blade Recruit|Stagiaire de la Lame Affamée|libre|
|[hevuaQkMCF9o5KZ2.htm](pfs-season-1-bestiary/hevuaQkMCF9o5KZ2.htm)|Zatqualmish (3-4)|Zatqualmish (3-4)|libre|
|[HffGyY6B9Kz20jOY.htm](pfs-season-1-bestiary/HffGyY6B9Kz20jOY.htm)|Lesser Giant Short-faced Bear|Ours à face courte géant inférieur|libre|
|[HFzLYAANff4fQSJZ.htm](pfs-season-1-bestiary/HFzLYAANff4fQSJZ.htm)|Season's Toll (3-4)|Valse des saison (3-4)|libre|
|[Hjl0cYnMyrcsbKlY.htm](pfs-season-1-bestiary/Hjl0cYnMyrcsbKlY.htm)|Cinder Wolf (3-4)|Loup des braises (3-4)|libre|
|[HmyF3aYbHxm8RDWf.htm](pfs-season-1-bestiary/HmyF3aYbHxm8RDWf.htm)|Quagmire|Bourbier|libre|
|[hNAN6bqWxFKjxR4Q.htm](pfs-season-1-bestiary/hNAN6bqWxFKjxR4Q.htm)|Reast Mycer (3-4)|Reast Mycer (3-4)|libre|
|[hpz50kmKMKVQWxW9.htm](pfs-season-1-bestiary/hpz50kmKMKVQWxW9.htm)|Scrabbling Hands|Mains crapahuteuses|libre|
|[HYUQOdH0WB6ckC5e.htm](pfs-season-1-bestiary/HYUQOdH0WB6ckC5e.htm)|Moldy Foodstuffs (3-4)|Denrées alimentaires moisies (3-4)|libre|
|[I3jGuCG3qL5ua7yE.htm](pfs-season-1-bestiary/I3jGuCG3qL5ua7yE.htm)|Glass River Midge|Moucheron de la Rivière de verre|libre|
|[I3RciQPRS1LWb5Sv.htm](pfs-season-1-bestiary/I3RciQPRS1LWb5Sv.htm)|Spitting Acid 'Bushes' (7-8)|'Buissons' cracheurs d'acide (7-8)|libre|
|[i4PIffSaQO2ry5nb.htm](pfs-season-1-bestiary/i4PIffSaQO2ry5nb.htm)|Kobold Warrior (1-14)|Guerrier Kobold (1-14)|libre|
|[I4Vcd1ce8ltK8OqJ.htm](pfs-season-1-bestiary/I4Vcd1ce8ltK8OqJ.htm)|Ruffian (Q11)|Voyou (Q11)|libre|
|[IcbDCGnefdowfQE0.htm](pfs-season-1-bestiary/IcbDCGnefdowfQE0.htm)|Skeleton Captain|Capitaine squelette|libre|
|[ihm2nXJtzcgFUJRN.htm](pfs-season-1-bestiary/ihm2nXJtzcgFUJRN.htm)|Statue Of Set|Statue de Set|libre|
|[iozA5orydjkBmbHx.htm](pfs-season-1-bestiary/iozA5orydjkBmbHx.htm)|Farmer Drystan's Scarecrow (3-4)|Épouvantail de Drystan le fermier (3-4)|libre|
|[iPInBeC8feDsmP2e.htm](pfs-season-1-bestiary/iPInBeC8feDsmP2e.htm)|Moldy Deck|Pont moisi|libre|
|[IsNHPVT7c6K42rIR.htm](pfs-season-1-bestiary/IsNHPVT7c6K42rIR.htm)|Malfunctioning Annihilator|Exterminateur dysfonctionnel|libre|
|[JDjwID3HZJqytPFP.htm](pfs-season-1-bestiary/JDjwID3HZJqytPFP.htm)|Pine Brute|Brute des pins|libre|
|[jEjUHCcGcGinFqOU.htm](pfs-season-1-bestiary/jEjUHCcGcGinFqOU.htm)|Chops (1-2)|Chops (1-2)|libre|
|[JEwMnYFfyxr5i6Dy.htm](pfs-season-1-bestiary/JEwMnYFfyxr5i6Dy.htm)|Fleshforge Dreg (Roots)|Résidu chairforgé (Racines)|libre|
|[JF257wpwvWWjCsN7.htm](pfs-season-1-bestiary/JF257wpwvWWjCsN7.htm)|Reefclaw (PFS 1-12)|Pince des récifs (PFS 1-12)|libre|
|[Jfm63bPimFxf3u1T.htm](pfs-season-1-bestiary/Jfm63bPimFxf3u1T.htm)|Halfling Druid (3-4)|Halfling druid (3-4)|libre|
|[jJd7Sa8DB7uBOn93.htm](pfs-season-1-bestiary/jJd7Sa8DB7uBOn93.htm)|Bandit Disciple (3-4)|Bandit disciple (3-4)|libre|
|[jkGd8nN4hnNLGS2D.htm](pfs-season-1-bestiary/jkGd8nN4hnNLGS2D.htm)|Mummy Shambler|Titubeur momie|libre|
|[jMjK6j16K1NC2BdC.htm](pfs-season-1-bestiary/jMjK6j16K1NC2BdC.htm)|Weak Soulbound Homunculus|Homoncule des âmes affaibli|libre|
|[js2sA6P939oTuLuI.htm](pfs-season-1-bestiary/js2sA6P939oTuLuI.htm)|Ahrkinos (5-6)|Ahrkinos (5-6)|libre|
|[JSZGXyG3cItFmG9N.htm](pfs-season-1-bestiary/JSZGXyG3cItFmG9N.htm)|Elite Waterworks Rebel|Rebelle élite de Waterwork|libre|
|[jXEmUa47j5o3lQIE.htm](pfs-season-1-bestiary/jXEmUa47j5o3lQIE.htm)|Barbed Bloodseeker|Cherchesang barbelé|libre|
|[K6JYKFI9HLFWf12d.htm](pfs-season-1-bestiary/K6JYKFI9HLFWf12d.htm)|Guard Captain|Capitaine de la garde|libre|
|[k76O7wKWMJrycd9i.htm](pfs-season-1-bestiary/k76O7wKWMJrycd9i.htm)|Collapsing Barricade (1-2)|Effondrement de la barricade (1-2)|libre|
|[KcLEkekZZw9uI88I.htm](pfs-season-1-bestiary/KcLEkekZZw9uI88I.htm)|Toxic Snapjaw (3-4)|Claque-machoîre toxique (3-4)|libre|
|[kDEiL11fbXWHQrdH.htm](pfs-season-1-bestiary/kDEiL11fbXWHQrdH.htm)|The Ascendant (3-4)|L'Ascendant (3-4)|libre|
|[KFAjzSMh6HAyJMhd.htm](pfs-season-1-bestiary/KFAjzSMh6HAyJMhd.htm)|Port Peril Bar Fight (Last Catch) (1-2)|Bagarre de bar à Port-Peril (Dernière Heure) (1-2)|libre|
|[kfoGOOgTkRjIzD4U.htm](pfs-season-1-bestiary/kfoGOOgTkRjIzD4U.htm)|Kanker|Kanker|libre|
|[KL2L7m5WlL21WcGN.htm](pfs-season-1-bestiary/KL2L7m5WlL21WcGN.htm)|Port Peril Bar Fight (Rivanti's Bar) (3-4)|Bagarre de bar à Port-Peril (Chez Rivanti) (3-4)|libre|
|[KorFY6YT3UBrzXOL.htm](pfs-season-1-bestiary/KorFY6YT3UBrzXOL.htm)|Spitting Acid 'Bushes' (5-6)|'Buissons' cracheurs d'acide (5-6)|libre|
|[KoYqhZoY3RSBEo5Y.htm](pfs-season-1-bestiary/KoYqhZoY3RSBEo5Y.htm)|Submerged Shard|Tesson submergé|libre|
|[kpYq0piOlafgYSdo.htm](pfs-season-1-bestiary/kpYq0piOlafgYSdo.htm)|Ghost Of Diggen Thrune (1-2)|Fantôme de Diggen Thrune (1-2)|libre|
|[KR3t3QnVESdhkYr2.htm](pfs-season-1-bestiary/KR3t3QnVESdhkYr2.htm)|Barralbus (1-2)|Barralbus (1-2)|libre|
|[ksClnBrkXAhoq4Di.htm](pfs-season-1-bestiary/ksClnBrkXAhoq4Di.htm)|Groetus's Chosen (1-2)|Élus de Groétus (1-2)|libre|
|[KsTZYCptXLSGgrcW.htm](pfs-season-1-bestiary/KsTZYCptXLSGgrcW.htm)|Whispering Spirits|Esprits chuchoteurs|libre|
|[Kv7ciLOLN8vDgIGy.htm](pfs-season-1-bestiary/Kv7ciLOLN8vDgIGy.htm)|Kobold Dragon Mage (PFS 1-11)|Mage draconique kobold (PFS 1-11)|libre|
|[Kz4YDRpaUaL3BQd7.htm](pfs-season-1-bestiary/Kz4YDRpaUaL3BQd7.htm)|Spear Launcher (Q9:1-2)|Lance-épieu (Q9:1-2)|libre|
|[l8PzmglYbEtM5Xy6.htm](pfs-season-1-bestiary/l8PzmglYbEtM5Xy6.htm)|Squirrel Swarm (1-04)|Nuée d'écureuils (1-04)|libre|
|[LAo7jI8NvMfuDbek.htm](pfs-season-1-bestiary/LAo7jI8NvMfuDbek.htm)|Great White Shark (PFS 1-12)|Grand requin blanc (PFS 1-12)|libre|
|[LAZDcdX1ISXJCqPd.htm](pfs-season-1-bestiary/LAZDcdX1ISXJCqPd.htm)|Steel Mephit|Méphite d'acier|libre|
|[lhSKgDzZm8FZknvG.htm](pfs-season-1-bestiary/lhSKgDzZm8FZknvG.htm)|Kobold Scout (1-14)|Éclaireur Kobold (1-14)|libre|
|[LKdTBOV9LUiFrKOU.htm](pfs-season-1-bestiary/LKdTBOV9LUiFrKOU.htm)|Sea Devil Invader|Diable des mers envahisseur|libre|
|[LMyYMgdcJqQPgGiY.htm](pfs-season-1-bestiary/LMyYMgdcJqQPgGiY.htm)|Iloise (7-8)|Iloise (7-8)|libre|
|[loJBcC34yhpVwfio.htm](pfs-season-1-bestiary/loJBcC34yhpVwfio.htm)|Rending Hands|Mains éventreuses|libre|
|[LOTsEiTCiONfJl3s.htm](pfs-season-1-bestiary/LOTsEiTCiONfJl3s.htm)|Lion|Lion|libre|
|[lvWJic1WYdyJATa0.htm](pfs-season-1-bestiary/lvWJic1WYdyJATa0.htm)|Manifestation of Qxal (3-4)|Manifestation de Qxal (3-4)|libre|
|[LXXodovLOERAGqWj.htm](pfs-season-1-bestiary/LXXodovLOERAGqWj.htm)|Sodden Floor|Sol spongieux|libre|
|[lzDOAywWh0mP4UeH.htm](pfs-season-1-bestiary/lzDOAywWh0mP4UeH.htm)|Fleshforge Prototype (Acid Spit)|Prototype Chairforgé (Crachat d'acide)|libre|
|[m3YBXS3ool7PmRfX.htm](pfs-season-1-bestiary/m3YBXS3ool7PmRfX.htm)|Fleshforge Prototype (Shield)|Prototype chairforgé (Bouclier)|libre|
|[mhKK0mhhI9V3OiUQ.htm](pfs-season-1-bestiary/mhKK0mhhI9V3OiUQ.htm)|Acid Spray Fountain|Fontaine à projection d'acide|libre|
|[mIz8CjpB8uxf6M9f.htm](pfs-season-1-bestiary/mIz8CjpB8uxf6M9f.htm)|Cinder Wolf (1-2)|Loup des braises (1-2)|libre|
|[mjD8kNTb8IC5FBNA.htm](pfs-season-1-bestiary/mjD8kNTb8IC5FBNA.htm)|Ravenous Giant Centipede|Mille-pattes géant affamé|libre|
|[MJtndSx73zEx8702.htm](pfs-season-1-bestiary/MJtndSx73zEx8702.htm)|Guard|Garde|libre|
|[mjW1q98gMnEmZJKt.htm](pfs-season-1-bestiary/mjW1q98gMnEmZJKt.htm)|Shadow Wisp|Flammerole de l'ombre|libre|
|[MOPGJAQUS4Aop7Mm.htm](pfs-season-1-bestiary/MOPGJAQUS4Aop7Mm.htm)|Doctor Velshun (7-8)|Docteur Velshun (7-8)|libre|
|[MOX8VzhEBVMkOpzr.htm](pfs-season-1-bestiary/MOX8VzhEBVMkOpzr.htm)|Crowd Agitator|Agitateur de foule|libre|
|[mTXgvQ7zhA8VNVeG.htm](pfs-season-1-bestiary/mTXgvQ7zhA8VNVeG.htm)|Orc Alchemist|Alchimiste orc|libre|
|[mUWCqhhcmEHsSjKo.htm](pfs-season-1-bestiary/mUWCqhhcmEHsSjKo.htm)|Blue Streak Mage (5-6)|Mage de la Marque bleue (5-6)|libre|
|[mWzScgUiWAGEb0RE.htm](pfs-season-1-bestiary/mWzScgUiWAGEb0RE.htm)|Human Bandit (5-6)|Bandit humain (5-6)|libre|
|[N6zflwcAIP2wewRI.htm](pfs-season-1-bestiary/N6zflwcAIP2wewRI.htm)|Skeleton Soldier|Soldat squelette|libre|
|[Nh3mKGv0uy1tovpL.htm](pfs-season-1-bestiary/Nh3mKGv0uy1tovpL.htm)|Keff The Lion (3-4)|Keff le lion (3-4)|libre|
|[NhMS6b3W6gXefijq.htm](pfs-season-1-bestiary/NhMS6b3W6gXefijq.htm)|Offended High Roller|Flambeur vexé|libre|
|[NmBBGhDNNEEfxCun.htm](pfs-season-1-bestiary/NmBBGhDNNEEfxCun.htm)|Skeletal Gladiator|Gladiateur squelettique|libre|
|[NmkeNdV7SWYG7bYe.htm](pfs-season-1-bestiary/NmkeNdV7SWYG7bYe.htm)|Vengeant Thorn (1-2)|Vengeant Thorn (1-2)|libre|
|[nmNVx7cAqeurlbeu.htm](pfs-season-1-bestiary/nmNVx7cAqeurlbeu.htm)|Hungry Blade Apprentice|Apprenti de la Lame affamée|libre|
|[NQgsuMnXXABeebuS.htm](pfs-season-1-bestiary/NQgsuMnXXABeebuS.htm)|Prefect (Electrowhip) (3-4)|Préfecteur (Fouet électrisé) (3-4)|libre|
|[Nr8LlIa0kRW71fJs.htm](pfs-season-1-bestiary/Nr8LlIa0kRW71fJs.htm)|Phoenix Rune (1-2)|Rune du phénix (1-2)|libre|
|[NTPKBjuubDmuSJTN.htm](pfs-season-1-bestiary/NTPKBjuubDmuSJTN.htm)|Vicious Vulpine|Vulpin vicieux|libre|
|[NUFsxlmHqk4LeYlq.htm](pfs-season-1-bestiary/NUFsxlmHqk4LeYlq.htm)|Greater Metallic Sod Hound|Molosse de tourbe métallique supérieur|libre|
|[NZvXkQHfXM2XS50I.htm](pfs-season-1-bestiary/NZvXkQHfXM2XS50I.htm)|Weak Green Hag|Guenaude verte affaiblie|libre|
|[O6YxMaRh6tRGnnbl.htm](pfs-season-1-bestiary/O6YxMaRh6tRGnnbl.htm)|Biloko Veteran (1-16)|Vétéran biloko (1-16)|libre|
|[ocdy8RFkw10tCUuY.htm](pfs-season-1-bestiary/ocdy8RFkw10tCUuY.htm)|Biloko Warrior (1-16)|Guerrier biloko (1-16)|libre|
|[oEHGUzWB0IH26h7y.htm](pfs-season-1-bestiary/oEHGUzWB0IH26h7y.htm)|Mummy Brute|Brute momie|libre|
|[oGYYCFUoopC8MSYS.htm](pfs-season-1-bestiary/oGYYCFUoopC8MSYS.htm)|Deadfall Trap (1-2)|Piège assommoir (1-2)|libre|
|[oHbuqmMgS8PutzMZ.htm](pfs-season-1-bestiary/oHbuqmMgS8PutzMZ.htm)|Blue Streak Mage (7-8)|Mage de la Marque bleue (7-8)|libre|
|[oKACWOSiBpxgMqa4.htm](pfs-season-1-bestiary/oKACWOSiBpxgMqa4.htm)|Prefect (Electrowhip) (5-6)|Préfecteur (Fouet électrisé) (5-6)|libre|
|[OOG9wbIY1mC6ehzS.htm](pfs-season-1-bestiary/OOG9wbIY1mC6ehzS.htm)|Dead Organ (5-6)|Orgue pour les morts-vivants (5-6)|libre|
|[oQmjMw2MTQDSGRs7.htm](pfs-season-1-bestiary/oQmjMw2MTQDSGRs7.htm)|Gwibble (3-4)|Gwibble (3-4)|libre|
|[ORzg1jprbdWl64gA.htm](pfs-season-1-bestiary/ORzg1jprbdWl64gA.htm)|Murderous Bathhouse (3-4)|Salle de bain meurtrière (3-4)|libre|
|[OTTvIkBvJ2ABYwEd.htm](pfs-season-1-bestiary/OTTvIkBvJ2ABYwEd.htm)|Whispering Souls|Âmes chuchoteuses|libre|
|[OtYNhDlyXrVWxeiS.htm](pfs-season-1-bestiary/OtYNhDlyXrVWxeiS.htm)|Damaged Alchemical Golem|Golem alchimique endommagé|libre|
|[oVg5QxC9vf0QQQeb.htm](pfs-season-1-bestiary/oVg5QxC9vf0QQQeb.htm)|Port Peril Bar Fight (Last Catch) (3-4)|Bagarre de bar à Port-Peril (Dernière heure) (3-4)|libre|
|[peNTVZwcm8Tg7s2e.htm](pfs-season-1-bestiary/peNTVZwcm8Tg7s2e.htm)|Blue Streak Sentry|Sentinelle de la Marque bleue|libre|
|[PFSkqGyg7eGaES1k.htm](pfs-season-1-bestiary/PFSkqGyg7eGaES1k.htm)|Technic Zombie (5-6)|Zombie techno ((5-6))|libre|
|[pG7P9AxqzcsCjikh.htm](pfs-season-1-bestiary/pG7P9AxqzcsCjikh.htm)|Zombie Shambler (PFS 1-03)|Titubeur zombie (PFS 1-03)|libre|
|[pJck5AXRMWcAequ5.htm](pfs-season-1-bestiary/pJck5AXRMWcAequ5.htm)|Boggard Warrior (PFS 1-09)|Guerrier bourbiérin (PFS 1-09)|libre|
|[pPpc1IevAeLYHZsh.htm](pfs-season-1-bestiary/pPpc1IevAeLYHZsh.htm)|Ahksiva (3-4)|Ahksiva (3-4)|libre|
|[Pq4fqQkoghoXPBoi.htm](pfs-season-1-bestiary/Pq4fqQkoghoXPBoi.htm)|Young Bloodseeker Swarm|Nuée de jeunes cherchesangs|libre|
|[PQaQSSjfkoV9mZzi.htm](pfs-season-1-bestiary/PQaQSSjfkoV9mZzi.htm)|Giant Short-faced Bear|Ours à face courte géant|libre|
|[pSQYsY76zjDMfjv2.htm](pfs-season-1-bestiary/pSQYsY76zjDMfjv2.htm)|Blue Streak Burglar|Cambrioleur de la Marque bleue|libre|
|[ptDF1ghCxNHGhH0h.htm](pfs-season-1-bestiary/ptDF1ghCxNHGhH0h.htm)|Greater Steel Mephit|Méphite d'acier supérieur|libre|
|[PtU1netfc7U8MaR4.htm](pfs-season-1-bestiary/PtU1netfc7U8MaR4.htm)|Kip The Druid (1-2)|Kip le druide (1-2)|libre|
|[pWJUeb0LfCXFSYpE.htm](pfs-season-1-bestiary/pWJUeb0LfCXFSYpE.htm)|Dilapidated Alchemical Golem|Golem Alchimique dilapidé|libre|
|[PX7QH8D43Bax2LC4.htm](pfs-season-1-bestiary/PX7QH8D43Bax2LC4.htm)|Moldy Foodstuffs (1-2)|Denrées alimentaires moisies (1-2)|libre|
|[pxDyzHzkEQTz3R1K.htm](pfs-season-1-bestiary/pxDyzHzkEQTz3R1K.htm)|Leshy Crafter (1-2)|Artisan léchi (1-2)|libre|
|[PXXHRzLRLB4bJ7M5.htm](pfs-season-1-bestiary/PXXHRzLRLB4bJ7M5.htm)|Fleshforge Dreg (Reach)|Résidu chairforgé (Allonge)|libre|
|[Qcl0FPQS2Ovr179p.htm](pfs-season-1-bestiary/Qcl0FPQS2Ovr179p.htm)|Collapsing Ceiling (3-4)|Effondrement de plafond (3-4)|libre|
|[Qeu2rsI1J5nohgm7.htm](pfs-season-1-bestiary/Qeu2rsI1J5nohgm7.htm)|Giant Centipede (PFS 1-03)|Mille-pattes géant (PFS 1-03)|libre|
|[qgWB2lP5EyaHsvKH.htm](pfs-season-1-bestiary/qgWB2lP5EyaHsvKH.htm)|Summoning Rune (PFS 1-11)|Rune de convocation (PFS 1-11)|libre|
|[QMT1Yfa2IrNrdZGP.htm](pfs-season-1-bestiary/QMT1Yfa2IrNrdZGP.htm)|Dire Cinder Wolf|Loup sanguinaire des braises|libre|
|[QtDH32oEVWpFmlCi.htm](pfs-season-1-bestiary/QtDH32oEVWpFmlCi.htm)|Lelzeshin (Tier 5-6)|Lelzeshin (Tiers 5-6)|libre|
|[QUimZFYGY12QjoC4.htm](pfs-season-1-bestiary/QUimZFYGY12QjoC4.htm)|Kip The Druid (3-4)|Kip le druide (3-4)|libre|
|[qumj2VUZ6aTnjjIU.htm](pfs-season-1-bestiary/qumj2VUZ6aTnjjIU.htm)|Plum Leshy (1-2)|Léchi Prune (1-2)|libre|
|[qXd4fcaY4cWKvj3G.htm](pfs-season-1-bestiary/qXd4fcaY4cWKvj3G.htm)|Spear Launcher (Q9:3-4)|Lance-épieu (Q9:3-4)|libre|
|[QxlguPRUm3rDPhjF.htm](pfs-season-1-bestiary/QxlguPRUm3rDPhjF.htm)|Waterworks Rebel|Rebelle de Waterwork|libre|
|[QYTRwhdkSWZKRtlj.htm](pfs-season-1-bestiary/QYTRwhdkSWZKRtlj.htm)|Synthetic Khismar (5-6)|Khismar synthétique (5-6)|libre|
|[QZVLjVPLl0PNo2K3.htm](pfs-season-1-bestiary/QZVLjVPLl0PNo2K3.htm)|Fleshforge Prototype (Leaping Charge)|Prototype chairforgé (Charge bondissante)|libre|
|[R8D2GrilngQIb1eI.htm](pfs-season-1-bestiary/R8D2GrilngQIb1eI.htm)|Fleshforge Prototype (Reach)|Prototype chairforgé (Allonge)|libre|
|[rEPnlpuRVCpnQNYP.htm](pfs-season-1-bestiary/rEPnlpuRVCpnQNYP.htm)|Zombie Warhorse|Cheval de guerre zombie|libre|
|[rJ9IdtFmjnqtN02o.htm](pfs-season-1-bestiary/rJ9IdtFmjnqtN02o.htm)|Palace Guard (PFS 1-17)|Garde de palais (PFS 1-17)|libre|
|[RLT5yfpN3FIgs3b6.htm](pfs-season-1-bestiary/RLT5yfpN3FIgs3b6.htm)|Spiky Pit (1-2)|Piège hérissé de pointes (1-2)|libre|
|[rOYazaPBX73nLCJE.htm](pfs-season-1-bestiary/rOYazaPBX73nLCJE.htm)|Manifestation of Qxal (5-6)|Manifestation de Qxal (5-6)|libre|
|[rrlWzq8qKpOgaq7P.htm](pfs-season-1-bestiary/rrlWzq8qKpOgaq7P.htm)|Mountain Goat|Chèvre de montagne|libre|
|[RS04fRQEWzk2aZOx.htm](pfs-season-1-bestiary/RS04fRQEWzk2aZOx.htm)|Desert Wind|Vent du désert|libre|
|[RUXPx9VPJCmuD1V8.htm](pfs-season-1-bestiary/RUXPx9VPJCmuD1V8.htm)|Mr. Chitters (5-6)|M. Chitters (5-6)|libre|
|[RYhsJ69TGrcvtfaM.htm](pfs-season-1-bestiary/RYhsJ69TGrcvtfaM.htm)|Giant Osprey|Balbuzard géant|libre|
|[RzLRzsHVjaPBKCkh.htm](pfs-season-1-bestiary/RzLRzsHVjaPBKCkh.htm)|Hogweed Leshy (3-4)|Léchi Berce (3-4)|libre|
|[S50loNsuTigldGll.htm](pfs-season-1-bestiary/S50loNsuTigldGll.htm)|Bandit Disciple (1-2)|Disciple bandit (1-2)|libre|
|[Si5l2FvDCCmG6DRa.htm](pfs-season-1-bestiary/Si5l2FvDCCmG6DRa.htm)|Skeleton Corporal|Brigadier squelette|libre|
|[sLt5RvozduZ2soA5.htm](pfs-season-1-bestiary/sLt5RvozduZ2soA5.htm)|Little Zura (1-2)|Petite Zura (1-2)|libre|
|[sN5jueRXgYI9DDGH.htm](pfs-season-1-bestiary/sN5jueRXgYI9DDGH.htm)|Fleshforge Prototype (Breath Weapon)|Prototype chairforgé (Arme de souffle)|libre|
|[SprnhrgMbZxb59fA.htm](pfs-season-1-bestiary/SprnhrgMbZxb59fA.htm)|Nalla, Rebel Leader (1-2)|Nalla, Chef de la rébellion (1-2)|libre|
|[stqY18rymB2XUSHq.htm](pfs-season-1-bestiary/stqY18rymB2XUSHq.htm)|Dead Organ (7-8)|Orgue pour les morts-vivants (7-8)|libre|
|[t1QyIgwLORMhHNL4.htm](pfs-season-1-bestiary/t1QyIgwLORMhHNL4.htm)|Fleshforge Dreg (Breath Weapon)|Résidu chairforgé (Arme de souffle)|libre|
|[t238i4Zw2JsDvUop.htm](pfs-season-1-bestiary/t238i4Zw2JsDvUop.htm)|Metallic Sod Hound|Molosse de tourbe métallique|libre|
|[t93p04HpLq4O2BjR.htm](pfs-season-1-bestiary/t93p04HpLq4O2BjR.htm)|Boggard Pit (3-4)|Fosse bourbiérienne (3-4)|libre|
|[tBY1ayiQyI5tyME6.htm](pfs-season-1-bestiary/tBY1ayiQyI5tyME6.htm)|Ptiro Valner (1-2)|Ptiro Valner   (1-2)|libre|
|[tDKhy3OvsOX8Hzik.htm](pfs-season-1-bestiary/tDKhy3OvsOX8Hzik.htm)|Nashaxian The Bored|Nashaxian Le blasé|libre|
|[TmDKaw1WenWKvNBU.htm](pfs-season-1-bestiary/TmDKaw1WenWKvNBU.htm)|Tough Mountain Goat|Chèvre de montagne coriace|libre|
|[toAsLLjdRwu1pT0b.htm](pfs-season-1-bestiary/toAsLLjdRwu1pT0b.htm)|Fleshforge Prototype (Roots)|Prototype chairforgé (Racines)|libre|
|[tShwzNRi8eEqznAi.htm](pfs-season-1-bestiary/tShwzNRi8eEqznAi.htm)|Collapsing Barricade (3-4)|Effondrement de la barricade (1-2)|libre|
|[tt4tZR8udjJ1zNyp.htm](pfs-season-1-bestiary/tt4tZR8udjJ1zNyp.htm)|Seaborn Fisher|Pêcheur marinier|libre|
|[ttGtrGAOC2QRBWjn.htm](pfs-season-1-bestiary/ttGtrGAOC2QRBWjn.htm)|Trapmaster Tok's Surprise (1-2)|Surprise de Tok le Maître des pièges (1-2)|libre|
|[tuuajdgozH7Tueiv.htm](pfs-season-1-bestiary/tuuajdgozH7Tueiv.htm)|Elite Sewer Ooze (Q9)|Vase des égouts élite (Q9)|libre|
|[U2igHGiYNNclSdsm.htm](pfs-season-1-bestiary/U2igHGiYNNclSdsm.htm)|Spirit Guardian Statue|Statue de l'esprit gardien|libre|
|[u2TGKzPfGniZLL14.htm](pfs-season-1-bestiary/u2TGKzPfGniZLL14.htm)|Flesh Golem (PFS 1-25)|Golem de chair (PFS 1-25)|libre|
|[U6mEWRAurIbSgtnF.htm](pfs-season-1-bestiary/U6mEWRAurIbSgtnF.htm)|Giant Shrew|Musaraigne géante|libre|
|[uedXIip0UPHJ4Egt.htm](pfs-season-1-bestiary/uedXIip0UPHJ4Egt.htm)|Flaming Skull Skeleton Guard|Crâne enflammé de garde squelettique|libre|
|[UGB9w4JLH8GFhew7.htm](pfs-season-1-bestiary/UGB9w4JLH8GFhew7.htm)|Jury-Rigged Annihilator|Exterminateur de fortune|libre|
|[Uhb6f4ettpS7K2Jl.htm](pfs-season-1-bestiary/Uhb6f4ettpS7K2Jl.htm)|Blue Streak Ambushers (7-8)|Piégeur de la Marque bleue (7-8)|libre|
|[UjVrEU4MTBH2eZOq.htm](pfs-season-1-bestiary/UjVrEU4MTBH2eZOq.htm)|Weak Ceustodaemon (PFS 1-01)|Ceustodaémon (PFS 1-01) affaibli|libre|
|[UKGbCdZlsWmjZiFN.htm](pfs-season-1-bestiary/UKGbCdZlsWmjZiFN.htm)|Automatic Fire Suppression (3-4)|Extincteurs de feu automatiques (3-4)|libre|
|[ul23A9ZfCMCUwADK.htm](pfs-season-1-bestiary/ul23A9ZfCMCUwADK.htm)|Jinkin (PFS 1-17)|Jinkin (PFS 1-17)|libre|
|[UlBksr4oy0gT3b3g.htm](pfs-season-1-bestiary/UlBksr4oy0gT3b3g.htm)|Guard (PFS 1-17)|Garde (PFS 1-17)|libre|
|[uQdILi1siFfXewFf.htm](pfs-season-1-bestiary/uQdILi1siFfXewFf.htm)|Zombie Minihulk|Mini mastodonte zombie|libre|
|[UQyvGqfFcYva3fzc.htm](pfs-season-1-bestiary/UQyvGqfFcYva3fzc.htm)|Zatqualmish (5-6)|Zatqualmish (5-6)|libre|
|[uRUiLihSFDU78IVP.htm](pfs-season-1-bestiary/uRUiLihSFDU78IVP.htm)|Aeon Nexus (5-6)|Nexus de pierre d'éternité (5-6)|libre|
|[ut2cLLH0tvJV2ewg.htm](pfs-season-1-bestiary/ut2cLLH0tvJV2ewg.htm)|Elite Ghast (PFS 1-07)|Blême d'élite (PFS 1-07)|libre|
|[uwAyhK6sEoOyoR96.htm](pfs-season-1-bestiary/uwAyhK6sEoOyoR96.htm)|Vigilant Guard|Garde vigilant|libre|
|[UyJUEp0asfGDBvYa.htm](pfs-season-1-bestiary/UyJUEp0asfGDBvYa.htm)|Elite Prefect (Fist) (3-4)|Prefect d'élite (Poing) 3-4|libre|
|[uZWktkWAmzadBTh1.htm](pfs-season-1-bestiary/uZWktkWAmzadBTh1.htm)|Thorned Cocoon (5-6)|Cocon épineux (5-6)|libre|
|[V0Lsna2i106JZYHY.htm](pfs-season-1-bestiary/V0Lsna2i106JZYHY.htm)|Flawed Ritual|Rituel déficient|libre|
|[v4XGvSCIj9dVS5Z9.htm](pfs-season-1-bestiary/v4XGvSCIj9dVS5Z9.htm)|Steaming Fields (7-8)|Champ de vapeur (7-8)|libre|
|[v8Spc0ALJdX8WUi4.htm](pfs-season-1-bestiary/v8Spc0ALJdX8WUi4.htm)|Eerie Skeletal Giant|Géant squelettique étrange|libre|
|[vgn39Yta3lgeRP1s.htm](pfs-season-1-bestiary/vgn39Yta3lgeRP1s.htm)|Muraxi (3-4)|Muraxi (3-4)|libre|
|[vLOwfWlS2XQQYhD2.htm](pfs-season-1-bestiary/vLOwfWlS2XQQYhD2.htm)|Raging Forest Fire|Feu de forêt violent|libre|
|[vtSVLCmJvef8980U.htm](pfs-season-1-bestiary/vtSVLCmJvef8980U.htm)|Leopard (1-14)|Léopard (1-14)|libre|
|[vV6a488IlsKsHpL8.htm](pfs-season-1-bestiary/vV6a488IlsKsHpL8.htm)|Ahrkinos (3-4)|Ahrkinos (3-4)|libre|
|[vve98ekdPx8nJbF1.htm](pfs-season-1-bestiary/vve98ekdPx8nJbF1.htm)|Injured Weak Otyugh|Otyugh blessé affaibli|libre|
|[w8pkSmcvpZfp7Hsw.htm](pfs-season-1-bestiary/w8pkSmcvpZfp7Hsw.htm)|Larraz Virtanne|Larraz Virtanne|libre|
|[wctTtNkLjyMeCCi2.htm](pfs-season-1-bestiary/wctTtNkLjyMeCCi2.htm)|Skeletal Mage|Mage squelettique|libre|
|[wCZcZnEHskvccWJE.htm](pfs-season-1-bestiary/wCZcZnEHskvccWJE.htm)|Young Sea Serpent|Jeune serpent de mer|libre|
|[WdmbEmvLzPciM7au.htm](pfs-season-1-bestiary/WdmbEmvLzPciM7au.htm)|Adolescent Sea Serpent|Serpent de mer adolescent|libre|
|[wGjNDVC0mDsk7rrH.htm](pfs-season-1-bestiary/wGjNDVC0mDsk7rrH.htm)|Pile of Fireworks (3-4)|Pile de feux d'artifice (3-4)|libre|
|[whbIBJJxlv5oVm6c.htm](pfs-season-1-bestiary/whbIBJJxlv5oVm6c.htm)|Leshy Crafter (3-4)|Artisan léchi (3-4)|libre|
|[WlJ8fVMV7gXNQ9eR.htm](pfs-season-1-bestiary/WlJ8fVMV7gXNQ9eR.htm)|Fleshforge Dreg (Leaping Charge)|Résidu chairforgé (Charge bondissante)|libre|
|[WM8ycweNEOEFE08F.htm](pfs-season-1-bestiary/WM8ycweNEOEFE08F.htm)|Aeon Nexus (3-4)|Nexus de pierre d'éternité (3-4)|libre|
|[wPe1IWcxMinHkuJE.htm](pfs-season-1-bestiary/wPe1IWcxMinHkuJE.htm)|Vengeful Spirits|Esprits vengeurs|libre|
|[WT62cRWceB4NlSE8.htm](pfs-season-1-bestiary/WT62cRWceB4NlSE8.htm)|Smoldering Forest Fire|Feu de forêt ardent|libre|
|[wv8PrDy91ae3njBv.htm](pfs-season-1-bestiary/wv8PrDy91ae3njBv.htm)|Rebel Brute|Brute rebelle|libre|
|[X6xVydw9XznvvFC2.htm](pfs-season-1-bestiary/X6xVydw9XznvvFC2.htm)|Mirrored Lelzeshin (Tier 3-4)|Image miroir de Lelzeshin (Tiers 3-4)|libre|
|[xbpF7O68n876hD3B.htm](pfs-season-1-bestiary/xbpF7O68n876hD3B.htm)|Storm Acolyte|Acolyte de la tempête|libre|
|[xFeeszDfh4RISbib.htm](pfs-season-1-bestiary/xFeeszDfh4RISbib.htm)|Gwibble (1-2)|Gwibble (1-2)|libre|
|[xHbU1j5KpscKk6q8.htm](pfs-season-1-bestiary/xHbU1j5KpscKk6q8.htm)|Marcon Tinol (3-4)|Marcon Tinol (3-4)|libre|
|[XiGTud90i1WkJYz2.htm](pfs-season-1-bestiary/XiGTud90i1WkJYz2.htm)|Offended Gambler|Parieur vexé|libre|
|[xIWdGhSA6Z7CQKBY.htm](pfs-season-1-bestiary/xIWdGhSA6Z7CQKBY.htm)|Marcon Tinol (1-2)|Marcon Tinol (1-2)|libre|
|[xKanh4M6QEF39bvd.htm](pfs-season-1-bestiary/xKanh4M6QEF39bvd.htm)|Zombie Brute (PFS 1-18)|Brute zombie (PFS 1-18)|libre|
|[xKfj6ctW8fKEfrxp.htm](pfs-season-1-bestiary/xKfj6ctW8fKEfrxp.htm)|Deadfall Trap (3-4)|Assommoir (3-4)|libre|
|[xu1dm0KnEHx5vySR.htm](pfs-season-1-bestiary/xu1dm0KnEHx5vySR.htm)|Elite Glass River Midge|Moucheron de la Rivière de verre élite|libre|
|[XVoGQC0ret7XiGZD.htm](pfs-season-1-bestiary/XVoGQC0ret7XiGZD.htm)|Dehydrated Krooth|Krooth déshydraté|libre|
|[xwLdaMLETLv6XSlK.htm](pfs-season-1-bestiary/xwLdaMLETLv6XSlK.htm)|Grasping Limbs (3-4)|Membres agrippants (3-4)|libre|
|[y14EZYMrfejrAuPI.htm](pfs-season-1-bestiary/y14EZYMrfejrAuPI.htm)|Frog Swarm|Nuée de grenouilles|libre|
|[yAZBjwCVDGbdT4Sb.htm](pfs-season-1-bestiary/yAZBjwCVDGbdT4Sb.htm)|Mirrored Lelzeshin (Tier 5-6)|Image miroir de Lelzeshin (Tiers 5-6)|libre|
|[yn7x4Me1YrPuXFZf.htm](pfs-season-1-bestiary/yn7x4Me1YrPuXFZf.htm)|Keff The Lion (1-2)|Keff le lion (1-2)|libre|
|[ySXw00WW2LJoDhZ6.htm](pfs-season-1-bestiary/ySXw00WW2LJoDhZ6.htm)|Crowd Leader|Meneur de foule|libre|
|[YtkTx8318EKs43yn.htm](pfs-season-1-bestiary/YtkTx8318EKs43yn.htm)|Grasping Limbs (5-6)|Membres agrippants (5-6)|libre|
|[YtR25uDQSkKQMoaW.htm](pfs-season-1-bestiary/YtR25uDQSkKQMoaW.htm)|Reast Mycer (1-2)|Reast Mycer (1-2)|libre|
|[z8Bp3EtiJhA3nNrY.htm](pfs-season-1-bestiary/z8Bp3EtiJhA3nNrY.htm)|Flaming Skull Skeletal Champion|Crâne enflammé de champion squelettique|libre|
|[zAOgSgAqMLCrIvTl.htm](pfs-season-1-bestiary/zAOgSgAqMLCrIvTl.htm)|Blue Streak Ambushers (5-6)|Piégeur de la Marque bleue (5-6)|libre|
|[zE9WuJXq3I4uZNNX.htm](pfs-season-1-bestiary/zE9WuJXq3I4uZNNX.htm)|Neidre Fliavazzana (5-6)|Neidre Fliavazzana (5-6)|libre|
|[zgvD5MpjPBJXIian.htm](pfs-season-1-bestiary/zgvD5MpjPBJXIian.htm)|Elite Duergar Taskmaster|Prévot hryngar d'élite|libre|
|[zJhgmaI99nePKSaY.htm](pfs-season-1-bestiary/zJhgmaI99nePKSaY.htm)|Boggard Swampseer (PFS 1-24)|Devin des marais bourbiérin (PFS 1-24)|libre|
|[zM01pTDcCS3F6z2V.htm](pfs-season-1-bestiary/zM01pTDcCS3F6z2V.htm)|Iloise (5-6)|Iloise (5-6)|libre|
|[ZMGXiRuVZ8r7hQVe.htm](pfs-season-1-bestiary/ZMGXiRuVZ8r7hQVe.htm)|Bandit Scout|Éclaireur bandit|libre|
|[ZpowNcW774gt8XjS.htm](pfs-season-1-bestiary/ZpowNcW774gt8XjS.htm)|Nashaxian The Angered|Nashaxian Le colérique|libre|
|[zqZu0W4L5HCUaKw7.htm](pfs-season-1-bestiary/zqZu0W4L5HCUaKw7.htm)|Crowd with Chamberpots|Foule avec des pots de chambre|libre|
|[ZUwhZ8D3TpH2ctzo.htm](pfs-season-1-bestiary/ZUwhZ8D3TpH2ctzo.htm)|Pile of Fireworks (5-6)|Pile de feux d'artifice (5-6)|libre|
|[zveTUg1NtYmTwlFn.htm](pfs-season-1-bestiary/zveTUg1NtYmTwlFn.htm)|Dire Warg|Warg sanguinaire|libre|
|[ZX2dP4rP01qaCJh4.htm](pfs-season-1-bestiary/ZX2dP4rP01qaCJh4.htm)|Protoplasmic Extruder|Extrudeuse protoplasmique|libre|
