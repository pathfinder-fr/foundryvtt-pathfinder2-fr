# État de la traduction (journals/pages-Domains)

 * **libre**: 60
 * **changé**: 1


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[ydbCjJ9PPmRzZhDN.htm](journals/pages-Domains/ydbCjJ9PPmRzZhDN.htm)|Creation Domain|Domaine de la Création|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0GwpYEjCHWyfQvgg.htm](journals/pages-Domains/0GwpYEjCHWyfQvgg.htm)|Knowledge Domain|Domaine du Savoir|libre|
|[0wCEUwABKPdKPj8e.htm](journals/pages-Domains/0wCEUwABKPdKPj8e.htm)|Dreams Domain|Domaine des Rêves|libre|
|[3P0NWwP3s7bIiidH.htm](journals/pages-Domains/3P0NWwP3s7bIiidH.htm)|Time Domain|Domaine du Temps|libre|
|[5MjSsuKOLBoiL8FB.htm](journals/pages-Domains/5MjSsuKOLBoiL8FB.htm)|Freedom Domain|Domaine de la Liberté|libre|
|[5TqEbLR9QT3gJGe3.htm](journals/pages-Domains/5TqEbLR9QT3gJGe3.htm)|Sorrow Domain|Domaine du Chagrin|libre|
|[6bDpXy7pQdGrd2og.htm](journals/pages-Domains/6bDpXy7pQdGrd2og.htm)|Star Domain|Domaine des Étoiles|libre|
|[6qTjtFWaBO5b60zJ.htm](journals/pages-Domains/6qTjtFWaBO5b60zJ.htm)|Dust Domain|Domaine de la Poussière|libre|
|[798PFdS8FmefcOl0.htm](journals/pages-Domains/798PFdS8FmefcOl0.htm)|Death Domain|Domaine de la Mort|libre|
|[7xrNAgAnBqBgE3yM.htm](journals/pages-Domains/7xrNAgAnBqBgE3yM.htm)|Change Domain|Domaine du Changement|libre|
|[9g1dNytABTpmmGkG.htm](journals/pages-Domains/9g1dNytABTpmmGkG.htm)|Glyph Domain|Domaine des Glyphes|libre|
|[A7vErdGAweYsFcW8.htm](journals/pages-Domains/A7vErdGAweYsFcW8.htm)|Healing Domain|Domaine de la Guérison|libre|
|[ajCEExOaxuB4C1tY.htm](journals/pages-Domains/ajCEExOaxuB4C1tY.htm)|Passion Domain|Domaine de la Passion|libre|
|[AOQZjqgfafqqtHOB.htm](journals/pages-Domains/AOQZjqgfafqqtHOB.htm)|Destruction Domain|Domaine de la Destruction|libre|
|[bTujFcUut9RX4GCy.htm](journals/pages-Domains/bTujFcUut9RX4GCy.htm)|Travel Domain|Domaine du Voyage|libre|
|[cAxBEZsej32riaY5.htm](journals/pages-Domains/cAxBEZsej32riaY5.htm)|Decay Domain|Domaine de la Décomposition|libre|
|[CbsAiY68e8n5vVVN.htm](journals/pages-Domains/CbsAiY68e8n5vVVN.htm)|Repose Domain|Domaine de la Quiétude|libre|
|[CkBvj5y1lAm1jnsc.htm](journals/pages-Domains/CkBvj5y1lAm1jnsc.htm)|Sun Domain|Domaine du Soleil|libre|
|[CM9ZqWwl7myKn2X1.htm](journals/pages-Domains/CM9ZqWwl7myKn2X1.htm)|Darkness Domain|Domaine des Ténèbres|libre|
|[Czi3XXuNOSE7ISpd.htm](journals/pages-Domains/Czi3XXuNOSE7ISpd.htm)|Perfection Domain|Domaine de la Perfection|libre|
|[DI3MYGIK8iEycanU.htm](journals/pages-Domains/DI3MYGIK8iEycanU.htm)|Zeal Domain|Domaine du Zèle|libre|
|[DS95vr2zmTsjsMhU.htm](journals/pages-Domains/DS95vr2zmTsjsMhU.htm)|Magic Domain|Domaine de la Magie|libre|
|[Dx47K8wpx8KZUa9S.htm](journals/pages-Domains/Dx47K8wpx8KZUa9S.htm)|Protection Domain|Domaine de la Protection|libre|
|[EC2eB0JglDG5j1gT.htm](journals/pages-Domains/EC2eB0JglDG5j1gT.htm)|Fate Domain|Domaine du Destin|libre|
|[egSErNozlL3HRK1y.htm](journals/pages-Domains/egSErNozlL3HRK1y.htm)|Fire Domain|Domaine du Feu|libre|
|[EQfZepZX6rxxBRqG.htm](journals/pages-Domains/EQfZepZX6rxxBRqG.htm)|Toil Domain|Domaine du Labeur|libre|
|[flmxRzGxN2rRNyxZ.htm](journals/pages-Domains/flmxRzGxN2rRNyxZ.htm)|Confidence Domain|Domaine de la Confiance en soi|libre|
|[FtW1gtbHgO0KofPl.htm](journals/pages-Domains/FtW1gtbHgO0KofPl.htm)|Pain Domain|Domaine de la Souffrance|libre|
|[FXOdRGOSdQJNprPx.htm](journals/pages-Domains/FXOdRGOSdQJNprPx.htm)|Metal Domain|Domaine du métal|libre|
|[GiuzDTtkQAgtGW6n.htm](journals/pages-Domains/GiuzDTtkQAgtGW6n.htm)|Indulgence Domain|Domaine des Petits plaisirs|libre|
|[hGoWOjdsUz16oJUm.htm](journals/pages-Domains/hGoWOjdsUz16oJUm.htm)|Plague Domain|Domaine du Fléau|libre|
|[jq9O1tl76g2AzLOh.htm](journals/pages-Domains/jq9O1tl76g2AzLOh.htm)|Cold Domain|Domaine du Froid|libre|
|[Kca7UPuMm44tOo9n.htm](journals/pages-Domains/Kca7UPuMm44tOo9n.htm)|Lightning Domain|Domaine de la Foudre|libre|
|[L11XsA5G89xVKlDw.htm](journals/pages-Domains/L11XsA5G89xVKlDw.htm)|Luck Domain|Domaine de la Chance|libre|
|[lgsJz7mZ1OTe340e.htm](journals/pages-Domains/lgsJz7mZ1OTe340e.htm)|Truth Domain|Domaine de la Vérité|libre|
|[mJBp4KIszuqrmnp5.htm](journals/pages-Domains/mJBp4KIszuqrmnp5.htm)|Wealth Domain|Domaine de la Richesse|libre|
|[MOVMHZU1SfkhNN1K.htm](journals/pages-Domains/MOVMHZU1SfkhNN1K.htm)|Might Domain|Domaine de la Puissance|libre|
|[nuywscaiVGXLQpZ1.htm](journals/pages-Domains/nuywscaiVGXLQpZ1.htm)|Dragon Domain|Domaine de la Parenté dracosire|libre|
|[qjnUXickBOBDBu2N.htm](journals/pages-Domains/qjnUXickBOBDBu2N.htm)|Introspection Domain|Domaine de l'introspection|libre|
|[qMS6QepvY7UQQjcr.htm](journals/pages-Domains/qMS6QepvY7UQQjcr.htm)|Abomination Domain|Domaine de l'abomination|libre|
|[QSk78hQR3zskMlq2.htm](journals/pages-Domains/QSk78hQR3zskMlq2.htm)|Cities Domain|Domaine des Villes|libre|
|[QzsUe3Rt3SifTQvb.htm](journals/pages-Domains/QzsUe3Rt3SifTQvb.htm)|Naga Domain|Domaine Naga|libre|
|[R20JXF43vU5RQyUj.htm](journals/pages-Domains/R20JXF43vU5RQyUj.htm)|Nightmares Domain|Domaine des Cauchemars|libre|
|[rd0jQwvTK4jpv95o.htm](journals/pages-Domains/rd0jQwvTK4jpv95o.htm)|Swarm Domain|Domaine des Nuées|libre|
|[RIlgBuWGfHC1rzYu.htm](journals/pages-Domains/RIlgBuWGfHC1rzYu.htm)|Undeath Domain|Domaine de la Non-mort|libre|
|[rtobUemb6vF2Yu3Y.htm](journals/pages-Domains/rtobUemb6vF2Yu3Y.htm)|Soul Domain|Domaine de l'Âme|libre|
|[S1gyomjojgtCdxc3.htm](journals/pages-Domains/S1gyomjojgtCdxc3.htm)|Secrecy Domain|Domaine de la Confidentialité|libre|
|[SAnmegCTIqGW9S7S.htm](journals/pages-Domains/SAnmegCTIqGW9S7S.htm)|Family Domain|Domaine de la Famille|libre|
|[StXN6IHR6evRaeXF.htm](journals/pages-Domains/StXN6IHR6evRaeXF.htm)|Vigil Domain|Domaine de la Veille|libre|
|[T0JHj79aGphlZ4Mt.htm](journals/pages-Domains/T0JHj79aGphlZ4Mt.htm)|Tyranny Domain|Domaine de la Tyrannie|libre|
|[T2y0vuYibZCL7CH0.htm](journals/pages-Domains/T2y0vuYibZCL7CH0.htm)|Air Domain|Domaine de l'Air|libre|
|[tuThzOCvMLbRVba8.htm](journals/pages-Domains/tuThzOCvMLbRVba8.htm)|Disorientation Domain|Domaine du Délirium|libre|
|[U8WVR6EDfmUaMCbu.htm](journals/pages-Domains/U8WVR6EDfmUaMCbu.htm)|Water Domain|Domaine de l'Eau|libre|
|[uGQKjk2w4whzomky.htm](journals/pages-Domains/uGQKjk2w4whzomky.htm)|Duty Domain|Domaine du Devoir|libre|
|[wBhgIgt47v9uspp3.htm](journals/pages-Domains/wBhgIgt47v9uspp3.htm)|Nature Domain|Domaine de la Nature|libre|
|[WEchjDynqZNfik35.htm](journals/pages-Domains/WEchjDynqZNfik35.htm)|Wood Domain|Domaine du bois|libre|
|[xJtbGqoz3BcCjUik.htm](journals/pages-Domains/xJtbGqoz3BcCjUik.htm)|Trickery Domain|Domaine de la Tromperie|libre|
|[xLxrtbsj4acqgsyC.htm](journals/pages-Domains/xLxrtbsj4acqgsyC.htm)|Nothingness Domain|Domaine de l'anéantissement|libre|
|[Y3DFBCWiM9GBIlfl.htm](journals/pages-Domains/Y3DFBCWiM9GBIlfl.htm)|Moon Domain|Domaine de la Lune|libre|
|[yaMJsfYZmWJLqbFE.htm](journals/pages-Domains/yaMJsfYZmWJLqbFE.htm)|Ambition Domain|Domaine de l'Ambition|libre|
|[zkiLWWYzzqoxmN2J.htm](journals/pages-Domains/zkiLWWYzzqoxmN2J.htm)|Earth Domain|Domaine de la Terre|libre|
