# État de la traduction (journals/pages-Ancestries)

 * **libre**: 14
 * **changé**: 38


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[26YL1jOPQQS6k7A9.htm](journals/pages-Ancestries/26YL1jOPQQS6k7A9.htm)|Kitsune|Kitsune|changé|
|[3cfdiId1IoxLsF9V.htm](journals/pages-Ancestries/3cfdiId1IoxLsF9V.htm)|Elf|Elfe|changé|
|[40koksC576nYV2Tf.htm](journals/pages-Ancestries/40koksC576nYV2Tf.htm)|Vanara|Vanara|changé|
|[5S1Pq3GoEzLfBbH7.htm](journals/pages-Ancestries/5S1Pq3GoEzLfBbH7.htm)|Catfolk|Homme-félin ou félide|changé|
|[7pU8yM7yPMw92SY3.htm](journals/pages-Ancestries/7pU8yM7yPMw92SY3.htm)|Human|Humain|changé|
|[9dpHTBpL3j8ZpqTS.htm](journals/pages-Ancestries/9dpHTBpL3j8ZpqTS.htm)|Anadi|Anadi|changé|
|[cbLJs37Nc8FveriK.htm](journals/pages-Ancestries/cbLJs37Nc8FveriK.htm)|Lizardfolk|Homme-lézard|changé|
|[Ck8L9livQph1GFal.htm](journals/pages-Ancestries/Ck8L9livQph1GFal.htm)|Poppet|Poupée|changé|
|[DuWzeY4zugKFFthl.htm](journals/pages-Ancestries/DuWzeY4zugKFFthl.htm)|Hobgoblin|Hobgobelin|changé|
|[dyusFHHyaQOaQhZ0.htm](journals/pages-Ancestries/dyusFHHyaQOaQhZ0.htm)|Android|Androïde|changé|
|[EcNSrvwIj22Jxkjz.htm](journals/pages-Ancestries/EcNSrvwIj22Jxkjz.htm)|Skeleton|Squelette|changé|
|[FhIJtKcINEe7fipX.htm](journals/pages-Ancestries/FhIJtKcINEe7fipX.htm)|Tripkee|Tripkee|changé|
|[GCxhDyf1ZNlLiDKZ.htm](journals/pages-Ancestries/GCxhDyf1ZNlLiDKZ.htm)|Orc|Orc|changé|
|[Gn0XGCWDb0N4zLE0.htm](journals/pages-Ancestries/Gn0XGCWDb0N4zLE0.htm)|Goblin|Gobelin|changé|
|[I1uEGrIhe4zLs8Ei.htm](journals/pages-Ancestries/I1uEGrIhe4zLs8Ei.htm)|Tanuki|Tanuki|changé|
|[IJOH7nk0w1IybAhp.htm](journals/pages-Ancestries/IJOH7nk0w1IybAhp.htm)|Kashrishi|Kashrishi|changé|
|[j2hOTtHcdu2SA8Ha.htm](journals/pages-Ancestries/j2hOTtHcdu2SA8Ha.htm)|Shoony|Shoony|changé|
|[jKglnHBFvzI80tIe.htm](journals/pages-Ancestries/jKglnHBFvzI80tIe.htm)|Goloma|Goloma|changé|
|[jX3VHEFgpqqUy0aP.htm](journals/pages-Ancestries/jX3VHEFgpqqUy0aP.htm)|Awakened Animal|Animal éveillé|changé|
|[kRjkrWAqQNXj3LLP.htm](journals/pages-Ancestries/kRjkrWAqQNXj3LLP.htm)|Ghoran|Ghoran|changé|
|[lLXEwzxawJ3Q0tpg.htm](journals/pages-Ancestries/lLXEwzxawJ3Q0tpg.htm)|Wayang|Wayang|changé|
|[N8aShsDuf3ur0vZ2.htm](journals/pages-Ancestries/N8aShsDuf3ur0vZ2.htm)|Strix|Strix|changé|
|[naQrTTE7CXu5lnNt.htm](journals/pages-Ancestries/naQrTTE7CXu5lnNt.htm)|Kobold|Kobold|changé|
|[qsbc1ZLCWzfl1sfZ.htm](journals/pages-Ancestries/qsbc1ZLCWzfl1sfZ.htm)|Shisk|Shisk|changé|
|[RlMgeku1FNJjF7YR.htm](journals/pages-Ancestries/RlMgeku1FNJjF7YR.htm)|Fetchling|Fetchelin|changé|
|[rsx8cRI20oEDgfQh.htm](journals/pages-Ancestries/rsx8cRI20oEDgfQh.htm)|Automaton|Automate|changé|
|[rvbMGyBr6y5K7HDP.htm](journals/pages-Ancestries/rvbMGyBr6y5K7HDP.htm)|Sprite|Sprite|changé|
|[SNhp04cirQnnBTbb.htm](journals/pages-Ancestries/SNhp04cirQnnBTbb.htm)|Azarketi|Azarketi|changé|
|[SWkwNivrfzWZqas9.htm](journals/pages-Ancestries/SWkwNivrfzWZqas9.htm)|Leshy|Léchi|changé|
|[TlTIwW61zK0dk95v.htm](journals/pages-Ancestries/TlTIwW61zK0dk95v.htm)|Nagaji|Nagaji|changé|
|[u6IGUd8Gtarj6h7K.htm](journals/pages-Ancestries/u6IGUd8Gtarj6h7K.htm)|Versatile Heritages|Héritages polyvalents|changé|
|[ufWdokSN5W3Einus.htm](journals/pages-Ancestries/ufWdokSN5W3Einus.htm)|Conrasu|Conrasu|changé|
|[voI7uPS9vsG74JIn.htm](journals/pages-Ancestries/voI7uPS9vsG74JIn.htm)|Halfling|Halfelin|changé|
|[vTB1bTYVE4onyyy1.htm](journals/pages-Ancestries/vTB1bTYVE4onyyy1.htm)|Fleshwarp|Distordu|changé|
|[xBV8oGvecw7abZxc.htm](journals/pages-Ancestries/xBV8oGvecw7abZxc.htm)|Kholo|Kholo|changé|
|[XzRv30zY3UXRbBho.htm](journals/pages-Ancestries/XzRv30zY3UXRbBho.htm)|Dwarf|Nain|changé|
|[YGIwmuVOJI7uNhGM.htm](journals/pages-Ancestries/YGIwmuVOJI7uNhGM.htm)|Tengu|Tengu|changé|
|[zoSkNev57C8OEDYL.htm](journals/pages-Ancestries/zoSkNev57C8OEDYL.htm)|Ratfolk|Homme-rat|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[19pE89qVIMELrZoY.htm](journals/pages-Ancestries/19pE89qVIMELrZoY.htm)|Athamaru|Athamaru|libre|
|[1lCAkGgIYYkczZ28.htm](journals/pages-Ancestries/1lCAkGgIYYkczZ28.htm)|Vishkanya|Vishkanya|libre|
|[6kuNg9Pr7LdaNPQq.htm](journals/pages-Ancestries/6kuNg9Pr7LdaNPQq.htm)|Surki|Surki|libre|
|[9hcAMLgnCAKkuyH6.htm](journals/pages-Ancestries/9hcAMLgnCAKkuyH6.htm)|Merfolk|Homme-poisson|libre|
|[AhKqWjsodDXRUelE.htm](journals/pages-Ancestries/AhKqWjsodDXRUelE.htm)|Centaur|Centaure|libre|
|[CDjWwC19sqUpK7AI.htm](journals/pages-Ancestries/CDjWwC19sqUpK7AI.htm)|Rare|Rare|libre|
|[D1SZXGFA8Hh4huax.htm](journals/pages-Ancestries/D1SZXGFA8Hh4huax.htm)|Minotaur|Minotaure|libre|
|[d91CO5iXPmcVLoPY.htm](journals/pages-Ancestries/d91CO5iXPmcVLoPY.htm)|Uncommon|Peu courant|libre|
|[f6IU2EGcJUzVZ4nD.htm](journals/pages-Ancestries/f6IU2EGcJUzVZ4nD.htm)|Yaksha|Yaksha|libre|
|[fdJiL2YSVVjXf6cQ.htm](journals/pages-Ancestries/fdJiL2YSVVjXf6cQ.htm)|Common|Courant|libre|
|[OlqTCYZK9sBDeuKf.htm](journals/pages-Ancestries/OlqTCYZK9sBDeuKf.htm)|Yaoguai|Yaoguai|libre|
|[oTRP3yJNZp5KRoHK.htm](journals/pages-Ancestries/oTRP3yJNZp5KRoHK.htm)|Sarangay|sarangay|libre|
|[pwrptby8NB54A6HQ.htm](journals/pages-Ancestries/pwrptby8NB54A6HQ.htm)|Samsaran|Samsaran|libre|
|[qIu10HByKeRaPPvD.htm](journals/pages-Ancestries/qIu10HByKeRaPPvD.htm)|Gnome|Gnome|libre|
