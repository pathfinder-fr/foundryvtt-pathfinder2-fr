# État de la traduction (journals/pages-Classes)

 * **changé**: 21
 * **libre**: 4


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[88K0xjShkhkckbhv.htm](journals/pages-Classes/88K0xjShkhkckbhv.htm)|Gunslinger|Franc-tireur|changé|
|[fYJruhQfzs4dj0mp.htm](journals/pages-Classes/fYJruhQfzs4dj0mp.htm)|Witch|Sorcier|changé|
|[gA4Ud8oSUGkkLwAi.htm](journals/pages-Classes/gA4Ud8oSUGkkLwAi.htm)|Summoner|Conjurateur|changé|
|[gcRmL4Id1ggKDhvg.htm](journals/pages-Classes/gcRmL4Id1ggKDhvg.htm)|Inventor|Inventeur|changé|
|[gdEmPPRYhRai7I1N.htm](journals/pages-Classes/gdEmPPRYhRai7I1N.htm)|Ranger|Rôdeur|changé|
|[hGKd722Kn4ZxIOJs.htm](journals/pages-Classes/hGKd722Kn4ZxIOJs.htm)|Monk|Moine|changé|
|[HZbrRRVzW7w17L2W.htm](journals/pages-Classes/HZbrRRVzW7w17L2W.htm)|Fighter|Guerrier|changé|
|[ixBx2wZpnf4qUoEv.htm](journals/pages-Classes/ixBx2wZpnf4qUoEv.htm)|Sorcerer|Ensorceleur|changé|
|[KVoEUJBAbAFUJTqw.htm](journals/pages-Classes/KVoEUJBAbAFUJTqw.htm)|Psychic|Psychiste|changé|
|[lNWQPwgT4vhYCOT7.htm](journals/pages-Classes/lNWQPwgT4vhYCOT7.htm)|Kineticist|Kinétiste ou Cinétiste|changé|
|[lvuutBI9BSCU6cWe.htm](journals/pages-Classes/lvuutBI9BSCU6cWe.htm)|Barbarian|Barbare|changé|
|[N4ABcd6CcCbqmw3x.htm](journals/pages-Classes/N4ABcd6CcCbqmw3x.htm)|Cleric|Prêtre|changé|
|[Om4e4tsgNWrComie.htm](journals/pages-Classes/Om4e4tsgNWrComie.htm)|Alchemist|Alchimiste|changé|
|[omDi7otL3UWLVgct.htm](journals/pages-Classes/omDi7otL3UWLVgct.htm)|Investigator|Enquêteur|changé|
|[qigU4oNH2KDVvhjX.htm](journals/pages-Classes/qigU4oNH2KDVvhjX.htm)|Thaumaturge|Thaumaturge|changé|
|[rebppVgBVi8J6TT2.htm](journals/pages-Classes/rebppVgBVi8J6TT2.htm)|Champion|Champion|changé|
|[rsDTKu4HUuKft7fk.htm](journals/pages-Classes/rsDTKu4HUuKft7fk.htm)|Swashbuckler|Bretteur|changé|
|[Tz0NWVqhZyt8EyUV.htm](journals/pages-Classes/Tz0NWVqhZyt8EyUV.htm)|Bard|Barde|changé|
|[u4qPrDRBagxG9wj8.htm](journals/pages-Classes/u4qPrDRBagxG9wj8.htm)|Rogue|Roublard|changé|
|[wDukeO3euLEGn6FA.htm](journals/pages-Classes/wDukeO3euLEGn6FA.htm)|Wizard|Magicien|changé|
|[YVoyzV9v4QyP2UIC.htm](journals/pages-Classes/YVoyzV9v4QyP2UIC.htm)|Magus|Magus|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[8ElntNAGahQka70r.htm](journals/pages-Classes/8ElntNAGahQka70r.htm)|Druid|Druide|libre|
|[bqaqOx3naiwTozBX.htm](journals/pages-Classes/bqaqOx3naiwTozBX.htm)|Oracle|Oracle|libre|
|[CIQggvnSfmuLa7eH.htm](journals/pages-Classes/CIQggvnSfmuLa7eH.htm)|Exemplar|Exalté|libre|
|[ThFPVuxGiZ2Asgyr.htm](journals/pages-Classes/ThFPVuxGiZ2Asgyr.htm)|Animist|Animiste|libre|
