Name: Herald's Ring
Nom: Anneau du héraut
État: libre

-- Desc (en) --
<p>Adorned with golden herald horns, this green ring both enhances a performer's vocal projection and provides partial protection against sonic attacks.</p><hr /><p><strong>Activate—Project Voice</strong> <span class="action-glyph">1</span> (manipulate)</p>
<p><strong>Frequency</strong> Once per day</p>
<p><strong>Effect</strong> When you turn the <em>herald's ring</em> around your finger three times in either direction, you gain the ability to easily project your voice up to 200 feet without raising the volume of speech, reaching your audience in most venues, including arenas. This effect lasts up to 2 hours.</p><hr /><p><strong>Activate—Reflect Sound</strong> <span class="action-glyph">r</span> (concentrate)</p>
<p><strong>Frequency</strong> once per day</p>
<p><strong>Trigger</strong> You take sonic damage from a spell or effect</p>
<p><strong>Effect</strong> You use the <em>herald's ring</em> to reflect a portion of the sonic damage back at its source by attempting to counteract the effect. The ring has a counteract modifier of [[/r 1d20+23 #Counteract]]{+23}.</p>
<p><strong>Critical Success</strong> All of the damage is reflected back on its source.</p>
<p><strong>Success</strong> Half of the damage is reflected back at its source, and the other half is negated.</p>
<p><strong>Failure</strong> Half of the damage is reflected back at the source, and you take the rest of the damage.</p>
<p><strong>Critical Failure</strong> None of the damage is reflected, but the herald's ring does absorb enough energy so that you can activate it again today.</p>
-- Desc (fr) --
<p>Orné de trompettes de héraut dorées, cet anneau vert permet à la fois d'améliorer la projection vocale d'un artiste et d'offrir une protection partielle contre les attaques soniques.</p>
<hr />
<p><strong>Activation - Projection de la voix</strong> <span class="action-glyph">1</span> (manipulation)</p>
<hr />
<p><strong>Fréquence</strong> Une fois par jour</p>
<p><strong>Effet</strong> Lorsque vous retourner l'<em>anneau du héraut</em> autour de votre doigt trois fois dans chaque direction, vous obtenez la capacité de projeter aisément votre voix jusqu'à 60 mètres sans augmenter le volume de votre discours, atteigant le public dans la plupart des enceintes, y compris des arènes. Cet effet dure jusqu'à 2 heures.</p>
<hr />
<p><strong>Activation - Réfléchir le son</strong> <span class="action-glyph">r</span> (concentration)</p>
<hr />
<p><strong>Fréquence</strong> Une fois par jour</p>
<p><strong>Déclencheur</strong> Vous subissez des dégâts oniques d'un sort ou d'un effet</p>
<p><strong>Effet</strong> Vous utilisez l'<em>anneau du héraut</em> pour refléter une partie des dégâts de son à leur source en tentant de contrer l'effet. L'anneau possède un modificateur de contre de [[/r 1d20+23 #contre]]{+23}.</p>
<hr />
<p><strong>Succès critique</strong> Tous les dégâts sont renvoyés à leur source.</p>
<p><strong>Succès</strong> La moitié des dégâts est renvoyée à sa source et l'autre moitié est annulée.</p>
<p><strong>Échec</strong> La moitié des dégâts est renvoyé à sa source et vous subissez le reste des dégâts.</p>
<p><strong>Échec critique</strong> Aucun des dégâts n'est renvoyé, mais l'<em>anneau du héraut</em> absorbe suffisamment d'énergie pour pouvoir l'activer de nouveau le même jour.</p>
-- End desc ---
