# État de la traduction (campaign-effects)

 * **libre**: 52
 * **changé**: 1
 * **aucune**: 6


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[Rzlo3kvFUavFQJ3S.htm](campaign-effects/Rzlo3kvFUavFQJ3S.htm)|Effect: Spirit Powers|
|[Up4O78PvSYAlXxDA.htm](campaign-effects/Up4O78PvSYAlXxDA.htm)|Effect: Consecrated Altar to Ydersius|
|[UQmizjvdBfSVYoZe.htm](campaign-effects/UQmizjvdBfSVYoZe.htm)|Effect: Spirit Power (Flight)|
|[XmNIl5QrNXy88Fk8.htm](campaign-effects/XmNIl5QrNXy88Fk8.htm)|Effect: Technological Defence|
|[ywrtR63bJqpN0FsK.htm](campaign-effects/ywrtR63bJqpN0FsK.htm)|Effect: Righteous Call|
|[zcGenQGOjZgFuqzs.htm](campaign-effects/zcGenQGOjZgFuqzs.htm)|Effect: Tarnbreaker Champions|

## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[84OsM8ml9VUAlNze.htm](campaign-effects/84OsM8ml9VUAlNze.htm)|Effect: Rugged Mentor|Effet : Mentor Robuste (PFS)|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0PDng7D5Aj0szeSU.htm](campaign-effects/0PDng7D5Aj0szeSU.htm)|Effect: Combat Mentor|Effet : Mentor de Combat (PFS)|libre|
|[1iIObOrXUGA3RwQ9.htm](campaign-effects/1iIObOrXUGA3RwQ9.htm)|Effect: Bickering Family|Effet : Chamailleries familiales|libre|
|[3BZisQv0wVLGEvzp.htm](campaign-effects/3BZisQv0wVLGEvzp.htm)|Aura: Righteous Call|Aura : Appel à la vertu|libre|
|[4vnF6BFM4Xg4Eg0k.htm](campaign-effects/4vnF6BFM4Xg4Eg0k.htm)|Effect: Improved reflexes|Effet : Réflexes améliorés|libre|
|[53nP5ljwRhD9BlUk.htm](campaign-effects/53nP5ljwRhD9BlUk.htm)|Effect: Heroic Inspiration|Effet : Inspiration Héroïque (PFS)|libre|
|[7pTfh1NwnXhEX7Lq.htm](campaign-effects/7pTfh1NwnXhEX7Lq.htm)|Effect: Azata's Grace|Effet : Grace de l'azata|libre|
|[7Rpu9g6GBxg3jmoG.htm](campaign-effects/7Rpu9g6GBxg3jmoG.htm)|Resonant Reflection: Reflection of Water|Reflet retentissant : Reflet d'eau|libre|
|[8KtjzHHR7n7ALvQc.htm](campaign-effects/8KtjzHHR7n7ALvQc.htm)|Effect: Seasonal Boon (Outskirt Dweller)|Effet : Récompense saisonnière (Habitant de la périphérie)|libre|
|[a8LGc1NEXXBTvvy3.htm](campaign-effects/a8LGc1NEXXBTvvy3.htm)|Effect: Heroic Aegis|Effet : Égide héroïque|libre|
|[axtAzam8kiKbTD2c.htm](campaign-effects/axtAzam8kiKbTD2c.htm)|Malevolence|Malveillance|libre|
|[b1sZC0knPcr7B6Gz.htm](campaign-effects/b1sZC0knPcr7B6Gz.htm)|Effect: Magical Mentor|Effet : Mentor Magique (PFS)|libre|
|[cESIlNrgjo1uW71D.htm](campaign-effects/cESIlNrgjo1uW71D.htm)|Mixed Drink: Guidance|Cocktail : Assistance surnaturelle|libre|
|[cpeMnTLFpi106fjg.htm](campaign-effects/cpeMnTLFpi106fjg.htm)|Effect: Chthonic Mucus (1-2)|Effet : Mucus chtonien (1-2)|libre|
|[CS1XbH7GYfP6Ve61.htm](campaign-effects/CS1XbH7GYfP6Ve61.htm)|Effect: Burned Mouth and Throat (No Singing or Yelling)|Effet : Gorge et langue brulées (Chanter ou Crier)|libre|
|[DOxl0FDd21VMDOMP.htm](campaign-effects/DOxl0FDd21VMDOMP.htm)|Effect: Reflection of Life (Fast Healing)|Effet : Reflet de vie (Guérison accélérée)|libre|
|[dZT1w2EciqaR2NRz.htm](campaign-effects/dZT1w2EciqaR2NRz.htm)|Effect: Crownhold Consecration|Effet : Consécration Crownhold|libre|
|[e8K0YTKxzf0bhayh.htm](campaign-effects/e8K0YTKxzf0bhayh.htm)|Mixed Drink: Luck|Cocktail : Chance|libre|
|[ECCyLvt5uRw8Qv8V.htm](campaign-effects/ECCyLvt5uRw8Qv8V.htm)|Effect: Spirit Power (Passion)|Effet : Pouvoir Spirituel (Passion)|libre|
|[eckkKdNGlaPlAgOW.htm](campaign-effects/eckkKdNGlaPlAgOW.htm)|Resonant Reflection: Reflection of Life|Reflet retentissant : Reflet de la vie|libre|
|[EdYvHSzJC5pGLdoV.htm](campaign-effects/EdYvHSzJC5pGLdoV.htm)|Effect: Resist Corruption|Effet : Resister à la corruption (PFS)|libre|
|[fOz4CDZmc6CSKAUm.htm](campaign-effects/fOz4CDZmc6CSKAUm.htm)|Resonant Reflection: Reflection of Storm|Reflet retentissant : Reflet de la tempête|libre|
|[GBaN8XZrzFp7C7fL.htm](campaign-effects/GBaN8XZrzFp7C7fL.htm)|Effect: Little Ripple's Blessing|Effet : Bénédiction de Petit Ripple|libre|
|[i5PHb9yR33Cr6XMK.htm](campaign-effects/i5PHb9yR33Cr6XMK.htm)|Effect: Heroic Hustle|Effet : Empressement héroïque (PFS)|libre|
|[ICT4vGhOmPbriy16.htm](campaign-effects/ICT4vGhOmPbriy16.htm)|Effect: Worldly Mentor|Effet : Mentor expérimenté (PFS)|libre|
|[IlSPZvli2IcLVPyi.htm](campaign-effects/IlSPZvli2IcLVPyi.htm)|Effect: Protective Mentor|Effet : Mentor protecteur (PFS)|libre|
|[IRmIB2435SCvoSTS.htm](campaign-effects/IRmIB2435SCvoSTS.htm)|Effect: Stoop|Effet : Courbure|libre|
|[j2xuFnWfTov0VQoG.htm](campaign-effects/j2xuFnWfTov0VQoG.htm)|Effect: Light in the Dark|Effet : Lumière dans les ténèbres(PFS)|libre|
|[j8hA7CsmSY90tBqw.htm](campaign-effects/j8hA7CsmSY90tBqw.htm)|Mixed Drink: Prestidigitation|Cocktail : Prestidigitation|libre|
|[JnQi8whNqY7sPW7x.htm](campaign-effects/JnQi8whNqY7sPW7x.htm)|Mixed Drink: Dancing Lights|Cocktail : Lumière|libre|
|[L2gnvfJ5y2cTNryW.htm](campaign-effects/L2gnvfJ5y2cTNryW.htm)|Effect: Grand Finale|Effet : Grand Final (PFS)|libre|
|[LJOZk2O6KS3iulXy.htm](campaign-effects/LJOZk2O6KS3iulXy.htm)|Effect: Boar Form|Effet : Forme de sanglier|libre|
|[Luti1qjFOKYwZio0.htm](campaign-effects/Luti1qjFOKYwZio0.htm)|Effect: Extreme stomach cramps|Effet : crampes d'estomac intenses|libre|
|[lZ4DA81o4kbL8nve.htm](campaign-effects/lZ4DA81o4kbL8nve.htm)|Effect: Burned Tongue (Linguistic)|Effet : Langue brûlée (Linguistique)|libre|
|[meTIFa2VsIzRVywE.htm](campaign-effects/meTIFa2VsIzRVywE.htm)|Effect: Hope or Despair (Critical Success)|Effet : Espoir ou Désespoir (Succès critique)|libre|
|[MH3VWOysJ8XboONX.htm](campaign-effects/MH3VWOysJ8XboONX.htm)|Effect: Wind Barrier|Effet : Barrière de vent|libre|
|[mHsp4jGurcemml59.htm](campaign-effects/mHsp4jGurcemml59.htm)|Persona Trait|Trait de personnage|libre|
|[mL13OZS2t1eMRlly.htm](campaign-effects/mL13OZS2t1eMRlly.htm)|Resonant Reflection: Reflection of Stone|Reflet retentissant : Reflet de la pierre|libre|
|[MXATTT52FQc8MTZv.htm](campaign-effects/MXATTT52FQc8MTZv.htm)|Resonant Reflection: Reflection of Light|Reflet retentissant : Reflet de la lumière|libre|
|[n6PAwAqignNXd75G.htm](campaign-effects/n6PAwAqignNXd75G.htm)|Effect: Skillful Mentor|Effet : Mentor talentueux (PFS)|libre|
|[nwFhhgZRggDoDgdk.htm](campaign-effects/nwFhhgZRggDoDgdk.htm)|Effect: Burned Mouth and Throat (Linguistic)|Effet : Langue et gorge brûlées (linguistique)|libre|
|[OJibRFm4JCVGQZnx.htm](campaign-effects/OJibRFm4JCVGQZnx.htm)|Effect: Practiced Medic|Effet : Secouriste entraîné (PFS)|libre|
|[OwrgpWYQTCLM6Ye7.htm](campaign-effects/OwrgpWYQTCLM6Ye7.htm)|Effect: Exotic Edge|Effet : Pointe d'exotisme (PFS)|libre|
|[PnAzso0F6XUvbYKb.htm](campaign-effects/PnAzso0F6XUvbYKb.htm)|Effect: Tiger Form|Effet : Forme de tigre|libre|
|[Px7sSipQxHdOMSjk.htm](campaign-effects/Px7sSipQxHdOMSjk.htm)|Effect: Hope or Despair (Failure or Critical Failure)|Effet : Espoir ou désespoir (Échec ou Échec critique)|libre|
|[QWW4hNCtLkOK1k71.htm](campaign-effects/QWW4hNCtLkOK1k71.htm)|Effect: Immediate and Intense Headache|Effet : Mal de tête immédiat et intense|libre|
|[rEQP3SwRF9Zb1ppV.htm](campaign-effects/rEQP3SwRF9Zb1ppV.htm)|Effect: Nexian Researcher|Effet : Chercheur nexien (PFS)|libre|
|[Rz35d02dUtAjKtMB.htm](campaign-effects/Rz35d02dUtAjKtMB.htm)|Effect: Heightened Awareness|Effet : Perception augmentée|libre|
|[SvR7Ez1lfnN4You5.htm](campaign-effects/SvR7Ez1lfnN4You5.htm)|Geb's Blessing|Bénédiction de Geb|libre|
|[tWFcPYueuOdSgSgg.htm](campaign-effects/tWFcPYueuOdSgSgg.htm)|Effect: Forgive Foe|Effet : Pardonner l'ennemi|libre|
|[uZ2mAwpVz1bkw5GK.htm](campaign-effects/uZ2mAwpVz1bkw5GK.htm)|Effect: Chthonic Mucus|Effet : Mucus chtonien|libre|
|[XOE1gp3phFyxL4Dq.htm](campaign-effects/XOE1gp3phFyxL4Dq.htm)|Effect: Burned Tongue (No Singing or Yelling)|Effet : Langue brûlée (Pas de chant ou de cri)|libre|
|[yJWWTfZkAF4raa4R.htm](campaign-effects/yJWWTfZkAF4raa4R.htm)|Effect: Keen insight|Effet : Perception affûtée|libre|
