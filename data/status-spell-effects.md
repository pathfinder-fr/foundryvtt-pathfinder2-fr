# État de la traduction (spell-effects)

 * **libre**: 476
 * **officielle**: 9
 * **changé**: 1
 * **aucune**: 3


Dernière mise à jour: 2025-03-05 07:07 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[5Zof6sC46ms4JLIl.htm](spell-effects/5Zof6sC46ms4JLIl.htm)|Effect: Bone Flense (Reaction)|
|[P6f6UnvYSF5zATyu.htm](spell-effects/P6f6UnvYSF5zATyu.htm)|Effect: Bone Flense (Damage)|
|[XstFvzPDKCfmoIED.htm](spell-effects/XstFvzPDKCfmoIED.htm)|Spell Effect: Scintillating Safeguard|

## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[4xtce4xbwZ9wjwMW.htm](spell-effects/4xtce4xbwZ9wjwMW.htm)|Spell Effect: Angelic Halo|Effet : Halo angélique|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[06zdFoxzuTpPPGyJ.htm](spell-effects/06zdFoxzuTpPPGyJ.htm)|Spell Effect: Rejuvenating Flames|Effet : Flammes rajeunissantes|libre|
|[09rtx50laDw68FGT.htm](spell-effects/09rtx50laDw68FGT.htm)|Spell Effect: Earthquake (Shaking Ground)|Effet : Tremblement de terre (Faire trembler le sol)|libre|
|[0B3noZtrBfeHC7ye.htm](spell-effects/0B3noZtrBfeHC7ye.htm)|Spell Effect: Animal Form (Crocodile)|Effet : Forme animale (crocodile)|libre|
|[0bfqYkNaWsdTmtrc.htm](spell-effects/0bfqYkNaWsdTmtrc.htm)|Spell Effect: Juvenile Companion|Effet : Compagnon juvénile|libre|
|[0Cyf07wboRp4CmcQ.htm](spell-effects/0Cyf07wboRp4CmcQ.htm)|Spell Effect: Dinosaur Form (Ankylosaurus)|Effet : Forme de dinosaure (Ankylosaure)|libre|
|[0eP0JRBPwfRyu7gN.htm](spell-effects/0eP0JRBPwfRyu7gN.htm)|Spell Effect: Phoenix Ward|Effet : Protection du phénix|libre|
|[0gv9D5RlrF5cKA3I.htm](spell-effects/0gv9D5RlrF5cKA3I.htm)|Spell Effect: Adapt Self (Darkvision)|Effet : Auto-adaptation (Vision dans le noir)|libre|
|[0JPnBD7yzrmrbZ0m.htm](spell-effects/0JPnBD7yzrmrbZ0m.htm)|Spell Effect: Devil Form (Vordine)|Effet : Forme de diable (Vordine)|libre|
|[0koqUuC1YW4F1l5z.htm](spell-effects/0koqUuC1YW4F1l5z.htm)|Spell Effect: Anima Invocation|Effet : Invocation d'anima|libre|
|[0lYbjDI2N3xVl24E.htm](spell-effects/0lYbjDI2N3xVl24E.htm)|Spell Effect: Glass Form|Effet : Forme de verre|libre|
|[0OC945wcZ4H4akLz.htm](spell-effects/0OC945wcZ4H4akLz.htm)|Spell Effect: Summoner's Visage|Effet : Visage du conjurateur|libre|
|[0PO5mFRhh9HxGAtv.htm](spell-effects/0PO5mFRhh9HxGAtv.htm)|Spell Effect: Mirror Image|Effet : Image miroir|libre|
|[0QVufU5o3xIxiHmP.htm](spell-effects/0QVufU5o3xIxiHmP.htm)|Spell Effect: Aerial Form (Bird)|Effet : Forme aérienne (Oiseau)|libre|
|[0s6YaL3IjqECmjab.htm](spell-effects/0s6YaL3IjqECmjab.htm)|Spell Effect: Roar of the Dragon|Effet : Rugissement du dragon|libre|
|[0TB7pLXBiVPMB08U.htm](spell-effects/0TB7pLXBiVPMB08U.htm)|Spell Effect: Loremaster's Etude|Effet : Étude du maître savant|libre|
|[0W87OkYi3qCwNGSj.htm](spell-effects/0W87OkYi3qCwNGSj.htm)|Spell Effect: Daemon Form (Piscodaemon)|Effet : Forme de daémon (Piscodaémon)|libre|
|[0WYy3bY7HJ7TDSYD.htm](spell-effects/0WYy3bY7HJ7TDSYD.htm)|Spell Effect: Draw Ire|Effet : Attirer la colère|libre|
|[0yy4t4UY1HqrEo70.htm](spell-effects/0yy4t4UY1HqrEo70.htm)|Spell Effect: Devil Form (Barbazu)|Effet : Forme de diable (Barbazu)|libre|
|[14m4s0FeRSqRlHwL.htm](spell-effects/14m4s0FeRSqRlHwL.htm)|Spell Effect: Arcane Countermeasure|Effet : Contremesure arcanique|libre|
|[16QrxIYal7PJFL2W.htm](spell-effects/16QrxIYal7PJFL2W.htm)|Spell Effect: Euphoric Renewal|Effet : Renouveau euphorique|libre|
|[1cBl1gVcpzOqlluC.htm](spell-effects/1cBl1gVcpzOqlluC.htm)|Spell Effect: Flame Dancer|Effet : Danseur enflammé|libre|
|[1Gax900IAwhLCi4q.htm](spell-effects/1Gax900IAwhLCi4q.htm)|Spell Effect: Animal Form (Crab)|Effet : Forme animale (crabe)|libre|
|[1jX8W0eyMPJ0MFKY.htm](spell-effects/1jX8W0eyMPJ0MFKY.htm)|Spell Effect: Malediction|Effet: Malédiction défensive|libre|
|[1kelGCsoXyGRqMd9.htm](spell-effects/1kelGCsoXyGRqMd9.htm)|Spell Effect: Diabolic Edict|Effet : Édit diabolique|officielle|
|[1n84AqLtsdT8I64W.htm](spell-effects/1n84AqLtsdT8I64W.htm)|Spell Effect: Daemon Form (Ceustodaemon)|Effet : Forme de daémon (Ceustodaémon)|libre|
|[1RsScTvNdGD9zGWe.htm](spell-effects/1RsScTvNdGD9zGWe.htm)|Spell Effect: Fire Shield|Effet : Bouclier de feu|libre|
|[1t64Sf8wYsoo7iFt.htm](spell-effects/1t64Sf8wYsoo7iFt.htm)|Spell Effect: Devil Form (Coarti)|Effet : Forme de diable (Coarti)|libre|
|[1W1LZ3qrZLXr2Mn7.htm](spell-effects/1W1LZ3qrZLXr2Mn7.htm)|Spell Effect: Musical Shift|Effet : Décalage musical|libre|
|[1XlF12UbvJsTZxfp.htm](spell-effects/1XlF12UbvJsTZxfp.htm)|Spell Effect: Evolution Surge|Effet : Flux d'évolution|libre|
|[20egTKICPMhibqgn.htm](spell-effects/20egTKICPMhibqgn.htm)|Spell Effect: Demon Form (Vrock)|Effet : Forme de démon (Vrock)|libre|
|[28NvrpZmELvyrHUt.htm](spell-effects/28NvrpZmELvyrHUt.htm)|Spell Effect: Variable Gravity (High Gravity)|Effet : Gravité variable (Gravité élevée)|libre|
|[2Hg3a06gZCSnUgSA.htm](spell-effects/2Hg3a06gZCSnUgSA.htm)|Spell Effect: Figment|Effet : Fantasme|libre|
|[2KQSsrzUqAxSXOdd.htm](spell-effects/2KQSsrzUqAxSXOdd.htm)|Spell Effect: Dancing Shield|Effet : Bouclier dansant|libre|
|[2sMXAGZfdqiy10kk.htm](spell-effects/2sMXAGZfdqiy10kk.htm)|Spell Effect: Clinging Ice|Effet : Glace tenace|libre|
|[2Ss5VblfZNHg1HjN.htm](spell-effects/2Ss5VblfZNHg1HjN.htm)|Spell Effect: Cosmic Form (Sun)|Effet : Forme cosmique (Soleil)|libre|
|[2SWUzp4JuNK5EX0J.htm](spell-effects/2SWUzp4JuNK5EX0J.htm)|Spell Effect: Adapt Self (Swim)|Effet : Auto-adaptation (Nage)|libre|
|[2W4qAipPVlapaWbc.htm](spell-effects/2W4qAipPVlapaWbc.htm)|Spell Effect: Silver's Refrain|Effet : Refrain d'argent|libre|
|[34yP4e2e6jpY1Bsi.htm](spell-effects/34yP4e2e6jpY1Bsi.htm)|Spell Effect: Mantle of the Magma Heart|Effet : Manteau du coeur magmatique|libre|
|[3bcUiWwc5KLc4X0T.htm](spell-effects/3bcUiWwc5KLc4X0T.htm)|Aura: Protector's Sphere|Aura : Sphère du protecteur|libre|
|[3hDKcbhn0j6DsRgm.htm](spell-effects/3hDKcbhn0j6DsRgm.htm)|Spell Effect: Touch of the Moon|Effet : Contact lunaire|libre|
|[3HEiYVhqypfc4IsP.htm](spell-effects/3HEiYVhqypfc4IsP.htm)|Spell Effect: Safeguard Secret|Effet : Secret bien gardé|officielle|
|[3Ktyd5F9lOPo4myk.htm](spell-effects/3Ktyd5F9lOPo4myk.htm)|Spell Effect: Illusory Disguise|Effet : Déguisement illusoire|libre|
|[3LyOkV25p7wA181H.htm](spell-effects/3LyOkV25p7wA181H.htm)|Effect: Guidance Immunity|Effet : Immunité à Assistance surnaturelle|officielle|
|[3qHKBDF7lrHw8jFK.htm](spell-effects/3qHKBDF7lrHw8jFK.htm)|Spell Effect: Guidance|Effet : Assistance surnaturelle|officielle|
|[3TRArzn1UdcWHkja.htm](spell-effects/3TRArzn1UdcWHkja.htm)|Spell Effect: Revel in Retribution|Effet : Se délecter du châtiment|libre|
|[3vWfew0TIrcGRjLZ.htm](spell-effects/3vWfew0TIrcGRjLZ.htm)|Spell Effect: Angel Form (Monadic Deva)|Effet : Forme d'ange (Deva monadique)|libre|
|[3zdBGENpmaze1bpq.htm](spell-effects/3zdBGENpmaze1bpq.htm)|Spell Effect: Ooze Form (Gelatinous Cube)|Effet : Forme de vase (Cube gélatineux)|libre|
|[41WThj17MZBXTO2X.htm](spell-effects/41WThj17MZBXTO2X.htm)|Spell Effect: Enlarge (Heightened 4th)|Effet : Agrandissement (Intensifié 4e)|libre|
|[46vCC77mBNBWtmx3.htm](spell-effects/46vCC77mBNBWtmx3.htm)|Spell Effect: Litany of Righteousness|Effet : Litanie de vertu|libre|
|[4ag0OHKfjROmR4Pm.htm](spell-effects/4ag0OHKfjROmR4Pm.htm)|Spell Effect: Anticipate Peril|Effet : Anticipation du danger|libre|
|[4dnt1P2SfcePzkrF.htm](spell-effects/4dnt1P2SfcePzkrF.htm)|Spell Effect: Incendiary Ashes (Success)|Effet : Cendres incendiaires (Succès)|libre|
|[4FD4vJqVpuuJjk9Q.htm](spell-effects/4FD4vJqVpuuJjk9Q.htm)|Spell Effect: Mind Swap (Critical Success)|Effet : Échange d'esprit (Succès critique)|libre|
|[4iakL7fDcZ8RT6Tu.htm](spell-effects/4iakL7fDcZ8RT6Tu.htm)|Spell Effect: Face in the Crowd|Effet : Fondu dans la foule|libre|
|[4ktNx3cVz5GkcGJa.htm](spell-effects/4ktNx3cVz5GkcGJa.htm)|Spell Effect: Untwisting Iron Augmentation|Effet : Amélioration du fer qui ne plie pas|libre|
|[4Lo2qb5PmavSsLNk.htm](spell-effects/4Lo2qb5PmavSsLNk.htm)|Spell Effect: Energy Aegis|Effet : Égide énergétique|libre|
|[4vlorajqpFcS5Ozi.htm](spell-effects/4vlorajqpFcS5Ozi.htm)|Spell Effect: Flashy Disappearance|Effet : Disparition spectaculaire|libre|
|[4wZaiZJtAA0iyWR5.htm](spell-effects/4wZaiZJtAA0iyWR5.htm)|Spell Effect: Sun's Fury|Effet : Furie du soleil|libre|
|[542Keo6txtq7uvqe.htm](spell-effects/542Keo6txtq7uvqe.htm)|Spell Effect: Dinosaur Form (Tyrannosaurus)|Effet : Forme de dinosaure (Tyrannosaure)|libre|
|[57lnrCzGUcNUBP2O.htm](spell-effects/57lnrCzGUcNUBP2O.htm)|Spell Effect: Athletic Rush|Effet : Athlétisme poussé|libre|
|[5AIV3cCLUt442Mmp.htm](spell-effects/5AIV3cCLUt442Mmp.htm)|Spell Effect: Shields of the Spirit (Security)|Effet : Boucliers de l'esprit (sécurité)|libre|
|[5blVLpIH65hvFJpn.htm](spell-effects/5blVLpIH65hvFJpn.htm)|Spell Effect: Safe Passage|Effet : Passage sûr|libre|
|[5gpsh5UviUnFIRXY.htm](spell-effects/5gpsh5UviUnFIRXY.htm)|Spell Effect: Spectral Advance|Effet : Avancée spectrale|libre|
|[5MI2c9IgxfSeGZQo.htm](spell-effects/5MI2c9IgxfSeGZQo.htm)|Spell Effect: Wind Jump|Effet : Saut du vent|libre|
|[5p3bKvWsJgo83FS1.htm](spell-effects/5p3bKvWsJgo83FS1.htm)|Aura: Protective Wards|Aura : Champs de protection|libre|
|[5xCheSMgtQhQZm00.htm](spell-effects/5xCheSMgtQhQZm00.htm)|Spell Effect: Garden of Death (Critical Success)|Effet : Jardin mortel (Succès critique)|libre|
|[5yCL7InrJDHpaQjz.htm](spell-effects/5yCL7InrJDHpaQjz.htm)|Spell Effect: Ant Haul|Effet : Charge de fourmi|libre|
|[61Hl31nyzt63vvX9.htm](spell-effects/61Hl31nyzt63vvX9.htm)|Spell Effect: Phase Familiar|Effet : Déphasage de familier|libre|
|[6ArAZeZyYSNLI0X5.htm](spell-effects/6ArAZeZyYSNLI0X5.htm)|Spell Effect: Vital Luminance|Effet : Luminosité vitale|libre|
|[6BjslHgY01cNbKp5.htm](spell-effects/6BjslHgY01cNbKp5.htm)|Spell Effect: Armor of Bones|Effet : Armure d'os|libre|
|[6embuvXCpS3YOD5u.htm](spell-effects/6embuvXCpS3YOD5u.htm)|Spell Effect: Resilient Touch|Effet : Toucher de résilience|libre|
|[6GAztnHuQSwAp1k1.htm](spell-effects/6GAztnHuQSwAp1k1.htm)|Spell Effect: Adaptive Ablation|Effet : Dissonance adaptative|libre|
|[6gRRjn0h0DAUh7nQ.htm](spell-effects/6gRRjn0h0DAUh7nQ.htm)|Spell Effect: Fungal Infestation|Effet : Infestation fongique|libre|
|[6IvTWcispcDaw88N.htm](spell-effects/6IvTWcispcDaw88N.htm)|Spell Effect: Insect Form (Ant)|Effet : Forme d'insecte (Fourmi)|libre|
|[6SAUuqvptTeXtyfH.htm](spell-effects/6SAUuqvptTeXtyfH.htm)|Spell Effect: Sage's Curse (Success)|Effet : Malédiction du savant (Succès)|libre|
|[6TGcfVyzzVHEo7ke.htm](spell-effects/6TGcfVyzzVHEo7ke.htm)|Spell Effect: Acid Grip|Effet : Poigne acide|libre|
|[70qdCBokXBvKIUIQ.htm](spell-effects/70qdCBokXBvKIUIQ.htm)|Spell Effect: Vision of Weakness|Effet : Vision de faiblesse|libre|
|[70tKwsgD8aOj9SxG.htm](spell-effects/70tKwsgD8aOj9SxG.htm)|Spell Effect: Hidebound|Effet : Peau épaisse|libre|
|[782NyomkDHyfsUn6.htm](spell-effects/782NyomkDHyfsUn6.htm)|Spell Effect: Insect Form (Spider)|Effet : Forme d'insecte (Araignée)|libre|
|[7cYUiOONB2lZfSaA.htm](spell-effects/7cYUiOONB2lZfSaA.htm)|Spell Effect: Tremorsense|Effet : Perception des vibrations|libre|
|[7IUPl6dQUktXZh5t.htm](spell-effects/7IUPl6dQUktXZh5t.htm)|Spell Effect: Mind of Menace|Effet : Esprit de menace|libre|
|[7OrZBwdgj563YzlW.htm](spell-effects/7OrZBwdgj563YzlW.htm)|Spell Effect: Warding Aggression (Critical Success)|Effet : Agression protectrice (Succès critique)|libre|
|[7tfF8ifVvOKNud8t.htm](spell-effects/7tfF8ifVvOKNud8t.htm)|Spell Effect: Ooze Form (Gray Ooze)|Effet : Forme de vase (Vase grise)|libre|
|[7tYv9lY3ksSUny2h.htm](spell-effects/7tYv9lY3ksSUny2h.htm)|Spell Effect: Gray Shadow|Effet : Ombre grise|libre|
|[7vIUF5zbvHzVcJA0.htm](spell-effects/7vIUF5zbvHzVcJA0.htm)|Spell Effect: Tailwind (8 hours)|Effet : Vent arrière (8 heures)|libre|
|[7wHYSgNG6LaxxlOz.htm](spell-effects/7wHYSgNG6LaxxlOz.htm)|Spell Effect: Thermal Remedy|Effet : Remède thermique|libre|
|[7zJPd2BsFl82qFRV.htm](spell-effects/7zJPd2BsFl82qFRV.htm)|Spell Effect: Warding Aggression|Effet : Agression protectrice|libre|
|[7zy4W2RXQiMEr6cp.htm](spell-effects/7zy4W2RXQiMEr6cp.htm)|Spell Effect: Dragon Claws|Effet : Déluge de griffes|libre|
|[81TfqzTfIqkQA4Dy.htm](spell-effects/81TfqzTfIqkQA4Dy.htm)|Spell Effect: Thundering Dominance|Effet : Domination tonitruante|libre|
|[85S2TWoiPwtFsDT3.htm](spell-effects/85S2TWoiPwtFsDT3.htm)|Spell Effect: Hippocampus Retreat|Effet : Retraite de l'hippocampe|libre|
|[8adLKKzJy49USYJt.htm](spell-effects/8adLKKzJy49USYJt.htm)|Spell Effect: Song of Strength|Effet : Chanson de force|libre|
|[8aNZhlkzRTRKlKag.htm](spell-effects/8aNZhlkzRTRKlKag.htm)|Spell Effect: Dragon Form (Horned)|Effet : Forme de dragon (Cornu)|libre|
|[8ecGfjmxnBY3WWao.htm](spell-effects/8ecGfjmxnBY3WWao.htm)|Spell Effect: Thicket of Knives|Effet : Multitude de couteaux|libre|
|[8gqb5FMTaArsKdWB.htm](spell-effects/8gqb5FMTaArsKdWB.htm)|Spell Effect: Prismatic Armor|Effet : Armure prismatique|libre|
|[8GUkKvCeI0xljCOk.htm](spell-effects/8GUkKvCeI0xljCOk.htm)|Spell Effect: Stormwind Flight|Effet : Vol de l'ouragan|libre|
|[8jaYbxnP4Z5wvnQs.htm](spell-effects/8jaYbxnP4Z5wvnQs.htm)|Spell Effect: Transmigrate (Encounter)|Effet : Transmigration (Rencontre)|libre|
|[8olfnTmWh0GGPDqX.htm](spell-effects/8olfnTmWh0GGPDqX.htm)|Spell Effect: Inner Upheaval|Effet : Bouleversement intérieur|libre|
|[8UqFvOCZqecPam4Y.htm](spell-effects/8UqFvOCZqecPam4Y.htm)|Spell Effect: Volcanic Eruption|Effet : Éruption volcanique|libre|
|[8wCVSzWYcURWewbd.htm](spell-effects/8wCVSzWYcURWewbd.htm)|Spell Effect: Bestial Curse (Failure)|Effet : Malédiction bestiale (Échec)|libre|
|[8XaSpienzVXLmcfp.htm](spell-effects/8XaSpienzVXLmcfp.htm)|Spell Effect: Inspire Heroics (Strength, +3)|Effet : Composition fortissimo (Force, +3)|libre|
|[8y6Ap9xIsnseYYvk.htm](spell-effects/8y6Ap9xIsnseYYvk.htm)|Spell Effect: Tempest Cloak|Effet : Cape tempête|libre|
|[98XT2QUk6wvXIqf7.htm](spell-effects/98XT2QUk6wvXIqf7.htm)|Spell Effect: Death Knell|Effet : Mise à mort|libre|
|[9Tl9jGUKoj0wS73d.htm](spell-effects/9Tl9jGUKoj0wS73d.htm)|Spell Effect: Warp Step|Effet : Pas déformant|libre|
|[9yzlmYUdvdQshTDF.htm](spell-effects/9yzlmYUdvdQshTDF.htm)|Spell Effect: Bullhorn|Effet : Porte-voix|libre|
|[9ZIP6gWSp9OTEu8i.htm](spell-effects/9ZIP6gWSp9OTEu8i.htm)|Spell Effect: Pocket Library|Effet : Bibliothèque de poche|libre|
|[a3uZckqOY9zQWzZ2.htm](spell-effects/a3uZckqOY9zQWzZ2.htm)|Spell Effect: Read the Air|Effet : Lire l'ambiance|libre|
|[a5rWrWwuevTzs9Io.htm](spell-effects/a5rWrWwuevTzs9Io.htm)|Spell Effect: Untamed Form|Effet : Forme indomptée|libre|
|[A61eVVVyUuaUl3tz.htm](spell-effects/A61eVVVyUuaUl3tz.htm)|Spell Effect: Celestial Brand|Effet : Marque céleste|libre|
|[aaA6cgroFMRMsisy.htm](spell-effects/aaA6cgroFMRMsisy.htm)|Spell Effect: River Carving Mountains|Effet : Rivière creusant les montagnes|libre|
|[AAypmg9Jz6Ysf02a.htm](spell-effects/AAypmg9Jz6Ysf02a.htm)|Spell Effect: Endure|Effet : Endurer|libre|
|[aDOL3OAEWf3ka9oT.htm](spell-effects/aDOL3OAEWf3ka9oT.htm)|Spell Effect: Blood Ward|Effet : Protection du sang|libre|
|[AF4vQ1xoOiJ1ewH1.htm](spell-effects/AF4vQ1xoOiJ1ewH1.htm)|Spell Effect: Elemental Gift|Effet : Don élémentaire|libre|
|[AJkRUIdYLnt4QOOg.htm](spell-effects/AJkRUIdYLnt4QOOg.htm)|Spell Effect: Tempt Fate|Effet : Tenter le destin|officielle|
|[alyNtkHLNnt98Ewz.htm](spell-effects/alyNtkHLNnt98Ewz.htm)|Spell Effect: Accelerating Touch|Effet : Contact accélérant|libre|
|[AmsVO5Q6078mEvNt.htm](spell-effects/AmsVO5Q6078mEvNt.htm)|Spell Effect: Ill Omen|Effet : Mauvais présage|libre|
|[amTa9jSml9ioKduN.htm](spell-effects/amTa9jSml9ioKduN.htm)|Spell Effect: Insect Form (Beetle)|Effet : Forme d'insecte (Coléoptère)|libre|
|[an4yZ6dyIDOFa1wa.htm](spell-effects/an4yZ6dyIDOFa1wa.htm)|Spell Effect: Soothing Words|Effet : Paroles apaisantes|libre|
|[aNomC4orI44YGBGU.htm](spell-effects/aNomC4orI44YGBGU.htm)|Spell Effect: Charged Javelin (Attacker)|Effet : Javelot électrifié ((Attaquant))|libre|
|[b8bfWIICHOsGVzjp.htm](spell-effects/b8bfWIICHOsGVzjp.htm)|Spell Effect: Monstrosity Form (Phoenix)|Effet : Forme monstrueuse (Phénix)|libre|
|[b8BZHeuz5jH8E2SG.htm](spell-effects/b8BZHeuz5jH8E2SG.htm)|Spell Effect: Albatross Curse|Effet : Malédiction de l'albatros|libre|
|[b8JCq6n1STl3Wkwy.htm](spell-effects/b8JCq6n1STl3Wkwy.htm)|Spell Effect: Illusory Shroud|Effet : Voile illusoire|libre|
|[bBD7HFzBPlSxYrtW.htm](spell-effects/bBD7HFzBPlSxYrtW.htm)|Spell Effect: Wish-Twisted Form (Failure)|Effet : Forme déformée par un souhait (échec)|libre|
|[Bc2Bwuan3716eAyY.htm](spell-effects/Bc2Bwuan3716eAyY.htm)|Spell Effect: Font of Serenity|Effet : Source de sérénité|libre|
|[Bd86oAvK3RLN076H.htm](spell-effects/Bd86oAvK3RLN076H.htm)|Spell Effect: Angel Form (Movanic Deva)|Effet : Forme d'ange (Deva movanique)|libre|
|[BdgWuULYjFwlVkr7.htm](spell-effects/BdgWuULYjFwlVkr7.htm)|Spell Effect: Song of Silver|Effet : Chant d'argent|libre|
|[BDMEqBsumguTrMXa.htm](spell-effects/BDMEqBsumguTrMXa.htm)|Spell Effect: Devil Form (Erinys)|Effet : Forme de diable (Érinye)|libre|
|[beReeFroAx24hj83.htm](spell-effects/beReeFroAx24hj83.htm)|Spell Effect: Courageous Anthem|Effet : Hymne de courage|libre|
|[BfaFe1cI9IkpvmmY.htm](spell-effects/BfaFe1cI9IkpvmmY.htm)|Spell Effect: Countless Eyes|Effet : Yeux innombrables|libre|
|[BGv44XBGtD4zOJBd.htm](spell-effects/BGv44XBGtD4zOJBd.htm)|Spell Effect: Eat Fire|Effet : Manger le feu|libre|
|[BIIYUbCsJxb0P8gm.htm](spell-effects/BIIYUbCsJxb0P8gm.htm)|Spell Effect: Demon Form (Abrikandilu)|Effet : Forme de démon (Abrikandilu)|libre|
|[BKam63zT98iWMJH7.htm](spell-effects/BKam63zT98iWMJH7.htm)|Spell Effect: Inspire Heroics (Defense, +3)|Effet : Composition fortissimo (Défense, +3)|libre|
|[bOjuEX3qj7XAOoDF.htm](spell-effects/bOjuEX3qj7XAOoDF.htm)|Spell Effect: Insect Form (Scorpion)|Effet : Forme d'insecte (Scorpion)|libre|
|[BT1ofB6RvRocQOWO.htm](spell-effects/BT1ofB6RvRocQOWO.htm)|Spell Effect: Animal Form (Bull)|Effet : Forme animale (Taureau)|libre|
|[buXx8Azr4BYWPtFg.htm](spell-effects/buXx8Azr4BYWPtFg.htm)|Spell Effect: Blood Vendetta (Failure)|Effet : Vendetta du sang (Échec)|libre|
|[bzpW0owbAV3UX7UA.htm](spell-effects/bzpW0owbAV3UX7UA.htm)|Spell Effect: Monstrosity Form (Kaiju)|Effet : Forme monstrueuse (Kaiju)|libre|
|[C1GAlZ6Gmmw0QIJN.htm](spell-effects/C1GAlZ6Gmmw0QIJN.htm)|Spell Effect: Hymn of Healing|Effet : Hymne de guérison|libre|
|[c4cIfS2974nUJDPt.htm](spell-effects/c4cIfS2974nUJDPt.htm)|Spell Effect: Fey Form (Dryad)|Effet : Forme de fée (dryade)|libre|
|[CdAyAiMGESvgNQtz.htm](spell-effects/CdAyAiMGESvgNQtz.htm)|Spell Effect: Unfettered Movement|Effet : Mouvement sans entrave|libre|
|[CDNKDV3UsAp95D1m.htm](spell-effects/CDNKDV3UsAp95D1m.htm)|Spell Effect: Serrate|Effet : Dentelé|libre|
|[ceEA7nBGNmoR8Sjj.htm](spell-effects/ceEA7nBGNmoR8Sjj.htm)|Spell Effect: Litany of Self-Interest|Effet : Litanie d'égoïsme|libre|
|[Chol7ExtoN2T36mP.htm](spell-effects/Chol7ExtoN2T36mP.htm)|Spell Effect: Inspire Heroics (Defense, +2)|Effet : Composition fortissimo (Défense, +2)|libre|
|[con2Hzt47JjpuUej.htm](spell-effects/con2Hzt47JjpuUej.htm)|Spell Effect: Resist Energy|Effet : Résistance à l'énergie|libre|
|[cSoL5aMy3PCzM4Yv.htm](spell-effects/cSoL5aMy3PCzM4Yv.htm)|Spell Effect: Return the Favor|Effet : Rendre la faveur|libre|
|[CTdEsMIwVYqqkH50.htm](spell-effects/CTdEsMIwVYqqkH50.htm)|Spell Effect: Litany of Depravity|Effet : Litanie de dépravation|libre|
|[ctMxYPGEpstvhW9C.htm](spell-effects/ctMxYPGEpstvhW9C.htm)|Spell Effect: Forbidding Ward|Effet : Sceau d'interdiction|libre|
|[cVVZXNbV0nElVOPZ.htm](spell-effects/cVVZXNbV0nElVOPZ.htm)|Spell Effect: Light|Effet : Lumière|libre|
|[CWC2fPmlgixoIKy5.htm](spell-effects/CWC2fPmlgixoIKy5.htm)|Spell Effect: Clawsong|Effet : Chant de la griffe|libre|
|[cwetyC5o4dRyFWJZ.htm](spell-effects/cwetyC5o4dRyFWJZ.htm)|Spell Effect: Necromancer's Generosity|Effet : Générosité du nécromant|libre|
|[DAAtzP2QYmCzSNXk.htm](spell-effects/DAAtzP2QYmCzSNXk.htm)|Spell Effect: Sage's Curse (Failure)|Effet : Malédiction du savant (Échec)|libre|
|[DBaMtFHRPEg1JeLs.htm](spell-effects/DBaMtFHRPEg1JeLs.htm)|Spell Effect: Hidden Mind|Effet : Esprit dissimulé|libre|
|[Dcva6SCHr9vWE7nJ.htm](spell-effects/Dcva6SCHr9vWE7nJ.htm)|Spell Effect: Animal Feature|Effet : Caractéristique animalière|libre|
|[DdqWMj7cuf4S1bgr.htm](spell-effects/DdqWMj7cuf4S1bgr.htm)|Spell Effect: Frostbite|Effet : Morsure du froid|libre|
|[deG1dtfuQph03Kkg.htm](spell-effects/deG1dtfuQph03Kkg.htm)|Spell Effect: Shillelagh|Effet : Gourdin magique|libre|
|[DENMzySYANjUBs4O.htm](spell-effects/DENMzySYANjUBs4O.htm)|Spell Effect: Insect Form (Centipede)|Effet : Forme d'insecte (Mille-pattes)|libre|
|[DHYWmMGmKOpRSqza.htm](spell-effects/DHYWmMGmKOpRSqza.htm)|Spell Effect: Chromatic Armor|Effet : Armure chromatique|libre|
|[dIftJU6Ki2QSLCOD.htm](spell-effects/dIftJU6Ki2QSLCOD.htm)|Spell Effect: Divine Vessel (9th level)|Effet : Réceptacle divin rang 9|libre|
|[DLwTvjjnqs2sNGuG.htm](spell-effects/DLwTvjjnqs2sNGuG.htm)|Spell Effect: Rallying Anthem|Effet : Hymne de ralliement|libre|
|[DrNpuMj14wVj4bWF.htm](spell-effects/DrNpuMj14wVj4bWF.htm)|Spell Effect: Dragon Form (Fortune)|Effet : Forme de dragon (Chance)|libre|
|[dU4viL9kh554TKeB.htm](spell-effects/dU4viL9kh554TKeB.htm)|Spell Effect: Community Repair|Effet : Réparation communautaire|libre|
|[dWbg2gACxMkSnZag.htm](spell-effects/dWbg2gACxMkSnZag.htm)|Spell Effect: Protective Wards|Effet : Champs de protection|officielle|
|[DwM5qcFp4JgKhXrY.htm](spell-effects/DwM5qcFp4JgKhXrY.htm)|Spell Effect: Fey Form (Unicorn)|Effet : Forme de fée (Licorne)|libre|
|[dXq7z633ve4E0nlX.htm](spell-effects/dXq7z633ve4E0nlX.htm)|Spell Effect: Regenerate|Effet : Régénération|officielle|
|[dy4neJZ1g1XQnUZL.htm](spell-effects/dy4neJZ1g1XQnUZL.htm)|Spell Effect: Daemon Form (Venedaemon)|Effet : Forme de daémon (Vénédaémon)|libre|
|[ei9MIyZbIaP4AZmh.htm](spell-effects/ei9MIyZbIaP4AZmh.htm)|Spell Effect: Flame Wisp|Effet : Feu follet enflammé|libre|
|[Eik8Fj8nGo2GLcbn.htm](spell-effects/Eik8Fj8nGo2GLcbn.htm)|Spell Effect: Monstrosity Form (Sea Serpent)|Effet : Forme monstrueuse (Serpent de mer)|libre|
|[EKdqKCuyWSkpXpyJ.htm](spell-effects/EKdqKCuyWSkpXpyJ.htm)|Spell Effect: Ooze Form (Black Pudding)|Effet : Forme de vase (Poudding noir)|libre|
|[ElkXovNrHB0Doi6O.htm](spell-effects/ElkXovNrHB0Doi6O.htm)|Spell Effect: Haste|Effet : Rapidité|libre|
|[eotqxEWIgaK7nMpD.htm](spell-effects/eotqxEWIgaK7nMpD.htm)|Spell Effect: Blunt the Final Blade (Critical Success)|Effet : Émousser la Lame finale (Succès critique)|libre|
|[eQzFYXLYPrGc9EBI.htm](spell-effects/eQzFYXLYPrGc9EBI.htm)|Spell Effect: Share Vision|Effet : Partager la vision|libre|
|[eRRiss7y7TsneiEu.htm](spell-effects/eRRiss7y7TsneiEu.htm)|Spell Effect: Cloak of Light|Effet : Cape de lumière|libre|
|[EScdpppYsf9KhG4D.htm](spell-effects/EScdpppYsf9KhG4D.htm)|Spell Effect: Ghostly Weapon|Effet : Arme fantomatique|libre|
|[ETgzIIv3M2zvclAR.htm](spell-effects/ETgzIIv3M2zvclAR.htm)|Spell Effect: Dragon Form (Conspirator)|Effet : Forme de dragon (Comploteur)|libre|
|[F1APSdrw5uv672hf.htm](spell-effects/F1APSdrw5uv672hf.htm)|Spell Effect: Battlefield Persistence|Effet : Persévérance sur le champ de bataille|libre|
|[F4DTpDXNu5IliyhJ.htm](spell-effects/F4DTpDXNu5IliyhJ.htm)|Spell Effect: Animal Form (Deer)|Effet : Forme animale (Cerf)|libre|
|[fcalovjrB3bzpiDH.htm](spell-effects/fcalovjrB3bzpiDH.htm)|Spell Effect: Swampcall|Effet : Appel du marais|libre|
|[fCIT9YgGUwIc3Z9G.htm](spell-effects/fCIT9YgGUwIc3Z9G.htm)|Spell Effect: Draw the Lightning|Effet : Attirer la foudre|libre|
|[FD9Ce5pqcZYstcMI.htm](spell-effects/FD9Ce5pqcZYstcMI.htm)|Spell Effect: Blessing of Defiance|Effet : Bénédiction du défi|libre|
|[fEhCbATDNlt6c1Ug.htm](spell-effects/fEhCbATDNlt6c1Ug.htm)|Spell Effect: Extract Poison|Effet : Extraction du poison|libre|
|[fGK6zJ7mWz9D5QYo.htm](spell-effects/fGK6zJ7mWz9D5QYo.htm)|Spell Effect: Rapid Adaptation|Effet : Adaptation rapide|libre|
|[fIloZhZVH1xTnX4B.htm](spell-effects/fIloZhZVH1xTnX4B.htm)|Spell Effect: Plant Form (Shambler)|Effet : Forme de plante (Grand tertre)|libre|
|[FInDzRVFPfiRQl6l.htm](spell-effects/FInDzRVFPfiRQl6l.htm)|Spell Effect: Weapon Trance|Effet : Transe d'arme|libre|
|[Fjnm1l59KH5YJ7G9.htm](spell-effects/Fjnm1l59KH5YJ7G9.htm)|Spell Effect: Inspire Heroics (Strength, +2)|Effet : Composition fortissimo (Force, +2)|libre|
|[fKeZDm8kpDFK5HWp.htm](spell-effects/fKeZDm8kpDFK5HWp.htm)|Spell Effect: Devil Form (Sarglagon)|Effet : Forme de diable (Sarglagon)|libre|
|[FlmR5n228jLHuLQG.htm](spell-effects/FlmR5n228jLHuLQG.htm)|Spell Effect: Ancestral Memories|Effet : Souvenirs ancestraux|libre|
|[Fms3IfhXqHiKAxlC.htm](spell-effects/Fms3IfhXqHiKAxlC.htm)|Spell Effect: Entreat Spirit|Effet : Implorer l'esprit|libre|
|[fpGDAz2v5PG0zUSl.htm](spell-effects/fpGDAz2v5PG0zUSl.htm)|Spell Effect: Sure Strike|Effet : Coup assuré|libre|
|[FqG4zXjSoxq9qTlf.htm](spell-effects/FqG4zXjSoxq9qTlf.htm)|Spell Effect: Transmigrate (Skill)|Effet : Transmigration (Compétence)|libre|
|[FT5Tt2DKBRutDqbV.htm](spell-effects/FT5Tt2DKBRutDqbV.htm)|Spell Effect: Dread Ambience|Effet : Ambiance terrifiante|libre|
|[fVEevbKVBbCc1x2F.htm](spell-effects/fVEevbKVBbCc1x2F.htm)|Spell Effect: Embrace Nothingness|Effet : Embrasser l'impermanence|libre|
|[fvIlSZPwojixVvyZ.htm](spell-effects/fvIlSZPwojixVvyZ.htm)|Spell Effect: Lucky Number|Effet : Nombre porte bonheur|libre|
|[FxB7DS6CFPhd2hBr.htm](spell-effects/FxB7DS6CFPhd2hBr.htm)|Spell Effect: Magic's Vessel (Resistance)|Effet : Réceptacle magique (résistance)|libre|
|[GA8SSGtpuyUunP4D.htm](spell-effects/GA8SSGtpuyUunP4D.htm)|Spell Effect: Sting of the Sea|Effet : Piqûre de la mer|libre|
|[GcEFca6I8f5Y06z7.htm](spell-effects/GcEFca6I8f5Y06z7.htm)|Spell Effect: Swear Oath|Effet : Prêter serment|libre|
|[GDzn5DToE62ZOTrP.htm](spell-effects/GDzn5DToE62ZOTrP.htm)|Spell Effect: Divine Vessel|Effet : Réceptacle divin (Chaotique)|libre|
|[GhNVAYtoF5hK3AlD.htm](spell-effects/GhNVAYtoF5hK3AlD.htm)|Spell Effect: Touch of the Void|Effet : Toucher du vide|libre|
|[gKGErrsS1WoAyWub.htm](spell-effects/gKGErrsS1WoAyWub.htm)|Spell Effect: Aberrant Form (Gogiteth)|Effet : Forme d'aberration (Gogiteth)|libre|
|[GlggmEqkGVj1noOD.htm](spell-effects/GlggmEqkGVj1noOD.htm)|Spell Effect: Bottle the Storm|Effet : Tempête en bouteille|libre|
|[GnWkI3T3LYRlm3X8.htm](spell-effects/GnWkI3T3LYRlm3X8.htm)|Spell Effect: Runic Weapon|Effet : Arme runique|libre|
|[GogALNdgQ7Fn5oMG.htm](spell-effects/GogALNdgQ7Fn5oMG.htm)|Spell Effect: Benediction|Effet : Bénédiction défensive|libre|
|[gQnDKDeBTtjwOWAk.htm](spell-effects/gQnDKDeBTtjwOWAk.htm)|Spell Effect: Animal Form (Bear)|Effet : Forme animale (Ours)|libre|
|[Gqy7K6FnbLtwGpud.htm](spell-effects/Gqy7K6FnbLtwGpud.htm)|Spell Effect: Bless|Effet : Bénédiction offensive|libre|
|[gX8O0ArQXbEVDUbW.htm](spell-effects/gX8O0ArQXbEVDUbW.htm)|Spell Effect: Embrace the Pit|Effet : Étreinte de la fosse|libre|
|[h0CKGrgjGNSg21BW.htm](spell-effects/h0CKGrgjGNSg21BW.htm)|Spell Effect: Boost Eidolon|Effet : Booster l'eidolon|libre|
|[h2JzNunzO8hXiNV3.htm](spell-effects/h2JzNunzO8hXiNV3.htm)|Spell Effect: Lifelink Surge|Effet : Afflux du lien vital|libre|
|[Hc8e3aU9yLKPSw8o.htm](spell-effects/Hc8e3aU9yLKPSw8o.htm)|Spell Effect: Diadem of Divine Radiance|Effet : Diadème d'éclat divin|libre|
|[HDT5oiQXXnRdDIKR.htm](spell-effects/HDT5oiQXXnRdDIKR.htm)|Spell Effect: Sweetest Solstice|Effet : Solstice plus sucré|libre|
|[heAj9paC8ZRh7QEj.htm](spell-effects/heAj9paC8ZRh7QEj.htm)|Spell Effect: Fey Form (Redcap)|Effet : Forme de fée (Bonnet rouge)|libre|
|[HEbbxKtBzsLhFead.htm](spell-effects/HEbbxKtBzsLhFead.htm)|Spell Effect: Devil Form (Osyluth)|Effet : Forme de diable (Osyluth)|libre|
|[HF1r1psnITHD52B9.htm](spell-effects/HF1r1psnITHD52B9.htm)|Aura: Spiral of Horrors|Aura : Aura effroyable|libre|
|[hfcFc8cV5wF2evWP.htm](spell-effects/hfcFc8cV5wF2evWP.htm)|Spell Effect: Nature Incarnate|Effet : Incarnation de la nature|libre|
|[hkLhZsH3T6jc9S1y.htm](spell-effects/hkLhZsH3T6jc9S1y.htm)|Spell Effect: Veil of Dreams|Effet : Voile de rêves|libre|
|[hnfQyf05IIa7WPBB.htm](spell-effects/hnfQyf05IIa7WPBB.htm)|Spell Effect: Demon Form (Nabasu)|Effet : Forme de démon (Nabasu)|libre|
|[HoCUCi2jL1OLfXWR.htm](spell-effects/HoCUCi2jL1OLfXWR.htm)|Spell Effect: Unblinking Flame Aura|Effet : Aura de la flamme qui ne vacille pas|libre|
|[HoOujAdQWCN4E6sQ.htm](spell-effects/HoOujAdQWCN4E6sQ.htm)|Spell Effect: Oaken Resilience|Effet : Résilience du chêne|libre|
|[hpbCDbDOoVyhOmck.htm](spell-effects/hpbCDbDOoVyhOmck.htm)|Spell Effect: Everlight|Effet : Lumière continue|libre|
|[HtaDbgTIzdiTiKLX.htm](spell-effects/HtaDbgTIzdiTiKLX.htm)|Spell Effect: Triple Time|Effet : À trois temps|libre|
|[hXtK08bTnDBSzGTJ.htm](spell-effects/hXtK08bTnDBSzGTJ.htm)|Spell Effect: Iron Gut|Effet : Boyaux de fer|libre|
|[hya8NfBB1GJofTXm.htm](spell-effects/hya8NfBB1GJofTXm.htm)|Spell Effect: Unblinking Flame Ignition|Effet : Allumage de la flamme qui ne vacille pas|libre|
|[I4PsUAaYSUJ8pwKC.htm](spell-effects/I4PsUAaYSUJ8pwKC.htm)|Spell Effect: Ray of Frost|Effet : Rayon de givre|libre|
|[i9YITDcrq1nKjV5l.htm](spell-effects/i9YITDcrq1nKjV5l.htm)|Spell Effect: Infectious Melody (Success)|Effet : Mélodie contagieuse (Succès)|libre|
|[IcQMLYWYDMZbq3XE.htm](spell-effects/IcQMLYWYDMZbq3XE.htm)|Spell Effect: Inscrutable Mask|Effet : Masque insondable|libre|
|[Ig9p2rDXGYHpEuTx.htm](spell-effects/Ig9p2rDXGYHpEuTx.htm)|Spell Effect: Traveler's Transit|Effet : Voyageur en transit|libre|
|[IhorZCrhO4dCq6n3.htm](spell-effects/IhorZCrhO4dCq6n3.htm)|Spell Effect: Fortifying Brew|Effet : Breuvage fortifiant|libre|
|[ihv1azg80N3kj7Vo.htm](spell-effects/ihv1azg80N3kj7Vo.htm)|Spell Effect: Lift Nature's Caul (Bonus)|Effet : Vraie nature (bonus)|libre|
|[iJ7TVW5tDnZG9DG8.htm](spell-effects/iJ7TVW5tDnZG9DG8.htm)|Spell Effect: Competitive Edge|Effet : Avantage du compétiteur|libre|
|[IjoYgBgEYSTZKmab.htm](spell-effects/IjoYgBgEYSTZKmab.htm)|Spell Effect: Healer's Blessing|Effet : Bénédiction du guérisseur|libre|
|[iN6shVYuzvQ4A95i.htm](spell-effects/iN6shVYuzvQ4A95i.htm)|Spell Effect: Shifting Form|Effet : Forme changeante|libre|
|[inNfTmtWpsxeGBI9.htm](spell-effects/inNfTmtWpsxeGBI9.htm)|Spell Effect: Darkvision (24 hours)|Effet : Vision dans le noir (24 heures)|libre|
|[iOKhr2El8R6cz6YI.htm](spell-effects/iOKhr2El8R6cz6YI.htm)|Spell Effect: Dinosaur Form (Triceratops)|Effet : Forme de dinosaure (Tricératops)|libre|
|[ITErgFRfydm1xmnW.htm](spell-effects/ITErgFRfydm1xmnW.htm)|Spell Effect: Uplifting Overture|Effet : Prélude inspirant|libre|
|[IUxQCyO3EXSkId5F.htm](spell-effects/IUxQCyO3EXSkId5F.htm)|Spell Effect: Corrosive Body (Temp HP)|Effet : Corps corrosif (PV Temp)|libre|
|[IVAPEtALVgSNOBdk.htm](spell-effects/IVAPEtALVgSNOBdk.htm)|Spell Effect: Demon Form (Kithangian)|Effet : Forme de démon (Kithangian)|libre|
|[IWD5RehCxZVfgrX9.htm](spell-effects/IWD5RehCxZVfgrX9.htm)|Spell Effect: Elephant Form|Effet : Forme d'éléphant|libre|
|[iwzKtzNV5t1op2xs.htm](spell-effects/iwzKtzNV5t1op2xs.htm)|Spell Effect: Distracting Decoy|Effet : Leurre distrayant|libre|
|[iXQCXCMiiJ8WV6K7.htm](spell-effects/iXQCXCMiiJ8WV6K7.htm)|Spell Effect: Draconic Barrage|Effet : Barrage draconique|libre|
|[IXS15IQXYCZ8vsmX.htm](spell-effects/IXS15IQXYCZ8vsmX.htm)|Spell Effect: Darkvision|Effet : Vision dans le noir|libre|
|[IYvzqymG4xOhqFir.htm](spell-effects/IYvzqymG4xOhqFir.htm)|Spell Effect: Rousing Splash|Effet : Aspersion réveillante|libre|
|[iZYjxY0qYvg5yPP3.htm](spell-effects/iZYjxY0qYvg5yPP3.htm)|Spell Effect: Angelic Wings|Effet : Ailes d'ange|libre|
|[j2LhQ7kEQhq3J3zZ.htm](spell-effects/j2LhQ7kEQhq3J3zZ.htm)|Spell Effect: Animal Form (Frog)|Effet : Forme animale (Grenouille)|libre|
|[J60rN48XzBGHmR6m.htm](spell-effects/J60rN48XzBGHmR6m.htm)|Spell Effect: Element Embodied (Air)|Effet : Incarnation élémentaire (Air)|libre|
|[j9l4LDnAwg9xzYsy.htm](spell-effects/j9l4LDnAwg9xzYsy.htm)|Spell Effect: Life Connection|Effet : Connexion vitale|libre|
|[Jbj3aIUeDc7LOXH6.htm](spell-effects/Jbj3aIUeDc7LOXH6.htm)|Spell Effect: Frenzied Revelry|Effet : Festivités frénétiques|libre|
|[Jemq5UknGdMO7b73.htm](spell-effects/Jemq5UknGdMO7b73.htm)|Spell Effect: Shield|Effet : Bouclier|libre|
|[JfvVyMWuD8yvSrIe.htm](spell-effects/JfvVyMWuD8yvSrIe.htm)|Spell Effect: Discomfiting Whispers|Effet : Murmures déconcertants|libre|
|[JHpYudY14g0H4VWU.htm](spell-effects/JHpYudY14g0H4VWU.htm)|Spell Effect: Mountain Resilience|Effet : Résilience du rocher|libre|
|[JhxAUu3JEyERedMd.htm](spell-effects/JhxAUu3JEyERedMd.htm)|Spell Effect: Garden of Healing|Effet : Jardin de Guérison|libre|
|[jj0P4eGVpmdwZjlA.htm](spell-effects/jj0P4eGVpmdwZjlA.htm)|Spell Effect: Instant Armor|Effet : Armure instantanée|libre|
|[jp88SCE3VCRAyE6x.htm](spell-effects/jp88SCE3VCRAyE6x.htm)|Spell Effect: Element Embodied (Earth)|Effet : Incarnation élémentaire (Terre)|libre|
|[jPZXZjetdauqYuEH.htm](spell-effects/jPZXZjetdauqYuEH.htm)|Aura: Angelic Halo|Aura : Halo angélique|libre|
|[JqrTrvwV7pYStMXz.htm](spell-effects/JqrTrvwV7pYStMXz.htm)|Spell Effect: Levitate|Effet : Lévitation|libre|
|[JrNHFNxJayevlv2G.htm](spell-effects/JrNHFNxJayevlv2G.htm)|Spell Effect: Plant Form (Flytrap)|Effet : Forme de plante (Attrape-mouches)|libre|
|[jSvpjSGnIVBAuFDu.htm](spell-effects/jSvpjSGnIVBAuFDu.htm)|Spell Effect: Albatross Curse (Critical Failure)|Effet : Malédiction de l'albatros (Échec critique)|libre|
|[jtW3VfI5Kktuy3GH.htm](spell-effects/jtW3VfI5Kktuy3GH.htm)|Spell Effect: Dragon Form (Empyreal)|Effet : Forme de dragon (Empyréen)|libre|
|[jUUFipVlyAYEBFNI.htm](spell-effects/jUUFipVlyAYEBFNI.htm)|Spell Effect: Demon Form (Vloriak)|Effet : Forme de démon (Vloriak)|libre|
|[jvwKRHtOiPAm4uAP.htm](spell-effects/jvwKRHtOiPAm4uAP.htm)|Spell Effect: Aerial Form (Bat)|Effet : Forme aérienne (Chauve-souris)|libre|
|[jy4edd6pvJvJgOSP.htm](spell-effects/jy4edd6pvJvJgOSP.htm)|Spell Effect: Dragon Wings|Effet : Ailes de dragon|libre|
|[K8NWxqtnrSg26KPJ.htm](spell-effects/K8NWxqtnrSg26KPJ.htm)|Spell Effect: Mantle of the Frozen Heart|Effet : Manteau du coeur gelé|libre|
|[KcBqo33ekJHxZLHo.htm](spell-effects/KcBqo33ekJHxZLHo.htm)|Spell Effect: Fey Form (Naiad)|Effet : Forme de fée (Naïade)|libre|
|[KkDRRDuycXwKPa6n.htm](spell-effects/KkDRRDuycXwKPa6n.htm)|Spell Effect: Dinosaur Form (Brontosaurus)|Effet : Forme de dinosaure (Brontosaure)|libre|
|[kMoOWWBqDYmPcYyS.htm](spell-effects/kMoOWWBqDYmPcYyS.htm)|Spell Effect: Bathe in Blood|Effet : Baignade de sang|libre|
|[KtAJN4Qr2poTL6BB.htm](spell-effects/KtAJN4Qr2poTL6BB.htm)|Spell Effect: Bestial Curse (Critical Failure)|Effet : Malédiction bestiale (Échec critique)|libre|
|[kZ39XWJA3RBDTnqG.htm](spell-effects/kZ39XWJA3RBDTnqG.htm)|Spell Effect: Inspire Heroics (Courage, +2)|Effet : Composition fortissimo (Courage, +2)|libre|
|[kz3mlFwb9tV9bFwu.htm](spell-effects/kz3mlFwb9tV9bFwu.htm)|Spell Effect: Animal Form (Snake)|Effet : Forme animale (Serpent)|libre|
|[l8HkOKfiUqd3BUwT.htm](spell-effects/l8HkOKfiUqd3BUwT.htm)|Spell Effect: Ancestral Form|Effet : Forme ancestrale|libre|
|[l9HRQggofFGIxEse.htm](spell-effects/l9HRQggofFGIxEse.htm)|Spell Effect: Heroism|Effet : Héroïsme|libre|
|[l9kHsnm7dBQx52JI.htm](spell-effects/l9kHsnm7dBQx52JI.htm)|Spell Effect: Spirit of the Beast|Effet : Esprit de la bête|libre|
|[ldl1aU5ovTQMUHJ2.htm](spell-effects/ldl1aU5ovTQMUHJ2.htm)|Spell Effect: Frenzied Revelry (Critical)|Effet : Festivités frénétiques|libre|
|[Le5Sewk43o7V60nO.htm](spell-effects/Le5Sewk43o7V60nO.htm)|Spell Effect: Devouring Dark Form|Effet : Forme sombre dévorante|libre|
|[lEU3DH1tGjAigpEt.htm](spell-effects/lEU3DH1tGjAigpEt.htm)|Spell Effect: Energy Absorption|Effet : Absorption d'énergie|libre|
|[LfxwvZRwtrh8mQN0.htm](spell-effects/LfxwvZRwtrh8mQN0.htm)|Spell Effect: Harrowing|Effet : Tirage du Tourment|libre|
|[LgqKCxm8A09WCLxm.htm](spell-effects/LgqKCxm8A09WCLxm.htm)|Spell Effect: Devil Form (Levaloch)|Effet : Forme de diable (Levaloch)|libre|
|[lGU4GIF2GUn21zFa.htm](spell-effects/lGU4GIF2GUn21zFa.htm)|Spell Effect: Open the Wall of Ghosts (Critical Success)|Effet : Ouvrir le mur des fantômes (Succès critique)|libre|
|[LHREWCGPkWsc4GGJ.htm](spell-effects/LHREWCGPkWsc4GGJ.htm)|Spell Effect: Faerie Dust (Failure)|Effet : Poussière féerique (Échec)|libre|
|[lIl0yYdS9zojOZhe.htm](spell-effects/lIl0yYdS9zojOZhe.htm)|Spell Effect: Life-Giving Form|Effet : Forme génératrice de vie|libre|
|[LldX5hnNhKzGtOS0.htm](spell-effects/LldX5hnNhKzGtOS0.htm)|Spell Effect: Elemental Absorption|Effet : Absorption élémentaire|libre|
|[llrOM8rPP9nxIuEN.htm](spell-effects/llrOM8rPP9nxIuEN.htm)|Spell Effect: Insect Form (Mantis)|Effet : Forme d'insecte (Mante)|libre|
|[lmAwCy7isFvLYdGd.htm](spell-effects/lmAwCy7isFvLYdGd.htm)|Spell Effect: Element Embodied (Fire)|Effet : Incarnation élémentaire (Feu)|libre|
|[LMXxICrByo7XZ3Q3.htm](spell-effects/LMXxICrByo7XZ3Q3.htm)|Spell Effect: Downpour|Effet : Déluge|libre|
|[LMzFBnOEPzDGzHg4.htm](spell-effects/LMzFBnOEPzDGzHg4.htm)|Spell Effect: Unusual Anatomy|Effet : Anatomie étrange|libre|
|[lRfiYmsoQMJZ81NQ.htm](spell-effects/lRfiYmsoQMJZ81NQ.htm)|Spell Effect: Element Embodied (Metal)|Effet : Incarnation élémentaire (Métal)|libre|
|[LT5AV9vSN3T9x3J9.htm](spell-effects/LT5AV9vSN3T9x3J9.htm)|Spell Effect: Corrosive Body|Effet : Corps corrosif|libre|
|[lTL5VwNrZ5xiitGV.htm](spell-effects/lTL5VwNrZ5xiitGV.htm)|Spell Effect: Nudge the Odds|Effet : Renforcer les chances|libre|
|[lUL4nvIVYqFOwxAZ.htm](spell-effects/lUL4nvIVYqFOwxAZ.htm)|Spell Effect: Blink Charge|Effet : Charge clignotante|libre|
|[LXf1Cqi1zyo4DaLv.htm](spell-effects/LXf1Cqi1zyo4DaLv.htm)|Spell Effect: Shrink|Effet : Rétrécissement|libre|
|[lyLMiauxIVUM3oF1.htm](spell-effects/lyLMiauxIVUM3oF1.htm)|Spell Effect: Lay on Hands|Effet : Imposition des mains|libre|
|[LzsCQ1mRnPYDzNpo.htm](spell-effects/LzsCQ1mRnPYDzNpo.htm)|Spell Effect: Demon Form (Brimorak)|Effet : Forme de démon (Brimorak)|libre|
|[m6x0IvoeX0a0bZiQ.htm](spell-effects/m6x0IvoeX0a0bZiQ.htm)|Spell Effect: Unbreaking Wave Vapor|Effet : Vapeur de la vague inexorable|libre|
|[mAofA4oy3cRdT71K.htm](spell-effects/mAofA4oy3cRdT71K.htm)|Spell Effect: Penumbral Disguise|Effet : Déguisement pénombral|libre|
|[mCb9mWAmgWPQrkTY.htm](spell-effects/mCb9mWAmgWPQrkTY.htm)|Spell Effect: Oaken Resilience (Arboreal's Revenge)|Effet : Résilience du chêne (Vengeance de l'arboréen)|libre|
|[Me470HI6inX3Bovh.htm](spell-effects/Me470HI6inX3Bovh.htm)|Spell Effect: Guided Introspection|Effet : Introspection guidée|libre|
|[mhklZ6wjfty0bF44.htm](spell-effects/mhklZ6wjfty0bF44.htm)|Spell Effect: Speaking Sky|Effet : Ciel parlant|libre|
|[MjtPtndJx31q2N9R.htm](spell-effects/MjtPtndJx31q2N9R.htm)|Spell Effect: Amplifying Touch|Effet : Toucher amplificateur|libre|
|[mKw9WvqnhaUsPvvy.htm](spell-effects/mKw9WvqnhaUsPvvy.htm)|Spell Effect: Tempest Form|Effet : Forme tempêtueuse|libre|
|[mlvDokpnQBhvQrSk.htm](spell-effects/mlvDokpnQBhvQrSk.htm)|Spell Effect: Shields of the Spirit|Effet : Boucliers de l'esprit|libre|
|[mmJNE57hC7G3SPae.htm](spell-effects/mmJNE57hC7G3SPae.htm)|Spell Effect: Silence|Effet : Silence|libre|
|[mojpoFI7eni1RaRB.htm](spell-effects/mojpoFI7eni1RaRB.htm)|Spell Effect: Canopy Crawler|Effet : Arpenteur de la canopée|libre|
|[Mp7252yAsSA8lCEA.htm](spell-effects/Mp7252yAsSA8lCEA.htm)|Spell Effect: One with the Land|Effet : Uni à la terre|libre|
|[MqZ6FScbfGtXB8tt.htm](spell-effects/MqZ6FScbfGtXB8tt.htm)|Spell Effect: Runic Body|Effet : Corps runique|libre|
|[mrSulUdNbwzGSwfu.htm](spell-effects/mrSulUdNbwzGSwfu.htm)|Spell Effect: Glutton's Jaws|Effet : Mâchoires du glouton|libre|
|[MsXuzUrCGVuRGWK7.htm](spell-effects/MsXuzUrCGVuRGWK7.htm)|Spell Effect: Hand of the Apprentice|Effet : Main de l'apprenti|libre|
|[MuRBCiZn5IKeaoxi.htm](spell-effects/MuRBCiZn5IKeaoxi.htm)|Spell Effect: Fly|Effet : Vol|libre|
|[myWvjlGLvbzkSCNO.htm](spell-effects/myWvjlGLvbzkSCNO.htm)|Spell Effect: Cinder Gaze|Effet : Regard de cendre|libre|
|[mzDgsuuo5wCgqyxR.htm](spell-effects/mzDgsuuo5wCgqyxR.htm)|Spell Effect: Mirecloak|Effet : Cape-miroir|libre|
|[N1b28wOrZmuSjN9i.htm](spell-effects/N1b28wOrZmuSjN9i.htm)|Spell Effect: Shield (Amped)|Effet : Bouclier (amplifié)|libre|
|[ndj0TpLxyzbyzcm4.htm](spell-effects/ndj0TpLxyzbyzcm4.htm)|Spell Effect: Necrotize (Legs)|Effet : Nécroser (Jambes)|libre|
|[nemThuhp3praALY6.htm](spell-effects/nemThuhp3praALY6.htm)|Spell Effect: Zealous Conviction|Effet : Conviction zélée|libre|
|[nFOJ53IkO5khO4Rr.htm](spell-effects/nFOJ53IkO5khO4Rr.htm)|Spell Effect: Entwined Roots|Effet : Racines entremêlées|libre|
|[nHXKK4pRXAzrLdEP.htm](spell-effects/nHXKK4pRXAzrLdEP.htm)|Spell Effect: Take its Course (Affliction, Help)|Effet : Suivre son cours (Afflicton, aide)|libre|
|[nIryhRgeiacQw1Em.htm](spell-effects/nIryhRgeiacQw1Em.htm)|Spell Effect: Soothing Blossoms|Effet : Bourgeons apaisants|libre|
|[nkk4O5fyzrC0057i.htm](spell-effects/nkk4O5fyzrC0057i.htm)|Spell Effect: Soothe|Effet : Apaiser|libre|
|[nLige83aiMBh0ylb.htm](spell-effects/nLige83aiMBh0ylb.htm)|Spell Effect: Musical Accompaniment|Effet : Accompagnement musical|libre|
|[npFFTAxN44WWrGnM.htm](spell-effects/npFFTAxN44WWrGnM.htm)|Spell Effect: Wash Your Luck|Effet : Laver votre chance|libre|
|[NQZ88IoKeMBsfjp7.htm](spell-effects/NQZ88IoKeMBsfjp7.htm)|Spell Effect: Life Boost|Effet : Gain de vie|libre|
|[nU4SxAk6XreHUi5h.htm](spell-effects/nU4SxAk6XreHUi5h.htm)|Spell Effect: Infectious Enthusiasm|Effet : Enthousiasme communicatif|libre|
|[nUJSdm4fy6fcwsvv.htm](spell-effects/nUJSdm4fy6fcwsvv.htm)|Spell Effect: Lashunta's Life Bubble|Effet : Bulle de vie du lashunta|libre|
|[nWEx5kpkE8YlBZvy.htm](spell-effects/nWEx5kpkE8YlBZvy.htm)|Spell Effect: Dragon Form (Mirage)|Effet : Forme de dragon (Mirage)|libre|
|[NXzo2kdgVixIZ2T1.htm](spell-effects/NXzo2kdgVixIZ2T1.htm)|Spell Effect: Apex Companion|Effet : Compagnon alpha|libre|
|[NY4oUb2RNgkY0AfL.htm](spell-effects/NY4oUb2RNgkY0AfL.htm)|Spell Effect: Tempest Touch|Effet : Toucher tempêtueux|libre|
|[oaRt210JV4GZIHmJ.htm](spell-effects/oaRt210JV4GZIHmJ.htm)|Spell Effect: Rejuvenating Touch|Effet : Toucher rajeunissant|libre|
|[oDDS6D2KTjpbA491.htm](spell-effects/oDDS6D2KTjpbA491.htm)|Spell Effect: Glass Shield|Effet : Bouclier en verre|libre|
|[OeCn76SB92GPOZwr.htm](spell-effects/OeCn76SB92GPOZwr.htm)|Spell Effect: Dragon Form (Diabolic)|Effet : Forme de dragon (Diabolique)|libre|
|[oJbcmpBSHwmx6FD4.htm](spell-effects/oJbcmpBSHwmx6FD4.htm)|Spell Effect: Dinosaur Form (Deinonychus)|Effet : Forme de dinosaure (Deinonychus)|libre|
|[OlkrQOPjLclyyxCw.htm](spell-effects/OlkrQOPjLclyyxCw.htm)|Spell Effect: Mantle of the Unwavering Heart|Effet : Manteau du coeur inébranlable|libre|
|[oNAqqcxPjzPCJJmW.htm](spell-effects/oNAqqcxPjzPCJJmW.htm)|Spell Effect: Trade Death for Life|Effet : Échanger la mort contre la vie|libre|
|[OSapCU8heNHst21y.htm](spell-effects/OSapCU8heNHst21y.htm)|Spell Effect: Knock|Effet : Déblocage|libre|
|[OwvrQKuMLEktNWzA.htm](spell-effects/OwvrQKuMLEktNWzA.htm)|Spell Effect: Animate Rope|Effet : Animation de corde|libre|
|[p8F3MVUkGmpsUDOn.htm](spell-effects/p8F3MVUkGmpsUDOn.htm)|Spell Effect: Untwisting Iron Pillar|Effet : Pilier du fer qui ne plie pas|libre|
|[PANUWN5xXC20WBg2.htm](spell-effects/PANUWN5xXC20WBg2.htm)|Spell Effect: False Vitality|Effet : Fausse vitalité|libre|
|[pcK88HqL6LjBNH2h.htm](spell-effects/pcK88HqL6LjBNH2h.htm)|Spell Effect: Faerie Dust (Critical Failure)|Effet : Poussière féerique (Échec critique)|libre|
|[PDoTV4EhJp63FEaG.htm](spell-effects/PDoTV4EhJp63FEaG.htm)|Spell Effect: Draw Ire (Success)|Effet : Attirer la colère (Succès)|libre|
|[pE2TWhG97XbhgAdH.htm](spell-effects/pE2TWhG97XbhgAdH.htm)|Spell Effect: Incendiary Aura|Effet : Aura incendiaire|libre|
|[Pfllo68qdQjC4Qv6.htm](spell-effects/Pfllo68qdQjC4Qv6.htm)|Spell Effect: Prismatic Shield|Effet : Bouclier prismatique|libre|
|[PNEGSVYhMKf6kQZ6.htm](spell-effects/PNEGSVYhMKf6kQZ6.htm)|Spell Effect: Call to Arms|Effet : Appel aux armes|libre|
|[PpkOZVoHkBZUmddx.htm](spell-effects/PpkOZVoHkBZUmddx.htm)|Spell Effect: Ooze Form (Ochre Jelly)|Effet : Forme de vase (Gelée ocre)|libre|
|[pPMldkAbPVOSOPIF.htm](spell-effects/pPMldkAbPVOSOPIF.htm)|Spell Effect: Protect Companion|Effet : Protéger le compagnon|libre|
|[ppVKJY6AYggn2Fma.htm](spell-effects/ppVKJY6AYggn2Fma.htm)|Spell Effect: Goodberry|Effet : Corne d'abondance|libre|
|[PQHP7Oph3BQX1GhF.htm](spell-effects/PQHP7Oph3BQX1GhF.htm)|Spell Effect: Tailwind|Effet : Vent arrière|libre|
|[PQuQtDixruZmmvT4.htm](spell-effects/PQuQtDixruZmmvT4.htm)|Spell Effect: Primal Summons|Effet : Convocations primordiales|libre|
|[ptOqsN5FS0nQh7RW.htm](spell-effects/ptOqsN5FS0nQh7RW.htm)|Spell Effect: Animal Form (Cat)|Effet : Forme animale (Félin)|libre|
|[pW1jauNjZ1owaciQ.htm](spell-effects/pW1jauNjZ1owaciQ.htm)|Spell Effect: Strength of Mind|Effet : Force de l'esprit|libre|
|[pzbU0cdyRvE66XBi.htm](spell-effects/pzbU0cdyRvE66XBi.htm)|Spell Effect: Discern Secrets|Effet : Discerner les secrets|libre|
|[qcrUH9YItV0eujQB.htm](spell-effects/qcrUH9YItV0eujQB.htm)|Spell Effect: Angel Form (Tennin)|Effet : Forme d'ange (Tennin)|libre|
|[qD1OA6dx8h33nKFC.htm](spell-effects/qD1OA6dx8h33nKFC.htm)|Spell Effect: Ferrous Form|Effet : Forme ferreuse|libre|
|[QF6RDlCoTvkVHRo4.htm](spell-effects/QF6RDlCoTvkVHRo4.htm)|Effect: Shield Immunity|Effet : Immunité à Bouclier|libre|
|[QFeJq767fymcw6LA.htm](spell-effects/QFeJq767fymcw6LA.htm)|Spell Effect: Albatross Curse (Circumstance Bonus)|Effet : Malédiction de l'albatros (bonus de circonstances)|libre|
|[qhNUfwpkD8BRw4zj.htm](spell-effects/qhNUfwpkD8BRw4zj.htm)|Spell Effect: Magic Hide|Effet : Peau magique|libre|
|[QJRaVbulmpOzWi6w.htm](spell-effects/QJRaVbulmpOzWi6w.htm)|Spell Effect: Girzanje's March|Effet : Marche de Guirzanjé|libre|
|[QjZ1PpYIeWh9SbmD.htm](spell-effects/QjZ1PpYIeWh9SbmD.htm)|Spell Effect: Leaden Steps|Effet : Pas de plomb|libre|
|[qkwb5DD3zmKwvbk0.htm](spell-effects/qkwb5DD3zmKwvbk0.htm)|Spell Effect: Mystic Armor|Effet : Armure mystique|libre|
|[qlz0sJIvqc0FdUdr.htm](spell-effects/qlz0sJIvqc0FdUdr.htm)|Spell Effect: Weapon Surge|Effet : Arme améliorée|libre|
|[qn7uO5Ih01yLJot7.htm](spell-effects/qn7uO5Ih01yLJot7.htm)|Spell Effect: Gecko Grip|Effet : Adhérence du gecko|libre|
|[qO1Gj9l8gh5CMEbf.htm](spell-effects/qO1Gj9l8gh5CMEbf.htm)|Spell Effect: Sand Form|Effet : Forme de sable|libre|
|[Qp0dlhJaCzXIx73r.htm](spell-effects/Qp0dlhJaCzXIx73r.htm)|Spell Effect: Elemental Form|Effet : Forme élémentaire|libre|
|[qPaEEhczUWCQo6ux.htm](spell-effects/qPaEEhczUWCQo6ux.htm)|Spell Effect: Animal Form (Shark)|Effet : Forme animale (Requin)|libre|
|[qQLHPbUFASKFky1W.htm](spell-effects/qQLHPbUFASKFky1W.htm)|Spell Effect: Hyperfocus|Effet : Hyperacuité|libre|
|[Qr5rgoZvI4KmFY0N.htm](spell-effects/Qr5rgoZvI4KmFY0N.htm)|Spell Effect: Calm|Effet : Apaisement|libre|
|[QrigfqVwBCGPylth.htm](spell-effects/QrigfqVwBCGPylth.htm)|Spell Effect: Charged Javelin|Effet : Javelot électrifié|libre|
|[qXWCvqt24iczeN8V.htm](spell-effects/qXWCvqt24iczeN8V.htm)|Spell Effect: Traveling Workshop|Effet : Atelier itinérant|libre|
|[qzZmVjtc9feqoQwA.htm](spell-effects/qzZmVjtc9feqoQwA.htm)|Spell Effect: Wish-Twisted Form (Success)|Effet : Forme déformée par un souhait (succès)|libre|
|[R27azQfzeFuFc48G.htm](spell-effects/R27azQfzeFuFc48G.htm)|Spell Effect: Take its Course (Affliction, Hinder)|Effet : Suivre son cours (Affliction, entraver)|libre|
|[r3SWKHCXxahSV54y.htm](spell-effects/r3SWKHCXxahSV54y.htm)|Spell Effect: Enlarge Companion|Effet : Agrandissement de compagnon|libre|
|[r4XX7yzeEOPK7l2a.htm](spell-effects/r4XX7yzeEOPK7l2a.htm)|Spell Effect: Seal Fate|Effet : Sceller le destin|libre|
|[rAGOerZJH2TY6nvO.htm](spell-effects/rAGOerZJH2TY6nvO.htm)|Spell Effect: Angel Form (Kuribu)|Effet : Forme d'ange (Kuribu)|libre|
|[rAW5o8MjjuIhYhLg.htm](spell-effects/rAW5o8MjjuIhYhLg.htm)|Spell Effect: Fortissimo Composition|Effet : Composition fortissimo|libre|
|[RawLEPwyT5CtCZ4D.htm](spell-effects/RawLEPwyT5CtCZ4D.htm)|Spell Effect: Protection|Effet : Protection|libre|
|[rEsgDhunQ5Yx8KZx.htm](spell-effects/rEsgDhunQ5Yx8KZx.htm)|Spell Effect: Monstrosity Form (Cave Worm)|Effet : Forme monstrueuse (Ver des cavernes)|libre|
|[RfCEHpMoEAZvB9IZ.htm](spell-effects/RfCEHpMoEAZvB9IZ.htm)|Aura: Bless|Aura : Bénédiction offensive|libre|
|[rHXOZAFBdRXIlxt5.htm](spell-effects/rHXOZAFBdRXIlxt5.htm)|Spell Effect: Dragon Form (Adamantine)|Effet : Forme de dragon (Adamantin)|libre|
|[rjM25qfw5BKj9h97.htm](spell-effects/rjM25qfw5BKj9h97.htm)|Spell Effect: Entangling Flora|Effet : Flore enchevêtrante|libre|
|[rmtCkBCEwyg919N0.htm](spell-effects/rmtCkBCEwyg919N0.htm)|Spell Effect: Animal Form (Orca)|Effet : Forme animale (orque)|libre|
|[rnkQKEwUxrGruE1e.htm](spell-effects/rnkQKEwUxrGruE1e.htm)|Spell Effect: Revel in Retribution (Temp HP)|Effet : Se délecter du châtiment (PV temp)|libre|
|[rQaltMIEi2bn1Z4k.htm](spell-effects/rQaltMIEi2bn1Z4k.htm)|Spell Effect: Qi Form|Effet : Forme Qi|libre|
|[rTVZ0zwiKeslRw6p.htm](spell-effects/rTVZ0zwiKeslRw6p.htm)|Spell Effect: Untamed Shift|Effet : Transformation indomptée|libre|
|[rVkUzqS4rpZp9LS3.htm](spell-effects/rVkUzqS4rpZp9LS3.htm)|Spell Effect: Bind Undead|Effet : Lier un mort-vivant|libre|
|[s6CwkSsMDGfUmotn.htm](spell-effects/s6CwkSsMDGfUmotn.htm)|Spell Effect: Death Ward|Effet : Protection contre la mort|libre|
|[S75DOLjKaSJGMc0D.htm](spell-effects/S75DOLjKaSJGMc0D.htm)|Spell Effect: Fey Form (Elananx)|Effet : Forme de fée (Élananxe)|libre|
|[SAkdopdD8IzOeqs1.htm](spell-effects/SAkdopdD8IzOeqs1.htm)|Spell Effect: Cascade Countermeasure|Effet : Contremesures en cascade|libre|
|[sccNh8j1PKLHCKh1.htm](spell-effects/sccNh8j1PKLHCKh1.htm)|Spell Effect: Angel Form (Choral)|Effet : Forme d'ange (Choral)|libre|
|[ScF0ECWnfXMHYLDL.htm](spell-effects/ScF0ECWnfXMHYLDL.htm)|Spell Effect: Daemon Form (Leukodaemon)|Effet : Forme de daémon (Leukodaémon)|libre|
|[sdKsB8dmIiPEHePZ.htm](spell-effects/sdKsB8dmIiPEHePZ.htm)|Spell Effect: Cauterize Wounds|Effet : Cautériser les blessures|libre|
|[sfJyQKmoxSRo6FyP.htm](spell-effects/sfJyQKmoxSRo6FyP.htm)|Spell Effect: Aberrant Form (Gug)|Effet : Forme d'aberration (Gug)|libre|
|[sILRkGTwoBywy0BU.htm](spell-effects/sILRkGTwoBywy0BU.htm)|Spell Effect: Vapor Form|Effet : Forme vaporeuse|libre|
|[sipLHLOyS7sQ0KQV.htm](spell-effects/sipLHLOyS7sQ0KQV.htm)|Spell Effect: Debilitating Terror|Effet : Terreur débilitante|libre|
|[SjfDoeymtnYKoGUD.htm](spell-effects/SjfDoeymtnYKoGUD.htm)|Spell Effect: Aberrant Form (Otyugh)|Effet : Forme d'aberration (Otyugh)|libre|
|[slI9P4jUp3ERPCqX.htm](spell-effects/slI9P4jUp3ERPCqX.htm)|Spell Effect: Impeccable Flow|Effet : Flux impeccable|libre|
|[sN3mQ7YrPBogEJRn.htm](spell-effects/sN3mQ7YrPBogEJRn.htm)|Spell Effect: Animal Form (Canine)|Effet : Forme animale (Canidé)|libre|
|[sPCWrhUHqlbGhYSD.htm](spell-effects/sPCWrhUHqlbGhYSD.htm)|Spell Effect: Enlarge|Effet : Agrandissement|libre|
|[sXe7cPazOJbX41GU.htm](spell-effects/sXe7cPazOJbX41GU.htm)|Spell Effect: Demon Form (Hezrou)|Effet : Forme de démon (Hezrou)|libre|
|[SyF5kpZlZuBF4lMf.htm](spell-effects/SyF5kpZlZuBF4lMf.htm)|Spell Effect: Tomorrow's Dawn|Effet : Aube de demain|libre|
|[T0eQfYKgejJqZiBA.htm](spell-effects/T0eQfYKgejJqZiBA.htm)|Spell Effect: Arcane Explosion|Effet : Explosion arcanique|libre|
|[T3NbslG6huktyNRm.htm](spell-effects/T3NbslG6huktyNRm.htm)|Spell Effect: Summon Healing Servitor|Effet : Convocation de serviteur soigneur|libre|
|[T3t9776ataHzrmTs.htm](spell-effects/T3t9776ataHzrmTs.htm)|Spell Effect: Inside Ropes (3rd Rank)|Effet : Cordes internes (rang 3)|libre|
|[T5bk6UH7yuYog1Fp.htm](spell-effects/T5bk6UH7yuYog1Fp.htm)|Spell Effect: See the Unseen|Effet : Discerner l'invisible|libre|
|[T6XnxvsgvvOrpien.htm](spell-effects/T6XnxvsgvvOrpien.htm)|Spell Effect: Dinosaur Form (Stegosaurus)|Effet : Forme de Dinosaure (Stégosaure)|libre|
|[tC0Qk4AjYRd3csL7.htm](spell-effects/tC0Qk4AjYRd3csL7.htm)|Spell Effect: Swarm Form|Effet : Forme de nuée|libre|
|[tfdDpf9xSWgQer5g.htm](spell-effects/tfdDpf9xSWgQer5g.htm)|Spell Effect: Cosmic Form (Moon)|Effet : Forme cosmique (Lune)|libre|
|[ThFug45WHkQQXcoF.htm](spell-effects/ThFug45WHkQQXcoF.htm)|Spell Effect: Fleet Step|Effet : Pas rapide|libre|
|[tjC6JeZgLDPIMHjG.htm](spell-effects/tjC6JeZgLDPIMHjG.htm)|Spell Effect: Malignant Sustenance|Effet : Alimentation maléfique|libre|
|[TjGHxli0edXI6rAg.htm](spell-effects/TjGHxli0edXI6rAg.htm)|Spell Effect: Schadenfreude (Success)|Effet : Joie malsaine (Succès)|libre|
|[tk3go5Cl6Qt130Dk.htm](spell-effects/tk3go5Cl6Qt130Dk.htm)|Spell Effect: Animal Form (Ape)|Effet : Forme animale (Singe)|libre|
|[tNjimcyUwn8afeH6.htm](spell-effects/tNjimcyUwn8afeH6.htm)|Spell Effect: Gravity Weapon|Effet : Arme pesante|libre|
|[TpVkVALUBrBQjULn.htm](spell-effects/TpVkVALUBrBQjULn.htm)|Spell Effect: Stoke the Heart|Effet : Enflammer les coeurs|libre|
|[TrmNSuv6zWEiceqn.htm](spell-effects/TrmNSuv6zWEiceqn.htm)|Spell Effect: Incendiary Ashes (Failure)|Effet : Cendres incendiaires (Échec)|libre|
|[tu8FyCtmL3YYR2jL.htm](spell-effects/tu8FyCtmL3YYR2jL.htm)|Spell Effect: Plant Form (Arboreal)|Effet : Forme de plante (Arboréen)|libre|
|[tUelWvSaNZBng42K.htm](spell-effects/tUelWvSaNZBng42K.htm)|Spell Effect: Animal Form (Seal)|Effet : Forme animale (phoque)|libre|
|[TwtUIEyenrtAbeiX.htm](spell-effects/TwtUIEyenrtAbeiX.htm)|Spell Effect: Tangle Vine|Effet : Liane gênante|libre|
|[U2eD42cGwdvQMdN0.htm](spell-effects/U2eD42cGwdvQMdN0.htm)|Spell Effect: Fiery Body (9th Level)|Effet : Corps enflammé (rang 9)|officielle|
|[UAiaJYUDLtLToRUU.htm](spell-effects/UAiaJYUDLtLToRUU.htm)|Spell Effect: Deep Sight|Effet : Vision des profondeurs|libre|
|[ubHqpJwUwygkc2dR.htm](spell-effects/ubHqpJwUwygkc2dR.htm)|Spell Effect: Spiral of Horrors|Effet : Aura Effroyable|libre|
|[ubKx634Kbl1hHzL9.htm](spell-effects/ubKx634Kbl1hHzL9.htm)|Spell Effect: Snake Fangs|Effet : Crocs de serpent|libre|
|[uD13zIE22foqmFgt.htm](spell-effects/uD13zIE22foqmFgt.htm)|Spell Effect: Weaken Earth|Effet : Terre fragilisée|libre|
|[uDOxq24S7IT2EcXv.htm](spell-effects/uDOxq24S7IT2EcXv.htm)|Spell Effect: Object Memory (Weapon)|Effet : Mémoire de l'objet (Arme ou outil)|libre|
|[UFfItbPq9cVq3LNa.htm](spell-effects/UFfItbPq9cVq3LNa.htm)|Spell Effect: Vitrifying Blast|Effet : Déflagration vitrifiante|libre|
|[UH2sT6eW5e31Xytd.htm](spell-effects/UH2sT6eW5e31Xytd.htm)|Spell Effect: Dutiful Challenge|Effet : Défi de dévouement|libre|
|[uHUcP59Z5fAdomda.htm](spell-effects/uHUcP59Z5fAdomda.htm)|Spell Effect: Spirit Sense|Effet : Perception des esprits|libre|
|[uIMaMzd6pcKmMNPJ.htm](spell-effects/uIMaMzd6pcKmMNPJ.htm)|Spell Effect: Fiery Body|Effet : Corps enflammé|libre|
|[uiXWJVwWuMS3KvkV.htm](spell-effects/uiXWJVwWuMS3KvkV.htm)|Spell Effect: Embodiment of Battle|Effet : Incarnation de la Bataille|libre|
|[Uj9VFXoVMH0mTTdt.htm](spell-effects/Uj9VFXoVMH0mTTdt.htm)|Spell Effect: Organsight|Effet : Vision des organes|libre|
|[UjoNm3lrhlg4ctAQ.htm](spell-effects/UjoNm3lrhlg4ctAQ.htm)|Spell Effect: Aerial Form (Pterosaur)|Effet : Forme aérienne (Ptérosaure)|libre|
|[UPisuG2cfjMIJOPa.htm](spell-effects/UPisuG2cfjMIJOPa.htm)|Spell Effect: Claws of the Otter|Effet : Griffes de loutre|libre|
|[uPmHi7ZiCj7PWM9N.htm](spell-effects/uPmHi7ZiCj7PWM9N.htm)|Spell Effect: Elemental Motion|Effet : Déplacement élémentaire|libre|
|[uStjY2mD6seP1K7I.htm](spell-effects/uStjY2mD6seP1K7I.htm)|Spell Effect: Daemon Form (Lacridaemon)|Effet : Forme de daémon (Lacridaémon)|libre|
|[UtIOWubq7akdHMOh.htm](spell-effects/UtIOWubq7akdHMOh.htm)|Spell Effect: Fortify Summoning|Effet : Convocation fortifiée|libre|
|[UTLp7omqsiC36bso.htm](spell-effects/UTLp7omqsiC36bso.htm)|Spell Effect: Bane|Effet : Imprécation|officielle|
|[UVrEe0nukiSmiwfF.htm](spell-effects/UVrEe0nukiSmiwfF.htm)|Spell Effect: Reinforce Eidolon|Effet : Renforcer l'eidolon|libre|
|[UXdt1WVA66oZOoZS.htm](spell-effects/UXdt1WVA66oZOoZS.htm)|Spell Effect: Flame Barrier|Effet : Barrière de flammes|libre|
|[v09uwq1eHEAy2bgh.htm](spell-effects/v09uwq1eHEAy2bgh.htm)|Spell Effect: Unbreaking Wave Barrier|Effet : Barrière de la vague inexorable|libre|
|[V4a9pZHNUlddAwTA.htm](spell-effects/V4a9pZHNUlddAwTA.htm)|Spell Effect: Dragon Form (Omen)|Effet : Forme de dragon (Présage)|libre|
|[Vc97f8ChfcZzprlZ.htm](spell-effects/Vc97f8ChfcZzprlZ.htm)|Spell Effect: Runic Impression|Effet : Impression runique|libre|
|[Vd72sGiTl7Pq8iQv.htm](spell-effects/Vd72sGiTl7Pq8iQv.htm)|Spell Effect: Manifest Will|Effet : Manifester la volonté|libre|
|[vf2KJ4oGJY4efnKE.htm](spell-effects/vf2KJ4oGJY4efnKE.htm)|Spell Effect: Precious Metals|Effet : Métaux précieux|libre|
|[VFereWC1agrwgzPL.htm](spell-effects/VFereWC1agrwgzPL.htm)|Spell Effect: Inspire Heroics (Courage, +3)|Effet : Composition fortissimo (Courage, +3)|libre|
|[vFZ7hG2j2DIQGkXg.htm](spell-effects/vFZ7hG2j2DIQGkXg.htm)|Spell Effect: Pact Broker|Effet : Négociateur de pacte|libre|
|[vhFnQBvguBXo6vxx.htm](spell-effects/vhFnQBvguBXo6vxx.htm)|Spell Effect: Repel Metal|Effet : Répulsion du métal|libre|
|[ViBlOrd6hno3DiPP.htm](spell-effects/ViBlOrd6hno3DiPP.htm)|Spell Effect: Stumbling Curse|Effet : Malédiction titubante|libre|
|[VJpRUgSDtAO2TSRR.htm](spell-effects/VJpRUgSDtAO2TSRR.htm)|Spell Effect: Glimpse Weakness|Effet : Aperçu des faiblesses|libre|
|[vUjRvriyuHDZrsgc.htm](spell-effects/vUjRvriyuHDZrsgc.htm)|Spell Effect: Ghostly Shift|Effet : Transformation fantomatique|libre|
|[VVSOzHV6Rz2YNHRl.htm](spell-effects/VVSOzHV6Rz2YNHRl.htm)|Spell Effect: Contagious Idea (Pleasant Thought)|Effet : Idée contagieuse (Pensée plaisante)|libre|
|[VzokjUa8RMYo5Id6.htm](spell-effects/VzokjUa8RMYo5Id6.htm)|Spell Effect: Keen Smell|Effet : Odorat aiguisé|libre|
|[w1HwO7huxJoK0gHY.htm](spell-effects/w1HwO7huxJoK0gHY.htm)|Spell Effect: Element Embodied (Water)|Effet : Incarnation élémentaire (Eau)|libre|
|[W4lb3417rNDd9tCq.htm](spell-effects/W4lb3417rNDd9tCq.htm)|Spell Effect: Sacred Form|Effet : Forme sacrée|libre|
|[wdA5DW7YiYR4jXPs.htm](spell-effects/wdA5DW7YiYR4jXPs.htm)|Spell Effect: Overwhelming Perfume|Effet : Parfum surpuissant|libre|
|[WdXLgPashH5in5eB.htm](spell-effects/WdXLgPashH5in5eB.htm)|Spell Effect: Conductive Weapon|Effet : Arme conductrice|libre|
|[WEpgIGFwtRb3ef1x.htm](spell-effects/WEpgIGFwtRb3ef1x.htm)|Spell Effect: Angel Form (Balisse)|Effet : Forme d'ange (Balisse)|libre|
|[Wlg9dAFQBuuv9oVa.htm](spell-effects/Wlg9dAFQBuuv9oVa.htm)|Spell Effect: Anointed Ground|Effet : Zone sanctifiée|libre|
|[Wsgum7pZrPtASRf6.htm](spell-effects/Wsgum7pZrPtASRf6.htm)|Spell Effect: Word of Truth|Effet : Discours de vérité|libre|
|[WtuhkNCNAW1JaGSe.htm](spell-effects/WtuhkNCNAW1JaGSe.htm)|Aura: Manifest Will|Aura : Manifester la volonté|libre|
|[WWtSEJGwKY4bQpUn.htm](spell-effects/WWtSEJGwKY4bQpUn.htm)|Spell Effect: Vital Beacon|Effet : Fanal de vie|libre|
|[X1kkbRrh4zJuDGjl.htm](spell-effects/X1kkbRrh4zJuDGjl.htm)|Spell Effect: Demon Form (Babau)|Effet : Forme de démon (Babau)|libre|
|[X7RD0JRxhJV9u2LC.htm](spell-effects/X7RD0JRxhJV9u2LC.htm)|Spell Effect: Infuse Vitality|Effet : Infusion de vitalité|libre|
|[X9Na6IK8FSPcTuoc.htm](spell-effects/X9Na6IK8FSPcTuoc.htm)|Spell Effect: Foresight|Effet : Prémonition|libre|
|[XDqtLdHnjLVhtqAW.htm](spell-effects/XDqtLdHnjLVhtqAW.htm)|Spell Effect: Infectious Melody (Critical Failure)|Effet : Mélodie contagieuse (Échec critique)|libre|
|[xgZxYqjDPNtsQ3Qp.htm](spell-effects/xgZxYqjDPNtsQ3Qp.htm)|Spell Effect: Aerial Form (Wasp)|Effet : Forme aérienne (Guêpe)|libre|
|[XMBoKRRyooKnGkHk.htm](spell-effects/XMBoKRRyooKnGkHk.htm)|Spell Effect: Practice Makes Perfect|Effet : En forgeant on devient forgeron|libre|
|[xPNKt1aQc3dquKlt.htm](spell-effects/xPNKt1aQc3dquKlt.htm)|Spell Effect: Element Embodied (Wood)|Effet : Incarnation élémentaire (Bois)|libre|
|[xPVOvWNJORvm8EwP.htm](spell-effects/xPVOvWNJORvm8EwP.htm)|Spell Effect: Mimic Undead|Effet : Imiter un mort-vivant|libre|
|[xsy1yaCj0SVsn502.htm](spell-effects/xsy1yaCj0SVsn502.htm)|Spell Effect: Aberrant Form (Chuul)|Effet : Forme d'aberration (Chuul)|libre|
|[XT3AyRfx4xeXfAjP.htm](spell-effects/XT3AyRfx4xeXfAjP.htm)|Spell Effect: Physical Boost|Effet : Amélioration physique|libre|
|[XTgxkQkhlap66e54.htm](spell-effects/XTgxkQkhlap66e54.htm)|Spell Effect: Iron Gut (3rd Level)|Effet : Boyaux de fer (niveau 3)|libre|
|[XXtz5BDNNBmP9ub2.htm](spell-effects/XXtz5BDNNBmP9ub2.htm)|Spell Effect: Protector's Sphere|Effet : Sphère du protecteur|libre|
|[y4y0nusC97R7ZDL5.htm](spell-effects/y4y0nusC97R7ZDL5.htm)|Spell Effect: Elemental Betrayal|Effet : Trahison élémentaire|libre|
|[Y6aNYnGVXdAMvL7Y.htm](spell-effects/Y6aNYnGVXdAMvL7Y.htm)|Spell Effect: Thermal Stasis|Effet : Stase thermique|libre|
|[Y8OGpgAEgISpTwYd.htm](spell-effects/Y8OGpgAEgISpTwYd.htm)|Spell Effect: Albatross Curse (Failure)|Effet : Malédiction de l'albatros (Échec)|libre|
|[y9PJdDYFemhk6Z5o.htm](spell-effects/y9PJdDYFemhk6Z5o.htm)|Spell Effect: Agile Feet|Effet : Pieds Agiles|libre|
|[yb9q5nVA1N0FfO6D.htm](spell-effects/yb9q5nVA1N0FfO6D.htm)|Spell Effect: Hallowed Ground|Effet : Sol sacré|libre|
|[ydsLEGjY89Akc4oZ.htm](spell-effects/ydsLEGjY89Akc4oZ.htm)|Spell Effect: Pest Form|Effet : Forme de nuisible|libre|
|[yEQXaB7XyaROVqyb.htm](spell-effects/yEQXaB7XyaROVqyb.htm)|Spell Effect: Sweet Dream|Effet : Doux rêve|libre|
|[yGedCb78XX6TtTq3.htm](spell-effects/yGedCb78XX6TtTq3.htm)|Spell Effect: Imitate Fauna|Effet : Imitation de la faune|libre|
|[yl0Pm78RXW5R2k50.htm](spell-effects/yl0Pm78RXW5R2k50.htm)|Spell Effect: Infectious Melody (Failure)|Effet : Mélodie contagieuse (Échec)|libre|
|[yYDj0G4O3q5iGexx.htm](spell-effects/yYDj0G4O3q5iGexx.htm)|Spell Effect: Life's Fresh Bloom|Effet : Floraison de la vie|libre|
|[YzZs7r8VUOp7PiAW.htm](spell-effects/YzZs7r8VUOp7PiAW.htm)|Spell Effect: Magic Stone|Effet : Pierre magique|libre|
|[z2PYQCsDDoBZUwR5.htm](spell-effects/z2PYQCsDDoBZUwR5.htm)|Spell Effect: Wooden Fists|Effet : Poings de bois|libre|
|[zbTpf11NtbmizuzR.htm](spell-effects/zbTpf11NtbmizuzR.htm)|Spell Effect: Forge (Critical Failure)|Effet : Forge - Échec critique|libre|
|[zdyelSV3S2ZFHsT4.htm](spell-effects/zdyelSV3S2ZFHsT4.htm)|Spell Effect: Vindicator's Mark|Effet : Marque du vindicateur|libre|
|[ZGzhNFB3SM8owk85.htm](spell-effects/ZGzhNFB3SM8owk85.htm)|Spell Effect: Take Root|Effet : Prendre racine|libre|
|[ZHVtJKnur9PAF5TO.htm](spell-effects/ZHVtJKnur9PAF5TO.htm)|Spell Effect: Enduring Might|Effet : Puissance protectrice|libre|
|[zIRnnuj4lARq43DA.htm](spell-effects/zIRnnuj4lARq43DA.htm)|Spell Effect: Daemon Form (Meladaemon)|Effet : Forme de daémon (Méladaémon)|libre|
|[zjFN1cJEl3AMKiVs.htm](spell-effects/zjFN1cJEl3AMKiVs.htm)|Spell Effect: Nymph's Token|Effet : Amulette de la nymphe|libre|
|[ZlsuhS9J0S3PuvCO.htm](spell-effects/ZlsuhS9J0S3PuvCO.htm)|Spell Effect: Flicker|Effet : Osciller|libre|
|[znWiM5RYLvf8STmR.htm](spell-effects/znWiM5RYLvf8STmR.htm)|Spell Effect: Darkened Sight|Effet : Vision obscurcie|libre|
|[znwjWUvGOFQ6VYaE.htm](spell-effects/znwjWUvGOFQ6VYaE.htm)|Spell Effect: Entropic Wheel|Effet : Roue entropique|libre|
|[zPGVOLz6xhsQN35C.htm](spell-effects/zPGVOLz6xhsQN35C.htm)|Spell Effect: Envenom Companion|Effet : Compagnon venimeux|libre|
|[zPPZz6lcp87ALUde.htm](spell-effects/zPPZz6lcp87ALUde.htm)|Spell Effect: Ash Form|Effet : Forme cendreuse|libre|
|[zpxIwEjnLUSO1B4z.htm](spell-effects/zpxIwEjnLUSO1B4z.htm)|Spell Effect: Magic's Vessel|Effet : Réceptacle magique|libre|
|[zRKw95WMezr6TgiT.htm](spell-effects/zRKw95WMezr6TgiT.htm)|Spell Effect: Moon Frenzy|Effet : Frénésie lunaire|libre|
|[ZVPlFsk5Zimd8Mc9.htm](spell-effects/ZVPlFsk5Zimd8Mc9.htm)|Spell Effect: Phase Bolt|Effet : Carreau de phase|libre|
