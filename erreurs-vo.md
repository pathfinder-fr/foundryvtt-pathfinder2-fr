## Journal pages
### Archetypes
- Rivethun Involutionist :
    - The journal is missing the last feat (lvl 18) : `Master Rivethun Spellcasting`
- Starlit Sentinel :
    - The list of Focus spell is enclosed by `<h3>`tags where it should be `<h2>` and the following list of focus spells are nested with `<h2>`where it should be `<h3>`.
    - The Second focus spell is wrongly labelled `Luminous Stardust Healing` where it should be `Shining Starlight Attack`