#!/bin/bash

set -e

echo "Extraction des compendiums VO et construction des fichiers de traduction"
./prepare.py

echo "Mise à jour des fichiers status et dictionnaires"
./update-status.py

if [ $CI_DEPLOY_MODULE != "true" ]
then
    if [ $CI_UPDATE_LANG = "true" ]
    then
        echo "Commit et push des fichiers lang"
        git add ../lang
    fi
    echo "Commit et push des fichiers compendiums"
    git add ../archive ../data
    git diff-index --quiet --cached HEAD || git commit -m "Mise à jour des fichiers"
    git push -o ci.skip https://root:$ACCESS_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH.git HEAD:master
fi

echo "Done"
