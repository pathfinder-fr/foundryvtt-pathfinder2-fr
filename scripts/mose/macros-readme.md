Les macros de mose
=========================

- installer l'extension `ssmacro`
- copier `scripts/mose/changed-macro.json` dans votre dossier :
  `%USERPROFILE%\.vscode\extensions\joekon.ssmacro-0.6.0\macros` sous windows
  `.vscode/extensions/joekon.ssmacro-0.6.0/macros` sous Linux et MacOs
- ouvrir les keybindings (file > preferences > keyboard shortcuts)
- en haut a droite il y a une icone de fichier qui permet d'ouvrir le fichier `Keybindings.json`
- ajoutez la macro dans le json:
```
  {
  "key": "ctrl+NumPad1",
  "command": "ssmacro.macro",
  "args":
    {
      "file": "changed-macro.json"
    }
  }
```
- J'ai mis ctrl + le 1 du pavé numérique mais après c'est chacun son truc.
- et paf, en une touche, sur un fichier changé, quand j'ai fini mes modifs, je mets l'état comme il faut
