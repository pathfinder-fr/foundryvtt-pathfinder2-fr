#!/bin/sh

# run from root path
# export PF2E_PATH=""

[ -z $PF2E_PATH ] && echo "export PF2E_PATH= first" && exit 0

IDS=`grep '_id' $PF2E_PATH/packs/journals/gm-screen.json | cut -d\" -f4`

for i in `ls -1 data/journals/pages-GMScreen | cut -d. -f1`
do
  echo -n $i
  if `echo $IDS | grep -q -w $i`; then
    echo " ok"
  else
    echo " dup - moving to archive"
    git mv -f data/journals/pages-GMScreen/$i.htm archive/journals/
  fi
done