#!/bin/env python
#
# Script pour trouver les valeurs de dégâts et de DD
# Usage: ./scripts/mose/filescan.py data/ > found.tmp.md
#
# en general je copie
# (?:(?:\[|,)\d+d\d+|dc:\d+)
# dans le champs de recherche ca surligne dans chaque fichier la ou il faut faire attention

import sys
import os
import re

if len(sys.argv) <= 1:
  print("Give a path")
  exit(0)

where = sys.argv[1]
e = re.compile(r"État: aucune")
p = re.compile(r"(?:(?:\[|,)\d+d\d+|dc:\d+)")
q = re.compile(r"@Check\[[^\]]*\]")
b_fr = re.compile(r"(bonus (?:de statut|d'objet|de circonstances))")
b_en = re.compile(r"((?:item|status|circumstance) bonus)")
focus_fr = b_fr # changer à q si on veut faire les @Check
             # changer a p si on veut focus sur les dégâts
             # changer a b_en pour les types de bonus
focus_en = b_en # changer à q si on veut faire les @Check
             # changer a p si on veut focus sur les dégâts
             # changer a b_fr pour les types de bonus

pe_en = re.compile(r"-- Desc \(en\) --\n.*?\n-- Desc \(fr\) --\n", re.DOTALL)
pe_fr = re.compile(r"-- Desc \(fr\) --\n.*?\n-- End desc ---\n", re.DOTALL)

for root, dir, files in os.walk(where):
  for file in files:
    if root[0] != "." and file.endswith(".htm"):
      filename = os.path.join(root, file)
      with open(filename) as f:
        s = f.read()
        if not e.search(s):
          extract_en = ' '.join(pe_en.findall(s))
          data_en = ' '.join(focus_en.findall(extract_en))
          extract_fr = ' '.join(pe_fr.findall(s))
          data_fr = ' '.join(focus_fr.findall(extract_fr))
          data_fr_en = data_fr
          
          # special for bonus checks
          data_fr_en = data_fr_en.replace("bonus de statut", "status bonus")
          data_fr_en = data_fr_en.replace("bonus de circonstances", "circumstance bonus")
          data_fr_en = data_fr_en.replace("bonus d'objet", "item bonus")
          
          if data_en != data_fr_en:
            print('[](' + filename + ')')
            print('en: ' + data_en)
            print('fr: ' + data_fr_en + '\n')