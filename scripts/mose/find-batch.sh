#!/bin/sh

if [ -z $1 ]
then
  echo "give a path to data"
  exit 0
fi

p=$1

for i in `git grep -l 'État: changé' $p`
do
  if [ `git log -1 --oneline $i | grep -ic batch` -eq 1 ]
  then
    echo "[]($i)"
  fi
done
