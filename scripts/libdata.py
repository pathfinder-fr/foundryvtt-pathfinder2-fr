#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import re
import plyvel
import requests
import json

##########################################
# Packs
##########################################
#
# Liste des packs supportés, avec les réglages de traduction
#
#
# transl    Nom du pack traduit en français
# paths     Dictionnaire contenant le chemin des informations principales dans le json anglais
#   name    Nom de la propriété contenant le nom de la donnée
#   desc    Chemin de la propriété contenant la description à traduire
#   type1   Chemin de la donnée à utiliser comme première partie du nom de fichier. Si absent c'est uniquement l'id qui servira à nommer le fichier. Le fichier sera nommé type1-id.htm
#   type2   Chemin de la seconde données. Si type2 est présent, type1 doit l'être aussi. Le fichier sera nommé avec type1-type2-id.htm
# extract   Dictionnaire contenant la liste des champs supplémentaires à extraire dans la section ------- Data et à traduire.
#           La clé correspondra au nom du champ auquel sera ajouté FR et EN
# lists     Dictionnaire contenant la liste des champs supplémentaires à extraire sous forme de liste, dont les différentes valeurs seront extraites avec un "|" comme séparateur, et à traduire.
# items     (Bestiaire only) Les objets présents sur la créature
SUPPORTED = json.load(open("packs.json", "r", encoding="utf-8"))
CONFIGS = json.load(open("configs.json", "r", encoding="utf-8"))

SKILLS = ["Acrobatics", "Arcana", "Athletics", "Crafting", "Deception", "Diplomacy", "Intimidation", "Medicine",
          "Nature", "Occultism", "Performance", "Religion", "Society", "Stealth", "Survival", "Thievery"]

SKILLSFR = {
    "Acrobatics": "Acrobaties",
    "Arcana": "Arcanes",
    "Athletics": "Athlétisme",
    "Crafting": "Artisanat",
    "Deception": "Duperie",
    "Diplomacy": "Diplomatie",
    "Intimidation": "Intimidation",
    "Medicine": "Médecine",
    "Nature": "Nature",
    "Occultism": "Occultisme",
    "Performance": "Représentation",
    "Religion": "Religion",
    "Society": "Société",
    "Stealth": "Discrétion",
    "Survival": "Survie",
    "Thievery": "Vol"
}

class bcolors:
    OK = '\033[92m'  # GREEN
    WARNING = '\033[93m'  # YELLOW
    FAIL = '\033[91m'  # RED
    RESET = '\033[0m'  # RESET COLOR


def print_error(message):
    print(bcolors.FAIL + message + bcolors.RESET)


def print_warning(message):
    print(bcolors.WARNING + message + bcolors.RESET)


def leveldb_decode(root_folder):
    for folder in os.listdir(root_folder):
        folder_path = os.path.join(root_folder, folder)

        if os.path.isdir(folder_path):
            db = plyvel.DB(folder_path, create_if_missing=False)
            data = []

            for key, value in db:
                if value is not None:
                    data.append(json.loads(value))

            with open(os.path.join(root_folder, folder + '.json'), 'w') as f:
                json.dump(data, f, indent=2)

            db.close()


#
# cette fonction lit le fichier system.json et extrait les informations sur les packs
#
def getFolders(list):
    folders = {}
    for folder in list:
        folders[folder.get("name")] = ""
        if "folders" in folder:
            folders.update(getFolders(folder["folders"]))
    return folders


def getPacks():
    packs = {
        "packs": [],
        "packFolders": {}
    }

    # PF2 system
    response = json.loads(requests.get(
        "https://raw.githubusercontent.com/foundryvtt/pf2e/master/static/system.json").text)
    for p in response["packs"]:
        match = re.search('packs/([-\w]+)', p['path'])
        if match:
            packId = match.group(1).strip()
            if packId in SUPPORTED:
                if SUPPORTED[packId]["type"] == "Actor":
                    pack = {**CONFIGS["ACTORS"]}
                elif SUPPORTED[packId]["type"] == "Item":
                    pack = {**CONFIGS["ITEMS"]}
                else:
                    pack = {"name": "name", "desc": "system.description.value"}
                pack.update({'id': packId, 'fileName': p['name'], **SUPPORTED[packId]})
                packs["packs"].append(pack)
    packs["packFolders"][response["id"]] = getFolders(response["packFolders"])

    # PF2 Animal Companion
    response = json.loads(requests.get(
        "https://raw.githubusercontent.com/TikaelSol/PF2e-Animal-Companions/main/module.json").text)
    leveldb_decode("../packs-animal")
    for p in response["packs"]:
        match = re.search('packs/([-\w]+)', p['path'])
        if match:
            packId = match.group(1).strip()
            if packId in SUPPORTED:
                if SUPPORTED[packId]["type"] == "Actor":
                    pack = {**CONFIGS["ACTORS"]}
                elif SUPPORTED[packId]["type"] == "Item":
                    pack = {**CONFIGS["ITEMS"]}
                else:
                    pack = {"name": "name", "desc": "system.description.value"}
                pack.update({'id': packId, 'fileName': p['name'], **SUPPORTED[packId]})
                packs["packs"].append(pack)
    packs["packFolders"][response["id"]] = getFolders(response["packFolders"])

    return packs


#
# cette fonction tente une extraction d'une valeur dans un objet
# Ex: data.level.value => obj["data"]["level"]["value"]
#
def getObject(obj, path):
    element = obj
    for p in path.split('.'):
        if p in element and element[p] is not None:
            element = element[p]
        else:
            return None
    return element


def getValue(obj, path, digitFormat=True):
    element = getObject(obj, path)
    if element is None:
        return None
    elif isinstance(element, int):
        if digitFormat:
            return "%02d" % element
        else:
            return element
    elif isinstance(element, list):
        if len(element) == 0:
            return None
        if len(element) > 1:
            print_warning(
                "List has more than 1 element for '%s'! %s" % (element, path))
            return element[len(element) - 1]
        return element[0]
    elif element.isdigit() and digitFormat:
        return "%02d" % int(element)
    else:
        return element


def getList(obj, path):
    element = getObject(obj, path)
    if element is None:
        return []
    if isinstance(element, list):
        return [(d['value'].replace("\r\n", "") if isinstance(d['value'], str) else d['value']) for d in element if 'value' in d]
    if isinstance(element, dict):
        return list(element.values())
    else:
        return []

def getArray(obj, path):
    element = getObject(obj, path)
    if element is None:
        return []
    if isinstance(element, list):
        return [(d.replace("\r\n", "") if isinstance(d, str) else d) for d in element if isinstance(d, (int, float, str, bool))]
    else:
        return []


#
# Cette fonction extrait l'information d'un fichier .htm sous forme d'un tableau contenant les différents attributs
# au format nom: Valeur
#
# Liste des valeurs renvoyées :
#
# id            identifiant unique complet (ex: skill-15-Vk7BzAb3D9r226sI), obtenu à partir du nom de fichier sans le .htm
# nameEN        nom anglais (Name)
# nameFR        nom français (Nom)
# status        état de la traduction (État)
# oldstatus     état d'origine de la traudction (État d'origine)
# benefitsEN    avantage (du don?) en anglais (Benefits)
# benefitsFR    avantage (du don?) en français (Avantage)
# spoilersEN    Balise SpoilersEN
# spoilersFR    Balise SpoilersFR
# descrFR/EN    Description en français/anglais
# dataEN/FR     Tableau des différentes données stockées dans la partie ------ Data
# listsEN/FR    Tableau des différentes listes
# arraysEN/FR   Tableau des différentes listes
def fileToData(filepath):
    if os.path.isfile(filepath):
        with open(filepath, 'r', encoding='utf8') as f:
            content = f.readlines()

        match = re.search('(\w{16})\.htm', filepath)
        if not match:
            print_error("Invalid filename %s" % filepath)
            exit(1)

        data = {"_id": match.group(1)}
        section = "main"
        is_desc = False
        is_gm = False
        item_id = ""
        text = ""
        for line in content:
            obj = data
            if section == "items" and item_id != "":
                obj = data["items"][item_id]
            if line.startswith("ID"):
                item_id = line[3:].strip()
                data["items"][item_id] = {}
            elif line.startswith("Journal:"):
                data['journal'] = line[8:].strip()
            elif line.startswith("Name:"):
                name = obj.get("name", {})
                name.update({"en": line[5:].strip()})
                obj["name"] = name
            elif line.startswith("Nom:"):
                name = obj.get("name", {})
                name.update({"fr": line[4:].strip()})
                obj["name"] = name
            elif line.startswith("État:"):
                obj["status"] = line[5:].strip()
            elif line.startswith("État d'origine:"):
                obj["oldstatus"] = line[15:].strip()
            elif line.startswith("Changés:"):
                obj["changes"] = line[8:].strip().split(",")
            elif line.startswith("-- Desc (en) --"):
                is_desc = True
            elif line.startswith("-- Desc (fr) --"):
                desc = obj.get("desc", {})
                desc.update({"en": text.strip()})
                obj["desc"] = desc
                text = ""
            elif line.startswith("-- End desc ---"):
                desc = obj.get("desc", {})
                desc.update({"fr": text.strip()})
                obj["desc"] = desc
                is_desc = False
                text = ""
            elif line.startswith("-- GM notes (en) --"):
                is_gm = True
            elif line.startswith("-- GM notes (fr) --"):
                gm = obj.get("gm", {})
                gm.update({"en": text.strip()})
                obj["gm"] = gm
                text = ""
            elif line.startswith("-- End GM notes ---"):
                gm = obj.get("gm", {})
                gm.update({"fr": text.strip()})
                obj["gm"] = gm
                is_gm = False
                text = ""
            elif line.startswith(
                    "----- Fields ------------------------------------------------------------------"):
                obj["fields"] = {}
                section = "fields"
            elif line.startswith(
                    "----- Lists -------------------------------------------------------------------"):
                obj["lists"] = {}
                section = "lists"
            elif line.startswith(
                    "----- Arrays ------------------------------------------------------------------"):
                obj["arrays"] = {}
                section = "arrays"
            elif line.startswith(
                    "----- Items -------------------------------------------------------------------"):
                obj["items"] = {}
                section = "items"
            elif line.startswith(
                    "-------------------------------------------------------------------------------"):
                section = "main"
            elif is_desc or is_gm:
                text += line if len(line) > 0 else ""
            elif len(line.strip()) > 0:
                # tente de lire toutes les propriétés restantes comme des traductions FR/EN
                # on commence par rechercher le : en fin du mot
                sep = line.find(":")
                if sep < 0:
                    print(bcolors.FAIL + "Invalid data '%s' in file %s " % (line, filepath) + bcolors.RESET)
                else:
                    key = line[0:sep]
                    value = line[sep + 1:].strip()
                    # on prend tous les attributs qui finissent par FR ou EN
                    if key.endswith("EN") or key.endswith("FR"):
                        key = key[0:-2]
                        lang = line[sep - 2:sep]
                        if key.find(".") != -1:
                            split_key = key.split(".")
                            if "submappings" not in obj:
                                obj["submappings"] = {}
                            if split_key[0] not in obj["submappings"]:
                                obj["submappings"][split_key[0]] = {}
                            if split_key[1] not in obj["submappings"][split_key[0]]:
                                obj["submappings"][split_key[0]][split_key[1]] = {}
                            subMappingKeyField = obj["submappings"][split_key[0]][split_key[1]].get(split_key[2], {})
                            subMappingKeyField.update({lang.lower(): value.replace("\\n", "\n")})
                            obj["submappings"][split_key[0]][split_key[1]][split_key[2]] = subMappingKeyField
                        elif section == "fields":
                            field = obj["fields"].get(key, {})
                            field.update({lang.lower(): value.replace("\\n", "\n")})
                            obj["fields"][key] = field
                        elif section == "lists":
                            liste = obj["lists"].get(key, {})
                            liste.update({lang.lower(): value.split("|")})
                            obj["lists"][key] = liste
                        elif section == "arrays":
                            array = obj["arrays"].get(key, {})
                            array.update({lang.lower(): value.split("|")})
                            obj["arrays"][key] = array
                    else:
                        print(bcolors.FAIL + "Invalid data '%s' in file %s " % (line, filepath) + bcolors.RESET)

        return data

    else:
        return None


#
# cette fonction écrit les datas avec le benefits en plus
#
def dataToFile(data, filepath):
    with open(filepath, 'w', encoding='utf8') as df:
        if "journal" in data and data['journal'] is not None:
            df.write("Journal: " + data['journal'] + "\n")
        df.write(f"Name: {data.get('name', {}).get('en', '')}\n")
        df.write(f"Nom: {data.get('name', {}).get('fr', '')}\n")
        df.write(f"État: {data.get('status', '')}\n")
        if "changes" in data:
            df.write("Changés: " + ",".join(data["changes"]) + "\n")
        if "oldstatus" in data:
            df.write("État d'origine: " + data["oldstatus"] + "\n")
        df.write("\n")

        df.write("-- Desc (en) --\n")
        df.write(f"{data.get('desc', {}).get('en', '')}\n")
        df.write("-- Desc (fr) --\n")
        df.write(f"{data.get('desc', {}).get('fr', '')}\n")
        df.write("-- End desc ---\n")

        if "gm" in data and data["gm"] is not None:
            df.write("\n-- GM notes (en) --\n")
            df.write(f"{data.get('gm', {}).get('en', '')}\n")
            df.write("-- GM notes (fr) --\n")
            df.write(f"{data.get('gm', {}).get('fr', '')}\n")
            df.write("-- End GM notes ---\n")

        if data.get("fields") or data.get("submappings"):
            df.write("\n")
            df.write("----- Fields ------------------------------------------------------------------\n")
            for key, value in sorted(data.get("fields", {}).items()):
                if len(value) > 0:
                    df.write(f"%sEN: %s\n" % (key, str(value.get("en", "")).replace("\n", "\\n")))
                    df.write(f"%sFR: %s\n" % (key, value.get("fr", "").replace("\n", "\\n")))
            for key_mapping, value_mapping in data.get("submappings", {}).items():
                for mapping_id, mapping_value in value_mapping.items():
                    for field_id, field_value in mapping_value.items():
                        df.write("%s.%s.%sEN: %s\n" % (
                            key_mapping, mapping_id, field_id, str(field_value.get("en", "")).replace("\n", "\\n")))
                        df.write("%s.%s.%sFR: %s\n" % (
                            key_mapping, mapping_id, field_id, field_value.get("fr", "").replace("\n", "\\n")))
            df.write("-------------------------------------------------------------------------------\n")

        if data.get("lists"):
            df.write("\n")
            df.write("----- Lists -------------------------------------------------------------------\n")
            for key, value in sorted(data["lists"].items()):
                if len(value) > 0:
                    df.write(f"%sEN: %s\n" % (key, "|".join(value.get("en", [])).replace('\n', '').replace('\r', '').strip()))
                    df.write(f"%sFR: %s\n" % (key, "|".join(value.get("fr", [])).replace('\n', '').replace('\r', '').strip()))
            df.write("-------------------------------------------------------------------------------\n")

        if data.get("arrays"):
            df.write("\n")
            df.write("----- Arrays ------------------------------------------------------------------\n")
            for key, value in sorted(data["arrays"].items()):
                if len(value) > 0:
                    df.write(f"%sEN: %s\n" % (key, "|".join(value.get("en", [])).replace('\n', '').replace('\r', '').strip()))
                    df.write(f"%sFR: %s\n" % (key, "|".join(value.get("fr", [])).replace('\n', '').replace('\r', '').strip()))
            df.write("-------------------------------------------------------------------------------\n")

        if data.get("items"):
            df.write("\n")
            df.write("----- Items -------------------------------------------------------------------\n")
            for key, value in sorted(data.get("items").items()):
                df.write(f"ID: {key}\n")
                if "name" in value:
                    df.write(f"Name: {value.get('name').get('en', '')}\n")
                    df.write(f"Nom: {value.get('name').get('fr', '')}\n")
                if "submappings" in value:
                    for key_mapping, value_mapping in value["submappings"].items():
                        for mapping_id, mapping_value in value_mapping.items():
                            for field_id, field_value in mapping_value.items():
                                df.write("%s.%s.%sEN: %s\n" % (
                                    key_mapping, mapping_id, field_id, field_value.get("en", "").replace("\n", "\\n")))
                                df.write("%s.%s.%sFR: %s\n" % (
                                    key_mapping, mapping_id, field_id, field_value.get("fr", "").replace("\n", "\\n")))

                if "desc" in value:
                    df.write("-- Desc (en) --" + '\n')
                    df.write(f"{value.get('desc').get('en', '')}\n")
                    df.write("-- Desc (fr) --" + '\n')
                    df.write(f"{value.get('desc').get('fr', '')}\n")
                    df.write("-- End desc ---" + '\n')

                df.write("\n")
            df.write("-------------------------------------------------------------------------------\n")
