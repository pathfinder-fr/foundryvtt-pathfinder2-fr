#!/usr/bin/python3
# -*- coding: utf-8 -*-

from scripts.prepare import *

print('Loading packs...')
all_packs = getPacks()
packs = all_packs["packs"]

for p in packs:
    if p["id"] != "criticaldeck":
        continue
    print('Migrating pack %s' % p["id"])
    path = "../data/%s/" % p["id"]
    if not os.path.isdir(path):
        os.mkdir(path)
    all_files = os.listdir(path)

    with open(p["pack"] + "/" + p["id"] + ".json", 'r', encoding='utf8') as f:
        duplic = {}
        for obj in json.load(f):
            source = extract_all(obj, p)

            if "pages" in p:
                pages_directory = "../data/%s/pages-%s" % (p["id"], obj["name"].replace(" ", ""))
                if not os.path.exists(pages_directory):
                    os.makedirs(pages_directory)
                source_pages = extract_pages(obj, p['pages'])
                for source_page_id, source_page in source_pages.items():
                    page_filename = "%s.htm" % source_page_id
                    existing_page = fileToData("../data/%s-pages/%s.htm" % (p["id"], source_page_id))
                    if existing_page is None:
                        existing_page = source_page
                        existing_page["status"] = "aucune"
                    else:
                        recursive_merge(existing_page, source_page)
                    dataToFile(existing_page, "%s/%s" % (pages_directory, page_filename))
                    print("Migrated page %s" % page_filename)