#!/usr/bin/python3
# -*- coding: utf-8 -*-

from libdata import *

print('Loading packs...')
all_packs = getPacks()
packs = all_packs["packs"]

for p in packs:
    if p["type"] != "Actor":
        print("Skipping pack %s" % p["id"])
        continue
    print('Migrating pack %s' % p["id"])
    path = "../data/%s/" % p["id"]
    if not os.path.isdir(path):
        os.mkdir(path)
    all_files = os.listdir(path)

    for fpath in all_files:
        if "_folders.json" in fpath:
            continue

        # Fichiers de données
        data = fileToData(path + fpath)
        if "items" in data:
            toRemove = []
            for oldkey, item in data["items"].items():
                if "name" in item and item["name"].get("en") in SKILLS and "submappings" in item and "skillVariants" in item["submappings"]:
                    if "submappings" not in data:
                        data["submappings"] = {}
                    key = "%sVariants" % item["name"].get("en")
                    if "key" not in data["submappings"]:
                        data["submappings"][key] = {}
                    for sub_entry_key, sub_entry_value in item["submappings"]["skillVariants"].items():
                        if sub_entry_key not in data["submappings"][key]:
                            data["submappings"][key][sub_entry_key] = {}
                        for field_key, field_value in item["submappings"]["skillVariants"][sub_entry_key].items():
                            # Migrate skill variant
                            data["submappings"][key][sub_entry_key][field_key] = {}
                            data["submappings"][key][sub_entry_key][field_key]["en"] = re.sub("\+(\d+)", "", field_value["en"]).strip()
                            data["submappings"][key][sub_entry_key][field_key]["fr"] = re.sub("\+(\d+)", "", field_value["fr"]).strip()
                    toRemove.append(oldkey)
            for key in toRemove:
                del data["items"][key]
            if len(toRemove) > 0:
                dataToFile(data, "%s%s" % (path, fpath))